---
title: Kintsugi
subtitle: La técnica que nos enseña la resilicencia 
date: 2021-04-30
draft: false
description: 
tags: ["Kintsugi","Productividad","Personal","Cultura","Resilicencia"]
---

![](img/01.png)

El [**Kintsugi**](https://es.wikipedia.org/wiki/Kintsugi) es una técnica de origen japones que consiste en reparar objetos de cerámica usando polvo de oro, de forma que una vez reconstruidos, las roturas de la vasija destacan y no se ocultan, las grietas pasan a formar parte la nueva historia del objeto y además se considera que lo hacen más bello.

Este precioso arte revela mucho de una cultura y como trata con las "cosas" rotas (que a menudo nosotros relacionamos con objetos inservibles) y nos ofrece valiosas interpretaciones, se considera que el objeto imperfecto es aún más hermoso y especial porque sus cicatrices nos muestran una historia única, también nos habla de aceptar el cambio y saber acomodarlo a nuestras vidas.

El **Kintsugi** bien podría ser el resultado de la celebre frase de Edison _"No fracasé, sólo descubrí 999 maneras de como no hacer una bombilla"_ que da valor al error como método de aprendizaje, como camino al éxito y a la consecución de nuestras metras y logros. 

![](img/02.jpg)

Evidentemente, si bien no se trata de fomentar el error está bien aceptar que somos humanos y que es casi inevitable cometer errores y por supuesto aprender de cada uno de ellos, esta aceptación a todos los niveles puede que nos ayude a relajarnos y crear un clima de trabajo más propicio precisamente para no cometerlos en una cultura organizacional sana.