---
title: Detectar y notificar claves que expiran de un dominio MS Active Directory 
subtitle: Cmdlet en PowerShell
date: 2019-01-22
draft: false
description: Scripting para administrar cuentas dominio 
tags: ["PS","PowerShell","Scripting","Active Directory","Windows","MS","Domain","Server"]
---

# Introducción

Script en **PS** (PowerShell) que **busca las contraseñas de usuarios de un dominio que expiran antes de N días (seleccionable) desde el moment de su ejecución y los notifica vía correo electrónico** para que establezcan una nueva contraseña.

El script está divido en dos funciones, la principal `AccountExpiresNextDays` que se llama desde la ejecución principal del script pasandole como parámetro de entrada el número de días. 

Desde la función principal se invoca otra función llamada `SendMail` cuyo único propósito es mandar un correo a los usuarios de aquellas cuentas cuyas contraseñas expiran antes de N días desde hoy.

# Función AccountExpiresNextDays

Función que recibe un entero con el número de días, si no se especifica el número de días en la llamada usamos 10 por defecto.

```powershell
function AccountExpiresNextDays([int] $MaxDaysToExpire = 10) {
	# ....
} 
```

Usaremos la función `Get-ADUser`, para ello debemos importat el módulo de PS correspondiente para usar funciones de AD (Active Directory), en el inicio del fichero:

```powershell
Import-Module ActiveDirectory
```

La consulta [`Get-ADUser`](https://docs.microsoft.com/en-us/powershell/module/addsadministration/get-aduser?view=win10-ps) es la siguiente, buscamos cuentas de usuario que esten habilitadas, tengan el flag `PasswordNeverExpires` deshabilitado y que aún no hayan expirado, el resultado queda almacenado en un array que luedo podemos recorrer.

```powershell
$users = get-aduser -filter * -properties Name, PasswordNeverExpires, mail, PasswordExpired, PasswordLastSet |where {$_.Enabled -eq "True"} | where { $_.PasswordNeverExpires -eq $false } | where { $_.passwordexpired -eq $false }
```

Podemos obtener la política de claves del dominio y conocer el tiempo máximo que puede tener la cuenta la misma clave con el atributo [`MaxPasswordAge`](https://docs.microsoft.com/en-us/powershell/module/addsadministration/set-addefaultdomainpasswordpolicy?view=win10-ps). 

```powershell
$maxPasswordAge = (Get-ADDefaultDomainPasswordPolicy).MaxPasswordAge
```

Ahora recorremos los elementos obtenidos de la consulta `Get-ADUser`. Lo primero es obtener algunos datos de interés de la cuenta, especialmente la dirección de correo electrónico del usuario que guardamos en la variable `$emailaddress` y la fecha en la que fue establecida por la ultima vez la contraseña `$passwordSetDate`.

Con estos datos realizamos una serie de comprobaciones comparando los días que restan para que expire la contraseña con el número de días que hemos fijado para que compruebe en `MaxDaysToExpire` pasado como parámetro a la función inicialmente.

En la llamada la función `SendMail` deberiamos definir los parámetros de nuestro servidor de correo interno y la dirección del remitente que reciben los usuarios (campos 'XXXXX').

```powershell
foreach ($user in $users) {	
	$Name = (Get-ADUser $user | foreach { $_.Name})		    		
	$emailaddress = $user.mail		
	$passwordSetDate = (get-aduser $user -properties * | foreach { $_.PasswordLastSet })	
	$PasswordPol = (Get-AduserResultantPasswordPolicy $user)	

	if (($PasswordPol) -ne $null) {
		$maxPasswordAge = ($PasswordPol).MaxPasswordAge		
	}		
  
	$expireson = $passwordsetdate + $maxPasswordAge	
	$today = (get-date)
	$daystoexpire = (New-TimeSpan -Start $today -End $expireson).Days	                
	$messageDays = $daystoexpire
						
	if (($daystoexpire -ge "0") -and ($daystoexpire -lt $MaxDaysToExpire)) {				
		if (($sendmail -eq "Enabled")) {				
			SendMail 'XXXXX' 'XXXXX' $emailaddress $messageDays $expireson				
				Start-Sleep -s 1
		}			
			
		# If Logging is Enabled Log Details
		if (($logging) -eq "Enabled") {
			Add-Content $CSVFile "$date,$Name,$daystoExpire,$expireson"  			 
		} 			
	} 	
}# End User Processing
```



# Función SendMail

La función `SendMail` es perfectamente reutilizable para otros scripts PS (con pequeñas modificaciones), recibe como parámetros de entrada el servidor SMTP de correo, el remitente, el destinatario y el número de días que queda para que expire la contraseña.

```powershell
function SendMail(
	[string] $SMTPServer='XXXXXXXXXXXXXXXXXX', 
	[string] $From = 'XXXXXXXXXXXXXXX',
	[string] $To = '',
	[string] $DaysToExpire,
	$ExpireDate) {		
	
	$encoding  = New-Object System.Text.utf8encoding	
	$str_date=$ExpireDate.ToString("dd/MM/yyyy")		
	$Subject = "Aviso: Expiración contraseña en pocos días"
	$Body = "<p>Hola '$Name', tu contraseña expirará dentro de '$DaysToExpire' día(s) el día '$str_date', te sugerimos que la cambies antes de que llegue el día señalado para evitar problemas.</p>"	
	$Body+='<p>Si a pesar de todo sigues teniendo problemas contacta con informática</p>'	
			
	if ( $TestModeEnabled -eq $true ) {	
		$To = $TestDestEmail
	}
		
	Send-MailMessage -smtpServer $SMTPServer -From $From -To $To -subject $Subject -body $Body -Encoding $encoding -BodyAsHtml -Priority high 
} #FIN SendMail
```

# cmdlet completo

El script completo tiene algunas variables declaradas para realizar algunas pruebillas básicas. Con `$logging` definido  como "Enabled" almacena información de las cuentas de correo próximas a expirar la contraseña en un fichero CSV con nombre `CSVFileCSVFile`. Con `$TestModeEnabled` definido como  `True` envía todos los correos de notificación a la misma dirección de destino `$TestDestEmail` en lugar de a los usuarios "afectados".

```
Import-Module ActiveDirectory

$logging = "Disabled" 
$sendmail = "Enabled"
$date = Get-Date -format yyyyMMdd
$CSVFile = $date+"_pwd_expires_days.csv"

$TestDestEmail = 'XXXXXXXXXXXXXXXXXXX'
$TestModeEnabled = $False

#----------------------------------------------------------------------------------------------------

function SendMail(
	[string] $SMTPServer='XXXXXXXXXXXXXXXXXX', 
	[string] $From = 'XXXXXXXXXXXXXXX',
	[string] $To = '',
	[string] $DaysToExpire,
	$ExpireDate) {		
	
	$encoding  = New-Object System.Text.utf8encoding	
	$str_date=$ExpireDate.ToString("dd/MM/yyyy")		
	$Subject = "Aviso: Expiración contraseña en pocos días"
	$Body = "<p>Hola '$Name', tu contraseña expirará dentro de '$DaysToExpire' día(s) el día '$str_date', te sugerimos que la cambies antes de que llegue el día señalado para evitar problemas.</p>"	
	$Body+='<p>Si a pesar de todo sigues teniendo problemas contacta con informática</p>'	
			
	if ( $TestModeEnabled -eq $true ) {	
		$To = $TestDestEmail
	}
		
	Send-MailMessage -smtpServer $SMTPServer -From $From -To $To -subject $Subject -body $Body -Encoding $encoding -BodyAsHtml -Priority high 
} #FIN SendMail

#----------------------------------------------------------------------------------------------------

function AccountExpiresNextDays([int] $MaxDaysToExpire = 10) {	
	$users = get-aduser -filter * -properties Name, PasswordNeverExpires, mail, PasswordExpired, PasswordLastSet |where {$_.Enabled -eq "True"} | where { $_.PasswordNeverExpires -eq $false } | where { $_.passwordexpired -eq $false }
	
	$maxPasswordAge = (Get-ADDefaultDomainPasswordPolicy).MaxPasswordAge

	foreach ($user in $users) {	
		$Name = (Get-ADUser $user | foreach { $_.Name})		    		
		$emailaddress = $user.mail		
		$passwordSetDate = (get-aduser $user -properties * | foreach { $_.PasswordLastSet })	
		$PasswordPol = (Get-AduserResultantPasswordPolicy $user)	

		if (($PasswordPol) -ne $null) {
			$maxPasswordAge = ($PasswordPol).MaxPasswordAge		
		}		
  
		$expireson = $passwordsetdate + $maxPasswordAge	
		$today = (get-date)
		$daystoexpire = (New-TimeSpan -Start $today -End $expireson).Days	                
		$messageDays = $daystoexpire
						
		if (($daystoexpire -ge "0") -and ($daystoexpire -lt $MaxDaysToExpire)) {				
			if (($sendmail -eq "Enabled")) {				
				SendMail 'XXXXXXXXXXXXXX' 'XXXXXXXXXXXXX' $emailaddress $messageDays $expireson				
				Start-Sleep -s 1
			}			
			
			# If Logging is Enabled Log Details
			if (($logging) -eq "Enabled") {
				Add-Content $CSVFile "$date,$Name,$daystoExpire,$expireson"  			 
			} 			
		} 	
	}# End User Processing
	
} #FIN AccountExpiresNextDays

#----------------------------------------------------------------------------------------------------

AccountExpiresNextDays 15
```

# Enlaces 

* [Get-ADUser - Microsoft Docs](https://docs.microsoft.com/en-us/powershell/module/addsadministration/get-aduser?view=win10-ps).
* [Get-ADUser - PowerShell - SS64.com](https://ss64.com/ps/get-aduser.html).
* [Get-ADDefaultDomainPasswordPolicy - Microsoft Docs](https://docs.microsoft.com/en-us/powershell/module/addsadministration/get-addefaultdomainpasswordpolicy?view=win10-ps).
* [How to Install and Import PowerShell Active Directory Module ...](https://theitbros.com/install-and-import-powershell-active-directory-module/).
* [ForEach - PowerShell - SS64.com](https://ss64.com/ps/foreach.html).
* [Functions - PowerShell - SS64.com](https://ss64.com/ps/syntax-functions.html).
* [about_Functions | Microsoft Docs](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_functions?view=powershell-6).