#!/usr/bin/env python3

from random import randint, choice

class MapGrid:
    """Clase MapGrid con el método constructor sobrecargado que recibe como argumentos el
    ancho y alto del mapa a dibujar"""

    def __init__(self, width, height):
        """Método constructor sobrecargado que recibe ancho y alto del mapa como argumentos"""
        self.width = width
        self.height = height
        self.walls = [] # or initialize it with walls using get_walls()


def draw_grid(graph):
    """Función para pintar mapa que recibe como argumento una instancia de la clase MapGrid
    y dibuja el mapa como una rejilla de ancho y alto como atributos de la clase basada e puntos"""
    for y in range(graph.height):
        for x in range(graph.width):
            if (x, y) in graph.walls:
                print('# ', end="")
                #print("%%-%ds" % width % '#', end="")
            else:
                #print("%%-%ds" % width % '.', end="")
                print('. ', end="")        
        print()

def get_walls(g, pct=0.3):
    """ pct is the percentage of the map covered by walls """   
    out = []
    for i in range(int(g.height*g.width*pct)//2):
    #for i in range(int(g.height*g.width*pct)):
        x = randint(1, g.width-1)
        y = randint(1, g.height-2)
        """ We make two pieces of wall based on the same random x and y
                but adding a bit of noise in order to have passages and more
                cave-looking walls"""
            
        out.append((x, y))
        #out.append((x + choice([-1, 0, 1]), y + choice([-1, 0, 1])))
    return out

"""
def main():
    g = MapGrid(30,15)
    g.walls = get_walls(g)
    draw_grid(g)

main()
"""

"""
if __name__ == '__main__':
    main()
"""

#g = MapGrid(10,10)
g = MapGrid(30,15)
g.walls = get_walls(g)
draw_grid(g)

