#!/usr/bin/env python3

class MapGrid:
    """Clase MapGrid con el método constructor sobrecargado que recibe como argumentos el
    ancho y alto del mapa a dibujar"""

    def __init__(self, width=30, height=15):
        """Método constructor sobrecargado que recibe ancho y alto del mapa como argumentos"""
        self.width = width
        self.height = height

def draw_grid(map):
    """Función para pintar mapa que recibe como argumento una instancia de la clase MapGrid
    y dibuja el mapa como una rejilla de ancho y alto como atributos de la clase"""
    for y in range(map.height):
        for x in range(map.width):            
            print('. ', end="")
        print()  

g = MapGrid()
draw_grid(g)