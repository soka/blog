#!/usr/bin/env python3

from random import randint, choice

class MapGrid:
    """Clase MapGrid con el método constructor sobrecargado que recibe como argumentos el
    ancho y alto del mapa a dibujar"""

    def __init__(self, width=30, height=15):
        """Método constructor sobrecargado que recibe ancho y alto del mapa como argumentos"""
        self.width = width
        self.height = height
        self.walls = [] # or initialize it with walls using get_walls()


def draw_grid(graph):
    """Función para pintar mapa que recibe como argumento una instancia de la clase MapGrid
    y dibuja el mapa como una rejilla de ancho y alto como atributos de la clase basada e puntos"""
    for y in range(graph.height):
        for x in range(graph.width):
            if (x, y) in graph.walls:
                print('# ', end="")         
            else:                
                print('. ', end="")        
        print()

def get_walls(g, pct=0.3):
    """ g es una instancia de la clase MapGrid, pct es el porcentaje del mapa usado para paredes"""   
    out = []
    for i in range(int(g.height*g.width*pct)//2):    
        x = randint(1, g.width-1)
        y = randint(1, g.height-2)                    
        out.append((x, y))        
    return out

g = MapGrid()
g.walls = get_walls(g,0.5)
draw_grid(g)

