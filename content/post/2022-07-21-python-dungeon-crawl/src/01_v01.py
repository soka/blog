#!/usr/bin/env python3


class MapGrid:
    """Clase MapGrid con el método constructor sobrecargado que recibe como argumentos el
    ancho y alto del mapa a dibujar"""

    def __init__(self, width, height):
        """Método constructor sobrecargado que recibe ancho y alto del mapa como argumentos"""
        self.width = width
        self.height = height


#def draw_grid(graph, width=2):
def draw_grid(graph):
    """Función para pintar mapa que recibe como argumento una instancia de la clase MapGrid
    y dibuja el mapa como una rejilla de ancho y alto como atributos de la clase basada e puntos"""
    for y in range(graph.height):
        for x in range(graph.width):
            #print("%%-%ds" % width % '.', end="")
            print('. ', end="")
            #print('.', end="")
        print()

"""        
def main():
    g = MapGrid(30,15)
    draw_grid(g)
main()


if __name__ == '__main__':
    main()
"""

g = MapGrid(30,15)
draw_grid(g)