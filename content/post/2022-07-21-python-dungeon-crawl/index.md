---
title: Dungeon crawl 
subtitle: Desarrollo de un juego RPG ASCII en Python   
date: 2022-07-21
draft: false
description: Desarrollo de un juego RPG ASCII en Python   
tags: ["Python","Retos","Aplicaciones","Juegos","ASCII"]
---

![](img/06.png)

<!-- vscode-markdown-toc -->
* 1. [Introducción a lo juegos de mazmorras](#Introduccinalojuegosdemazmorras)
* 2. [Descripción del hackathon Nº2](#DescripcindelhackathonN2)
* 3. [Construcción paso a paso](#Construccinpasoapaso)
	* 3.1. [Paso 1. Lo básico, dibujar el mapa](#Paso1.Lobsicodibujarelmapa)
	* 3.2. [Paso 2: Crear las paredes de nuestra mazmorra](#Paso2:Crearlasparedesdenuestramazmorra)
		* 3.2.1. [Paso 2.1: Clase mapa](#Paso2.1:Clasemapa)
		* 3.2.2. [Paso 2.2: Función para crear paredes aleatorias](#Paso2.2:Funcinparacrearparedesaleatorias)
	* 3.3. [Paso 3: ¡Nuestro héroe se mueve por la mazmorra!](#Paso3:Nuestrohroesemueveporlamazmorra)
		* 3.3.1. [Paso 3.1: Movimiento del jugador](#Paso3.1:Movimientodeljugador)
		* 3.3.2. [Paso 3.2: Cambios en la función para pintar el mapa](#Paso3.2:Cambiosenlafuncinparapintarelmapa)
		* 3.3.3. [Paso 3.3: Constructor del mapa](#Paso3.3:Constructordelmapa)
	* 3.4. [Paso 4: Lógica del juego](#Paso4:Lgicadeljuego)
* 4. [Creditos y autoria original](#Creditosyautoriaoriginal)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Introduccinalojuegosdemazmorras'></a>Introducción a lo juegos de mazmorras

Un [juego de mazmorras](https://es.wikipedia.org/wiki/Juego_de_mazmorras) o juego de exploración de mazmorras ([dungeon crawl](https://en.wikipedia.org/wiki/Dungeon_crawl)) es un tipo de aventura para juegos de tablero, de rol y videojuegos en la que los **personajes jugadores exploran una gran mazmorra** (u otra estructura laberíntica similar como un castillo o una cueva), luchando contra los dragones, orcos y cualquier otro monstruo que se encuentren en su camino y recolectando los tesoros que protegían. 

Ejemplo de mapa:

![](img/01.png)

Podemos decir que el primer juego de este tipo o al menos uno que gozo de gran popularidad fue [Dungeons & Dragons](https://es.wikipedia.org/wiki/Dungeons_%26_Dragons) (1974),  se jugaba con tablero, papel y lápiz, más adelante llegaron las versiones en forma de videojuego para consolas y PCs. 

![](img/02.png)


##  2. <a name='DescripcindelhackathonN2'></a>Descripción del hackathon Nº2

Queremos crear nuestra primera versión del juego, por supuesto jugaremos por consola usando carácteres ASCII, somos amantes y nostálgicos de todo lo retro. **Queremos hacer un juego simple donde el objetivo es ir de un extremo al otro en una cuadrícula**. En el camino encontraremos monstruos y objetos (esta parte la dejaremos para más adelante), ¡y tendremos que descubrir el camino más seguro para atravesar a medida que la mazmorra se vuelve cada vez más peligrosa! Tu idea personal puede ser un poco o muy diferente, pero los fundamentos que revisaremos deberían aplicarse a tu propio proyecto.

##  3. <a name='Construccinpasoapaso'></a>Construcción paso a paso

###  3.1. <a name='Paso1.Lobsicodibujarelmapa'></a>Paso 1. Lo básico, dibujar el mapa

Primero, tenemos que definir cómo representaremos nuestro mapa de juego. Comenzaremos de la manera más simple posible, primero creando una cuadrícula para contener nuestro mapa y una función para mostrar el mapa. 

Crearemos una **clase** (`MapGrid`) donde pasamos como argumentos al **constructor sobrecargado el ancho y alto** del mapa (`width`, `height`), sino se especifican será de 30x15 por defecto.

La **función para pintar el mapa** (`draw_grid`) recibe **como argumento una instancia de la clase mapa** (`MapGrid`), cada posición del mapa se pintará con un punto “.”.

Figura: Ejemplo de un mapa de alto 30 y ancho 15.  Para separar un poco los puntos del mapa se ha añadido un espacio después de cada punto. Puedes consultar la función `print` en este [enlace](https://www.w3schools.com/python/ref_func_print.asp), para imprimir varios puntos en una sola línea se ha usado el argumento end (Optional. Specify what to print at the end. Default is '\n' (line feed)) para no provocar saltos de línea.   

![](img/03.png)

[src/01.py](src/01.py)

```python
class MapGrid:
    """Clase MapGrid con el método constructor sobrecargado que recibe como argumentos el
    ancho y alto del mapa a dibujar"""

    def __init__(self, width=30, height=15):
        """Método constructor sobrecargado que recibe ancho y alto del mapa como argumentos"""
        self.width = width
        self.height = height

def draw_grid(map):
    """Función para pintar mapa que recibe como argumento una instancia de la clase MapGrid
    y dibuja el mapa como una rejilla de ancho y alto como atributos de la clase"""
    for y in range(map.height):
        for x in range(map.width):            
            print('. ', end="")
        print()  

g = MapGrid()
draw_grid(g)
```

###  3.2. <a name='Paso2:Crearlasparedesdenuestramazmorra'></a>Paso 2: Crear las paredes de nuestra mazmorra

Ahora debemos crear otra función (`get_walls`) que pasando como argumento la instancia de nuestro mapa y el porcentaje de paredes que queremos retorna una lista con las posiciones (x,y) de las paredes (lista de listas).

[src/02.py](src/02.py)

```python
from random import randint, choice

class MapGrid:
    """Clase MapGrid con el método constructor sobrecargado que recibe como argumentos el
    ancho y alto del mapa a dibujar"""

    def __init__(self, width=30, height=15):
        """Método constructor sobrecargado que recibe ancho y alto del mapa como argumentos"""
        self.width = width
        self.height = height
        self.walls = [] # or initialize it with walls using get_walls()


def draw_grid(graph):
    """Función para pintar mapa que recibe como argumento una instancia de la clase MapGrid
    y dibuja el mapa como una rejilla de ancho y alto como atributos de la clase basada e puntos"""
    for y in range(graph.height):
        for x in range(graph.width):
            if (x, y) in graph.walls:
                print('# ', end="")         
            else:                
                print('. ', end="")        
        print()

def get_walls(g, pct=0.3):
    """ g es una instancia de la clase MapGrid, pct es el porcentaje del mapa usado para paredes"""   
    out = []
    for i in range(int(g.height*g.width*pct)//2):    
        x = randint(1, g.width-1)
        y = randint(1, g.height-2)                    
        out.append((x, y))        
    return out

g = MapGrid()
g.walls = get_walls(g,0.5)
draw_grid(g)
```

####  3.2.1. <a name='Paso2.1:Clasemapa'></a>Paso 2.1: Clase mapa

Debemos modificar la clase `MapGrid` creada previamente para añadir un nuevo atributo `walls` inicializado a lista vacía en el constructor.

####  3.2.2. <a name='Paso2.2:Funcinparacrearparedesaleatorias'></a>Paso 2.2: Función para crear paredes aleatorias

Vamos a crear una nueva función (get_walls) con dos argumentos:

1. **Instancia de la clase con nuestro mapa** (`MapGrid`). 
2. **Argumento opcional** (`pct`) con el **porcentaje de paredes** sobre el total de cuadrículas del mapa, cuando no se especifica será del 3% (expresado como 0.3).

Usaremos el módulo [random](https://docs.python.org/es/3/library/random.html) para distribuir de forma aleatoria las paredes en nuestra mazmorra (función `randint`). 

**Para calcular el número de paredes** haremos lo siguiente:  ancho*alto*porcentaje // 2. Por ejemplo para  una cuadrícula de 30 de ancho y 15 de alto haremos lo siguiente:  30*15*0.3 // 2 = 67.

Una vez que sabemos cuantas paredes vamos a colocar (representadas con el carácter“#”)  recorremos el número de paredes y para cada una escogemos una posición aleatoria ([randint](https://docs.python.org/es/3/library/random.html?highlight=randint#random.randint)) dentro del ancho y alto de la cuadrícula, iremos generando una lista que contiene listas con las posiciones x e y de las paredes.

Al final de la función **retornamos una lista de listas con las coordenadas de las paredes**, en el programa principal guardaremos en el atributo `walls` de la clase `MapGrid` la lista resultante

Figura: Mapa de ejemplo con paredes. El mapa es de dimensiones 30x15, el porcentaje de paredes no se ha especificado en la llamada `get_walls`, por defecto será 30% (0.3). 

![](img/04.png)

###  3.3. <a name='Paso3:Nuestrohroesemueveporlamazmorra'></a>Paso 3: ¡Nuestro héroe se mueve por la mazmorra!

Hasta aquí todo bien. Pero, ¿de qué sirve un mapa sin objetivo, personajes o interacciones? Bueno, dijimos que el objetivo sería llegar de un extremo al otro, digamos la entrada al final de la mazmorra (o el nivel de la mazmorra). Para hacerlo más fácil, **diremos que la esquina superior izquierda será la entrada y la esquina inferior derecha será la salida**; después de todo, nuestra función de generación de muros también se diseñó para evitar la creación de callejones sin salida desde/hacia estos dos puntos (las posibilidades son pequeñas, pero algunos mapas generados tendrán callejones sin salida, por lo que tendremos que manejar eso eventualmente, pero todavía no). También necesitará un personaje para moverse por el mapa, que representaremos como valores (x, y), y de la misma manera representaremos la entrada y la meta.

Como puedes ver en la figura de abajo, tenemos **4 controles simples y un personaje** que podemos mover desde un principio hasta un objetivo. No es mucho, pero está empezando a tomar la forma de un juego. 

Figura: Inicio del juego, el “$” representa nuestro héroe jugador que se mueve por la mazmorra. El carácter “>” representa la salida de la mazmorra a donde debe moverse el jugador (la salida y entrada o punto de inicio siempre serán los mismos, la esquina superior izquierda y la esquina inferior derecha respectivamente).

![](img/05.png)

[src/03.py](src/03.py)

```python
from random import randint, choice

class MapGrid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.walls = []
        self.start = (0, 0)
        self.goal = (width-1, height-1)
        self.player = (0, 0)

    def move_player(self, d):
        x = self.player[0]
        y = self.player[1]
        pos = None

        if d[0] == 'r':
            pos = (x + 1, y)
        if d[0] == 'l':
            pos = (x - 1, y)
        if d[0] == 'u':
            pos = (x, y - 1)
        if d[0] == 'd':
            pos = (x, y + 1)

        if pos not in self.walls:
            self.player = pos

        if pos == self.goal:
            print("You made it to the end!")


def draw_grid(g):
    for y in range(g.height):
        for x in range(g.width):
            if (x, y) in g.walls:
                symbol = '#'
            elif (x, y) == g.player:
                symbol = '$'
            elif (x, y) == g.start:
                symbol = '<'
            elif (x, y) == g.goal:
                symbol = '>'
            else:
                symbol = '.'            
            print(symbol+' ', end="")
        print()


def get_walls(g: MapGrid, pct=0.3):
        out = []
        for i in range(int(g.height*g.width*pct)//2):

            x = randint(1, g.width-2)
            y = randint(1, g.height-2)

            out.append((x, y))           
        return out

g = MapGrid(10, 10)
g.walls = get_walls(g)

while g.player != g.goal:
    draw_grid(g)
    d = input("Which way? (r, l, u, d)")
    g.move_player(d)
```

####  3.3.1. <a name='Paso3.1:Movimientodeljugador'></a>Paso 3.1: Movimiento del jugador

Vamos a definir un **método** (`move_player`) asociado a la clase mapa (`MapGrid`) que recibe un argumento el **movimiento a realizar**:

* ‘r’: Derecha
* ‘l’: Izquierda
* ‘u’: Arriba
* ‘d’: Abajo

Con el nuevo movimiento debemos debemos recalcular la posición de nuestro héroe (atributo `player` de la clase  `MapGrid`).  

Si la nueva posición está dentro de la lista de muros, simplemente no se tendrá en cuenta. 

Si el movimiento es fuera de la cuadrícula debería advertirlo y no tenerlo en cuenta.

La posición (x,y) del héroe en cada momento se guarda en un atributo (`player`) de la clase mapa. 

Si la nueva posición coincide con la posición de salida de la mazmorra (atributo `goal` de la clase `MapGrid`) debemos mostrar un mensaje "You made it to the end!".

####  3.3.2. <a name='Paso3.2:Cambiosenlafuncinparapintarelmapa'></a>Paso 3.2: Cambios en la función para pintar el mapa

Debemos modificar la función que pinta el mapa (`draw_grid`) para mostrar los nuevos elementos:  entrada (“<”) y salida (“>”) de la mazmorra y nuestro héroe dentro del mapa (“$”).

Como ya hemos hecho anteriormente, recorremos el mapa en alto y ancho con dos bucles anidados:

* Si la posición (x,y) está dentro de la lista de listas que contiene las paredes pintamos una pared “#”.
* En caso contrario:
    * Si la posición corresponde a la posición de nuestro héroe pintamos un “$”.
    * Si la posición es la de inicio pintamos “<”.
    * Si es la de fin pintamos “>”
    * En caso contrario pintamos una casilla “vacía” con “.”.

####  3.3.3. <a name='Paso3.3:Constructordelmapa'></a>Paso 3.3: Constructor del mapa

Debemos crear nuevos atributos al constructor sobrecargado del mapa (`MapGrid`):

* `start`: Lista con la posición (x,y) de salida, será (0,0).
* `goal`: Coordenadas de salida de la mazmorra (alto y ancho de la mazmorra o lo que es lo mismo, esquina inferior derecha).
* `player`: Lista que mantiene la posición actual de nuestro héroe en la mazmorra.

###  3.4. <a name='Paso4:Lgicadeljuego'></a>Paso 4: Lógica del juego

Ahora sólo queda crear la lógica de nuestro juego para ponerlo en marcha. 

Creamos una instancia de nuestra clase mapa y obtenemos las posiciones de las paredes.

Mientras la posición de nuestro héroe no coincida con la posición de salida de la mazmorra:

* Pintar mazmorra (`función draw_grid`)
* Leer movimiento del héroe ("Which way? (r, l, u, d)") 
* Mover al héroe.

##  4. <a name='Creditosyautoriaoriginal'></a>Creditos y autoría original






