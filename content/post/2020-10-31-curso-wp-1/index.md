---
title: La invasión de los blogs
subtitle: Sesión nº 1 curso WordPress
date: 2020-10-31
draft: false
description: Introdución a WordPress
tags: ["Wordpress","WP - Curso","WP","Cursos","Comunicación","Blogs","Internet"]
---

![](img/wordpress-logo-01.png)

## Pad de trabajo

[https://pad.riseup.net/p/penascalf52021](https://pad.riseup.net/p/penascalf52021)

## La blogosfera y el nuevo paradigma de la comunicación

Con el nuevo milenio nacen los blogs y la "autocomunicación" de masas (también se habla de la **desmasificación de la comunicación pública** mediante la personalización de los servicios de información), se produce un nuevo **[paradigma](https://es.wikipedia.org/wiki/Paradigma) de la comunicación**, hasta el momento los medios tradicionales estaban en manos de unos pocos (grandes empresas, _lobbies_ de presión y gobiernos básicamente), la **comunicación se reducía a un camino unidireccional** donde la sociedad consumía más o menos de forma pasiva el mensaje (radio, prensa, televisión, discos o libros).

![](img/09.jpg)

Imagen: Imprenta europea del siglo XV ([Fuente Wikipedia](https://es.wikipedia.org/wiki/Imprenta)), paradigma de la comunicación de los creadores [amanuenses](https://es.wikipedia.org/wiki/Copista) a un sistema mecánico.

El **nacimiento de los blogs** permite que personas "anónimas" informen de cualquier cosa desde el salón de sus casas, "**la libertad de prensa ya no es para el que tiene una prensa**", por primera vez la gente puede contar su propia historia en primera persona al mismo tiempo que los grandes [_mass media_](https://es.wikipedia.org/wiki/Medio_de_comunicaci%C3%B3n_de_masas) sin contar con intermediarios. Se empieza a hablar del **periodismo ciudadano** que en muchos casos contradice el relato oficial o incluso avergüenza a las grandes cadenas supeditadas a intereses oscuros.

En **1998** se produce un hito en la red, nace [**Blogger**](https://es.wikipedia.org/wiki/Blogger), es una de las primeras **herramientas de publicación de bitácora en línea** donde los contenidos son mostrados en orden cronológico inverso. El usuario no debe escribir ningún código y usando un formulario pública contenidos casi al instante, para entonces ya existían [Slashdot](https://es.wikipedia.org/wiki/Slashdot) (1997) y [LiveJournal](https://es.wikipedia.org/wiki/LiveJournal).

![](img/01.png)

Blogger, about page on Pyra website (screenshot February 2000) [Fuente](https://www.firstversions.com/2015/08/blogger.html).

Hasta ese momento las Webs eran cosa de especialistas que conocían HTML y podían contratar un servicio de alojamiento, a partir de ese momento los **blogs y wikis** ([Wikipedia](https://es.wikipedia.org/wiki/Wikipedia) nace en el 2001 bajo licencia [GPL](https://es.wikipedia.org/wiki/GNU_General_Public_License)) **permiten publicar contenido de forma fácil**, sólo es necesario escribir texto en un editor [WYSIWYG](https://es.wikipedia.org/wiki/WYSIWYG) preocupándonos a lo sumo de las negritas y los títulos, en unos minutos cualquier persona pone en marcha su sitio Web y empieza a publicar contenidos. En poco tiempo se produce una "invasión" de blogs (The New York Times [Invasion of the 'Blog': A Parallel Web of Personal Journals](https://www.nytimes.com/2000/12/28/technology/invasion-of-the-blog-a-parallel-web-of-personal-journals.html)) interconectados y mantenidos por "aficionados" no profesionales de la comunicación ni expertos técnicos que informan de cualquier cosa imaginable.

## El nacimiento y evolución de WordPress

![](img/08.png)

Fuente: [https://codex.wordpress.org/WordPress_Versions](https://codex.wordpress.org/WordPress_Versions).

[**WordPress**](https://es.wikipedia.org/wiki/WordPress) ve la luz en el 2003 (un _fork_ de [b2/cafelog](https://ma.tt/2003/01/the-blogging-software-dilemma/) con licencia GPL), se populariza inicialmente como un [CMS](https://es.wikipedia.org/wiki/Sistema_de_gesti%C3%B3n_de_contenidos) o blog pero permite crear casi cualquier tipo de Web, se licencia como **[GPL](https://es.wikipedia.org/wiki/GNU_General_Public_License) y es software libre**, está **desarrollado en lenguaje [PHP](https://es.wikipedia.org/wiki/PHP)** que se ejecuta sobre un servidor HTTP [Apache](https://es.wikipedia.org/wiki/Servidor_HTTP_Apache) normalmente, como sistema de gestión de base de datos usa [MySQL](https://es.wikipedia.org/wiki/MySQL) (aunque puede funcionar con Nginx, MariaDB o PostreSQL).

Este es el aspecto que luce el _backend_ de administración en la **primera versión de WordPress** ([v0.71, Mayo del 2003](http://wordpress.org/news/2003/05/wordpress-now-available/)), directamente tiene un formulario de publicación de _posts_ y poco más, las funcionalidades son muy limitadas, sólo se puede **asignar una categoría a un _post_**.

![](img/02.png)

No es hasta la **[versión 1.2](http://wordpress.org/news/2004/05/heres-the-beef/) en Mayo del 2004 cuando se introducen los _plugins_**, esto permite a desarrolladores y usuarios extender las funcionalidades de WordPress y compartirlos si así lo desean, **WordPress se está abriendo a la comunidad**, algo opuesto a lo que están haciendo otros aplicativos de la industria del _blogging_.

La **[versión 1.5](http://wordpress.org/news/2005/02/strayhorn/) en Febrero del 2005** se anuncia con el nombre de "Strayhorn" (se usan nombres de músicos como [Billy Strayhorn](https://es.wikipedia.org/wiki/Billy_Strayhorn)) incorpora importantes novedades como un **sistema completamente nuevo de _themes_ más flexible**, por defecto se usa ahora un nuevo _theme_ llamado [Kubrick](https://wordpress.org/themes/default/), los _smartphones_ aún no se han popularizado y el _theme_ no es _responsive_. Elementos como el la cabecera, el pie o las barras laterales se almacenan en ficheros propios lo que facilita realizar cambios. Se introducen los [_permalinks_](https://wordpress.org/support/article/using-permalinks/) que hace que las URLs luzcan mejor y más legibles (http://example.com/?p=N -> http://example.com/2012/post-name/), hay que modificar Apache (mod_rewrite) para poder usarlos. 

![](img/03.png)

Una nueva **[versión 2.0](http://wordpress.org/news/2005/12/wp2/) también en 2005** introduce un nuevo panel de control de administración (_admin dashboard_), también incorpora código Javascript para evitar recargar la página constantemente para los cambios más simples. También viene preinstalada uno de los plugins más usados sin duda, [**Akismet**](https://akismet.com/) funciona como filtro _antispam_.

Y así transcurren los años y evolucionan las versiones con mejoras y nuevas funcionalidades, la **[versión (Coltrane) 2.7](https://wordpress.org/news/2008/12/coltrane/) en 2008 rediseña el interfaz de administración** que se asemeja al que hemos conocido los últimos años.

![](img/04.png)

**La [versión 3.0](https://wordpress.org/news/2010/06/thelonious/) "Thelonious" en 2010** trae grandes cambios como los **tipos de _posts_, las [taxonomías](https://wordpress.org/support/article/taxonomies/), los fondos y cabeceras personalizados y los menús de navegación** entre otras.

Por último el 2018 se produce un importante cambio con la [versión 5.0](https://wordpress.org/news/2018/12/bebo/) "[Bebo Valdés](https://es.wikipedia.org/wiki/Bebo_Vald%C3%A9s)", el nuevo editor visual de bloques denominado [Gutenberg](https://es.wordpress.org/plugins/gutenberg/).

![](img/10.jpg)

## Algunas características

- Basado en PHP, MySQL. 
- Licencia GPL.
- [API REST](https://developer.wordpress.org/rest-api/) (“REpresentational State Transfer”) con JSON (JavaScript Object Notation).
- Los elementos principales que se publican son los **_posts_** o entradas ordenadas por fecha y categorizadas y las **páginas estáticas**.
- Interacción con tus lectores usando comentarios.
- Subscripción a noticias mediante correo electrónico.
- **Sistema de plantillas visuales** independiente del contenido con opciones de personalización.
- Los [**Widgets**](https://wordpress.org/support/article/wordpress-widgets/) son bloques con funciones específicas.
- Multiblog.
- Múltiples autores o usuarios, junto con sus roles o perfiles que establecen distintos niveles de permisos.
- Editor [WYSIWYG](https://es.wikipedia.org/wiki/WYSIWYG).
- _permalinks_ (enlaces permanentes y fáciles de recordar).
- **Distribución RSS** (Really Simple Syndication), ATOM, etc. Yo uso [Feedly](http://feedly.com) como lector de _feeds_.
- Subida y gestión de datos adjuntos y archivos multimedia.
- Sistema de _plugins_ o complementos.
- Preparado para [SEO](https://es.wikipedia.org/wiki/Posicionamiento_en_buscadores). 
- [Página de estadísticas](https://wordpress.com/es/support/estadisticas/).

## Estadísticas y datos de uso de WordPress

Según W3Techs (World Wide Web Technology Surveys) [el 38% de las Webs todo Internet funcionan con WordPress](https://w3techs.com/technologies/details/cm-wordpress), en el mercado de CMS su posición es más dominante aún, no tiene nada que temer a herramientas como Joomla o Drupal.

![](img/05.png)

En BuiltWith se muestra una [distribución de tecnologías CMS](https://trends.builtwith.com/cms) en 1 millón de sitios:

![](img/06.png)

Existen al menos [27,021,750 sitios Web funcionando con WP](https://trends.builtwith.com/cms/WordPress) en el mundo:

![](img/07.png)

Algunos sitios populares que usan WP: El [blog de Zoom](https://blog.zoom.us/es/), el [blog oficial de Star Wars](https://www.starwars.com/news), el [centro de noticias de Microsoft](https://news.microsoft.com/),  [The Walt Disney Company](https://thewaltdisneycompany.com/), [The Mozilla Blog](https://blog.mozilla.org/), etc.

## Enlaces externos

- [WordPress Versions](https://codex.wordpress.org/WordPress_Versions).
- [The Blogging Software Dilemma](https://ma.tt/2003/01/the-blogging-software-dilemma/) By Matt, January 24, 2003.
- Kinsta [WordPress market share](https://kinsta.com/wordpress-market-share/).
- [40+ Most Notable Big Name Brands that are Using WordPress](https://www.wpbeginner.com/showcase/40-most-notable-big-name-brands-that-are-using-wordpress/).
- BuiltWith [CMS Usage Distribution in the Top 1 Million Sites](https://trends.builtwith.com/cms).
- w3techs [Usage statistics and market share of WordPress](https://w3techs.com/technologies/details/cm-wordpress).
- [Matt Mullenweg: State of the Word 2019](https://www.youtube.com/watch?v=LezbkeV059Q&feature=youtu.be).
- creativeminds [The Ultimate Guide to WordPress Statistics (2020)](https://www.cminds.com/ultimate-guide-wordpress-statistics%E2%80%A8%E2%80%A8/).
- es.wordpress.org plugins [Gutenberg](https://es.wordpress.org/plugins/gutenberg/).
- [From Kubrick to Twenty Sixteen: A History of WordPress Default Themes](https://www.elegantthemes.com/blog/editorial/from-kubrick-to-twenty-sixteen-a-history-of-wordpress-default-themes).
- wordpress.org [Announcing WordPress 1.5](https://wordpress.org/news/2005/02/strayhorn/) February 17, 2005 by Matt Mullenweg. 
- wpbeginner [The History of WordPress from 2003 – 2019 (with Screenshots)](https://www.wpbeginner.com/news/the-history-of-wordpress/).
- wpbeginner [Evolution of WordPress User Interface (2003 – 2019)](https://www.wpbeginner.com/showcase/evolution-of-wordpress-user-interface-2003-2009/).
- The New York Times [Invasion of the 'Blog': A Parallel Web of Personal Journals](https://www.nytimes.com/2000/12/28/technology/invasion-of-the-blog-a-parallel-web-of-personal-journals.html) By David F. Gallagher (2000).
- [El Enemigo Conoce el Sistema](https://www.casadellibro.com/libro-el-enemigo-conoce-el-sistema/9788417636395/9501752) de Marta Peirano. Capítulo 5 - La promesa de la blogosfera: vivir para contarlo juntos.
- Julen Orbegozo: [“Redes sociales, internet (y política)”](https://kulturetxea.github.io/charla-soberania-tecnologica/presentacion_julen_orbegozo.html).


