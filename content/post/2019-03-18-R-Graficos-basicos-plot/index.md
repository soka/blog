---
title: R - Gráficos básicos
subtitle: Función plot()
date: 2019-03-18
draft: false
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "Programación",
    "R - Básico",
    "R - Gráficos",
    "R - Diagramas",
    "Diagramas",
  ]
---

# Gráfico básico

Empezamos con una llamada básica a `plot()` pasando sólo los datos que debe representar:

```R
x <- 1:20
y <- x**3
plot(x,y)
```

El resultado es este:

![](images/plot_00.PNG)

# Unir los puntos

Ahora voy a crear un sencillo diagrama de puntos unidos por líneas:

```R
ventas <- c(1,30,4,6,11,20)
plot(ventas,type = 'o')
```

![](images/plot_01.PNG)

Podemos cambiar el color por ejemplo:

```R
plot(ventas,type = 'o',col = "blue")
```

Podemos añadir un título principal:

```R
title(main="ventas")
```

# Añadir nuevos datos como una línea nueva

Ahora vamos a añadir una nueva línea al diagrama previo con ayuda de la función `lines()`:

```R
visitas <- c(3,4,5,7,10,12)
lines(visitas,type = 'o',col='red')
```

Se puede jugar con el grosor de línea con el argumento `lwd`.

![](images/plot_02.PNG)

# Añadir nuevos puntos

Añadimos un único punto con un símbolo personalizado ('@'), también se puede usar un entero para definir un símbolo entre los disponibles.

```R
points(5,5,pch='@')
```

# Añadiendo texto

Se pueden nombrar los ejes con los argumentos `xlab=` e `ylab=`, el título como ya hemos visto se define con el parámetro `title()`. También podemos añadir texto en cualquier lugar del gráfico con la función `text()`:

```R
plot(ventas,type = 'o',col = "blue",xlab="EjeX",ylab="EjeY")
title("Título\nen dos líneas")
text(4,15,"Texto en cualquier lugar")
```

Resultado:

![](images/plot_03.PNG)

# Múltiples gráficos

Podemos visualizar varios gráficos en una sola ventana con la función `par()`.

```R
par(mfrow=c(2,2))
plot(x,y)
plot(x,y)
plot(x,2*y)
plot(x,log(y))
```

![](images/plot_04.PNG)

# Parámetros gráficos

Todas las opciones para personalizar nuestro gráfico se pueden consultar en la ayuda con `?par`. Ahora voy a crear un nuevo gráfico combinando muchas de esas opciones.

Ejemplo:

```R
x <- 1:20
y <- x**3
plot(x,y,
     xlab="Título eje x",
     ylab="Título eje y",
     main="Título\nprincipal",
     xlim=c(0,25), # Límites del eje X
     ylim=c(0,20**3),
     col="red", # Color de los puntos
     pch=22, # FOrmato de los puntos
     bg="yellow", # COlor relleno de los puntos
     tcl=0.4, #Tamaño trazo ejes
     las=1, #Orientación del texto en y
     cex=1.5, #Tamaño del objeto del punto
     bty="l" # Altera los bordes
     )
```

Resultado:

![](images/plot_05.PNG)

# Ejercicios

- r / intro / 27-plot / [plot.R](https://gitlab.com/soka/r/blob/master/intro/27-plot/plot.R).

# Enlaces

- ["Diagramas de dispersión y modelo de regresión lineal en R"](https://soka.gitlab.io/blog/post/2019-02-05-diagramas-dispersion-en-r/).
- ["Diagramas tipo tarta en R"](https://soka.gitlab.io/blog/post/2019-02-14-diagramas-de-tarta-en-r/): Gráficos para variables cualitativas con pie().
- ["Recetas para hacer diagramas en R como lo hace la BBC"](https://soka.gitlab.io/blog/post/2019-02-19-graficos-r-estilo-bbc/).
- ["Diagramas de barras en R"](https://soka.gitlab.io/blog/post/2019-02-21-diagramas-barras-en-r/): Usando la función barplot().

- ["Combining Plots"](https://www.statmethods.net/advgraphs/layout.html).
