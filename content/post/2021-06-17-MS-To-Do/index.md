---
title: Gestión de tareas con MS To Do
subtitle: 
date: 2021-06-17
draft: false
description: 
tags: ["Aplicaciones","Herramientas","MS To Do","Gestión","Tareas"]
---

<!-- vscode-markdown-toc -->
* 1. [Microsoft To Do](#MicrosoftToDo)
* 2. [Características principales](#Caractersticasprincipales)
* 3. [Acceso](#Acceso)
* 4. [Nuestra primera tarea](#Nuestraprimeratarea)
* 5. [Completar una tarea](#Completarunatarea)
* 6. [ Vencimiento, avisos y repeticiones](#Vencimientoavisosyrepeticiones)
* 7. [Agrupa en categorías](#Agrupaencategoras)
* 8. [Adjunta un archivo a una tarea](#Adjuntaunarchivoaunatarea)
* 9. [Planifica tu día con antelación](#Planificatudaconantelacin)
* 10. [Listas](#Listas)
	* 10.1. [Compartir una lista](#Compartirunalista)
* 11. [Integración con Outlook](#IntegracinconOutlook)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='MicrosoftToDo'></a>Microsoft To Do

[**MS To Do**](https://todo.microsoft.com/tasks/) (creado en 2017) es una herramienta Web de gestión de tareas, permite a los usuarios gestionar sus tareas desde un _smartphone_ o tableta ([Android](https://play.google.com/store/apps/details?id=com.microsoft.todos&hl=es&gl=US), [iOS](https://go.microsoft.com/fwlink/?linkid=2135821)), o usando la computadora y un navegador Web (existe versión de escritorio para [Windows](https://go.microsoft.com/fwlink/?linkid=2135654)). Desde el 2018 **permite compartir las tareas con otros usuarios**.

![](img/01.PNG)

##  2. <a name='Caractersticasprincipales'></a>Características principales

* Planificador diario inteligente "Mi Día".
* Administrar la lista de tareas pendientes online. Estes donde estes, app para _smartphone_, iPhone, Android, Windows 10 y en la Web.
* Uso compartido simplificado: Las listas de tareas pendientes compartidas te ayudan a mantenerte conectado con tus amigos, familiares y colegas.
* Facilitar la administración de tareas: Desglosa las tareas en pasos simples, agrega fechas de vencimiento y define avisos para llevar un seguimiento de tu lista de tareas diarias.
* Integración de Tareas de Outlook.
* Fácil de usar e intuitivo.
* Vale para centrarse desde el trabajo hasta el ocio y cosas personales (la misma lista de compra por ejemplo).

##  3. <a name='Acceso'></a>Acceso

Usando el siguiente enlace [https://to-do.office.com/tasks/](https://to-do.office.com/tasks/) o desde [https://www.office.com/](https://www.office.com/) donde están todas las aplicaciones.

![](img/02.PNG)

##  4. <a name='Nuestraprimeratarea'></a>Nuestra primera tarea

A primera vista la aplicación Web no imresiona especialmente, su interfaz es minimalista: 

![](img/03.PNG)

Usamos el icono "+" para añadir una nueva tarea, definimos un título y pulsamos "Enter" o usamos el botón lateral para añadir la tarea.

![](img/04.gif)

Una vez introducida la tarea **podemos marcarla como importante** para que nos aparezca entre las tareas importantes agrupadas en el menú lateral. Si hacemos clic sobre una tarea podemos visualizarla en detalle.

![](img/05.gif)

Haciendo click sobre la tarea de muestra el detalle de la misma en un barra lateral vertical en la parte derecha de la pantalla:

![](img/06.gif)

* Una tarea puede estar compuesta por una serie de **pasos** que podemos definir e ir completando
* Si es para hoy podemos añadirla a la **vista del día**.
* Podemos definir un recordatorio que nos avisará desde el navegador (hay que darle permisos al navegador)
* Fecha vencimiento.
* La tarea puede ser repetitiva (por ejemplo una reunión semanal)
* Podemos definir una o más categorías (las categorías las obtiene de Outlook)
* Adjuntar archivos.
* Añadir una descripción.

Los botones inferiores permiten borrar la tarea o esconder el detalle.

##  5. <a name='Completarunatarea'></a>Completar una tarea 

Marcar una tarea como completada es muy sencillo, sólo debemos hacer click sobre el círculo y se mostrara como tachada en una lista desplegable inferior:

![](img/07.gif)

##  6. <a name='Vencimientoavisosyrepeticiones'></a> Vencimiento, avisos y repeticiones

Una vez hayas configurado tus tareas, llega la hora de cumplirla. Para ello, tienes varias herramientas. Por ejemplo, es importante agregarle una **fecha de vencimiento** para decir cuándo quieres haber completado esta tarea.

Además, también puedes añadir avisos, que hacen las veces de recordatorios. Así, To Do te avisará de que tienes ahí esa tarea que debes cumplir, y aunque no haya llegado la fecha límite de vencimiento (que puedes cambiar si quieres), podrás asegurarte de que no se te pase. Además, puedes hacer que tus tareas sean recurrentes con la sección Repetir, con la que puedes decidir cuándo quieres volver a hacerla.

Cualquier tarea con una fecha límite definida se verá en el menú lateral en el apartado "Planeado".

![](img/08.gif)

##  7. <a name='Agrupaencategoras'></a>Agrupa en categorías

**To Do comparte el mismo sistema de categorías de Outlook**, pero no las puedes editar de forma local, y te toca ir al cliente de correo electrónico para editarlas allí o directamente para crear otras nuevas.

##  8. <a name='Adjuntaunarchivoaunatarea'></a>Adjunta un archivo a una tarea

Una función bastante útil para algunas tareas es la de agregarle archivos desde tu PC o móvil. De esta manera, si para completar una tarea necesitas determinados archivos o documentación, puedes irlos añadiendo antes de ponerte en serio a completarla. Y si es una tarea colaborativa, también puedes ayudar a quien la vaya a hacer o te pueden ayudar a ti adjuntando archivos relacionados.

![](img/10.gif)

##  9. <a name='Planificatudaconantelacin'></a>Planifica tu día con antelación

To Do tiene una pestaña llamada **Mi Día**, en la que puedes añadir manualmente las cosas que quieres cada día para tenerlas siempre a mano. La idea de esta lista es que al iniciar tu día te planifiques, para lo cuál te va a sugerir qué tareas elegir dependiendo de parámetros como las fechas de vencimiento. Simpre podemos quitar una tarea de la pestaña 

##  10. <a name='Listas'></a>Listas

To Do te permite crear diferentes listas, en las cuales vas a poder ir añadiendo diferentes tareas. Por lo tanto, **vas a poder organizarte creando diferentes listas temáticas** con todo lo que quieras tener controlado en esta aplicación. En las listas vas a poder crear todas las tareas que quieras, y cada una con los diferentes pasos que necesites. Da igual si son listas de compras, de enlaces a tener en cuenta o simplemente de ideas que quieres ir sacando o anotando.

###  10.1. <a name='Compartirunalista'></a>Compartir una lista 

**Puedes crear listas colaborativas** invitando a otras personas para que se unan. Lo harás creando un enlace que luego puedes compartir con el resto de personas.

![](img/11.gif)

Para crear el enlace, entra en la tarea y pulsa en la opción de Compartir. Esto te permitirá gestionar el enlace que luego debes compartir con otras personas. En cualquier momento vas a poder activar una opción llamada Limitar el acceso a los miembros actuales, que desactiva el enlace que hayas creado para que nadie mas pueda unirse a las tareas, y así mantener el control sobre quién puede unirse.

Cuando alguien se une a una lista el número de miembros aumenta, si volvemos a entrar en la opción de compartir **podemos dejar de compartir una lista** con el usuario o usuarios que queramos.

Una vez compartida una lista se pueden asignar las tareas que contiene a otros usuarios (**nota**: una tarea sólo puede asignarse a un usuario), una vez asignada se puede ver el icono del usuario en la vista.

![](img/12.PNG)

##  11. <a name='IntegracinconOutlook'></a>Integración con Outlook

Podemos [marcar un correo](https://support.microsoft.com/en-us/office/using-microsoft-to-do-with-flagged-email-from-outlook-f90c37b0-4453-4756-a6d5-e2ef8d33b395?ui=en-US&rs=en-US&ad=US) con una fecha y automáticamente aparecerá en MS To Do. Se puede abrir el correo original desde MS To Do.

Los tareas que provienen de Outlook tienen un espacio especial en los apartados del menú lateral de la izquierda.

![](img/09.PNG)

**NOTA**: Sólo se muestran los marcados los últimos 30 días y 100 como máximo.