---
title: Soluciones Web para encuestas y citas [borrador] 
subtitle: Herramientas de trabajo colaborativo remoto
date: 2020-05-02
draft: false
description: 
tags: ["Aplicaciones","Web","Citas","Poll","Encuestas","Votaciones"]
---

## Croodle

**Enlace**: [Croodle](https://www.systemli.org/poll/).

[**Croodle**](https://www.systemli.org/poll/) facilita la **toma de decisiones** o la **selección de una fecha**, para crear una nueva encuesta seguimos este [enlace](https://www.systemli.org/poll/#/create), lo primero como decía es seleccionar el tipo de encuesta:

1. Buscar una fecha.
2. La respuesta a una pregunta determinada.

![](img/01.png)

A continuación añado un título y una descripción. En la siguiente pantalla dependiendo del tipo de encuesta se mostrará una cosa u otra, si es una fecha seleccionamos los días posibles en un calendario.

![](img/02.png)

Luego debemos escoger la hora de la cita para cada día, se puede definir más de una hora para cada día. El último apartado permite definir los ajustes finales, **se puede definir el tipo de respuesta Sí, No, Quizás o incluso un texto libre**, se puede escoger una fecha límite a partir de la cual la encuesta se destruye entre varias opciones disponibles. Si queremos podemos permitir participantes anónimos, también se puede obligar a responder a todas las opciones.

Una vez creada la aplicación ofrece una URL que debemos compartir con el resto de participantes. Para visualizar los resultados creo que debería guardar la URL donde la estas creando.

Cuando se trata de una encuesta respecto a una pregunta determinada, se escoge la pregunta y las posibles respuestas, para cada respuesta se puede seleccionar las mismas opciones de antes: Sí, No, Quizás o incluso un texto libre.

## Framadate

**Enlace**: [Framadate](https://poll.disroot.org/).

![](img/03.png)

[**Framadate**](https://poll.disroot.org/) es muy parecida a la anterior, no requiere registro previo, las ***opciones de configuración** permiten algunas opciones adicionales, por ejemplo límitar el número máximo de votantes, personalizar la URL, proteger la votación con una contraseña o que los votantes puedan modificar las votaciones. El resto es lo mismo, consiste en seleccionar varias fechas y horas para una cita  

![](img/04.jpg)

Cuando se crea el evento la aplicación nos proporciona dos URL, una para los usuarios y otra para acceder de nuevo a la interfaz de administración. Si quieres instalarlo en tu servidor el código fuente está programado PHP y está alojado en un [servidor GIT en este enlace](https://framagit.org/framasoft/framadate/framadate).

## Framagenda

**Enlace**: [Framagenda](https://framagenda.org/login).

![](img/05.png)

[**Framagenda**](https://framagenda.org/login) tiene otro propósito, es un servicio en línea de gestión de calendarios de forma colaborativa. La herramienta **requiere registro previo** y cuenta con un entorno con acceso a utilidades adicionales.

![](img/06.png)

Cuando se establece un nuevo evento se pueden definir un gran número de detalles a parte de la fecha inicial y final, se puede definir como se comparte con otros usuarios, definir una serie de categorías para el evento o invitar a otros usuarios que no tienen que estar registrados necesariamente (si aceptan la cita se sincroniza con los calendarios personales de los usuarios).

Un usuario puede definir más de un calendario para diferentes propósitos. 