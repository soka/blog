---
title: Organiza un Webinar profesional en línea
subtitle: Transmisión en vivo con Zoom y OBS Studio a Youtube Studio
date: 2020-12-11
draft: false
description: Controla la fuente de la Webcam desde OBS Studio y envíala a una aplicación de videoconferencia
tags: ["OBS Studio","OBS","Videoconferencías","Streaming","Vídeo","Aplicaciones","Webinar"]
---

<!-- vscode-markdown-toc -->
* 1. [OBS Studio](#OBSStudio)
* 2. [Fuentes](#Fuentes)
	* 2.1. [Captura de la ventana de participantes](#Capturadelaventanadeparticipantes)
	* 2.2. [Añadir un fondo](#Aadirunfondo)
* 3. [Jugando con las escenas en OBS](#JugandoconlasescenasenOBS)
	* 3.1. [Compartir una presentación](#Compartirunapresentacin)
	* 3.2. [Escena con el hablante activo](#Escenaconelhablanteactivo)
	* 3.3. [¿Una escena de transición?](#Unaescenadetransicin)
* 4. [Vista múltiple](#Vistamltiple)
* 5. [El audio](#Elaudio)
* 6. [Emitir en vivo](#Emitirenvivo)
* 7. [Conclusiones](#Conclusiones)
* 8. [Enlaces internos](#Enlacesinternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Un Webinar es una conferencía en línea en vivo para impartir una charla o una formación por ejemplo, normálmente pueden asistir varios ponentes a un lado y un público numeroso que atiende o recibe la información al otro, es habitual recurrir a presentaciones compartidas en pantalla con los asistentes, la audiencia interactuá usando un chat u otro sistema. Normalmente las personas que imparten el Webinar no estarán físicamente en la misma sala sino en diferentes ubicaciones.

De forma resumida haré lo siguiente paso a paso, capturar una videoconferencia organizada en Zoom usando [OBS Studio](https://es.wikipedia.org/wiki/Open_Broadcaster_Software) en una computadora de control, aplicare varios efectos y modificaciones (texto, logos y fondos personalizados) y desde OBS la emitiré a un servicio de transmisión en vivo como [Youtube Studio](https://studio.youtube.com/).

##  1. <a name='OBSStudio'></a>OBS Studio

**Funcionalidades** destacas:

- Captura y mezcla de diversas fuentes de imagen y sonido: Compartir pantalla, una imagen estática, una ventana determinada, etc.
- Escenas ilimitadas.
- Transiciones.
- Filtros (vídeo y audio), eliminar fondos, croma, etc.
- Grabación.
- Transmisión en vivo.
- Sistema de [_plugins_](https://obsproject.com/forum/resources/categories/obs-studio-plugins.6/).

##  2. <a name='Fuentes'></a>Fuentes

###  2.1. <a name='Capturadelaventanadeparticipantes'></a>Captura de la ventana de participantes

Ya tengo abierta una reunión de Zoom con 2 participantes. Voy a capturar la ventana de Zoom de uno de los participantes, agrego una nueva fuente que sea de tipo ventana y le pongo el nombre del ponente.

![](img/02.jpg)

Realizo algunos ajustes de las propiedades, oculto el cursor del ratón, sólo capturo el área cliente (sin la barra superior o los controles de la ventana de Zoom), he tenido que cambiar el método de captura como en la imagen inferior (Windows Graphics Capture) para que se viese la imagen de Zoom.

![](img/03.jpg)

Ahora voy a recortar la captura y quedarme sólo con la ventana de vídeo del participante. Con el ratón sobre el área de trabajo se puede redimensionar y mover la captura, **combinando el ratón con la tecla ALT se puede recortar el área que necesito**.

![](img/04.jpg)

Realizo la misma operación para el resto de participantes, al final tendré una captura por cada persona. Finalmente alineo las ventanas o las dispongo como quiera.

###  2.2. <a name='Aadirunfondo'></a>Añadir un fondo

Para darle un efecto más profesional he creado un fondo de pantalla completa con un logo al azar sobre el que superpondrán las ventanas de vídeo de los participantes, añado una nueva fuente de tipo imagen, muevo las fuentes para que el fondo quede abajo (al final de la lista de fuentes). Aplico una transformación sobre la imagen para que ocupe toda la ventana disponible (con el botón derecho del ratón sobre la fuente en el área de trabajo).

También podemos añadir un marco creando una imagen previamente con un reborde y el contenido transparente, subimos la capa para que quede por encima, a medida que añadamos fuentes **es buena idea bloquearlas cuando obtengamos el efecto deseado**.

![](img/05.jpg)

##  3. <a name='JugandoconlasescenasenOBS'></a>Jugando con las escenas en OBS

###  3.1. <a name='Compartirunapresentacin'></a>Compartir una presentación

**Aunque los participantes en Zoom minimicen o redimensionen la ventana de Zoom esto no repercute en OBS y seguiremos viendo a los interlocutores**, pero si alguno de los participantes comparte la pantalla en su cliente Zoom o cambiamos la vista de Zoom en la computadora de control **puede ocasionar problemas**, en ese caso toda la composición se va al traste. Así como se vería si uno de los participantes comparte pantalla con un vídeo en pantalla completa.

![](img/06.jpg)

Una posible **solución es crear una nueva escena en OBS donde capturar la presentación como fuente y desplazarnos entre la escena de los participantes y la presentación usando un atajo de teclado**. Añado una escena nueva y capturo la pantalla compartida de Zoom del ponente que va a compartir su presentación. Recorto el área que me interesa y dimensiono la captura para que ocupe toda la pantalla.

![](img/08.jpg)

Ahora para cambiar de escena de forma rápida puedo **asignar una tecla a cada escena** en "Archivo > Configuración > Atajos". También puedo asignar teclas para mostrar o esconder fuentes dentro de una escena, por ejemplo puedo superponer el vídeo del presentador sobre la presentación y mostrarla y ocultarla.

![](img/09.jpg)

###  3.2. <a name='Escenaconelhablanteactivo'></a>Escena con el hablante activo

Para cuando queramos destacar en un plano principal una persona que este interviniendo podemos componer otra escena y habilitar la vista del hablante en Zoom (el atajo ALT esconde o muestra los controles de Zoom para hacerlos desaparecer).

###  3.3. <a name='Unaescenadetransicin'></a>¿Una escena de transición?

Para evitar el efecto visual mencionado de que salga la presentación partida en la escena de los participantes mientras el presentador comparte la pantalla y hacemos la transición entre escenas se puede añadir una escena adicional con una imagen para los momentos de transición mientras se prepara el presentador, si va a compartir una presentación podemos usar la portada o la primera diapositiva exportada como imagen.

![](img/10.jpg)

##  4. <a name='Vistamltiple'></a>Vista múltiple

Cuando estamos emitiendo trabajando con varias escenas esta utilidad de OBS es genial. Desde el menú "Vista > Vista Multiple" podemos visualizar todas las escenas y con un simple clic de ratón ir cambiando entre ellas, prueba con los efectos de transición de escenas. 

![](img/13.png)

## Añade texto en OBS

OBS también permite añadir una fuente de texto que nos permitirá añadir textos personalizados en cualquier parte de la escena.

![](img/14.png)

##  5. <a name='Elaudio'></a>El audio

Para capturar el audio voy a instalar una aplicación adicional llamada [VB-CABLE](https://vb-audio.com/Cable/) que permite crear un dispositivo virtual, enrutare la salida de sonido del altavoz a una entrada virtual que voy a capturar en OBS.

![](img/07.jpg)

##  6. <a name='Emitirenvivo'></a>Emitir en vivo

Emitir en vivo en un servicio como Youtube Studio es la parte más fácil, en [esta entrada](https://soka.gitlab.io/blog/post/2020-04-30-obs-1/) de mi blog muestro como hacerlo.

![](img/12.jpg)

Recuerda probar el _video birate_ en los ajustes de OBS, en esta tabla puedes consultar los ratios recomendados para la calidad que busques ([Fuente](https://filmora.wondershare.com/video-editing-tips/what-is-video-bitrate.html?gclid=EAIaIQobChMIxpae0fPK7QIVDflRCh380gRiEAAYASAAEgKeXPD_BwE)), un mayor _birate_ representa mayor bits por segundo de tranmisión que se traduce en mayor calidad. Recuerda también revisar los [ajustes en Youtube Studio](https://support.google.com/youtube/answer/2853702?hl=es).

![](img/11.png)

##  7. <a name='Conclusiones'></a>Conclusiones

OBS Studio es sumamente versátil, ofrece muchas opciones para editar diferentes fuentes y mezclarlas en diferentes escenas, con un poco de maña con los controles prácticamente podemos hacer lo que queramos. **Si vamos a mezclar muchas escenas y fuentes recomiendo poner un nombre representativo a cada una, puede ser de utilidad preceder el nombre por un número asociado a cada atajo de teclado** (se pueden asociar teclas a escenas y también a fuentes pero no deben coincidir).

Recomiendo **usar siempre un monitor adicional en la computadora de control**, usando el escritorio extendido en una pantalla se puede visualizar OBS Studio para tener el control de la transmisión en todo momento, en la pantalla principal podemos otras aplicaciones o soportes multimedia que queramos compartir o el propio Zoom por ejemplo (se puede capturar un monitor como fuente).

Si vas a retransmitir un Webinar con muchas escenas y intervenciones Prepara un _timing_ o un guion con las escenas y tenlo a mano para saber como se desarrollan los contenidos.

Si buscas funcionalidades adicionales como transiciones avanzadas, efectos, etc. no dejes de visitar la [**sección de _plugins_ de OBS**](https://obsproject.com/forum/resources/categories/obs-studio-plugins.6/).

- [StreamFX](https://obsproject.com/forum/resources/streamfx-for-obs-studio.578/): Filtros, efectos y transiciones avanzadas.
- [Advanced Scene Switcher](https://obsproject.com/forum/resources/advanced-scene-switcher.395/).
- [OBS-VirtualCam](https://obsproject.com/forum/resources/obs-virtualcam.539/): Permite capturar la Webcam en OBS, montarla como queramos y capturar la salida como una Webcam virtual en Zoom, Meet o cualquier aplicación de videoconferencias. Este plugin puede ser muy útil cuando todas las personas que intervienen están presentes en la misma sala y podemos usar la Webcam de la computadora, para captar diferentes tomas se pueden usar varias Webcams USB y añadir un fondo con logo, etc.

##  8. <a name='Enlacesinternos'></a>Enlaces internos

- [OBS Studio - Software libre de grabación y transmisión de vídeo en vivo](https://soka.gitlab.io/blog/post/2020-04-30-obs-1/).
- [Usar OBS como una cámara virtual (Windows)](https://soka.gitlab.io/blog/post/2020-12-02-obs-virtualcam-plugin/).
