---
title: Introduction to Free Software and the Liberation of Cyberspace
subtitle: Richard Stallman's TEDx video
date: 2022-05-24
draft: true
description: RMS was invited to give a TEDx talk.
tags: ["FSF","Free","GPL","GNU","Richard Stallman","TEDx","Charlas","Talks"]
---

<!-- vscode-markdown-toc -->
* 1. [Richard Stallman: Free software, free society](#RichardStallman:Freesoftwarefreesociety)
	* 1.1. [¿Quién controla tu computadora?](#Quincontrolatucomputadora)
	* 1.2. [¿Tu o una gran corporación?](#Tuounagrancorporacin)
	* 1.3. [¿Qué es una computadora?](#Quesunacomputadora)
* 2. [Escribiendo el programa adecuado se puede conseguir cualquier cosa](#Escribiendoelprogramaadecuadosepuedeconseguircualquiercosa)
	* 2.1. [¿Quien da instrucciones a tu computadora?](#Quiendainstruccionesatucomputadora)
	* 2.2. [Obedece a alquien antes que a ti](#Obedeceaalquienantesqueati)
	* 2.3. [Lo que la compañia permita](#Loquelacompaiapermita)
	* 2.4. [Con el software hay dos posibilidades](#Conelsoftwarehaydosposibilidades)
	* 2.5. [El programa controla a los usuarios](#Elprogramacontrolaalosusuarios)
	* 2.6. [Libertades 0,1,2 y 3](#Libertades012y3)
	* 2.7. [Libertades 0,1,2 y 3](#Libertades012y3-1)
	* 2.8. [Libertad 0](#Libertad0)
	* 2.9. [Libertad 0](#Libertad0-1)
	* 2.10. [Libertad 1](#Libertad1)
	* 2.11. [Libertad 1](#Libertad1-1)
	* 2.12. [Pero, ¿Qué es el código fuente?](#PeroQueselcdigofuente)
	* 2.13. [Pero, ¿Qué es el código fuente?](#PeroQueselcdigofuente-1)
	* 2.14. [Control individual](#Controlindividual)
	* 2.15. [¿Qué ocurre si no eres un programador?](#Quocurresinoeresunprogramador)
	* 2.16. [¿Qué ocurre si no eres un programador?](#Quocurresinoeresunprogramador-1)
	* 2.17. [La libertad de distribuir copias exactas](#Lalibertaddedistribuircopiasexactas)
	* 2.18. [Distribuir las copias con modificaciones.](#Distribuirlascopiasconmodificaciones.)
	* 2.19. [Los usuarios controlan el programa](#Losusuarioscontrolanelprograma)
	* 2.20. [El programa controla a los usuarios](#Elprogramacontrolaalosusuarios-1)
	* 2.21. [¿Cuando usamos software propietario que sucede?](#Cuandousamossoftwarepropietarioquesucede)
	* 2.22. [A veces rastrea al usuario](#Avecesrastreaalusuario)
	* 2.23. [A veces restringe lo que quiere hacer el usuario](#Avecesrestringeloquequierehacerelusuario)
	* 2.24. [Abusan con fines de lucro](#Abusanconfinesdelucro)
	* 2.25. [Abusan con fines de lucro](#Abusanconfinesdelucro-1)
	* 2.26. [Software para tontos](#Softwareparatontos)
	* 2.27. [Software para tontos](#Softwareparatontos-1)
	* 2.28. [¿Como huimos de esto y dejar de ser una victima?](#Comohuimosdeestoydejardeserunavictima)
	* 2.29. [¿Como huimos de esto y dejar de ser una victima?](#Comohuimosdeestoydejardeserunavictima-1)
	* 2.30. [Distribuciones](#Distribuciones)
	* 2.31. [Distribuciones](#Distribuciones-1)
	* 2.32. [Mantener tu libertad requiere un sacrificio](#Mantenertulibertadrequiereunsacrificio)
	* 2.33. [Mantener tu libertad requiere un sacrificio](#Mantenertulibertadrequiereunsacrificio-1)
	* 2.34. [Small sacrifices for freedom](#Smallsacrificesforfreedom)
	* 2.35. [Small sacrifices for freedom](#Smallsacrificesforfreedom-1)
	* 2.36. [Non free applications](#Nonfreeapplications)
	* 2.37. [Non free applications](#Nonfreeapplications-1)
	* 2.38. [Non free Javascript program](#NonfreeJavascriptprogram)
	* 2.39. [Non free Javascript program](#NonfreeJavascriptprogram-1)
	* 2.40. [Servicios como sustitutos de software](#Servicioscomosustitutosdesoftware)
	* 2.41. [Servicios como sustitutos de software](#Servicioscomosustitutosdesoftware-1)
	* 2.42. [¿Prioridad? Servidores web o tu propia máquina](#PrioridadServidoreswebotupropiamquina)
	* 2.43. [¿Prioridad? Servidores web o tu propia máquina](#PrioridadServidoreswebotupropiamquina-1)
	* 2.44. [¿Que obstáculos?](#Queobstculos)
	* 2.45. [¿Que obstáculos?](#Queobstculos-1)
	* 2.46. [Grandes compañias](#Grandescompaias)
	* 2.47. [Grandes compañias](#Grandescompaias-1)
	* 2.48. [Medios de comunicación](#Mediosdecomunicacin)
	* 2.49. [Medios de comunicación](#Mediosdecomunicacin-1)
	* 2.50. [Free software activists - Open source boosters](#Freesoftwareactivists-Opensourceboosters)
	* 2.51. [Free software activists - Open source boosters](#Freesoftwareactivists-Opensourceboosters-1)
	* 2.52. [¡Diga software libre!](#Digasoftwarelibre)
	* 2.53. [¡Diga software libre!](#Digasoftwarelibre-1)
	* 2.54. [Escuelas y software libre](#Escuelasysoftwarelibre)
	* 2.55. [Escuelas y software libre](#Escuelasysoftwarelibre-1)
	* 2.56. [Enseñar software libre en la escuela](#Ensearsoftwarelibreenlaescuela)
	* 2.57. [Enseñar software libre en la escuela](#Ensearsoftwarelibreenlaescuela-1)
	* 2.58. [Escuelas](#Escuelas)
	* 2.59. [Escuelas](#Escuelas-1)
	* 2.60. [El espíritu de la buena voluntad](#Elespritudelabuenavoluntad)
	* 2.61. [El espíritu de la buena voluntad](#Elespritudelabuenavoluntad-1)
	* 2.62. [Manual de usuario](#Manualdeusuario)
	* 2.63. [Manual de usuario](#Manualdeusuario-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='RichardStallman:Freesoftwarefreesociety'></a>Richard Stallman: Free software, free society

RMS was invited to give a TEDx talk at "FREEDOM (@ digital age)" in April and took the opportunity to explain the fundamentals of the free software movement to the general public. In this speech, RMS specifies the issue and makes the stakes clear; he explains what a computer is and who controls it, what the various implications of free software and of proprietary software are for you, the user, what you can do to stop being a victim, what the obstacles to computer-user freedom are, and how you can make a difference.

The TEDx talks are "designed to help communities, organizations, and individuals to spark conversation and connection through local TED-like experiences." At these events, screenings of videos, like this one, "or a combination of live presenters and TED Talks videos — sparks deep conversation and connections at the local level."

We hope you share the recording, to help raise awareness and inspire conversations and connections in your own circles, and introduce a friend, loved one, or acquaintance, to this "first step in the liberation of cyberspace."

Fuente: [https://www.fsf.org/blogs/rms/20140407-geneva-tedx-talk-free-software-free-society](https://www.fsf.org/blogs/rms/20140407-geneva-tedx-talk-free-software-free-society)


Otras charlas y entrevistas:

* [https://youtu.be/8SdPLG-_wtA](https://youtu.be/8SdPLG-_wtA)
* [https://youtu.be/szSU1A2RhuQ](https://youtu.be/szSU1A2RhuQ)
* [https://youtu.be/5t_EcPTEzh4](https://youtu.be/5t_EcPTEzh4)

###  1.1. <a name='Quincontrolatucomputadora'></a>¿Quién controla tu computadora?

> "_El software libre es la primera batalla en la liberación del ciberespacio"_ Richard Stallman.

![](img/rms_tedx_es-01.png)

---

###  1.2. <a name='Tuounagrancorporacin'></a>¿Tu o una gran corporación?

¿Eres tu o una gran compañía quien realmente la controla?

![](img/rms_tedx_es-02.png)

---

Life360, una popular app de seguridad familiar utilizada por 33 millones de personas en todo el mundo, se vende como una forma estupenda para los padres de rastrear los movimientos de sus hijos utilizando sus teléfonos. 

Life360 asegura que anonimizan los datos antes de venderlos.

«los metadatos te cuentan absolutamente todo acerca de la vida de alguien. Si tienes  suficientes  metadatos  no  necesitas  contenido».  En  la  era  del Big Data,  el  contenido  es lo menos valioso. El metadato es el rey.

* [https://www.applesfera.com/airtag/life360-compro-tile-mes-pasado-ahora-sabemos-que-vende-ubicacion-millones-usuarios-cada-dia](https://www.applesfera.com/airtag/life360-compro-tile-mes-pasado-ahora-sabemos-que-vende-ubicacion-millones-usuarios-cada-dia).

---

###  1.3. <a name='Quesunacomputadora'></a>¿Qué es una computadora?

**Una máquina universal**. Debería procesar y hacer cualquier cosa que le pidas porque le diste **un programa que ordena que es lo quieres que ejecute**. La computadora sólo sabe como recibir una instrucción y ejecutarla, recibir otra instrucción y ejecutarla. El programa tiene las instrucciones que dicen que hacer. 

![](img/rms_tedx_es-03.png)

Los _smartphone_, computadoras o la misma Internet (con la nube y los programas que se ejecutan en ella en vez de en local) son una especie de [máquina universal de Turing](https://es.wikipedia.org/wiki/M%C3%A1quina_de_Turing_universal) en muchos sentidos, se han convertido en nuestro medio universal, capaces de desempeñar las tareas realizadas por cualquier otro sistema, lo usamos par ver la hora como reloj o calculadora, ha reemplazado la máquina de escribir, los aparatos de radio y Cine / TV.

![](img/05.png)

Superficiales: ¿Qué está haciendo Internet con nuestras mentes?

---

##  2. <a name='Escribiendoelprogramaadecuadosepuedeconseguircualquiercosa'></a>Escribiendo el programa adecuado se puede conseguir cualquier cosa

Escribiendo el programa adecuado, podéis conseguir cualquier cosa (esos sois vosotros programadores).

![](img/rms_tedx_es-04.png)

---

###  2.1. <a name='Quiendainstruccionesatucomputadora'></a>¿Quien da instrucciones a tu computadora? 

Entonces, la siguiente pregunta lógica es **¿Quien da instrucciones a tu computadora?** Puedes pensar que obedece tus ordenes.

![](img/rms_tedx_es-05.png)

---

###  2.2. <a name='Obedeceaalquienantesqueati'></a>Obedece a alquien antes que a ti

Cuando realmente está obedeciendo a alguien antes que a ti. 

![](img/rms_tedx_es-06.png)

---

###  2.3. <a name='Loquelacompaiapermita'></a>Lo que la compañia permita

Y tu sólo puedes ser escuchado hasta donde la compañía te permita.

![](img/rms_tedx_es-07.png)

---

###  2.4. <a name='Conelsoftwarehaydosposibilidades'></a>Con el software hay dos posibilidades

**Con el software hay dos posibilidades**. 

O los usuarios controlan el programa. 

![](img/rms_tedx_es-08.png)

---

<<<<<<< HEAD
###  2.5. <a name='Elprogramacontrolaalosusuarios'></a>El programa controla a los usuarios
=======
O el programa controla a los usuarios. 
Es inevitable una u otra.
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

![](img/rms_tedx_es-09.png)

---

<<<<<<< HEAD
###  2.6. <a name='Libertades012y3'></a>Libertades 0,1,2 y 3
=======
###  2.7. <a name='Libertades012y3-1'></a>Libertades 0,1,2 y 3
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Para que los usuarios puedan controlar el programa, necesitan cuatro libertades esenciales. Esta es la **definición del software libre**.

![](img/rms_tedx_es-10.png)

---

¿Cual es la definición de software libre? 

El software libre respeta la libertad de los usuarios y la comunidad.

**LIBRE not GRATIS**. Para desambiguar el termino en inglés a menudo se usa la palabra libre en castellano.

No estamos hablando de precio, hablamos de libertad y de respeto.

![](img/rms_tedx_es-11.png)

---

<<<<<<< HEAD
###  2.8. <a name='Libertad0'></a>Libertad 0
=======
###  2.9. <a name='Libertad0-1'></a>Libertad 0
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

La primera libertad es para **ejecutar el programa como tu quieras**. Para cualquier propósito.

![](img/rms_tedx_es-12.png)

---

<<<<<<< HEAD
###  2.10. <a name='Libertad1'></a>Libertad 1
=======
###  2.11. <a name='Libertad1-1'></a>Libertad 1
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

La libertad 1 es para **estudiar el código fuente y modificarlo**, para que así se ejecute la orden que quieras.

![](img/rms_tedx_es-13.png)

---

<<<<<<< HEAD
###  2.12. <a name='PeroQueselcdigofuente'></a>Pero, ¿Qué es el código fuente?
=======
###  2.13. <a name='PeroQueselcdigofuente-1'></a>Pero, ¿Qué es el código fuente?
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Un programa tiene típicamente dos formas, una que permite leerlo y que puedes entender, eso es el código fuente que se puede cambiar si conoces el lenguaje de programación.

Un ejecutable es un binario que la computadora entiende, si solo tienes el ejecutable es un horror cambiarlo e imaginarse lo que hace. 

**Para que exista la verdadera posibilidad de cambiarlo deben darte el código fuente**, es un requisito indispensable.

![](img/rms_tedx_es-14.png)

---

###  2.14. <a name='Controlindividual'></a>Control individual

Con las dos primeras libertades diferentes usuarios pueden empezar a copiarlo, modificarlo y hacer que ejecute la orden que quieren. Eso es **control individual**.

![](img/rms_tedx_es-15.png)

---

<<<<<<< HEAD
###  2.15. <a name='Quocurresinoeresunprogramador'></a>¿Qué ocurre si no eres un programador?
=======
###  2.16. <a name='Quocurresinoeresunprogramador-1'></a>¿Qué ocurre si no eres un programador?

Pero, ¿Qué ocurre si no eres un programador? Cuando miras el código fuente no puedes entenderlo.

**El control individual no es suficiente**, también **es necesario un control colectivo** que significa que un grupo de usuarios es libre de trabajar juntos para adaptar el programa a lo que quieran. 
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Por supuesto, algunos serán programadores, son quienes realmente escriben los cambios, pero lo hacen como parte del grupo, para lo que el grupo quiere. 

Por supuesto, el grupo no tiene porqué ser todo el mundo, otros pueden usarlo de otra forma. Todos son libres para hacer eso.

![](img/rms_tedx_es-16.png)

---

###  2.17. <a name='Lalibertaddedistribuircopiasexactas'></a>La libertad de distribuir copias exactas

Entonces, **el control colectivo requiere dos libertades esenciales más**.

La libertad de distribuir copias exactas. Esto **no está reñido con el negocio o con cobrar estas copias**, la FSF desde sus inicios se mantenía gracias en gran medida a la venta y distribución de copias de software como Emacs o manuales, en menor medida también recibe subvenciones.

![](img/rms_tedx_es-17.png)

---

###  2.18. <a name='Distribuirlascopiasconmodificaciones.'></a>Distribuir las copias con modificaciones.

La libertad nº 3 es parecida pero permite distribuir las copias pero con modificaciones.

![](img/rms_tedx_es-18.png)

---

###  2.19. <a name='Losusuarioscontrolanelprograma'></a>Los usuarios controlan el programa

Si realmente tienes estas libertades, el software es libre. Los usuarios controlan el programa.

![](img/rms_tedx_es-19.png)

---

###  2.20. <a name='Elprogramacontrolaalosusuarios-1'></a>El programa controla a los usuarios

Si falta alguna de las libertades esenciales el programa controla a los usuarios, y el desarrollador controla el programa, lo que significa que **el programa es un instrumento de poder injusto. El programador queda entonces por encima de los usuarios.**

![](img/rms_tedx_es-20.png)

---

![](img/rms_tedx_es-21.png)

---

###  2.21. <a name='Cuandousamossoftwarepropietarioquesucede'></a>¿Cuando usamos software propietario que sucede?

A menudo el programa vigila al usuario. 

The guardian ['They know us better than we know ourselves': how Amazon tracked my last two years of reading](https://www.theguardian.com/technology/2020/feb/03/amazon-kindle-data-reading-tracking-privacy).

Cuando una mujer en EEUU solicito toda la información que tenía Amazon sobre su cuenta (existe esa posibilidad legal) los ficheros contenían más de 20000 líneas que contenían los títulos que leía, registros de las fechas y acciones detallando sus habitos de lectura en una app de Kindle en su iPhone.

Gnu.org informa en ['Amazon's Software Is Malware'](https://www.gnu.org/proprietary/malware-amazon.html) como Amazon publica actualizaciones que dejan inservibles los dispositivos _rooteados_ con otro firmware [[Fuente](https://www.techdirt.com/articles/20150321/13350230396/while-bricking-jailbroken-fire-tvs-last-year-amazon-did-same-to-kindle-devices.shtml)].

![](img/rms_tedx_es-22.png)

---

###  2.22. <a name='Avecesrastreaalusuario'></a>A veces rastrea al usuario

![](img/rms_tedx_es-23.png)

---

###  2.23. <a name='Avecesrestringeloquequierehacerelusuario'></a>A veces restringe lo que quiere hacer el usuario

**A veces restringe lo que quiere hacer el usuario**. Mención especial a los sistemas anticopia del Blu-Ray de Sony y dispositivos DVD o soportes digitales. Con las leyes DRM ([Gestión de derechos digitales](https://es.wikipedia.org/wiki/Gesti%C3%B3n_de_derechos_digitales), del inglés digital rights management) también llamado programas anticopia, desarrollar un programa para saltarse el bloqueo puede ser perseguido por la ley.

En 2002 es un hacker noruego alias DVD Jon que estuvo implicado en el desarrollo de un programa para descifrar la protección de los DVD comerciales fue llevado a juicio en su país por ello, pero no se le halló culpable de ningún comportamiento ilegal. El 5 de marzo de 2003 tuvo lugar un segundo juicio con idéntico resultado. 

![](img/rms_tedx_es-24.png)

---

["The Amazon Kindle-Swindle has a back door that has been used to remotely erase books. One of the books erased was 1984, by George Orwell."](http://pogue.blogs.nytimes.com/2009/07/17/some-e-books-are-more-equal-than-others/)

Miles de personas vieron como Amazon borraba de forma silenciosa el libro 1984 por error de los dispositivos Kindle de sus usuarios, el camino demostro lo fácil que es aplicar la censura en era Kindle.

![](img/rms_tedx_es-25.png)

The Electronic Frontier Foundation has examined and found [various kinds of surveillance in the Swindle and other e-readers](https://www.eff.org/pages/reader-privacy-chart-2012).

![](img/01.png)

---

A veces el fabricante te obliga a usar una versión en concreto.

Gnu.org ['Sabotaje en el software privativo'](https://www.gnu.org/proprietary/proprietary-sabotage.es.html).

"Algunas funcionalidades maliciosas facilitan el sabotaje, esto es, la abusiva intromisión del desarrollador en la utilización del software, con graves consecuencias para los usuarios. Más abajo presentamos una lista de tales situaciones."

![](img/rms_tedx_es-26.png)

Por poner un ejemplo de muchos. [Sony saboteó la Playstation 3](https://www.eff.org/deeplinks/2010/03/sony-steals-feature-from-your-playstation-3) con una actualización regresiva del firmware que eliminaba la funcionalidad que permitía a los usuarios ejecutar GNU/Linux en ella. A continuación, Sony mandó a la policía tras Geohot, después de que este rompiera el código que impedía a los usuarios cambiar el firmware, a lo cual respondimos con un llamamiento para boicotear a Sony. Tras un acuerdo judicial, Sony está ahora pagando por el sabotaje.

![](img/02.png)

![](img/03.png)

---

**El software que no es libre (privativo) a menudo es malware (diseñado para maltratar a los usuarios)**. El software que no es libre está controlado por quienes lo han desarrollado, lo que los coloca en una posición de poder sobre los usuarios; esa es la injusticia básica. A menudo los desarrolladores y fabricantes ejercen ese poder en perjuicio de los usuarios a cuyo servicio deberían estar.

Habitualmente, esto suele realizarse mediante funcionalidades maliciosas.

La FSF mantiene en su sitio Web de GNU.org una relación de malas prácticas de MS [https://www.gnu.org/proprietary/malware-microsoft.html](https://www.gnu.org/proprietary/malware-microsoft.html). Desde puertas traseras a fallas de seguridad que pueden permitir a algún intruso tomar el control de nuestra computadora.

![](img/rms_tedx_es-27.png)

---

Incluso pueden sabotear a sus usuarios, como cuando ["Microsoft informa a la NSA de los errores de Windows antes de repararlos."](https://web.archive.org/web/20130622044225/http://blogs.computerworlduk.com/open-enterprise/2013/06/how-can-any-company-ever-trust-microsoft-again/index.htm), información que puede usar para atacar las computadoras de la gente. 

![](img/rms_tedx_es-28.png)

Se puede profundizar en el tema en el libro ["Vigilancia Permanente"](https://www.planetadelibros.com/libro-vigilancia-permanente/299867) de Richard Snowden. El autor relata la connivencia entre un gobierno que vigila masivamente a sus ciudadanos y las corporaciones tecnológicas de EEUU que lo ayudan a sus espaldas (informan a su gobierno para tener una ventaja en la cigerguerra entra paises).

---
<<<<<<< HEAD
###  2.24. <a name='Abusanconfinesdelucro'></a>Abusan con fines de lucro
=======
###  2.25. <a name='Abusanconfinesdelucro-1'></a>Abusan con fines de lucro
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Es basicamente lo que tienes con el software propietario, el dueño (que no eres tu) tiene el poder sobre los usuarios y se aprovecha de ese poder, para definir algunas funcionalidades maliciosas para dañar al usuario. Por supuesto que no lo hacen porqué sean sádicos; lo hacen por dinero, por avaricia.

Tienen varias formas de sacar provecho de este poder sobre los usuarios 

![](img/rms_tedx_es-29.png)

---

Pero no se averguenzan, incluso hacen conferencias donde hablan sobre las últimas novedades sobre como aprovecharse del usuario gracias al poder que tienen.

![](img/rms_tedx_es-30.png)

---

<<<<<<< HEAD
###  2.26. <a name='Softwareparatontos'></a>Software para tontos
=======
###  2.27. <a name='Softwareparatontos-1'></a>Software para tontos
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Básicamente, el software propietario están usando malware propietario. Es software para tontos.

![](img/rms_tedx_es-31.png)

---

<<<<<<< HEAD
###  2.28. <a name='Comohuimosdeestoydejardeserunavictima'></a>¿Como huimos de esto y dejar de ser una victima? 
=======
###  2.29. <a name='Comohuimosdeestoydejardeserunavictima-1'></a>¿Como huimos de esto y dejar de ser una victima? 
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Antiguamente tenías que dejar de usar computadoras, pero ya no. Eso es casi imposible, en su lugar hemos construido un sistema libre. Puedes unirte al mundo libre que han creado.

![](img/rms_tedx_es-32.png)

---

En enero de **1984** RMS deja su empleo en el en el MIT y comenzó a escribir software GNU. Abandonar el MIT era imprescindible si quería que nadie interfiriera en la distribución de GNU como software libre (que el propio MIT quisiera apodersarse del S.O como producto suyo por ejemplo).

Comienza el desarrollo de un sistema operativo completamente libre llamado Gnu.

El primer programa para el proyecto GNU debía ser un compilador para crear nuevos programas. Stallman comienza a escribirlo en **1985**. Crea un compilador de cero, el resultado es el compilador conocido como [GCC](https://es.wikipedia.org/wiki/GNU_Compiler_Collection) (GNU C Compiler).

En **1992** casi tienen completo el S.O pero les falta el _kernel_. Linus Torvalds en aquel año liberó el núcleo Linux con el que llenar el último hueco. Nace el primer S.O completamente libre que funciona sobre un PC: **GNU/Linux**.

![](img/rms_tedx_es-33.png)

---

<<<<<<< HEAD
###  2.30. <a name='Distribuciones'></a>Distribuciones
=======
###  2.31. <a name='Distribuciones-1'></a>Distribuciones
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Desafortunadamente, tener libertad por un lado no garantiza mantenerla. Hay más de un centenar de versiones diferentes de GNU/Linux conocidas como distribuciones. 

Algunas son enteramente software libre pero no todas las distribuciones son completamente libres, la mayoría de ellas tienen software privativo añadido porqué están mantenidos por gente que no esta preocupada por el software libre, sino por el conveniente coste del software libre. 

Por ello debes comprobar que distribuciónes libre.

![](img/rms_tedx_es-34.png)

Un ejemplo clásico son los [codecs MP3](https://audio-video.gnu.org/docs/formatguide.html), los repositorios de distribuciones basadas en software libre 100% ofrecen alternativas. Distribuciones como Ubuntu permiten añadir repositorios con paquetes con licencias privativas (Flash de Adobe es otro ejemplo).

---

<<<<<<< HEAD
###  2.32. <a name='Mantenertulibertadrequiereunsacrificio'></a>Mantener tu libertad requiere un sacrificio
=======
###  2.33. <a name='Mantenertulibertadrequiereunsacrificio-1'></a>Mantener tu libertad requiere un sacrificio
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Mantener tu libertad requiere un sacrificio, a veces un gran sacrificio como en Lexington (1775 - guerra de independencia de EEUU para liberarse del control británico). 

![](img/rms_tedx_es-35.png)

---

<<<<<<< HEAD
###  2.34. <a name='Smallsacrificesforfreedom'></a>Small sacrifices for freedom
=======
###  2.35. <a name='Smallsacrificesforfreedom-1'></a>Small sacrifices for freedom
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Otras es suficiente con pequeños gestos, cualquiera puede hacerlos.

![](img/rms_tedx_es-36.png)

---

<<<<<<< HEAD
###  2.36. <a name='Nonfreeapplications'></a>Non free applications
=======
###  2.37. <a name='Nonfreeapplications-1'></a>Non free applications
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Por ejemplo, quieres aplicaciones pero algunas no son libres. Si quieres libertad, debes hacerlo sin ellas. Puede haber algunos inconvenientes a sufrir por el bien de la libertad.

![](img/rms_tedx_es-37.png)

---

<<<<<<< HEAD
###  2.38. <a name='NonfreeJavascriptprogram'></a>Non free Javascript program
=======
###  2.39. <a name='NonfreeJavascriptprogram-1'></a>Non free Javascript program
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Muchos servidores envía código JS no libre a nuestros navegadores. Si no quieres usar software propietario deberías instalar [LibreJS](https://www.gnu.org/software/librejs/), que bloquea y evita el JavaScript no libre.

[https://www.gnu.org/philosophy/javascript-trap.html](https://www.gnu.org/philosophy/javascript-trap.html)

"Some sites still use JavaScript that way, but many use it for major programs that do large jobs. For instance, Google Docs tries to install into your browser a JavaScript program which measures half a megabyte, in a compacted form that we could call Obfuscript. This compacted form is made from the source code, by deleting the extra spaces that make the code readable and the explanatory remarks that make it comprehensible, and replacing each meaningful name in the code with an arbitrary short name so we can't tell what it is supposed to mean."

![](img/rms_tedx_es-38.png)

[LibreJS](https://www.gnu.org/software/librejs/) es un _add-on_ para navegadores que bloquea código JS no trivial.

![](img/04.png)

---

<<<<<<< HEAD
###  2.40. <a name='Servicioscomosustitutosdesoftware'></a>Servicios como sustitutos de software
=======
###  2.41. <a name='Servicioscomosustitutosdesoftware-1'></a>Servicios como sustitutos de software
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

A veces, los servidores te ofrecen realizar sus tareas (SaaS). Dicen, "Envíanos toda tu información". El servidor realiza la tarea y te devuelve los resultados. Supuestamente no deberías pensar sobre lo que pasa, porque es una "nube" y no ves lo que ocurre. Pues deberías mirar. Se ofrece como sustituto del software y le quita el control a tu computadora.

![](img/rms_tedx_es-39.png)

---

<<<<<<< HEAD
###  2.42. <a name='PrioridadServidoreswebotupropiamquina'></a>¿Prioridad? Servidores web o tu propia máquina
=======
###  2.43. <a name='PrioridadServidoreswebotupropiamquina-1'></a>¿Prioridad? Servidores web o tu propia máquina
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Gran parte de los servidores del mundo se ejecutan sobre GNU/Linux.

¿Prioridad? ¿Servidores Web o tu propia máquina? La forma más eficiente y segura es usar tu propia computadora y no los servicios de grandes compañias. Las computadoras más importantes donde establecer la libertad son las nuestras, no la de los servidores. Por encima de todo es la gente la que merece libertad.

![](img/rms_tedx_es-40.png)

---

<<<<<<< HEAD
###  2.44. <a name='Queobstculos'></a>¿Que obstáculos? 
=======
###  2.45. <a name='Queobstculos-1'></a>¿Que obstáculos? 
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Para ello necesitamos avanzar y salvar los obstaculos. 

![](img/rms_tedx_es-41.png)

---

<<<<<<< HEAD
###  2.46. <a name='Grandescompaias'></a>Grandes compañias
=======
###  2.47. <a name='Grandescompaias-1'></a>Grandes compañias
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Uno de ellos es que las grandes compañias sacan grandes beneficios controlando a los usuarios y no quieren que avancemos. Tenemos que vencer su oposición.

![](img/rms_tedx_es-42.png)

---

<<<<<<< HEAD
###  2.48. <a name='Mediosdecomunicacin'></a>Medios de comunicación 
=======
###  2.49. <a name='Mediosdecomunicacin-1'></a>Medios de comunicación 
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Otro obstaculo son los grandes medios de comunicación, en lugar de hablar del software libre tienen un termino que usan para evitar hablar de cuestiones éticas, lo llaman "código abierto". 

![](img/rms_tedx_es-43.png)

---

<<<<<<< HEAD
###  2.50. <a name='Freesoftwareactivists-Opensourceboosters'></a>Free software activists - Open source boosters
=======
###  2.51. <a name='Freesoftwareactivists-Opensourceboosters-1'></a>Free software activists - Open source boosters
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Se habla más o menos de los mismos programas, pero con diferentes ideas.

"Es una cuestión de lo correcto y incorrecto. Los usuarios precisan libertad, queremos libertad".

La gente que dice código abierto, no quieren decir eso. En cambio, dicen, "Dejen a los usuarios cambiar el software y redistribuirlo, y mejorarán el código, corregirán muchos fallos". Quiza sea verdad, pero no es el tema más importante. Si queremos mantener nuestra libertad, tenemos que hablar de la libertad. 

![](img/rms_tedx_es-44.png)

---

<<<<<<< HEAD
###  2.52. <a name='Digasoftwarelibre'></a>¡Diga software libre!
=======
###  2.53. <a name='Digasoftwarelibre-1'></a>¡Diga software libre!
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Por eso digan software libre, y así ayudarán todo el tiempo a desambiguar ambos términos.

![](img/rms_tedx_es-45.png)

---

<<<<<<< HEAD
###  2.54. <a name='Escuelasysoftwarelibre'></a>Escuelas y software libre
=======
###  2.55. <a name='Escuelasysoftwarelibre-1'></a>Escuelas y software libre
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Algo parecido pasa en las escuelas usando y enseñando software privativo, basicamente es lo mismo que enseñarles a fumar tabaco. Es implantar la dependencia, lo opuesto a lo que la escuela debería hacer. **La escuela debería prepararnos a los ciudadanos para vivir en una sociedad fuerte, capaz, independiente y cooperativa**.

![](img/rms_tedx_es-46.png)

---

<<<<<<< HEAD
###  2.56. <a name='Ensearsoftwarelibreenlaescuela'></a>Enseñar software libre en la escuela
=======
###  2.57. <a name='Ensearsoftwarelibreenlaescuela-1'></a>Enseñar software libre en la escuela
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Lo que siginfica enseñar software libre en las escuelas. 

![](img/rms_tedx_es-47.png)

---

<<<<<<< HEAD
###  2.58. <a name='Escuelas'></a>Escuelas 
=======
###  2.59. <a name='Escuelas-1'></a>Escuelas 
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

No es la única razón, muchos niños pueden sentir la curiosidad de saber como funcionan los programas, tal vez quieran convertirse en programadores, son curiosos. El que estudia software no libre no aprende nada, porque el conocimiento en el programa no libre es retenido y negado a los estudiantes.

Para defender el espíritu de la educación, la escuela debería asegurarse de que sus programas son libres.

![](img/rms_tedx_es-48.png)

[Eskolak – Hezkuntzan ere Librezale](https://hezkuntza.librezale.eus/)

---

<<<<<<< HEAD
###  2.60. <a name='Elespritudelabuenavoluntad'></a>El espíritu de la buena voluntad
=======
###  2.61. <a name='Elespritudelabuenavoluntad-1'></a>El espíritu de la buena voluntad
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f

Pero hay una razón más importante: Las escuelas deberían enseñar el espíritu de la buena voluntad, el hábito de ayudar a otros. La clase debería decir, "Si traes un programa a clase, es como si llevaras galletas a clase, tienes que compartirlas con todos". No puedes guardártelas para ti sólo. Tienes que compartir el código fuente para que otros puedan aprender, así que no traigas software propietario a la escuela, la escuela debe dar un buen ejemplo siguiendo sus propias normas. 

![](img/rms_tedx_es-49.png)

---

<<<<<<< HEAD
###  2.62. <a name='Manualdeusuario'></a>Manual de usuario
=======
###  2.63. <a name='Manualdeusuario-1'></a>Manual de usuario
>>>>>>> 95130e49eb1058a90f0c3bddf2753f3bf056fb2f


![](img/rms_tedx_es-50.png)

---

![](img/rms_tedx_es-51.png)

---

Cada actividad de la vida depende de las libertades básicas, si perdemos alguna es dificil mantener el resto. 
¿Cómo podemos ayudar? Escribiendo sotware libre, organizando campañas a favor del software libre, etc. Persuadir a gobiernos y escuelas de que usen el software libre, ayudar a otros a usarlo. 

**Usar software libre es el primer paso** pero necesitamos por supuesto otras cosas como la neutralidad en la red.

![](img/rms_tedx_es-52.png)


