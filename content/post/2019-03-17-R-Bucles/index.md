---
title: R - Bucles y ejecución repetitiva
subtitle: for, while, repeat
date: 2019-03-17
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico", "R - Bucles"]
---

![](images/EBpC_2EXsAMzEAp.jpg)

Se trata de replicar unas instrucciones una serie de veces.

Tenemos 3 tipos de bucles:

- for
- while
- repeat

Y 2 comandos asociados:

- break
- next

## Bucle for

![](images/content_flowchart1.png)

El bucle `for` permite ejecutar una serie de instrucciones de forma repetitiva varias veces, en el ejemplo de abajo bucle cada iteración del bucle la variable `i` toma el valor de la secuencia que recorre los números desde -5 al 5 (pasando por el 0 claro):

```R
for (i in -5:5) print(i)
```

Ahora vamos a usar la condicional `if` dentro de un bucle `for`, recorremos con la variable `i` los valores del 1 al 10, imprimimos el valor de `i` en cada iteración

```R
a <- 5
for (i in 1:10) {
  cat(i,"\n")
  if (i==a) cat("i vale 5\n")
}
```

Por supuesto no hay problema en anidar bucles `for`. La siguiente función usa dos bucles uno dentro de otro para calcular la [matriz de Hilbert](https://es.wikipedia.org/wiki/Matriz_de_Hilbert) (es una matriz cuadrada cuyos campos constituyen una fracción de la unidad).

```R
MatrizHilbert <- function(m,n) {
  y <- matrix(1,nrow = m,ncol = n)
  for (i in 1:m) {
    for (j in 1:n) {
      y[i,j] = 1/(i+j-1)
    }
  }
  return(y)
}

MatrizHilbert(3,3)
```

Otro ejemplo interesante donde usamos un vector:

```R
years <- c(2005,2006,2007)
for(i in years) print(i)
```

Otro ejemplo:

```R
ciudades <- c("Madrid","Bilbao","Barcelona","Donostia","Valencia")
for (ciudad in ciudades) {
  if (nchar(ciudad) > 6) print(ciudad)
}
```

Otra forma recorrer los elementos del vector de arriba, la ventaja es que permite usar `i` para indexar cada elemento del vector:

```R
for(i in 1:length(ciudades)) {
  print(ciudades[i])
}
```

## Bucle while

Ejecuta una serie de comandos dentro del bucle mientras se cumpla una condición lógica, en el siguiente ejemplo inicializamos una variable `i` a 1 antes de entrar en el bucle, seguido comienza el bucle `while` evaluando la condición, dentro imprimimos el valor de `i` y le sumamos una unidad.

```R
i <- 1; while(i<=5) { print(i); i<-i+1}
```

```R
frase <- "Esta es mi. frase"
i <- 0
letra <- ''
while(letra!='.') {
  i <- i+1
  letra <- substr(frase,i,i)
  print(letra)
}
```

## Bucle repeat

Este se usa menos a menudo, ejecuta la secuencia de instrucciones hasta que salimos con `break`, por ejemplo:

```R
i = 1
repeat {
  print(i)
  i <- i+1
  if (i==5) break
}
```

Recorremos una cadena como en el ejemplo anterior con una condición de parada cuando leamos la ",":

```R
frase <- "Esta. es mi, frase"
i <- 0
letra <- ''
repeat {
  i <- i+1
  letra <- substr(frase,i,i)
  print(letra)
  if (letra == ',') break
}
```

## Break en un For

Vamos a usar el comando `break` en un bucle que no sea un `repeat`. Creamos una matriz de 5x5 con valores 0, usando dos bucles `for` anidados recorremos sus filas y columnas, cuando el valor de `i` y `j` son iguales (diagonal de la matriz) el bucle anidado se interrumpe y pasa a la siguiente iteración del `for` superior (salta a la siguiente fila).

```R
> matriz <- matrix(0,nrow = 5 ,ncol = 5)
> for(i in 1:dim(matriz)[1]) {
+   for(j in 1:dim(matriz)[2]) {
+     if (i == j) break
+     else matriz[i,j] <- i*j
+   }
+ }
> matriz
     [,1] [,2] [,3] [,4] [,5]
[1,]    0    0    0    0    0
[2,]    2    0    0    0    0
[3,]    3    6    0    0    0
[4,]    4    8   12    0    0
[5,]    5   10   15   20    0
>
```

## Comando next

El comando `next` interrumpe ejecución de la iteración actual y salta al siguiente ciclo.

```R
> x <- 1:5
> for (val in x) {
+   if (val == 3){
+     next
+   }
+   print(val)
+ }
[1] 1
[1] 2
[1] 4
[1] 5
```

## Medir el tiempo de ejecución de un bucle

```R
> v1 <- seq(1,10)
> v2 <- seq(20,11)
> vt <- 0
> pt <- proc.time()
> for(i in 1:10) {
+   vt[i] <-v1[i]+v2[i]
+ }
> proc.time() - pt
   user  system elapsed
   0.01    0.00    0.04
>
```

## Ejercicios

- r / intro / 26-Bucles / [bucles.R](https://gitlab.com/soka/r/blob/master/intro/26-Bucles/bucles.R).
