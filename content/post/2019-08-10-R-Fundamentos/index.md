---
title: Repaso rápido conceptos básicos en R
subtitle: Fundamentos de R
date: 2019-08-10
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Básico"]
---

![](images/r-logo.PNG)

Este artículo es un repaso rápido de los conceptos básicos de **R** para desentumecer un poco la mente y practicar un poco, si ya conoces R probablemente no merezca la pena, sigue caminando y no pares aquí. En muchos casos he puesto enlaces a artículos previos donde se desarrolla y se profundiza sobre temas concretos, el listado completo de todos los artículos está en [https://gitlab.com/soka/r](https://gitlab.com/soka/r), [enlace](https://gitlab.com/soka/r/blob/master/intro/47-Repaso-Fundamentos-R/Fundamentos-Repaso.R) al código fuente.

## Operaciones aritméticas

```r
1+1
1+2+3
1+1;2+5
```

Enlaces:

- ["Operadores y aritmética básica en R - Aritméticos, comparativos y lógicos"](https://soka.gitlab.io/blog/post/2019-01-28-aritmetica-y-objetos-en-r/).

## Cadenas

```r
'Hola mundo';"Hola mundo"
```

Enlaces:

- ["Manipulación de cadenas en R"](https://soka.gitlab.io/blog/post/2019-08-11-r-manipulacion-cadenas/).

## Variables

```r
# Variables (se puede inspeccionar en RStudio en "Global Environment")
a <- 1+2
b <- 1+1+3
# El operador = es equivalente pero es mejor usar <-
c <- 'Hola mundo'
```

Enlaces:

- ["Tipos de datos básicos en R"](https://soka.gitlab.io/blog/post/2019-03-03-tipos-de-datos-r/).

## Entorno de trabajo

```r
# Entorno de trabajo
getwd()
setwd("C:/Users/i.landajuela/Documents/GitLab/r/intro/47-Repaso-Fundamentos-R")
sink("salida.log") # Dirigir salida a fichero
sink() # Sin argumentos para volver a consola normal
objects() # Variables
rm(a) # Borrar una variable
rm(b,c,x) # O borrar varias
cat('\014') # Borrar consola
```

## Ayuda

```r
?class
x <- 12
class(x) # [1] "numeric"
```

## Operadores lógicos y de comparación

```r
a <- 5
if (a==5) cat("a vale 5")
if (a!=4) cat("a NO vale 5")
if (a >= 5) cat("a es igual o mayor que 5")
if (a < 3) cat("a es menor que 3") else cat("es mayor o igual que 3")
```

```r
# Bloque comandos
if (a==5) {
  cat("a vale 5")
  a <- 4
} else {
  cat("a NO vale 5")
}
```

```r
!(a<3) # [1] TRUE
```

## Bucles

```r
for (i in 1:10) {
  cat(i,'\n')
  if ( i == 5 ) cat("Vale 5\n")
}
```

Enlaces:

- ["Bucles"](https://soka.gitlab.io/blog/post/2019-03-17-r-bucles/).

## Vectores

Definición:

```r
v1 <- c(1,2,3.0,2)
c(1,2,3.0,2) -> v1 # Alternativa
assign("v2",c(1,2,3)) # Alternativa
class(v1) # [1] "numeric"
str(v1) # num [1:4] 1 2 3 2
length(v1) # [1] 4
```

Indexación:

```r
v1[1] # 1
v1[-1] # Todos menos primer elemento
v1[1:3]
# Oper
v2 <- v1 >= 2 # [1] FALSE  TRUE  TRUE  TRUE
# Indexar por condición del valor
v1[v1>=2] # [1] 2 3 2
# Mismo resultado
v1[v2] # [1] 2 3 2
```

Concatenar vectores:

```r
v3 <- c(v1,v2,7,8,9,-v2) #  [1]  1  2  3  2  1  2  3  7  8  9 -1 -2 -3
```

Operaciones con vectores:

```r
v4 <- 2*v1-3 # [1] -1  1  3  1
```

```r
max(v1)
min(v1)
length(v1)
sum(v1)
prod(v1) # Multiplicación
mean(v1); sum(v1) / length(v1) # Media
# Ordena posición elementos
order(v1); sort.list(v1) # [1] 1 2 4 3
v1[order(v1)] # podemos comprobarlo
```

Enlaces:

- ["Vectores"](https://soka.gitlab.io/blog/post/2019-03-11-r-vectores/).

## Dataframes

Creando un DF:

```r
ciudades <- c("Bilbo","Barcelona","Madrid","Valencia")

# Generamos una muestra de 10 elementos (con elementos duplicados)
ciudades.sample <- sample(ciudades,10,replace = TRUE)
table(ciudades.sample) # Nos da la frecuencia de aparición de los elementos
# Con probabilidad de aparición de cada elemento
ciudades.sample <- sample(ciudades,10,replace = TRUE,prob = c(.1,.5,.2,.2))
poblacion <- c(1:4)
df <- data.frame(ciudades,poblacion)
```

Indexar:

```r
df[3,1]
```

```r
# Añadir nueva columna
nivel <- c("Alto","Alto","Medio","Bajo")
df <- cbind(df,nivel)
# Nueva fila
fila <- data.frame(ciudades="Gijon",poblacion=12,nivel="Medio")
rbind(df,fila)
```

```r
# Acceso a un fila
df$ciudades
#subset
df[c(1:3),c(1,2)]
df[c(1:3),c('poblacion','nivel')]
df[c(1:3),]
df[,c('ciudades','nivel')]
```

```r
# Nombres de las columnas
names(df)
names(df)[c(2,3)]
# Modificamos nombres columnas
names(df)[c(2,3)] <- c('pobl','lv')
```

Enlaces:

- ["Data Frames en R"](https://soka.gitlab.io/blog/post/2019-03-12-r-data-frame/).
- ["Añadir una nueva columna cbind()"](https://soka.gitlab.io/blog/post/2019-03-01-ainadir-nueva-columna-df-en-r/).
- ["Operaciones con Data Frames"](https://soka.gitlab.io/blog/post/2019-04-22-r-operaciones-con-data-frames/).
- ["Seleccionar datos con [] y subset"](https://soka.gitlab.io/blog/post/2019-03-15-r-seleecionar-datos-subset/).

## Listas

```r
lst <- list(nombre = 'pedro',num.hijos=2,edad.hijos <- c(14,22))
lst$nombre
lst[[1]]
#lst$edad.hijos # ??????????
lst[[3]]
lst[[3]][2]
# Concatenar listas
lst2 <- list(nombre = 'mikel',num.hijos=3,edad.hijos <- c(4,2,3))
lst.total <- c(lst,lst2)
lst.total[[4]]
class(lst.total)
# Listado de listas
lst.total1 <- list(lst,lst2)
lst.total1[[2]]["nombre"] # acceso
```

Enlaces:

- [Listas en R](https://soka.gitlab.io/blog/post/2019-03-11-r-listas/).

## Series temporales

Enlaces:

- ["Series de tiempo en R"](https://soka.gitlab.io/blog/post/2019-04-16-r-series-de-tiempo/).
