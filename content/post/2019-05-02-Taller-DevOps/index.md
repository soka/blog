---
title: DevOps como estrategia para la servitización de aplicaciones software
subtitle: Documentación jornada
date: 2019-05-02
draft: false
description: Impartido por División ICT – IT Competitiveness
tags: ["DevOps", "Talleres", "Jornadas", "SPRI", "Presentaciones"]
---

[Enlace](https://www.spri.eus/euskadinnova/es/enpresa-digitala/agenda/devops-como-estrategia-para-servitizacion-aplicaciones-software/14850.aspx) jornada.

Ponentes: Leire Orue-Echevarria y Gorka Benguria.

_"El objetivo de esta jornada es introducir la filosofía DevOps y dar a conocer las mejores prácticas y herramientas existentes en este campo."_

_"En los últimos años se ha producido un importante cambio en el modo en que consumimos, producimos y hacemos negocio en general. La transformación hacia las "servitización" en todos los ámbitos, sociales, tecnológicos y económicos es un hecho que ha propiciado la aparición de nuevos modelos económicos y de negocio, respaldados por importantes avances tecnológicos. La tendencia actual para la integración y despliegue continuos es adoptar un enfoque DevOps: unir los entornos de desarrollo y de operación facilitando la comunicación entre los miembros de hasta ahora dos equipos independientes y con escasa comunicación."_

_"Esta jornada pretende presentar la filosofía de DevOps para el desarrollo y operación de aplicaciones. Durante la jornada se presentan el marco metodológico y tecnológico de DevOps de acuerdo con la experiencia desarrollada en proyectos multi-tecnología y multi-empresarial."_

![](images/201801129_DevOps_EnpresaDigitala_v0.2-001.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-002.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-003.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-004.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-005.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-006.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-007.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-008.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-009.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-010.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-011.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-012.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-013.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-014.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-015.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-017.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-018.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-019.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-020.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-021.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-022.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-023.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-024.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-025.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-026.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-027.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-028.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-029.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-030.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-031.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-032.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-033.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-034.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-035.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-036.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-037.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-038.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-039.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-040.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-041.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-042.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-043.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-044.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-045.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-046.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-047.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-048.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-049.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-050.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-051.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-052.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-053.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-054.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-055.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-056.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-057.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-058.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-059.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-060.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-061.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-062.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-063.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-064.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-065.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-066.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-067.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-068.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-069.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-070.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-071.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-072.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-073.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-074.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-075.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-076.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-077.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-078.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-079.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-080.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-081.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-082.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-083.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-084.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-085.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-086.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-087.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-088.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-089.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-090.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-091.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-092.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-093.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-094.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-095.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-096.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-097.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-098.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-099.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-100.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-101.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-102.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-103.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-104.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-105.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-106.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-107.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-108.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-109.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-110.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-111.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-112.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-113.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-114.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-115.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-116.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-117.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-118.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-119.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-120.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-121.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-122.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-123.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-124.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-125.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-126.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-127.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-128.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-129.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-130.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-131.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-132.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-133.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-134.jpg)

![](images/201801129_DevOps_EnpresaDigitala_v0.2-135.jpg)
