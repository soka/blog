---
title: Exportar los datos a un fichero CSV - R 
subtitle: write.table()
date: 2019-03-01
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico"]
---

La función `write.table()` escribe el contenido de un DF en un archivo. 

```R
dfPersonas <- data.frame("Id" = 1:2, "Age" = c(21,15), "Name" = c("John","Dora"))

write.table(dfPersonas,"datos_personas.csv")
```

El contenido del nuevo fichero es:

![](images/csv_01.PNG)

Vamos a modificar algunos parámetros de la función para que no guarde las comillas dobles (`quotes=FALSE`), definimos el separador de campos con el carácter ";" y no guardamos los nombres de filas.

```R
write.table(dfPersonas,"datos_personas.csv",
            quote = FALSE, # Sin dobles comillas en cadenas
            sep = ";", # Separador de campos
            row.names = FALSE # No coge nombres de filas del DF
            )
```

![](images/csv_02.PNG)



# Ejercicios

* r / intro / 17-Exportar-los-datos / [exportar-los-datos.R](https://gitlab.com/soka/r/blob/master/intro/17-Exportar-los-datos/exportar-los-datos.R).

# Enlaces

**Enlaces externos:**

* ["Objetos en R: Data Frames y Listas"](http://www.dma.ulpgc.es/profesores/personal/stat/cursoR4ULPGC/6g-Data_frames-Listas.html).

