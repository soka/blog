---
title: Herramientas de diseño Web
subtitle: Combinación de fuentes, paletas de colores y prototipado
date: 2021-10-06
draft: false
description: Combinación de fuentes, paletas de colores y prototipado
tags: ["Diseño","Interfaz","Web","Colores","Layout","Wireframe","Herramientas","Aplicaciones"]
---

<!-- vscode-markdown-toc -->
* 1. [Excalidraw | Hand-drawn look & feel • Collaborative • Secure](#ExcalidrawHand-drawnlookfeelCollaborativeSecure)
* 2. [fontjoy.com](#fontjoy.com)
* 3. [color.adobe.com](#color.adobe.com)
* 4. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Muchos de estos interesantes recursos están obtenidos de [Freddy Montes](https://www.instagram.com/fmontes/) ([@fmontes](https://twitter.com/fmontes)).

##  1. <a name='ExcalidrawHand-drawnlookfeelCollaborativeSecure'></a>Excalidraw | Hand-drawn look & feel • Collaborative • Secure

[Excalidraw](https://excalidraw.com/) ([@excalidraw](https://twitter.com/excalidraw)) es una pizarra virtual colaborativa Web para representar el esqueleto o la estructura visual básica de los componentes ([_wireframe_](https://es.wikipedia.org/wiki/Website_wireframe)), la aplicación no requiere registro previo y se puede compartir mediante enlace con otras personas para trabajar simultaneamente.

![](img/01.PNG)

La aplicación ofrece los controles básicos para dibujar rapidamente sin muchos lios, se puede exportar o salvar como una imagen.

Los mismos desarrolladores de [Excalidraw](https://excalidraw.com/) han desarrollado Excalidraw+, una versión de pago.

##  2. <a name='fontjoy.com'></a>fontjoy.com

El porcentaje medio de texto en una Web era del 26,88% en 2014 ([The rise and fall of text on the Web: a quantitative study of Web archives](http://www.informationr.net/ir/20-3/paper682.html#.YV1bC5pBzIU), [esta página](https://fontjoy.com/) propone combinaciones de diferentes tipografías. 

![](img/02.PNG)

##  3. <a name='color.adobe.com'></a>color.adobe.com  

Normalmente para un diseño Web se elije un color base y otro secundario que combine o contraste con el anterior, además de variantes más oscuras, claras o con menor opacidad de los anteriores.

[color.adobe.com](https://color.adobe.com/) ofrece varias utilidades: ruedas de combinaciones de color, tendencias y paletas de colores preparadas, herramientas de accesibilidad, etc.

![](img/03.PNG)

##  4. <a name='Enlacesexternos'></a>Enlaces externos

* ["Unsplash"](https://unsplash.com/): The internet’s source of freely-usable images.
* ["30 Best Website Layout Examples and Ideas for Web Design"](https://www.mockplus.com/blog/post/website-layout-example).
* ["How Much Text is Too Much?"](https://www.wholegraindigital.com/blog/too-text-much/).
* ["How Users Read on the Web"](https://www.nngroup.com/articles/how-users-read-on-the-web/).