---
title: Manual LastPass [borrador]
subtitle: La mejor forma de gestionar contraseñas
date: 2021-01-24
draft: false
description: Memorice solo su contraseña maestra. LastPass se ocupa de recordar todas las demás.
tags: ["LastPass","Aplicaciones","Manuales","Seguridad","Contraseñas","Web","Ciberseguridad"]
---

<!-- vscode-markdown-toc -->
* 1. [LASTPASS](#LASTPASS)
	* 1.1. [Web](#Web)
		* 1.1.1. [Configuración](#Configuracin)
	* 1.2. [Extensión para navegadores](#Extensinparanavegadores)
	* 1.3. [Aplicación para móvil](#Aplicacinparamvil)
	* 1.4. [LastPass Authenticator](#LastPassAuthenticator)
* 2. [ALTERNATIVAS](#ALTERNATIVAS)
* 3. [ENLACES INTERNOS](#ENLACESINTERNOS)
* 4. [ENLACES EXTERNOS](#ENLACESEXTERNOS)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

En una entrada previa en este mismo blog ["La clave eres tú"](https://soka.gitlab.io/blog/post/2021-01-15-la-clave-eres-tu/) hablé entre otras cosas de la necesidad de usar contraseñas robustas y no reutilizar las mismas contraseñas en diferentes servicios, estos dos criterios chocan entre sí, las contraseñas cada vez son más largas y complejas y cada vez nos registramos en más servicios en línea para usos profesionales o personales, recordarlas todas cada vez es más difícil.

[Este vídeo](https://youtu.be/XEQAMvZoKNA) del OSI (Oficina de Seguridad del Internauta) explica de forma breve en que consiste un gestor de contraseñas.

{{< youtube XEQAMvZoKNA >}}

Un [**gestor de contraseñas**](https://es.wikipedia.org/wiki/Gestor_de_contrase%C3%B1as) o administrador de contraseñas es una aplicación que permite almacenar una gran cantidad de pares de usuario/contraseña. La base de datos (se le llama también bóveda) donde se almacena esta información suele estar cifrada mediante una **contraseña maestra** (como mínimo debería ofrecer una autenticación de doble factor para acceder a los registros), de forma que el usuario sólo tenga que memorizar una clave para acceder a todas las demás, esto facilita usar diferentes contraseñas para cada servicio y que estas sean complejas y largas sin miedo a no poder recordarlas posteriormente.

##  1. <a name='LASTPASS'></a>LASTPASS

![](img/01.png)

[**LastPass**](https://www.lastpass.com) es un gestor de contraseñas disponible para el navegador Web y dispositivos móviles ([Android](https://play.google.com/store/apps/details?id=com.lastpass.lpandroid&hl=en), [iOS](https://apps.apple.com/es/app/lastpass-password-manager/id324613447)). Es una solución de pago pero ofrece una versión gratuita con algunas funcionalidades menos, en mi experiencia personal la versión gratuita es suficiente para empezar. En este [enlace](https://www.lastpass.com/es/pricing) se pueden comparar los diferentes planes.

Resumen de características de la versión _free_:

* Acceso desde cualquier dispositivo y sin conexión.
* Uso compartido con una persona.
* Guardar y autocompletar contraseñas, permite que complete los datos de los formularios de acceso de forma automática.
* Generador de contraseñas. Cuando te registras en un sitio nuevo o decides cambiar una contraseña de una cuenta LastPass se encarga de sugerirte una.
* Notas seguras.
* Autenticación multifactor.
* [LastPass Authenticator](https://lastpass.com/auth/).

El primer paso es crear una cuenta gratuita en esta dirección [https://lastpass.com/create-account.php](https://lastpass.com/create-account.php). Una vez creada la cuenta yo recomiendo instalar la [extensión disponible](https://lastpass.com/misc_download2.php) para casi todos los navegadores conocidos: [Chrome](https://chrome.google.com/webstore/detail/lastpass-free-password-ma/hdokiejnpimakedhajhdlcegeplioahd?hl=es), [Firefox](https://addons.mozilla.org/es/firefox/addon/lastpass-password-manager/), [Opera](https://addons.opera.com/en/extensions/details/lastpass/) o [Edge](https://microsoftedge.microsoft.com/addons/detail/bbcinlkgjjkejfdpemiealijmmooekmp) por ejemplo. Trataré de explicar más adelante porqué se convierte en indispensable esta extensión para trabajar con comodidad.

###  1.1. <a name='Web'></a>Web

Una vez registrados iniciamos la sesión en el [sitio Web de LastPass](https://lastpass.com/?ac=1&lpnorefresh=1), como es la primera vez aún no hay ningún contenido en el área central.

![](img/02.png)

Podemos añadir un elemento inmediatamente o pasando el puntero del ratón por el mismo botón añadir una carpeta, mi sugerencia es primero crear una estructura básica de carpetas para ordenar los contenidos usando algún criterio, las carpetas pueden contener otras carpetas anidadas a su vez.

Por ejemplo voy a agrupar todas las cuentas de correo, dentro creo una carpeta para Google ya que tengo varias cuentas con este servicio. En la parte superior se puede cambiar la forma de visualizar los contenidos, a mi me gusta la vista de lista.

![](img/03.png)

Ahora es el momento de añadir el primer elemento de tipo contraseña, debemos rellenar un formulario con algunos campos. La URL se refiere a la dirección de la Web donde está el formulario de acceso, el nombre es una información sólo para LastPass, podemos poner el que queramos, supongamos que queremos guardar el usuario/contraseña de [Popplet](https://app.popplet.com/#/signup) (una aplicación Web para generar diagramas) donde acabamos de registrarnos (si tenemos el _plugin_ instalado en el navegador el propio LastPass nos sugiere una contraseña segura para la nueva cuenta y nos permite registrarla en nuestra bóveda nada más crear la cuenta).

![](img/05.png)

La carpeta es donde queremos guardar el nuevo contenido, el nombre de usuario y la contraseña son las que acabamos de introducir en Popplet, podemos añadir notas debajo. En la configuración avanzada es muy cómodo marcar el inicio de sesión automático.

Al final del formulario podemos guardar el nuevo registro entre los favoritos o compartir con una dirección de correo electrónico el contenido (incluida la contraseña si así lo queremos), una vez compartido con otro usuario podrá ver el contenido dentro de su cuenta en el apartado "Centro de uso compartido" (para compartir una carpeta es necesario una cuenta de pago).

Antes de seguir probando el _plugin_ para el navegador no está de menos revisar la configuración.

####  1.1.1. <a name='Configuracin'></a>Configuración

Conviene revisar una vez al menos la configuración de la cuenta, en la pestaña "General" define un número de teléfono para recuperar la cuenta.

![](img/06.png)

La configuración avanzada contiene un montón de opciones más para proteger nuestra cuenta de LastPass.

También es recomendable activar la autenticación de dos factores usando una aplicación móvil como [Google Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=es&gl=US) o la propia de [LastPass Authenticator](https://lastpass.com/auth/).

###  1.3. <a name='Aplicacinparamvil'></a>Aplicación para móvil

Durante la instalación solicita confirmar la nueva "ubicación" confirmando mediante el correo electrónico, lo mejor es habilitar la opción de autocompletar formularios.

##  3. <a name='ENLACESINTERNOS'></a>ENLACES INTERNOS

- Manual LastPass [https://soka.gitlab.io/blog/post/2021-01-24-manual-lastpass/](https://soka.gitlab.io/blog/post/2021-01-24-manual-lastpass/).
- [https://soka.gitlab.io/blog/post/2021-01-15-la-clave-eres-tu/](https://soka.gitlab.io/blog/post/2021-01-15-la-clave-eres-tu/).

##  4. <a name='ENLACESEXTERNOS'></a>ENLACES EXTERNOS

* How do I use the LastPass web browser extension? [https://support.logmeininc.com/lastpass](https://support.logmeininc.com/lastpass) 
* Use the Google Authenticator [https://support.logmeininc.com/lastpass/help/google-authenticator-lp030015](https://support.logmeininc.com/lastpass/help/google-authenticator-lp030015).

**Bitwarden**:

- [https://soka.gitlab.io/blog/post/2021-01-15-la-clave-eres-tu/#Gestoresdecontraseas](https://soka.gitlab.io/blog/post/2021-01-15-la-clave-eres-tu/#Gestoresdecontraseas)
- [https://opensource.com/article/18/3/behind-scenes-bitwarden](https://opensource.com/article/18/3/behind-scenes-bitwarden)
