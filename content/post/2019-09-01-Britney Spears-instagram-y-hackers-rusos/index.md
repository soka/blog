---
title: ¿Que tienen que ver un grupo de espionaje cibernético ruso y la cuenta de Instragram de Britney Spears?
subtitle: Como usan la red social para comunicar con computadoras infectadas con malware
date: 2019-09-01
draft: false
description: Técnicas para esconder mensajes en redes sociales
tags: ["Malware","Seguridad","Instagram","Turla","bit.ly","Firefox","Watering Hole","Redes Sociales"]
---

![](images/01.png)

**Comentarios en los perfiles de [Instagram](https://www.instagram.com/) de famosos podrían haber sido usados para mandar instrucciones a computadoras infectadas con malware**, uno de los perfiles usado parece ser del de [Britney Spears](https://www.instagram.com/britneyspears/) con 22 millones de seguidores, usando los comentarios de sus seguidores **con un patrón especial en sus publicaciones**. 

![](images/britney_instagram_square-768x572-671x500.jpg)

El comentario marcado en la captura de arriba podría parecer SPAM o un mensaje sin sentido pero esconde algo muy especial.

Partimos de una serie de computadoras infectadas con un malware que forman una [botnet](https://es.wikipedia.org/wiki/Botnet) que funcionan de forma autónoma y automática pero que deseamos controlar (por ejemplo para coordinar ataques de denegación de servicio y tumbar un sitio Web), **estas aplicaciones maliciosas se controlan mediante instrucciones que obtienen de servidores Web C&C** ([command and control](https://en.wikipedia.org/wiki/Command_and_control_(malware))), aplicando ingenieria inversa sobre el malware sería relativamente "sencillo" obtener esta lista de servidores, ocultando las direcciones en mensajes de la red social [Instagram](https://www.instagram.com/) se complica el tema. 

**El malware comienza buscando comentarios cuyo [hash](https://es.wikipedia.org/wiki/Funci%C3%B3n_hash) o suma de comprobación sea igual a 183**.

Este es uno de los comentarios en cuestión "#2hot make loveid to her, uupss #Hot #X" publicado el 6 de Febrero que **contiene un enlace oculto a la siguiente dirección [http://bit.ly/2kdhuHX](http://bit.ly/2kdhuHX)**. [Bit.ly](https://bitly.com/) es un servicio muy popular para acortar URLs, se usa en muchas redes sociales para no exceder el límite máximo de caracteres por mensaje por ejemplo. Actualmente el enlace de arriba se encuentra ya bloqueado por bit.ly pero aún se puede consultar la dirección final a la que apunta (ya no existe): [http://static.travelclothes.org/dolR_1ert.php](http://static.travelclothes.org/dolR_1ert.php)  

Para obtener la dirección Web del comentario se aplica una [expresión regular](https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular): `(?:\\u200d(?:#|@)(\\w)`, si la analizamos con detenimiento se aprecia que busca el carácter unicode UTF ["0x200D"](https://www.fileformat.info/info/unicode/char/200d/index.htm) llamado [Zero Width Joiner (ZWJ)](https://en.wikipedia.org/wiki/Zero-width_joiner), este carácter especial no se imprime en el mensaje final ('de ancho nulo') y se suele usar para separar dos emojis, en formato HTML se escribe como `&#8205;`.

El malware busca entonces cada carácter del mensaje prececido por el carácter ZWJ para formar la URL, marcados en negrita "#**2**hot ma**k**e lovei**d** to **h**er, **u**upss #**H**ot #**X**" forman la URL [http://bit.ly/2kdhuHX](http://bit.ly/2kdhuHX).



## Conclusiones

No es casual que hayan elegido una plataforma como [Instagram](https://www.instagram.com/) con millones de usuarios y que genera una gran cantidad de tráfico de datos a modo de posts díficil de controlar, ademas que este tipo de ataque se apoye en cuentas de otros usuarios de la red social lo complica aún más.

Es la compañia de seguridad ESET quien ha puesto en conocimiento este ejemplo concreto y en los posts se señala al grupo Turla ("se cree es el ciber-brazo de la inteligencia rusa") de estar detrás de este malware.

El malware en si mismo parece que tenía origen en una extensión del navegador Firefox llamada “HTML5 Encoding 0.3.7” (colgado en el sitio comprometido de una empresa de seguridad suiza) con una puerta trasera que permitía recopilar información del sistema, ejecutar aplicaciones o subir y descargar ficheros del servidor C&C entre otras cosas. 

![](images/extension_file.jpg)

Parece que este sistema de comunicación con el malware ha sido una prueba ya que al usar el servicio acortador de URLs [Bit.ly](https://bitly.com/) ha permitido saber el número de veces usado que era 17 veces en el momento de ser descubierto, aunque otra teoría apunta que podía estar dirigido a objetivos muy concretos.


## Enlaces externos

- welivesecurity.com ["Campaña Watering Hole de Turla: extensión para Firefox se ..."](https://www.welivesecurity.com/la-es/2017/06/06/campana-watering-hole-de-turla-instagram/).
- adslzone.net ["Así escondían mensajes los hackers rusos en el perfil de Britney Spears"](https://www.adslzone.net/2017/06/07/asi-escondian-mensajes-los-hackers-rusos-en-el-perfil-de-britney-spears/).
- xataka.com ["Cuando hackers rusos usaron los comentarios de la cuenta de Instagram de Britney Spears para comunicarse"](https://www.xataka.com/seguridad/hackers-rusos-usaban-los-comentarios-de-la-cuenta-de-instagram-de-britney-spears-para-comunicarse).
- engadget.com ["Russian malware link hid in a comment on Britney Spears' Instagram"](https://www.engadget.com/2017/06/07/russian-malware-hidden-britney-spears-instagram/).
- ["LightNeuron: backdoor del grupo de espionaje Turla se esconde en ..."](https://www.welivesecurity.com/la-es/2019/05/06/lightneuron-backdoor-turla-esconde-pdf-jpeg/).
- incibe-cert.es ["APT Turla: malware de espionaje | INCIBE-CERT"](https://www.incibe-cert.es/alerta-temprana/bitacora-ciberseguridad/turla-es).
- tripwire.com ["Turla Using Instagram Comments to Obtain C&C Servers - Tripwire"](https://www.tripwire.com/state-of-security/latest-security-news/turla-using-instagram-comments-obtain-cc-servers/).
- 401trg.com ["Turla Watering Hole Campaigns 2016/2017"](https://401trg.com/turla-watering-hole-campaigns-2016-2017/)