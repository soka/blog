---
title: Google Clasroom
subtitle: Plataformas de aprendizaje virtual
date: 2020-05-11
draft: false
description: 
tags: ["Aplicaciones","Google Classroom"]
---

El acceso habitual es usando este enlace URL: [https://classroom.google.com/](https://classroom.google.com/).

## Elementos del entorno de trabajo

La primera vez que accedamos la pantalla principal está vacía, poco a poco la iremos llenando con cursos propios y otros en los que hayamos participado.

![](img/01.png)

En la esquina superior izquierda podemos acceder al menú con opciones donde podemos visualizar un calendario con nuestras clases programadas:

![](img/02.png)

Si estas participando en un curso también se mostrará otra opción donde consultar los pendientes. Más abajo en el mismo menú se encuentran los ajustes de la aplicación donde activar las notificaciones:

![](img/03.png)

## Creando un curso

Tenemos dos opciones, acceder a un curso mediante un código que nos hayan proporcionado o crear nuestro propio curso, previamente antés de crear nuestro primer curso debemos aceptar las condiciones de Google.

Creamos nuestra primera clase:

![](img/04.png)

## Organizar cursos por unidades o temas

Ya tenemos nuestro curso creado, podemos seleccionar una imagen representativa del curso entre los disponibles o subir una imagen propia.

![](img/11.png)

En el lateral izquierdo debajo de la presentación del curso podemos ver los siguientes eventos del curso.

Podemos **crear publicaciones, compartir información y preguntas con el resto de la clase usando el hilo de noticias**, como profesor puedo configurar si sólo yo puedo añadir publicaciones o permitir sólo comentarios de los alumnos a lo que yo publique. También puedo seleccionar los estudiantes a los que va dirigida la noticia y adjuntar archivos, enlaces o vídeos de Yotube. El estudiante recibirá un correo electrónico con la notificación de la nueva noticia.

Para organizar el curso por actividades o sesiones debemos ir a la opción "Classwork" o trabajo de clase.

![](img/06.png)

Una vez dentro podemos usar el botón para crear una nueva sección de nuestro curso.

![](img/07.png)

Creamos un nuevo tema (_topic_) con un nombre "Tema 1".

![](img/08.png)

Una vez dentro se pueden crear nuevos elementos, he creado varios temas, a continuación los ordeno arrastrando y soltando para que se muestren en el orden correcto al alumno.

![](img/09.png)

## Añadir archivos

Ahora veremos como compartir materiales que ya tengo preparados con los alumnos, para ello volvemos a usar el botón para crear un material, creamos un contenido de ejemplo con el resumen de los temas a tratar en el curso y lo asociamos a la primera sesión, aunque no es imprescindible. Podemos añadir un archivo de la computadora o cogerlo directamente de Google Drive. Se puede programar la publicación de los materiales para que coincidan por ejemplo con la fecha y hora de de una sesión.

![](img/12.png)

![](img/13.png)

## Como asignar una tarea

En este caso usamos el botón crear y la opción tarea, los alumnos deberán entregar o subir algún material para completarla, podemos asignar el peso específico de la tarea como puntos que se calificarán, se puede definir una fecha límite de entrega y asignar a todos o algunos estudiantes la tarea. La tarea puede ir asociada a un tema, en este caso también se pueden anexar archivos, enlaces o vídeos. Una vez rellenados todos los datos hacemos clic en "Crear". También se puede asociar una rubrica de evaluación de la tarea.

![](img/14.png)

![](img/15.png)

Ahora el alumno podrá ver que tiene una fecha de entrega y añadir su trabajo como archivo adjunto asociado a la tarea. 

## Organizar una encuesta

## Funcionalidades generales

- Materiales de clase: Cada clase genera una carpeta de Google Drive de la clase.
- Tablón de anuncios: Tiene un aspecto similar al timeline de las RRSS con este formato de presentación de contenidos con diferentes formatos ordenados según su publicación más reciente. El profesor puede escribir allí anuncios, publicarlos al momento o programarlos, para que los alumnos tengan en cuenta los avisos que allí se publiquen. Estos avisos pueden ser respondidos por los alumnos en caso de que haya alguna duda al respecto
- Tareas
- Conexión con Google Calendar
- Conexión con APPs externas: FlipGrid, Geogebra, Khan Academy, Mathgames, Opened, Quizlet, Duolingo o Edulastic....


## Conclusiones

La ventaja evidente de Google Classroom es que se puede combinar con el resto de utilidades de G Suite como son los formularios, el calendario, las Webs de Google Sites o el propio Google Drive para subir materiales. Ademas no hay que invertir en mantenimiento ni actualización de versiones, copias de seguridad, etc.

Tiempo de aprendizaje corto para aprender a usarla.

De momento Classroom **no permite trabajar con subgrupos**, lo que sí permite es publicar un anuncio o tarea que solo sea visible por algunos alumnos (los que tu selecciones).

Otro dato a tomar en cuenta es que **no sigue ningún estándar** como SCORM o los nuevos CMI5+xAPI.

Las clases en vivo no están integradas en la plataforma aunque podemos usar Hangouts o Meet.

El servicio no es nuestro, Google puede decidir cambiar los términos del acuerdo en cualquier momento. 

El negocio de Google son nuestros datos, sin embargo en Moodle somos los responsables directos para bien o para mal.

## Enlaces externos

- https://www.youtube.com/watch?time_continue=21&v=lzqex0u7850&feature=emb_logo
- https://new.edmodo.com/?go2url=%2Fhome
- Schoology
- Canvas LMS 
- Chamilo LMS
- Sakai
- eDucativa.

