---
title: Instala una red social en tu Raspberry Pi
subtitle: Borrador de ideas y fuentes de información  
date: 2019-01-20
draft: true
description: Intancia de Mastodon corriendo en una RPi
tags: ["Raspberry Pi","RPi","Mastodon","Pleorama"]
---

https://github.com/wimvanderbauwhede/limited-systems/wiki/Pleroma-and-Mastodon-on-the-Raspberry-Pi-3

https://hub.docker.com/r/gilir/rpi-mastodon/

https://www.raspberrypi.org/forums/viewtopic.php?t=208880

https://opensource.com/article/17/4/guide-to-mastodon

