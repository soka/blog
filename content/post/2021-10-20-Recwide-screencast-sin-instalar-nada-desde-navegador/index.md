---
title: Recwide - graba la pantalla o la webcam sin necesidad de programas adicionales
subtitle: ¡Gratuito, sencillo de usar en Web y sin registro previo!
date: 2021-10-20
draft: false
description: ¡Gratuito, sencillo de usar en Web y sin registro previo!
tags: ["Herramientas","Recwide","Web","Online","Aplicaciones","Screen recorder","Webcam"]
---

<!-- vscode-markdown-toc -->
* 1. [Enlaces internos](#Enlacesinternos)
* 2. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

![](img/01.PNG)

[**Recwide**](https://www.recwide.com/) es una aplicación Web que nos puede sacar de un apuro en un momento dado para grabar un vídeo sencillo de nuestra pantalla y/o Webcam, su uso es super sencillo y además no requiere registrarse previamente como usuario, el resultado final se puede descargar en MP4 para enviarlo por correo o subirlo a cualquier sitio.

Las opciones que ofrece son muy sencillas cuando entramos: Combinar en un vídeo sólo lo que vemos en la pantalla y la imagen de la Webcam o solo uno de los dispositivos.

![](img/02.PNG)

Para poder empezar a grabar el navegador solicita permisos de acceso a la Webcam y/o al micrófono, podemos grabar el audio del sistema junto con nuestra voz a través del micrófono.

![](img/03.PNG)

Poco más hay que decir, sencillo de usar, con opciones limitadas pero suficientes cuando no necesitamos una aplicación vídeo de calidad profesional con opciones de edición, para algo más avanzado yo recomiendo OBS Studio por ejemplo.

##  1. <a name='Enlacesinternos'></a>Enlaces internos

* [Herramientas para teletrabajar de forma cómoda y sencilla](https://soka.gitlab.io/blog/post/2020-06-26-jornada-teletrabajo/).
* [Trabajo remoto y cuidado de equipos](https://soka.gitlab.io/blog/post/2020-07-14-trabajo-remoto-coordinadora-ong/).
* [Guía para el Uso soluciones tecnológicas de Teletrabajo](https://soka.gitlab.io/blog/post/2020-07-20-inplantariak-teletrabajo/).
* [Organiza un Webinar profesional en línea](https://soka.gitlab.io/blog/post/2020-12-11-obs-webinar/).

##  2. <a name='Enlacesexternos'></a>Enlaces externos

* [Cómo grabar la pantalla del ordenador sin programas](https://eju.tv/2021/10/como-grabar-la-pantalla-del-ordenador-sin-programas/).
* [CÓMO GRABAR LA PANTALLA DEL ORDENADOR SIN PROGRAMAS](https://wwwhatsnew.com/2021/10/18/como-grabar-la-pantalla-del-ordenador-sin-programas/).