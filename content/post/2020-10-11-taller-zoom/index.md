---
title: Curso de Zoom [borrador]
subtitle: Aprende a usar la popular plataforma de videoconferencias
date: 2020-10-11
draft: false
description: Aprende a usar la popular plataforma de videoconferencias
tags: ["Zoom","Vídeo","Videollamadas","aplicaciones","herramientas","talleres","cursos"]
---

## Registro en la plataforma

En la Web [https://zoom.us/](https://zoom.us/) podemos registrarnos de forma gratuita con algunas limitaciones.

[**Zoom**](https://zoom.us/) ofrece diferentes [planes de pago](https://zoom.us/pricing) con funcionalidades avanzadas si contratamos una licencia de pago. **Para este curso he usado una cuenta Pro** que permite celebrar reuniones de hasta 100 participantes, transmisión en streaming y 1GB almacenamiento de las grabaciones en la nube. La cuenta gratuita tiene una importante limitación, las reuniones tienen una duración máxima de 40 minutos.

![](img/01.png)

## Funciones principales de Zoom

### Descarga e instalación

Lo primero es descargar la aplicación e instalarla, para ello vamos al menú "RECURSOS" y dentro seleccionamos "Descargar el cliente de Zoom". Zoom es multiplataforma, está disponible para Windows, Linux, Android, iPads, etc. **También ofrecen extensiones para Chrome y Firefox para programar reuniones**.

![](img/02.jpg)

Una vez descargado procedemos a instalarlo, así de fácil es instalar [**Zoom**](https://zoom.us/). Cuando lo ejecutamos tenemos una pequeña ventana donde podemos entrar a una reunión o también podemos ingresar para generar nuestra propia reunión.

![](img/03.jpg)

### Registrarse en la plataforma

Después de realizar la descarga correspondiente y haber instalado la aplicación en nuestra computadora procedemos a registrarnos dentro de la plataforma. Hacemos clic en el logo de [**Zoom**](https://zoom.us/) para ir a la ventana principal de la Web y registrarnos de forma gratuita introduciendo una dirección de correo electrónico.

![](img/04.jpg)

También podemos registrarnos desde la aplicación usando la opción "Ingresar":

![](img/05.jpg)

Si ya contamos con una cuenta podemos autenticarnos, si es para registrarnos hacemos clic en "Regístrese gratuitamente" que nos abrirá una ventana en el navegador Web donde debemos confirmar nuestra fecha de nacimiento.

![](img/06.jpg)

A continuación introducimos nuestra dirección de correo electrónico, podemos usar una cuenta de Google o Facebook como método de autenticación externa.

![](img/07.png)

Recibiremos un correo para activar nuestra cuenta de [**Zoom**](https://zoom.us/) 

![](img/08.png)

Si queremos podemos invitar a otra gente para que creen su cuenta gratuita o omitir este paso.

![](img/09.png)

Al final nos ofrece iniciar una reunión de prueba, por el momento omitimos este paso y vamos a nuestra cuenta.

![](img/10.png)

### Programar reuniones, clases, videoconferencias

Nos vamos al panel del lado izquierdo y hacemos clic en la opción "Reuniones" donde se muestran diferentes elementos como las reuniones que tenemos programadas, aquellas que ya hayan pasado, una sala de reunión personal y plantillas de reunión. Debajo podemos ver el listado de las reuniones que hemos programado o iremos programando.

![](img/11.png)

Para programar una nueva reunión hacemos clic en el botón "Programar una reunión" y nos muestra el siguiente formulario:

![](img/12.png)

Introducimos un tema y una descripción (opcional), la fecha y la hora de la reunión y el tiempo de duración (40 minutos para el plan básico gratuito), podemos programar la reunión de forma recurrente. Podemos usar un ID personal o uno generado automáticamente para compartirlo con el resto de participantes. La contraseña de la reunión no permite cambiarla en el plan básico, habilitamos la sala de espera para controlar la gente que quiera entrar. Definimos como queremos que accedan el anfitrión y los participantes, con el vídeo encendido o apagado. Podemos permitir que los participantes entren a una reunión programada antes que el propio anfitrión y esperen. Podemos silenciar a los participantes de forma automática al entrar. Finalmente tenemos la opción de grabar la reunión en la computadora local.

**Con el plan de pago Pro existen funcionalidades añadidas muy interesantes como la de solicitar una inscripción obligatoria al evento**:

![](img/13.jpg)

También con la cuenta Pro podemos designar anfitriones alternativos que tengan cuenta en [**Zoom**](https://zoom.us/).

### Administrar reuniones

Ahora que ya hemos programado una reunión volvemos al listado de próximas reuniones, podemos ver un resumen de los datos de la reunión y tres botones para iniciar, editar o eliminar la reunión. Si hacemos clic sobre el nombre de la reunión podremos acceder a los de la reunión 

![](img/14.jpg)

### Invitar usuarios a una reunión

Para poder invitar a los usuarios o personas que queremos que participen en una reunión tenemos algunos métodos que veremos a continuación. 

Si queremos invitar a otras personas podemos agregar la reunión al calendario de Google, de Outlook o de Yahoo para posteriormente invitar al evento a los usuarios que deseemos desde el calendario.

![](img/15.png)

En la ubicación del evento en el calendario de Google se guarda el enlace a la reunión, añadimos como invitados las personas que deseemos junto con los permisos que les otorguemos. 

![](img/16.png)

Esta vendría a ser la primera forma de como podemos compartir invitar a otros usuarios. 

**Advertencia**: Si eliminamos la reunión en Zoom no se borra en el calendario de Google.

La segunda forma sería a través del enlace:

![](img/17.png)

Si hacemos clic sobre "Copiar invitación" se nos muestra una ventana con toda la información:

![](img/18.png)

Simplemente copiamos la información y podemos compartirla a través de correo electrónico o un mensaje de texto en Whatsapp o Telegram por ejemplo.

### Iniciar reunión e interfaz de Zoom

Para poder iniciar nuestra reunión después de ser programada simplemente hacemos clic en "Iniciar esta reunión" o desde la propia lista con todas las reuniones próximas, se abrirá el programa de Zoom que hemos instalado en nuestra computadora.

![](img/19.jpg)

No salta una ventana para configurar nuestro audio que ya veremos más adelante, por el momento la cerramos.

Esta vendría a ser la ventana principal de la reunión que vamos a realizar con la información principal. 

![](img/20.jpg)

Entre los iconos encontramos uno para "Invitar a otros", esta sería la tercera opción para invitar a otros participantes.

![](img/21.jpg)

En la esquina superior izquierda tenemos un icono que nos muestra información sobre la reunión:

![](img/22.jpg)

Junto al icono de información tenemos otro con forma de escudo de color verde que nos permite acceder a la configuración general de la aplicación, esto lo veremos más adelante.

![](img/23.jpg)

En la barra de control inferior tendremos las opciones de audio y vídeo, la opción de seguridad, los participantes que están dentro de la reunión, un botón para chatear mediante de mensajes de texto, la opción de compartir pantalla o una ventana específica con todos nuestros participantes, la opción de grabar la reunión, la opción de [salas para grupos pequeños](https://support.zoom.us/hc/es/articles/206476093-Introducci%C3%B3n-a-las-salas-para-grupos-peque%C3%B1os) ([Managing Breakout Rooms](https://support.zoom.us/hc/en-us/articles/206476313)) y por último un menú "Más" para retransmitir en vivo en un servicio de streaming.

![](img/24.jpg)

### Configuración de audio y vídeo

Para poder realizar la configuración del audio tenemos varias opciones.

![](img/25.jpg)

Si hacemos clic en el icono "Entrar al audio", podemos probar la salida de sonido y el micrófono, el desplegable permite probar diferentes periféricos.

![](img/26.jpg)

![](img/27.jpg)

![](img/28.jpg)

Si hacemos clic en el escudo verde también llegamos a la configuración del audio. Podemos probar el altavoz y elegir si tenemos varios dispositivos de donde queremos escuchar la reunión, también se puede regular el nivel de salida. De la misma forma para elegir diferentes entrada de micrófono y el nivel de entrada. Asimismo podemos configurar otros parámetros adicionales. 

![](img/29.jpg)

En el caso del vídeo es similar, hacemos clic en la misma opción y nos vamos al apartado de vídeo, podemos seleccionar la cámara si disponemos de más de una, también podemos usar la vista panorámica con proporción 16:9 o el tamaño original. Podemos habilitar calidad HD y otros parámetros. Aquí también tenemos algunas opciones avanzadas.

![](img/30.jpg)

### Compartir pantalla


### Chat


## Administrando los participantes de una reunión

[https://support.zoom.us/hc/en-us/articles/115005759423](https://support.zoom.us/hc/en-us/articles/115005759423).

## Salas para grupos pequeños

[https://support.zoom.us/hc/en-us/articles/206476093-Getting-Started-with-Breakout-Rooms](https://support.zoom.us/hc/en-us/articles/206476093-Getting-Started-with-Breakout-Rooms)


## Votaciones



## Enlaces internos

- [Como programar un evento en streaming](https://soka.gitlab.io/blog/post/2020-09-17-taller-streaming-zoom-youtube/) Usando Zoom Pro y Youtube.
- [Guía para el Uso soluciones tecnológicas de Teletrabajo](https://soka.gitlab.io/blog/post/2020-07-20-inplantariak-teletrabajo/) Inplantariak - SPRI.
- [Trabajo remoto y cuidado de equipos](https://soka.gitlab.io/blog/post/2020-07-14-trabajo-remoto-coordinadora-ong/) Coordinadora de organizaciones para el desarrollo.
- [Herramientas para teletrabajar de forma cómoda y sencilla](https://soka.gitlab.io/blog/post/2020-06-26-jornada-teletrabajo/) Contenidos de la jornada impartida por Imanol Terán.
- [Herramientas de trabajo colaborativo](https://soka.gitlab.io/blog/post/2020-04-07-tabla-aplicaciones-trabajo-colaborativo/) Aplicaciones mensajería, reuniones, videoconferencia, documentación...

## Enlaces externos

- [Tres alternativas a la formalidad de Zoom que buscan fomentar las interacciones naturales en el teletrabajo y el aula online](https://www.genbeta.com/herramientas/tres-alternativas-a-formalidad-zoom-que-buscan-fomentar-interacciones-naturales-teletrabajo-aula-online) - Genbeta.
