---
title: Aplicaciones R para la Web
subtitle: Introducción a Shiny
date: 2019-08-16
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Paquetes", "R - Shiny"]
---

![](images/shiny.png)

## Introducción

[**Shiny**](https://shiny.rstudio.com/) es un paquete desarrollado por **RStudio** que permite crear aplicaciones Web interactivas, la instalación se realiza como es habitual con `library(shiny)`.

**RStudio** facilita crear este tipo de aplicaciones desde el menú ("File > New File > Shiny Web app..."), crea un script app.R y añade un código de ejemplo:

```r
#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI for application that draws a histogram
ui <- fluidPage(

   # Application title
   titlePanel("Old Faithful Geyser Data"),

   # Sidebar with a slider input for number of bins
   sidebarLayout(
      sidebarPanel(
         sliderInput("bins",
                     "Number of bins:",
                     min = 1,
                     max = 50,
                     value = 30)
      ),

      # Show a plot of the generated distribution
      mainPanel(
         plotOutput("distPlot")
      )
   )
)

# Define server logic required to draw a histogram
server <- function(input, output) {

   output$distPlot <- renderPlot({
      # generate bins based on input$bins from ui.R
      x    <- faithful[, 2]
      bins <- seq(min(x), max(x), length.out = input$bins + 1)

      # draw the histogram with the specified number of bins
      hist(x, breaks = bins, col = 'darkgray', border = 'white')
   })
}

# Run the application
shinyApp(ui = ui, server = server)
```

Se puede ejecutar la aplicación desde **RStudio**:

![](images/01.PNG)

**Una aplicación en Shiny tiene dos partes, la interfaz de usuario y el servidor**. El primer paso es crear una función que defina la UI:

```r
ui <- type_of_page()
```

Se pueden crear muchos tipos de páginas, los argumentos de la función determinan la apariencia y funcionalidad de la página, en el ejemplo de arriba usa el _layout_ [`fluidPage`](https://shiny.rstudio.com/reference/shiny/latest/fluidPage.html).

A continuación se definen una serie de instrucciones para el servidor que se ejecutan cuando el usuario interactua con el interfaz.

```r
server <- function(input,output){}
```

Y finalmente la función que une las dos partes:

```r
shinyApp(ui=ui, server=server)
```

Ahora entraré en detalle en cada uno de los componentes de la página de ejemplo que proporciona **Shiny**.

## Interfaz de usuario

Necesito una función que defina el _slider_ y el _input_ (cuando el usuario mueve el _slider_) y otra función que procese la salida:

```r
         sliderInput("bins",
                     "Number of bins:",
                     min = 1,
                     max = 50,
                     value = 30)
```

La función [`sliderInput`](https://shiny.rstudio.com/reference/shiny/latest/sliderInput.html) construye un _widget_ _slider_ que permite al usuario seleccionar un valor dentro de un rango, el primer parámetro "bins" es el identificador que contendrá el número que selecciona el usuario en el _slider_ , más adelante en el servidor se hace referencia a este valor como `input$bins`. El resto de argumentos son fáciles de entender, un título o _label_, valores máximo y mínimo y el valor inicial por defecto. Finalmente reservo un área para el diagrama en el panel principal con `plotOutput("distPlot")`, en este momento la app aún no sabe que tipo de diagrama debe mostrar, sólo conoce su nombre, si ejecutamos la función `fluidPage` (sin `ui <-`) podemos ver que genera una salida HTML.

## El servidor

La primera expresión hace referencia a la salida `output$distPlot` que muestra el diagrama, la variable recibe el contenido de la función [`renderPlot({})`](https://shiny.rstudio.com/reference/shiny/latest/renderPlot.html), esta función permite definir el código que necesitemos entre las llaves, en el ejemplo usa la función [`hist`](https://www.rdocumentation.org/packages/graphics/versions/3.6.1/topics/hist) para crear un histograma (mi propio tutorial básico: ["Histogramas con hist()"](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-basicos-hist/))

```r
   output$distPlot <- renderPlot({
      # generate bins based on input$bins from ui.R
      x    <- faithful[, 2]
      bins <- seq(min(x), max(x), length.out = input$bins + 1)

      # draw the histogram with the specified number of bins
      hist(x, breaks = bins, col = 'darkgray', border = 'white')
   })
```

[faithful](https://stat.ethz.ch/R-manual/R-patched/library/datasets/html/faithful.html) es un _dataset_ que contiene el tiempo entre erupciones y su duración del geyser de Yellowstone, contiene un _dataframe_ con 272 observaciones y 2 variables, `faithful[, 2]` contiene el tiempo en minutos hasta la siguiente erupción.

```r
>      head(faithful)
  eruptions waiting
1     3.600      79
2     1.800      54
3     3.333      74
4     2.283      62
5     4.533      85
6     2.883      55
```

Con la segunda variable o columna genera un secuencia de números para alimentar el [histograma](https://www.rdocumentation.org/packages/graphics/versions/3.6.1/topics/hist) con la función [`seq`](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/seq):

```r
seq(from = 1, to = 1, by = ((to - from)/(length.out - 1)),
    length.out = NULL, along.with = NULL, …)
```

Usa los valores máximo y mínimo de `x` como límites de los valores de la secuencia a generar, el tercer parámetro indica el número de valores o longitud de la secuencia definido por el usuario en el _slider_ del UI.

```r
bins <- seq(min(x), max(x), length.out = input$bins + 1)
```

Finalmente dibuja el histograma, el primer parámetro son los valores y el segundo indica el número de clases en los que se agrupan:

```r
hist(x, breaks = bins, col = 'darkgray', border = 'white')
```

## Últimos pasos

Ahora solo queda asociar el interfaz de usuario al servidor con [`shinyApp`](https://shiny.rstudio.com/reference/shiny/1.3.1/shinyApp.html):

```r
shinyApp(ui = ui, server = server)
```

En la consola podemos observar como el servidor está levantado en el equipo local y en un puerto esperando que el usuario interactue con la aplicación, también podemos abrirla en cualquier navegador Web.

```r
> runApp('~/GitLab/r/paquetes/shiny/Shiny-01')

Listening on http://127.0.0.1:3570
```

## Código del ejemplo

Hasta ahora el código fuente es el mismo que genera Shiny como ejemplo:

- r / paquetes / shiny / [Shiny-01](https://gitlab.com/soka/r/tree/master/paquetes/shiny/Shiny-01).

## Enlaces externos

- ["Histogramas con hist()"](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-basicos-hist/).
- [faithful](https://stat.ethz.ch/R-manual/R-patched/library/datasets/html/faithful.html): Waiting time between eruptions and the duration of the eruption for the Old Faithful geyser in Yellowstone National Park, Wyoming, USA.
- [`hist`](https://www.rdocumentation.org/packages/graphics/versions/3.6.1/topics/hist)
- [`renderPlot`](https://shiny.rstudio.com/reference/shiny/latest/renderPlot.html).
- [`sliderInput`](https://shiny.rstudio.com/reference/shiny/latest/sliderInput.html)
- [`fluidPage`](https://shiny.rstudio.com/reference/shiny/latest/fluidPage.html).
- [`shinyApp`](https://shiny.rstudio.com/reference/shiny/1.3.1/shinyApp.html): Create a Shiny app object.
