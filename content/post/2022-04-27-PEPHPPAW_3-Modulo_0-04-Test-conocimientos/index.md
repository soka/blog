---
title: Test de conocimientos 0
subtitle: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 0
date: 2022-04-27
draft: false
description: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 0
tags: ["PEPHPPAW_3","Cursos","Miriadax","Programación","Web","Desarrollo","HTML","CSS","Bootstrap"]
---

La programación en el ámbito de la informática se define como:

* Conjunto de operaciones matemáticas
* **Codificación mediante un lenguaje de programación, que parte de un algoritmo previamente diseñado**
* Relación entre un cliente y un servidor
* Ejecución de flujogramas


El desarrollo web del lado del cliente involucra tecnologías web como:

* Java, C, C#
* PHP, Visual, Ruby
* **JavaScript, HTML, CSS**
* MySQL, MariaDB, Python

Los algoritmos son usados en la ingeniería con mucha frecuencia, su definición acertada es:

* **Conjunto de instrucciones ordenadas para la solución de un problema o tarea**
* Lenguaje que permite definir operaciones algebraicas
* Sentencias de código que permiten insertar registros
* Sentencias de código que permiten mostrar en pantalla un valor

El framework Bootstrap es usado especialmente para:

* **Diseño web**
* Programación web
* Conexión con bases de datos
* Facilitar el trabajo en PHP

Para escribir el código de HTML es necesario:

* **Bloc de notas**
* Procesador de texto
* Hoja de cálculo
* Navegador web

Identifique la etiqueta correcta para aplicar negrita a un texto en HTML

* **Etiqueta "b"**
* Etiqueta "p"
* Etiqueta "u"
* Etiqueta "a"

Bootstrap se usa en diseño web, por lo cual corresponde a un:

* Lenguaje de alto nivel
* Lenguaje de bajo nivel
* Lenguaje de programación
* **Framework o marco de desarrollo**

El lenguaje de estilo en cascada CSS permite

* **Aplicar estilos**
* Definir un documento
* Programar una clase
* Programar una función

El desarrollo web del lado del servidor, involucra tecnologías como:

* JavaScript, HTML, Bootstrap
* CSS, Perl, Visual
* Visual C#, Java, C++
* **PHP, MySQL**

Identifique la etiqueta correcta para definir un elemento párrafo en HTML

* Etiqueta "b"
* **Etiqueta "p"**
* Todas las anteriores
* Ninguna de las anteriores




