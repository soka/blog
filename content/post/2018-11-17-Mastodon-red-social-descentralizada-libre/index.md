---
title: Mastodon
subtitle: Abraza la red social descentralizada y libre
date: 2018-11-17
draft: false
description: Mastodon es una red social federada basada en nodos autónomos
tags: ["Aplicaciones", "Redes Sociales","Mastodon"]
---

![](images/mastodon.jpg)

Al hilo de un [artículo](https://www.berria.eus/paperekoa/1879/035/002/2018-11-17/mamut_bat_dabil.htm) (Gorka Julio - 17/11/2018) muy interesante publicado en [Berria](https://www.berria.eus/) donde anuncian la apertura de un nuevo nodo en [**Mastodon**](https://mastodon.social/about) dedicado a la comunidad de vascoparlantes [mastodon.eus](https://mastodon.eus/about) he querido investigar un poco más de que va esta red social que suena en todos lados. La presencia de nuestra lengua materna en el basto mar de Internet es pequeña en comparación con otras lenguas dominantes en el mundo pero se abre paso en **Mastodon** gracias a que **esta red es descentralizada y libre**, este anuncio describe perfectamente lo que es o lo que no es, este tipo de iniciativas probablemente es imposible en Twitter u otras plataformas de microblogging, tal vez porque no es rentable o simplemente porque su diseño ni siquiera lo permite en muchos casos.

![](images/mastodon-social-main-web.PNG)

**Mastodon** nace como una plataforma de microblogging alternativa a Twitter en 2016 bajo el dominio [mastodon.social](mastodon.social), se caracteriza por ser una **red federada de la aplicación distribuida en diferentes servidores**, su código fuente está alojado en [GitHub](https://github.com/tootsuite/mastodon) de forma pública y libre, lo que hace que sea posible instalar nuestra propia instancia en nuestra máquina. Las diferentes instancias pueden interactuar entre si creando una red descentralizada de comunidades autonómas entre si.

![](images/topologias_01.png)

Los usuarios interactuan mediante **mensajes o "toots" de hasta 500 carácteres o publicando contenido multimedia**, las **menciones a otros usuarios** están disponibles y las **etiquetas**. Cada instancia puede estar formada por ejemplo por un grupo de personas con una afición común, el creador o administrador de la instancia es quien define las políticas de uso y regula la interacción con otras instancias.

Después de esta palabrería insustancial me he abierto una cuenta, en **Mastodon** abrimos una cuenta asociada a una instancia ([ver lista](https://instances.social/list#lang=&allowed=&prohibited=&users=)) por eso nuestro identificador estará formado por el nombre de usuario y la instancia, en mi caso por ejemplo es [@popu@mastodon](https://mastodon.social/@popu) (para comunicarnos dentro de una instancia es suficiente con usar el nombre).

![](images/mastodon-interface-main.PNG)

El usuario puede establecer si permite ser seguido públicamente o previo consentimiento. Para relacionarse se emplea el término "local" para los usuarios registrados en la misma instancia y "federeada" para ser mostrados en toda la red.

Lo primero que hacemos nada más entrar como es lógico es buscar alquien a quien seguir (también podemos buscar etiquetas), el buscador permite buscar otras cuentas. También es interesante revisar nuestro perfil y completarlo.

![](images/seguir.PNG)


# Tecnologías detras de Mastodon

**Mastodon** esta desarrollado a partir de [GNUSocial](https://gnusocial.net/main/public), otra red de microbloging, está escrito en Ruby on Rails, [React.js](https://reactjs.org/) y [Redux](https://es.redux.js.org/). Para manejar los datos usa [PostgreSQL](https://www.postgresql.org/) y [Redis](https://redis.io/), [OStatus](https://es.wikipedia.org/wiki/OStatus) como protocolo.

# Mastodon móvil

**Mastodon** está disponible para navegadores Web y móviles, para Android existe [Tusky](https://tuskyapp.github.io/).

![](images/Tusky-login.jpeg)

# Enlaces externos

* Wikipedia ["Mastodon (red social)"](https://es.wikipedia.org/wiki/Mastodon_(red_social)).
* Berria ["Mamut bat dabil"](https://www.berria.eus/paperekoa/1879/035/002/2018-11-17/mamut_bat_dabil.htm) - Gorka Julio.
* Xakata ["Mastodon, así es la alternativa a Twitter que evita la censura gracias a la descentralización"](https://www.xataka.com/servicios/mastodon-asi-alternativa-a-twitter-que-evita-censura-gracias-a-descentralizacion).
* Xataka ["Qué es Mastodon, cómo funciona y cómo te puedes registrar"](https://www.xataka.com/basics/que-mastodon-como-funciona-como-te-puedes-registrar).
* ["¿TE ANIMAS A PROBARLO?
¿Qué demonios es Mastodon y por qué todo el mundo está hablando de ello?"](https://www.elconfidencial.com/tecnologia/2018-10-02/mastodon-que-es-twitter-libre-descentralizado_1624125/).
* ["Mastodon, el ‘Twitter libre’, tiene ya un millón y medio de usuarios"](https://www.elsaltodiario.com/redes-sociales/mastodon-el-twitter-libre-tiene-ya-un-millon-y-medio-de-usuarios).
* ["Tres aplicaciones para usar Mastodon en Android y iOS"](https://www.xatakamovil.com/aplicaciones/tres-aplicaciones-para-usar-mastodon-android-ios).
* ["Mastodon, el "Twitter libre", tiene ya un millón y medio de usuarios"](https://www.internautas.org/html/10101.html).