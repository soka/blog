---
title: Mastodon-eko "toots"-ak nola txio bihurtu automatikoki
subtitle: IFTTT serbitzua erabilita
date: 2018-11-18
draft: false
description: Eskala eta sare ekonomiak
tags: ["Twitter","Mastodon","IFTTT","Euskaraz"]
---

![](images/ifttt.png)

Espero nuena baino errezago izan da **[Mastodon](https://mastodon.social/)-eko "toots" Twitter-eko txio bihurtzea automatikoki**, [Yeray](https://mastodon.eus/@Yeray) erabiltzaileak egin duela ikusi dut eta jakinmina piztu zait, lehendik baneukan [IFTTT](https://ifttt.com/discover) Webgunean kontua zabalduta, izenak dioen bezala "IF THIS THEN THAT" ("Baldin HAU, orduan HURA"), serbitzu honek egiten duena da: Aplikazio batek sortzen duen gertakari bat ematen denean (adibidez "toot" bat argitaratu Mastodon-en) orduan IFFT-ek zeozer gerta dadin ahalbidetzen du (adibidez txio bat bidali). 

![](images/applet-conf.PNG)

IFTTT-ek badu prest "applet" bat hau egiteko, [Mastodon → Twitter](https://ifttt.com/applets/zVRwYKr4-mastodon-twitter). Konfiguratzea oso erraza da, aktibatu eta behean Atom "feed" baten helbidea jartzen dugu, bertatik hartzen ditu gure mezuak, nabigatzailean frogatu, nire kasuan hau da [https://mastodon.eus/@ikerlandajuela.atom](https://mastodon.eus/@ikerlandajuela.atom), XML fitxategian beherago bagoaz hor daude nire mezu publiko guztiak.

```
<?xml version="1.0"?>
<feed xmlns="http://www.w3.org/2005/Atom" xmlns:thr="http://purl.org/syndication/thread/1.0" xmlns:activity="http://activitystrea.ms/spec/1.0/" xmlns:poco="http://portablecontacts.net/spec/1.0" xmlns:media="http://purl.org/syndication/atommedia" xmlns:ostatus="http://ostatus.org/schema/1.0" xmlns:mastodon="http://mastodon.social/schema/1.0">
  <id>https://mastodon.eus/users/ikerlandajuela.atom</id>
  <title>Iker Landajuela</title>
  <subtitle>Ezina ekinez egina, egunero gauza berriak ikasten</subtitle>
  <updated>2018-11-19T15:33:45Z</updated>
  <logo>https://mastodon.eus/system/accounts/avatars/000/001/030/original/21bc4d7002c182a4.jpg?1542535270</logo>
  <author>
    <id>https://mastodon.eus/users/ikerlandajuela</id>
    <activity:object-type>http://activitystrea.ms/schema/1.0/person</activity:object-type>
    <uri>https://mastodon.eus/users/ikerlandajuela</uri>
    <name>ikerlandajuela</name>
    <email>ikerlandajuela@mastodon.eus</email>
    <summary type="html">&lt;p&gt;Ezina ekinez egina, egunero gauza berriak ikasten&lt;/p&gt;</summary>
    <link rel="alternate" type="text/html" href="https://mastodon.eus/@ikerlandajuela"/>
    <link rel="avatar" type="image/jpeg" media:width="120" media:height="120" href="https://mastodon.eus/system/accounts/avatars/000/001/030/original/21bc4d7002c182a4.jpg?1542535270"/>
    <poco:preferredUsername>ikerlandajuela</poco:preferredUsername>
    <poco:displayName>Iker Landajuela</poco:displayName>
    <poco:note>Ezina ekinez egina, egunero gauza berriak ikasten</poco:note>
    <mastodon:scope>public</mastodon:scope>
  </author>
  <link rel="alternate" type="text/html" href="https://mastodon.eus/@ikerlandajuela"/>
  <link rel="self" type="application/atom+xml" href="https://mastodon.eus/users/ikerlandajuela.atom"/>
  <link rel="hub" href="https://mastodon.eus/api/push"/>
  <link rel="salmon" href="https://mastodon.eus/api/salmon/1030"/>
  <entry>
    <id>https://mastodon.eus/users/ikerlandajuela/statuses/101098561569759829</id>
    <published>2018-11-19T15:33:45Z</published>
    <updated>2018-11-19T15:33:45Z</updated>
    <title>New status by ikerlandajuela</title>
    <activity:object-type>http://activitystrea.ms/schema/1.0/note</activity:object-type>
    <activity:verb>http://activitystrea.ms/schema/1.0/post</activity:verb>
    <link rel="alternate" type="application/activity+json" href="https://mastodon.eus/users/ikerlandajuela/statuses/101098561569759829"/>
    <content type="html" xml:lang="eu">&lt;p&gt;Ni ere frogatzen nabil nola bidali toots-ak automatikoki Twiter-era IFTTT serbitzua erabilita &lt;a href="https://ifttt.com/applets/zVRwYKr4-mastodon-twitter" rel="nofollow noopener" target="_blank"&gt;&lt;span class="invisible"&gt;https://&lt;/span&gt;&lt;span class="ellipsis"&gt;ifttt.com/applets/zVRwYKr4-mas&lt;/span&gt;&lt;span class="invisible"&gt;todon-twitter&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;</content>
    <link rel="mentioned" ostatus:object-type="http://activitystrea.ms/schema/1.0/collection" href="http://activityschema.org/collection/public"/>
    <mastodon:scope>public</mastodon:scope>
    <link rel="alternate" type="text/html" href="https://mastodon.eus/@ikerlandajuela/101098561569759829"/>
    <link rel="self" type="application/atom+xml" href="https://mastodon.eus/users/ikerlandajuela/updates/1175.atom"/>
    <ostatus:conversation ref="tag:mastodon.eus,2018-11-19:objectId=3874:objectType=Conversation"/>
  </entry>
```

Hau eginda nahiko da, aldiro aldiro IFTTT-ek "applet"-a exekutatuko automatikoki du mezuak berbidalita Mastodon-etik Twiter-er, esaten digu noiz exekutatu den azken aldiz eta zalantzarik badugu "View activity log" aukeran sartu gaitezke ere konfigurazio atalean.

![](images/log.PNG)

Eta hemen dago emaitza! badabil! 

![](images/emaitza.PNG)

# Estekak

* ["Zereginen automatizazioa: Zer da IFTTT?"](http://e-forma.kzgunea.eus/mod/book/view.php?id=5267).
* IFTTT [Mastodon → Twitter](https://ifttt.com/applets/zVRwYKr4-mastodon-twitter).
* Wikipedia ["Atom"](https://eu.wikipedia.org/wiki/Atom).