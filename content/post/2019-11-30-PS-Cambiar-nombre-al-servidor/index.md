---
title: Cambiar el nombre al servidor
subtitle: PowerShell y Windows Server
date: 2019-11-30
draft: false
description: Administración servidores Windows
tags: ["Windows","Sistemas","PowerShell","Administración","cmdlets","Scripting"]
---

![](img/logo2.png)

Aunque se puede hacer con "sconfig" (utilidad para configurar el servidor por línea de comandos) lo voy a hacer con PS:

```ps
Rename-Computer -NewName "ServerCore"
```

Compruebo con el comando `hostname` que aún no ha cambiado, requiere reiniciar el equipo para que tenga efecto, ahora ya sabemos como hacerlo con PS con el cmdlet `Restart-Computer`.


Puedo usar el parámetro `-LocalCredential Administrador` o si estoy dentro de un dominio `-DomainCredential dominio\Administrador`.

Si quiero hacerlo remotamente puedo usar como en otras ocasiones el parámetro `-ComputerName`.


