---
title: Alternativas para compartir archivos [borrador]
subtitle: Herramientas de trabajo colaborativo remoto
date: 2020-05-01
draft: false
description: 
tags: ["Aplicaciones","Internet","Web","Archivos","Transferencia"]
---

El correo electrónico ha sido durante muchos años el sistema predilecto para el intercambio de información, hoy en día tal vez no se trate de la mejor ni única opción, especialmente cuando hablamos de archivos de gran tamaño que nos llenan los buzones de correo o sobrepasan la capacidad permitida de nuestro servicio, o cuando los archivos son de un sólo uso por ejemplo.

Para estos supuestos también existen alternativas gratuitas (algunas de ellas libres) en la Web. 

## Firefox Send

**Enlace**: [Firefox Send](https://send.firefox.com/).

![](img/01.jpg)

La primera propuesta proviene de [**Mozilla**](https://www.mozilla.org/es-ES/), si no quieres registrarte puede **compartir archivos de hasta 1GB**, la transmisión de los datos es cifrada de origen a destino. El **contenido expira cuando se producen determinadas descargas o transcurre cierto tiempo** (5 minutos, 1 hora, 1 día o 7 días), también se puede proteger con una clave. Todas estas opciones son muy útiles si queremos que sólo el destinatario legítimo acceda o una vez descargado el contenido desparezca y ninguna otra persona pueda descargarlo o se reenvíe el enlace.

![](img/02.png)

La única forma de aumentar la capacidad a 2,5GB es si te registras (la misma cuenta que se usa para sincronizar los datos del navegador), además el archivo permanecerá durante 7 días.

El código de [**Firefox Send**](https://send.firefox.com/) está liberado bajo la licencia [Mozilla Public License Version 2.0](https://github.com/mozilla/send/blob/master/LICENSE) y [alojado en GitHub](https://github.com/mozilla/send). La aplicación está desarrollada con Node.js y se puede instalar y poner en marcha con NPM, parece fácil, algún día probare a montarme uno en casa. 

## WebWormHole

**Enlace**: [**WebWormHole**](https://webwormhole.io/#).

![](img/03.png)

[**WebWormHole**](https://webwormhole.io/#) es una propuesta diferente a los sistemas típicos como [**Firefox Send**](https://send.firefox.com/) donde el archivo se aloja en un servidor y el tráfico pasa a través de el. [**WebWormHole**](https://webwormhole.io/#) utiliza el protocolo [WebRTC](https://es.wikipedia.org/wiki/WebRTC) (Web Real-Time Communication) que permite a los navegadores Web comunicación en tiempo real (RTC) entre pares ([P2P](https://es.wikipedia.org/wiki/Peer-to-peer)) de forma nativa sin necesidad de extensiones, ademas WebRTC integra cifrado por defecto para proteger las comunicaciones.

Cuando creamos un nuevo "Wormhole" como la imagen inferior basta mandar el código QR o la URL al receptor para iniciar la transferencia, cada vez que accedamos a la Web nos proporciona un código nuevo. 

![](img/04.png)

La transmisión es bidireccional, el receptor a su vez puede mandar archivos al emisor.

![](img/05.png)

La velocidad de transferencia está condicionada por la de subida del emisor y la de bajada del receptor, aunque tiene la ventaja de que no pasa por servidores intermedios. Una vez abierto el túnel se pueden transferir tantos archivos como queramos mientras se mantengan abiertos los navegadores. Una de sus ventajas es que en teoría no tiene limitaciones de tamaño como [**Firefox Send**](https://send.firefox.com/) y otras alternativas basadas en un servidor central. 

El [código fuente del proyecto](https://github.com/saljam/webwormhole) está alojado en GitHub bajo la licencia BSD que permite su uso, modificación y distribución.

## Riseup Share

**Enlace**: [Riseup](https://riseup.net/es).

[Riseup](https://riseup.net/es) proporciona un servicio de transferencia algo más modesto en [https://share.riseup.net/](https://share.riseup.net/). 

![](img/06.png)

La **capacidad se limita a 50mb y el contenido expira en 12 horas**, los archivos se guardan cifrados en el servidor usando la utilidad [Up1](https://github.com/Upload/Up1) (A Client-side Encrypted Image Host) basada en Node.js.

Para compartirlo con alguien solo debemos enviarle la URL. 

![](img/07.png)

## Framadrop

**Enlace**: [Framadrop](https://lufi.ethibox.fr/).

[**Framadrop**](https://lufi.ethibox.fr/) es un servicio de intercambio de ficheros confidencial y gratuito, está basado en el software libre [Lufi](https://framagit.org/fiat-tux/hat-softwares/lufi). 

![](img/09.png)


[**Framadrop**](https://lufi.ethibox.fr/) está alojado por la fundación Framasoft en una instancia [Ethibox](https://ethibox.fr/).

## YouTransfer.io

**Enlace**: [YouTransfer](https://www.youtransfer.io/).

[**YouTransfer**](https://www.youtransfer.io/) es similar a Firefox Send pero no encuentro instancias publícadas, está ideado para instalarlo en tu servidor privado.

## ProjectSend

**Enlace**: [ProjectSend](https://www.projectsend.org/).

No me voy a detener mucho en esta alternativa, es similar a la anterior para implementarla en un servidor privado. Se puede probar una [demo](https://www.projectsend.org/demo/), 

![](img/08.png)

[**ProjectSend**](https://www.projectsend.org/) se parece más a un gestor de ficheros como [NextCloud](https://nextcloud.com/) con gestión de usuarios, grupos y espacio de almacenamiento para las cuentas.


## Referencias externas

- librezale.eus ["KomunikazioEtaElkarlanerakoTresnak"](https://librezale.eus/wiki/KomunikazioEtaElkarlanerakoTresnak).
- wwwhatsnew.com ["Alternativas para compartir archivos sin depender del correo"](https://wwwhatsnew.com/2020/05/01/alternativas-para-compartir-archivos-sin-depender-del-correo/).
- genbeta.com ["WebWormHole: envía y recibe archivos de gran tamaño rápido y gratis a través del navegador"](https://www.genbeta.com/intercambio-de-ficheros/webwormhole-envia-recibe-archivos-gran-tamano-rapido-gratis-a-traves-navegador).
- hipertextual.com ["Smash te permite enviar archivos sin ningún tipo de límites"](https://hipertextual.com/2018/05/smash-enviar-archivos-grandes).
- wwwhatsnew.com ["Tres opciones para compartir archivos de forma privada usando P2P"](https://wwwhatsnew.com/2011/12/06/tres-opciones-para-compartir-archivos-de-forma-privada-usando-p2p/).
- genbeta.com ["9 páginas gratis para compartir online tus archivos sin depender de la nube"](https://www.genbeta.com/web/9-paginas-gratis-para-compartir-online-tus-archivos-sin-depender-de-la-nube).
- wwwhatsnew.com ["4 herramientas para enviar archivos pesados a través de Internet"](https://wwwhatsnew.com/2017/07/01/4-herramientas-para-enviar-archivos-pesados-a-traves-de-internet/).
- wwwhatsnew.com ["5 sitios web gratuitos para que envíes archivos de gran tamaño"](https://wwwhatsnew.com/2017/07/24/5-sitios-web-gratuitos-para-que-envies-archivos-de-gran-tamano/).
- genbeta.com ["Mozilla lanza una herramienta gratuita que te permite enviar archivos de 2GB que se autodestruyen"](https://www.genbeta.com/web/mozilla-lanza-una-herramienta-gratuita-que-te-permite-enviar-archivos-de-2gb-que-se-autodestruyen).