---
title: Usar servicios Web en PrestaShop v1.7.0.4
subtitle: Webservice
date: 2019-03-04
draft: false
description: Administración PrestaShop
tags: ["PrestaShop","Webservice","eCommerce","Servicios Web","API"]
---

Los **servicios Web (Webservice en adelante)** permiten que aplicaciones de terceros se comuniquen con nuestra tienda en línea de **PrestaShop**, esto abre la posibilidad de crear por ejemplo una aplicación móvil para los clientes de nuestra tienda. Existe un acuerdo o varios estándares de como debe funcionar un Webservice, son muy típicos los basados en [SOAP](https://es.wikipedia.org/wiki/Simple_Object_Access_Protocol) (Simple Object Access Protocol) que intercambia datos basados en la sintaxis XML y los últimos años han ganado popularidad los basados en [REST](https://es.wikipedia.org/wiki/Transferencia_de_Estado_Representacional) (Representational State Transfer) usando el protocolo HTTP (usando los métodos GET, POST, PUT, DELETE, etcétera). 

Cada aplicación **debe tener un Webservice habilitado en PrestaShop**, lo primero es crear una nueva clave en el backend de administración en la sección de configuración, parámetros avanzados > Webservice.

![](http://doc.prestashop.com/download/attachments/54264618/par%C3%A1metrosAvanzados026-ListadoWebservices-es.png?version=1&modificationDate=1492973093000&api=v2) 

[[Fuente](http://doc.prestashop.com/pages/viewpage.action?pageId=54264618)]

Dejamos que **PrestaShop** generé una nueva clave, le damos una descripción y damos permisos a los módulos que necesitemos especificando tipo de operación (borrar es DELETE, ver o consultar es GET...).

Ahora ya podemos hacer la primera prueba, accede a la API de tu tienda de la siguiente forma: "www.tudominio.xxx/dondeestétutienda/api" (o también con "nuestraclave@laUrldemitienda/api/"), en un cuadro de diálogo emergente te pedirá usuario y clave, introduce la clave del Webservice generada previamente como usuario y deja la clave en blanco, si todo va correctamente deberia mostrar algo como la imagen inferior, es un índice con las diferentes llamadas que podemos hacer (según los módulos que hayamos habilitado en el backend al habilitar el Webservice).

![](images/webservice_02.PNG)

Si por ejemplo queremos obtener mediante GET los productos entonces introducimos la URL "www.tudominio.xxx/dondeestétutienda/api/products".

Si por ejemplo queremos un registro de un cliente concreto debería ser algo similar a esto: "www.tudominio.xxx/dondeestétutienda/api/customers/56"

# Enlaces externos 

* wikipedia [Servicio web](https://es.wikipedia.org/wiki/Servicio_web).
* PrestaShop 1.7 …  Configurar los Parámetros Avanzados - ["Webservice (Servicio Web)"](http://doc.prestashop.com/pages/viewpage.action?pageId=54264618).
* ["Cómo activar los Webservices en Prestashop y para qué sirven"](https://www.smythsys.es/8352/activar-los-webservices-prestashop-sirven/).

* [PDF] ["Example for Using the PrestaShop Web Service : CRUD"](http://doc.prestashop.com/download/attachments/720902/CRUD%20Tutorial%20EN.pdf).

**Librerías y paquetes:**

* ["PrestaShop/PrestaShop-webservice-lib"](https://github.com/PrestaShop/PrestaShop-webservice-lib).
* ["kusflo/prestashop-webservice"](https://packagist.org/packages/kusflo/prestashop-webservice): PHP wrapper for PrestaShop Webservices 1.7.x and 1.6.x..
* ["Bukimedia/PrestaSharp"](https://github.com/Bukimedia/PrestaSharp): CSharp .Net client library for the PrestaShop API via web service.

