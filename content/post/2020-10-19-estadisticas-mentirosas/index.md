---
title: Estadísticas engañosas [borrador]
subtitle: 
date: 2020-10-19
draft: true
description: Aprende a usar la popular plataforma de videoconferencias
tags: ["R","R - Diagramas","Programación","Estadística"]
---

> Mark Twain  , "Hay tres tipos de mentiras: mentiras, malditas mentiras y estadísticas"

Este post nace inspirado por el libro ["El enemigo conoce el sistema"](https://www.casadellibro.com/libro-el-enemigo-conoce-el-sistema/9788417636395/9501752) de Marta Peirano, hacia el final del libro se centra en campañas políticas basadas en contenidos virales y _fake news_ que mezclan muy poco de verdad con grandes cantidades de desinformación. Decidí buscar como se puede manipular un diagrama de forma intencionada para conseguir que un mal resultado pueda parecer no tan malo o a la inversa, de paso es una excusa para representar algunos de estos diagramas en R.

## Manipular los límites del eje vertical

Normalmente en un diagrama cartesiano de dos dimensiones la intersección entre los ejes X e Y parten del valor 0, pero esto no tiene porqué ser siempre así, partiendo de los mismos datos voy a generar dos gráficas.

Supongamos que tengo los resultados de unas votaciones muy reñidas y quiero aparentar que la diferencia del partido con mayor número de votos respecto al resto es mucho mayor.

Primero represento la gráfica de la forma más habitual, los ejes X e Y parten de 0.

![](img/02.png)

Así en la segunda gráfica la diferencia entre las alturas o valores de las barras parece mucho mayor que lo que realmente es.

![](img/03.png)

El código fuente del diagrama en R se puede encontrar en este [enlace](https://gitlab.com/soka/r/-/blob/master/intro/100-estadistica-mentirosa/cortar-ejes.R) de mi repositorio de R en GitLab.

En el mundo real se pueden encontrar ejemplos como este diagrama de Fox News sobre un [plan de rebaja impuestos](https://es.qaz.wiki/wiki/Bush_tax_cuts) donde de forma evidente y seguramente intencionada se pretende favorecer al presidente Bush.

![](img/04.png)

Figura. Fuente [https://www.forbes.com/sites/naomirobbins/2012/08/04/another-misleading-graph-of-romneys-tax-plan/#7b0e371c33b8](https://www.forbes.com/sites/naomirobbins/2012/08/04/another-misleading-graph-of-romneys-tax-plan/#7b0e371c33b8).

## Jugando con la escala



## Barras de ancho variable


## Enlaces externos

1. [Why this National Review global temperature graph is so misleading](https://www.washingtonpost.com/news/the-fix/wp/2015/12/14/why-the-national-reviews-global-temperature-graph-is-so-misleading/) - The Washington Post. Menciona un gráfico sobre el aumento de la temperatura publicado por el National Review donde manipula la escala para que los cambios sean apenas imperceptibles.
2. [6 formas en que las estadísticas pueden engañarnos](https://eldefinido.cl/actualidad/mundo/6744/6-formas-en-que-las-estadisticas-pueden-enganarnos/) - El Definido.
3. [The statisticians at Fox News use classic and novel graphical techniques to lead with data](https://simplystatistics.org/2012/11/26/the-statisticians-at-fox-news-use-classic-and-novel-graphical-techniques-to-lead-with-data/). 