---
title: Familia apply
subtitle: Aplicar una función sobre estructuras de datos
date: 2019-08-11
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Básico", "R - Funciones"]
---

![](images/iwnqgrrbls5z.png)

A continuación voy a revisar algunos **métodos para aplicar una función sobre estructuras de datos** (matrices, _data frames_, vectores, listas, etc.) de forma repetitiva. La forma clásica de hacerlo es usar un bucle accediendo a cada elemento y aplicarle una función para transformar los datos, con las funciones de la familia **apply** del paquete base de **R** (apply, lapply , sapply, vapply, mapply, rapply y tapply) se puede simplificar mucho el código y aplicar la función sobre los elementos en una sola línea evitando los bucles. Una función pasada a **apply** puede ser una función agregada como `sum` o `mean` que retornan un número o un escalar como resultado, una función que transforme o seleccione sub-elementos u otras funciones vectorizadas que retornan estructuras más complejas como listas, vectores o matrices por ejemplo.

**R** ya implementa algunas funciones para realizar algunas operaciones matemáticas directamente, **aplicados sobre una matriz devuelven un array**:

```r
# Algunas funciones para aplicar operaciones marginales directamente:
myMatrix <- matrix(1:9,nrow = 3, ncol = 3)
rowSums(myMatrix) # [1] 12 15 18
is.array(myMatrix) # TRUE
# Otras colSums(myMatrix)) , rowMeans() y colMeans().
```

## apply(): Operaciones marginales en matrices

Las **columnas y filas de una matriz** pueden ser tratadas como elementos individuales con [**apply**](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/apply) realizando operaciones para todas sus columnas o filas (_operaciones marginales_), la función **puede retornar un vector, un array, o un lista**.

Veamos como podemos hacerlo con [**apply**](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/apply) y aplicando una función cualquiera (incluso una función propia claro) , creamos una matriz 3x3 y le damos los valores 1..9 (asigna los valores por columnas, por ejemplo `myMatrix[2,1]` contiene un 2 y `myMatrix[3,1]` un 3):

```r
> myMatrix <- matrix(1:9,nrow = 3, ncol = 3)
> myMatrix
     [,1] [,2] [,3]
[1,]    1    4    7
[2,]    2    5    8
[3,]    3    6    9
```

Obtenemos la suma de los elementos de cada fila **como un vector de enteros** `int [1:3] 12 15 18`.

```r
myMatrix.apply <- apply(myMatrix, 1, sum) # Suma filas = [1] 12 15 18
```

```r
> class(myMatrix.apply) # "integer"
[1] "integer"
> #View(mydata.apply)
> str(myMatrix.apply) # int [1:3] 12 15 18
 int [1:3] 12 15 18
> is.vector(myMatrix.apply) # TRUE
[1] TRUE
```

Si en lugar de 1 le pasamos un 2 opera sobre las columnas, otro ejemplo:

```r
#  Generamos 30 observaciones aleatorias que distribuimos en un matriz de 6x5.
myMatrix <- matrix(rnorm(30), nrow=6)
apply(myMatrix, 1, mean) # Media de las filas
apply(myMatrix, 2, mean) # Media de las columnas
```

Ahora con una función propia que recibe un vector y multiplica cada elemento por su posición, usamos [**apply**](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/apply) sobre las columnas de una matriz:

```r
# Ahora con una función propia que multiplica cada
# elemento de un vector por su posición en el mismo (multiplica el vector por otro vector
# creado como una secuencia ordenada de 1 a la longitud total)
myMatrix <- matrix(1:9,nrow = 3, ncol = 3)
myFunc <- function(v) v * (1:length(v))
# probemos la función:
myFunc(c(1, 2, 3)) ## [1] 1 4 9
# Ahora la aplicaremos a todas las columnas de la matriz
apply(myMatrix, 2, myFunc)
```

**El resultado ha sido una matriz**, pero cada una de las columnas de esta nueva matriz es el resultado de aplicar la función `myFunc`, recién creada, a cada una de las columnas de la matriz original:

```r
> apply(myMatrix, 2, myFunc)
     [,1] [,2] [,3]
[1,]    1    4    7
[2,]    4   10   16
[3,]    9   18   27
```

En los primeros ejemplos se le pasa una matriz a **apply** y se aplica una **función agregada** (`sum` o `mean` por ejemplo) sobre cada fila o columna tratada como elementos individuales, en la salida se devuelve un vector unidimensional con el resultado, sin embargo nuestra función personalizada `myFunc` opera con filas o columnas pero su salida es otro vector que compone una matriz a la salida de **apply**.

Aplicar **apply** sobre un vector produce un error ya que **la función espera un array o una matriz**.

Otro **ejemplo** aplicado sobre un _array_:

```r
v1 <- c(1,2,3)
a1 <- array(v1,dim=length(v1))
apply(a1,1,function(x) x*x) # Devuelve un vector [1] 1 4 9
```

Aplicado sobre un _data frame_ retorna un vector:

```r
Edad <- c(56,34,67,33,25,28)
Peso <- c(78,67,56,44,56,89)
Altura <- c(165, 171,167,167,166,181)

PersonasDF<-data.frame(Edad,Peso,Altura)
apply(PersonasDF,2,mean)
```

También podemos aplicarlo sobre las columnas y filas con `MARGIN=c(1,2)`:

```r
> myMatrix <- matrix(1:9,nrow = 3, ncol = 3)
> apply(myMatrix,c(1,2),sqrt)
         [,1]     [,2]     [,3]
[1,] 1.000000 2.000000 2.645751
[2,] 1.414214 2.236068 2.828427
[3,] 1.732051 2.449490 3.000000
```

## lapply(): Aplica una función y retorna una lista

Se diferencia con [**apply**](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/apply) en que:

- **opera con listas, vectores o _data frames_.**
- **devuelve una lista**.

![](images/lapply.png)

Un ejercicio clásico estudiando algoritmos básicos en la universidad es programar una función que permite convertir de grados Fahrenheit a Celsius. La formula de conversión es la siguiente: T(°C) = (T(°F) - 32) × 5/9. Normalmente ejecutaríamos un bucle que recorre un arreglo y llamamos a nuestra función para realizar la conversión para cada elemento, con [**lapply**](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/lapply) es mucho más sencillo y compacto, con una sola orden se aplica la función a cada elemento:

```r
temps <- c(68, 73, 70, 61, 59, 66, 79)

toCelsius <- function(temp,asInt=F){
  if (asInt){
    round(((temp-32) * (5/9)),0)
  } else {
    (temp-32) * (5/9)
  }
}

temps <- lapply(temps, toCelsius, asInt = T) # Retorna una lista de 7 elementos
# [[1]][1] 20    [[2]][1] 23  ....... List of 7
```

Otro ejemplo para ahorrar líneas instalando un conjunto de paquetes definido en un vector:

```r
pkgs = c("dplyr", "ggplot2", "shiny")
install.packages(pkgs)
lapply(pkgs,library,character.only=T,verbose=T)
```

Aplicado sobre el DF que hemos creado con la edad, peso y altura:

```r
Edad <- c(56,34,67,33,25,28)
Peso <- c(78,67,56,44,56,89)
Altura <- c(165, 171,167,167,166,181)
PersonasDF<-data.frame(Edad,Peso,Altura)
Pers <- lapply(PersonasDF,function(Pers) Pers/2) # El resultado es una lista
Pers[['Peso']][1] # Acceso [1] 39
```

Otro ejemplo que demuestra la utilidad de **lapply**:

```r
> movies <- c("SPYDERMAN","BATMAN","VERTIGO","CHINATOWN")
> movies_lower <-lapply(movies, tolower)
> str(movies_lower)
List of 4
 $ : chr "spyderman"
 $ : chr "batman"
 $ : chr "vertigo"
 $ : chr "chinatown"
```

Podemos convertir la lista resultante en un vector con `unlist`:

```r
> unlist(movies_lower)
[1] "spyderman" "batman"    "vertigo"   "chinatown"
```

Otro uso diferente es cuando usamos **lapply** con el operador de selección "[" para obtener porciones de matrices en una lista por ejemplo:

Extrae la segunda columna de cada matriz A,B,C dentro de la lista MyList:

```r
A <- matrix(1:9,nrow = 3)
B <- matrix(10:13,nrow = 2)
C <- matrix(14:19,nrow = 2)
MyList <- list(A,B,C)
lapply(MyList,"[", , 2)
```

![](images/content_content_apply2.png)

## sapply(): La versión amigable de lappy

`sapply()` es un _wrapper_ para `lapply()` y una versión más amigable, **retorna un vector**.

```r
temps.list <- list(68, 73, 70, 61, 59, 66, 79)
# Aunque le pasemos lista devuelve vector
temps.sapply <- sapply(temps.list, toCelsius, asInt = T)
is.vector(temps.sapply) # [1] TRUE
is.list(temps.sapply) # [1] FALSE
```

Ejemplo:

```r
> random <- c("This","is","random","vector")
> sapply(random,nchar)
  This     is random vector
     4      2      6      6
```

## mapply(): multivariate apply

[**mapply**](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/mapply) es un poco diferente a las anteriores, aplica una función a los primeros elementos de los argumentos, a los segundos, etc.

En este ejemplo aplica `sum` a 1+1+1, luego a 2+2+2 y así:

```r
> mapply(sum, 1:4, 1:4, 1:4)
[1]  3  6  9 12
```

## vapply(): Determina el valor que debe devolver

Ejemplo:

```r
> vapply(list(1,'a',2), is.numeric, logical(1))
[1]  TRUE FALSE  TRUE
```

## Código fuente del ejemplo

- r / intro / 49-R-Apply-Funciones-A-Objetos / [R-Apply-Funciones-A-Objetos.R](https://gitlab.com/soka/r/blob/master/intro/49-R-Apply-Funciones-A-Objetos/R-Apply-Funciones-A-Objetos.R).

## Enlaces internos

Tipos de datos:

- ["Vectores"](https://soka.gitlab.io/blog/post/2019-03-11-r-vectores/).
- ["Matrices"](https://soka.gitlab.io/blog/post/2019-03-11-r-matrices/).
- ["Listas"](https://soka.gitlab.io/blog/post/2019-03-11-r-listas/).
- ["Data.Frame"](https://soka.gitlab.io/blog/post/2019-03-12-r-data-frame/).
- Data.frame ["Añadir una nueva columna"](https://soka.gitlab.io/blog/post/2019-03-01-ainadir-nueva-columna-df-en-r/).

## Enlaces externos

- [**apply**](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/apply): Apply Functions Over Array Margins. Returns a vector or array or list of values obtained by applying a function to margins of an array or matrix..
- [**lapply**](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/lapply): Apply A Function Over A List Or Vector. lapply returns a list of the same length as X, each element of which is the result of applying FUN to the corresponding element of X.
- ["Apply Function in R – apply vs lapply vs sapply vs mapply vs tapply vs rapply vs vapply"](http://www.datasciencemadesimple.com/apply-function-r/).
- datacamp.com ["Tutorial on the R Apply Family"](https://www.datacamp.com/community/tutorials/r-tutorial-apply-family).
- Advanced R by Hadley Wickham ["Functionals"](http://adv-r.had.co.nz/Functionals.html).
