---
title: Regresión lineal
subtitle: Modelos predictivos aprendizaje supervisado
date: 2000-01-01
draft: false
description: ML
tags: ["Python","ML","Aprendizaje supervisado","Regresión lineal"]
---

<!-- vscode-markdown-toc -->
* 1. [Regresión lineal simple](#Regresinlinealsimple)
* 2. [Datos de partida](#Datosdepartida)
	* 2.1. [Nube de puntos](#Nubedepuntos)
* 3. [Criterio de cuadrados mínimos](#Criteriodecuadradosmnimos)
	* 3.1. [Estimación de los coeficientes](#Estimacindeloscoeficientes)
		* 3.1.1. [Función para calcular los coeficientes](#Funcinparacalcularloscoeficientes)
		* 3.1.2. [Gráficar los resultados](#Grficarlosresultados)
* 4. [Scikit-Learn](#Scikit-Learn)
	* 4.1. [Split conjunto de entrenamiento y test](#Splitconjuntodeentrenamientoytest)
	* 4.2. [Aplicar el modelo y realizar el ajuste](#Aplicarelmodeloyrealizarelajuste)
	* 4.3. [Obtener el Interceptor y el Coeficiente](#ObtenerelInterceptoryelCoeficiente)
	* 4.4. [Predicciones con el conjunto de test](#Prediccionesconelconjuntodetest)
	* 4.5. [Métricas para evaluar la bondad del modelo](#Mtricasparaevaluarlabondaddelmodelo)
	* 4.6. [Representación gráfica](#Representacingrfica)
* 5. [Statsmodels](#Statsmodels)
	* 5.1. [Entrenar al modelo](#Entrenaralmodelo)
	* 5.2. [Sumario resultado](#Sumarioresultado)
* 6. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Regresinlinealsimple'></a>Regresión lineal simple

Vídeo [2021 Clase 11 Parte 5 Regresión Lineal](https://youtu.be/8tKruBXMDZY).

Supongamos que queremos modelar la relación entre dos variables reales mediante un [modelo lineal](https://es.wikipedia.org/wiki/Modelo_lineal). Y que vamos a ajustar los parámetros de ese modelos a partir de ciertos valores conocidos (mediciones, digamos). Es decir que vamos a estar pensando que las variables tienen una relación lineal, `Y = a*X + b`, donde `X` es la variable explicativa (sus componentes se denominan _independientes_ o _regresores_), e `Y` es la variable a explicar (también denominada _dependiente_ o _regresando_), `a` será la pendiente de la recta y `b` será el interceptor (para un valor de `x=0` donde corta el eje Y la recta).

A partir de un conjunto de datos de tipo `(x_i, y_i)`, planteamos el modelo `Y = a*X + b`.

![](img/11.png)

En general el modelo no va a ser exacto, es decir, no se va a complir que `y_i = a*x_i + b` para los valores `(x_i, y_i)` (salvo que estén, justamente, todos los valores sobre una línea recta, correlación perfecta sin residuo). 

![](img/01.png)

En general, decíamos, vamos a tener que `y_i = a*x_i + b + r_i` donde, los valores `r_i`, llamados _residuos_, representan las diferencias entre los valores de la recta en cada valor de x que tenemos y los valores de y asociados.

![](img/12.png)

El problema de **regresión lineal consiste en elegir los parámetros `a, b` de la recta (es decir, su pendiente y ordenada al origen)**, de manera que la recta sea la que mejor se adapte a los datos disponibles.

##  2. <a name='Datosdepartida'></a>Datos de partida

Hemos escogido un conjunto de datos pequeño para ilustrar el proceso paso a paso, todas las operaciones son escalables a _datasets_ más grandes.

###  2.1. <a name='Nubedepuntos'></a>Nube de puntos

Debemos definir dos [arrays](https://numpy.org/doc/stable/reference/generated/numpy.array.html#numpy-array) de NumPy y guardarlos en dos variables `x` e `y`. 

* Valores del array `x`: 55.0, 38, 68, 70, 53, 46, 11, 16, 20, 4.
* Valores del array `y`: 153.0, 98, 214, 220, 167, 145, 41, 63, 65, 25.

A continuación con Matplotlib generaremos una gŕafica de nube de puntos ([scatter plot](matplotlib.pyplot.scatter)) con un resultado similar a este:

![](img/02.png)

**Código**: [src/01.py](src/01.py)

```python
import numpy as np
import matplotlib.pyplot as plt

x = np.array([55.0, 38, 68, 70, 53, 46, 11, 16, 20, 4])
y = np.array([153.0, 98, 214, 220, 167, 145, 41, 63, 65, 25])
g = plt.scatter(x = x, 
                y = y,
                color = 'hotpink',
                alpha=0.5) # You can adjust the transparency of the dots with the alpha argument.
plt.title('scatterplot de los datos')
plt.legend(["X"])
plt.xlabel("Valores X")
plt.ylabel("Valores Y")
plt.show()
```

Hemos usado colores personalizados para los puntos ('hotpink'), se ha aplicado algo de transparencia a los puntos (0.5), se han definidos los títulos (_title_) y las etiquetas (_label_) con texto de los ejes y la leyenda (_legend_).

##  3. <a name='Criteriodecuadradosmnimos'></a>Criterio de cuadrados mínimos

Vamos a elegir como mejor recta a la que minimice los residuos. Más precisamente, vamos a elegir **la recta de manera tal que la suma de los cuadrados de los residuos sea mínima**.

![](img/03.png)

###  3.1. <a name='Estimacindeloscoeficientes'></a>Estimación de los coeficientes

Como hemos dicho antes, **la estimación de los coeficientes se realiza con valores reales conocidos de la tupla (X,y)**. Supongamos que tenemos `n` registros con las variables independientes y dependientes conocidas, con estas n observaciones, el objetivo es obtener las estimaciones de los coeficientes `a` y `b`  que hagan que el modelo lineal se ajuste lo mejor posible a los datos. Esta medida de cercania entre la recta y los puntos se puede hacer de distintas formas, siendo la forma más común la de ajustar por [mínimos cuadrados](https://es.wikipedia.org/wiki/M%C3%ADnimos_cuadrados) (less squares criterion).

Siendo:

![](img/07.png)

El residuo o diferencia entre la predicción y el valor real es: 	

![](img/08.png)

El método de mínimos cuadrados obtendrá los coeficientes que minimicen la suma de residuos al cuadrado, es decir, que minimicen:

![](img/09.png)

Usando derivadas parciales e igualando a 0 obtenemos que:

![](img/10.png)

Siendo  

![](img/13.png)

e  

![](img/14.png)

las medias aritméticas de las muestras.

####  3.1.1. <a name='Funcinparacalcularloscoeficientes'></a>Función para calcular los coeficientes

Definir una función llamada `ajuste_lineal_simple` que reciba como argumentos `x` e `y` y retorne los coeficientes. NumPy facilita mucho las cosas son su método de ndarray [mean](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.mean.html#numpy-ndarray-mean) o la función [sum](https://numpy.org/doc/stable/reference/generated/numpy.sum.html#numpy-sum)

Código: [src/02.py](src/02.py).

```python
def ajuste_lineal_simple(x,y):
    a = sum(((x - x.mean())*(y-y.mean()))) / sum(((x-x.mean())**2))
    b = y.mean() - a*x.mean()
    return a, b
```

Imprimir los resultados de aplicar la nueba función con los arrays dados de ejemplo al principio.

Salida:

```
Coeficientes. Pendiente= 2.905522724210283 , Interceptor= 8.39958420758822
``` 

####  3.1.2. <a name='Grficarlosresultados'></a>Gráficar los resultados

Ahora realiza un gráfico con la nube de puntos que hemos usado de entrenamiento y la recta ajustada, deberías obtener algo como esto:

![](img/15.png)

Código: [src/02.py](src/02.py).

```python
import numpy as np
import matplotlib.pyplot as plt

def ajuste_lineal_simple(x,y):
    a = sum(((x - x.mean())*(y-y.mean()))) / sum(((x-x.mean())**2))
    b = y.mean() - a*x.mean()
    return a, b

x = np.array([55.0, 38, 68, 70, 53, 46, 11, 16, 20, 4])
y = np.array([153.0, 98, 214, 220, 167, 145, 41, 63, 65, 25])

g = plt.scatter(x = x, 
                y = y,
                color = 'hotpink',
                alpha=0.5) # You can adjust the transparency of the dots with the alpha argument.
plt.title('Ajuste líneal')
plt.legend(["X"])
plt.xlabel("Valores X")
plt.ylabel("Valores Y")


a,b=ajuste_lineal_simple(x,y)
print('Coeficientes. Pendiente=',a,', Interceptor=',b)

grilla_y = x*a + b

plt.plot(x, 
         grilla_y, 
         c = 'green',
         alpha=0.5) # You can adjust the transparency of the dots with the alpha argument.
plt.xlabel('x')
plt.ylabel('y')
plt.show()
```

##  4. <a name='Scikit-Learn'></a>Scikit-Learn

Vamos a reproducir el modelo pero ahora usando librerías especializadas que nos ahorran muchos ćalculos.

###  4.1. <a name='Splitconjuntodeentrenamientoytest'></a>Split conjunto de entrenamiento y test

Primero vamos a hacer un _split_ para dividir el _dataset_ en una parte para el entrenamiento y otra para el testeo.

Usando [sklearn.model_selection.train_test_split](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html#sklearn-model-selection-train-test-split) con los arrays de datos iniciales `x` y `y` obtener los datos y guardarlos en las variables `x_train, x_test, y_train, y_test`. Vamos a emplear el 80% del dataset para el entrenamiento (`train_size`) y usaremos `shuffle` a `True` para mezclar los datos. 

Aunque no obtengamos los mismos resultados (por el `shuffle`) si imprimes los conjuntos verás que están tomados de los datos originales de forma aleatoria. Además el conjunto de test tendrá menos valores o será más pequeño.

Código: [src/03.py](src/03.py).


```python
x_train, x_test, y_train, y_test = train_test_split(
                                        x,
                                        y,
                                        train_size   = 0.8, # If float, should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the train split.
                                        shuffle      = True # Whether or not to shuffle the data before splitting
                                    )
# List containing train-test split of inputs.
print('x_train:')
print(x_train)
print('y_train:')
print(y_train)
print('x_test:')
print(x_test)
print('y_test:')
print(y_test)							
```						

Salida de ejemplo:

```
x_train:
[68.  4. 20. 38. 11. 55. 16. 53.]
y_train:
[214.  25.  65.  98.  41. 153.  63. 167.]
x_test:
[46. 70.]
y_test:
[145. 220.]
```

###  4.2. <a name='Aplicarelmodeloyrealizarelajuste'></a>Aplicar el modelo y realizar el ajuste

Aplicar el modelo de regresión líneal con [LinearRegression](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html#sklearn-linear-model-linearregression) y guardar en una variable llamada `ajus`, a continuación aplicar el método [fit](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html#sklearn.linear_model.LinearRegression.fit) para el ajuste.

**Ojo**: para pasar los argumentos `x_train, y_train` a [fit](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html#sklearn.linear_model.LinearRegression.fit) deberás hacer un [reshape](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.reshape.html#numpy-ndarray-reshape) de los arrays. 

Ejemplo:

![](img/16.png)

**Código**: [src/03.py](src/03.py).

```python
ajus = linear_model.LinearRegression() # llamo al modelo de regresión lineal
# ajusto el modelo
ajus.fit(x_train.reshape(-1,1), 
         y_train.reshape(-1,1)) 
```

###  4.3. <a name='ObtenerelInterceptoryelCoeficiente'></a>Obtener el Interceptor y el Coeficiente

Imprimir los valores del interceptor y el array con los coficiente (atributos del modelo), la salida debería ser algo así:

```
Interceptor:  [8.93892599]
Coeficientes:  [[2.91597106]]
```

**Ojo**: Estos valores pueden cambiar entre una ejecución y otra ya que estamos cogiendo un conjunto de entrenamiento aleatorio, además tampoco coincidirán con los valores obtenidos con nuestra función `ajuste_lineal_simple`, en esa ocasión hemos usado todos los datos de partida.

**Código**: [src/03.py](src/03.py).

```python
print('Interceptor: ',ajus.intercept_) # Interceptor:  [10.46019696]
print('Coeficientes: ',ajus.coef_) # Coeficientes:  [[2.97332786]]
```

###  4.4. <a name='Prediccionesconelconjuntodetest'></a>Predicciones con el conjunto de test

Una vez entrenado el modelo, se evalúa la capacidad predictiva empleando el conjunto de test. Aplicaremos el modelo ajustado sobre los datos de test `x_test` que no hemos usado hasta el momento, el modelo producirá como salida una serie de predicciones de valores "y" que guardaremos en la variable `y_pred`.

**Código**: [src/03.py](src/03.py).

```python
y_pred = ajus.predict(X = x_test.reshape(-1,1))
print('y_pred:')
print(y_pred)
```

Si imprimimos las predicciones serán sólo dos valores ya que sólo hemos usado el 20% del _dataset_ para testear nuestro modelo, es decir, los dos valores de `x_test`.

La salida será algo como esto:

```
y_pred:
[[140.85765143]
 [161.0324012 ]]
```

###  4.5. <a name='Mtricasparaevaluarlabondaddelmodelo'></a>Métricas para evaluar la bondad del modelo

Ahora vamos a evaluar la bondad de nuestro modelo aplicando las métricas: 

* [MSE](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.mean_squared_error.html#sklearn-metrics-mean-squared-error): El [error cuadrático medio](https://es.wikipedia.org/wiki/Error_cuadr%C3%A1tico_medio)
* RMSE: La [raíz del error cuadrático medio](https://es.wikipedia.org/wiki/Ra%C3%ADz_del_error_cuadr%C3%A1tico_medio). Simplemente aplicando la [raíz cuadrada](https://www.geeksforgeeks.org/python-math-function-sqrt/) con la librería math.
* [MAE](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.mean_squared_error.html#sklearn-metrics-mean-squared-error): [Error absoluto medio](https://es.wikipedia.org/wiki/Error_absoluto_medio).
* [R2](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.r2_score.html#sklearn.metrics.r2_score): [Coeficiente de determinación](https://es.wikipedia.org/wiki/Coeficiente_de_determinaci%C3%B3n). Adquiere valores entre 0 y 1, donde 1 es una predicción perfecta.

Formula MSE:

![](img/18.png)

Las métricas se obtienen usando `y_test,y_pred`, es decir los datos de test reales y los predecidos anteriormente.

**Código**: [src/04.py](src/04.py).

```python
from sklearn.metrics import mean_squared_error, mean_absolute_error,r2_score
mse = mean_squared_error(y_test,y_pred)
mae = mean_absolute_error(y_test,y_pred)
print("Mean Square Error (MSE) : ", mse)
import math
rmse = math.sqrt(mse)
print("Root Mean Square Error (RMSE):",rmse)
print("Mean Absolute Error (MAE) : ", mae)
# Coeficiente de determinación R cuadrado. 1 es una predicción perfecta
print("Coeficiente de determinación (R2): ",r2_score(y_test, y_pred))
```

Salida de ejemplo:

```
Mean Square Error (MSE) :  1.5443417777927537
Root Mean Square Error (RMSE): 1.2427154854562463
Mean Absolute Error (MAE) :  1.0575435203094834
Coeficiente de determinación (R2):  0.9892754043208837
```

El error de test del modelo RMSE es de 1.24. Las predicciones del modelo final se alejan en promedio 1.24 unidades del valor real. 

###  4.6. <a name='Representacingrfica'></a>Representación gráfica

Vamos a representar en una sola gráfica los datos de entrenamiento, los de test y la recta obtenida con el ajuste ().

![](img/17.png)


##  5. <a name='Statsmodels'></a>Statsmodels

###  5.1. <a name='Entrenaralmodelo'></a>Entrenar al modelo



**Código**: [src/05.py](src/05.py).

```python
import statsmodels.api as sm
import statsmodels.formula.api as smf
datosxy = pd.DataFrame({'x': x, 'y': y}) # paso los datos a un dataframe
print(datosxy)
modelo = smf.ols(formula = 'y ~ x', data = datosxy)
modelo = modelo.fit()
```

###  5.2. <a name='Sumarioresultado'></a>Sumario resultado

```python
print(modelo.summary())
```

**Código**: [src/05.py](src/05.py).

Salida ejemplo:

```
==============================================================================
Dep. Variable:                      y   R-squared:                       0.979
Model:                            OLS   Adj. R-squared:                  0.977
Method:                 Least Squares   F-statistic:                     381.7
Date:                Mon, 20 Feb 2023   Prob (F-statistic):           4.90e-08
Time:                        19:36:34   Log-Likelihood:                -36.794
No. Observations:                  10   AIC:                             77.59
Df Residuals:                       8   BIC:                             78.19
Df Model:                           1                                         
Covariance Type:            nonrobust                                         
==============================================================================
                 coef    std err          t      P>|t|      [0.025      0.975]
------------------------------------------------------------------------------
Intercept      8.3996      6.603      1.272      0.239      -6.827      23.626
x              2.9055      0.149     19.536      0.000       2.563       3.248
==============================================================================
Omnibus:                        5.043   Durbin-Watson:                   1.169
Prob(Omnibus):                  0.080   Jarque-Bera (JB):                2.446
Skew:                          -1.211   Prob(JB):                        0.294
Kurtosis:                       3.019   Cond. No.                         86.5
==============================================================================

Notes:
[1] Standard Errors assume that the covariance matrix of the errors is correctly specified.
```

El valor de **R-squared** indica que el modelo es capaz de explicar el 97,9% de la variabilidad observada en la variable respuesta.

El modelo lineal generado sigue la ecuación:

y = 8.3996 + 2.9055 x

Por cada unidad que se incrementa x, y aumenta en promedio 2.9055 unidades.

##  6. <a name='Enlacesexternos'></a>Enlaces externos

* [https://programacionpython.ecyt.unsam.edu.ar/material/11_Recursion/05_Regresion_Lineal/](https://programacionpython.ecyt.unsam.edu.ar/material/11_Recursion/05_Regresion_Lineal/): Fuente original usada para este artículo.
* [https://programacionpython.ecyt.unsam.edu.ar/material/05_Random_Plt_Dbg/03_NumPy_Arrays/#operaciones-un-poco-mas-complejas](https://programacionpython.ecyt.unsam.edu.ar/material/05_Random_Plt_Dbg/03_NumPy_Arrays/#operaciones-un-poco-mas-complejas): MSE con NumPy.
* [https://www.w3schools.com/python/matplotlib_scatter.asp](https://www.w3schools.com/python/matplotlib_scatter.asp)
* [https://realpython.com/visualizing-python-plt-scatter/](https://realpython.com/visualizing-python-plt-scatter/)
* [https://scikit-learn.org/stable/auto_examples/linear_model/plot_ols.html#sphx-glr-auto-examples-linear-model-plot-ols-py](https://scikit-learn.org/stable/auto_examples/linear_model/plot_ols.html#sphx-glr-auto-examples-linear-model-plot-ols-py): The example below uses only the first feature of the diabetes dataset, in order to illustrate the data points within the two-dimensional plot. The straight line can be seen in the plot, showing how linear regression attempts to draw a straight line that will best minimize the residual sum of squares between the observed responses in the dataset, and the responses predicted by the linear approximation. The coefficients, residual sum of squares and the coefficient of determination are also calculated.
* [https://www.geeksforgeeks.org/ml-linear-regression/?ref=gcse](https://www.geeksforgeeks.org/ml-linear-regression/?ref=gcse)
* [https://www.geeksforgeeks.org/linear-regression-python-implementation/?ref=gcse](https://www.geeksforgeeks.org/linear-regression-python-implementation/?ref=gcse)
* [https://machinelearningparatodos.com/regresion-lineal-en-python/](https://machinelearningparatodos.com/regresion-lineal-en-python/).
* [https://www.cienciadedatos.net/documentos/py10-regresion-lineal-python.html](https://www.cienciadedatos.net/documentos/py10-regresion-lineal-python.html).

