#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

def ajuste_lineal_simple(x,y):
    a = sum(((x - x.mean())*(y-y.mean()))) / sum(((x-x.mean())**2))
    b = y.mean() - a*x.mean()
    return a, b

x = np.array([55.0, 38, 68, 70, 53, 46, 11, 16, 20, 4])
y = np.array([153.0, 98, 214, 220, 167, 145, 41, 63, 65, 25])

g = plt.scatter(x = x, 
                y = y,
                color = 'hotpink',
                alpha=0.5) # You can adjust the transparency of the dots with the alpha argument.
plt.title('Ajuste líneal')
plt.legend(["X"])
plt.xlabel("Valores X")
plt.ylabel("Valores Y")


a,b=ajuste_lineal_simple(x,y)
print('Coeficientes. Pendiente=',a,', Interceptor=',b)

grilla_y = x*a + b

plt.plot(x, 
         grilla_y, 
         c = 'green',
         alpha=0.5) # You can adjust the transparency of the dots with the alpha argument.
plt.xlabel('x')
plt.ylabel('y')
plt.show()





