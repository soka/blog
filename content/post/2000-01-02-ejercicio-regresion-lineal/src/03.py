#!/usr/bin/env python3

import numpy as np
import pandas as pd
from sklearn import linear_model
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

x = np.array([55.0, 38, 68, 70, 53, 46, 11, 16, 20, 4]) # mismos datos x, y
y = np.array([153.0, 98, 214, 220, 167, 145, 41, 63, 65, 25])


x_train, x_test, y_train, y_test = train_test_split(
                                        x,
                                        y,
                                        train_size   = 0.8, # If float, should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the train split.
                                        shuffle      = True # Whether or not to shuffle the data before splitting
                                    )
# List containing train-test split of inputs.
print('x_train:')
print(x_train)
print('y_train:')
print(y_train)
print('x_test:')
print(x_test)
print('y_test:')
print(y_test)


datosxy = pd.DataFrame({'x': x, 'y': y}) # paso los datos a un dataframe
print(datosxy)

ajus = linear_model.LinearRegression() # llamo al modelo de regresión lineal
# ajusto el modelo
ajus.fit(x_train.reshape(-1,1), 
         y_train.reshape(-1,1)) 

print('Interceptor: ',ajus.intercept_) # Interceptor:  [10.46019696]
print('Coeficientes: ',ajus.coef_) # Coeficientes:  [[2.97332786]]

predicciones = ajus.predict(X = x_test.reshape(-1,1))
print('predicciones:')
print(predicciones)









