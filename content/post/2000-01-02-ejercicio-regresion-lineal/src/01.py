#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

x = np.array([55.0, 38, 68, 70, 53, 46, 11, 16, 20, 4])
y = np.array([153.0, 98, 214, 220, 167, 145, 41, 63, 65, 25])
g = plt.scatter(x = x, 
                y = y,
                color = 'hotpink',
                alpha=0.5) # You can adjust the transparency of the dots with the alpha argument.
plt.title('scatterplot de los datos')
plt.legend(["X"])
plt.xlabel("Valores X")
plt.ylabel("Valores Y")
plt.show()



