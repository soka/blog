---
title: Factores en R
subtitle: Categorización de datos y la función factor
date: 2019-08-15
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Básico", "R - Funciones", "R - Base"]
---

![](images/r-logo.PNG)

En **R** los **factores** se usan para categorizar conjuntos de datos basados en un conjunto fijo de valores posibles. Este tipo de dato no es muy común en otros lenguajes de programación y es conveniente entender como funciona ya que lo veremos muy a menudo trabajando con _data frames_ por ejemplo:

```r
> v1 <- c(1,2,3)
> v2 <- c("uno","dos","tres")
> str(data.frame(v1,v2))
'data.frame':	3 obs. of  2 variables:
 $ v1: num  1 2 3
 $ v2: Factor w/ 3 levels "dos","tres","uno": 3 1 2
```

Aunque podemos evitar este comportamiento con `stringsAsFactors=FALSE`:

```r
> data.frame(v1,v2,stringsAsFactors=FALSE) %>% str
'data.frame':	4 obs. of  2 variables:
 $ v1: num  1 2 3 1
 $ v2: chr  "uno" "dos" "tres" "uno"
```

Vamos con un ejemplo muy sencillo, definimos un vector con las tallas de unas camisetas y lo convertimos en una **clase factor**, la nueva variable ahora contiene una estructura adicional denominada `Levels` donde se registran los elementos del vector original sin repetición:

```r
> tallasPrendas <- c('L','M','S','X','S','L','M','S','Select one')
> factor.tallasPrendas <- factor(tallasPrendas)
> class(factor.tallasPrendas)
[1] "factor"
> unclass(factor.tallasPrendas)
[1] 1 2 3 5 3 1 2 3 4
attr(,"levels")
[1] "L"          "M"          "S"          "Select one" "X"
```

La clase `factor` está compuesta por dos vectores:

- El primero es un vector de índices enteros que sustituye el vector de carácteres original, cada número del vector indica que cadena contiene apuntando a la posición del vector `levels`, así sabemos que el 1 es "L", el dos "M", etc.
- El segundo son las categorías o `levels`.

Usando la función `table` que toma como argumento un factor podemos obtener las frecuencias de aparición, por ejemplo tenemos 2 prendas con la talla 'M':

```r
> table(factor.tallasPrendas)
factor.tallasPrendas
         L          M          S Select one          X
         2          2          3          1          1
```

En realidad obtenemos el mismo resultado pasando el argumento como vector a la función `table` ya que espera recibir un factor como entrada y de forma automática convierte el vector.

```r
> table(tallasPrendas)
tallasPrendas
         L          M          S Select one          X
         2          2          3          1          1
```

## Niveles

Podemos establecer un rango de categorías validas como un vector pasado como parámetro `levels`, en el siguiente ejemplo defino un rango de valores posibles y de forma premeditada introduzco un elemento mal escrito "Eme", al convertirlo en factor se traduce como un valor NA, si quisiésemos ver las advertencias podemos usar la función `parse_factor` con los mismos argumentos.

```r
> misMeses <- c("Eme","Ene","Ene","May","Jun","Jun","Dic")
> nivelesMeses <- c("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago",
+            "Sep","Oct","Nov","Dic")
> factor.meses <- factor(misMeses, levels=nivelesMeses)
> unclass(factor.meses)
[1] NA  1  1  5  6  6 12
attr(,"levels")
 [1] "Ene" "Feb" "Mar" "Abr" "May" "Jun" "Jul" "Ago" "Sep" "Oct" "Nov" "Dic"
```

## Acceso a los elementos del factor

```r
> factor.meses[2]
[1] Ene
Levels: Ene Feb Mar Abr May Jun Jul Ago Sep Oct Nov Dic
> levels(factor.meses)[3]
[1] "Mar"
```

Modificar un nivel:

```r
levels(factor.meses)[2] <- "febrero"
```

Si se quiere tener acceso al factor como un vector de índices, se convierte a entero:

```r
> as.integer(factor.meses)
[1] NA  1  1  5  6  6 12
```

## Eliminar un elemento

```r
> factor(c("Iker","Mikel","Aitor","Naiara","Igor","Olatz"),exclude = c("Igor","Aitor"))
[1] Iker   Mikel  <NA>   Naiara <NA>   Olatz
Levels: Iker Mikel Naiara Olatz
```

## Código fuente

- r / intro / 50-Factores / [Factores.R](https://gitlab.com/soka/r/blob/master/intro/50-Factores/Factores.R).

## Enlaces externos

- ["R para Ciencia de Datos"](https://es.r4ds.hadley.nz/factores.html).
- ["Wrangling categorical data in R"](https://peerj.com/preprints/3163/).
- ["stringsAsFactors: An unauthorized biography"](https://simplystatistics.org/2015/07/24/stringsasfactors-an-unauthorized-biography/).
- ["stringsAsFactors = <sigh> - Biased and Inefficient"](https://notstatschat.tumblr.com/post/124987394001/stringsasfactors-sigh).
