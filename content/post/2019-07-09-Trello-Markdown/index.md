---
title: Trello - Texto formateado con Markdown
subtitle: La pizarra digital inspirada en kanban
date: 2019-07-09
draft: false
description: Sintaxis Markdown
tags:
  [
    "Trello",
    "Talleres",
    "Cursos",
    "Aplicaciones",
    "Trello - Introducción",
    "Trello - Tarjetas",
    "Trello - Power-Ups",
    "Kanban",
    "Metodologías",
    "Gestión de Proyectos",
    "Herramientas",
    "Markdown",
  ]
---

![](img/logo-trello.png)

[**Markdown**](https://es.wikipedia.org/wiki/Markdown) es una sintáxis reducida para formatear textos, su filosofía está en el polo opuesto de los procesadores de texto de tipo [**WYSIWYG**](https://es.wikipedia.org/wiki/WYSIWYG) (**W**hat **Y**ou **S**ee **I**s **W**hat **Y**ou **G**et) que usamos a diario cuando escribimos en [LibreOffice](https://es.wikipedia.org/wiki/LibreOffice) o cualquier otro editor de texto.

Cuando editamos un texto en **Markdown** introducimos carácteres especiales para "embellecer" el texto, por ejemplo para marcar un texto en negrita usamos `**Este texto se verá en negrita**`.

La sintaxis **Markdown** no es exclusiva de **Trello**, este blog también está desarrollado en Markdown y los programadores informáticos los usan de forma habitual, un fichero escrito con Markdown se puede leer y editar con un editor de texto plano cualquiera (como el Notepad de MS Win por ejemplo) lo que hace que no tenga dependencias para poder editarlo o leer su contenido como puede tener MS Word por ejemplo, esto hace que sea muy portable y ligero. AL final de este post he recopilado algunos enlaces de editores Markdown en línea.

## Sintaxis para descripciones de tarjetas, comentarios, listas y tu biografía

- **Negrita**: Encerramos el texto entre dos "\*", así `**Este texto se verá en negrita**`.
- **Cursiva**: Con guiones bajos `_así_`.
- **Tachado**: Con dos tildes a cada lado `~~así~~`.
- **Código en línea**: Incluye código formateado en línea usando un solo acento grave (`) al principio y al final del código.
- **Enlaces**: Crea un enlace poniendo el texto del enlace entre corchetes y la URL entre paréntesis, `[así](http://www.trello.com)`.

![](img/01.gif)

## Sintaxis solo para descripciones de tarjetas y comentarios

- **Línea horizontal**: Una línea formada por al menos tres guiones `---`.
- **Bloque de código**: Incluye código formateado poniendo tres acentos graves (```) al inicio y al final del bloque.
- **Sangría / Bloques destacados** - Sangra un texto incluyendo “>” delante de cada línea del texto que quieras sangrar o citar.

![](img/02.gif)

- **Listas con guiones o números**.

![](img/03.gif)

- **Encabezados**:

![](img/04.gif)

## Enlaces externos

- ["Cómo formatear tu texto en Trello"](https://help.trello.com/article/821-using-markdown-in-trello).

**Markdown**:

- ["Online Markdown Editor - Dillinger, the Last Markdown Editor ever."](https://dillinger.io/).
- ["StackEdit – In-browser Markdown editor"](https://stackedit.io/).
