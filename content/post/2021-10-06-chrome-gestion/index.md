---
title: Chrome para administradores de sistemas
subtitle: Notas sobre la gestión de navegadores Chrome
date: 2021-10-06
draft: false
description: Gestión de navegadores Chrome
tags: ["Chrome","Sysadmin","Administración","GPO","Políticas"]
---

<!-- vscode-markdown-toc -->
* 1. [Comandos](#Comandos)
* 2. [Políticas](#Polticas)
* 3. [Registrar nuevos navegadores en la consola](#Registrarnuevosnavegadoresenlaconsola)
* 4. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


##  1. <a name='Comandos'></a>Comandos

Políticas del equipo ([Chrome Enterprise policy list](https://cloud.google.com/docs/chrome-enterprise/policies)), se pueden cambiar desde la consola de administrador de Google.

`chrome://policy/`

![](img/02.PNG)

Acceso a la configuración:

`chrome://settings/`

`chrome://version/`

Directorio por defecto del usuario con el perfil, cookies, marcadores, etc.

`chrome::GetDefaultUserDataDirectory`

![](img/01.PNG)

En Win suele ser "C:\Users\informatica\AppData\Local\Google\Chrome\User Data\".

##  2. <a name='Polticas'></a>Políticas

`HKEY_CURRENT_USER\Software\Policies\Google\Chrome\`

##  3. <a name='Registrarnuevosnavegadoresenlaconsola'></a>Registrar nuevos navegadores en la consola

En la consola de administrador de Google se puede bajar un fichero con la clave de registro con la clave `CloudManagementEnrollmentToken` en "[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Chrome]"

![](img/04.PNG)

Una vez registrado se pueden gestionar el navegador en la consola.

##  4. <a name='Enlacesexternos'></a>Enlaces externos

* [https://chromium.googlesource.com/chromium/src/+/HEAD/docs/user_data_dir.md](https://chromium.googlesource.com/chromium/src/+/HEAD/docs/user_data_dir.md)