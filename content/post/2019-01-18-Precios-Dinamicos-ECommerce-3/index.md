---
title: Ajuste dinámico de precios en comercios electrónicos (3/3) [BORRADOR]
subtitle: Modelos estadísticos y algoritmos de predicción de la demanda
date: 2019-01-18
draft: true
description: Predicción de la demanda y tecnologías
tags: ["Algoritmos","eCommerce","Machine learning","Marketing","R","Economía","Comercio"]
---

# Modelos predictivos y algoritmos 

## Pronóstico por promedio móvil 

En estadística, una media móvil es un cálculo utilizado para analizar un **conjunto de datos en modo de puntos para crear series de promedios**. Así las medias móviles **son una lista de números en la cual cada uno es el promedio de un subconjunto de los datos originales**.

Consiste en realizar medias aritméticas basadas en conjuntos o subgrupos de datos historicos de _n_ periodos anteriores.

La media móvil **es un indicador de tendencia** que nunca se anticipa al movimiento o tendencia de las cotizaciones, es decir, simplemente **sigue a la curva de cotizaciones confirmando la tendencia que hay en vigor en cada momento**. **No nos adelanta cambios de tendencia**, pero si los puede confirmar.

Es recomendable **ligar la longitud de la media móvil a un ciclo de mercado**, periodos estacionales de ventas por trimestre por ejemplo.

### Promedio móvil simple (PMS)

Es una técnica elemental de predicción. **Usando la media artimética de los _n_ datos anteriores**, aplicado a una serie temporal de datos cuanto más grande sea el conjunto de datos _n_ mayor será la influencia de datos antiguos. Con un _n_ bajo nuestra predicción tendrá una alta capacidad para responder rápidamente ante fluctuaciones o variaciones significativas en los datos de un período a otro

Cada punto de una media móvil de una serie temporal es la media aritmética de un número de puntos consecutivos de la serie, donde el número de puntos es **elegido de tal manera que los efectos estacionales y / o irregulares sean eliminados**.

El escenario ideal para la utilización del método de **Media Móvil Simple** es cuando la **demanda real no presenta mayores variaciones de corto plazo, no presenta una tendencia marcada e idealmente no presenta estacionalidades**.

La **función matemática** es la siguiente:

![](images/media_movil/formula_media_movil_simple_01.PNG)

[[Fuente](http://trucosycursos.es/la-media-movil-en-excel/?cn-reloaded=1)]

Donde: 

* `N`: Número de periodos anteriores.
* `At`: Valor real.
* `Ft`: Valor pronosticado para siguiente periodo. 

Vamos a realizar un ejercicio super sencillo en Excel 2010 para aplicar la media móvil. Antes debemos habilitar el complemento de análisis (Archivo > Opciones > Complementos : "Herramientas para análisis") en MS Excel.

![](images/media_movil/excel_01.PNG)

La siguiente tabla 

**¿Cuando usarlo?** 


**Enlaces**:

* Wikipedia [**media móvil**](https://es.wikipedia.org/wiki/Media_m%C3%B3vil).
* ingenieriaindustrialonline.com [**promedio móvil**](https://www.ingenieriaindustrialonline.com/herramientas-para-el-ingeniero-industrial/pron%C3%B3stico-de-ventas/promedio-m%C3%B3vil/): Presenta la formula necesaria y un ejemplo paso a paso fácil de seguir. 
* ["Pronóstico de Demanda con Media Móvil Simple"](https://www.gestiondeoperaciones.net/proyeccion-de-demanda/pronostico-de-demanda-con-media-movil-simple/).
* http://trucosycursos.es/la-media-movil-en-excel/?cn-reloaded=1

https://www.uam.es/docencia/predysim/predysim/descargas/2_3_ejercicio1.xls

https://nubededatos.blogspot.com/2018/01/media-movil-en-r.html

https://rpubs.com/joser/SeriesTemporalesBasicas

### Promedio móvil ponderado

El promedio ponderado suele reaccionar más rápido ante los cambios de la demanda, con relación al promedio simple. Se requiere conocimiento del "negocio" para ponderar o dar más peso a un periódo u otro. La suma de las ponderaciones debe ser igual a 1.

La formula es la siguiente:

![](https://ingenioempresa.com/wp-content/uploads/2016/02/formula-promedio-ponderado.png)

[[Fuente](https://ingenioempresa.com/promedio-movil-ponderado/)]

Normalmente se ponderan los datos más recientes o se las da mayo peso 

 
### Promedio móvil exponencial

### Indicadores 

https://www.gestiondeoperaciones.net/proyeccion-de-demanda/calculo-del-mad-y-la-senal-de-rastreo-para-un-pronostico-de-demanda/

#### MAD (Error Absoluto Medio)


#### Señal de rastreo (TS) 

## Modelos regresivos para predecir la demanda 

El [análisis de regresión](https://es.wikipedia.org/wiki/An%C3%A1lisis_de_la_regresi%C3%B3n) es una **técnica estadística para investigar la relación funcional entre dos o más variables usando un modelo matemático**.

Se trata de que dada una gran cantidad de información, podamos **generar un sistema que sea capaz de describirnos de una forma aceptable la información y**, además, que **sea capaz de generar nueva**. 

La primera forma de regresión fue el [**método de mínimos cuadrados**](https://es.wikipedia.org/wiki/M%C3%ADnimos_cuadrados) para buscar una función continua que mejor se aproxime a los datos dados como un conjunto de pares ordenados. 

Se utiliza **cuando se evidencia una tendencia en los datos históricos** (series de tiempo).

Aplicado al sector económico y la previsión de la demanda de un producto .... 

https://www.eoi.es/blogs/scm/2012/12/17/regresion-lineal-como-tecnica-mas-eficiente-para-le-prevision-de-la-demanda/

https://www.gestiondeoperaciones.net/proyeccion-de-demanda/como-utilizar-una-regresion-lineal-para-realizar-un-pronostico-de-demanda/

https://www.youtube.com/watch?v=Eo9Yx-hVpLQ

https://ingenioempresa.com/regresion-lineal/





### Regresión lineal simple

En estadística un modelo de [**regresión líneal**](https://es.wikipedia.org/wiki/Regresi%C3%B3n_lineal) es un **modelo matemático para aproximar la relación de dependencia entre dos variables, una dependiente de la otra**. 

Para entender mejor el modelo de regesión simple y darle un aspecto práctico usaremos un **ejemplo** muy común en internet, **buscamos la relación entre el precio medio de una vivienda y el número de habitaciones** por hogar, dibujamos una gráfica con los diferentes pisos en venta, donde el eje "X" representa el número de habitaciones y el eje "Y" a su vez el precio, cada punto representa una vivienda en diferentes barrios de la ciudad. Logicamente existe una relación entre el número de habitaciones y el precio de la vivienda, a mayor número de habitaciones en el eje "X" el precio sube en el eje "Y".  

El [**diagrama de dispersión**](https://es.wikipedia.org/wiki/Diagrama_de_dispersi%C3%B3n) (nube de puntos) resultante podría tener este aspecto por ejemplo:

![](images/grafica_regresion_linea_simple_01.PNG)

Ahora se trata de hallar la recta que mejor "encaje" con el conjunto de puntos dados. Para esto podremos usar métodos como el de los [**"mínimos cuadrados"**](https://es.wikipedia.org/wiki/M%C3%ADnimos_cuadrados) que busca minimizar la distancia vertical de todos los puntos a la línea.

![](images/grafica_regresion_linea_simple_02.PNG) 

Una vez obtenida esta línea, seremos capaces de hacer predicciones hipotéticas sobre cuál sería el valor de "Y" (precio de la vivienda) dado "X" (número de habitaciones). 

![](images/formula_basica_regresion_lineal_01.PNG)

La [recta](https://es.wikipedia.org/wiki/Recta) (una sucesión de puntos en la misma dirección) se puede reducir a una formula (la forma simplificada) mediante la ecuación de arriba (es un polinomio de primer grado) y se representa gráficamente como una línea recta en un plano en [coordenadas Cartesianas](https://es.wikipedia.org/wiki/Coordenadas_cartesianas)**. Partiendo de una serie de observaciones "X" e "Y" se busca una línea recta que describa cada uno de los pares, en este caso la variable "X" es la independiente, regresiva o predictora y "Y" es la respuesta para cada valor específico de "Xi".

La descripción de la formula matemática para una recta es la siguiente.

El **termino independiente o secante** "b" muestra a que altura la recta corta el eje "Y". 

La variable "m" es la **pendiente que graficamente define la inclinación de la recta respecto al eje "X" y conceptualmente la relación entre la variable "X" y la variable de salida "Y"**, es el cambio esperado en "Y" por cada incremento unitario en "X", conforme "m", la pendiente, aumenta, la recta se hace más empinada. Cuando el valor absoluto de "m" se acerca a cero, la pendiente se aplana. Para determinar la pendiende de una recta se requiere de sólo dos coordenadas o puntos.

![](images/grafica_regresion_linea_simple_03.PNG) 

Es facil comprenderlo usando números enteros, he encontrado este [enlace](https://www.desmos.com/calculator) que permite definir una formular y visualizarla gráficamente.

#### Método de los mínimos cuadrados ordinarios (Gauss) 

Ahora **debemos buscar la recta que satisfaga los criterios anteriores**. Uno de los métodos es el de [**mínimos cuadrados ordinarios**](https://es.wikipedia.org/wiki/M%C3%ADnimos_cuadrados) (atribuido a Gauss).

Rescatando el diagrama de dispersión previo, para un punto "X" nuestra recta predice un valor de "Y" pero sin embargo el valor real de nuestros datos es otro, esa distancia representa el error, debemos aplicar este criterio para todos los datos de nuestro modelo y calcular la media.

![](images/grafica_regresion_linea_simple_error_01.PNG)

Si nos fijamos lo que estamos haciendo al elevar al cuadrado el error es calcular el área de coste para cada punto y realizar la media, jugando con el termino independiente y la pendiente de la función de la función de la recta debemos buscar la combinación de parámetros que minimize esta suma de cuadrados. 

![](images/funcion_coste_y_error_cuadratico_medio_01.PNG)

Sin desarrollar más la lógica de la ecuación al final tenemos las siguientes expresiones:

![](https://miprofe.com/wp-content/uploads/2016/04/minimos-cuadrados7-e1460480353422-300x150.png)

Ayudados por una tabla para realizar los cálculos vamos a aplicar la **formula en un caso real, el pronóstico de ventas de un producto para los siguientes dos años**, esta la es la tabla de partida con los datos:

![](images/regresion_lineal_mco_ejercicio_01/01.PNG)

El gráfico de dispersión resultante de los valores es este: 

![](images/regresion_lineal_mco_ejercicio_01/03.PNG)

En las columnas de la derecha aplicamos el cuadro de X e Y y su mutiplicación, la fila inferior corresponde al sumatorio de los valores. 

![](images/regresion_lineal_mco_ejercicio_01/02.PNG)

Ahora usando la tabla con los nuevos campos con los cálculos intermedios es fácil determinar que:

* b = 12
* a = 213,3
* Y = 213,3+12X

Concluimos que el pronóstico de ventas para el año seis es de 285,3 y para el siguiente de 297,3.

![](images/regresion_lineal_mco_ejercicio_01/04.png)

#### Coeficiente de correlación de Pearson y R cuadrado

El  [**coeficiente de correlación de Pearson**](https://es.wikipedia.org/wiki/Coeficiente_de_correlaci%C3%B3n_de_Pearson) es una medida para medir el grado de relación entre dos variables y la bondad del ajuste de nuestra recta, puede variar entre -1 y 1.

![](https://image.jimcdn.com/app/cms/image/transf/none/path/s075f076504dfea8d/image/i5b4d913fcf35fde8/version/1397760391/image.png)

[[Fuente](https://www.ingenieriaindustrialonline.com/herramientas-para-el-ingeniero-industrial/pron%C3%B3stico-de-ventas/regresi%C3%B3n-lineal/	)] 

La correlación entre dos variables X e Y es perfecta positiva cuando exactamente en la medida que aumenta una de ellas aumenta la otra. Es perfecta negativa cuando exactamente en la medida que aumenta una variable disminuye la otra.

La finalidad de la correlación es examinar la dirección y la fuerza de la asociación entre dos variables cuantitativas. Así conoceremos la intensidad de la relación entre ellas y si, al aumentar el valor de una variable, aumenta o disminuye el valor de la otra variable.

El valor del índice de correlación varía en el intervalo [-1,1], indicando el signo el sentido de la relación:

Valor del Coeficiente de Pearson | Grado de Correlación entre las Variables
---------------------------------|------------------------------------------
r = 0     						 |  no existe relación lineal. Pero esto no necesariamente implica que las variables son independientes: pueden existir todavía relaciones no lineales entre las dos variables.
r = 1							 | existe una correlación positiva perfecta. El índice indica una dependencia total entre las dos variables denominada relación directa: cuando una de ellas aumenta, la otra también lo hace en proporción constante.
0 < r < 1						 | correlación positiva
r = 0							 | no existe relación lineal. Pero esto no necesariamente implica que las variables son independientes: pueden existir todavía relaciones no lineales entre las dos variables
-1 < r < 0						 | existe una correlación negativa
r = -1							 | existe una correlación negativa perfecta. El índice indica una dependencia total entre las dos variables llamada relación inversa: cuando una de ellas aumenta, la otra disminuye en proporción constante

La expresión para calcular el valor de R cuadrado es la siguiente:

![](https://rodas5.us.es/file/6ee22068-3a5f-b78e-06a3-b06ace495ead/1/practica2_SCORM.zip/images/pic002.jpg)

[[Fuente](https://rodas5.us.es/items/6ee22068-3a5f-b78e-06a3-b06ace495ead/1/viewcontent?_sl.t=true)]

**Enlaces**:

* Wikipedia [**Coeficiente de determinación**](https://es.wikipedia.org/wiki/Coeficiente_de_determinaci%C3%B3n): En estadística, el coeficiente de determinación, denominado R² y pronunciado R cuadrado, es un estadístico usado en el contexto de un modelo estadístico cuyo principal propósito es predecir futuros resultados o probar una hipótesis. Hay varias definiciones diferentes para R² que son algunas veces equivalentes. Las más comunes se refieren a la regresión lineal. En este caso, el R² es simplemente el cuadrado del coeficiente de correlación de Pearson, lo cual es sólo cierto para la regresión lineal simple. Si existen varios resultados para una única variable, es decir, para una X existe una Y, Z... el coeficiente de determinación resulta del cuadrado del coeficiente de determinación múltiple. En ambos casos el R² adquiere valores entre 0 y 1. Existen casos dentro de la definición computacional de R² donde este valor puede tomar valores negativos.2​
* Wikipedia [**Coeficiente de correlación de Pearson**](https://es.wikipedia.org/wiki/Coeficiente_de_correlaci%C3%B3n_de_Pearson). En estadística, el coeficiente de correlación de Pearson es una medida lineal entre dos variables aleatorias cuantitativas. A diferencia de la covarianza, la correlación de Pearson es independiente de la escala de medida de las variables. De manera menos formal, podemos definir el coeficiente de correlación de Pearson como un índice que puede utilizarse para medir el grado de relación de dos variables siempre y cuando ambas sean cuantitativas y continuas.

### Modelo de regresión líneal multiple

La realidad demuestra que es más compleja y que un fenomeno suele estar afectado por muchos factores diferentes, si introducimos estas variables al modelo ya no estariamos antes un modelo de regresión simple sino antes un modelo de regresión líneal múltiple, este modelo sigue la misma ecuación que el modelo simple pero con más variables, por ejemplo:

![](images/formula_regresion_multiple_basica_02.PNG)

En este caso ya no se trataría de buscar la recta que mejor se ajusta a los puntos en el espacio bidimensional sino de buscar el mejor hiperplano en espacio multidimensional. 

Siguiendo el ejemplo anterior, factores como la criminalidad, que tenga buenos colegios o transporte público en las cercanías y otros muchos factores pueden ser determinantes para el cliente. 

![](images/formula_regresion_multiple_basica_03.PNG)

Se establece una correlación entre el número de atributos o características del problema y el numero de dimensiones, como forma de representación se usan matrices de valores "X" y vectores para valores "Y" e "W", matematicamente se reduce a la formula `Y=WX`.  

La matriz "X" donde cada columna representa una característica de los datos de entrada de nuestro problema, por ejemplo el número de habitaciones, el índice de criminalidad y otros. Cada fila de la matriz representa cada una de las mediciones del conjunto de datos. Formamos un vector de elementos "Y" y lo mismo para generar un vector de parámetros "W".

![](images/matriz_x_regresion_multiple_01.PNG) 


**Casos de uso**:

* Por ejemplo, puede predecir qué parte de las ventas totales anuales de un vendedor (la variable dependiente) se debe a variables independientes como la formación y los años de experiencia (las variables independientes).
* Para analizar la relación entre la edad y los ingresos familiares
* El caso que hemos mencionado para [pronósticar la demanda](https://ingenioempresa.com/regresion-lineal/).
* [Estudio](http://www.ics-aragon.com/cursos/salud-publica/2014/pdf/M2T04.pdf) [PDF] de correlación y regresión líneal entre la altura y el peso (Autor: Clara Laguna).

**Recomiendo los siguientes enlaces para seguir profundizando**:

* inbest.cloud ["MACHINE LEARNING: REGRESIONES PARA LA PREDICCIÓN FINANCIERA"](https://www.inbest.cloud/comunidad/machine-learning-regresiones-para-predicci%C3%B3n-financiera).
* ["REGRESIÓN LINEAL SIMPLE"](http://www.dcb.unam.mx/profesores/irene/Notas/Regresion.pdf) [PDF].
* ingenioempresa.com ["La regresión lineal para pronosticar la demanda"](https://ingenioempresa.com/regresion-lineal/).
* ["Coeficiente correlación de Pearson"](https://personal.us.es/vararey/adatos2/correlacion.pdf) [PDF].
* ["Correlación y regresión líneal"](http://www.ics-aragon.com/cursos/salud-publica/2014/pdf/M2T04.pdf) - Clara Laguna [PDF].

Algunos videos explicativos sobre el modelo de regresión simple:

{{< youtube k964_uNn3l0 >}}

Video explicativo práctico sobre como calcular la demanda por promedio simple y regresión lineal usando Excel. 

{{< youtube 6V8ftP9Sgas >}}

Pronostico de ventas Metodo de Minimos Cuadrados EJEMPLO Y EJERCICIO RESUELTO

{{< youtube JU-KjzW0QfM >}}

Ecuación de correlación lineal y coeficiente de correlación (Ejercicio 1):

{{< youtube fNeXC8d5En8 >}}

CORRELACIÓN DE PEARSON TEORÍA: 

{{< youtube wpIVG3JsQ6M >}}


## Aprendizaje basado en arboles de decisión

Un [**arbol de decisión**](https://es.wikipedia.org/wiki/Aprendizaje_basado_en_%C3%A1rboles_de_decisi%C3%B3n) es un **modelo predictivo** donde cada nodo representa una variable o categoria y las ramas representan resultados posibles de esa variable. Se trata de crear un modelo que predice el valor de un objetivo basado en una serie de inputs o entradas independientes.  

La metodología CART (Classification & Regression Trees) se usa referirse a los siguientes tipos de arboles de decisión:

* Arboles de clasificación.
* Arboles de regresión. 

Los elementos principales son de CART son:

* Las reglas para clasificar un dato basado en una variable de entrada.
* Reglas de parada para decidir cuando una rama es terminal y no puede volver a ser divido en nuevas ramas.
* Una predicción para la variable objetivo en cada nodo terminal.

El algoritmo CART se define como una secuencia de preguntas, las respuestas determinan la siguiente pregunta. El resultado final es como un diagrama de arbol invertido, los finales o hojas son nodos donde acaban las preguntas. 

El siguiente ejemplo corresponde al gráfico del clásico juego 'Piedra, papel o tijera'.

![](https://es.wikipedia.org/wiki/%C3%81rbol_de_decisi%C3%B3n#/media/File:%C3%81rbol_de_decisi%C3%B3n_del_juego_%27Piedra,_papel_o_tijera%27.png)

[[Fuente](https://es.wikipedia.org/wiki/%C3%81rbol_de_decisi%C3%B3n)]

Enlaces externos:

* datasciencecentral.com ["Introduction to Classification & Regression Trees (CART)"](https://www.datasciencecentral.com/profiles/blogs/introduction-to-classification-regression-trees-cart) - Venky Rao on January 13, 2013.

### Random forest

[Random forest](https://es.wikipedia.org/wiki/Random_forest) es un algoritmo popular basado en [arboles de decisión](https://es.wikipedia.org/wiki/Aprendizaje_basado_en_%C3%A1rboles_de_decisi%C3%B3n), consiste en una combinación de múltiples árboles de decisión que juntos conforman un “bosque”.


## Clustering K-means


# Redes neuronales

Son modelos de regresión no lineales. 

![](https://gogul09.github.io/images/software/first-neural-network-keras/mlp-1.png)

[[Fuente](https://gogul09.github.io/software/first-neural-network-keras)]

# Librerias y herramientas

# Lenguaje R

Enlaces:

* ["Ejemplo práctico de regresión lineal simple, múltiple, polinomial e interacción entre predictores"](https://rpubs.com/Joaquin_AR/254575).
* ["6 Steps to Create Your First Deep Neural Network using Keras and Python"](https://gogul09.github.io/software/first-neural-network-keras).

# Aplicaciones 

## TensorFlow

TensorFlow biblioteca de código abierto para ML desarrollada por Google.

**Enlaces TensorFlow**:

# Pentaho

[**Pentaho**](https://es.wikipedia.org/wiki/Pentaho) es un conjunto de aplicaciones de software libre para BI (Business Inteligence)


## Competera

## Wiser (QuadAnalytics)

## Market Track

## Amazon machine learning (ML)

Es un servicio potente y basado en la nube que permite a los desarrolladores usar fácilmente la tecnología de aprendizaje automático sin necesidad de aprender complejos algoritmos. 

Puede utilizar enfoques de ML supervisados para estas tareas específicas de aprendizaje automático: clasificación binaria (predicción de uno de dos posibles resultados, por ejemplo si el cliente comprará un determinado producto), clasificación multiclase (predicción de uno o de más de dos resultados) y regresión (predicción de un valor numérico como el precio de un producto).

# Google Cloud Platform 



# Conclusiones

Es imprescindible dominar la terminología y una comprensión al menos básica de los modelos que apliquemos teniendo en mente el objetivo que buscamos. La definición del problema debe ser clara ya que podemos diferentes tipos de métricas basadas en el beneficio, la demanda o los ingresos obtenidos.

La **selección de datos de calidad, transformación y análisis es esencial**, tanto o más que el propio modelo que apliquemos, puede ser una tarea laboriosa pero establece los cimientos para que el proyecto culmine con exito. 

Software como servicio VS plataformas internas. Podemos empezar alquilando por un tiempo alguna solución basada en la nube.

**Compatibilidad y interoperabilidad** con los sistemas existentes, deberia ser capaz de integrarse con los sistemas que ya usamos en nuestro negocio como ERP y CRM.

Es importante **evitar complicar excesivamente el problema** y enmarcar la solución más sencilla que satisfaga sus necesidades. Sin embargo, también es importante evitar la pérdida de información, especialmente información en las respuestas históricas

# Enlaces externos

* sweetpricing.com ["How Dynamic Pricing Uses Machine Learning to Increase Revenue"](https://sweetpricing.com/blog/2017/02/machine-learning/).
* blog.statsbot.co ["Strategic Pricing in Retail with Machine Learning"](https://blog.statsbot.co/strategic-pricing-d1adcc2e0fd6).
* pwc.com ["2018 Global Consumer Insights Survey"](https://www.pwc.com/gx/en/industries/consumer-markets/consumer-insights-survey.html).
* cloud.google.com ["Using machine learning for insurance pricing optimization"](https://cloud.google.com/blog/products/gcp/using-machine-learning-for-insurance-pricing-optimization).
* news.mit.edu ["A machine-learning approach to inventory-constrained dynamic pricing"](https://news.mit.edu/2018/thompson-sampling-machine-learning-inventory-constrained-dynamic-pricing-0315).
* blog.aimultiple.com ["Dynamic pricing: In-depth Guide to Improved Margins [2019]"](https://blog.aimultiple.com/dynamic-pricing/).
* blog.aimultiple.com ["Dynamic pricing is the strongest profitability lever for a large company"](https://blog.aimultiple.com/pricing-optimization/).
* datasciencecentral.com ["Price Optimisation Using Decision Tree (Regression Tree) - Machine Learning"](https://www.datasciencecentral.com/profiles/blogs/price-optimisation-using-decision-tree-regression-tree).
* ["Decision Tree Classification and Forecasting of Pricing Time Series Data"](http://www.diva-portal.org/smash/get/diva2:746332/FULLTEXT01.pdf) EMIL LUNDKVIST [PDF].
* linkedin.com ["https://www.linkedin.com/pulse/5-algorithms-every-pricing-director-should-know-manu-carricano-phd/"](5 Algorithms every Pricing Director should know).
* ["Dynamic Pricing and Learning"](https://www.math.vu.nl/~mei/theses/arnoud/proefschrift.pdf) A.V. den Boer [PDF].
* mindtools.com [Decision Trees - Choosing by Projecting "Expected Outcomes"](https://www.mindtools.com/dectree.html).
* researchgate.net ["Online Self-Adaptive Cellular Neural Network Architecture for Robust Time-Series Forecast"](https://www.researchgate.net/publication/309590088_Online_Self-Adaptive_Cellular_Neural_Network_Architecture_for_Robust_Time-Series_Forecast).
* inbest.cloud ["MACHINE LEARNING: REGRESIONES PARA LA PREDICCIÓN FINANCIERA"](https://www.inbest.cloud/comunidad/machine-learning-regresiones-para-predicci%C3%B3n-financiera).
* ["Machine Learning para dummies"](https://www.paradigmadigital.com/techbiz/machine-learning-dummies/).

Aplicaciones en línea para dibujar gráficas:

* [XY - Online Chart Tool](https://www.onlinecharttool.com/): Soporta diferentes tipos de gráficos parametrizables. 

Aplicaciones en línea para aplicar el modelo de regresión líneal:

* [GraphPad - Linear regression calculator](https://www.graphpad.com/quickcalcs/linear1/). Introduciendo o copiando y pegando los valores X e Y muestra la gráfica de dispersión de los puntos y la recta de regresión líneal junto con el conjunto de datos resultantes como la pendiente o el valor [R Cuadrado (Coeficiente de determinación)](https://economipedia.com/definiciones/r-cuadrado-coeficiente-determinacion.html).



# Imágenes

