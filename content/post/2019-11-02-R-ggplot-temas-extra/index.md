---
title: Temas visuales extra para ggplot [borrador]
subtitle: Cambia el aspecto visual de los diagramas
date: 2019-11-02
draft: true
description: Ciencia de datos con R
tags: ["R","R - ggplot","R - Diagramas"]
---

## hrbrthemes

["hrbrthemes"](https://hrbrmstr.github.io/hrbrthemes/) ofrece principalmente nuevas fuentes tipográficas para los textos que acompañan los diagramas.

El paquete se puede obtener de varios repositorios, yo he optado por descargarlo de GitLab:

```r
if (!require(hrbrthemes)) {remotes::install_gitlab("hrbrmstr/hrbrthemes")}
```

Además del paquete "hrbrthemes" cargo los paquetes "tidyverse" y ["gcookbook"](https://cran.r-project.org/web/packages/gcookbook/index.html) con _datasets_  usados en el libro ""R Graphics Cookbook" (Winston Chang - O'Reilly Media).

Para saber que versión de ["hrbrthemes"](https://hrbrmstr.github.io/hrbrthemes/)  estamos usando:

```r
> packageVersion("hrbrthemes")
[1] ‘0.7.2’
```

El tema básico `theme_ipsum` usa la fuente Arial Narrow disponible casi en cualquier sistema, el paquete incorpora seis temas en total, para apreciar como cambia el diagrama a continuación he usado `theme_ft_rc` con el tipo de fuente Roboto Condensed y la paleta de colores del tema oscuro FT:

```r
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ,y = hwy,colour=class)) + 
  labs(x="Fuel efficiency (mpg)", y="Weight (tons)",
       title="Modern Roboto Condensed typography",
       subtitle="A plot that is only useful for demonstration purposes",
       caption="Brought to you by the letter 'g'") +
  theme_ft_rc() +
  scale_color_ft()
```

Que produce este diagrama:

![](images/01.png)

## firatheme

El paquete ["firatheme"](https://github.com/vankesteren/firatheme) de Erik-Jan van Kesteren usa la fuente de Mozilla [Fira Sans](https://mozilla.github.io/Fira/). 

Instalación:

```r
if (!require(firatheme)) {devtools::install_github("vankesteren/firatheme")}
```

Voy a crear el mismo tipo de diagrama que en el ejemplo anterior aplicando este nuevo tema:

```r
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ,y = hwy,colour=class)) + 
  labs(x="Fuel efficiency (mpg)", y="Weight (tons)",
       title="firatheme is based on Fira Sans",
       subtitle="A plot that is only useful for demonstration purposes",
       caption="Brought to you by the letter 'g'") + 
  theme_fira() +
  scale_colour_fira()
```

El resultado:

![](images/02.png)

La función `scale_colour_fira` ofrece una paleta de colores 


## Código fuente



## Enlaces externos

- [R/theme-defaults.r](https://ggplot2.tidyverse.org/reference/ggtheme.html): Temas por defecto con ggplot.
- [Themes to Improve Your ggplot Figures](https://rfortherestofus.com/2019/08/themes-to-improve-your-ggplot-figures/): Fuente de este post.
- [hrbrmstr/hrbrthemes](https://github.com/hrbrmstr/hrbrthemes): Repositorio en GitHub.
