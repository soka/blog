---
title: Modelos de priorización de requisitos
subtitle: Análisis de diferentes modelos y sus beneficios en diferentes escenarios
date: 2019-06-16
draft: true
description: MoSCoW, Kano, Eisenhower, RICE...
tags:
  ["Técnicas", "Proyectos", "Tareas", "Gestión", "Equipos", "Requerimientos"]
---

**Establecer las prioridades** para tener éxito en el desarrollo de un proyecto no siempre es fácil, requiere un orden claro en el el desarrollo de las funcionalidades que lo componen, conocer el impacto y de cada uno o saber ponderar la importancia de cada uno de ellos en relación a los otros.

# Método MoSCoW

![](img/03.PNG)

Daig Clegg en 1994 ideó el método **MoSCoW** como técnica para priorizar requerimientos, se aplica en diversos ámbitos como pueden ser análisis de un negocio, gestión de proyectos o el desarrollo de un nuevo software.

El termino **MoSCoW** es un acrónimo formado por las primeras letras de cada tipo de prioridad, se resume en las siguientes cuatro categorías: _MUST have_ (debe tener); _SHOULD have_ (debería tener); _COULD have_ (podría tener); _WON’T have_ (no tendrá).

En contraste con la forma típica de priorizar (alta, media, baja) donde al final da igual el nivel y todo se debe hacer (además esta forma de priorizar no ofrece una promesa clara sobre que esperar del producto final), con el método **MoSCoW** a medida que analizamos los requisitos nos damos cuenta que no todo es obligatorio ni hay que hacerlo en este momento, **la escala que usamos tiene un significado intrínseco** frente a otras técnicas tradicionales, las elecciones realizadas tendrán un impacto real sobre el producto final.

## Categorías "Must, Should, Could, Won’t"

- **MUST have (debe tener)**: Se pueden considerar como atributos que determinan el MVP ([_minimum viable product_](https://en.wikipedia.org/wiki/Minimum_viable_product)), son **requerimientos indispensables o mínimos** y normálmente no son negociables, sino se logra el conjunto de requerimientos de este tipo se considera que el proyecto no ha tenido éxito.
- **SHOULD have (debería tener)**: Este tipo de requisitos suelen ser **altamente deseables** pero no cruciales. Puede tratarse de funcionalidades que no son tan urgentes en este momento (pero puede que lo sean en un periodo corto de tiempo). Este tipo de atributos se centran en obtener un mayor [retorno de la inversión ROI](https://es.wikipedia.org/wiki/Retorno_de_la_inversi%C3%B3n), se pueden evaluar midiendo la cantidad de usuarios afectados por este requisito. **El producto final aún sigue siendo plenamente funcional** sin estos requerimientos.
- **COULD have (podría tendría)**: También se les conoce como _nice-to-have_. A pesar de que pueden ser interesantes no suelen ser importantes, **no llevarlos a cabo no reduce la viabilidad del proyecto**, no tienen gran impacto en el resultado final. Pueden ser funcionalidades extra que se acometan siempre y cuando quede tiempo y dinero una vez realizadas las anteriores o no exijan excesivo esfuerzo.
- **Won’t have/ Would-have (no tendría)**: Requisitos o deseos que **no merecen aún la pena invertir en ellos**. También se conocen como _would have_ porque una empresa podría implementarlos bajo ciertas condiciones en el futuro.

## Conclusiones, pros y contras del modelo MoSCoW

Está técnica se implementa de forma **rápida y es sencilla de aplicar**. Si se realiza en equipo todas las personas integrantes forman parte de la decisión de la prioridad de cada trabajo a realizar y cual debe ser el resultado final del conjunto.

Permite redirigir los recursos (materiales, de personas o tiempo) de los que disponemos a las categorías más importantes, **si en algún momento te quedas sin recursos para seguir trabajando, te aseguras que lo más importante siempre está hecho**.

Los deseos y requisitos nunca son estáticos, dependen de la situación y el momento, **pueden cambiar de categoría a través del tiempo**.

Este método se suele aplicar **cuando existen plazos límite de entrega y debemos poner el foco en los requisitos más importantes**.

Esta técnica está muy **dirigida a aspectos funcionales o atributos que proporcionan un valor añadido sobre un producto final**, puede casar muy bien para definir MVP de un nuevo proyecto.

Algunos enlaces interesantes:

- Wikipedia ["MoSCoW method"](https://en.wikipedia.org/wiki/MoSCoW_method).
- ticportal.es ["Método MoSCoW - Una manera uniforme de priorizar con el método MoSCoW"](https://www.ticportal.es/glosario-tic/metodo-moscow).
- ["Técnica de priorización MoSCoW"](https://www.laboratorioti.com/2016/09/26/tecnica-priorizacion-moscow/).
- ["Time Boxing Planning: Buffered Moscow Rules"](https://www.stickyminds.com/article/time-boxing-planning-buffered-moscow-rules).
- agilebusiness.org ["MoSCoW Prioritisation"](https://www.agilebusiness.org/content/moscow-prioritisation)
  http://creativehero.es/tototo-method/
  https://www.hotpmo.com/management-models/moscow-kano-prioritize

# El modelo Kano y la satisfacción del cliente

El [**modelo Kano**](https://es.wikipedia.org/wiki/Modelo_de_Kano) desarrollado por Noriaki Kano en los años 80 es una herramienta analítica que relaciona los atributos de un producto con el nivel de satisfacción del cliente.

## Grados del modelo Kano

**Establece cinco niveles basados en como afectan los atributos en la satisfacción del cliente**:

- **Atractivo**: Las funcionalidades que pertenecen a esta categoría producen satisfacción extra y no son esperados por el cliente, en el otro extremo la no implementación no produce insatisfacción en el cliente. Son detalles con los que la empresa es capaz de sobrepasar las expectativas del cliente.
- **Unidimensional**: Estos tienen una naturaleza dual, a pesar de que no son indispensables para que el producto final funcione son muy deseables para el cliente, no cumplir con ellos deriva en un cliente insatisfecho. Este tipo de atributos pueden marcar la diferencia con nuestros competidores, son atributos que el cliente valora, agradece y que al mismo tiempo los utilizará para hacer comparaciones y decidir con cuál oferente se queda.
- **Requerido**: Los clientes dan por sentado que el producto final contiene estos atributos, cumplirlos por tanto no aumenta la satisfacción del cliente, el cliente los considera básicos.
- **Indiferente**: No tienen impacto en la satisfacción del cliente, no son buenos ni malos.
- **Inversa**: Las funcionalidades que caen en esta categoría son consideradas molestas, su presencia provoca un efecto negativo en la satisfacción del cliente.

## Conclusiones, pros y contras del modelo

Uno de los valores que predomina en este modelo se basa en involucrar y obtener el _feedback_ del cliente, las funcionalidades del producto se priorizan basadas en el valor que aportan al cliente.

Nos puede ayudar a determinar si es conveniente o no invertir en agregar o no un determinado atributo. ¿El cliente estará o no dispuesto a pagar por este atributo adicional?.

Los deseos y anhelos del cliente no suelen ser uniformes, identificar el segmento de mercado y cliente objetivo al que nos queremos dirigir y establecer un sistema para recoger sus expectativas respecto a un futuro producto puede ser muy trabajoso y retrasar el arranque del proyecto.

El **modelo Kano** se basa en la opinión del cliente y sus conocimientos, una de las posibles debilidades es que el propio cliente no tiene un perfil técnico y el producto final está basado en una lista de deseos planos.

Este modelo no aporta detalles sobre los recursos y tiempo necesarios para desarrollar una funcionalidad, es deseable que se convine con otros métodos.

Enlaces sobre el **modelo Kano**:

- Wikipedia ["Modelo Kano"](https://es.wikipedia.org/wiki/Modelo_de_Kano).
- ["Modelo de Kano - Definición, qué es y concepto | Economipedia"](https://economipedia.com/definiciones/modelo-de-kano.html).

# RICE

El acrónimo RICE se refiere a cuatro factores que usaremos para evaluar las ideas.

## Reach, Impact, Confidence, and Effort

- **Reach**: Se basa en estimar cuantas personas se verán afectadas por una funcionalidad, pone el foco en una medida real.
- **Impact**: El impacto refleja la contribución de una funcionalidad en el producto.
- **Confidence**:
- **Effort**: Se trata de estimar el tiempo necesario que requiere el desarrollo de una funcionalidad.

Enlaces:

- ["RICE Scoring: Quick Prioritization for Product Managers | Hygger Blog"](https://hygger.io/blog/4-powerful-factors-rice-scoring-model/).

# Matriz de Eisenhower

> ""Dicen que para lograr el éxito nunca debes olvidar cual es el objetivo""

Con el objetivo en mente debes **alinear tus decisiones** para su consecución.

![](img/eisenhower.png)

Conocemos a [Dwight D. Eisenhower](https://es.wikipedia.org/wiki/Dwight_D._Eisenhower) (1890 - 1969) por ser el trigésimo cuarto presidente de los EE.UU. Vivió la segunda guerra mundial en vivo y en directo en el frente, tras la guerra fue jefe del estado mayor del ejercito y en 1952 se involucro en política de la mano del partido republicano. Su productividad es legendaria. Era considerado un maestro de la administración del tiempo; tenía la habilidad de hacer todo cuando era necesario hacerlo.

Pensaba que debemos dedicar atención y tiempo a nuestras actividades en función de su importancia y urgencia.

## ¿Qué es Urgente?

Algo es urgente cuando requiere una atención inmediata. La urgencia tiene que ver con la temporalidad de la tarea y con las consecuencias que acarrea no hacerla. Algo urgente puede se por ejemplo mandar un correo electrónico hoy porqué finaliza un plazo.

# WSJF: Weighted Shortest Job First

# Walking Skeleton

# Enlaces internos

- ["Ley de Parkinson"](https://soka.gitlab.io/blog/post/2019-05-21-ley-de-parkinson/): El trabajo se expande hasta que llena todo el tiempo disponible para su realización.
- ["Mapas mentales para equipos creativos"](https://soka.gitlab.io/blog/post/2019-02-04-mapas-mentales/).
- ["Matriz de riesgos"](https://soka.gitlab.io/blog/post/2019-01-23-matriz-de-riesgos/): Evaluación de riesgos en nuevos proyectos.

# Enlaces externos

- ["The Most Popular Prioritization Techniques and Methods: MoSCoW, RICE, KANO model, Walking Skeleton, and others"](https://www.altexsoft.com/blog/business/most-popular-prioritization-techniques-and-methods-moscow-rice-kano-model-walking-skeleton-and-others/).
