---
title: Inscrustar un formulario de Google en WordPress [borrador]
subtitle: Integra formularios y otros productos de Google en WP
date: 2020-12-02
draft: false
description: Rápido y sencillo
tags: ["Wordpress","WP - Curso","WP","WP - Google","WP - Formularios","Forms","Google"]
---

![](img/wordpress-logo-01.png)

Los [**formularios de Google**](https://www.google.com/intl/es_es/forms/about/) tienen algunas **ventajas frente a otros basados en plugins** como [Contact Form 7](https://soka.gitlab.io/blog/post/2020-11-14-curso-wp-7/):

- Está libre de dependencias respecto a WP, se puede distribuir mediante un enlace o incrustado en cualquier Web o correo electrónico.
- No requiere plugins adicionales que pueden afectar al rendimiento del sitio Web.
- Podemos a invitar a colaborar a otros usuarios diseñando el formulario Web sin necesidad de crear más usuarios en WP. 
- Como las respuestas quedan registradas en una hoja de cálculo de Google se pueden tratar los datos fácilmente y obtener reportes e informes.
- ¡Son muy fáciles de crear y configurar!

Algunos tipos de formularios más comunes en un sitio Web:

- El omnipresente formulario de contacto.
- Registro a un evento.
- Encuestas y sondeo.
- Suscripción a mediante correo electrónico a un servicio de noticias o información.
- Pedido de productos.

**NOTA**: Para seguir este tutorial es necesario tener una cuenta de Google.

Lo que más tiempo lleva es construir el propio formulario, insertarlo en WordPress es realmente sencillo usando un [`<iframe>`](https://developer.mozilla.org/es/docs/Web/HTML/Elemento/iframe) HTML en una entrada, página o un _widget_ en cualquier sitio de nuestra Web, veremos como hacerlo al final del artículo.

## Paso 1: Crear un formulario

Para crear un formulario accedemos a [Google Drive](https://drive.google.com/) y usamos el botón **Nuevo > Formularios de Google** para añadir un nuevo formulario en la carpeta actual, podemos hacer lo mismo con el botón derecho del ratón sobre el contenido de la carpeta, nos mostrará un menu emergente (o accediendo al portal de formularios [aquí](https://docs.google.com/forms/u/1/)). Podemos acceder directamente a la creación del formulario usando este enlace [forms.google.com](forms.google.com). **Como sugerencia o buena práctica recomiendo crear una carpeta para albergar el formulario y la hoja de cálculo que recibirá las respuestas**, así queda ordenado y agrupado.

Este es el aspecto inicial recién creado el formulario:

![](img/01.png)

## Paso 2: Editar un formulario

Lo primero es definir un título para el formulario y el propio documento:

![](img/02.png)

Debajo del título se puede añadir una descripción opcional, por ejemplo con indicaciones de como completarlo o información sobre el propósito del mismo. **La descripción no permite definir estilos**, es básicamente texto plano.

Voy a crear un formulario típico con una encuesta de satisfacción de un taller de formación por ejemplo. La encuesta consta de varias secciones donde se agrupan las preguntas por temas, la primera sección será para registrar los datos personales básicos del usuario, la mayoría de los campos de esta sección serán opcionales para que la encuesta pueda ser anónima.

### Añadir una sección

Añade secciones para que leer y completar tu formulario resulte más sencillo, haz clic en Añadir sección y ponle un nombre a la sección.

![](img/03.png)

Podemos condicionar la navegación entre secciones, por ejemplo después de rellenar la primera podemos saltar a la tercera.

### Añadir preguntas

Con el botón "+" podemos añadir nuevas preguntas:

- Respuesta corta.
- Párrafo.
- Selección múltiple.
- Casilla de verificación.
- Desplegable
- Subida de archivo.
- Escala lineal.
- Rejilla selección múltiple.
- Tick box grid.
- Fecha.
- Hora.

Añado respuestas cortas para el nombre y el correo electrónico, también un párrafo que permite introducir textos largos para las sugerencias generales, todos los campos serán opcionales.

![](img/04.png)

Se pueden reordenar las preguntas arrastrándolas arriba y abajo sin problema, también se pueden duplicar o borrar en cualquier momento.

Ahora añado una nueva sección para agrupar preguntas con una escalar líneal del 1 al 5 para evaluar la satisfacción con los "Contenidos y documentación" y del curso, las respuestas serán obligatorias (se marcan de forma automática con un "*" rojo):

![](img/05.png)

En cualquier momento podemos visualizar como está quedando usando el icono con el ojo.

![](img/06.png)

## Paso 3: Personaliza el aspecto

Podemos añadir una imagen de cabecera y cambiar los colores y la fuente tipográfica.

![](img/07.png)

## Paso 4: Configuración

La primera pestaña permite recopilar las direcciones de correo electrónico de los encuestados y que estos además reciban si queremos una notificación por correo electrónico. Si marcamos estas opciones no es necesario crear un campo corto de forma manual como he hecho al principio, automáticamente se añade un campo nuevo a nuestro formulario (obligatorio). Podemos limitar el número de respuestas de un usuario (requiere que tenga cuenta en Google), si queremos podemos hacer que los encuestados editen sus respuestas después de envíar el formulario o que puedan visualizar las respuestas.

![](img/08.png)

El segundo apartado permite mostrar una barra de progreso, ordenar las preguntas de manera aleatoria o que se pueda repetir la encuesta.

![](img/09.png)

La tercera opción es especial, permite crear concursos de preguntas con puntuación.

![](img/10.png)

## Paso 5: Envía el formulario

El último paso es envíar el formulario, podemos hacerlo mediante correo electrónico, proporcionando un enlace o la última opción que es la que nos interesa, como código HTML que podemos incrustar en un sitio Web como WP.

![](img/11.png)

## Paso 6: WordPress

Este paso no reviste complejidad, copiamos y pegamos el código en un bloque HTML personalizado.

![](img/13.png)

## Respuestas

A medida que recibamos respuestas se irán mostrando diversas gráficas con información y resumen.

![](img/14.png)

Para almacenar las respuestas para su posterior tratamiento y análisis en una hoja de cálculo usamos el icono de Google Spreadsheet para crear una nueva hoja:

![](img/15.png)

En el menú lateral podemos habilitar que nos lleguen notificaciones por correo electrónico cada vez que alguien rellena el cuestionario. En cualquier momento podemos decidir que no queremos aceptar más respuestas.

## Editar el diseño usando CSS

[Esta Web](https://googleformrestyler.apixml.net/) facilita retocar el estilo del formulario:

![](img/16.png)

Una vez realizados los pasos de arriba vamos al editor de temas:

Por ejemplo si queremos quitar el logo de Google del pie:

```css
.freebirdFormviewerViewFooterFooterContainer {
    display: none !important
}
```

## Enlaces externos

- Usar Formularios de Google [https://support.google.com/docs/answer/6281888?co=GENIE.Platform%3DDesktop&hl=es](https://support.google.com/docs/answer/6281888?co=GENIE.Platform%3DDesktop&hl=es)
- [https://kinsta.com/es/blog/insertar-google-form/](https://kinsta.com/es/blog/insertar-google-form/)
- [https://wordpress.org/plugins/wpgform/](https://wordpress.org/plugins/wpgform/).


