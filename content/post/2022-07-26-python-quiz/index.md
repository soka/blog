---
title: Test Python
subtitle: Pon a prueba tus conocimientos de programación
date: 2022-07-26
draft: false
description: Pon a prueba tus conocimientos de programación
tags: ["Python","Retos","Aplicaciones","Juegos","ASCII","Hackathon","Dungeon crawl","Proyectos"]
---


<!-- vscode-markdown-toc -->
* 1. [01](#)
* 2. [02](#-1)
* 3. [03](#-1)
* 4. [04](#-1)
* 5. [05](#-1)
* 6. [06](#-1)
* 7. [07](#-1)
* 8. [08](#-1)
* 9. [09](#-1)
* 10. [10](#-1)
* 11. [11](#-1)
* 12. [12](#-1)
* 13. [13](#-1)
* 14. [14](#-1)
* 15. [15](#-1)
* 16. [16](#-1)
* 17. [17](#-1)
* 18. [18](#-1)
* 19. [19](#-1)
* 20. [20](#-1)
* 21. [21](#-1)
* 22. [22](#-1)
* 23. [23](#-1)
* 24. [24](#-1)
* 25. [25](#-1)
* 26. [26](#-1)
* 27. [27](#-1)
* 28. [28](#-1)
* 29. [29](#-1)
* 30. [30](#-1)
* 31. [31](#-1)
* 32. [32](#-1)
* 33. [33](#-1)
* 34. [34](#-1)
* 35. [35](#-1)
* 36. [36](#-1)
* 37. [37](#-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name=''></a>01

Dada esta lista `lst_num = [3,4,5]` ¿Cuál de las opciones nos permite obtener una lista con los siguientes valores : `[9,16,25]`?

* `output = [n * 2 for n in lst_num]` 
* **ok** `output = [n ** 2 for n in lst_num]` 
* `output = [n ** 2 for lst_num in n]`
* `output = [n ^2 for n in lst_num]`

##  2. <a name='-1'></a>02

Completa la línea de código faltante para obtener los textos en mayúscula a partir de la siguiente lista:

`lst_lp = ["python", "c", "java","php"]`

* **ok** `output = [lp.upper() for lp in lst_lp]`
* `output = [lp.capitalize() for lp in lst_lp]`
* `output = [lp.lower() for lp in lst_lp]`
* `output = [lp.uppercase() for lp in lst_lp]`

##  3. <a name='-1'></a>03 

¿Qué elementos tendrá la lista al ejecutar el siguiente código?

![](img/01.png)

```python
num=[1,2,3,4]
out=[n-1 for n in num if n<= 3]
print(out) 
``` 

* `[1,2,3]`
* `[1, 2, 3, 4]`
* `[0,1, 2]` **ok**
* `[0,1, 2, 3]`

##  4. <a name='-1'></a>04

¿Qué elementos tendrá la lista después de la ejecución del siguiente código?

![](img/02.png)

```python
lst_test_a=[9,34.9,'Python']
lst_test_a.append('Flask')
```

* `['Flask', 34.9, 'Python']`
* **ok** `[9, 34.9, 'Python', 'Flask']`
* `['Flask', 9, 34.9, 'Python']`
* `[9, 34.9, 'Flask']` 

##  5. <a name='-1'></a>05

¿Qué instrucción es necesaria para obtener los siguientes elementos: [1,5,10,15] en la lista "lst_test_b"?

```python
lst_test_b = [1,10,15]
```

* **ok** `lst_test_b.insert(1,5)`
* `lst_test_b.extend(5,1)`
* `lst_test_b.append(2,5)`
* `lst_test_b.insert(5,1)`

##  6. <a name='-1'></a>06

¿Qué elementos tendrá la lista al ejecutar el siguiente código?

```python
lst_test_c = [3,'5',2,5,2,5,5]
lst_test_c.remove(5)
```

* `[3, 2, 5, 2, 5, 5]`
* `[3,'5', 2, 5, 2, 5]` 
* `[3, 2, 2]`
* **ok** `[3, '5', 2, 2, 5, 5]`

##  7. <a name='-1'></a>07 

¿Cuál es la salida en el siguiente bloque de instrucciones?

```python
lst_test_d = ['PHP','Python','Go','Java']
language = lst_test_d.pop()
print(language) 
```

* PHP
* **ok** Java
* Python
* ['PHP','Python','Go', 'Java'] 

##  8. <a name='-1'></a>08

El método clear se utiliza para borrar todos los elementos de la lista.

* **ok** Verdadero
* Falso

##  9. <a name='-1'></a>09

Complete la palabra reservada de python en el siguiente programa:

![](img/04.png)

* elseif 
* **ok** elif 
* else 
* else if 

##  10. <a name='-1'></a>10

¿Cuál es la salida que produce este bloque de código en python?

![](img/05.png)

```python
x=0
while x<3:
    x+=1
else:
    print(x) 
```

* 0
* 1
* 2
* **ok** 3

##  11. <a name='-1'></a>11

¿Cuál es la salida que produce este bloque de código en python?. 

![](img/06.png)

```python
for num in range(5):
    print(num)
```

* **ok** 0,1,2,3,4 
* 0,1,2,3,4,5 
* 1,2,3,4,5 
* 1,2,3,4 

##  12. <a name='-1'></a>12

¿Qué números imprime el siguiente código en python?

```python
x = range(7,15,2)
for n in x:
    print(n)
``` 

* 7, 9, 11,13 y 15 
* 9, 11,13 y 15 
* **ok** 7, 9, 11 y 13 
* 9, 11 y 13 

##  13. <a name='-1'></a>13

Si utilizo el operador + con 2 cadenas de caracteres, me da un error?

Ejemplo: `print('Python'+'Senior')` 

* Verdadero
* **ok** Falso

##  14. <a name='-1'></a>14

Si utilizo el operador - con 2 cadenas de caracteres, me da un error?

Ejemplo: `print('1'-'2')` 

* **ok** Verdadero
* Falso

##  15. <a name='-1'></a>15 

Si utilizo el operador * tal se muestra en el código siguiente. ¿Cuál es el resultado obtenido?

```python
print('dos'*3) 
```

* seis 
* 6 
* **ok** dosdosdos 
* dos3 

##  16. <a name='-1'></a>16

¿Qué devuelve Python si ejecuto esta instrucción?

```python
print('y' in 'Python') 
```

* **ok** True
* False 

##  17. <a name='-1'></a>17

¿Qué devuelve Python si ejecuto esta instrucción?

```python
print('bot' not in 'Yo Robot') 
``` 

* True
* **ok** False 

##  18. <a name='-1'></a>18

¿Cómo obtengo el elemento cuyo valor es 'España' de la lista de países (lst_paises)?

![](img/08.png)

* `lst_paises[-1]`
* `lst_paises[2]`
* **ok** `lst_paises[3]`
* `lst_paises[4]`


##  19. <a name='-1'></a>19

Las listas en Python no pueden contener elementos de diferentes tipos de datos. ¿El siguiente código provocaría un error?

![](img/09.png)

```python
lst_dif = ['Juan',700,True,3.5]
```

* Verdadero
* **ok** Falso

##  20. <a name='-1'></a>20

Las listas pueden tener como elementos a otras listas, tal como se muestra en la imagen.

![](img/10.png)

```python
lst_numeros = [10,9,[8.5,9,[3,4,5]],8,5,7]
```

* **ok** Verdadero
* Falso

##  21. <a name='-1'></a>21

¿Qué valor devuelve el siguiente código?

![](img/11.png)

```python
lista_a=[10,20,5,6,10]
print(lista_a[-1] == lista_a[0])
```

* **ok** Verdadero
* Falso

##  22. <a name='-1'></a>22

¿Qué instrucción es necesaria para poder obtener una lista con estos elementos [67, 8, 90, 3]?

![](img/12.png)

```python
lst_n = [56,78,67,8,90,3,2,1]
```

* lst_n[2:4]
* lst_n[2:5]
* lst_n[1:-3]
* **ok** lst_n[2:-2]

##  23. <a name='-1'></a>23

¿Qué elementos devuelve la ejecución del siguiente código?

![](img/13.png)

```python
lst_2=[43,28,12,5,0,1,2]
new_lst=lst_2[3:]
print(new_lst)
```

* []
* [5]
* **ok** [5, 0, 1, 2]
* [0,1,2]

##  24. <a name='-1'></a>24

¿Qué elementos devuelve la ejecución del siguiente código?

![](img/14.png)

```python
lst_3 = [True,False,False,True]
new_lst=lst_3[:]
print(new_lst) 
```

* **ok** [True, False, False, True]
* [False, True, True, False]
* [True]
* [False]

##  25. <a name='-1'></a>25

¿Qué instrucción es necesaria para ordenar la lista mostrada en la imagen?

![](img/15.png)

```python
lst_4=['Javier','Benito','Pedro','Alfredo','Francisco','Eduardo']
```

* lst_4.sorted()
* **ok** lst_4.sort()
* lst_4.sorted
* lst_4.order()

##  26. <a name='-1'></a>26

¿Qué elementos devuelve la ejecución del siguiente código?

![](img/16.png)

```python
lst_5=[78,36,13,6,9,83,91]
new_lst=lst_5[0:3]
print(new_lst)
```

* [ 6, 9, 83, 91]
* [9, 83, 91]
* **ok**  [78, 36, 13]
* [36, 13, 6]

##  27. <a name='-1'></a>27

¿Qué valor devuelve la ejecución del siguiente código?

![](img/17.png)

```python
lst_6=['Python','PHP','Java','Javascript']
indice=lst_6.index('Abap')
```

* -1
* False
* True
* **ok** Error 

##  28. <a name='-1'></a>28

¿Cuál de estas líneas no producirá un error?

* **ok** def funcion():
* define funcion():
* include funcion():
* define funcion(){ continue }

##  29. <a name='-1'></a>29

Python es un lenguaje...

* Compilado
* Anidado
* Hackfer
* **ok** Interpretado
* Importado
* Stranger

##  30. <a name='-1'></a>30

¿Cuál de estas líneas producirá diez iteraciones sobre un bloque de código?

* for (x = 0; x < 10; x++){ }
* for x in range(9):
* while True:
* **ok** for x in range(10):

##  31. <a name='-1'></a>31

¿Cuál de estas librerías es utilizada para conectar a un sitio web mediante HTTP?

* httpcont
* connect
* quihttp
* **ok** requests

##  32. <a name='-1'></a>32

En python 3, ¿Cuál de estas lineas pide al usuario que ingrese un dato?

* **ok** input("Ingresa algo")
* int("Ingresa algo")
* rawinput("Ingresa algo")
* raw_input("Ingresa algo")

##  33. <a name='-1'></a>33

¿Cómo se define una clase en python?

* **ok** class Clase:
* class Clase(self):
* new class Clase:
* class define Clase(self):

##  34. <a name='-1'></a>34

Salida que produce la siguiente asignación sobre una lista:

```python
aList = [4, 8, 12, 16]
aList[1:4] = [20, 24, 28]
print(aList)
``` 

* [4, 20, 24, 28, 8, 12, 16]
* [4, 20, 24, 28] **ok**

##  35. <a name='-1'></a>35

Salida que produce la siguiente operación sobre una lista:

```python
sampleList = [10, 20, 30, 40, 50]
print(sampleList[-4:-1])
``` 

* 40
* [20, 30, 40] **ok**
* Error índice fuera de rango

##  36. <a name='-1'></a>36 

Salida que produce la siguiente operación sobre una lista:

```python
aList = [5, 10, 15, 25]
print(aList[::-2])
``` 

* [15, 10, 5]
* [10, 5]
* [25, 10] **ok**

##  37. <a name='-1'></a>37

Salida producida por la llamada a la función:

```python
def display(**kwargs):
    for i in kwargs:
        print(i,end=' ')

display(emp="Kelly", salary=9000)
``` 

* TypeError
* Kelly 9000
* (’emp’, ‘Kelly’) (‘salary’, 9000)
* emp salary **ok**













