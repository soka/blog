---
title: Actualización de seguridad en Google Drive
subtitle: Los enlaces de Google Drive cambiarán a partir del 13 de Septiembre
date: 2021-09-08
draft: false
description: Los enlaces de Google Drive cambiarán a partir del 13 de Septiembre
tags: ["Google","Calendar","Updates","Actualizaciones","Google Workspace","Novedades","Workspace"]
---

<!-- vscode-markdown-toc -->
* 1. [¿Qué significa el mensaje sobre la actualización de seguridad de Google Drive?](#QusignificaelmensajesobrelaactualizacindeseguridaddeGoogleDrive)
* 2. [¿Que archivos se verán afectados?](#Quearchivossevernafectados)
* 3. [¿Cómo influirá esta actualización de seguridad en el acceso a mis archivos afectados?](#Cmoinfluirestaactualizacindeseguridadenelaccesoamisarchivosafectados)
* 4. [¿Me enteraré si alguien no puede acceder a un archivo de mi propiedad porque no tiene un vínculo con la clave de recurso?](#Meenterarsialguiennopuedeaccederaunarchivodemipropiedadporquenotieneunvnculoconlaclavederecurso)
* 5. [¿Debería seguir compartiendo los archivos afectados de la forma habitual?](#Deberaseguircompartiendolosarchivosafectadosdelaformahabitual)
* 6. [Enlaces internos](#Enlacesinternos)
* 7. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

![](img/04.jpg)

##  1. <a name='QusignificaelmensajesobrelaactualizacindeseguridaddeGoogleDrive'></a>¿Qué significa el mensaje sobre la actualización de seguridad de Google Drive?

El **próximo lunes 13 de Septiembre es la fecha señalada**, ese día se producirán cambios encaminados a **hacer más seguros los enlaces** que permiten compartir contenidos en **Google Drive**.

![](img/01.png)

Google Drive tiene dos formas de compartir contenidos: 

1. Una lista de personas / usuarios permitidos usando sus cuentas.
2. Una opción de **compartir mediante un enlace**: esto supone un problema, no es enlace realmente privado ya que este enlace se puede "adivinar" y dar acceso a personas que no queremos (de hay la razón de que los enlaces generados para compartir algo sean tan largos y complicados.

La segunda opción es la que se verá afectada por la nueva por la actualización de Google. Hasta ahora un enlace podía tener este aspecto `https://drive.google.com/file/d/0BxI1YpjkbX0OZ0prTHYyQ1U2djQ/`, con la nueva actualización pasarán a ser algo como `https://drive.google.com/file/d/0BxI1YpjkbX0OZ0prTHYyQ1U2djQ/view?resourcekey=0-OsOHHiQFk1QEw6vIyh8v_w` (un nuevo parámetro `resourcekey`).

##  2. <a name='Quearchivossevernafectados'></a>¿Que archivos se verán afectados?

1. En la computadora, ve a [drive.google.com](https://www.drive.google.com/).
2. En la parte superior, escribe `is:security_update_applied` o `is:security_update_removed` en la barra de búsqueda. 

![](img/05.PNG)

En el momento de escribir este artículo ninguna de las búsquedas en mi cuenta de arroja ningún resultado.

##  3. <a name='Cmoinfluirestaactualizacindeseguridadenelaccesoamisarchivosafectados'></a>¿Cómo influirá esta actualización de seguridad en el acceso a mis archivos afectados? 

Una vez que se aplique la actualización de seguridad a un archivo, se necesitará una URL con una clave de recurso para acceder a él. **Los usuarios que hayan visto el archivo o hayan accedido a él anteriormente no necesitarán la URL con la clave de recurso**. **Solo la necesitarán aquellos usuarios que no hayan accedido previamente al archivo**. Si no tienen la URL con la clave del recurso, tendrán que solicitar acceso al archivo.

##  4. <a name='Meenterarsialguiennopuedeaccederaunarchivodemipropiedadporquenotieneunvnculoconlaclavederecurso'></a>¿Me enteraré si alguien no puede acceder a un archivo de mi propiedad porque no tiene un vínculo con la clave de recurso?

Si alguien solicita acceso a uno de tus archivos con la actualización de seguridad, **recibirás un correo electrónico en el que se te informará si esa persona intentó acceder al archivo con un vínculo antiguo**. Luego, puedes enviarle un vínculo nuevo con la clave de recurso para que pueda acceder.

##  5. <a name='Deberaseguircompartiendolosarchivosafectadosdelaformahabitual'></a>¿Debería seguir compartiendo los archivos afectados de la forma habitual?

**Sí**, pero debes enviar a los colaboradores el nuevo vínculo actualizado que incluye la clave de recurso de tus archivos para que puedan obtener acceso una vez que se aplique la actualización de seguridad. No quites el parámetro "resourcekey" cuando compartas el vínculo con otras personas.

##  6. <a name='Enlacesinternos'></a>Enlaces internos

* [Comparte tu ubicación de trabajo desde Google Calendar](https://soka.gitlab.io/blog/post/2021-09-06-comparte-ubicac%C3%ADon-trabajo-desde-google-calendar/).
* [Google Calendar permite especificar si asistirás virtualmente o de forma presencial](https://soka.gitlab.io/blog/post/2021-07-19-google-calendar-opcion-asistencia-presencial-o-virtual/).
* [Algunas novedades interesantes en Google Workspace](https://soka.gitlab.io/blog/post/2021-07-02-google-workspace-novedades/).
* [Integración de Google Meet en Slack](https://soka.gitlab.io/blog/post/2021-06-23-slack-integracion-google-meet/).
* [Google Calendar para Slack](https://soka.gitlab.io/blog/post/2021-06-21-slack-integracion-google/).
* [Meet for Slack](https://soka.gitlab.io/blog/post/2021-04-26-meet-for-slack/).
* [Seguridad con cuentas de Google](https://soka.gitlab.io/blog/post/2021-02-21-seguridad-cuentas-google/).

##  7. <a name='Enlacesexternos'></a>Enlaces externos

* Actualización de seguridad para compartir archivos de Google Drive [https://support.google.com/drive/answer/10729743?hl=es-419](https://support.google.com/drive/answer/10729743?hl=es-419).
* Security update for Google Drive [https://drive.google.com/drive/update-drives](https://drive.google.com/drive/update-drives).
* Share files from Google Drive [https://support.google.com/drive/answer/2494822](https://support.google.com/drive/answer/2494822).
* [https://www.genbeta.com/almacenamiento/que-google-drive-rompera-enlaces-compartidos-a-partir-septiembre-que-significa-actualizacion-seguridad-que-hacer](https://www.genbeta.com/almacenamiento/que-google-drive-rompera-enlaces-compartidos-a-partir-septiembre-que-significa-actualizacion-seguridad-que-hacer).
* [https://www.varonis.com/blog/the-dangers-of-shared-links/](https://www.varonis.com/blog/the-dangers-of-shared-links/).
* [https://arstechnica.com/gadgets/2021/07/heres-what-that-google-drive-security-update-message-means/](https://arstechnica.com/gadgets/2021/07/heres-what-that-google-drive-security-update-message-means/).
* Decidir cómo aplicar la actualización de seguridad para compartir mediante enlace en Drive [https://support.google.com/a/answer/10685032?p=update_drives&visit_id=1631112984976-3086199167701059916&rd=1](https://support.google.com/a/answer/10685032?p=update_drives&visit_id=1631112984976-3086199167701059916&rd=1).