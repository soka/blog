---
title: Conoce a tu enemigo
subtitle: Tipos de ataques a contraseñas
date: 2021-02-23
draft: false
description: Consejos para proteger mejor nuestra cuenta
tags: ["Seguridad","Contraseñas","Ciberseguridad"]
---

<!-- vscode-markdown-toc -->
* 1. [ATAQUES A CONTRASEÑAS](#ATAQUESACONTRASEAS)
	* 1.1. [Fuerza bruta](#Fuerzabruta)
	* 1.2. [Ataques por diccionario](#Ataquespordiccionario)
	* 1.3. [Credential Stuffing](#CredentialStuffing)
	* 1.4. [Rainbow tables](#Rainbowtables)
	* 1.5. [¿Cómo me protejo?](#Cmomeprotejo)
	* 1.6. [Aplicaciones para descubrir contraseñas](#Aplicacionesparadescubrircontraseas)
		* 1.6.1. [John the Ripper password cracker](#JohntheRipperpasswordcracker)
		* 1.6.2. [THC Hydra](#THCHydra)
		* 1.6.3. [Aircrack-Ng](#Aircrack-Ng)
		* 1.6.4. [Ncrack](#Ncrack)
		* 1.6.5. [SAMInside](#SAMInside)
		* 1.6.6. [Hashcat](#Hashcat)
* 2. [INGENIERÍA SOCIAL](#INGENIERASOCIAL)
	* 2.1. [Phishing](#Phishing)
* 3. [OTROS](#OTROS)
	* 3.1. [Google Hacking](#GoogleHacking)
* 4. [ENLACES INTERNOS](#ENLACESINTERNOS)
* 5. [ENLACES EXTERNOS](#ENLACESEXTERNOS)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Bruce Schneier en su artículo [Choosing Secure Passwords](https://www.schneier.com/blog/archives/2014/03/choosing_secure_1.html) lo explica perfectamente.

> "The best way to explain how to choose a good password is to explain how they’re broken" - Bruce Schneier.

![](img/03.jpg)

##  1. <a name='ATAQUESACONTRASEAS'></a>ATAQUES A CONTRASEÑAS


###  1.1. <a name='Fuerzabruta'></a>Fuerza bruta

Al contrario que otras técnicas, los ataques de fuerza bruta y diccionario **no se basan en vulnerabilidades que puedan contener un software**. En su lugar **se confían en que los usuarios adopten contraseñas débiles o fáciles de adivinar**.

El [**ataque de fuerza bruta**](https://es.wikipedia.org/wiki/Ataque_de_fuerza_bruta) es el más simple y seguramente el menos sofisticado de todos.

**¿Cómo funciona?** Consiste en adivinar nuestra contraseña a base de ensayo y error probando todas las combinaciones posibles hasta encontrar la que permita el acceso. El objetivo de los atacantes siempre será conseguir la información almacenada en nuestras cuentas.

Para el atacante el factor tiempo es determinante, dos variables que pueden determinar que lleve más tiempo o menos son la longitud ((también conocido como el espacio de claves)) y el juego de carácteres, esto último se conoce como la complejidad, a mayor cantidad mayor número de distintas combinaciones debe probar el atacante. Por ejemplo si solo usamos una longitud de cuatro carácteres y sólo los dígitos del 0 al 9 las contraseñas posibles a probar son 10^4 =  10.000. Si además puede ser de longitud menor el cálculo se complica, eso para otro día.

Para este tipo de ataques normalmente se usan una o varias potentes computadoras coordinadas que permitan realizar un gran número de operaciones, una aplicación como [Hashcat](https://hashcat.net/hashcat/) puede probar [8 millones de contraseñas por segundo](http://www.hackersnewsbulletin.com/2013/09/new-password-cracking-software-tries-8-million-times-per-second-crack-password.html).

**Síntomas**. ¿Qué aspecto puede tener un ataque de fuerza bruta en la red? La misma dirección IP tratando se autenticarse en múltiples ocasiones, diferentes direcciones IP tratando de acceder a una misma cuenta en un periódo corto de tiempo por ejemplo.

###  1.2. <a name='Ataquespordiccionario'></a>Ataques por diccionario

Los atacantes utilizan un software que, de forma automática, trata de averiguar nuestra contraseña.

Una contraseña a menudo está formada por una raíz y un "apéndice", la raíz no es necesariamente una palabra de diccionario, pero suele ser algo pronunciable, el "apéndice" suele ser un sufijo en la mayoría de los casos (90%) o un prefijo (10%), un programa de _cracking_ puede empezar probando un lista de X palabras como "123456", "admin" y luego combinar cada una de ellas con sufijos comunes (fechas, símbolos comunes).

Los atacantes usan diferentes listados de palabras, diccionarios en diferentes idiomas, nombres, fechas, etc. Aplican sobre los mismos variaciones como mayúsculas-minúsculas, sustituciones comunes como "$" en lugar del carácter "s", "@" en lugar de "a", etc.

Un buen atacante además probará con cualquier información personal sobre el objetivo (direcciones, fechas significativas, códigos postales, etc.).

###  1.3. <a name='CredentialStuffing'></a>Credential Stuffing

Con un número creciente de brechas de seguridad, **reutilizar contraseñas es una forma de comprometer tus cuentas**.

![](img/01.png)

Es recomendable mirar periódicamente páginas como [**Have I been pwned**](https://haveibeenpwned.com/). Se trata de una Web que recopila todas las filtraciones de contraseñas. En ella, sólo escribes tu correo electrónico y la web te dice si se ha filtrado alguna contraseña en servicios en los que lo has utilizado. De esta manera, si ves que ha habido una filtración puedes prevenir y empezar a cambiar contraseñas.

![](img/02.png)

Firefox siempre saca buenas aplicaciones en torno a la privacidad y seguridad, en colaboración con el creador de ([HIBP](https://en.wikipedia.org/wiki/Have_I_Been_Pwned%3F)) han publicado una Web similar ([**Firefox Monitor**](https://monitor.firefox.com/))  donde puedes introducir tu correo electrónico para averiguar si se ha visto comprometido en una brecha de seguridad, además ofrece la opción de suscripción por correo electrónico para recibir las últimas alertas, previamente debemos registrarnos, en la misma cuenta se puede registrar más de un correo electrónico. La Web también contiene una lista de los servicios que han sufrido un incidente de seguridad, a priori podríamos pensar que son Webs insignificantes pero en el listado figuran sitios como Canva o Bitly, solamente ver la lista impresiona. El tercer apartado son algunos [consejos de seguridad](https://monitor.firefox.com/security-tips) para protegerte a ti mismo (la mayoría se comentan en este post).

###  1.4. <a name='Rainbowtables'></a>Rainbow tables



###  1.5. <a name='Cmomeprotejo'></a>¿Cómo me protejo?

La única forma de **protegernos contra ataques de fuerza bruta es usar contraseñas complejas y con una longitud mínima** formada por una combinación de letras mayúsculas y minúsculas, carácteres especiales y números.

* Usar contraseñas robustas.
* Aplicar 2FA siempre que el servicio lo permita.

Otra garantía de **seguridad es la que pueda ofrecer el servicio donde estemos registrados**, existen infinidad de herramientas y medidas, algunos ejemplos:

* Aumentando el tiempo que debemos esperar entre sucesivos intentos sin éxito de introducir una contraseña
* Usando listas negras para bloquear una dirección IP cuando realiza demasiados intentos. En este caso los atacantes usarán servidores proxy para evitar el bloqueo.
* Bloqueando la cuenta, esto es un problema porque también la estás bloqueando para su usuario legítimo.
* Usar CAPTCHA para evitar ataques automatizados.
* Sólo permitir al acceso desde una/s IP origen (listas blancas).
* Una solución tremendamente sencilla, en vez de retornar “HTTP 401 error” retonar un “HTTP 200 SUCCESS” con la explicación del fallo de autenticación.
* Restringir el acceso a la URL de autenticación, por ejemplo en WordPress moviendo "/wp-login.php" a "/mysite-login" o añadiendo un capa adicional de autenticación en el servidor HTTP (en Apache el típico [.htaccess](https://es.wikipedia.org/wiki/.htaccess) por ejemplo).
* WAF (web application firewall).

###  1.6. <a name='Aplicacionesparadescubrircontraseas'></a>Aplicaciones para descubrir contraseñas

####  1.6.1. <a name='JohntheRipperpasswordcracker'></a>John the Ripper password cracker

[**John the Ripper**](https://www.openwall.com/john/) es un programa multiplataforma de código abierto para auditar la seguridad de contraseñas y recuperarlas, soporta multitud de tipos de cifrado y sumas de comprobación (Linux, Win, macOS, WordPress, servidores de bases de datos, etc). Además en el propio sitio Web podemos encontrar [listas de palabras](https://www.openwall.com/wordlists/) y contraseñas habituales en más de 20 idiomas (de pago).

####  1.6.2. <a name='THCHydra'></a>THC Hydra

Permite realizar ataques usando diferentes protocolos (Telnet, FTP, HTTP, HTTPS, SMB, etc).

[https://sectools.org/tool/hydra/](https://sectools.org/tool/hydra/)

####  1.6.3. <a name='Aircrack-Ng'></a>Aircrack-Ng

####  1.6.4. <a name='Ncrack'></a>Ncrack

####  1.6.5. <a name='SAMInside'></a>SAMInside

####  1.6.6. <a name='Hashcat'></a>Hashcat



##  2. <a name='INGENIERASOCIAL'></a>INGENIERÍA SOCIAL

Estos son ataques basados en el engaño y manipulación donde el objetivo somos nosotros.

###  2.1. <a name='Phishing'></a>Phishing

Consiste en envíar un mensaje (SMS, correo electrónico o incluso una llamada) suplantando a una entidad legítima, como puede ser un banco, una red social o un proveedor.

En ocasiones incluyen un enlace a una Web fraudulenta que es un clon del sitio original,

##  3. <a name='OTROS'></a>OTROS

###  3.1. <a name='GoogleHacking'></a>Google Hacking

**En Google** se puede encontrar de todo sabiendo hacer la pregunta correcta, **usando los términos de búsqueda concretos podemos encontrar información sensible que no se ha protegido correctamente**, por ejemplo introduciendo este texto en el buscador `allintext:password filetype:txt after:2018` Google buscará ficheros de texto con la palabra "password", a esto se la llama comunmente [Google Hacking](https://es.wikipedia.org/wiki/Google_Hacking).

##  4. <a name='ENLACESINTERNOS'></a>ENLACES INTERNOS

* Taller contraseñas seguras [https://soka.gitlab.io/blog/post/2021-02-25-taller-contraseinas-seguras/](https://soka.gitlab.io/blog/post/2021-02-25-taller-contraseinas-seguras/).
* La clave eres tú. Recursos sobre gestión de contraseñas seguras [https://soka.gitlab.io/blog/post/2021-01-15-la-clave-eres-tu/](https://soka.gitlab.io/blog/post/2021-01-15-la-clave-eres-tu/).
* Manual LastPass [https://soka.gitlab.io/blog/post/2021-01-24-manual-lastpass/](https://soka.gitlab.io/blog/post/2021-01-24-manual-lastpass/).
* Alternativas a LastPass [https://soka.gitlab.io/blog/post/2021-02-20-alternativas-lastpass/](https://soka.gitlab.io/blog/post/2021-02-20-alternativas-lastpass/).
* [Recursos y fuentes de información](https://soka.gitlab.io/blog/post/2021-03-04-recursos-taller-seguridad-contrase%C3%B1as/).

##  5. <a name='ENLACESEXTERNOS'></a>ENLACES EXTERNOS

* [https://www.osi.es/es/campanas/contrasenas-seguras/ataques-contrasenas](https://www.osi.es/es/campanas/contrasenas-seguras/ataques-contrasenas).
* [https://arstechnica.com/information-technology/2013/05/how-crackers-make-minced-meat-out-of-your-passwords/](https://arstechnica.com/information-technology/2013/05/how-crackers-make-minced-meat-out-of-your-passwords/).
* Bruce Schneier [Choosing Secure Passwords](https://www.schneier.com/blog/archives/2014/03/choosing_secure_1.html).
* [https://www.osi.es/es/guia-ciberataques](https://www.osi.es/es/guia-ciberataques).
* [http://hackersnewsbulletin.com/2013/09/new-password-cracking-software-tries-8-million-times-per-second-crack-password.html](http://hackersnewsbulletin.com/2013/09/new-password-cracking-software-tries-8-million-times-per-second-crack-password.html).
* [https://www.incibe.es/protege-tu-empresa/guias/ciberamenazas-entornos-empresariales-guia-aproximacion-el-empresario](https://www.incibe.es/protege-tu-empresa/guias/ciberamenazas-entornos-empresariales-guia-aproximacion-el-empresario).
* [https://owasp.org/www-community/controls/Blocking_Brute_Force_Attacks](https://owasp.org/www-community/controls/Blocking_Brute_Force_Attacks).

**Herramientas**

* John the Ripper password cracker [https://www.openwall.com/john/](https://www.openwall.com/john/).
* hashcat [https://hashcat.net/hashcat/](https://hashcat.net/hashcat/).
