---
title: Python venv
subtitle: crear entornos virtuales
date: 2024-02-21
draft: false
description: crear entornos virtuales
tags: ["Python","venv"]
---

<!-- vscode-markdown-toc -->
* 1. [venv](#venv)
* 2. [Instalazioa](#Instalazioa)
* 3. [Ingurune berria sortu](#Inguruneberriasortu)
* 4. [“Kaixo, Mundua” programa bat sortzea](#KaixoMunduaprogramabatsortzea)
* 5. [Ingurumena uztea](#Ingurumenauztea)
* 6. [Kanpo loturak](#Kanpoloturak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='venv'></a>venv

Venv moduluak “ingurune birtual” arinak sortzeko euskarria eskaintzen du, bere kokapen-direktorioekin, sistematik isolatuta. Ingurune birtual bakoitzak bere Python bertsioa du (ingurune hau sortzeko erabili zen binarioaren bertsioarekin bat datorrena) eta bere Python pakete multzo independentea izan dezake bere kokapen direktorioetan instalatuta.

##  2. <a name='Instalazioa'></a>Instalazioa

Debian eta honetan oinarritutako banaketak:

```python
sudo apt install -y python3-venv
```

##  3. <a name='Inguruneberriasortu'></a>Ingurune berria sortu

```bash
mkdir environments
cd environments
``` 

```bash
python3 -m venv my_env
```


```bash
ls my_env
``` 

```bash
source my_env/bin/activate
``` 

```bash
(my_env) popu@xinaurri:~/Documentos/python/environments$
``` 

##  4. <a name='KaixoMunduaprogramabatsortzea'></a>“Kaixo, Mundua” programa bat sortzea

```bash
(my_env) popu@xinaurri:~/Documentos/python/environments$ nano hello.py
``` 

```python
print("Hello, World!")
``` 

##  5. <a name='Ingurumenauztea'></a>Ingurumena uztea


```bash
deactivate
``` 

##  6. <a name='Kanpoloturak'></a>Kanpo loturak

* [https://docs.python.org/es/3.8/library/venv.html](https://docs.python.org/es/3.8/library/venv.html)