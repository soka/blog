---
title: Utopías digitales
subtitle: Licencias  
date: 2024-02-17
draft: true
description: Licencias
tags: ["Utopías digitales","Ekaitz Cancela","Libros","Ensayo"]
---

<!-- vscode-markdown-toc -->
* 1. [Desautomatizar el calentamiento global](#Desautomatizarelcalentamientoglobal)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Desautomatizarelcalentamientoglobal'></a>Desautomatizar el calentamiento global

* [https://www.greenpeace.org/usa/reports/oil-in-the-cloud/](https://www.greenpeace.org/usa/reports/oil-in-the-cloud/)
* [https://www.expansion.com/economia-digital/companias/2018/08/05/5b60222fca4741135e8b4614.html](https://www.expansion.com/economia-digital/companias/2018/08/05/5b60222fca4741135e8b4614.html)
