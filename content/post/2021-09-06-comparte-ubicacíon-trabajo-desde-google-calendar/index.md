---
title: Comparte tu ubicación de trabajo desde Google Calendar
subtitle: Entradas del blog que nunca pensé que haría
date: 2021-09-06
draft: false
description: 
tags: ["Google","Calendar","Updates","Actualizaciones","Google Workspace","Novedades","Workspace"]
---

<!-- vscode-markdown-toc -->
* 1. [La nueva funcionalidad de Calendar permite especificar una ubicación de trabajo para nuestro horario laboral](#LanuevafuncionalidaddeCalendarpermiteespecificarunaubicacindetrabajoparanuestrohorariolaboral)
* 2. [Activación de la nueva funcionalidad](#Activacindelanuevafuncionalidad)
* 3. [Enlaces internos](#Enlacesinternos)
* 4. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='LanuevafuncionalidaddeCalendarpermiteespecificarunaubicacindetrabajoparanuestrohorariolaboral'></a>La nueva funcionalidad de Calendar permite especificar una ubicación de trabajo para nuestro horario laboral

Google sigue apostando e introduciendo nuevas funcionalidades para modelos de trabajo "híbridos", ahora podemos **especificar una ubicación de trabajo para nuestro horario semanal habitual** y que otras personas lo vean para poder planificar las reuniones.

##  2. <a name='Activacindelanuevafuncionalidad'></a>Activación de la nueva funcionalidad

Está funcionalidad se encuentra desactivada por defecto, Calendar te mostrará un cuadro de dialogo para ponerlo en marcha como en la imagen inferior o puedes hacerlo desde los ajustes del calendario en cualquier momento.

![](img/01.png)

**Activar nueva funcionalidad en los ajustes**

Dentro de ajustes de Calendar marcar la opción "Habilitar ubicación del trabajo", ahora ya podemos definir nuestra ubicación para nuestro horario laboral semanal.

![](img/04.png)

Desde la vista de calendario normal ahora podemos ver una fila nueva con la ubicación y podemos cambiarla en cualquier momento.

![](img/05.png)

## Cómo ven el resto de personas nuestra ubicación

Igual hay alguna otra forma pero una muy cómoda es usar el cuadro de búsqueda "Reunirse con..."

![](img/06.png)

##  3. <a name='Enlacesinternos'></a>Enlaces internos

* [Google Calendar permite especificar si asistirás virtualmente o de forma presencial](https://soka.gitlab.io/blog/post/2021-07-19-google-calendar-opcion-asistencia-presencial-o-virtual/).

##  4. <a name='Enlacesexternos'></a>Enlaces externos

*  Google Workspace Updates [Share where you’re working from in Google Calendar](https://workspaceupdates.googleblog.com/2021/08/share-where-youre-working-from-in.html) - August 18, 2021.