---
title: Curso - Organiza y gestiona tu correo electrónico de forma productiva
subtitle: Inbox Zero, GTD, 5SDigitales
date: 2021-02-04
draft: false
description: Impartido por Mikroenpresa Digitala
tags: ["Inbox Zero","Correo","Gmail","5S","GTD","Productividad","Cursos","Mikroenpresa Digitala"]
---

[https://www.spri.eus/euskadinnova/es/enpresa-digitala/agenda/organiza-gestiona-correo-forma-productiva-digitales-inbox-zero-gmail/15550.aspx](https://www.spri.eus/euskadinnova/es/enpresa-digitala/agenda/organiza-gestiona-correo-forma-productiva-digitales-inbox-zero-gmail/15550.aspx)

Compartiremos buenas prácticas de metodologías que abordan la gestión del correo electrónico para que su clasificación y organización no supongan un lastre en la productividad de tu negocio y vida personal: • 5S Digitales • Inbox Zero • GTD PRÁCTICA con GMAIL: El propósito del curso será ofrecer una visión general de cada metodología para que el asistente adopte las mejores prácticas en su día a día y pueda llevarlo a la práctica en su cuenta de Gmail, -si bien las metodologías son extensibles a cualquier cliente de correo con el que se trabaje-. Se recomienda que los asistentes tengan cuenta de Gmail para poder practicar en el curso.
Objetivos

Este curso ayudará a los asistentes a mantener su correo electrónico organizado, ordenado, limpio, visualmente controlado y sostenible:

- Cómo contemplar la gestión del correo electrónico según la metodología Inbox Zero.
- Comprender qué son las 5S y cómo aplicarlo a la organización de nuestro correo electrónico.
- Cómo gestionar las tareas que se reciben a partir de nuestro correo electrónico con la metodología GTD.
- Abordar las buenas prácticas desde nuestra cuenta de GMAIL.

Programa

- Reflexiones previas: ¿Cuál es mi problema? ¿Qué relación mantengo con mi email? Consejos prácticos para controlar la email-dependencia.
- Aproximación a la metodología Inbox Zero.
- Aplicación de las 5S en la gestión del correo electrónico.
- Metodologías GTD: Cómo gestionar las tareas derivadas de los emails.
- Práctica con Gmail.
- Repaso a la configuración general de la cuenta.
- Un poco de orden ¿Cómo conseguir una bandeja de entrada limpia?: etiquetas, categorías, filtros, paneles, bandejas de entrada, estrellas...
- Gestión de búsquedas.
- Integración de correo electrónico.
- Seguridad.
- Aplicaciones y Extensiones para Gmail: Unroll.me, Mailstorm, Boomerang, Mailtrack, Duffel Gmail. Dmail, Keyrocket, Emailmeter...y más.

- [Manual Gmail-5s.pdf](docs/Manual Gmail-5s.pdf)
- [Presentación Gmail5s.pdf](docs/Presentación Gmail5s.pdf)

![](img/01.png)

![](img/02.png)

![](img/03.png)

![](img/04.png)

![](img/05.png)

![](img/06.png)

![](img/07.png)

![](img/08.png)

![](img/09.png)

![](img/10.png)

![](img/11.png)

![](img/12.png)

![](img/13.png)

![](img/14.png)

![](img/15.png)

![](img/16.png)

![](img/17.png)

![](img/18.png)

![](img/19.png)

![](img/20.png)

![](img/21.png)

![](img/22.png)

![](img/23.png)

![](img/24.png)

![](img/25.png)

![](img/26.png)

![](img/27.png)

![](img/28.png)

![](img/29.png)

![](img/30.png)

![](img/31.png)

![](img/32.png)

![](img/33.png)

![](img/34.png)

![](img/35.png)

![](img/36.png)

![](img/37.png)

![](img/38.png)

![](img/39.png)

![](img/40.png)

![](img/41.png)

![](img/42.png)

![](img/43.png)

![](img/44.png)

![](img/45.png)

![](img/46.png)

![](img/47.png)

![](img/48.png)

![](img/49.png)

![](img/50.png)

![](img/51.png)

![](img/52.png)

![](img/53.png)

![](img/54.png)

![](img/55.png)

![](img/56.png)

![](img/57.png)

![](img/58.png)

![](img/59.png)

![](img/60.png)

![](img/61.png)

![](img/62.png)

![](img/63.png)

![](img/64.png)

![](img/65.png)

![](img/66.png)

![](img/67.png)
