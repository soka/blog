---
title: Objetos en R
subtitle: Propiedades y conversión de tipo 
date: 2019-03-14
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico","R - Datos","R - Objetos"]
---

Como he mencionado en otros posts, cualquier cosa en **R** es un objeto referenciado por un nombre. Cuando arranca **R** se crea un espacio de trabajo (environment) en el que se pueden crear, salvar o borrar las variables que usamos (en R Studio se muestran el entorno en una ventana). 

# Funciones relacionadas con los objetos y la memoria

La función `ls()` lista los objetos y datasets en el entorno de trabajo, la llamada `objects()` es equivalente.

```R
> var1 <- "test"
> ls() #lista los objetos en el entorno de trabajo
[1] "var1"
```

La función `rm()` (o `remove()`) borra los objetos del entorno de trabajo:

```R
rm(var1) 
```

# Propiedades

Cualquier cosa en **R** es un objeto ¡Incluso las funciones y los operadores!. La función `attributes()` informa de los atributos de un objeto, si no tiene el resultado es `NULL`. La función `attr()` permite acceder a un determinado atributo. Se pueden declarar atributos para cualquier objeto y se almacenan en una lista según sus nombres.

```R
> x <- 1:4 # vector numérico
> attributes(x) <- list(nombre='resultados',desc='vector tipo')
> attributes(x)
$nombre
[1] "resultados"

$desc
[1] "vector tipo"
```

Para cambiar el contenido de un atributo:

```R
attr(x,'resultados') <- 'res'
```

# Clase

La función `class()` informa de que clase es el objeto. 

```R 
> class(x)
[1] "integer"
```

# Dimensiones y longitud

```R 
> dim(x)
NULL
> length(x)
[1] 4
```

# Nombres

La función `names()` presenta el nombre de cada uno de los elementos del objeto.

```R 
# Imprime atributo nombre de dataset islands
names(islands)
```

# Tipos

La función `typeof()` devuelve una cadena con el tipo de objeto.

```R
> typeof(x)
[1] "integer"
```

# Estructura 

# Conversión de objetos

Vamos a ver como convertir de un objeto a otro. 

Por ejemplo declaramos una matriz que posteriormente convertiremos en un Data Frame con la función `as.data.frame()`. Es muy sencillo viendo el ejemplo de abajo:

```R 
mx1 <- matrix(1:9,nrow = 3,ncol = 3)
df1 <- as.data.frame(mx1)
```

Si introducimos `as.` veremos que existen multitud de opciones de conversión a diferentes tipos (`as.list`,`as.array`...). 

Una operación muy común es convertir una cadena de texto que representa un número en un valor numérico:

```R
> ch1 <- "01"
> num1 <- as.numeric(ch1)
> num1
[1] 1
```

# Ejercicios

* r / intro / 23-Conversion-objetos / [conversion-objetos.R](https://gitlab.com/soka/r/blob/master/intro/23-Conversion-objetos/conversion-objetos.R).

# Enlaces



