---
title: Administración de servicios con Powershell
subtitle: Administración de Windows Server con PowerShell
date: 2020-01-05
draft: false
description: Administración servidores Windows
tags: ["Windows","Sistemas","PowerShell","Administración","cmdlets","Scripting","Windows Server","Services","Servicios"]
---

![](img/win-server-logo.png)

El primer comando que uso es [`Get-Service`](https://docs.microsoft.com/en-us/powershell/module/Microsoft.PowerShell.Management/Get-Service?view=powershell-5.1) para obtener un listado de los servicios de la máquina.

![](img/01.png)

También puedo buscar un servicio concreto con el parámetro `-Name` ([WinRM](https://en.wikipedia.org/wiki/Windows_Remote_Management) es Windows Remote Management):

![](img/02.png)

Puedo buscar los servicios que comienzan con un texto `Get-Service "win*"`.

Ahora aplico un filtro con un pipe para obtener los servicios que están corriendo:

```ps
Get-Service | Where-Object {$_.Status -eq "Running"}
```

Podemos consultar los servicios en diferentes máquinas con `Get-Service -ComputerName 'server1','server2','server3'`.

El cmdlet `Get-Service` pertenece al módulo [Microsoft.PowerShell.Management](https://docs.microsoft.com/en-us/powershell/module/Microsoft.PowerShell.Management/?view=powershell-5.1#microsoft.powershell.management) que contiene comandos con diferentes propósitos, se pueden obtener todos con `Get-Command -Module Microsoft.PowerShell.Management`.

Los cmdlets mas usados son para detener o arrancar un servicio:

```ps
Stop-Service -Name W32Time
Get-Service | Where-Object {$_.Name -eq "W32Time"}
Get-Service -Name W32Time
Start-Service -Name W32Time
```

Para cambiar un atributo de un servicio por ejemplo el `DisplayName` (cuando son servicios del sistema mejor no tocar mucho):

```ps
Set-Service -Name W32Time -DiplayName "Hora Win 2"
```

Para crear un nuevo servicio ([Svchost.exe](https://es.wikipedia.org/wiki/Svchost.exe)):

```ps
New-Service -Name "TestService" -BinaryPathName "C:\Windows\System32\svchost.exe -k netsvcs" -DependsOn NetLogon -DisplayName "Servicio de prueba" -StartupType Manual -Description "Este servicio es una prueba"
sc.exe delete TestService
```





