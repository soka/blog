---
title: Introducción a Trello, crear una cuenta
subtitle: La pizarra digital inspirada en kanban
date: 2019-07-02
draft: false
description: Crear una cuenta
tags:
  [
    "Trello",
    "Talleres",
    "Cursos",
    "Aplicaciones",
    "Trello - Introducción",
    "Kanban",
    "Metodologías",
    "Gestión de Proyectos",
    "Herramientas",
  ]
---

![](img/logo-trello.png)

## Introducción

Este es un curso totalmente práctico por lo que en esta breve introducción daré unas pinceladas de que es **Trello**.

**Trello** es un software o aplicativo desarrollado por la compañía Fog Creek (adquirido en 2017 por Atlassian), funciona sobre cualquier navegador de Internet y no es necesario instalarlo (podemos instalarlo de forma nativa en un smartphone pero no es imprescindible).

**Trello** está basado en el sistema de tarjetas visuales kanban.

Los usos de **Trello** son infinitos, básicamente permite crear tarjetas con tareas o elementos que concentran algún tipo de información y agruparlas en listas.

**¿Qué vamos a aprender en esta clase?**

- Crear una cuenta en Trello.
- Editar el perfil y añadir un avatar.
- Borrar el tablero de ejemplo.

## Crear una cuenta en Trello

El primer paso lógico es crear una cuenta de usuario en **Trello**, introducimos en la barra del navegador la dirección [https://trello.com/](https://trello.com/) o bien buscamos el termino en un buscador.

![](img/01.PNG)

Ya estoy en la página principal, no es necesario descargar ni instalar nada, es enteramente Web.

Si aún no tenemos cuenta usamos la opción "Registrarse" que nos solicita un nombre, un correo electrónico y una contraseña.

![](img/02.PNG)

Para los usuarios de Google existe una opción para vincular la cuenta de Google a **Trello**.

Pantalla de bienvenida:

![](img/03.PNG)

Si seleccionas un uso concreto **Trello** crea un tablero inicial de ejemplo:

![](img/04.PNG)

Si vamos a trabajar en grupo es aconsejable primero dar de alta la cuenta de la persona que lo va a liderar.

![](img/05.PNG)

Lo primero que nos pide es confirmar nuestro correo electrónico.

![](img/06.PNG)

Si hacemos clic sobre el botón de verificar dirección nos abre una pestaña que nos dirige al "cuadro de mando" general:

![](img/07.PNG)

## Actualizar perfil

Una vez que estamos dentro lo primero es actualizar los datos personales de nuestra cuenta, especialmente una fotografía que ayudará al resto del equipo a que nos identifique de forma visual en las tarjetas.

Pinchamos sobre el icono de nuestro perfil en la esquina superior derecha y en el menú que se despliega le damos a "Perfil":

![](img/08.PNG)

Seleccionamos "Editar perfil":

![](img/09.PNG)

Y pinchamos sobre la imagen para "Cambiar" la imagen.

![](img/10.PNG)

El usuario es conveniente dejarlo como está, es el que nos asigna **Trello**, incluso podemos completar la biografía (especialmente si es un grupo público) que es opcional.

Para volver al panel principal con el resumen de nuestros tableros hacemos clic en el logo de **Trello**.

![](img/11.PNG)

Pinchamos sobre el tablero de ejemplo y accedemos a el.

![](img/12.PNG)

Ahora borramos el tablero de ejemplo, los tableros siempre tienen un menú lateral asociado.

![](img/13.PNG)

Usamos la opción "Más" para ampliar las opciones disponibles y usamos la opción "Cerrar tablero..."

![](img/14.PNG)

![](img/15.PNG)
