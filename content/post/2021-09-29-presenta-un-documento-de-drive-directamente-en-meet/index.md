---
title: Presenta desde Google Docs, Sheets o Slides directamente a Meet
subtitle: Mejoras para el trabajo colaborativo en videoconferencias
date: 2021-09-29
draft: false
description: Mejoras para el trabajo colaborativo en videoconferencias
tags: ["Google","Drive","Updates","Workspace","Novedades","Meet"]
---

<!-- vscode-markdown-toc -->
* 1. [Enlaces internos](#Enlacesinternos)
* 2. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Google incoropora un nuevo control incrustado en los documentos, hojas de cálculo y presentaciones para compartirlos directamente en Meet. El funcionamiento es muy sencillo, previamente debemos crear la reunión en Meet o seleccionar una reunión programada.

![](img/05.gif)

##  1. <a name='Enlacesinternos'></a>Enlaces internos

* [Publicaciones internas dentro de la categoría Google](https://soka.gitlab.io/blog/tags/google/) .

##  2. <a name='Enlacesexternos'></a>Enlaces externos

* [Present a doc, sheet, or slide directly in Google Meet](https://support.google.com/docs/answer/10540294?hl=en).
* [Present from Google Docs, Sheets, and Slides directly to Google Meet](https://workspaceupdates.googleblog.com/2021/05/present-to-google-meet-directly-from-google-docs-sheets-slides.html).
* [Building the future of work with Google Workspace](https://cloud.google.com/blog/products/workspace/the-future-of-work-with-google-workspace).
* [Transforming collaboration in Google Workspace](https://cloud.google.com/blog/products/workspace/next-evolution-of-collaboration-for-google-workspace) 