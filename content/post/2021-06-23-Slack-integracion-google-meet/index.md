---
title: Integración de Google Meet en Slack
subtitle: Apps para Slack
date: 2021-06-23
draft: false
description: Conectar Meet con Slack os permite a ti y a tu equipo organizaros y estar al día con las notificaciones, recordatorios de eventos y mucho más
tags: ["Slack","Google","Productividad","Aplicaciones","Meet"]
---

<!-- vscode-markdown-toc -->
* 1. [Instalación](#Instalacin)
* 2. [Configuración](#Configuracin)
* 3. [Uso](#Uso)
* 4. [Enlaces internos](#Enlacesinternos)
* 5. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Con la aplicación [Google Meet for Slack](https://meetforslack.com/) puedes organizar una reunión sin salir de Slack en pocos segundos.

**¿Como funciona?**

* Meet for Slack se integra con tu cuenta de Google.
* Cuando introduces el comando `/meet` en cualquier conversación Meet for Slack crea una reunión al instante y comparte el enlace de acceso en la conversación.
* Funciona en DM personales, DM de grupo y en canales (públicos y privados).
* Cuando creas una reunión usando el comando `/meet` se actualiza tu calendario con la reunión durante 15 minutos, además cambia tu estado en Slack.

##  1. <a name='Instalacin'></a>Instalación

![](img/01.png)

La aplicación de puede instalar desde el propio Slack o desde la [página principal de la aplicación](https://meetforslack.com/) usando el botón ["Add to Slack"](https://slack-gmeet.springworks.in/direct-install).

![](img/02.png)

##  2. <a name='Configuracin'></a>Configuración

![](img/03.png)

* Elimina Meet for Slack de todo tu espacio de trabajo y evita que lo utilicen los miembros del entorno de trabajo.

##  3. <a name='Uso'></a>Uso

Los comandos de barra diagonal te permiten realizar una acción con una aplicación enviando sencillamente un mensaje en Slack. Escribe una barra diagonal `/` en cualquier conversación para ver la lista de comandos de barra diagonal disponibles.

Inicia una nueva reunión:

```
/meet [meeting title]
```

![](img/04.gif)

Al escribir el comando `/meet [título del meet]` la aplicación crea un enlace compartido a la reunión.

![](img/04.png)

## Conclusiones

En definitiva, es una buena aplicación en los tiempos que corren, nos permite ahorrar tiempo y saltar a una videoconferencia en esas situaciones en las que no conseguimos avanzar y la conversación mediante mensajes se eterniza.

##  4. <a name='Enlacesinternos'></a>Enlaces internos

* Meet for Slack [https://soka.gitlab.io/blog/post/2021-04-26-meet-for-slack/](https://soka.gitlab.io/blog/post/2021-04-26-meet-for-slack/).
* Slack - Comunicación colaborativa para el trabajo en equipo [https://soka.gitlab.io/blog/post/2021-04-01-taller-slack-spri-enpresa-digitala/](https://soka.gitlab.io/blog/post/2021-04-01-taller-slack-spri-enpresa-digitala/).

##  5. <a name='Enlacesexternos'></a>Enlaces externos

* [https://meetforslack.com/](https://meetforslack.com/)
* [https://wwwhatsnew.com/2021/04/23/google-meet-para-slack-para-organizar-reuniones-en-segundos/](https://wwwhatsnew.com/2021/04/23/google-meet-para-slack-para-organizar-reuniones-en-segundos/)