---
title: geom_bar() - Gráficos de barras
subtitle: Introducción a ggplot2
date: 2019-04-08
draft: false
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "Programación",
    "R - Básico",
    "R - Gráficos",
    "R - Diagramas",
    "R - ggplot2",
    "Diagramas",
  ]
---

![](images/ggplot_hex.jpg)

Vamos con otro de los gráficos más comunes, usaremos un dataset nuevo llamado "mpg" que viene con **ggplot2**, contiene información sobre diversos modelos de coches y sus características.

Como siempre antes de nada cargamos la librería ggplot2 con `library("ggplot2")` (en caso contrario la función `head` de abajo no funciona).

```R
> head(mpg)
# A tibble: 6 x 11
  manufacturer model displ  year   cyl trans      drv     cty   hwy fl    class
  <chr>        <chr> <dbl> <int> <int> <chr>      <chr> <int> <int> <chr> <chr>
1 audi         a4      1.8  1999     4 auto(l5)   f        18    29 p     compact
2 audi         a4      1.8  1999     4 manual(m5) f        21    29 p     compact
3 audi         a4      2    2008     4 manual(m6) f        20    31 p     compact
4 audi         a4      2    2008     4 auto(av)   f        21    30 p     compact
5 audi         a4      2.8  1999     6 auto(l5)   f        16    26 p     compact
6 audi         a4      2.8  1999     6 manual(m5) f        18    26 p     compact
```

`geom_bar` calcula la altura de cada barra basándose en el número de coincidencias o el número de casos para ese grupo, en este caso el eje X contendrá la agrupación de los casos de la columna _class_ con el tipo de vehículo (_compact_, _minivan_, etc), `geom_bar` acepta la siguientes estéticas (_aes_): x, y, alpha, colour, fill, group, linetype, size.

Empezamos como siempre con `ggplot` y definiendo la relación del eje X con la columna _class_ en `aes`. La llamada más simple posible es como sigue:

```R
g <- ggplot(mpg, aes(x=class)) + geom_bar()
```

![](images/geom-bar-01.PNG)

Ahora vamos a hacer algo un poco más avanzado apilando (_stacked_) en cada barra mediante otra agrupación con el parámetro _fill_ (rellenar):

```R
g <-  g + geom_bar(aes(fill=drv))
```

Ahora podemos ver que algunos modelos de coche usan más tracción delantera, trasera, o las 4 ruedas:

![](images/geom-bar-02.PNG)

Otro ejemplo partiendo de un _data.frame_ creado a "mano" en R:

```R
df <- data.frame(trt = c("a", "b", "c"), outcome = c(2.3, 1.9, 3.2))
ggplot(df, aes(x=trt,y=outcome)) + geom_col()
```

![](images/geom-bar-03.PNG)

# Ejercicios

- r / intro / 32-ggplot2-geom-bar / [geom-bar.R](https://gitlab.com/soka/r/blob/master/intro/32-ggplot2-geom-bar/geom-bar.R).

# Enlaces internos

**ggplot2:**

- ["Gráficos avanzados en R - Introducción a ggplot2"](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-avanzados-ggplot2-1/).
- ["geom_line() - Gráficos de líneas"](https://soka.gitlab.io/blog/post/2019-04-01-r-ggplot2-grafico-lineas/).

# Enlaces externos

- ["Bar charts — geom_bar • ggplot2"](https://ggplot2.tidyverse.org/reference/geom_bar.html).
- ["geom_bar| Examples | Plotly"](https://plot.ly/ggplot2/geom_bar/).
- ["ggplot2 barplots : Quick start guide - R software and data visualization ..."](http://www.sthda.com/english/wiki/ggplot2-barplots-quick-start-guide-r-software-and-data-visualization).
