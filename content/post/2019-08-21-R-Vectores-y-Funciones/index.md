---
title: Vectores y funciones
subtitle: Aplicando funciones sobre vectores en R
date: 2019-08-21
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Básico", "R - Funciones", "R - Vectores", "R - Datasets"]
---

![](images/r-logo.PNG)

## Creando vectores

```r
c(1,2,3) # [1] 1 2 3
is.vector(c(1,2,3)) # [1] TRUE
# Como una secuencia de enteros
2:5 # [1] 2 3 4 5
seq(2,3,by=0.5) # [1] 2.0 2.5 3.0

rep(1:2,times=3) # [1] 1 2 1 2 1 2
rep(1:2,each=2) # [1] 1 1 2 2
```

## Seleccionando elementos de un vector

**Por posición**:

```r
v1 <- 1:6
v1[4] # Seleccionando el cuarto elemento
v1[-4] # Todos excepto el cuarto
v1[2:4] # Del 2º al 4º elemento
v1[-(2:4)] # Todos excepto del 2º al 4º
v1[c(1,5)] # El 1º y el 5º elemento
```

**Por valor**:

```r
v1[v1==5] # Elementos que tienen el valor 5
v1[v1>3]  # Elementos con valor mayor que 3
v1[v1 %in% c(2,4)]
```

## Operaciones aritméticas: +, -, \* y ^

Suma:

```r
v1 <- c(1,2,3)
v2 <- c(4,5,6)
v1+v2 # [1] 5 7 9
```

**¿Pero que pasa si realizamos una operación con vectores de diferente longitud?** R usa la regla _recycle rule_ , vuelve a reutilizar elementos del vector más corto:

```r
> v1 <- c(1,2,3)
> v2 <- c(4,5,6,7)
> v1+v2
[1] 5 7 9 8
Warning message:
In v1 + v2 :
  longer object length is not a multiple of shorter object length
```

## Aplicando funciones sobre vectores

Voy a usar el _dataset_ `rivers` que contiene un vector con la longitud de los 141 ríos más largos de Norte America (ver `help(rivers)`), podemos consultar otros _dataset_ de ejemplo con `data()`.

Genero un vector con 7 elementos que contiene: El número de elementos del _dataset_ con [length](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/length), la suma de ellos con [sum](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/sum), la media o [mean](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/mean), [mediana](<https://es.wikipedia.org/wiki/Mediana_(estad%C3%ADstica)>) y [median](https://www.rdocumentation.org/packages/stats/versions/3.6.1/topics/median), [varianza](https://es.wikipedia.org/wiki/Varianza) con [var](https://www.rdocumentation.org/packages/stats/versions/3.6.1/topics/cor), [desviación estándar](https://es.wikipedia.org/wiki/Desviaci%C3%B3n_t%C3%ADpica) con [sd](https://www.rdocumentation.org/packages/stats/versions/3.6.1/topics/sd), el valor mínimo y máximo.

```r
> c(length(rivers),sum(rivers),mean(rivers),median(rivers),
+        var(rivers),sd(rivers),min(rivers),max(rivers))
[1]    141.0000  83357.0000    591.1844    425.0000 243908.4086    493.8708
[7]    135.0000   3710.0000
```

## Parámetros adicionales de funciones

Estas funciones aceptan parámetros adicionales, por ejemplo la función [mean](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/mean) acepta un parámetro `trim` que indica que porcentaje del total de elementos se excluye por el inicio y el final para calcular la media. En el siguiente ejemplo excluye el 25% de 10 elementos, o sea 2 elementos por ambos lados, [mean](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/mean) se aplica sobre 1, 2, 3, 6:

```r
> length(c(-100, 0, 1, 2, 3, 6, 50, 73))
[1] 8
> mean(c(-100, 0, 1, 2, 3, 6, 50, 73), trim = 0.25)
[1] 3
```

Queremos excluir 10 observaciones por el inicio y por el final del vector del _dataset_ rivers con 141 elementos:

```r
> trimfraction <- 10/length(rivers)
> mean(rivers, trim = trimfraction)
[1] 505.719
```

## Funciones que aceptan múltiples vectores

Usaré el dataset `women` que contiene un _dataframe_ con 15 observaciones de 2 variables con las [medias](https://es.wikipedia.org/wiki/Media_aritm%C3%A9tica) de altura y peso de mujeres entre 30 y 39 años. Podemos inspeccionar como está formado el _dataframe_:

```r
?women
head(women)
class(women)
is.data.frame(women)
str(women)
View(women)
summary(women)
```

La función [cor](https://www.rdocumentation.org/packages/stats/versions/3.6.1/topics/cor) mide la [correlación](https://es.wikipedia.org/wiki/Correlaci%C3%B3n) entre dos variables estadísticas, se considera que dos variables están correlacionadas cuando los valores de una de las variables varía respecto a los valores de la otra, si tenemos dos variables (A y B) existe correlación entre ellas si al disminuir los valores de A lo hacen también los de B y viceversa. Existen diferentes métodos para medir la correlación ("pearson", "kendall" o "spearman"), voy a usar el [coeficiente de correlación de Pearson](https://es.wikipedia.org/wiki/Coeficiente_de_correlaci%C3%B3n_de_Pearson) en el ejemplo:

```r
attach(women)
cor(height, weight,method=c("pearson"))
```

La función [attach](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/attach) permite acceder a las observaciones del _dataframe_ por sus nombres.

El resultado de 0.9954948 obtenido con [cor](https://www.rdocumentation.org/packages/stats/versions/3.6.1/topics/cor) indica que existe una correlación positiva, en un post previo ["Diagramas de dispersión y modelo de regresión líneal en R"](https://soka.gitlab.io/blog/post/2019-02-05-diagramas-dispersion-en-r/) se muestra como podemos representar dos variables de forma visual mediante un diagrama de dispersión y medir el grado de correlación.

## Funciones que retornan vectores

En los ejemplos anteriores he empleado funciones que reciben vectores y retornan un único valor como resultado, las siguientes funciones retornan un vector con la misma longitud del vector de entrada.

La función [round](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/Round) realiza un redondeo:

```r
> x <- c(300.99, 1.6, 583, 42.10)
> y <- round(x)
> y
[1] 301   2 583  42
```

[ceiling](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/Round) nos devuelve el próximo valor entero del número elegido.

```r
> ceiling(c(300.99, 1.6, 583, 42.10))
[1] 301   2 583  43
```

[trunc](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/Round) es básicamente lo que conocemos matemáticamente como truncar un valor. Es decir, cortamos literalmente por el número entero.

## Funciones de ordenación

R tiene varias funciones para ordenar datos. [sort](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/sort) recibe un vector y lo devuelve ordenado de forma incremental (se puede usar `decreasing=TRUE` para invertir el orden):

```r
> sort(c(1,3,2,7,5))
[1] 1 2 3 5 7
```

Estos ejemplos están basados en la documentación original, el _dataset_ swiss contiene datos socio económicos de 47 provincias de Suiza, una de las variables es `[,4] Education % education beyond primary school for draftees.`.

```r
x <- swiss$Education[1:25]
x;sort(x)
```

La función [order](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/order) devuelve los índices de los elementos del vector de menor a mayor:

```r
> order(c(1,3,2,7,5))
[1] 1 3 2 5 4
```

## Código fuente

- r / intro / 51-Vectores-y-Funciones / [Vectores-y-Funciones.R](https://gitlab.com/soka/r/blob/master/intro/51-Vectores-y-Funciones/Vectores-y-Funciones.R).

## Enlaces internos

- ["Vectores"](https://soka.gitlab.io/blog/post/2019-03-11-r-vectores/): Introducción básica a vectores.
- ["Diagramas de dispersión y modelo de regresión líneal en R"](https://soka.gitlab.io/blog/post/2019-02-05-diagramas-dispersion-en-r/).
- ["Diagramas de dispersión en R (1/2)"](https://soka.gitlab.io/blog/post/2019-04-25-r-ggplot2-scatterplot/).

## Enlaces externos

![](https://www.r-exercises.com/wp-content/uploads/2019/07/Start-here-to-learn-r-vol.-1-book-cover-600x854.png)

- ["Working With Vectors"](https://www.r-exercises.com/2019/08/05/working-with-vectors/) 5 August 2019 by Han de Vries.
- ["Vectors and Functions"](https://www.r-bloggers.com/vectors-and-functions/) By Han de Vries. Contenidos originales en los que se base este artículo.
- statmethods.net ["Correlations"](https://www.statmethods.net/stats/correlations.html).
- R para profesionales de los datos: una introducción ["3.4 Ordenación"](https://www.datanalytics.com/libro_r/ordenacion-1.html) Carlos J. Gil Bellosta - 2018-04-22.
