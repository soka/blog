---
title: Lo que consonni me ha enseñado
subtitle: primero aprender, después desprenderse y finalmente trascender.
date: 2019-07-17
draft: true
description: Manipulación de datos y análisis estadístico
tags: ["Trello", "Gestión de proyectos", "Metodologías", "Trabajo colaboratio"]
---

![](images/logo-consonni-nuevo.png)

Hace unos días impartí la tercera y última sesión formativa y de "consultoría" en una pequeña empresa llamada [**consonni**](https://www.consonni.org/), el propósito era explorar nuevas formas de colaboración digital y hacer frente a algunas problemáticas que viven en el día a día, a pesar de que provengo del ámbito de las tecnologías creí que mis experiencias pasadas podían resultar provechosas.

**Las dos primeras sesiones no revisten especial interés** ya que se centraron en una breve introducción a los sistemas y la metodología **Kanban** y algunos trucos e ideas de como aplicarlo en un tablero digital con **Trello**, de una forma muy libre y fléxible siempre, al final de este artículo pongo algunos enlaces con información (un poco desordenada) especialmente sobre **Trello** (aún debo revisarlos y añadir algunas cosas).

Volviendo a la **última sesión** esta se planteaba sin ningún esquema predeterminado cuyo único objetivo era estudiar como implantar **Trello** en [**consonni**](https://www.consonni.org/), fue una labor de puesta en común ofreciendo ideas y consejos basados en mi experiencia personal y de construcción en grupo entre todas. Para ello previamente debía conocer como trabajan y que producen, es un momento de escucha y concentración para ofrecer alternativas y dudas que se germinen a su vez nuevas ideas.

## consonni

La primera letra de [**consonni**](https://www.consonni.org/) se escribe en minúsculas, pero las personas que lo componen son muy grandes, esto fue lo primero que aprendí con este grupo de personas geniales.

[**consonni**](https://www.consonni.org/) es un espacio de creación cultural formado por cinco personas jóvenes y muy dinámicas, para mí es difícil dar una definición precisa de su ámbito de trabajo, probablemente

## La problemática

En sus comunicaciones diarias generan un volumen considerable de correos electrónicos, en muchos casos estos correos son internos entre sus trabajadoras, la información se pierde en algunos casos y es muy difícil seguir un hilo o desarrollar una comunicación efectiva de todo el equipo.

[**consonni**](https://www.consonni.org/) trabaja muchos ámbitos de la producción cultural, desde la edición de libros hasta emisiones de radio o eventos públicos, entre todos esos trabajos que realizan nos propusimos elegir una de las áreas de actividad que tiene más peso en ....

Un producto final "tangible" y que además tiene una clara orientación a proyecto porque pasa por una etapas que abarcan desde la búsqueda de nuevos libros y la negociación de derechos hasta la publicación, distribución y comunicación.

Su objetivo es publicar diez libros

## Identificar el elemento básico de información y lxs actores involucrados

El primer paso consistió en identificar un proceso que se prestase a ser estudiado y en que participasen de una forma u otra todo el equipo de [**consonni**](https://www.consonni.org/).

Identificar los procesos y explicitar un proyecto de producción de un libro

El primer debate

## El proceso de producción de un libro

Las siguientes fotos corresponden a un instante de la tercera sesión, en un momento dado cualquier soporte digital no era suficiente para expresar ideas y conceptos y dimos el salto al papel y rotulador para identificar las etapas de producción de un nuevo libro.

FLujo de trabajo.

Una de las persoñas señalaba que si el elemento es el libro debe estar representado sólo por una tarjeta en el tablero,

Se produce una normalización de los datos y atributos asociados a un libro y se crea una tarjeta que sirva de plantilla

Las tareas que componen todo el proyecto se definen como subtareas , dificulta un poco seguimiento ya que la asignación de la tara se realiza por tarjeta (que es un libro) y como mucho en un _checklist_ se puede preceder cada elemento con su responsable.

## Algunas conclusiones

Huir de las formulas rígidas y adoptar un sistema flexible. Buscar el equilibrio entre un estándar ....

Las claves son el consenso y respeto mutuo, en muchos casos opiniones encontradas han derivado en ideas super provechosas y ha permitido definir una visión compartida del proceso donde todas las personas finalmente estaban de acuerdo en hacerlo de cierta manera.

Sin un acuerdo difícilmente se adoptará la herramienta si resulta poco útil para algunas personas o sienten que no les facilita su trabajo

La escucha proactiva y sobre todo el una visión compartida de un objetivo.

Terminología común y el peso de las palabras, no querían identificar las fases con las áreas funcionales

La necesidad de unificar herramientas

Un proceso de construcción compartido

No he usado ninguna captura de los tableros ya que el producto del trabajo en equipo es enteramente de **consonni** y espero que les ayude y que en pocas semanas empiecen a notar los beneficios.

No se ha profundizado (tampoco era el propósito) en como implementar métricas y medir la productividad / rendimiento.

Por falta de tiempo especialmente no se han explorado otras configuraciones de tableros, en la segunda sesión para practicar un poco se generó un tablero con una lista de cada una de las personas que componen el equipo, una buena forma de conocerse.

## Recursos para el curso

## Todas las fotos
