---
title: Hormigas digitales
subtitle: Comunidades libres en la red
date: 2019-10-17
draft: true
description: 
tags: ["Aplicaciones","Redes"]
---


## Twitter


Cuenta suspendida en Twitter: https://twitter.com/anonktalonia


![](images/01.png)

https://twitter.com/tsunami_dem

## Telegram

https://t.me/anoncatalonia

https://t.me/tsunamid


## Webs censuradas

tsunamidemocratic.cat redireccion a (http://82.223.97.47/) Pertenece a Arsys? La ip real es 104.18.37.39 de Cloudfare
app.tsunamidemocratic.cat
tsdem.org
api.tsdem.org
app.tsdem.org
tsunamidemocratic.net
tsunamidemocratic.com
app.tsunamidemocratic.com

![](images/02.png)

```sh
$ curl -i 'http://tsunamidemocratic.cat' 
HTTP/1.1 200 OK
Server: nginx
Date: Fri, 18 Oct 2019 20:05:06 GMT
Content-Type: text/html
Content-Length: 119
Connection: keep-alive
X-Accel-Version: 0.01
Last-Modified: Thu, 12 Sep 2019 08:30:42 GMT
ETag: "77-59256f40af9c4"
Accept-Ranges: bytes
Vary: Accept-Encoding
MS-Author-Via: DAV
X-Powered-By: PleskLin

<!DOCTYPE html>
<html>
<body>

<center><img src="Montaje_Bloqueo.jpg" style="margin-top:3%"></center>

</body>
</html>
```

http://whois.domaintools.com/tsunamidemocratic.cat

DNS Cloudflare

Registrado en https://www.tucowsdomains.com/. 

Acaban de bloquear https://tsunamidemocratic.github.io/

## TOR + Firefox + Proxy Switcher

Con TOR no rula http://tsunamidemocratic.cat/

https://riseup.net/es/vpn/linux

Usando una Web VPN Sí! Cambia IP origen y la situa en otro pais.

Surge una https://tsunamidemocratic.github.io/ que funciona!

## Protonmail

premsatsunamid@protonmail.com


## APP




https://www.elnacional.cat/ca/politica/comissio-europea-tsunami-democratic_432110_102.html


## Cambiar DNS en router doméstico

DNS Jazztel:
87.216.1.65
87.216.1.66

# DNS-over-HTTPS (DoH) en Firefox

DNS mediante HTTPS (DoH, siglas en inglés de DNS over HTTPS) es un protocolo de seguridad para realizar una resolución remota del sistema de nombres de dominio (DNS) a través del protocolo HTTPS. Uno de los objetivos del método es aumentar la privacidad y la seguridad de los usuarios mediante la prevención de las escuchas ilegales y la manipulación de los datos del DNS a través de ataques de intermediario.

Salvar la censura usando los DNSs de Cloudfare

https://1.1.1.1/help

![](images/03.png)

- Escribir about:config en la barra de la URL del navegador
- network.trr.mode: Ponemos  2 (DoH is enabled, and regular DNS works as a backup).
- network.trr.uri: URL del DNS compatible DoH usamos "https://mozilla.cloudflare-dns.com/dns-query".
- network.trr.bootstrapAddress: IP resolución DNS Cloudfare "1.1.1.1" (opcional).



