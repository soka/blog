---
title: Ajuste dinámico de precios en comercios electrónicos (1/3) [BORRADOR]
subtitle: Introducción, estrategias en el sector retail
date: 2019-01-18
draft: true
description: Predicción de la demanda y tecnologías
tags: ["Algoritmos","eCommerce","Machine learning","Marketing","R","Economía","Comercio"]
---

# Introducción

![](images/chiste.jpg)

[Dilbert - by Scott Adams [Fuente](https://dilbert.com/)]

https://www.genbeta.com/actualidad/deepmind-ia-google-pulveriza-a-jugadores-profesionales-starcraft-ii
https://ecommerce-news.es/no-la-ia-no-salvara-tu-estrategia-de-atribucion-en-2019-92004
https://iahuawei.xataka.com/como-puedo-aprovechar-hoy-oportunidades-inteligencia-artificial/
https://oriolbozzo.wordpress.com/2017/01/27/marketing-mix-la-p-de-precio-inditex/
https://observatorioecommerce.com/marketplaces-mas-influyentes/
https://medium.com/@mehulved1503/dynamic-pricing-in-e-commerce-3f0a066a3728
https://medium.com/@mehulved1503/dynamic-pricing-in-e-commerce-3f0a066a3728
https://www.iebschool.com/blog/que-es-churn-rate-marketing-digital/
https://iahuawei.xataka.com/ia-inteligente-va-a-quitarnos-trabajo-ocho-mitos-interrogantes/
https://ingenioempresa.com/promedio-movil-ponderado/
https://www.rankia.cl/blog/analisis-ipsa/2039072-medias-movil-simple-exponencial-ponderada-formulas-ejemplos
https://www.gestiondeoperaciones.net/proyeccion-de-demanda/calculo-del-mad-y-la-senal-de-rastreo-para-un-pronostico-de-demanda/

https://retina.elpais.com/retina/2018/11/15/tendencias/1542275835_464715.html

Este el primer post de una serie de ellos sobre **ajuste dinámico de precios, machine learning y modelos predictivos de la demanda**, este primero se centra más en estrategias de fijación de precios y como la aplican grandes tiendas en línea (descuentos y promociones, combinaciones de productos, estudio del comportamiento de los usuarios, etc). El siguiente es ["Machine learning"](https://soka.gitlab.io/blog/post/2019-01-18-precios-dinamicos-ecommerce-2/) donde quiero revisar conceptos teoricos y algunas soluciones de mercado para análiticas de negocio y ventas. El tercero y último por el momento es ["Modelos estadísticos y algoritmos de predicción de la demanda"](https://soka.gitlab.io/blog/post/2019-01-18-precios-dinamicos-ecommerce-3/), profundiza en modelos y ejemplos básicos. Pretendía hacer algo resumido pero por el momento soy incapaz de sintetizarlo como un todo lógico.

La humanidad siempre ha sido curiosa respecto a que le depara el futuro, tratamos de predecir pequeñas cosas como el resultado de un encuentro deportivo (no es mi caso) o el partido vencedor de las próximas elecciones. Se atribuie al presidente de IBM [Thomas Watson la siguiente predicción en 1943]((https://maikelnai.naukas.com/2008/06/26/las-mas-famosas-predicciones-erroneas-de-la-historia-reciente/)): 

_"Creo que existe un mercado mundial para tal vez… cinco computadoras"_

Esta predicción se ha demostrado evidentemente como un gran error en el mundo de las tecnologías, esto pone de relieve a su vez **la importancia de las predicciones económicas para el éxito de nuestra empresa en el futuro**. 

En la era pre-internet la capacidad para establecer diferentes precios para diferentes consumidores era muy limitada. Con la llegada de Amazon, eBay y otras plataformas de ventas masivas en línea (los famosos marketplaces) **los eCommerce pueden desarrollar portales especificos para cada cliente basado en sus necesidades**, la información que podemos recopilar de nuestros sistemas ofrece interesantes posibilidades para personalizar servicios a un nivel de detalle tremendo (si sabemos reconocer las señales). 

Lo mismo que todas las personas no son iguales tampoco los usuarios de nuestra tienda en línea lo son, factores culturales como el lugar de origen, factores económicos, preferencias personales u otros pueden determinar el gasto medio de nuestro cliente, **el factor precio aún prevalece como razon para adquirir un producto entre los consumidores** ([para el 60% de los compradores](http://www.pwc.com/gx/en/industries/retail-consumer/global-total-retail.html)) 

_¿Cuanto esta dispuesto a pagar un **cliente** por nuestro **producto**?_. 

Además el usuario puede comparar precios y características en diferentes sitios Web o buscar productos similares o alternativos. La siguiente gráfica está obtenida con [Camel Camel Camel](https://es.camelcamelcamel.com/) con la fluctuación de precios de venta de un producto concreto en Amazon.

![](images/camelcamelcamel_01.PNG)

# Ajuste de precios dinámicos en eCommerce

**La variación de precios de forma dinámica en respuesta a cambios en los mercados puede ayudarnos a maximizar los beneficios y crear ventajas competitivas a nuestro favor**, también a minizar la incertidumbre y el riesgo de una mala decisión.

Volviendo al tema de las grandes empresas y sus estrategias de fijación de precios, **en el sector retail Zara fija diferentes precios basándose en los ingresos medios por familia** dependiendo del pais en el que operan sus tiendas.

Algunos negocios apuestan por ofrecer sus productos en **marketplaces** (plataformas en las que se venden productos de diferentes negocios, marcas, empresas) creados por empresas como eBay, Amazon o AliExpress que actuan como intermediarios.

Voy a tratar de agrupar de forma resumida algunas de las **variables principales que pueden afectar a nuestras ventas**.

**El catalogo de productos**, sus atributos y clasificación en clases lógicas que permita más tarde agruparlos basandonos en diferentes criterios. También deberiamos tomar en cuenta la tipología de los productos y sus atributos. Todo o casi todo gira en torno al producto que ofrecemos, modelizar estos artículos para nuestra estrategia de precios es esencial, la calidad de estos datos puede determinar el éxito de nuestro proyecto.

**Existencias y costes**: Si contamos con stock o cada uno es único por ejemplo, dentro de este campo puede entrar el **margen deseado coste-beneficio**.


**Demanda**: Una alta demanda de nuestro producto en el futuro puede animarmos a subir el precio o al contrario un producto que no se vende bien puede ser aconsejable una bajada de precios.  

Datos específicos del **consumidor y datos historicos de ventas**. Ejemplos como el total que paga el cliente en cada compra (de uno o varios productos), cantidad de artículos por cada compra. 

**Precios y promociones**. Tipos de promociones y precios, pueden estar sujetas a un producto concreto, el clásico descuento por volumen por ejemplo, o al conjunto de la compra (descuentos por carrito de la compra combinando X productos), hoy en día las aplicaciones eCommerce ofrecen muchas y variadas formas de ofrecer descuentos, por tiempo límitado, los típicos cupones de descuento especiales para algunos clientes,etc. 

Precios de la **competencia** de productos similares. 

**Calendarios y eventos**. Si el día de la semana es laborable, fin de semana, festivo o un evento concreto como el Black Friday.

# Enlaces externos



# Imágenes

