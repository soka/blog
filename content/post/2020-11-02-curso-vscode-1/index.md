---
title: Introducción a Visual Studio Code [borrador]
subtitle: Sesión nº 1 curso Visual Studio Code
date: 2020-11-02
draft: false
description: Instalación y configuración
tags: ["Cursos","Curso VSCode","Visual Studio Code","Aplicaciones","Editores"]
---

<p align="center">  
  <img src="img/iker-circle-mini.png" />
</p>

<h1>Introducción a Visual Studio Code</h1>

[**Visual Studio Code**](https://es.wikipedia.org/wiki/Visual_Studio_Code) es un editor de código fuente **multiplataforma** desarrollado por M$. Incluye soporte para la **depuración**, control integrado de **Git de forma nativa**, **resaltado de sintaxis**, **autocompleta el código** de forma inteligente, etc. También es personalizable, por lo que los usuarios pueden cambiar el tema del editor, los atajos de teclado y las preferencias. Es gratuito y de [**código abierto**](https://es.wikipedia.org/wiki/C%C3%B3digo_abierto).

**Visual Studio Code** se basa en [**Electron**](https://es.wikipedia.org/wiki/Electron_(software)), un _framework_ que se utiliza para implementar Chromium y Node.js como aplicaciones para escritorio ([https://soka.gitlab.io/electron/](https://soka.gitlab.io/electron/)).

## Pad de trabajo

[https://pad.riseup.net/p/penascalf52021](https://pad.riseup.net/p/penascalf52021)

## Instalación Visual Studio Code en Linux Mint

### Usando Snap

[Snap](https://es.wikipedia.org/wiki/Snappy) es un sistema de paquetes autocontenidos, funcionan en cualquier distribución y dispositivo (distribuciones que lo soporten claro)

Instalar Snap:

```bash
sudo rm /etc/apt/preferences.d/nosnap.pref
sudo apt update
sudo apt install snapd
```

Instalar VSCode:

```bash
sudo snap install code --classic
```

Ejecución:

```bash
snap run code
```

## Usando el paquete para Debian de la Web oficial

Acceder a [https://code.visualstudio.com/](https://code.visualstudio.com/) y usar el paquete .deb para Debian y otras distribuciones derivadas como Ubuntu o Mint.

![](img/01.png)

Cuando pinchamos sobre el enlace reconoce que debe abrirlo con [GDebi](https://es.wikipedia.org/wiki/GDebi) (en línea de comandos con `dpkg -i paquete`).

## Atajos de teclado (keyboard shortcuts)

Obtenido de la Web oficial en [https://code.visualstudio.com/shortcuts/keyboard-shortcuts-linux.pdf](https://code.visualstudio.com/shortcuts/keyboard-shortcuts-linux.pdf).

![](img/03.png)

## Instalación Git

Abrir el terminal en VSCode con "CTRL +`" (para cerrar también) o desde el menú "Ver > Terminal".

```bash
sudo apt update
sudo apt upgrade

sudo apt search git
sudo apt install git
```

Desinstalar con `sudo apt remove git`.

Comprobar versión GIT en terminal:

```bash
git --version
```

## Configurar el login en Git

**Pendiente**.

## Clonando el primer repositorio

Creamos y abrimos una carpeta para proyectos en /Documentos/GitLab.

Abrimos paleta de comandos en VSCode con `CTRL + Mayús + P` y escribimos `Git: Clone`. Nos pedira la URL del repositorio [https://gitlab.com/soka/penascalf52021.git](https://gitlab.com/soka/penascalf52021.git).

También se puede hacer desde el CLI de VSCode `git clone https://gitlab.com/soka/penascalf52021.git` ("CTRL +`").

Abrimos carpeta con `CTRL+K` y `CTRL+O`.

## Nuestro primer fichero

Creamos un fichero en la carpeta [ej01/index.html](https://gitlab.com/soka/penascalf52021/-/tree/master/ej01):

```html
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Ejercicio 1</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <script>
    alert("RUN");
    var nombre = prompt("Dame tu nombre!");
    console.log(nombre);
</script>
</head>

<body>
  
</body>
</html>
```

## Depurar código JS en VSCode

Instalo la extensión (`CTRL + Mayús + X`) [Debugger for Firefox](https://marketplace.visualstudio.com/items?itemName=firefox-devtools.vscode-firefox-debug) en VSCode, permite depurar el código ejecutado en la parte cliente como JS.

## La barra de estado

![](img/02.png)

Contiene la siguiente información de izquierda a derecha (además de otra información de _plugins_):

- Información estado de Git y rama.
- Errores y _warnings_ del código fuente.
- Posición del cursor, línea y columna.
- Identación o sangría (se puede clicar para cambiar).
- Codificación ([UTF-8](https://es.wikipedia.org/wiki/UTF-8)).
- Tipo de fin de línea (se puede cambiar haciendo clic).
- Lenguaje del fichero.
- Notificaciones (normalmente actualizaciones).

## Barra de actividad

![](img/04.png)

Atajos para el explorador de archivos, búsqueda, Git, depuración, extensiones y ajustes. 

## Explorador de archivos

Provee una vista ordenada de las carpetas y archivos con los que estamos trabajando.

![](img/05.png)

## User Interface

[https://code.visualstudio.com/docs/getstarted/userinterface](https://code.visualstudio.com/docs/getstarted/userinterface)

## Basic Editing

[https://code.visualstudio.com/docs/editor/codebasics](https://code.visualstudio.com/docs/editor/codebasics)

## Enlaces externos

- [https://marketplace.visualstudio.com/items?itemName=firefox-devtools.vscode-firefox-debug](https://marketplace.visualstudio.com/items?itemName=firefox-devtools.vscode-firefox-debug)
- [https://code.visualstudio.com/docs/editor/debugging](https://code.visualstudio.com/docs/editor/debugging).
- [https://hostadvice.com/how-to/how-to-use-the-apt-command-to-manage-ubuntu-packages/](https://hostadvice.com/how-to/how-to-use-the-apt-command-to-manage-ubuntu-packages/).
- [https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html).
- [https://marketplace.visualstudio.com/items?itemName=donjayamanne.git-extension-pack](https://marketplace.visualstudio.com/items?itemName=donjayamanne.git-extension-pack).


