---
title: Introducción Framework Bootstrap
subtitle: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 0
date: 2022-04-26
draft: false
description: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 0
tags: ["PEPHPPAW_3","Cursos","Miriadax","Programación","Web","Desarrollo","HTML","CSS","Bootstrap"]
---

<!-- vscode-markdown-toc -->
* 1. [Video parte I: Módulo 0 Framework Boostrap parte 1](#VideoparteI:Mdulo0FrameworkBoostrapparte1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

A continuación encontrarás los vídeos que te permitirán adquirir habilidades en el manejo de un Framework relacionado con el diseño web, el cual se llama Bootstrap.

##  1. <a name='VideoparteI:Mdulo0FrameworkBoostrapparte1'></a>Video parte I: Módulo 0 Framework Boostrap parte 1

{{< youtube WnJy061CslY >}}

[https://www.youtube.com/watch?v=WnJy061CslY](https://www.youtube.com/watch?v=WnJy061CslY)

[https://getbootstrap.com/docs/5.1/getting-started/introduction/](https://getbootstrap.com/docs/5.1/getting-started/introduction/)

Copiamos la plantilla de inicio rápido en un fichero HTML nuevo.

![](img/01.png)

Fuente: [src/bootstrap-01.html](src/bootstrap-01.html).

Vamos a introducir una tabla [https://getbootstrap.com/docs/5.1/content/tables/](https://getbootstrap.com/docs/5.1/content/tables/).

![](img/02.png)

```html
<table class="table table-bordered border-primary">
  ...
</table>
```

Vamos a probar los [botones](https://getbootstrap.com/docs/5.1/components/buttons/).

![](img/03.png)

```html
<button type="button" class="btn btn-primary">Primary</button>
<button type="button" class="btn btn-secondary">Secondary</button>
<button type="button" class="btn btn-success">Success</button>
<button type="button" class="btn btn-danger">Danger</button>
<button type="button" class="btn btn-warning">Warning</button>
<button type="button" class="btn btn-info">Info</button>
<button type="button" class="btn btn-light">Light</button>
<button type="button" class="btn btn-dark">Dark</button>

<button type="button" class="btn btn-link">Link</button>
```

Este es el aspecto que tiene la Web por el momento:

![](img/04.png)

Ahora usaremos los [contenedores](https://getbootstrap.com/docs/5.1/layout/containers/) de Bootstrap para ordenar el contenido. 

```html
<div class="container">
  <!-- Content here -->
</div>
```

Podemos comprobar que es responsivo. 

Otro contenido que se trabajará más adelante están los [formularios](https://getbootstrap.com/docs/5.1/forms/overview/), Bootstrap tiene una sección entera sólo dedicada a los formularios, este es un ejemplo que he introducido en mi proyecto:

```html
<form>
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1">
  </div>
  <div class="mb-3 form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
```

![](img/05.png)

