---
title: Creando accesos directos con GPO
subtitle: Administración de dominio con Windows Server 
date: 2021-07-09
draft: false
description: 
tags: ["Windows","GPO","Windows Server","Políticas","Red","Usuarios","Mapeo","Unidades de red"]
---

<!-- vscode-markdown-toc -->

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

En esta ocasión voy a crear una GPO para crear una acceso directo en la maquina cliente que apunte a la carpeta compartida (también podemos hacerlo para crear un acceso directo a una aplicación). Abro la consola de políticas de grupo con `gpmc.msc`.

Creo una nueva política en objetos de directuiva de grupo y le asigno un nombre significativo, quito el grupo por defecto que aplica (Usuarios autentificados) y añado un grupo creado por mi al efecto llamado "Trabajadores".  Verificamos que el grupo tenga permisos para leer y aplicar la política en la pestaña "Delegación" con el botón "Opciones avanzadas".

Ahora debo vincular la política al dominio, después vuelvo a la política y con el botón derecho la edito para crear el enlace.

![](img/01.PNG)

Hago click derecho sobre Accesos directos y selecciono un nuevo acceso directo. En este caso vamos a crearlo en lugar de reemplazarlo o otra opción ya que sólo se creará una vez y no se cambiará la ruta normalmente. Selecciono una ubicación donde se creará el acceso, lo crearé en el Escritorio (puede ser en unidades de red, inicio y en otros muchos sitios). Si queremos podemos seleccionar un icono y otras opciones, por el momento lo dejo así:

![](img/02.PNG)

Ya está creada:

![](img/03.PNG)

Ahora sólo queda un despliegue de la política ejecutando `gpupdate /force`.

## Comprobar problemas

* Comprobar el visor de eventos (`eventvwr.exe`).

## Enlaces internos

* [Mapeo automático de carpetas de Red usando GPO](https://soka.gitlab.io/blog/post/2021-07-09-mapeo-auto-carpeta-red-gpo-win-server/).


