---
title: Software libre para una sociedad libre
subtitle: La inteligencia contra el capitalismo
date: 2020-11-27
draft: false
description: Historia, evolución y valores 
tags: ["Free Software","Software Libre","GPL","GNU","Copyright","Copyleft"]
---

## Introducción

El titulo de esta entrada está directamente basada en el ensayo del mismo título cuyo autor es [**Richard M.Stallman**](https://es.wikipedia.org/wiki/Richard_Stallman), muchas de sus ideas son usadas directamente en este artículo, el subtítulo algo subversivo proviene de un [artículo](https://www.laizquierdadiario.com/Aaron-Swartz-el-caso-del-capitalismo-contra-la-inteligencia) dedicado a un genio llamado [**Aaron Swartz**](https://es.wikipedia.org/wiki/Aaron_Swartz), programador y activista político de izquierdas cuya vida se vio truncada cuando se quito la vida empujado probablemente por una hostigamiento del aparato judicial y policial criminal de EEUU.

![](img/03.jpg)

Siempre que hablo de software libre me gusta citar este eslogan ciberpunk (el enlace original ya no está en lasindias.net):

> “Bajo toda arquitectura informacional se oculta una estructura de poder”

**La topología de una red o el diseño de un software por tanto tiene profundas implicaciones en términos de libertad**, las infraestructuras digitales determinan como la información y el conocimiento pueden fluir por sus conexiones entre nodos, a un nivel más profundo hablando de tecnologías de la información podemos hablar de arquitecturas cliente-servidor, topologías de red descentralizadas VS centralizadas, de información distribuida o de patrones de diseño software.

![](img/08.gif)

“El concepto de "descentralización" se escuchó por primera vez nada menos que a mediados del siglo XIX, cuando se hablaba de él como la fuerza que inspiró la Revolución Francesa: "La descentralización tiene una dimensión cívica, ya que aumenta las oportunidades de los ciudadanos de participar en asuntos públicos, y les acostumbra a usar su libertad", escribía el abogado Alexis de Tocqueville.”

![](img/09.jpg)

En una red distribuida **todos los nodos se conectan entre si sin que tengan que pasar necesariamente por uno o varios centros**. Desaparece la división centro/periferia y por tanto el poder de filtro y censura sobre la información que fluye por ella. La red es robusta ante la caída de nodos: ningún nodo al ser extraído genera la desconexión de otro, surge una [pluriarquía](https://blogs.20minutos.es/codigo-abierto/2012/05/26/la-era-de-la-pluriarquia/) (_“todo actor individual decide sobre sí mismo, pero carece de la capacidad y de la oportunidad para decidir sobre cualquiera de los demás actores”_) con una gobernanza compartida.

El **software libre, la privacidad, el anonimato y la transparencia** también son a mi parecer uno de los pilares de la [**soberanía tecnológica**](https://es.wikipedia.org/wiki/Soberan%C3%ADa_tecnol%C3%B3gica), una sociedad consciente y responsable en las tecnologías que decide consumir, basadas en alternativas autogestionadas y libres.

Finalmente antés de empezar creo firmemente que las tecnologías están guiadas o imbuidas por los valores de sus creadores.

---

## El proyecto GNU y el software libre

La historia del software libre está indisolublemente unida a un personaje muy carismático llamado Richard Stallman, programador de profesión que comenzó trabajando en el MIT, supo cruzar valores humanos como la libertad con algo aparentemente tan inocuo cómo es el código y que cada vez rige más nuestras vidas.

![](img/02.jpg)

Usaré mucho el termino "libre" en las siguientes líneas, antes de empezar debemos desambiguar este término, especialmente su equivalente en inglés "free" puede adoptar diferentes significados, **uno de ellos es cuando se refiere a un precio o cuando un producto es gratis, pero Stallman se refiere a libre en un significado más profundo referido a los derechos que podemos ejercer sin restricciones** ([Gratis frente a libre](https://es.wikipedia.org/wiki/Gratis_frente_a_libre)), Richard Stallman resumió la diferencia en un eslogan: "Piense en free como en libertad de expresión, no como en cerveza gratis" (_"Think free as in free speech, not free beer"_) ([Free, as in Beer](https://www.wired.com/2006/09/free-as-in-beer/) - Lawrence Lessig, 2006).​ 

Una de las acepciones de [libre en la RAE](https://dle.rae.es/libre) me parece que se ajusta bien "Independiente o no sujeto a una autoridad superior".

---

## Terminología básica

### Copyright / Copyleft

![](img/05.png)

El [**Copyright**](https://es.wikipedia.org/wiki/Derecho_de_autor) (traducido literalmente como ‘derecho de copia’) o derecho de autor concede a los creadores la propiedad intelectual sobre sus obras y el derecho a permitir su publicación, copia o modificación. Normalmente transcurrido un tiempo las obran pasar a ser de **dominio público** (simplemente significa que no tiene propietario, se puede modificar y hacer una versión propietaria a partir de él).

La **licencia** sin embargo se refiere a los términos bajo los que se distribuye el software.

![](img/06.jpg)

En EEUU muchas obras pueden estar "protegidas" por la ley de derechos de autor 70 años después de la muerte del autor, o 120 años después de la creación o 95 años después de la publicación, ciertamente es un poco exagerado, casos como los derechos de Elvis Presley que han sido explotados después de su muerte por sus herederos y después comprados por una marca generan millones de beneficios anuales en [_royalties_](https://es.wikipedia.org/wiki/Regal%C3%ADa).

Por ejemplo el Copyright establece quien es el dueño de Windows, por mucho que lo distribuía y te deje usarlo.

El [_lobby_](https://es.wikipedia.org/wiki/Grupo_de_presi%C3%B3n) de Disney consiguió gracias a la ley conocida como  “Ley de Protección de Mickey Mouse” alargar sus derechos hasta 2024 (publicadas entre 1923 y 1977), por comentar [Walt Disney](https://es.wikipedia.org/wiki/Walt_Disney) murió en 1966, no se los derechos de quien protege si su creador está muerto o congelado.

![](img/04.jpg)

Es muy anecdótico el caso de la mítica película de terror "La noche de los muertos vivientes" (1968), del director George A.Romero que pertenece al **dominio público**, no por deseo expreso del director o la productora ni porque hayan transcurrido unos años y sus derechos hayan caducado, un error permitió que se distribuyese sin incluir la nota del copyright en los títulos de crédito.  

En contraposición al copyright nace el mecanísmo del [**copyleft**](https://es.wikipedia.org/wiki/Copyleft) para producir software transparente, susceptible de modificación y libre que se implementa a través de las licencia libres como la GPL.

### Open Source / Free Software

![](img/16.jpg)

A menudo se confunde el software de [código abierto con el software libre](https://es.wikipedia.org/wiki/Software_libre_y_de_c%C3%B3digo_abierto), ambos comparten el principio del acceso al código fuente pero el software libre abarca aspectos filosóficos más profundos, el software libre se enfoca en las libertades filosóficas que se les otorga a los usuarios, mientras que el software de código abierto se enfoca en las ventajas de su modelo de desarrollo, sus principios son menos ideológicos y más pragmáticos.. Más adelante veremos en detalle las 4 libertades que debe cumplir cualquier software libre y las 10 premisas del software de código abierto.

### Patentes, marcas y derechos de autor

#### Patentes

¿Que es una [**patente](https://es.wikipedia.org/wiki/Patente)? Se trata de un **monopolio temporal sobre una tecnología o producto**, concede la explotación comercial exclusiva a su creador a cambio de la divulgación de su invención, supuestamente garantizan el retorno de la inversión realizada en el desarrollo del producto.

En general existen numerosos argumentos sobre el [**perjuicio que supone el sistema de patentes**](https://es.wikipedia.org/wiki/Patente#Perjuicios_del_sistema_de_patentes), pueden dificultar la libre innovación y el desarrollo tecnológico, es un obstáculo monopolista a la libre competencia o dificulta el acceso a los países empobrecidos. Este es el caso de los medicamentos anti VIH que las farmacéuticas se negaban a rebajar de precio a costa de la muerte de miles de personas en países con pocos recursos.

#### Marcas

**¿Qué es una marca?** Es una denominación, signo o medio material que sirve para distinguir un producto o servicio de otros similares. 

Las marcas se cimientan sobre el imaginario colectivo (los abogados llaman a eso «ficción legal»), lo mismo sucede con religiones, estados o el propio dinero (Fuente: "Sapiens: De animales a dioses" de Yuval Noah Harari - La Leyenda de Peugeot).

**La titularidad de una marca tiene una duración indefinida**, las oficinas de registro de la administración pública en cada país se ocupa del registro de solicitudes.

En España es conocido el caso de Puma, los derechos sobre la marca no los tenía registrados la empresa alemana en España, después de varios litigios para obtener el reconocimiento lo consiguió pero no sin pagar antes 98 millones a la empresa afincada en Elche.

#### Derechos de autor

Los [**derechos de autor**](https://es.wikipedia.org/wiki/Derecho_de_autor) es el reconocimiento público de la autoría de la creación de un bien cultural, la protección de los derechos de autor cae dentro de la legislación sobre propiedad intelectual, que es diferente de la propiedad industrial (patentes y marcas). En muchos casos los autores se agrupan en forma de sociedades para proteger sus derechos de forma colectiva, en España es muy conocida la SGAE. Por desgracia estas sociedades **en la practica estas sociedades han acabado siendo meras gestoras del derecho de copia (copyright)**, representando más a los editores, productores y distribuidores que a los autores. El mercado ha acabado en un modelo de cobro por copia. 

Fuente: "Sobre software libre, Compilación de ensayos sobre software libre" Grupo de Sistemas y Comunicaciones Universidad Rey Juan Carlos.

### Freeware / Shareware

---

## El proyecto GNU

En 1971 Richard Stallman trabajaba en el MIT, en aquel ámbito académico trabajando en un laboratorio era totalmente normal compartir software entre los colegas, a pesar de ello los ordenadores de la época funcionaban con sistemas operativos propietarios, era necesario firmar un acuerdo con la empresa suministradora de no revelar los secretos que escondía.

El auge de **los PC domésticos abre un nicho de mercado y propicia la creación de grandes empresas tecnológicas**, la primera versión 1.0 de MS Windows se remonta al año 1985 se distribuya en 4 discos floopy de 5.15 con una capacidad cada uno de 1200 KiB, se distribuía bajo licencia comercial propietaria y costaba 99 dolares. Su rival Apple hacia lo mismo con su sistema operativo gráfico.

![](img/10.jpg)

Para instalar Windows tenías que tener el número de serie solamente, todo el mundo lo copiaba y se lo pasaba a sus amigos y conocidos pero en teoría esto estaba prohíbido ya que la **licencia se concedía para el uso del sistema operativo a esa persona pero el producto seguía siendo de MS**. Lo mismo pasaba con otro modelos de ordenadores, usar su sistema operativo implicaba no compartirlo con tu vecino, tampoco modificarlo o mejorarlo sin permiso expreso de los titulares, en la práctica esto cierra las posibilidades de comunidades cooperativas de desarrolladores, **la cooperación es lo que hace avanzar las sociedades**, el usuario está indefenso y debe asumir cualquier fallo del software.

![](img/13.png)

Campaña de la FSF para abandonar MS Win ([Fuente](https://www.fsf.org/windows)).

La solución fue obvia para Stallman, **la primera cosa necesaria era un sistema operativo libre**, opto por crear un SO compatible con Unix para facilitar la transición de sus usuarios al nuevo sistema. El nombre que le puso fue GNU, acrónimo recursivo de «GNU’s Not Unix». Para poder desarrollar aplicaciones para el SO desarrolla las primeras herramientas, el compilador GCC y el editor de texto Emacs.

### Las 4 libertades básicas

1. La libertad de ejecutar el programa sea cual sea el propósito.
2. La libertad de modificar el programa para ajustarlo a tus necesidades. (Para que se trate de una libertad efectiva en la práctica,
deberás tener acceso al código fuente, dado que sin él la tarea de incorporar cambios en un programa es extremadamente difícil).
3. La libertad de redistribuir copias, ya sea de forma gratuita, ya sea a cambio del pago de un precio.
4. La libertad de distribuir versiones modificadas del programa, de tal forma que la comunidad pueda aprovechar las mejoras introducidas.

### El Copyleft y la GNU GPL

![](img/14.png)

Para distribuir el software GNU era necesario idear unos nuevos términos de distribución que evitase que se convirtiese en software propietario, el método empleado se denomino [**Copyleft**](https://es.wikipedia.org/wiki/Copyleft).

[**Copyleft**](https://es.wikipedia.org/wiki/Copyleft) utiliza la ley de Copyright, pero dándole la vuelta para servir a un propósito opuesto al habitual: en lugar de privatizar el software, ayuda a preservarlo como software libre.

La idea fundamental es preservar las cuatro libertades de forma duradera, para que sea efectivo cualquier modificación realizada sobre el software original no se licenciar como propietario o restringir alguno de los 4 principios.

Esta forma de asegurar que cualquier producto derivado siga siendo libre ha sido llamado el efecto vírico por su capacidad de propagarse, también se denomina **el efecto [Midas](https://es.wikipedia.org/wiki/Midas)**, Midas era un rey al que se le atribuía el poder de convertir en oro todo lo que tocaba.

![](img/11.jpg)

La licencia GNU GPL es una forma específica de Copyleft (existen otras licencias Copyleft como la BSD o Apache) para código software, también existen otras variaciones para documentación y manuales.

### La FSF (Free Software Foundation)

![](img/12.png)

En 1985 crean la [Free Software Foundation](https://www.fsf.org/es), una organización sin ánimo de lucro dedicada al desarrollo de software libre, para la distribución del código asi como la venta de manuales de software libre.

Los empleados de la FSF han creado y mantienen algunos paquetes de software como la [biblioteca C](https://es.wikipedia.org/wiki/Glibc) indispensable para comunicarse con el kernel de Linux o la shell Bash («Bourne again Shell» en referencia de humor a la «Bourne Shell» de Unix).

### Linux y GNU/Linux

En 1991, Linus Torvalds desarrolló un kernel compatible con Unix y lo llamó Linux. En el año 1992, la combinación de Linux con el incompleto sistema GNU dió como resultado un sistema operativo libre.

Denominamos esta versión GNU/Linux para explicar su composición, una combinación del sistema GNU con Linux como kernel. Previamente se trato de desarrollar otras versiones incompletas del núcleo como Hurd o Alix.

### Hardware secreto

--- 

## Tipos de licencias

### GNU General Public License (GPLv3)

![](img/15.png)

La versión más actual es [v3.0](https://www.gnu.org/licenses/gpl-3.0.html) fue liberada en 2007. Existen traducciones a varios idiomas como [Euskera](https://www.euskadi.eus/contenidos/informacion/software_lizentziak_2018/eu_def/adjuntos/gnu-gpl-v3.eu.html), para el Español no hay traducción.

Como en versiones anteriores busca garantizar a los usuarios finales las 4 libertades fundamentales y se distingue del dominio público o de otras licencias de software libre conocidas como permisivas por hacer hincapié en el copyleft.

#### Algunas novedades

> El software que no es libre (privativo) a menudo es malware (diseñado para maltratar a los usuarios). **El software que no es libre está controlado por quienes lo han desarrollado, lo que los coloca en una posición de poder sobre los usuarios; esa es la injusticia básica**. A menudo los desarrolladores y fabricantes ejercen ese poder en perjuicio de los usuarios a cuyo servicio deberían estar.
> Habitualmente, esto suele realizarse mediante funcionalidades maliciosas. (Fuente: ["Tiranos en el software privativo"](https://www.gnu.org/proprietary/proprietary-tyrants.es.html) - gnu.org)

La [**tivoización**](https://es.wikipedia.org/wiki/Tivoizaci%C3%B3n), es un mecanismo que emplean fabricantes de dispositivos que corren con Software GPL pero con un _hardware_ que impide la ejecución de versiones modificadas de ese software. 

Algunos ejemplos de esta tiranía son las tabletas de MS con Windows RT, por descuido dejarón [abierta una puerta trasera que permitía la posibilidad de instalar otros sistemas operativos](https://www.securitynewspaper.com/2016/07/15/microsoft-silently-kills-dev-backdoor-boots-linux-locked-windows-rt-slabs/), actualmente han subsanado el "error" con una actualización. En la actualidad una amplía gama de dispositivos como Televisores, consolas como la [PlayStation 3](https://www.defectivebydesign.org/sony) o los procesadores [Intel](https://soylentnews.org/article.pl?sid=14/03/15/1912255).

### GNU LGPL v3 (Lesser General Public License) 

La licencia [LGPL](https://es.wikipedia.org/wiki/GNU_Lesser_General_Public_License) fue creada por la FSF, es más permisiva que la original GPL, autoriza el enlace de software propietario con librerías o bibliotecas sin incluir el código dentro de sí (por ejemplo la [biblioteca C de GNU](https://es.wikipedia.org/wiki/Glibc)).

**¿Por qué permitir esta excepción?** No es una cuestión de principios, responde más bien a una estrategia para incentivar la adopción del SO GNU Linux. Se trata de fomentar su uso ya que ofrece tareas genéricas para interactuar con el sistema operativo.

El uso de esta licencia para nuestros productos se debe estudiar caso por caso, si publicamos una librería con la GPL ordinaria no representa una perdida para nosotros, puede ser una ventaja competitiva para los desarrollos de software libre pero puede desincentivar su uso y como consecuencia el uso del sistema GNU, el autentico logro para la comunidad es la creación de aplicaciones de usuario basadas en LGPL.

### AGPL - GNU Affero General Public License

La [GNU Affero General Public License](https://es.wikipedia.org/wiki/GNU_Affero_General_Public_License) está pensada para aplicaciones [**SaaS**](https://es.wikipedia.org/wiki/Software_como_servicio) (Software as a Service).

- Es una modificación de la GPLv3 con una cláusula nueva que añade la obligación de distribuir el software si este se ejecuta para ofrecer servicios a través de una red de ordenadores.  
- Implica que el código de las aplicaciones que corren en la red e interactúan con el usuario también debe estar disponible para esos usuarios.

### Licencias libres permisivas sin Copyleft robusto

#### BSD

#### Apache

#### Mozilla

### Licencias "libres" que no lo son

### Licencias para otros productos

---

## Modelos de negocio basados en software libre

> Los desarrolladores de software propietario cuentan con la ventaja que proporciona el dinero; los de software libre deben idear sus
propias ventajas. Richard Stallman - Software libre para una sociedad libre.

Existe la creencia equivocada de que el software libre está reñido con los negocios y la obtención de beneficio, **la filosofía del software libre rechaza una práctica empresarial concreta y muy generalizada**, pero no está en contra del éxito siempre que la empresa respete las libertades fundamentales de sus usuarios.

La propia FSF obtiene la mayoría de sus beneficios distribuyendo copias de su software (también mediante donativos pero en menor medida), otras vías de ingresos pueden ser la formación o la adaptación de software a otros entornos o la personalización para el cliente.

[Red Hat](https://www.redhat.com/es) (1993) es un ejemplo de empresa de éxito cuyo producto está basado en software libre, ofrecen una distribución del sistema operativo Linux de pago y servicios adicionales como soporte, capacitación o consultoría para el sector empresarial, en paralelo para usuarios particulares han creado la distribución derivada Fedora. Red Hat mantiene y contribuye muchos proyectos de software libre. En 2018, IBM anunció su intención de adquirir Red Hat por 34.000 millones de dolares.  Otras empresas como Red Hat mantienen una dualidad entra la liberación de su producto con versiones comunitarias y licencias comerciales.

Hoy en día existen otras empresas que basan su negocio en **servicios relacionados con el software libre**, por ejemplo ofreciendo soporte técnico y mantenimiento, consultoría o la implantación de aplicaciones empresariales basadas en software libre. A menudo adoptar un software libre empresarial nos libera de los costes de las licencias pero no quiere decir que sea gratis, una implantación eficaz y adecuada suele requerir un socio tecnológico que nos acompañe.

Muchas pequeñas y medianas empresas "satélite" viven de desarrollar funcionalidades extendidas y _plugins_ para productos concretos como WordPress, Odoo, etc.


## Enlaces externos

## Nombres propios
 
- Wikipedia [Aaron Swartz](https://es.wikipedia.org/wiki/Aaron_Swartz).
- Wikipedia [Richard Stallman](https://es.wikipedia.org/wiki/Richard_Stallman).
- Wikipedia [Lawrence Lessig](https://es.wikipedia.org/wiki/Lawrence_Lessig).

- mi granito de arena organizando [https://gitlab.com/soka/charla-soberania-tecnologica](https://gitlab.com/soka/charla-soberania-tecnologica).



