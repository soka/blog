---
title: Primeros pasos para el analista de Cibervigilancia
subtitle: Recursos charla organizada por el CCN-CERT
date: 2021-07-06
draft: false
description: 
tags: ["Ciberseguridad","Cibervigilancia","CNI","CCN-CERT","OSINT"]
---

<!-- vscode-markdown-toc -->
* 1. [Herramientas y recursos](#Herramientasyrecursos)
* 2. [Capturas de la presentación](#Capturasdelapresentacin)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Herramientasyrecursos'></a>Herramientas y recursos

* Vídeo [Por qué me vigilan, si no soy nadie? | Marta Peirano - TED](https://www.ted.com/talks/marta_peirano_the_surveillance_device_you_carry_around_all_day?language=es).
* [El FBI atrapó a un peligroso hacker gracias a foto de su novia](https://www.infobae.com/2012/04/17/642702-el-fbi-atrapo-un-peligroso-hacker-gracias-foto-su-novia/) (2012).
* [Parler hack yields GPS data that tracks users at US Capitol](https://www.dailymail.co.uk/news/article-9142985/GPS-data-Parler-hack-shows-users-app-stormed-Capitol-Building.html).
* [New Research Exposes Iranian Threat Group Operations](https://securityintelligence.com/posts/new-research-exposes-iranian-threat-group-operations/).

* Comprobar si nuestra VPN expone datos, ver nuestra IP en [http://en.utrace.de/](http://en.utrace.de/), con TOR [https://check.torproject.org/](https://check.torproject.org/).
* Herramientas VPN [https://riseup.net/es/vpn](https://riseup.net/es/vpn).
* Enter a URL to test the page load time, analyze it, and find bottlenecks [https://tools.pingdom.com/](https://tools.pingdom.com/).
* [https://unshorten.it/](https://unshorten.it/).
* BrowserLeaks - Web Browser Fingerprinting - Browsing Privacy [https://browserleaks.com/](https://browserleaks.com/).
* Whonix es una distribución basada en Debian GNU/Linux enfocada a la seguridad. Busca proporcionar intimidad, seguridad y anonimato en Internet [https://www.whonix.org/](https://www.whonix.org/).
* [https://victorhck.gitlab.io/privacytools-es/](https://victorhck.gitlab.io/privacytools-es/) [https://gitlab.com/victorhck/privacytools-es](https://gitlab.com/victorhck/privacytools-es).
* [https://prism-break.org/es/all/](https://prism-break.org/es/all/)
* [https://thispersondoesnotexist.com/](https://thispersondoesnotexist.com/) Fotos aleatorias para nuestros perfiles RRSS.
* [https://es.fakenamegenerator.com/](https://es.fakenamegenerator.com/).

* [https://socialbearing.com/](https://socialbearing.com/): Free Twitter analytics & search for tweets, timelines & twitter maps.
* Foller.me Analytics for Twitter [https://foller.me/](https://foller.me/).
* Instagram User Search - SearchUsers.com [https://searchusers.com/](https://searchusers.com/).




##  2. <a name='Capturasdelapresentacin'></a>Capturas de la presentación

![](img/01.png)

![](img/02.png)

![](img/03.png)

![](img/04.png)

![](img/05.png)

![](img/06.png)

![](img/07.png)

![](img/08.png)

![](img/09.png)

![](img/10.png)

![](img/11.png)

![](img/12.png)

![](img/13.png)

![](img/14.png)

![](img/15.png)

![](img/16.png)

![](img/17.png)

![](img/18.png)

![](img/19.png)

![](img/20.png)

![](img/21.png)

![](img/22.png)

![](img/23.png)

![](img/24.png)

![](img/25.png)

![](img/26.png)

![](img/27.png)

![](img/28.png)

![](img/29.png)

![](img/30.png)

![](img/31.png)

![](img/32.png)

![](img/33.png)

![](img/34.png)

![](img/35.png)

![](img/36.png)

![](img/37.png)

![](img/38.png)

![](img/39.png)

![](img/40.png)

![](img/41.png)

![](img/42.png)

![](img/43.png)

![](img/44.png)

![](img/45.png)

![](img/46.png)

![](img/47.png)

![](img/48.png)

![](img/49.png)

![](img/50.png)

![](img/51.png)

![](img/52.png)

##  3. <a name='Enlacesexternos'></a>Enlaces externos
