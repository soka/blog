---
title: Hardening Microsoft Windows 10 version 1709 Workstations
subtitle: ACSC - Australian Ciber Security Centre
date: 2021-07-20
draft: true
description: 
tags: ["Windows","Ciberseguridad","Hardening","ACSC"]
---

<!-- vscode-markdown-toc -->
* 1. [Prioridad alta](#Prioridadalta)
	* 1.1. [Application hardening](#Applicationhardening)
	* 1.2. [Credential entry](#Credentialentry)
* 2. [Recursos](#Recursos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Prioridadalta'></a>Prioridad alta

###  1.1. <a name='Applicationhardening'></a>Application hardening

[AppLocker](https://docs.microsoft.com/es-es/windows/security/threat-protection/windows-defender-application-control/applocker/applocker-overview) se puede usar para gestionar una lista blanca de aplicaciones, la aplicación viene incorporada en las ediciones empresariales de Win, para un sólo equipo se puede usar `secpol.msc`, para un grupo de equipos se puede usar  la Consola de administración de directivas de grupo (GPMC).

###  1.2. <a name='Credentialentry'></a>Credential entry


##  2. <a name='Recursos'></a>Recursos

- [ACSC_Hardening_Win10.pdf](./ACSC_Hardening_Win10.pdf).