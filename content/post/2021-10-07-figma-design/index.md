---
title: Diseño colaborativo con Figma
subtitle: Genera prototipos e interfaces en equipo
date: 2021-10-07
draft: true
description: Genera prototipos e interfaces en equipo
tags: ["Herramientas","Figma","Diseño","Aplicaciones","Web","Figma","Prototipos","Interfaces"]
---

<!-- vscode-markdown-toc -->
* 1. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

[Figma](https://www.figma.com/) es más que una herramienta de generación de prototipos e interfaces basada en Web, aunque su propósito principal sea ese esta aplicación Web permite trabajar de forma colaborativa y darle otros usos como una sesión de _brainstorming_, planificar una serie de tareas para un proyecto, etc. Hay varias plantillas preparadas para que puedas probar y darle diferentes usos.


##  1. <a name='Enlacesexternos'></a>Enlaces externos

