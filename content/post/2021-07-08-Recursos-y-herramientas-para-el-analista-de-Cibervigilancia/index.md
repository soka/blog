---
title: Recursos y herramientas para el analista de Cibervigilancia
subtitle: Recursos charla organizada por el CCN-CERT
date: 2021-07-08
draft: true
description: 
tags: ["Ciberseguridad","Cibervigilancia","CNI","CCN-CERT","OSINT"]
---

<!-- vscode-markdown-toc -->
* 1. [Herramientas y recursos](#Herramientasyrecursos)
* 2. [Capturas de la presentación](#Capturasdelapresentacin)
* 3. [Enlaces internos](#Enlacesinternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Herramientasyrecursos'></a>Herramientas y recursos




##  2. <a name='Capturasdelapresentacin'></a>Capturas de la presentación


##  3. <a name='Enlacesinternos'></a>Enlaces internos

* 1º parte del taller "Primeros pasos para el analista de Cibervigilancia" [https://soka.gitlab.io/blog/post/2021-07-06-primeros-pasos-para-el-analista-de-cibervigilancia/](https://soka.gitlab.io/blog/post/2021-07-06-primeros-pasos-para-el-analista-de-cibervigilancia/).