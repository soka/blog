---
title: Trabajando con repositorios remotos
subtitle: Sesión nº 3 curso Git
date: 2020-11-08
draft: false
description: Control de versiones con Git y GitLab
tags: ["Cursos","Curso Git","Código","Control Versiones","GitLab","Git","SCM"]
---

![](img/00.png)

## Trabajando con repositorios remotos

Para poder colaborar en cualquier proyecto Git, necesitas saber cómo gestionar repositorios remotos. Los repositorios remotos son versiones de tu proyecto que están hospedadas en Internet o en cualquier otra red. Puedes tener varios de ellos, y en cada uno tendrás generalmente permisos de solo lectura o de lectura y escritura. Colaborar con otras personas implica gestionar estos repositorios remotos enviando y trayendo datos de ellos cada vez que necesites compartir tu trabajo. Gestionar repositorios remotos incluye saber cómo añadir un repositorio remoto, eliminar los remotos que ya no son válidos, gestionar varias ramas remotas, definir si deben rastrearse o no y más. En esta sección, trataremos algunas de estas habilidades de gestión de remotos.

## Ver Tus Remotos

Para ver los remotos que tienes configurados, debes ejecutar el comando [`git remote`](https://git-scm.com/docs/git-remote). Mostrará los nombres de cada uno de los remotos que tienes especificados. Si has clonado tu repositorio, deberías ver al menos origin (origen, en inglés) - este es el nombre que por defecto Git le da al servidor del que has clonado.

```bash
$ git clone https://gitlab.com/soka/penascalf52021.git
Clonando en 'penascalf52021'...
remote: Enumerating objects: 54, done.
remote: Counting objects: 100% (54/54), done.
remote: Compressing objects: 100% (34/34), done.
remote: Total 54 (delta 11), reused 50 (delta 10), pack-reused 0
Desempaquetando objetos: 100% (54/54), listo.
$ cd penascalf52021/
$ git remote
origin
```

También puedes pasar la opción -v, la cual muestra las URLs que Git ha asociado al nombre y que serán usadas al leer y escribir en ese remoto:

```bash
$ git remote -v
origin	https://gitlab.com/soka/penascalf52021.git (fetch)
origin	https://gitlab.com/soka/penascalf52021.git (push)
```

## Enviar a tus remotos

Cuando tienes un proyecto que quieres compartir, debes enviarlo a un servidor. El comando para hacerlo es simple: [`git push [nombre-remoto] [nombre-rama]`](https://git-scm.com/docs/git-push). Si quieres enviar tu rama master a tu servidor origin (recuerda, clonar un repositorio establece esos nombres automáticamente), entonces puedes ejecutar el siguiente comando y se enviarán todos los commits que hayas hecho al servidor:

```bash
$ git status
En la rama master
Tu rama está actualizada con 'origin/master'.

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	git/

no hay nada agregado al commit pero hay archivos sin seguimiento presentes (usa "git add" para hacerles seguimiento)
$ git add .
$ git commit -m "apuntes de git introduccion" 
[master 2393192] apuntes de git introduccion
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 git/01-control-de-versiones-con-git.pdf
$ git push
```

## Traer y combinar remotos

Para obtener datos de tus proyectos remotos puedes ejecutar [`git fetch [remote-name]`](https://git-scm.com/docs/git-fetch). El comando irá al proyecto remoto y se traerá todos los datos que aun no tienes de dicho remoto.

```bash
$ git fetch
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 2), reused 0 (delta 0), pack-reused 0
Desempaquetando objetos: 100% (3/3), listo.
Desde https://gitlab.com/soka/penascalf52021
   2393192..b89f528  master     -> origin/master
$ git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
$ git diff remotes/origin/master master
diff --git a/README.md b/README.md
index 0425daa..6ff3bf1 100644
--- a/README.md
+++ b/README.md
@@ -4,6 +4,4 @@ Curso Full Stack Web Developer. Ejercicios básicos Git y VSCode.
 
 Viewing Your Staged and Unstaged Changes
 
-Test: Cambiar y fetch
-
```

Es importante destacar que el comando [`git fetch`](https://git-scm.com/docs/git-fetch) solo trae datos a tu repositorio local - ni lo combina automáticamente con tu trabajo ni modifica el trabajo que llevas hecho. La combinación con tu trabajo debes hacerla manualmente. Puedes usar el comando [`git pull`](https://git-scm.com/docs/git-pull) para traer y combinar automáticamente la rama remota con tu rama actual.

Para fusionar los cambios del repositorio remoto con el directorio de trabajo usamos [`git merge`](https://git-scm.com/docs/git-merge):

```bash
$ git merge
Actualizando 2393192..b89f528
Fast-forward
 README.md | 2 ++
 1 file changed, 2 insertions(+)
```

## Enlaces internos

- [Control de versiones con Git - Sesión nº 1 curso Git](https://soka.gitlab.io/blog/post/2020-11-07-curso-git-1/).
- [Uso básico de Git - Sesión nº 2 curso Git](https://soka.gitlab.io/blog/post/2020-11-07-curso-git-2/)

## Enlaces externos

- [https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos)

## Autor

Iker Landajuela.

## Licencia

[GNU Free Documentation License Version 1.3](https://www.gnu.org/licenses/fdl-1.3-standalone.html).