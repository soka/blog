---
title: Windows Server 2012, primeros pasos y configuración básica
subtitle: Windows Server 2012
date: 2019-11-06
draft: false
description: Recursos Windows Server
tags: ["Windows","Sistemas","Windows Server","Windows Server 2012","PowerShell"]
---

## Como trabajar con el server manager

![](img/01.png)

Por ejemplo en el menu superior derecha podemos deshabilitar que arranque el administrador del servidor al iniciar sesión.

También podemos acceder a herramientas y consolas que tengamos instaladas (visor de eventos, consola PS, copias de seguridad, etc).

Ahora vamos al servidor local (menu lateral) donde se puede ver un resumen de las propiedades, funcionamiento y de su estado. Podemos cambiar la hora del sistema, configurar la dirección IP, habilitar / deshabilitar la administración remota, el RDP o el FW por ejemplo. **En el post anterior he cambiado el nombre de la máquina usando PS pero también se puede hacer desde aquí**.

## Últimos consejos

Asignar una IP estática al servidor.


## Roles de Servidor en Server Core

- Servicios de certificados de Active Directory
- Servicios de dominio de Active Directory
- Servidor DHCP
- Servidor DNS
- Servicios de archivo (incluido Administrador de recursos del servidor de archivos)
- Active Directory Lightweight Directory Services (AD LDS)
- Hyper-V
- Servicios de impresión y documentos
- Servicios de multimedia de transmisión por secuencias
- Servidor web (incluido un subconjunto de ASP.NET)
- Servidor Windows Server Update
- Servidor Active Directory Rights Management
- Servidor de enrutamiento y acceso remoto, incluidos los siguientes subroles:
    - Agente de conexión a Servicios de Escritorio remoto
    - Licencias
    - Virtualización


## Características de Servidor en Server Core

- Microsoft .NET Framework 3.5
- Microsoft .NET Framework 4.5
- Windows PowerShell
- Servicio de transferencia inteligente en segundo plano (BITS)
- Cifrado de unidad BitLocker
- Desbloqueo de BitLocker en red
- BranchCache
- Protocolo de puente del centro de datos
- Almacenamiento mejorado
- Clúster de conmutación por error
- E/S de múltiples rutas
- Equilibrio de carga de red
- Protocolo de resolución de nombres de mismo nivel
- Experiencia de calidad de audio y vídeo de Windows (qWave)
- Compresión diferencial remota
- Servicios simples de TCP/IP
- Proxy RPC sobre HTTP
- Servidor SMTP
- Servicio SNMP
- Cliente Telnet
- Servidor Telnet
- Cliente TFTP
- Windows Internal Database
- Windows PowerShell Web Access
- Servicio de activación de procesos de Windows
- Administración de almacenamiento basada en directivas de Windows
- Extensión WinRM de IIS
- Servidor WINS
- Compatibilidad con WoW64

## Enlaces
