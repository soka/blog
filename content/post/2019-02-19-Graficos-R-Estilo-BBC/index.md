---
title: Recetas para hacer diagramas en R como lo hace la BBC
subtitle: Diagramas con paquete bbplot
date: 2019-02-19
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Recursos","R - Diagramas","Diagramas"]
---

El equipo de la [BBC](https://www.bbc.com/news) (diario de noticias de Reino Unido) que se ocupa de componer visualizaciones de datos ha desarrollado un paquete en **R** ([post del anuncio](https://medium.com/bbc-visual-and-data-journalism/how-the-bbc-visual-and-data-journalism-team-works-with-graphics-in-r-ed0b35693535)) y han publicado una serie de ["recetas"](https://bbc.github.io/rcookbook/) para realizar diagramas con resultados como los de la imagen inferior.

![](https://cdn-images-1.medium.com/max/1600/0*DDRU1L5LIJkgDBxd.png)

El paquete pretende facilitar la generación de diagramas con un estilo uniforme como los que usan en la BBC, además sin depender de otros desarrollos (les ofrece mayor flexibilidad para cambiar diseño o automatizar la generación de diagramas), [**bblot**](https://github.com/bbc/bbplot) está basado en **ggplot2** y se puede descargar de GitHub (no está disponible en CRAN para instalar como es habitual).

Estoy probando el paquete y guardando el código en mi repositorio público de GitLab dedicado a **R**, puedes acceder al script en esta ruta: r / recursos / graficos-estilo-bbc / [graficos-estilo-bbc.R](https://gitlab.com/soka/r/blob/f303892b568ec4210bfae40c79c6888efbd95e6c/recursos/graficos-estilo-bbc/graficos-estilo-bbc.R).

# Instalar librerías necesarias

Antes de comenzar debemos instalar una serie de paquetes, usando la función `p_load` del paquete **pacman** podemos cargar todos de golpe sin necesidad de instalarlos uno a uno. 



# Enlaces

* ["BBC Visual and Data Journalism cookbook for R graphics"](https://bbc.github.io/rcookbook/).
* ["Create data visualizations like BBC News with the BBC's R Cookbook"](https://blog.revolutionanalytics.com/2019/02/bbc-r-cookbook.html).
* ["How the BBC Visual and Data Journalism team works with graphics in R"](https://medium.com/bbc-visual-and-data-journalism/how-the-bbc-visual-and-data-journalism-team-works-with-graphics-in-r-ed0b35693535).
* ["Introduction to pacman"](https://cran.r-project.org/web/packages/pacman/vignettes/Introduction_to_pacman.html): The pacman package is an R package management tool that combines the functionality of base library related functions into intuitively named functions.
