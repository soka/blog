---
title: Slack - Comunicación colaborativa para el trabajo en equipo
subtitle: Contenido del curso organizado por la SPRI
date: 2021-04-01
draft: false
description: 
tags: ["Aplicaciones","Slack","Mensajería","Colaboración","Trabajo colaborativo","Cursos","SPRI"]
---

<!-- vscode-markdown-toc -->
* 1. [Objetivos](#Objetivos)
* 2. [Programa](#Programa)
* 3. [Presentación](#Presentacin)
* 4. [Manual Slack](#ManualSlack)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Objetivos'></a>Objetivos

El curso pretende ser un taller guiado para que las empresas conozcan qué les puede reportar Slack antes de su implantación en la empresa.

En este curso conoceremos cómo centralizar y organizar los distintos hilos de comunicación que existan en los diferentes departamentos y proyectos de una empresa.

Una vez que se unan al espacio de trabajo creado, tu equipo podrá navegar y unirse a canales, enviar mensajes directos, compartir archivos e integrarse con otros gestores de tareas y muchas cosas más.
Además mostraremos trucos y buenas prácticas para aprovechar todo el potencial de la herramienta.

##  2. <a name='Programa'></a>Programa

* Introducción a Slack: aplicaciones web, ordenador y móvil
* Funcionalidades del plan gratuito de Slack
* Cómo crear un entorno colaborativo de comunición para tu equipo
* Creación del espacio de trabajo: Roles. Invitar a miembros
* Creación de canales abiertos o cerrados
* Envío de mensajes
* Los mensajes directos
* Tu canal de mensaje directo personal
* Anuncios, notas y notificaciones
* Cómo adjuntar archivos
* Búsquedas en el repositorio
* Slack Calls: llamadas, videoconferencia, compartir pantallas
* Principales integraciones de aplicaciones con Slack
* Slackbot: respuestas automáticas 

##  3. <a name='Presentacin'></a>Presentación

[Presentacion_Slack.pdf](./Presentacion_Slack.pdf)

![](img/presentacion/Presentacion_Slack-01.png)

![](img/presentacion/Presentacion_Slack-02.png)

![](img/presentacion/Presentacion_Slack-03.png)

![](img/presentacion/Presentacion_Slack-04.png)

![](img/presentacion/Presentacion_Slack-05.png)

![](img/presentacion/Presentacion_Slack-06.png)

![](img/presentacion/Presentacion_Slack-07.png)

![](img/presentacion/Presentacion_Slack-08.png)

![](img/presentacion/Presentacion_Slack-09.png)

![](img/presentacion/Presentacion_Slack-10.png)

![](img/presentacion/Presentacion_Slack-11.png)

![](img/presentacion/Presentacion_Slack-12.png)

![](img/presentacion/Presentacion_Slack-13.png)

![](img/presentacion/Presentacion_Slack-14.png)

![](img/presentacion/Presentacion_Slack-15.png)

![](img/presentacion/Presentacion_Slack-16.png)

![](img/presentacion/Presentacion_Slack-17.png)

![](img/presentacion/Presentacion_Slack-18.png)

![](img/presentacion/Presentacion_Slack-19.png)

![](img/presentacion/Presentacion_Slack-20.png)

![](img/presentacion/Presentacion_Slack-21.png)

![](img/presentacion/Presentacion_Slack-22.png)

![](img/presentacion/Presentacion_Slack-23.png)

![](img/presentacion/Presentacion_Slack-24.png)

![](img/presentacion/Presentacion_Slack-25.png)

![](img/presentacion/Presentacion_Slack-26.png)

![](img/presentacion/Presentacion_Slack-27.png)

![](img/presentacion/Presentacion_Slack-28.png)

![](img/presentacion/Presentacion_Slack-29.png)

![](img/presentacion/Presentacion_Slack-30.png)

![](img/presentacion/Presentacion_Slack-31.png)

![](img/presentacion/Presentacion_Slack-32.png)

![](img/presentacion/Presentacion_Slack-33.png)

![](img/presentacion/Presentacion_Slack-34.png)

![](img/presentacion/Presentacion_Slack-35.png)

![](img/presentacion/Presentacion_Slack-36.png)

![](img/presentacion/Presentacion_Slack-37.png)

![](img/presentacion/Presentacion_Slack-38.png)

![](img/presentacion/Presentacion_Slack-39.png)

![](img/presentacion/Presentacion_Slack-40.png)

![](img/presentacion/Presentacion_Slack-41.png)

![](img/presentacion/Presentacion_Slack-42.png)

![](img/presentacion/Presentacion_Slack-43.png)

![](img/presentacion/Presentacion_Slack-44.png)

![](img/presentacion/Presentacion_Slack-45.png)

![](img/presentacion/Presentacion_Slack-46.png)

![](img/presentacion/Presentacion_Slack-47.png)

![](img/presentacion/Presentacion_Slack-48.png)

![](img/presentacion/Presentacion_Slack-49.png)

![](img/presentacion/Presentacion_Slack-50.png)

##  4. <a name='ManualSlack'></a>Manual Slack

[SPRI_Manual_Slack.pdf](./SPRI_Manual_Slack.pdf)

![](img/manual/SPRI_Manual_Slack-01.png)

![](img/manual/SPRI_Manual_Slack-02.png)

![](img/manual/SPRI_Manual_Slack-03.png)

![](img/manual/SPRI_Manual_Slack-04.png)

![](img/manual/SPRI_Manual_Slack-05.png)

![](img/manual/SPRI_Manual_Slack-06.png)

![](img/manual/SPRI_Manual_Slack-07.png)

![](img/manual/SPRI_Manual_Slack-08.png)

![](img/manual/SPRI_Manual_Slack-09.png)

![](img/manual/SPRI_Manual_Slack-10.png)

![](img/manual/SPRI_Manual_Slack-11.png)

![](img/manual/SPRI_Manual_Slack-12.png)

![](img/manual/SPRI_Manual_Slack-13.png)

![](img/manual/SPRI_Manual_Slack-14.png)

![](img/manual/SPRI_Manual_Slack-15.png)

![](img/manual/SPRI_Manual_Slack-16.png)

![](img/manual/SPRI_Manual_Slack-17.png)

![](img/manual/SPRI_Manual_Slack-18.png)

![](img/manual/SPRI_Manual_Slack-19.png)

![](img/manual/SPRI_Manual_Slack-20.png)

![](img/manual/SPRI_Manual_Slack-21.png)

![](img/manual/SPRI_Manual_Slack-22.png)

![](img/manual/SPRI_Manual_Slack-23.png)

![](img/manual/SPRI_Manual_Slack-24.png)

![](img/manual/SPRI_Manual_Slack-25.png)

![](img/manual/SPRI_Manual_Slack-26.png)

![](img/manual/SPRI_Manual_Slack-27.png)

![](img/manual/SPRI_Manual_Slack-28.png)

![](img/manual/SPRI_Manual_Slack-29.png)

![](img/manual/SPRI_Manual_Slack-30.png)

![](img/manual/SPRI_Manual_Slack-31.png)

![](img/manual/SPRI_Manual_Slack-32.png)

![](img/manual/SPRI_Manual_Slack-33.png)

![](img/manual/SPRI_Manual_Slack-34.png)

![](img/manual/SPRI_Manual_Slack-35.png)

![](img/manual/SPRI_Manual_Slack-36.png)

![](img/manual/SPRI_Manual_Slack-37.png)

![](img/manual/SPRI_Manual_Slack-38.png)

![](img/manual/SPRI_Manual_Slack-39.png)

![](img/manual/SPRI_Manual_Slack-40.png)

![](img/manual/SPRI_Manual_Slack-41.png)

![](img/manual/SPRI_Manual_Slack-42.png)

![](img/manual/SPRI_Manual_Slack-43.png)

![](img/manual/SPRI_Manual_Slack-44.png)

![](img/manual/SPRI_Manual_Slack-45.png)

![](img/manual/SPRI_Manual_Slack-46.png)

![](img/manual/SPRI_Manual_Slack-47.png)

![](img/manual/SPRI_Manual_Slack-48.png)

![](img/manual/SPRI_Manual_Slack-49.png)

![](img/manual/SPRI_Manual_Slack-50.png)

![](img/manual/SPRI_Manual_Slack-51.png)

![](img/manual/SPRI_Manual_Slack-52.png)