---
title: Introducción a Linux Mint [borrador]
subtitle: Montando un entorno de desarrollo Web
date: 2020-10-24
draft: false
description: Primeros pasos configurando herramientas de desarrollo
tags: ["Linux","Mint","Web","Desarrollo","Programación","Developer","Wordpress","LAMP"]
---

Me he creado una máquina virtual con una ISO de Linux Mint 20 (XFCE) en Debian con Virtual Machine Manager (QEmu en KVM).

Para ejecutar el administrador de máquinas virtuales:

```bash
# virt-manager
```

## LAMP

Antes de comenzar actualizo el sistema (a mi me gusta logearme como root con `sudo su`):

```bash
sudo apt-get update
sudo apt-get upgrade
```

Ahora instalo la pila LAMP con un comando:

```bash
sudo apt-get install lamp-server^
```

Fuente: [https://prognotes.net/2019/06/lamp-stack-linux-mint-installation/](https://prognotes.net/2019/06/lamp-stack-linux-mint-installation/)

Abro [http://localhost](http://localhost) y compruebo que el servidor Web Apache de ejecuta bien.

Ahora hago un pequeño script en PHP para comprobar su funcionamiento:

phpinfo.php:

```php
<?
phpinfo();
?>
```

Y lo guardo en /var/wwww/html, yo he tenido que modificar los permisos de la ruta para poder guardar el archivo `sudo chmod o+w /var/www/html`.

Compruebo que accedo a MySQL con `sudo mysql -u root`, a continuación puedo probar un comando `mysql> show databases;`.

### MySQL

Fuentes:

- [https://dev.mysql.com/doc/refman/8.0/en/mysql.html](https://dev.mysql.com/doc/refman/8.0/en/mysql.html)
- [https://www.mysqltutorial.org/mysql-cheat-sheet.aspx](https://www.mysqltutorial.org/mysql-cheat-sheet.aspx)

## Visual Studio Code como editor con Snap

Instalación Snap:

```bash
sudo rm /etc/apt/preferences.d/nosnap.pref
sudo apt update
sudo apt install snapd
```

Ahora instalo VS Code:

```bash
sudo snap install code --classic
```

Ejecuto el editor:

```bash
snap run code
```

Fuente: [https://snapcraft.io/install/code/mint](https://snapcraft.io/install/code/mint)

## Git

```
sudo apt install git
git --version
```

## phpMyAdmin

```bash
sudo apt install phpmyadmin php-mbstring php-gettext
```

**El paquete php-gettext no se encuentra**, por el momento no lo instalo.

Selecciono Apache:

![](img/01.png)

Usaré [dbconfig-common](https://www.debian.org/doc/manuals/dbconfig-common/) para configurar la BD:

![](img/02.png)

![](img/03.png)

```
sudo phpenmod mbstring
```

Edita "sudo vim /etc/apache2/apache2.conf" y añade al final "include /etc/phpmyadmin/apache.conf".

```
sudo /etc/init.d/apache2 restart
```

Ahora ya debería ser accesible en [http://127.0.0.1/phpmyadmin/](http://127.0.0.1/phpmyadmin/), el usuario es "phpmyadmin" y la clave la que hayamos definido durante la instalación.

![](img/04.png)

Si sale una advertencia que indica que no tienes privilegios suficientes abre una ventana de privada nueva o borra la chache en Firefox y vuelve a entrar.

![](img/05.png)

Si no funciona usa para acceder el usuario y contraseña de mantenimiento en /ect/mysql/debian.cnf.

Fuentes:

- [https://io.bikegremlin.com/12127/lamp-stack-mint/#2.2](https://io.bikegremlin.com/12127/lamp-stack-mint/#2.2).

## Wordpress

```bash
wget -c http://wordpress.org/latest.tar.gz
tar -xzvf latest.tar.gz
sudo rsync -av wordpress/* /var/www/html/

sudo chown -R www-data:www-data /var/www/html/
sudo chmod -R 755 /var/www/html/
```

Crear la BD para WP, si te dan la lata estos comandos se puede hacer lo mismo desde PHPMyAdmin.

```bash
sudo mysql -u root
create database wordpressdatabase;
create user 'wpadmin'@'localhost' identified by 'contraseña';
grant all privileges on `wordpressdatabase`.* to 'wpadmin'@'localhost';
```

```bash
/var/wwww/html/$ sudo cp wp-config-sample.php wp-config.php
/var/wwww/html/$ sudo vim wp-config.php
```

```bash
sudo /etc/init.d/apache2 restart
sudo /etc/init.d/mysql restart
```

Abrimos [http://127.0.0.1/index.php](http://127.0.0.1/index.php) y comenzamos instalación WP.

![](img/06.png)

Contraseña: Xb5D4LaVzZfBrBx


Fuentes:

- [https://idroot.us/install-wordpress-linux-mint-19/](https://idroot.us/install-wordpress-linux-mint-19/)
- [https://techviewleo.com/how-to-install-wordpress-on-linux-mint/](https://techviewleo.com/how-to-install-wordpress-on-linux-mint/)