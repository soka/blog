---
title: PeerTube, la plataforma P2P, open source y descentralizada como alternativa a Youtube [borrador]
subtitle: Aplicaciones mensajería, reuniones, videoconferencia, documentación...
date: 2020-04-26
draft: false
description: 
tags: ["Aplicaciones","PeerTube","Web","Vídeo","P2P","Streaming","Open Source","Difusión contenidos"]
---

Le dedico este post a Maider por recordarme que hace tiempo que quería investigar [**PeerTube**](https://joinpeertube.org/), tal vez así se anime a subir sus _perfomances_ en vídeo a esta plataforma.

![](img/PeerTube-logo.png)

[**PeerTube**](https://joinpeertube.org/) es una plataforma de código libre descentralizada que permite la difusión de contenidos de vídeo, se postula como **alternativa a otras plataformas de difusión de contenidos de vídeo (_video broadcasting services_) como Youtube o Vimeo**.

![](img/05.png)

Como comentan en el vídeo introductorio está herramienta pretende empoderarnos para que seamos algo más que un meros usuarios, incluso **puedes instalar tu propia instancia de [PeerTube](https://joinpeertube.org/) en tu servidor**.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://framatube.org/videos/embed/9c9de5e8-0a1e-484a-b099-e80766180a6d?subtitle=es" frameborder="0" allowfullscreen></iframe>

[**PeerTube**](https://joinpeertube.org/) funciona como una **red federada de nodos**, es decir instancias instaladas en servidores diferentes pueden decidir "aliarse" y sincronizar sus vídeos de forma voluntaria, los nodos federados crean un catálogo compartido sin necesidad de almacenamiento adicional.

En la sección About se pueden ver seguidores de la instancia y a quien sigue:

![](img/06.png)

Una de las características que lo hace especial es un **sistema de retransmisión ([_streaming_](https://es.wikipedia.org/wiki/Streaming)) [P2P](https://es.wikipedia.org/wiki/Peer-to-peer) (punto a punto o _Peer-to-peer_ en inglés, la misma tecnología que los _torrent_)** usando [WebTorrent](https://github.com/webtorrent/webtorrent) o [p2p-media-loader](https://github.com/novage/p2p-media-loader).

Cuando varias personas están viendo el mismo vídeo simultáneamente la información también se comparte entre ellas de fondo. La transmisión P2P es más resiliente y eficiente, un vídeo muy reproducido por muchos usuarios no hace caer la instancia del servidor, esto permite a PeerTube "competir" con grandes empresas que pueden mantener miles de servidores diseminados por todo el planeta que pueden soportar el tráfico de datos centralizado basado en el modelo clásico cliente-servidor.

![](img/03.png)

PeerTube es software de [código abierto](https://es.wikipedia.org/wiki/C%C3%B3digo_abierto) mantenido por la comunidad de desarrolladores y colaborares.

![](img/04.png)

## Subir tus vídeos

PeerTube cuenta con diversas [instancias o alojamientos](https://instances.joinpeertube.org/instances) para tus videos; cada uno tiene sus propios términos de servicio, política de moderación y límite de espacio en disco duro por usuario, por ejemplo.

Instancias de demostración:

- [peertube.cpy.re](https://peertube.cpy.re/).
- [peertube2.cpy.re](https://peertube2.cpy.re/).
- [peertube3.cpy.re](https://peertube3.cpy.re/).

Quienes solo quieran ver videos en PeerTube, sin registrarse, lo pueden hacer.  Para subir un clip es necesario crear una cuenta en la instancia o página web, para lo cual se requiere de un correo electrónico, una contraseña y estar de acuerdo con los términos de la misma.

En cuanto al contenido ilegal, el servicio permite reportar videos problemáticos, “y cada administrador debe aplicar su moderación de acuerdo con sus términos y condiciones, y a la ley (local)”.

## Registro de prueba

No me ha sido fácil encontrar una instancia en el estado Español y que me permitiese registrarme, he encontrado esta [https://video.hardlimit.com/](https://video.hardlimit.com/), parece un sitio dedicado a la informática. 

## Conclusión

El problema es encontrar determinado contenido de tu interés, si esperas encontrar vídeos de cualquier cosa busques lo que busques como en Youtube olvídate, al tratarse de instancias independientes cada una alberga sus vídeos y muchos nodos son temáticos, en los resultados de búsqueda sólo se muestran los vídeos de la instancia y de otras instancias a las que sigue.

El proyecto es promovido por la organización sin fines de lucro [Framasoft](https://framasoft.org/en/). En este sentido, el servicio no está ligado a una gran firma tecnológica ni depende de los anuncios, como YouTube. Si quieres apoyar el proyecto puedes [hacer una donación](https://soutenir.framasoft.org/en/) o si eres desarrollador siempre puedes contribuir con tu ayuda.

PeerTube continúa evolucionando y su objetivo no es reemplazar a Youtube u otras plataformas. “La ambición sigue siendo ser una alternativa libre y descentralizada: el objetivo de una alternativa no es reemplazar, sino proponer algo más, **con valores diferentes**, en paralelo a lo que ya existe”, enfatiza el servicio.

Instalar una instancia propia puede ser interesante en algunos casos, para asociaciones, colectivos o por ejemplo centros de enseñanza-divulgación que quieran publicar sus vídeos bajo sus condiciones y tenerlos almacenados en sus servidores.

## Enlaces

- [**PeerTube**](https://joinpeertube.org/): Web principal.
- [Chocobozzz / PeerTube](https://github.com/Chocobozzz/PeerTube): Código fuente del proyecto alojado en GitHub.
- framatube.org Vídeo [What is PeerTube?](https://framatube.org/videos/embed/9c9de5e8-0a1e-484a-b099-e80766180a6d).
- docs.joinpeertube.org [documentación oficial](https://docs.joinpeertube.org/#/).
- Chat en Matrix puenteado con IRC y Discord: [#peertube:matrix.org](https://matrix.to/#/#peertube:matrix.org).
- [Framasoft](https://framasoft.org/en/): Web principal en inglés.
- [PeerTube instances](https://instances.joinpeertube.org/instances).
- es.wikipedia.org [Streaming](https://es.wikipedia.org/wiki/Streaming).
- es.wikipedia.org [Peer-to-peer](https://es.wikipedia.org/wiki/Peer-to-peer).
- es.wikipedia.org [Código abierto](https://es.wikipedia.org/wiki/C%C3%B3digo_abierto).
- genbeta.com [PeerTube, la plataforma P2P, open source y descentralizada que quiere ser una alternativa a YouTube](https://www.genbeta.com/multimedia/peertube-la-plataforma-p2p-open-source-y-descentralizada-que-quiere-ser-una-alternativa-a-youtube)
- es.digitaltrends.com [¿Sabes qué es PeerTube y cómo funciona? Te lo explicamos](https://es.digitaltrends.com/sociales/que-es-peertube/).