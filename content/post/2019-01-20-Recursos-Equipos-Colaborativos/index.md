---
title: Recursos para gestionar equipos de trabajo
subtitle: Aplicaciones y herramientas
date: 2019-01-20
draft: true
description: Trello y GSuite
tags: ["Aplicaciones","Trello"]
---

# Introducción

Actualmente trabajamos con un ecosistema de herramientas para gestionar la carga de trabajo de los proyectos y asistencia técnica a los usuarios (proyecto sistema ticketing KooperaSAT). Básicamante son dos herramientas:

* [**Trello**](https://trello.com) como **herramienta de gestión de proyectos**. 
* Un **repositorio para almacenar la documentación y datos** (código fuente y otro software por ejemplo) basado en [**GSuite**](https://es.wikipedia.org/wiki/G_Suite) (servicios de Google para empresas). 

Para comunicarnos de forma ágil usamos el **sistema de mensajería [Telegram](https://es.wikipedia.org/wiki/Telegram_Messenger)**.

# Gestión de proyectos y metodologías

[**Trello**](https://trello.com) es nuestro sistema predilecto **para la gestión de proyectos** y solicitudes de asistencia técnica SAT (también de otro tipo general como compras o valoraciones), también para trabajar en aquellos proyectos en los que estamos involucrados.

Aplicamos de forma libre o flexible metodologías como [Kanban](https://es.wikipedia.org/wiki/Kanban_(desarrollo)) y [Scrum](https://es.wikipedia.org/wiki/Scrum_(desarrollo_de_software)) ideados especialmente para equipos de desarrollo software y equipos [ágiles](https://es.wikipedia.org/wiki/Desarrollo_%C3%A1gil_de_software). 

Algunos de los **valores que guían estas metodologías** y que nosotros adoptamos como nuestros (en [**Trello**](https://trello.com)), son por ejemplo:

* Visualización y transparencia: Visualizar el flujo de trabajo (diseño procesos internos con políticas explicitas), todos los componentes del equipo pueden ver las tareas y en que están trabajando el resto en todo momento.
* Comunicación y colaboración: [**Trello**](https://trello.com) facilita mediante menciones ('@' como en Twitter) desarrollar contenidos e ideas de forma colaborativa en torno a una tarea concreta generando un poso de conocimiento accesible para todo el equipo en el futuro. Hemos desterrado el correo electrónico para comunicaciones internas por tratarse de una herramienta que está muy lejos del lenguaje natural.
* Fomentar el liderazgo en áreas internas y proyectos. 

Tenemos una orientación clara por la **gestión basada en proyectos** formados por equipos multidisplinares con objetibos claros y medibles.

En [**Trello**](https://trello.com) tenemos cuentas gratuitas (no es muy aconsejable) registradas para todo el equipo y llevamos funcionando con este sistema dos años aproximadamente, **para implementar el sistema de ticketing KooperaSAT hemos realizado algunos desarrollos internos** como los formularios Web de solicitud SAT y valoración posterior, también un CM (cuadro de mando) con estadísticas del sistema SAT en tiempo real.

Como criterio para adoptar unas herramientas u otras consideramos imprescindible que cualquier aplicación software que usemos deba tener comportamiento social (menciones, conversaciones, etc) y que implemente aplicación nativa para smartphones Android para facilitar el trabajo y la movilidad. 

# Mejoras 

Otras mejoras que hemos acometido estos últimos años respecto al sistema anterior de acciones basadas en el ERP Kooperia:

* Movilidad, usando la App nativa de Trello para Android podemos trabajar en nuestras tareas y colaborar practicamente desde cualquier sitio con un smartphone. Kooperia está lastrado por su propia tecnologia obsoleta y es imposible acceder desde fuera, antaño cuando usabamos las acciones de Kooperia los trabajadores eran muy reticientes a usarlas y en ocasiones ni siquiera podían (es imposible correr nuestro ERP en un smartphone). 
* Reducir el número de canales de entrada y "ruido": Era práctica común que la gente nos reportase sus necesidades de viva voz por ejemplo, después de un tiempo hemos conseguido que el usuario 
* Trazabilidad e indicadores:
* **Teléfono SAT rotativo** dentro del área de sistemas.
* **Valoración o cierre explicito de la acción por parte del solicitante**: Hemos conseguido que en la mayoria de los casos el trabajador colabore en la resolución de su necesidad, para nosotros es de suma importancia obtener el visto bueno para dar por finalizada una acción.
* **Gestión del tiempo**: 	


# Un día cualquiera

Reuniones (Scrum Daily) con el área de sistemas casi diarias o cada 2/3 días.


# Conclusiones

Nuestros aplicaciones no están "esclavas" en nuestras instalaciones sino que
