---
title: Lunes 4 de Marzo - Taller sonido binaural  
subtitle: Iniciación para gente aficionada o curiosa 
date: 2019-02-25
draft: false
description: Impartido Amaia Vicente y Sergio Robles
tags: ["Talleres","Sonido","Arte","Binaural","Tecnología","Holofónico"]
---

El **Lunes 04 de Marzo a las 19:00 [Amaia Vicente](https://amaiavicente.com/author/amaiavicente/) y Sergio Robles imparten un taller donde darán a conocer que es el sonido binaural**, la entrada es libre y gratuita y el taller no tiene requisitos previos, cualquier persona curiosa que quiera iniciarse en está técnica sin conocimientos previos será bienvenida. El taller se impartirá en el centro autogestionado de la Kultur Etxea, C/ Urazurrutia 7 del barrio de Bilbao La Vieja ([ver mapa](https://goo.gl/maps/nPSY65DZsnx)).   

![](images/KULTURETXEA_txiki.jpg)

El sonido binaural es una técnica de grabación de sonidos ambiente mediante el uso de micrófonos alojados en una cabeza artificial, se intenta captar el sonido como si el oyente estuviese fisicamente en el espacio donde se graban, algo así como **un sonido 3D envolvente**. Las frecuencias captadas por cada micrófono es ligeramente diferente lo que hace que el cerebro cree la ilusión espacial al intentar fusionar o sincronizar ambas frecuencias.

El origen de esta técnica se remonta a los años 70 y suele atribuirse a [Hugo Zuccarelli](https://es.wikipedia.org/wiki/Hugo_Zuccarelli), la idea es conseguir la misma "tridimensionalidad" que se tiene en imagen con la holografía. El ingeniero usó un sistema de grabación que utilizaba como base una cabeza con micrófonos que mencionabamos al principio de este post. Antes de caer en el olvido relegado por el sonido Dolby Surround Zuccarelli sí tuvo la oportunidad de grabar con Pink Floyd el disco 'The Final Cut', donde se utilizó esta técnica holofónica.

![](https://amaiavicente.files.wordpress.com/2018/12/captura-de-pantalla-2018-11-26-a-las-19-39-32.png?w=700&h=&crop=1)

[[Fuente](https://amaiavicente.com/)]

Sobre el trabajo de **Amaia Vicente** podeis consultar los siguientes vídeos en MP4 producidos durante su estancia en Wroclaw (Polonia) en este [enlace](https://drive.google.com/drive/folders/1awQbKBvK7oQULa-KlfiSvNy60zMxTwK_?usp=sharing) y está [muestra de sonido en MP3](https://drive.google.com/open?id=1g6gce6VkUZ5EN9O4lSDQ15Jyng5tHXKj) (se recomienda usar auriculares).

# Enlaces y recursos

* [**Evento en Facebook**](https://www.facebook.com/events/1302312709908284/).
* [https://amaiavicente.com/author/amaiavicente/](https://amaiavicente.com/author/amaiavicente/).
* Correo electrónico de contacto para consultas: ikastaroak.kulturetxea@gmail.com o la dirección principal kulturtxeabizirik@gmail.com.
* Video ["AUDIO 3D - Hugo Zuccarelli"](https://www.youtube.com/watch?v=_UeFsXY03V4&t=169s).


