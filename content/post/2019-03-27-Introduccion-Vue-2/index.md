---
title: Vue.js - Introducción
subtitle: Condicionales v-if y v-show
date: 2019-03-27
draft: false
description: The Progressive JavaScript Framework
tags: ["Vue", "JS", "JavaScript", "Programación", "Vue.js"]
---

![](images/logo.png)

Para no hacer posts muy largos seguiré aquí con una introducción básica a vue.js, el primer post se puede encontrar en este [enlace](https://soka.gitlab.io/blog/post/2019-03-18-introduccion-vue/).

El primero fue una introducción muy básica donde se destacaban algunas de las características de **Vue.js**, me quede a las puertas de como implementar la lógica básica de una aplicación con **sentencias condicionales y bucles**.

Estoy pensando en usar [JSFiddle](https://jsfiddle.net/), [CodePen](https://codepen.io/) o algo parecido para los ejemplos pero por el momento están en [mi cuenta de GitLab](https://gitlab.com/soka) en un repositorio público en este [enlace](https://gitlab.com/soka/vuejs/tree/master).

# Condicionales v-if

Las directivas son atributos HTML que dirigen el comportamiento de la aplicación, por ejemplo para una condicional usamos `v-if`.

```JS
<div id="app-1">
  <span v-if="seen">
    Condicional if
  </span>
</div>
```

```JS
var app1 = new Vue({
  el: "#app-1",
  data: {
    seen: true
  }
});
```

Usando Vue.js Devtools ([enlace](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd) a extensión de Chrome) modificamos la variable `app1.seen = false` e inmediatamente el texto "Condicional if" desaparece.

![](images/vue_devtools_01.PNG)

El ejemplo ilustra que podemos hacer data binding no solo a un texto a un atributo, también a una estructura del DOM como el bloque `span`.

# v-else

Como en cualquier otro lenguaje de programación podemos extender la sentencia condicional para contemplar los casos que no cumplen la condición lógica con `v-else`.

```JS
<div id="app-2">
  <h1 v-if="ok">Yes</h1>
  <h1 v-else>No</h1>
</div>
```

```JS
var app2 = new Vue({
  el: "#app-2",
  data: {
    ok: true
  }
});
```

# v-if para más de un elemento

`v-if` es una directiva y debe asociarse a una etiqueta HTML determinada, si queremos que la condición afecte a más de un elemento usamos `v-if` como atributo de un tag `<template>`, este elemento es "invisible" cuando se renderiza.

```JS
    <div id="app-3">
      <template v-if="ok">
        <h1>Título</h1>
        <p>parrafo 1</p>
        <p>parrafo 2</p>
      </template>
    </div>
```

```JS
      var app3 = new Vue({
        el: "#app-3",
        data: {
          ok: true
        }
      });
```

# Encadenar varias condiciones con v-else-if

En el siguiente ejemplo vamos a encadenar varias condiciones concatenadas con `v-else-if` y condicionadas al valor de una variable:

```JS
    <div id="app-4">
      <p v-if="dato > 6">Mayor que 6</p>
      <p v-else-if="dato < 6">Menor que 6</p>
      <p v-else>No</p>
    </div>
```

```JS
      var app4 = new Vue({
        el: "#app-4",
        data: {
          dato: 12
        }
      });
```

# Ejercicios if

- vuejs / src / intro / 04-if-conditional / [index.html](https://gitlab.com/soka/vuejs/blob/master/src/intro/04-if-conditional/index.html).

# Visibilidad de un elemento con v-show

Se puede hacer lo mismo y condicionar la visibilidad de un elemento con `v-show`. La diferencia es que un elemento con v-show siempre será renderizado y permanecerá en el DOM. v-show simplemente alterna el valor de la propiedad CSS display del elemento.

```JS
    <div id="app-1">
      <h1 v-show="ok">Hello!</h1>
    </div>
```

```JS
      var app1 = new Vue({
        el: "#app-1",
        data: {
          ok: true
        }
      });
```

**Ejercicio**: vuejs / src / intro / 05-v-show / [index.html](https://gitlab.com/soka/vuejs/blob/master/src/intro/05-v-show/index.html).

# Enlaces internos

- ["Vue.js - The Progressive JavaScript Framework. Introducción"](https://soka.gitlab.io/blog/post/2019-03-18-introduccion-vue/): Primer post.
- ["Recarga los cambios de tu página HTML/JS/CSS de forma automática"](https://soka.gitlab.io/blog/post/2019-03-17-npm-live-server/): Una utilidad para probar los ejemplos.

# Enlaces externos

- Guía oficial Vue.js con una introducción muy recomendable para empezar [enlace](https://vuejs.org/v2/guide/index.html).
- ["Aprende Vue2 y Firebase paso a paso"](https://wmedia.teachable.com/courses/enrolled/140226): **Curso gratuito muy recomendable** para empezar.

**v-if**:

- ["Renderizado condicional — Vue.js"](https://es-vuejs.github.io/vuejs.org/v2/guide/conditional.html).
- ["Mostrar y ocultar elementos usando v-if y v-show con Vue.js 2"](https://styde.net/mostrar-y-ocultar-elementos-usando-v-if-y-v-show-con-vue-js-2/).
- ["Introducción a las directivas de Vue.js con v-if, v-show y v-else – Styde ..."](https://styde.net/introduccion-a-las-directivas-de-vue-js-con-v-if-v-show-y-v-else/).
- ["Vue.js - v-if / v-else | vue.js Tutorial"](https://riptutorial.com/vue-js/example/11981/v-if---v-else).
- ["VueJS Rendering - Tutorialspoint"](https://www.tutorialspoint.com/vuejs/vuejs_rendering.htm).
- ["V-IF V-ELSE y TEMPLATE"](https://rimorsoft.com/v-if-v-else-y-template).

```

```
