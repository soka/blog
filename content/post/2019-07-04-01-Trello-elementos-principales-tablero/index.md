---
title: Trello - elementos principales de un tablero
subtitle: La pizarra digital inspirada en kanban
date: 2019-07-04
draft: false
description: Tableros en Trello
tags:
  [
    "Trello",
    "Talleres",
    "Cursos",
    "Aplicaciones",
    "Trello - Introducción",
    "Trello - Tableros",
    "Kanban",
    "Metodologías",
    "Gestión de Proyectos",
    "Herramientas",
  ]
---

![](img/logo-trello.png)

## Menú "Tableros"

![](img/01.PNG)

En la esquina superior izquierda un botón con la leyenda "Tableros", al hacer clic sobre el se despliegan todos los tableros sobre los que trabajamos, pueden ser nuestros o de equipos de trabajo a los que pertenecemos, podremos salta de forma rápida entre tableros usando este control (podemos usar el atajo de teclado con la tecla "b").

![](img/02.gif)

## Buscador

![](img/03.PNG)

Adyacente al menú "Tableros" encontramos un buscador, permite buscar tarjetas que contengan términos o palabras que buscamos entre todos los tableros que manejamos (["Searching for Cards (All Boards) - Trello Help](https://help.trello.com/article/808-searching-for-cards-all-boards)).

## Inicio

![](img/04.PNG)

El logo en el centro es un enlace al inicio [https://trello.com/](https://trello.com/).

## Crear

![](img/05.PNG)

Permite crear:

- Crear nuevo tablero.
- Crear nuevo equipo.
- La tercera opción de crear no me molesto en mencionarla ya que es para usuario de pago.

## Información

![](img/06.PNG)

Información y ayuda del propio **Trello**.

## Notificaciones

![](img/07.PNG)

El siguiente botón de gran utilidad, permite seguir de forma cronológica las notificaciones de todos los tableros, las notificaciones registran diferentes actividades como: Comentarios en una tarjeta, movimientos de tarjetas, cuando se creen o archiven tarjetas y un largo listado de eventos.

Más adelante profundizaremos más en las notificaciones ya que se merecen su propio apartado.

## Perfil

![](img/08.PNG)

El último control en la esquina superior derecha es el que permite realizar ajustes de la configuración de nuestra cuenta, ver las tarjetas que tenemos asignadas o la actividad de esta cuenta.

## Área de trabajo

![](img/09.PNG)

En el área de trabajo podemos cambiar el nombre del tablero, **si marcamos el tablero como favorito** saldrá entre los primeros en la lista de tableros.

**Los tableros pueden ser personales o pertenecer a un equipo de trabajo**.

**Es importante saber como se comparten o visibilizan los tableros**, pueden ser:

- **Privado:** Solo las personas añadidas al tablero pueden verlo y editarlo.
- **Equipo**: Pueden verlo los miembros del equipo.
- **Público**: Lo puede ver cualquiera.

## Menú de tablero

![](img/10.PNG)

Desplegando el menú lateral derecho accedemos a la gestión del tablero actual, algunas opciones que ofrece:

- **Acerca de este tablero**: Permite añadir una descripción del tablero, puede ser interesante si es público o queremos compartirlo con colaboradores de fuera de nuestra organización, clientes, etc. También permite cambiar los permisos del tablero.
  - Cambiar equipo del tablero.
  - Mostrar o no imágenes en la parte delantera de las tarjetas sin abrirlas.
  - Permisos para comentar.
  - Añadir/Eliminar permisos.
  - Permitir que los miembros del equipo editen y se unan.
- **Cambiar fondo**: Permite establecer un color solido de fondo o una serie de imágenes que ofrece **Trello**.
- **Buscar tarjetas**: Buscar por término, etiqueta, miembro o fecha de vencimiento (["Filtering vs. Searching - Trello Help"](https://help.trello.com/article/972-filtering-vs-searching)).
- **Pegátinas**.
- **Más**:
  - Configuración.
  - Etiquetas.
  - Elementos archivados.
  - Configuración de correo electrónico a tablero.
  - [Seguir](https://help.trello.com/article/799-watching-cards-lists-and-boards).
  - Copiar tablero.
  - Imprimir y exportar.
  - Cerrar tablero.
  - Enlace a tablero.
- **Power Ups**.

## Enlaces externos

- ["Searching for Cards (All Boards) - Trello Help](https://help.trello.com/article/808-searching-for-cards-all-boards).
- ["Filtering vs. Searching - Trello Help"](https://help.trello.com/article/972-filtering-vs-searching).
- ["Receiving Trello notifications - Trello Help"](https://help.trello.com/article/793-receiving-trello-notifications).
- ["Watching cards, lists, and boards - Trello Help"](https://help.trello.com/article/799-watching-cards-lists-and-boards).
