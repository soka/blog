---
title: Etherpad, editor Web de textos colaborativo en tiempo real [borrador]
subtitle: Documentación colaborativa
date: 2020-04-26
draft: false
description: 
tags: ["Aplicaciones","Etherpad","Web","Pad","Documentación","Editores"]
---

![](img/logo-01.png)

[**Etherpad**](https://etherpad.org/) es un **editor de texto en línea de [código abierto](https://es.wikipedia.org/wiki/C%C3%B3digo_abierto) colaborativo en tiempo real basado en Web**.

![](img/etherpad_demo.gif)

Invitar a otros a trabajar en equipo son dos sencillos pasos, como crear un nuevo documento y compartir el enlace.

Para empezar a probarlo en este enlace existe una lista de instancias de [**Etherpad**](https://etherpad.org/): ["Sites that run Etherpad Lite"](https://github.com/ether/etherpad-lite/wiki/Sites-that-run-Etherpad-Lite).

Cada instancia es autónoma (podemos instalar un Etherpad para nuestros propios propósitos en nuestro servidor) y pueden estar configuradas de forma diferente, algunos pueden requerir _login_ de acceso por ejemplo, a menudo se configuran para que los _Pads_ (documentos) se autodestruyan cuando transcurre un tiempo sin actividad.

Entre los _Pads_ públicos se encuentra uno llamado [Framapad](https://framapad.org/en/) creado por Framasoft, casualmente responsables también de PeerTube (ver ["PeerTube, la plataforma P2P, open source y descentralizada como alternativa a Youtube"](https://soka.gitlab.io/blog/post/2020-04-26-peertube/)).

![](img/01.png)

En la parte inferior se puede crear un _Pad_  con un nombre aleatorio, siempre puedes definir un nombre propio. Para la demostración he usado el nombre que me sugiere, una vez dentro si comparto la URL personalizada (en mi caso [https://hebdo.framapad.org/p/9gd2-0xe9phkhcw?lang=en](https://hebdo.framapad.org/p/9gd2-0xe9phkhcw?lang=en)) con alguien puede entrar y empezar a escribir.

![](img/02.png)

Con el enlace no requiere registro previo, el texto de cada autor se muestra en diferente color:

![](img/03.png)

En la barra lateral incorpora un chat que permite conversar de forma directa.

La edición de texto no acepta muchas opciones, podemos formatear el texto en negrita, cursiva, subrayado o tachado, acepta cabeceras de diferente nivel para los títulos, se pueden crear listas ordenadas o desordenadas y se pueden deshacer los cambios, etc.

**Siempre que recordemos el enlace podemos entrar más tarde y nuestros cambios permanecen en el documento**.

## Instancias

Algunas referencias están cógidas de la Web Librezale.eus.

- Yo en ocasiones he usado el servicio que ofrece [Riseup](https://riseup.net/es) [https://pad.riseup.net/](https://pad.riseup.net/).
- [https://etherpad.wikimedia.org/](https://etherpad.wikimedia.org/) (traducida a Euskera).
- [https://paper.komun.org/](https://paper.komun.org/) (traducida a Euskera).

## Conclusiones

Como no requiere registro es muy fácil crear un _Pad_ en pocos pasos y compartirlo, es ideal para reuniones o tomar notas provisionales rápidas para luego trasladar a un soporte definitivo más adelante.

No está ideado para maquetar documentos finales embellecidos para distribuir, ni siquiera permite exportar el documento a otros formatos.

## Enlaces internos

- ["PeerTube, la plataforma P2P, open source y descentralizada como alternativa a Youtube"](https://soka.gitlab.io/blog/post/2020-04-26-peertube/).

## Enlaces externos

- [https://etherpad.org/](https://etherpad.org/).
- github.com ["Sites that run Etherpad Lite"](https://github.com/ether/etherpad-lite/wiki/Sites-that-run-Etherpad-Lite).
- es.wikipedia.org ["Etherpad"](https://es.wikipedia.org/wiki/Etherpad).
- nobbot.com ["EtherPad permite la colaboración en tiempo real en tus documentos"](https://www.nobbot.com/personas/etherpad-permite-la-colaboracion-en-tiempo-real-en-tus-documentos/).
- ["EtherPad, desarrollo de documentos de manera colaborativa y en tiempo real"]([https://www.genbeta.com/web/etherpad-desarrollo-de-documentos-de-manera-colaborativa-y-en-tiempo-real).
- librezale.eus [KomunikazioEtaElkarlanerakoTresnak](https://librezale.eus/wiki/KomunikazioEtaElkarlanerakoTresnak#Etherpad).