---
title: Diagramas de dispersión en R (1/2)
subtitle: Guía avanzada Scatter Plots en R con ggplot y geom_point
date: 2019-04-25
draft: false
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "Programación",
    "R - Básico",
    "R - Gráficos",
    "R - Diagramas",
    "R - ggplot2",
    "Diagramas",
  ]
---

![](images/ggplot_hex.jpg)

En (["“Gráficos avanzados en R - Introducción a ggplot2”."](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-avanzados-ggplot2-1/) revise algunas opciones básicas para generar [diagramas de dispersión](https://es.wikipedia.org/wiki/Diagrama_de_dispersi%C3%B3n) (scatter plots) con **ggplot**.

Los diagramas de dispersión se usan para **mostrar la relación entre dos variables continuas** (aunque también se suele representar como la relación de una variable continua y una categórica), cada observación se representa como un punto representado en coordenadas cartesianas.

En esta ocasión como origen de datos emplearé el paquete ["gcookbook"](https://cran.r-project.org/web/packages/gcookbook/index.html) que contiene _datasets_ usados como ejemplo en la publicación ["R Graphics Cookbook"](http://www.cookbook-r.com/Graphs/) de Winston Chang, publicado por O'Reilly (Reference manual: [gcookbook.pdf](https://cran.r-project.org/web/packages/gcookbook/gcookbook.pdf)).

Como siempre instalamos y cargamos los paquetes imprescindibles:

```R
install.packages("ggplot2")
library(ggplot2)

install.packages("gcookbook") # Data sets used in the book "R Graphics Cookbook" by Winston Chang, published by O'Reilly Media.
library(gcookbook)
```

De todos los _datasets_ usaremos "heightweight" que contiene observaciones de peso y altura de escolares.

```R
> head(heightweight,n=5)
  sex ageYear ageMonth heightIn weightLb
1   f   11.92      143     56.3     85.0
2   f   12.92      155     62.3    105.0
3   f   12.75      153     63.3    108.0
4   f   13.42      161     59.0     92.0
5   f   15.92      191     62.5    112.5
```

## Diagrama básico

De todas las columnas sólo usaremos dos, la edad y la altura:

```R
heightweight[, c("ageYear", "heightIn")]
```

Proporcionamos el `data.frame` _heightweight_ a [`ggplot`](https://www.rdocumentation.org/packages/ggplot2/versions/3.1.1/topics/ggplot) en primer lugar y especificamos la estética con [`aes`](https://www.rdocumentation.org/packages/ggplot2/versions/3.1.1/topics/aes) (_aesthetic_) asociando cada columna a un eje del diagrama (_"Aesthetic mappings describe how variables in the data are mapped to visual properties (aesthetics) of geoms."_).

La función [`geom_point`](https://www.rdocumentation.org/packages/ggplot2/versions/3.1.1/topics/geom_point) se usa para crear diagramas de dispersión (geometría de puntos).

```R
ggplot(heightweight, aes(x=ageYear, y=heightIn)) + geom_point()
```

![](images/scatterplot-01.PNG)

Deducimos que existe una **correlación positiva entre ambas variables**, a mayor edad mayor peso.

Se puede dibujar una línea de ajuste (llamada también "línea de tendencia") con el fin de estudiar la correlación entre las variables (el modelo de [**regresión lineal**](https://es.wikipedia.org/wiki/Regresi%C3%B3n_lineal) por ejemplo).

## Modificando los puntos

Para cambiar apariencia de los puntos usamos `shape`. Por ejemplo usamos una alternativa a los círculos sólidos (shape #16) por círculos sin relleno (#21).

```R
ggplot(heightweight, aes(x=ageYear, y=heightIn)) + geom_point(shape=21)
```

![](images/scatterplot-02.PNG)

![](images/plot-symbols.PNG)

Podemos controlar el tamaño de los puntos con `size`, por defecto es 2.

```R
ggplot(heightweight, aes(x=ageYear, y=heightIn)) + geom_point(size=5.5)
```

![](images/scatterplot-03.PNG)

## Agrupando los puntos usando una variable usando shape o color

Vamos a agrupar los puntos usando la columna _sex_:

```R
heightweight[, c("sex", "ageYear", "heightIn")]
```

Podemos agruparlos usando puntos de colores diferentes (["Colors (ggplot2) - Cookbook for R"](<http://www.cookbook-r.com/Graphs/Colors_(ggplot2)/>)):

```R
ggplot(heightweight, aes(x=ageYear, y=heightIn, colour=sex)) + geom_point()
```

![](images/scatterplot-04.PNG)

O usando diferentes tipos de puntos:

```R
ggplot(heightweight, aes(x=ageYear, y=heightIn, shape=sex)) + geom_point()
```

![](images/scatterplot-05.PNG)

También podemos combinar las opciones de arriba en uno:

```R
ggplot(heightweight, aes(x=ageYear, y=heightIn, shape=sex, colour=sex)) +
  geom_point()
```

![](images/scatterplot-06.PNG)

Podemos definir de forma manual los colores y el diseño de los puntos:

```R
ggplot(heightweight, aes(x=ageYear, y=heightIn, shape=sex, colour=sex)) +
  geom_point() +
  scale_shape_manual(values=c(1,2)) +
  scale_colour_brewer(palette="Set1")
```

![](images/scatterplot-07.PNG)

## Mapeando una variable continua al color o forma del punto

En los ejemplos anteriores modificamos la forma o color para una variable categórica. ¿Como lo hacemos para una variable cuantitativa continua?
Un diagrama de nube de puntos o de dispersión relaciona dos variables continuas, una mapeada al eje X y la otra al eje Y. Cuando tenemos una tercera variable debemos usar _aes_ (estética) modificando el tamaño o color de los puntos.

Por ejemplo por la columna peso _weightLb_ define el **color de los puntos**:

```R
heightweight[, c("sex", "ageYear", "heightIn", "weightLb")]
ggplot(heightweight, aes(x=ageYear, y=heightIn, colour=weightLb)) + geom_point()
```

![](images/scatterplot-08.PNG)

También podemos hacerlo por el **tamaño de puntos** (_bubblechart_):

```R
ggplot(heightweight, aes(x=ageYear, y=heightIn, size=weightLb)) + geom_point()
```

![](images/scatterplot-09.PNG)

## Añadiendo información

Sintaxis usando [`lab`](https://www.rdocumentation.org/packages/ggplot2/versions/2.1.0/topics/labs) para añadir textos personalizados a los ejes, definir un título, etc.

```R
p <- ggplot(heightweight, aes(x=ageYear, y=heightIn)) + geom_point()
observaciones <- dim(heightweight)[[1]]
p <- p +  labs(
  title = paste("Observaciones de ",observaciones," escolares"),
  subtitle = "Relación altura y edad",
  caption = "Autor: Iker Landajuela",
  x = "Edad",
  y = "Altura")
```

![](images/scatterplot-11.PNG)

## Cambiando el theme

`ggplot` permite personalizar el aspecto usando los _theme_ disponibles (ver [lista](https://ggplot2.tidyverse.org/reference/ggtheme.html) completa temas disponibles):

```R
p <- p + theme_dark()
```

![](images/scatterplot-12.PNG)

## Escalando resultados

En ocasiones debemos transformar los datos:

```R
head(mtcars,n=5)
ggplot(mtcars, aes(x = log(mpg), y = log(wt),color = factor(gear))) +
  geom_point()
```

![](images/scatterplot-13.PNG)

## Guardando el resultado

Para guardar los resultados usamos [`ggsave`](https://www.rdocumentation.org/packages/ggplot2/versions/3.1.1/topics/ggsave):

```R
ggsave("my_fantastic_plot.png")
```

## Añadiendo la recta de regresión lineal

Para añadir una recta de regresión lineal al diagrama usamos `stat_smooth()` indicando que use el modo de lineal (`method=lm`):

```R
sp <- ggplot(heightweight, aes(x=ageYear, y=heightIn))
sp + geom_point() + stat_smooth(method=lm)
```

![](images/scatterplot-10.PNG)

Por supuesto también podemos modificar la recta:

```R
sp <- ggplot(heightweight, aes(x=ageYear, y=heightIn))
sp + geom_point() +
  stat_smooth(method = "lm",
              col = "#C42126",
              se = FALSE,
              size = 1)
```

Donde:

- `method = "lm"`: Método de regresión lineal.
- `col = "#C42126"`: Color rojo de línea.
- `se = FALSE`: No mostrar el error estándar.
- `size = 1`: Tamaño de la línea.

![](images/scatterplot-14.PNG)

Por defecto `stat_smooth` añade una región con el 95% de confianza (zona sombreada que acompaña la recta), podemos modificarla:

```R
sp <- ggplot(heightweight, aes(x=ageYear, y=heightIn))
sp + geom_point() + stat_smooth(method=lm, level=0.99)
```

![](images/scatterplot-15.PNG)

Por supuesto si no queremos intervalo de confianza usamos `se = FALSE`.

La recta de regresión líneal no es el único método, si no indicamos ningún parámetro a `stat_smooth()` aplica un método loess (locally weighted polynomial) con forma de curva (se lo podemos indicar explicitamente con `method=loess`):

```R
sp <- ggplot(heightweight, aes(x=ageYear, y=heightIn))
sp + geom_point() + stat_smooth()
```

![](images/scatterplot-16.PNG)

## Ejercicios

- r / intro / 42-ggplot-scatterplot / [ggplot-scatterplot.R](https://gitlab.com/soka/r/blob/master/intro/42-ggplot-scatterplot/ggplot-scatterplot.R).
- r / intro / 30-ggplot2-1 / [ggplot2-intro.R](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-avanzados-ggplot2-1/): Estos son los ejemplos de la introducción a ggplot (artículo previo).

## Enlaces internos

- ["Gráficos avanzados en R - Introducción a ggplot2"](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-avanzados-ggplot2-1/).

## Enlaces externos

- R-bloggers ["A Detailed Guide to the ggplot Scatter Plot in R"](https://www.r-bloggers.com/a-detailed-guide-to-the-ggplot-scatter-plot-in-r/).
- [ggplot function | R Documentation](https://www.rdocumentation.org/packages/ggplot2/versions/3.1.1/topics/ggplot).
- ["Colors (ggplot2) - Cookbook for R"](<http://www.cookbook-r.com/Graphs/Colors_(ggplot2)/>)
- ["ggplot2 Quick Reference: colour (and fill) | Software and Programmer ..."](http://sape.inf.usi.ch/quick-reference/ggplot2/colour).
- ["Scatter Plot in R using ggplot2 (with Example)"](https://www.guru99.com/r-scatter-plot-ggplot2.html).
- ["ggplot2 scatter plots : Quick start guide - R software and data visualization"](http://www.sthda.com/english/wiki/ggplot2-scatter-plots-quick-start-guide-r-software-and-data-visualization).
