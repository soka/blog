---
title: Scripts en R 
subtitle: Ficheros de ejecución de secuencias de comandos
date: 2019-01-28
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico"]
---

Podemos crear un script desde RStudio usando el menú o la combinación de teclas CTRL+Shift+N, se abre una nueva pestaña donde introducimos la secuencia de código que luego podemos ejecutar.

![](images/rstudio-new-script-01.PNG)

Realizamos unas operaciones con variables, cuando seleccionamos todo el código y pulsamos enter se ejecuta.

![](images/rstudio-new-script-02.PNG)

# Enlaces 

* ["Enlaces posts en R en esta Web"](https://soka.gitlab.io/blog/tags/r/).


