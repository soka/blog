function initFunction() {
    const element = document.getElementById("sample");
    element.addEventListener("click", listenerFunction);
    console.log("Evento registrado");
}
  
function listenerFunction(this: HTMLElement, ev: Event) {
    //alert(ev); // Muestra [object MouseEvent]
    console.log("listenerFunction");
    ev.preventDefault();
    this.style.backgroundColor = "red";
}

