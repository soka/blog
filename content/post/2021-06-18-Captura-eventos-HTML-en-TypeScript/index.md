---
title: Captura de eventos HTML en TypeScript
subtitle: TS interactúa con HTML 
date: 2021-06-18
draft: true
description: TypeScript
tags: ["TypeScript","TypeScript - Básico","TypeScript - HTML","Programación","TypeScript - DOM"]
---

<!-- vscode-markdown-toc -->
* 1. [TypeScript](#TypeScript)
* 2. [HTML](#HTML)
* 3. [Ejecución](#Ejecucin)
	* 3.1. [tsconfig.json](#tsconfig.json)
	* 3.2. [package.json](#package.json)
	* 3.3. [Live Server - Extensión para VSCode](#LiveServer-ExtensinparaVSCode)
* 4. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


##  1. <a name='TypeScript'></a>TypeScript

El método [`addEventListener`](https://developer.mozilla.org/es/docs/Web/API/EventTarget/addEventListener) registra un evento a un objeto específico, le pasamos como primer parámetro el [tipo de evento](https://developer.mozilla.org/en-US/docs/Web/API/Event/type) que queremos capturar, el segundo parámetro es el objeto que recibe la notificación cuando ocurre el evento, en nuestro caso una simple función en TS.

Sintaxis:

```js
target.addEventListener(tipo, listener[, useCapture]);
```

La función `initFunction` la invocaremos desde el HTML más abajo.

La función notificada por el evento que se dispara cuando hacemos "click" sobre el elemento HTML es `listenerFunction`, el primer parámetro hace referencia al elemento HTML desde donde se disparó el evento, el segundo parámetro es un objeto de tipo [Event](https://developer.mozilla.org/es/docs/Web/API/Event).  	

```ts
function initFunction() {
    const element = document.getElementById("sample");
    element.addEventListener("click", listenerFunction);    
}
  
function listenerFunction(this: HTMLElement, ev: Event) {
    ev.preventDefault();
    this.style.backgroundColor = "red";
}
```

##  2. <a name='HTML'></a>HTML


```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>HTML Event Handling using TypeScript</title>
    <script src="script.js"></script>
  </head>
  <body>
    <div
      id="sample"
      style="height: 200px; width: 200px; background-color: blue"
    ></div>
  </body>
  <script>  
    initFunction();
  </script>
</html>
```

##  3. <a name='Ejecucin'></a>Ejecución

###  3.1. <a name='tsconfig.json'></a>tsconfig.json

```json
{
    "compilerOptions": {
      "module": "es6",
      "target": "es5",
      "outDir": ".",
      "sourceMap": true
    },
    "include": [
      "."
    ],
    "exclude": [
      "node_modules"
    ]
}
```

###  3.2. <a name='package.json'></a>package.json


```json
{
    "name": "src",
    "version": "1.0.0",
    "description": "",
    "scripts": {
        "build": "tsc",
        "start": "tsc -w"
    }
}
``` 

```bash
npm run start
```

Abrimos con Firefox:

```bash
firefox-esr index.html
``

###  3.3. <a name='LiveServer-ExtensinparaVSCode'></a>Live Server - Extensión para VSCode

[http://127.0.0.1:5500/](http://127.0.0.1:5500/).

##  4. <a name='Enlacesexternos'></a>Enlaces externos

* Artículo original fuente de este proyecto [https://medium.com/geekculture/html-event-handling-in-typescript-b9ca7178d912](https://medium.com/geekculture/html-event-handling-in-typescript-b9ca7178d912)
* element.addEventListener [https://developer.mozilla.org/es/docs/Web/API/EventTarget/addEventListener](https://developer.mozilla.org/es/docs/Web/API/EventTarget/addEventListener)