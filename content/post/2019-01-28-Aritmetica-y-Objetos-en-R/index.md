---
title: Operadores y aritmética básica en R
subtitle: Aritméticos, comparativos y lógicos
date: 2019-01-28
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico"]
---

A continuación vamos a ver los principales operadores aritméticos, lógicos y de comparación en R.

# Operadores aritméticos

En **R** podemos realizar cualquier tipo de operación aritmética:

```
> 1+1
[1] 2
> 1/2
[1] 0.5
> 1*2
[1] 2
```

Las operaciones aritméticas son: suma, resta, multiplicación, división, potenciación, división entera: +, -, \*, /, ^, %/% or %%.

También podemos asignar un valor a una variable, podemos usar el operador de asignación `=` pero en **R** se suele usar la siguiente notación (atajo de teclado 'ALT+-'):

```
> x <- 4
> x
[1] 4
```

Una de las cosas más importantes que debemos saber de R es que es un lenguaje orientado a objeto ([POO](https://es.wikipedia.org/wiki/Programaci%C3%B3n_orientada_a_objetos)), las variables que acabamos de definir internamente también son objetos.

```
> x <- "HOla soy x"
> x
[1] "HOla soy x"
```

# Operadores Comparativos

Los operadores comparativos evaluan la condición entre los valores, por ejemplo podemos comprobar si el contenido de la variable `x` es mayor que `y`. El resultado de comparar dos valores es verdadero (`TRUE`) o falso (`FALSE`) (booleano o [tipo de dato lógico](https://es.wikipedia.org/wiki/Tipo_de_dato_l%C3%B3gico)).

```
> X <- 5
> y <- 2
> X > y
[1] TRUE
> x == y
[1] FALSE
> x != y
[1] TRUE
```

La siguiente resume las expresiones que podemos usar:

| Operador | Significado   |
| -------- | ------------- |
| >        | mayor que     |
| <        | menor que     |
| >=       | mayor o igual |
| <=       | menor o igual |
| ==       | igual que     |
| !=       | distinto que  |

Aunque ya revisaré más adelante las sentencias condicionales `if` podemos hacer algo así:

```R
> a <- 5

> if (a == 5) cat("Es igual a 5")
Es igual a 5

> if (a != 4) cat("Es distinto 5")
Es distinto 5

> a <- 5
> if (a!=5) cat("es diferente a 5") else cat("Es cinco")
Es cinco

> a <- 5
> if (a!=5) cat("es diferente a 5") else cat("Es cinco")
Es cinco

> if (a>6) {
+   cat("Es mayor que 6")
+ } else {
+   cat("Es menor o igual que 6\n")
+   cat("otra línea")
+ }
Es menor o igual que 6
otra línea
```

Elemento de negación:

```R
> b <- 3
> # Comparamos e invertimos el resultado lógico a FALSE
> !(b < 6)
[1] FALSE
```

La orden `cat()` sirve para escribir por pantalla (concatenate and print). `\n` es un carácter de escape para introducir un salto de línea.

# Operadores lógicos

```
> ( y > 1 & x > 1)
[1] TRUE
```

AND, OR, XOR y NOT son también operadores lógicos y sirven para evaluar y combinar varias operaciones lógicas (e.g. comparaciones). Las distintas modalidades son: cumplimiento en forma simultánea (AND), alternativa (OR), exclusiva (XOR) y el negativo (NOT). Los símbolos para todos estos operadores son & (&&), | (||), xor y !.

# Casos particulares: NA, NULL y NaN

En **R** los valores no definidos se representan con el simbolo `NA`(**N**ot **A**vailable). Los valores imposibles (como una división por 0) se representan como `NaN` (**N**ot **a** **N**umber). Para identificarlos en cambio se usan las funciones is.xxx.

```
# NA (not available)
is.na(NA)     # TRUE
is.na(5)      # FALSE
```

# Ejercicios

En mi [repositorio de R en GitLab](https://gitlab.com/soka/r) se puede encontrar el script on ejercicios básicos de operadores.

- intro/00_operadores/[operadores.R](https://gitlab.com/soka/r/blob/master/intro/00_operadores/operadores.R)

# Enlaces

- ["Enlaces posts en R en esta Web"](https://soka.gitlab.io/blog/tags/r/).
- ["Nuestra primera sesión en R"](https://gitlab.com/soka/r/blob/master/intro/05-primera-sesion/primera-sesion.md).
- ["Sintaxis básica"](https://gitlab.com/soka/r/blob/master/intro/04-sintaxis-basica/sintaxis-basica.md).

**Enlaces externos**:

- ["Curso de R: 1.1. introducción al lenguaje (Objetos y operadores básicos)"](https://es.slideshare.net/NerysRamirez/curso-de-r-11-introduccin-al-lenguaje-objetos-y-operadores-bsicos).
- Quick-R by Datacamp ["Missing Data"](https://www.statmethods.net/input/missingdata.html).
- ["NA function | R Documentation"](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/NA).
