---
title: Menús de navegación en WordPress
subtitle: Personaliza el diseño y apariencia de tu sitio
date: 2020-11-24
draft: false
description: Facilita la navegación por los diferentes apartados
tags: ["Wordpress","WP - Curso","WP","WP - Plugins","WP - Menús"]
---

![](img/wordpress-logo-01.png)

## Pad de trabajo

[https://pad.riseup.net/p/penascalf52021](https://pad.riseup.net/p/penascalf52021)

## Introducción

Los menús juegan un papel muy importante para facilitar la navegación y la búsqueda de información del sitio Web. Los menús clásicos se posicionan en la parte superior de la página listando páginas individuales que forman parte del sitio Web. Algunas páginas típicas son la de _contacto_ o _quienes somos_  por ejemplo. En esencia son listas de enlaces que apuntan a diferentes áreas de nuestro sitio.

WP gestiona los menús de forma amigable, se pueden crear de forma sencilla menús que apunten a **páginas, entradas, categorías determinadas o un enlace cualquiera a una URL**.

## Añadiendo un menú

Dentro del escritorio debemos ir a la sección **Apariencia | Menús**. Para **crear nuestro primer menú** debemos introducir un nombre y guardar los cambios con el botón "Crear Menú".

![](img/01.png)

Ahora ya podemos añadir páginas determinadas desde el panel de la izquierda con el botón "Añadir al menú".

![](img/02.png)

Si quieres añadir entradas concretas también puedes hacerlo usando el apartado **Entradas** y el botón "Añadir al menú". Se pueden añadir enlaces personalizados con una URL, si queremos añadir un elemento de menú que contenga otros elementos de submenú y que no navega a ningún lado usamos la "#" en el campo de la URL.

![](img/03.png)

Podemos añadir categorías como elementos de menú para que muestre todas las entradas pertenecientes a esa categoría.

![](img/04.png)

**Siempre debemos de acordarnos de guardar los cambios que realicemos**.

## Creando múltiples niveles de menú

Podemos arrastrar y soltar un elemento del menú para posicionarlo como un subelemento del padre. También podemos arrastrarlos arriba y abajo para cambiarlos de posición.

![](img/05.png)

## Mostrando un menú

Dependiendo del tema visual instalado podemos tener diferentes localizaciones para nuestro menú, incluso podemos tenerlo duplicado en varios sitios (aunque no se si tiene mucho sentido).

![](img/06.png)

## Eliminando un elemento del menú

Eliminar un elemento del menú es realmente sencillo, desplegando las opciones del elemento encontramos la opción.

![](img/07.png)

## Propiedades avanzadas

En la parte superior de la pantalla de Menús desplegamos el apartado **Opciones de pantalla**.

![](img/08.png)

Los **Atributos del título** es el texto que se muestra cuando pasamos el ratón por encima del elemento, permite a los visitantes conocer que contiene. 

Las **Clases CSS** permiten definir estilos adicionales del menú.

Podemos hacer que se abra elemento en una pestaña nueva:

![](img/09.png)

## Enlaces externos

- [https://codex.wordpress.org/WordPress_Menu_User_Guide](https://codex.wordpress.org/WordPress_Menu_User_Guide).
- [https://wordpress.com/support/advanced-menu-settings/](https://wordpress.com/support/advanced-menu-settings/)
- [https://wpza.net/how-to-open-menu-link-in-a-new-tab-in-wordpress/](https://wpza.net/how-to-open-menu-link-in-a-new-tab-in-wordpress/).

