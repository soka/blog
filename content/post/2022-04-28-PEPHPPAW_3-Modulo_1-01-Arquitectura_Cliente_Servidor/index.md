---
title: Arquitectura Cliente Servidor -  Módulo 1
subtitle: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 1
date: 2022-04-28
draft: false
description: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 1
tags: ["PEPHPPAW_3","Cursos","Miriadax","Programación","Web","Desarrollo","HTML"]
---

<!-- vscode-markdown-toc -->
* 1. [Introducción](#Introduccin)
* 2. [Arquitectura Cliente Servidor](#ArquitecturaClienteServidor)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Introduccin'></a>Introducción

Video de presentación del módulo 1 del curso Programación efectiva en PHP para Aplicaciones Web.

{{< youtube J6vAY8f1KAg >}}

[https://www.youtube.com/watch?v=J6vAY8f1KAg](https://www.youtube.com/watch?v=J6vAY8f1KAg)

##  2. <a name='ArquitecturaClienteServidor'></a>Arquitectura Cliente Servidor

