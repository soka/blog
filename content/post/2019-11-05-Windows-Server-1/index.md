---
title: Instalación de Windows Server 2012 virtualizado en Debian
subtitle: Windows Server 2012 virtualizado en Debian 10 Buster
date: 2019-11-05
draft: false
description: Recursos Windows Server
tags: ["Windows","Sistemas","Windows Server","Windows Server 2012","Debian","Virtualización","libvrt","virt-manager","PowerShell"]
---

## Descarga de la ISO con Windows Server

En la Web de Microsoft se pueden descargar imágenes en formato ISO límitadas a un periódo de evaluación de 180 días ([enlace](https://www.microsoft.com/es-es/evalcenter/evaluate-windows-server-2012-r2)). El archivo que he descargado yo tiene este nombre "9600.17050.WINBLUE_REFRESH.140317-1640_X64FRE_SERVER_EVAL_ES-ES-IR3_SSS_X64FREE_ES-ES_DV9.ISO".

## Virtualización en Debian con libvrt

El paquete de Virual Box para Debian 10.0 Buster no está disponible ni tienes visos de que vayan a incorporarlo en el futuro ([ver anuncio](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=794466)), en su lugar recomiendan la aplicación de escritorio [virt-manager](https://packages.debian.org/buster/virt-manager) para gestionar máquinas virtuales.

Instalación:

```bash
# apt-get install qemu-kvm libvirt-clients libvirt-daemon-system
# apt-get install virt-manager
```

Ahora ejecutamos la aplicación gráfica:

```bash
# virt-manager
```

![](img/01.png)

Creamos una nueva MV cuyo origen de instalación sea la ISO, el resto es elegir el tamaño de la memoria, el número de CPU y el tamaño del disco (yo he elegido 10 GB).

![](img/02.png)


Ya estoy preparado para comenzar la instalación de Windows Server.

## Instalación básica

Nada más arrancar la MV comienza el proceso de instalación.

![](img/03.png)

Voy a realizar la instalación más sencilla con entorno gráfico (con GUI), una vez configurado el servidor y en marcha se puede configurar como modo "_core_" o consola, evita la sobrecarga de recursos y puede mejorar la seguridad, Windows Server 2012 nos permite instalar y desinstalar esta interfaz gráfica como una característica más de Windows.

Uso de **Server Core**: Despliegue de grandes plataformas homogéneas y estandarizadas, generalmente automatizadas, cargas de trabajo muy especializadas y estables, servidores que requieran de un uptime continuado debido a que su carga de trabajo no puede ser desplazada a otro servidor, etc.

El último paso es establecer la contraseña de Administrador y listo.

## Algunos comandos básicos en PowerShell

Ahora que ya está arrancado el sistema operativo podemos abrir una consola de comando y averiguar o configurar algunos parámetros:

- `hostname`: Para averiguar el nombre de nuestra máquina.
- El clásico `ipconfig /all` para averiguar la configuración de red.
- `rename-computer nombreservidor` para renombrar la máquina, requiere reinicio con `restart-computer`.
- `get-netadapter` información sobre el adaptador de red.
- `Get-NetIPConfiguration`.
- `Get-NetIPAddress`.
- `Add-Computer`: Unirse a un dominio.
- Apagado `shutdown /s`.
- **Habilitamos la comunicación remota de Windows PowerShell con `Enable-PSRemoting`** (Ver [https://soka.gitlab.io/PSFabrik/networking/remoto/](https://soka.gitlab.io/PSFabrik/networking/remoto/)).
- `sconfig`: **Configuración del servidor**.

Un ejemplo para cambiar la dirección de red y el servidor DNS:

```bash
New-NetIPAddress –IPAddress 192.168.0.99 -InterfaceAlias "Ethernet" -DefaultGateway 192.168.0.1 –AddressFamily IPv4 –PrefixLength 24

Set-DnsClientServerAddress -InterfaceAlias "Ethernet" -ServerAddresses 192.168.0.100
```

## Algunos consejos antes de terminar

Guarda una instantánea de tu MV de vez en cuando con los comentarios de los avances para volver sobre tus pasos o al estado inicial con el servidor recién instalado.

## Enlaces externos

- [Enable-PSRemoting - Microsoft Docs](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/enable-psremoting?view=powershell-5.1).
- [How to Run PowerShell Commands on Remote Computers](https://www.howtogeek.com/117192/how-to-run-powershell-commands-on-remote-computers/).

