<?php
/*
Dado un número almacenado en una variable "num", usar sentencias condicionales anidadas para 
evaluar las siguientes condiciones, si es positivo (mayor que 0) imprimir el valor 
del número en una frase que diga "El número 4 es positivo" (si "num" valiese 4), 
en caso contrario "El número -4 es negativo".

Sólo cuando sean positivos realizar una nueva comprobación, si es par imprimir "El número es par".
*/
$num = 4;
  
// condicional anidada
if ($num > 0){
    echo 'El número '.$num.' es positivo.<br>';
      if ($num % 2 === 0){
            echo 'El número es par';
      }
} else {
      echo 'El número '.$num.' es negativo.<br>';
}
?>