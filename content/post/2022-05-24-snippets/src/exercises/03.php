<?php
/*
Define una clase "Transporte" con 3 atributos protegidos: "nombre","color","vel_max", los dos primeros contendrán una cadena, la velocidad máxima será un número.

Sobrecargar el constructor de la clase donde se le pueden pasar los 3 atributos arriba descritos, definir parámetros por defecto: "sin nombre", "sin color" y 0 cuando no se especifican al instanciar la clase.

Definir una clase "Bicicleta" que hereda de "Transporte", contendrá un único atributo privado "tipo". 
Además sobrecargar el constructor para que reciba los atributos de la clase principal y el "tipo"

Crear un método público "imprime" de la clase "Bicicleta" que imprima los atributos en forma de tabla HTML con este diseño.

Ejemplo:

-------------------------------------------------------------
| Nombre    | Color     |  Velocidad máxima | Tipo          |
-------------------------------------------------------------
| Orbea     | Rojo      | 120               | Carretera     |
-------------------------------------------------------------

*/
class Transporte {
    protected $nombre ="";
    protected $color = "";
    protected $vel_max = 0;

    function __construct($nombre="sin nombre",$color="sin color",$vel_max=0) {
        $this->nombre = $nombre;        
        $this->color = $color;
        $this->vel_max = $vel_max;
    }   
}


class Bicicleta extends Transporte {
    private $tipo = "sin tipo";

    function __construct($nombre="sin nombre",$color="sin color",$vel_max=0,$tipo="sin tipo") {
        parent::__construct($nombre,$color,$vel_max);                    
        $this->tipo=$tipo;
    } 
    
    public function imprime() {
        //echo "test";
        //parent::imprimirResultado();
        echo "<table><tr><td>Nombre</td><td>Color</td><td>Velocidad máxima</td><td>Tipo</td></tr><tr><td>".$this->nombre."</td><td>".$this->color."</td><td>".$this->vel_max."</td><td>".$this->tipo."</td></tr></table>";
    }
}


$myBici = new Bicicleta("Orbea","Rojo",120,"Carretera");
$myBici->imprime();
?>