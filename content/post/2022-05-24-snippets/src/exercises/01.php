<?php
/* 
Implementar una función llamada "pinta" que reciba como parámetro una variable "altura". 
La función debe crear en HTML un triangulo donde cada altura está representada por el número de fila
que le corresponde con los números separados por espacios HTML.

Por ejemplo para un triangulo de altura 5 la salida esperada es esta:

1 
2 2 
3 3 3 
4 4 4 4 
5 5 5 5 5 
*/

function pinta($altura) {

    for($fila=1;$fila<=$altura;$fila++) {
        for($ancho=1;$ancho<=$fila;$ancho++) {
            echo $fila."&nbsp;";
        }
        echo "<br>";
    }

}

pinta(5);
?>