<?php
/*
Crea una clase "Persona" con los siguientes atributos privados: "nombre", "apellido" y "edad". 

Crea su constructor sobrecargado que acepte parámetros para los tres atributos de arriba, 
crear los métodos get y set.

Crear los siguientes métodos:

– "mayorEdad": retorna un booleano que indica si es o no mayor de edad.
– "nombreCompleto": devuelve una cadena con el nombre y el apellido.

Crea un objeto "persona" de la clase "Persona" pasando los siguientes parámetros:  "Xabier"," Elkoro", 29.

Usar una sentencia condicional en el programa principal que empleando el método "mayorEdad" 
compruebe si es mayor de edad, si es mayor de edad imprimirá una frase con el nombre y apellido indicando 
que es mayor de edad 

*/
class Persona { 
    private $nombre;
    private $apellidos;
    private $edad;
 
    function __construct($nombre, $apellidos, $edad) {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->edad = $edad;
    }
 
    function __get($propiedad) {        
        return $this->$propiedad;
    }
 
    function __set($propiedad, $valor) {
        $this->$propiedad = $valor;    
    }
 
    function mayorEdad(){
        return $this->edad >= 18;
    }
 
    function nombreCompleto(){
        return $this->nombre . " " . $this->apellidos;
    }
 
}
 
$persona = new Persona("Fernando", "Ureña Gomez", 29);
 
if($persona->mayorEdad()){
    echo $persona->nombreCompleto(). " es mayor de edad";
}else{
    echo $persona->nombreCompleto(). " no es mayor de edad";
}
?>