<?php

/*

Obtener los divisores de un número.

Un divisor es aquel número que al dividir del número principal su resto es 0, excepto 1 y el propio número
Ejemplo. Los divisores de 28: 2, 4, 7 y 14

Implementa una función llamada "dame_divisores" que reciba un parámetro llamado "numero", la función
debe obtener todos los divisores de ese número y retornarlos al programa principal en 
un array "divisores" donde se debe imprimir el array con cada divisor separado por un guion medio.

*/ 

function dame_divisores($numero) {
    $divisores = array();
    for($i = 2; $i < $numero; $i++) {
        if ($numero % $i == 0) {
            $divisores[] = $i;
        }
    }
    return $divisores;
}
     
foreach (dame_divisores(28) as $div) {
    echo $div."-";
} 

?>     