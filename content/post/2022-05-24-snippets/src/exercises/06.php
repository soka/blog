<?php
/*
Evaluar una variable llamada "color" usando una estructura "switch", cuando su valor sea "rojo" imprimir "hola", 
si es verde entonces imprimir "Bienvenido", en cualquier otro caso imprimir "Nada".
*/
$color="verde";

switch ($color) {
    case "rojo":
      echo "Hola";
      break;
    case "verde":
      echo "Bienvenido";
      break;
    default:
      echo "Nada";
      break;
  }
?>