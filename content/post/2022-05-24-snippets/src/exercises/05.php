<?php
/*
Usando sentencias condicionales, escribe "1" la variable "a" es igual a "b", 
si "a" es mayor que "b" escribe "2", en cualquier otro case escribe "3".
*/
$a = 50;
$b = 10;
if ($a == $b) {
  echo "1";
} elseif ($a > $b) {
  echo "2";
}  else {
  echo "3";
}
?>