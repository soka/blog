<?php  // [xyz-ips snippet="Test"]

//print_r($_GET);

if (count($_GET)) {
  //echo "Tiene parámetros GET!<br>";  

  if (isset($_GET['Id'])) {

    $esta_pedido = FALSE;
    //primero comprobamos si ya está seleccionado
    foreach($_SESSION['IdLst'] as $IdPedido) {
      if ($IdPedido == $_GET['Id']) {
        echo "Ese plato ya existe, no se pudo añadir<br>";
        $esta_pedido=TRUE;
      }
    }

    if(!$esta_pedido) {
      // Añadimos un nuevo plato al carrito
      $_SESSION['IdLst'][] = $_GET['Id'];
    }

    //print_r($_SESSION);
  }

  if(isset($_GET['Borrar'])) {
    $_SESSION['IdLst'] = array();
  }

} else {
  echo "no he recibido nada aún<br>";
  
}

global $wpdb;
// Realizamos una consulta llamando al método get_results
// https://developer.wordpress.org/reference/classes/wpdb/get_results/
// retorna un array de objetos  
$results = $wpdb->get_results( "SELECT * FROM Platos", OBJECT );

//print_r($results);

if(!count($results)) {
  echo "No hay resultados o error sintaxis SQL";
} else { 

echo "<h1>Platos a elegir hoy</h1>";

echo <<<EOT
    <table>
    <thead>
        <tr>
          <th>Name</th>
          <th>Descripción</th>
          <th>Añadir</th>
        </tr>
    </thead>
    <tbody>
    EOT;
  
  foreach($results as $row) {
    //print_r($row);
    //echo "Nombre: ".$row->Nombre."<br>";    
    echo "<tr>";    
    echo "<td>$row->Nombre</td>";
    echo "<td>$row->Descripción</td>";
    echo "<td><a href=\"?Id=$row->Id\">+</a></td>";
    echo '</tr>';


  }

echo <<<EOT
    </tbody>
    </table>
  EOT;  

}

echo "<h1>Pedido</h1>";

echo "<table>
<thead>
    <tr>
      <th>Nombre</th>      
    </tr>
</thead>
<tbody>";

foreach( $_SESSION['IdLst'] as $IdPedido) {
  $results = $wpdb->get_results( "SELECT * FROM Platos WHERE Id=$IdPedido", OBJECT );
  //print_r($results);
  //echo "<td></td>Pedido:".$IdPedido."<br>";
  echo "<tr>";
  echo "<td>".$results[0]->Nombre."</td>";
  echo "</tr>";
}

echo "</tbody></table>";

echo "<a href=\"?Borrar=yes\">Borrar pedido</a>";