DROP TABLE `Platos`;

CREATE TABLE `Platos` (
    `Nombre` varchar(255) NOT NULL,
    `Descripción` varchar(255) NOT NULL,
    `Id` bigint NOT NULL AUTO_INCREMENT,
    `Gluten` bool NOT NULL,
    `Vegano` bool NOT NULL,
    PRIMARY KEY (`Id`)
);

INSERT INTO `Platos` (`Nombre`, `Descripción`, `Id`, `Gluten`, `Vegano`) VALUES ('Lentejas caseras con chorizo', 'No a todo el mundo le gustan, como dice el refrán «El que quiera las coma y el que no las deja» pero creo que es importante educar a todos los niños/as desde pequeños para que les guste este guiso introduciéndolo en la dieta diaria.', NULL, '1', '0');


INSERT INTO `Platos` (`Nombre`, `Descripción`, `Id`, `Gluten`, `Vegano`) VALUES ('Bacalao al pil pil', 'El bacalao al pilpil es un plato tradicional de la cocina vasca elaborado con cuatro ingredientes básicos: bacalao, aceite de oliva, ajo y guindillas.', NULL, '0', '0');


INSERT INTO `Platos` (`Nombre`, `Descripción`, `Id`, `Gluten`, `Vegano`) VALUES ('Hummus o crema de garbanzos', 'El hummus bi tahina, frecuentemente simplificado a hummus es una crema de garbanzos cocidos con zumo de limón, ​ que incluye pasta de tahina y aceite de oliva, que según la variante local puede llevar además otros ingredientes como ajos o pimentón..', NULL, '0', '1');









