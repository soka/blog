<?php

function session_init() {
    if (!session_id()) {
        session_start();
    }
}
add_action( 'init', 'session_init' );

/* Esto también funciona 
// session_id() returns the session id for the current session or the empty string ("") if there is no current session (no current session id exists). On failure, false is returned.
if(!session_id()) {
    session_start();
    print_r($_SESSION);
}
*/

