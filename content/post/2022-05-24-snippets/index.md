---
title: Snippets
subtitle: Curso WordPress
date: 2022-05-24
draft: true
description: Buscando noticias y recursos 
tags: ["Wordpress","WP - Curso","WP","WP - Reto","WP - Recursos"]
---

<!-- vscode-markdown-toc -->
* 1. [wpdb - WordPress database access abstraction class](#wpdb-WordPressdatabaseaccessabstractionclass)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='wpdb-WordPressdatabaseaccessabstractionclass'></a>wpdb - WordPress database access abstraction class


**Enlaces:**

* https://developer.wordpress.org/reference/classes/wpdb/get_results/
* https://developer.wordpress.org/reference/classes/wpdb/


## Sesiones en WP

El core de WP no usa las sesiones, para mantener la información de forma persistente cuando navegamos entre páginas o cuando se recarga una página debemos llamar a las funciones de PHP para crear una sesión de forma explicita.

Debemos añadir las siguientes líneas al comienzo del archivo functions.php del tema:

```php
if(!session_id()) {
    session_start();    
}
```

Usamos [`session_id`](https://www.php.net/manual/es/function.session-id.php) para obtener el identificador de la sesión actual, si no existe devuelve cadena vacía (""), en ese caso procedemos a crear una nueva sesión con [`session_start`](https://www.php.net/manual/es/function.session-start.php).

Algo más correcto sería algo así colocado en el mismo sitio en lugar de lo de arriba:

```php
function session_init() {
    if (!session_id()) {
        session_start();
    }
}
add_action( 'init', 'session_init' );
```

La función [`add_action`](https://developer.wordpress.org/reference/functions/add_action/) es propia de WordPress, lo que hacemos es ejecutar nuestra función `session_init` en un punto concreto durante la ejecución de WP. El primer parámetro indica la acción de WP durante la cual llamará a nuestra función, en este caso al iniciarse la ejecución de WP.

**Enlaces:**

* https://pupungbp.com/using-session-wordpress/
