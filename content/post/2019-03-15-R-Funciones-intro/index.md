---
title: Funciones en R
subtitle: Introducción básica
date: 2019-03-15
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico", "R - Datos", "R - Funciones"]
---

![](images/r-logo.PNG)

En esta ocasión vamos a crear nuestra propia función. La declaración básica de una función se muestra abajo, debemos darle un nombre para poder invocarla más tarde:

```R
Func1 <- function() {

}
```

## Parámetros de una función

Podemos pasar parámetros a la función como sigue y llamarla tantas veces queramos, se evidencia que es muy útil cuando queremos ejecutar una secuencia de instrucciones llamando a la función desde cualquier parte de nuestra aplicación.

```R
Func1 <- function(edad) {
  edad
}

Func1(30)
Func1(40)
```

Ahora vamos a crear una función para que imprima el resultado de una suma

```R
Func2 <- function(x,y) {
  x+y
}

Func2(1,2)
```

¿Qué pasa si llamamos a la función de arriba sólo con un parámetro?

```R
> Func2(1)
Error in Func2(1) : argument "y" is missing, with no default
```

Produce un error ya que no hemos definido un parámetro por defecto.

El orden no importa si ponemos el nombre del parámetro:

```R
f <- function(a,b) {
  cat("a es: ",a," y b es:",b)
}

f(b=3,a=1)
```

## Valores por defecto

```R
mi_suma <- function(x,y=1) {
  x+y
}

mi_suma(3)
```

## Retornar valores de una función

Las funciones pueden retornar valores que podemos usar en el cuerpo principal del programa:

```R
Func3 <- function(x,y) {
  return(x+y)
}
z <- Func3(1,2)
```

Como no hemos puesto restricciones sobre los argumentos se pueden sumar objetos de cualquier tipo, por ejemplo vectores o matrices:

```R
> Func3(c(1,2,3),c(1,2,3))
[1] 2 4 6
```

## Retornar más de una variable

Para devolver más de una variable debemos usar un vector:

```R
Func4 <- function() {
  x <- 1
  y <- 2
  return(c(x,y))
}
```

## Utilidades para funciones

Esta función permite establecer o obtener los argumentos de una función:

```R
> formals(Func3)
$x

$y
```

Hacer lo mismo con el cuerpo de la función:

```R
> body(Func3)
{
    return(x + y)
}
```

Para editar la función:

```R
edit(Func3)
```

`args` Muestra los nombres de los argumentos de una función y sus valores por defecto:

```R
> args(sd)
function (x, na.rm = FALSE)
NULL
```

## Comprobar si falta un argumento de una función

`missing` se puede usar para comprobar si se proporciono una variable como argumento de una función:

```R
func <- function(a, b) {
  if (missing(a)) {
    cat("missing")
  }
}

func()
func(1,2)
```

## Ejercicios

- r / intro / 25-Funciones-intro / [funciones.R](https://gitlab.com/soka/r/blob/master/intro/25-Funciones-intro/funciones.R).

## Enlaces externos

- ["Crea tu propia función en R paso a paso | Máxima Formación"](https://www.maximaformacion.es/blog-dat/crea-tu-propia-funcion-en-r-paso-a-paso/).
- ["Funciones en R (principios y fundamentos) | R blogs / lang - R-bloggers"](https://www.r-bloggers.com/lang/spanish/2512).
