---
title: Introducción a OpenRefine [borrador] 
subtitle: Wikipedia - Encuentros/Sesiones en línea WMES 2020
date: 2020-05-09
draft: true
description: 
tags: ["Aplicaciones","OpenRefine"]
---

![](img/openrefine.jpg)

Las notas iniciales las cogí en la sesión organizada por [Wikimedia España](https://www.wikimedia.es/wiki/Portada) el 09 de Mayo del 2020 (["Wikipedia:Encuentros/Sesiones en línea WMES 2020"](https://es.wikipedia.org/wiki/Wikipedia:Encuentros/Sesiones_en_l%C3%ADnea_WMES_2020)), he completado el artículo posteriormente usando las referencias que se encuentran más abajo.

## Introducción

Afrontemos un hecho, nuestros datos siempre contienen incoherencias y errores, los grandes conjuntos de datos los suelen tener por mucho cuidado y atención que les dediquemos, especialmente cuando pasan por muchas manos, además el factor tiempo suele jugar en contra nuestra. El proceso para buscar la calidad de los datos suele involucrar el [**perfilado de datos**](https://es.qwe.wiki/wiki/Data_profiling) (_data profiling_) y la **limpieza** de los mismos.

El [**perfilado de datos**](https://es.qwe.wiki/wiki/Data_profiling) es definido por Olson (Data Quality: The Accuracy Dimension, Jack E. Olson, Morgan Kaufman, 2003) como _"the use of analytical techniques to discover the true structure, content, and quality of data"_(en Inglés original).

La [**limpieza de datos**](https://es.wikipedia.org/wiki/Limpieza_de_datos) consiste en corregir los errores de una forma semiautomatizada, por ejemplo eliminando espacios en blanco o datos duplicados, filtrándo o transformándolos de formas diversas (Interactive Data Transformation tools - IDTs).

## OpenRefine

![](img/06.png)

[**OpenRefine**](https://openrefine.org/) (previamente llamado Google Refine) es una herramienta libre de tipo IDT (Interactive Data Transformation tools) para visualizar y manipular datos, limpiarlos y transformarlos en otro formato de salida, se maneja mediante un interfaz Web (se ejecuta en un servidor Web local sin necesidad de Internet excepto que debas conectar con bases de datos o Web externas para obtener la información) y es muy intuitiva y sencilla de usar pero con funcionalidades potentes, aparenta una especie de Excel pero trabaja más como una base de datos.

En la sesión que he asistido la salida del proyecto se conectaba a una base de datos en [**Wikidata**](https://www.wikidata.org/wiki/Wikidata:Main_Page), en la terminología de OpenRefine a este proceso se le llama reconciliación, pero yo voy a comentar [**OpenRefine**](https://openrefine.org/) desde un plano más general.

## Instalación en Debian y primera ejecución

El [código fuente](https://github.com/OpenRefine/openrefine.github.com) está disponible en GitHub bajo [licencia](https://github.com/OpenRefine/openrefine.github.com/blob/master/LICENSE.md) MIT, existen ya paquetes preparados para plataformas Windows, Mac y Linux, yo he usado el de Linux para instalarlo en mi computadora con sistema operativo Debian, el momento de escribir este artículo la versión es la 3.3.

**Instrucciones de instalación**:

Instrucciones de instalación y ejecución:

```bash
wget https://github.com/OpenRefine/OpenRefine/releases/download/3.3/openrefine-linux-3.3.tar.gz
tar -zxvf openrefine-linux-3.3.tar.gz
cd openrefine-3.3
./refine
```

Con conjuntos de datos pequeños no hay problema pero si queremos cargar _datasets_ muy grandes podemos definir la memoria que puede usar con `./refine -m 2048M`.

Ahora se puede abrir [http://127.0.0.1:3333/](http://127.0.0.1:3333/), lo primero es escoger el idioma, este el aspecto que tiene la primera vez que accedemos si todo funciona correctamente es el siguiente:

![](img/01.png)

## Nuevo proyecto

En el menú de la izquierda podemos encontrar tres opciones (además de cambiar el idioma):

- **Crear proyecto**: Esta opción carga un conjunto de datos (_dataset_), [**OpenRefine**](https://openrefine.org/) reconoce diferentes tipos de **archivos con datos estructurados**: CSV, JSON (JavaScript Object Notation ), Excel, XML, etc. Se pueden importar los datos usando diferentes formas (en algunos casos se requiere conexión a Internet), cargando un fichero local, desde una dirección Web, copiando y pegando del portapapeles, de una base de datos (PostgreSQL, MySQL, MariaDB) o de una hoja de calculo almacenada en Google Drive (una opción interesante que debo probar).
- **Abrir proyecto**: Permite abrir un proyecto en que hemos trabajado previamente.
- **Importar proyecto**: Con esta opción se pueden compartir proyectos con otras personas, incluye toda la historia de las transformaciones realizadas a los datos desde la creación del proyecto.

Me he descargado el típico CSV que he usado mucho en R [mtcars.csv](https://gist.github.com/seankross/a412dfbd88b3db70b74b#file-mtcars-csv) , lo he cargado en [**OpenRefine**](https://openrefine.org/). 

El _dataset_ [mtcars.csv](https://gist.github.com/seankross/a412dfbd88b3db70b74b#file-mtcars-csv) contiene modelos de coches y datos de los mismos, como se dice en R contiene 32 observaciones (registros) y 11 variables numéricas (columnas), en la [documentación del _dataset_](https://www.rdocumentation.org/packages/datasets/versions/3.6.2/topics/mtcars) se puede consultar que significa cada columna, en este caso hubiese podido perfectamente usar la URL para cargar los datos con [este enlace](https://gist.githubusercontent.com/seankross/a412dfbd88b3db70b74b/raw/5f23f993cd87c283ce766e7ac6b329ee7cc2e1d1/mtcars.csv) sin necesidad de descargarlos.

![](img/02.png)

Le damos un nombre al proyecto, yo me quedo con el nombre sugerido "mtcars cvs", una vez cargados los datos se pueden visualizar en una rejilla, dependiendo del tipo de datos con los que estemos trabajando en la parte inferior se pueden visualizar opciones del analizador (_parser_), para un CSV se puede seleccionar el delimitador de columnas, en este caso también está marcada la opción "Seleccionar primera(s)" ya que este fichero contiene los nombres de las columnas en la primera fila (es el comportamiento por defecto), existen más opciones pero no he tenido que cambiar ninguna de ellas.

Si todo está correcto vamos a "Crear proyecto".

![](img/03.png)

El _dataset_ contiene sólo 32 registros, se puede modificar el número de registros que se visualizan hasta 50 registros, el resto de resultados saldrán paginados. En la parte superior se muestran los nombres de columnas, cada una tiene un menú desplegable con opciones para trabajar con los datos.

## Manipulación de datos

Por defecto se visualizan todas las columnas, en algunos casos pueden ser demasiadas o no nos interesan todas, podemos **ocultar o visualizar de forma temporal las columnas que nos interesan**, usando el menú junto a cada una de ellas seleccionamos "Ver" para contraer varias columnas.

![](img/07.png)

Se puede **cambiar el orden de las columnas o moverlas de sitio** usando la opción "Editar columnas", también se pueden **eliminar columnas, dividirlas o combinar varias en una** y cambiar el nombre.

En cualquier momento se puede **deshacer o rehacer un cambio** usando el historial de cambios.

## Exportando un proyecto

En cualquier momento se puede exportar el proyecto: 

![](img/08.png)

La opción "Exportar proyecto" permite guardarlo en la computadora local o en "Google Drive" para compartirlo con otras personas o volver a importarlo más tarde. Se puede exportar como una tabla HTML, ideal si queremos incrustar la tabla en una Web, como CSV o Excel o una hoja de calculo ODF ([OpenDocument](https://es.wikipedia.org/wiki/OpenDocument)).

## Análisis de datos

Los datos se pueden ordenar por los valores de una columna.

![](img/09.png)



## Enlaces externos

1. [Wikipedia:Encuentros/Sesiones en línea WMES 2020](https://es.wikipedia.org/wiki/Wikipedia:Encuentros/Sesiones_en_l%C3%ADnea_WMES_2020).
2. [OpenRefine página oficial](https://openrefine.org/).
3. [Documentación](https://openrefine.org/documentation.html) en la Web oficial.
4. [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page).
5. [https://github.com/OpenRefine/OpenRefine/wiki/External-Resources](https://github.com/OpenRefine/OpenRefine/wiki/External-Resources).
6. [Wikidata:Herramientas/OpenRefine](https://www.wikidata.org/wiki/Wikidata:Tools/OpenRefine/es).
7. Libro ["Using OpenRefine - Ruben Verborgh, Max De Wilde"](https://www.packtpub.com/big-data-and-business-intelligence/using-openrefine), algunos contenidos se pueden previsualizar, gran parte de mi introducción proviene de aquí.
8. [Introducción a Google Refine](https://www.irekia.euskadi.eus/assets/attachments/2421/Tutorial4_IntroduccionGoogleRefine.pdf?1342079458) Por David Cabo. Me parece especialmente interesante porqué trabaja con datos de Open Euskadi sobre desaparecidos en la guerra civil.
9. [Tratar conjuntos de datos con OpenRefine - Junta de Andalucia](https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/tecnicos/usar-openrefine.html).
