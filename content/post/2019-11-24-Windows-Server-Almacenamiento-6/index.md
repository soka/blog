---
title: Almacenamiento local [borrador]
subtitle: Windows Server 2012
date: 2019-11-24
draft: true
description: Recursos Windows Server
tags: ["Windows","Sistemas","Windows Server","Windows Server 2012","DNS"]
---

## Administración de volúmenes

Para realizar pruebas he añadido un disco duro SATA de 5GB en mi máquina virtual Windows Server 2012 (virt-manager sobre Debian).

Ahora abro la aplicación "Administración de discos":

![](img/01.png)

Lo primero es inicializar el disco y ponerlo en línea como en la imagen:

![](img/02.png)

Ahora con el botón derecho creo un volumen simple, asigno una letra a la unidad de 1GB, con formato NTFS y le asigno un nombre cualquiera ("Vol01"). Así voy creando varios volúmenes.

![](img/03.png)

Puedo montar un volumen como un acceso directo sin necesidad de asignar una letra.

## Espacios de almacenamiento

Contenedores donde puedo añadir discos de diferentes tipos y capacidades para crear un disco duro virtual que uso de forma normal. En el administrador del servidor existe un apartado para administrar los grupos de almacenamiento, creo uno nuevo usando el asistente:




