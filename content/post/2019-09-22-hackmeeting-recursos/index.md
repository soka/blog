---
title: Recursos y herramientas para organizarse en comunidades digitales libres
subtitle: Lo que aprendí del Hackmeeting en Errekaleor
date: 2019-09-22
draft: true
description: Recursos de trabajo colaborativo y organización 
tags: ["Hackmeeting","Errekaleor","Aplicaciones","Tecnologías","Colaboración","Jornadas","Faircoin","Matrix","Etherpad"]
---

<!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="https://soka.gitlab.io/blog/post/2019-09-22-hackmeeting-recursos/" data-a2a-title="Lo que aprendí del Hackmeeting en Errekaleor">
<a class="a2a_button_twitter"></a>
<a class="a2a_button_facebook"></a>
<a class="a2a_button_email"></a>
<a class="a2a_button_linkedin"></a>
<a class="a2a_button_whatsapp"></a>
<a class="a2a_button_telegram"></a>
<a class="a2a_button_google_bookmarks"></a>
<a class="a2a_button_wordpress"></a>
</div>
<script>
var a2a_config = a2a_config || {};
a2a_config.locale = "es";
a2a_config.num_services = 22;
</script>
<script async src="https://static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->

![](images/EE7JskbWsAA4PvE.png)

Este post recoge algunas notas y ideas en bruto que pude tomar en el [**Hackmeeting**](https://es.hackmeeting.org/) con la esperanza de poder estudiar algunas de estas tecnologías más adelante. Antes de comenzar quisiera decir que no soy un experto en ninguno de estos temas y unicamente asistí como humilde oyente, totalmente agradecido a las personas que de forma desinteresada organizaron estas jornadas en el precioso barrio de [@Errekaleor](https://twitter.com/Errekaleor).

Creo que merece la pena revisar las tecnologías y plataformas en las que se apoyan para organizar estas jornadas. Creo que cada vez me interesa más como se auto organizan comunidades digitales.

## MediaWiki: Documentación colaborativa

La mayoría de la información una vez consensuada se vuelca en un sitio Web montado con [**MediaWiki**](https://www.mediawiki.org/wiki/MediaWiki) en este [enlace](https://es.hackmeeting.org/hm).

![](images/01.png)

Supongo que me repetiré un poco pero se puede jugar una [imagen de MediaWiki para Docker](https://hub.docker.com/_/mediawiki) `docker pull mediawiki`.

## Matrix: Mensajería y comunicación descentralizada**

Las conversaciones y interacciones directas se apoyan en un canal creado usando [**[Matrix]**](https://matrix.org/) [#hackmeeting:matrix.org](https://riot.im/app/#/room/#hackmeeting:matrix.org).

## Etherpad: Editor Web colaborativo en tiempo real

Los borradores de los textos se pueden "cocinar" en un editor colaborativo Web en tiempo real montado con [**Etherpad**](https://etherpad.org/).

![](images/screenshot.png)

En este [enlace](https://scalar.vector.im/etherpad/) se puede abrir un nuevo editor solamente proporcionando un título, puedes pasar el enlace a tus compañerxs y empezar a escribir, las opciones de formateo de texto son básicas, los cambios que realiza cada participante destacan en diferente color y un chat permite dialogar mientras trabajamos. El pad se puede incrustar en otros sitios dentro de un iframe HTML (por ejemplo en un canal de Matrix).

Una opción interesante es instalar tu propio servicio de [**Etherpad**](https://etherpad.org/) clonando el repositorio en [GitHub](https://github.com/ether/etherpad-lite), los requisitos son mínimos, Node.js y poco más, para probarlo se puede desplegar rápidamente con [Docker](https://hub.docker.com/r/etherpad/etherpad): `docker pull etherpad/etherpad`.

En resumen, **Etherpad** es espartáno y minimalista, pero cumple su función, existen otros servicios similares como el de [**Riseup**](https://riseup.net/es) en este [enlace](https://pad.riseup.net/), esta aplicación no es nueva, recuerdo que la conocí de la mano de un consultor y amigo en uno de mis trabajos.

![](images/04.png)

## Lista de correo en Sindominio.net

![](images/02.png)

Para no repetirme mucho [**Riseup**](https://riseup.net/es) ofrece aplicaciones similares como Email, [Chat](https://riseup.net/es/chat) (XMPP), [VPN](https://riseup.net/es/vpn), listas de correo, [Wikis](https://we.riseup.net/) privadas un [pad](https://pad.riseup.net/) o un servicio de intercambio de archivos ([share.riseup.net](https://share.riseup.net/)).

![](images/05.png)

## Retroshare: red F2F (Friend to Friend) completamente descentralizada**

>_"[Retroshare](https://retroshare.guifi.net/) creates encrypted connections to your friends. Nobody can spy on you. Retroshare is completely decentralized. This means there are no central servers. It is entirely Open-Source and free. There are no costs, no ads and no Terms of Service"_.

[Algunas indicaciones](https://es.hackmeeting.org/hm/index.php?title=2018/Herramientas_de_comunicaci%C3%B3n) obtenidas de la Web del Hackmeeting 2018.

![](images/06.png)

## FairCoin: La moneda digital

> "[FairCoin](https://fair-coin.org/es) implementa el intercambio justo de valor a escala global. La Prueba de Cooperación (PoC), su innovador mecanismo de blockchain, es el algoritmo único de consenso desarrollado para FairCoin. Requiere mucha menos energía que otras cadenas de bloques (blockchains), al tiempo que permite transacciones más rápidas. Estamos orgullosas de que FairCoin sea, en este momento, la criptomoneda más ecológica y resiliente."

![](images/03.png)


