---
title: Copias de seguridad y sincronización remota
subtitle: RSync en MS Windows
date: 2019-03-26
draft: false
description: Crear nuevos ficheros, añadir contenidos
tags: ["RSync", "Windows", "Aplicaciones", "Backup", "Windows"]
---

[RSync](https://es.wikipedia.org/wiki/Rsync) es una aplicación libre creada para sistemas GNU/Linux y portado a MS Win (donde normalmente se usa [Robocopy](https://es.wikipedia.org/wiki/Robocopy) o xcopy). **RSync** permite sincronizar datos de forma incremental entre maquinas remotas o en la propia máquina local. Ofrece también compresión de los datos transmitidos (zlib) y cifrado del canal SSH.

Funcionando como un servicio o demonio en un servidor **escucha por defecto en el puerto TCP 873**.

# RSync en MS Win

## DeltaCopy: Servidor y cliente

![](images/deltacopy_01.PNG)

Es un port de Linux para Windows usando las librerías [Cygwin](https://www.cygwin.com/), requiere instalación previa (por defecto en `C:\DeltaCopy`) y se puede administrar mediante una interface gráfica, la imagen inferior corresponde a la pantalla de administración del servidor (`DeltaS.exe`):

![](images/deltacopy_02.PNG)

Una vez instalado como servicio y arrancado en la pestaña "Virtual Directories" se configuran una o mas rutas para guardar los archivos (por defecto ya viene configurado para usar `c:\Backup`).

Podemos comprobar con una aplicación como [Nmap](https://nmap.org/) que el puerto TCP 873 está abierto a la escucha de los clientes.

Ahora debemos lanzar el cliente `DeltaC.exe` e introducir los datos de la conexión y la carpeta o archivos locales que queremos sincronizar.

![](images/deltacopy_03.PNG)

A continuación usando el programador de tareas de Win (botón "Modify Schedule") ajustamos la hora de inicio de la sincronización, la periodicidad o frecuencia y otros parámetros.

También podemos configurar las notificaciones vía correo electrónico.

# Enlaces externos

- Wikipedia [Rsync](https://es.wikipedia.org/wiki/Rsync).
- [DeltaCopy - Rsync for Windows - About my IP](http://www.aboutmyip.com/AboutMyXApp/DeltaCopy.jsp).
- https://www.rsync.net/resources/howto/windows_rsync.html
- http://www.aboutmyip.com/AboutMyXApp/DeltaCopy.jsp
- http://www.opbyte.it/grsync/screenshot.html
