---
title: Como sortear la censura en Internet
subtitle: Manipulación de DNS de ISPs
date: 2019-10-17
draft: false
description: 
tags: ["Censura","DNS","ISP","tsunamidemocratic"]
---

La Web tsunamidemocratic.cat está redireccionada [http://82.223.97.47](http://82.223.97.47) presumiblemente por los ISP que filtran el tráfico, ls ip real es 104.18.37.39 de Cloudfare.

## Usando una VPN

Para Firefox se puede usar el add-on [CyberGhost VPN Free Proxy](https://addons.mozilla.org/en-US/firefox/addon/cyberghost-vpn-free-proxy/), es gratuito y permite seleccionar el país de salida a Internet, se puede comprobar que la IP de origen cambia usando un servicio como [https://www.whatismyip.com/es/](https://www.whatismyip.com/es/):

![](images/05.png)

Compruebo que la Web [http://tsunamidemocratic.cat](http://tsunamidemocratic.cat) es accesible.

Si no quieres instalar nada puedes usar un servicio como [https://hide.me/es/proxy](https://hide.me/es/proxy) (navegador proxy anónimo).

![](images/06.png)

## DNS-over-HTTPS (DoH)

DNS mediante HTTPS (DoH, siglas en inglés de DNS over HTTPS) es un protocolo de seguridad para **realizar una resolución remota del sistema de nombres de dominio (DNS) a través del protocolo HTTPS**. Uno de los objetivos del método es aumentar la privacidad y la seguridad de los usuarios mediante la prevención de las escuchas ilegales y la manipulación de los datos del DNS a través de ataques de intermediario ([Wikipedia](https://es.wikipedia.org/wiki/DNS_mediante_HTTPS)).

- Escribir about:config en la barra de la URL del navegador
- network.trr.mode: Ponemos  2 (DoH is enabled, and regular DNS works as a backup).
- network.trr.uri: URL del DNS compatible DoH usamos "https://mozilla.cloudflare-dns.com/dns-query".
- network.trr.bootstrapAddress: IP resolución DNS Cloudfare "1.1.1.1" (opcional).

Ahora lo probamos [https://1.1.1.1/help](https://1.1.1.1/help):

![](images/03.png)

Accedo a [http://tsunamidemocratic.cat](http://tsunamidemocratic.cat) y me redirige correctamente a [https://tsunamidemocratic.github.io/](https://tsunamidemocratic.github.io/):

![](images/04.png)

## Cambiar los DNSs en el router de casa

En lugar de usar los DNSs de tu ISP si puedes administrar el router de tu casa cambialos por los de Cloudfare utilizando las IPs 1.1.1.1 y 1.0.0.1. La empresa promete borrar todos los registros de consultas DNS a las 24 horas de manera que no registraría por dónde navegan sus usuarios. 

## Usando TOR

Usando TOR y el Add-on Proxy Switcher para Firefox:

```sh
# apt install tor
```

Comprobar que funciona [https://check.torproject.org/](https://check.torproject.org/):

![](images/07.png)

# Enlaces

- ["Introducing DNS Resolver, 1.1.1.1 (not a joke)"](https://blog.cloudflare.com/dns-resolver-1-1-1-1/).
- ["Running a DNS over HTTPS Client"](https://developers.cloudflare.com/1.1.1.1/dns-over-https/cloudflared-proxy/).
- ["Cloudflare presenta su DNS 1.1.1.1: más rápido que el de Google y con la promesa de respetar tu privacidad"](https://www.xataka.com/seguridad/cloudflare-presenta-su-dns-1-1-1-1-mas-rapido-que-el-de-google-y-con-la-promesa-de-respetar-tu-privacidad).