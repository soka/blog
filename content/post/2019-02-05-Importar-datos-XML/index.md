---
title: Importar datos XML en R
subtitle: Trabajando con ficheros XML 
date: 2019-02-04
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico","XML"]
---

Como en la anterior ocasión con los ficheros XLSX también para trabajar con ficheros XML debemos instalar y cargar un paquete:

```R
install.packages("XML")
library(XML)
```
Ahora parseamos la estructura del XML pasandole la ruta del fichero a la función `xmlTreeParse()`:

```R
datos.xml <- xmlTreeParse("datos.xml")
```

El contenido del fichero XML de ejemplo es muy sencillo:

![](images/datos-xml.PNG)

Podemos comprobar que el objeto devuelto es un documento XML:

```R
> class(datos.xml)
[1] "XMLDocument"         "XMLAbstractDocument"
```

Ahora para desplazarnos por la estructura XML nos posicionamos en el nodo raíz con `xmlRoot()`:

```R 
rootNode <- xmlRoot(datos.xml)
```

Obtenemos los nombres de las columnas:

```R
> names(rootNode[[1]])
  Nombre     Edad   Genero 
"Nombre"   "Edad" "Genero"
```

Imprimimos el contenido de un nodo por su posición:

```R
> print(rootNode[5])
$row
<row>
 <Nombre>Carmen</Nombre>
 <Edad>23</Edad>
 <Genero>F</Genero>
</row>

attr(,"class")
[1] "XMLNodeList"
```

Convertimos la estructura en un DF:

```R 
> datos <- xmlSApply(rootNode, function(x) xmlSApply(x, xmlValue))
> df <- data.frame(t(datos), row.names=NULL)
> df
   Nombre Edad Genero
1  Carlos   23      M
2 Alberto   25      M
3    Juan   21      M
4   Maria   27      F
5  Carmen   23      F
6    Alba   22      F
```

# Ejercicios

* r / intro / 10-importar-xml / [importar-xml.R](https://gitlab.com/soka/r/blob/master/intro/10-importar-xml/importar-xml.R).
* r / intro / 10-importar-xml / [datos.xml](https://gitlab.com/soka/r/blob/master/intro/10-importar-xml/datos.xml). 

# Enlaces

**Enlaces externos:**

* ["xmlTreeParse - RDocumentation"](https://www.rdocumentation.org/packages/XML/versions/3.98-1.16/topics/xmlTreeParse).
* ["xmlTreeParse | R Handbook"](https://rhandbook.wordpress.com/tag/xmltreeparse/).
* ["xmlRoot function | R Documentation"](https://www.rdocumentation.org/packages/XML/versions/3.98-1.16/topics/xmlRoot).
* ["Adding and removing columns from a data frame"](http://www.cookbook-r.com/Manipulating_data/Adding_and_removing_columns_from_a_data_frame/).