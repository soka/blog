---
title: Más trucos con las tarjetas de Trello
subtitle: La pizarra digital inspirada en kanban
date: 2019-07-04
draft: false
description: Creando tarjetas en Trello
tags:
  [
    "Trello",
    "Talleres",
    "Cursos",
    "Aplicaciones",
    "Trello - Introducción",
    "Trello - Tarjetas",
    "Kanban",
    "Metodologías",
    "Gestión de Proyectos",
    "Herramientas",
  ]
---

![](img/logo-trello.png)

## Añadir listas de verificación a las tarjetas

Las _checklists_ sirven para hacer un **seguimiento de las tareas que haya dentro de una tarjeta**. Se pueden añadir una gran variedad de _checklists_ en una sola tarea.

**Añade un _checklist_** mediante el botón "Añadir checklist" que encontrarás en la sección "Añadir", en la parte posterior de la tarjeta. Dale un título (o deja el nombre predeterminado) y luego haz clic en "Añadir"

![](img/07.gif)

**Añade nuevos elementos a un _checklist_** mediante la opción, "Añadir elemento de checklist" que encontrarás en cualquier checklist. Para **marcar un elemento como completado o incompleto,** haz clic en la casilla que encontrarás a la izquierda del título. Si quieres cambiar el nombre de una checklist o de uno de los elementos de la checklist, haz clic en el título, cambia el texto y guarda los cambios. Para **cambiar el orden de un elemento de un _checklist_**, arrástralo hasta la posición deseada.

Para **eliminar una lista de verificación que ya no necesita**, haga clic en "Eliminar ..." a la derecha del nombre de la lista de verificación.

**Complete los elementos individuales de la lista de verificación** marcándolos mediante la casilla de verificación al comienzo de la línea. Una vez que se hayan completado todos los elementos de la lista de verificación, la barra de progreso cambiará a verde y la credencial de la lista de verificación en el frente de la tarjeta también se volverá verde.

![](img/08.gif)

## Añadir fecha de vencimiento a una tarjeta

Se puede [**establecer una fecha y hora de vencimiento**](https://help.trello.com/article/794-adding-due-dates-to-cards) en una tarjeta. Puedes añadir una fecha de vencimiento desde la parte posterior de la tarjeta, en el menú "Añadir" de la barra lateral de la tarjeta. También puedes añadir una fecha a través del menú de la tarjeta, en la parte frontal, o si pasas el puntero del ratón sobre una tarjeta y pulsas la tecla "d". Otra opción es acceder al menú de edición rápida. Para ello, tienes que pasar el ratón por encima de la tarjeta y hacer clic en el icono del lápiz.

![](img/04.gif)

## Archivar tarjetas

Para archivar tarjetas, haz clic en la opción "Archivar tarjetas" en el menú "Acciones" que encontrarás en la parte posterior de la tarjeta. También puedes seleccionar la opción "Archivar" desde el menú de la tarjeta (para ello, haz clic en el icono con un lápiz que aparece al pasar el ratón por encima). También puedes archivar la tarjeta mediante la tecla "c" cuando pases el ratón por encima de la tarjeta.

![](img/01.gif)

## Borrar tarjetas

Para borrar una tarjeta, primero debes archivarla. Haga clic en la tarjeta para abrirla, luego seleccione "Archivar" en la parte inferior derecha. Después de archivar, una nueva opción "Eliminar" estará disponible. Debido a que la eliminación de una tarjeta es irrecuperable y permanente, esto requiere un clic de confirmación.

La eliminación de una tarjeta es permanente y las tarjetas eliminadas no se pueden restaurar. S

## ¡Crear tarjetas por correo electrónico!

![](img/03.gif)

Las tarjetas se pueden crear a partir de un correo electrónico. Para obtener tu dirección de correo electrónico para una tarjeta abrir el menú en la barra lateral derecha y seleccione "Más", y luego "Correo electrónico para esta tarjeta". Desde aquí, puede seleccionar en qué parte de la pizarra se creará la nueva tarjeta. Puede hacer clic en el nombre de la lista actual ('Ninguno' en el ejemplo siguiente) para cambiarlo a una lista diferente.

Usted también puede tener su dirección de correo electrónico de ese foro por correo electrónico. Tenga en cuenta que la dirección de correo electrónico es único para cada tabla y miembro del consejo. Además, los comentarios deben estar habilitadas para la placa con el fin de generar la dirección de correo-a-bordo.

La tarjeta se mostrará en los próximos minutos después de que se envió el correo electrónico.

Cuando envíes por correo electrónico una tarjeta a Trello, **puedes añadir inmediatamente archivos adjuntos, etiquetas, y miembros a la tarjeta**. Además, también puedes añadir el título y la descripción.

- El **asunto del correo electrónico** se convertirá entonces en el título de la tarjeta.
- El **cuerpo del mensaje** se convertirá en la descripción de la tarjeta.
- Los **archivos adjuntos** que tenga el correo electrónico también se añadirán a la tarjeta.
- **Etiquetas**: En el tema, añadir #labelname, #labelcolor, o #labelnumber
  - Si su etiqueta consta de dos o más palabras, en la línea de asunto o bien unirse a las palabras o el uso de relieve entre las palabras. Por ejemplo, si la etiqueta se denomina "hacer" en la línea de asunto del correo electrónico o bien introducir #ToDo o #To_Do que la etiqueta se muestra correctamente en su tarjeta.
  - Si tiene varias etiquetas en una tabla con el mismo color o del mismo nombre, enviar por correo electrónico un tablero con #color o #nombre sólo añadir la primera etiqueta con ese color o el nombre de la tarjeta.
- **Miembros**: En el tema, añadir @nombredeusuario. Los miembros también pueden ser añadidos poniéndoles @nombredeusuario en el cuerpo del correo electrónico en su misma línea. Si envías un correo electrónico a Trello e incluyes direcciones de correo electrónico de otros usuarios Trello como "a" o "CC", Trello también los añadirá como miembros de la tarjeta.

## Enviar comentarios por correo electrónico a una tarjeta

Cada tarjeta también tiene su propio correo electrónico: enviar una nota a esta dirección hará que su correo electrónico aparezca como un comentario. Puede encontrar el correo electrónico de una tarjeta debajo del botón "Compartir" en la parte posterior de la tarjeta, en la esquina inferior derecha.

![](img/02.gif)

## Buscar tarjetas (en todos los tableros)

**Para buscar tarjetas concretas de un único tablero usa los filtros de tarjeta**.

Trello le da la capacidad de [buscar tarjetas en todos los tableros](https://help.trello.com/article/808-searching-for-cards-all-boards).

Los Operadores de búsqueda refinan tu búsqueda para ayudarte a encontrar tarjetas específicas y crear listas altamente personalizadas. Trello sugerirá operadores para ti mientras escribes, pero aquí hay una lista completa para tener en cuenta.

- @name - Muestra las **tarjetas asignadas a un miembro**. Cuando empieces una búsqueda con @, Trello te sugerirá algunos miembros. member: realiza la misma función. Si escribes "@me", la búsqueda sólo incluirá tus tarjetas.

![](img/05.gif)

## Aplicar filtros

![](img/06.gif)

## Enlaces externos
