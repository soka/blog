---
title: Operaciones con Data Frames
subtitle: Carga de datos, origen de datos y limpieza
date: 2019-04-22
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico", "R - Datos"]
---

![](images/r-logo.PNG)

Partimos del CSV de ejemplo que cargamos en el artículo ["Descarga de un fichero de Internet"](https://soka.gitlab.io/blog/post/2019-04-22-r-descarga-fichero-internet/).

```R
setwd("~/GitLab/r/intro/40-data-frames-operaciones")
camarasData <- read.csv("data/camaras.csv")
```

![](images/dataframe-cheatsheet.PNG)

## Insertar una nueva columna

Vamos a añadir una nueva columna al `data.frame` usando la función `cbind`. Los datos de la nueva columna los generamos de forma aleatoria con `rnorm` (generamos 80 observaciones en torno al 0 siguiendo la distribución normal o campana de Gauss).

```R
camarasData <- cbind(camarasData,rnorm(80))
```

Si visualizamos la variable `camarasData` en RStudio R le asigna como nombre a la nueva columna `rnorm(80)`, realizamos una pequeña modificación para poner un nombre de nuestra elección a la nueva columna:

```R
camarasData <- cbind(camarasData,NuevaCol=rnorm(80))
```

Siempre podemos volver a cambiar el nombre de las columnnas con `names`:

```R
names(camarasData)[7] <- 'rnorm'
```

Ahora comprobamos los nombres de las columnas:

```R
> names(camarasData)
[1] "address"      "direction"    "street"       "crossStreet"  "intersection"
[6] "Location.1"   "rnorm"
```

## Subsetting

Podemos acceder a subconjuntos del `data.frame`, por ejemplo usando su índice numérico o el nombre de la columna:

```R
camarasData[,1]
# Equivalente
camarasData[,'address']
```

También podemos seleccionar las dos primeras filas de una columna:

```R
camarasData[1:2,'address']
```

Bajo unas condiciones:

```R
camarasData2 <- camarasData[camarasData$direction == 'N/B' | camarasData$direction == 'S/B',]
```

Comprobamos que de los 80 registros sólo se han guardado algunos (34 filas y 7 columnas):

```R
> dim(camarasData2)
[1] 34  7
```

![](images/01.PNG)

## Ordenar

Ordenación de forma creciente basado en los valores de una columna

```R
sort(camarasData$rnorm)
```

De forma decreciente

```R
sort(camarasData\$rnorm,decreasing = T)
```

Una expresión equivalente y guardando el resultado:

```R
camarasData <- camarasData[order(camarasData$rnorm,decreasing = T),]
```

## Información

```R
head(camarasData,n=3)
# Dimensiones filas x columnas
dim(camarasData)
# Las 5 últimas
tail(camarasData,n=5)
# Resumen: Valores más repetidos, direction distribución en valores, cuando es una columna
# numérica calcula el mínimo, media, ....
summary(camarasData)
# Tipos de variables en cada columna
str(camarasData)
```

## Tabulación cruzada

Ahora vamos a trabajar con un _dataset_ que viene con R, "UCBAdmissions", cargamos el dataset y lo convertimos de tipo _table_ en un _data.frame_ para trabajar más cómodamente:

```R
> data("UCBAdmissions")
> class(UCBAdmissions)
[1] "table"
> ucbadmin <- as.data.frame(UCBAdmissions)
> class(ucbadmin)
[1] "data.frame"
```

24 observaciones de 4 variables.

![](images/ucbadmin-01.PNG)

Realizamos un resumen:

```R
> summary(ucbadmin)
      Admit       Gender   Dept       Freq
 Admitted:12   Male  :12   A:4   Min.   :  8.0
 Rejected:12   Female:12   B:4   1st Qu.: 80.0
                           C:4   Median :170.0
                           D:4   Mean   :188.6
                           E:4   3rd Qu.:302.5
                           F:4   Max.   :512.0
```

El comando `xtabs` crea una tabla de contingencia de los factores clasificados transversalmente.

```R
> xt <- xtabs(Freq ~ Gender + Admit, data = ucbadmin)
> xt
        Admit
Gender   Admitted Rejected
  Male       1198     1493
  Female      557     1278
```

El resultado es una tabla cruzada.

## Ejercicios

- r \ intro \ 40-data-frames-operaciones \ [data-frames-operaciones.R](https://gitlab.com/soka/r/blob/master/intro/40-data-frames-operaciones/data-frames-operaciones.R).

## Enlaces internos

- ["Data Frames en R"](https://soka.gitlab.io/blog/post/2019-03-12-r-data-frame/).
- ["Añadir una nueva columna cbind()"](https://soka.gitlab.io/blog/post/2019-03-01-ainadir-nueva-columna-df-en-r/).

## Enlaces externos

- ["Obtención aleatoria de valores: `rnorm` – Picando con R"](https://picandoconr.wordpress.com/2016/07/14/obtencion-aleatoria-de-valores-rnorm/).
- ["Distribución normal - Wikipedia, la enciclopedia libre"](https://es.wikipedia.org/wiki/Distribuci%C3%B3n_normal).
- Vídeo ["01 Qué es la distribución normal"](https://www.youtube.com/watch?v=phY8Z9-TXCY).

```

```
