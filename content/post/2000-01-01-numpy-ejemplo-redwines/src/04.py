#!/usr/bin/env python3

import numpy as np

wines = np.genfromtxt("winequality-red.csv", 
                        delimiter=";", 
                        skip_header=1)

print(wines)

print('Tipo de objeto: ',type(wines)) # <class 'numpy.ndarray'>
print('Tipo de registros internos: ',type(wines[0])) # <class 'numpy.ndarray'>
print('Tipo de los valores: ',wines.dtype) # float64

print('Forma de la matriz: ',wines.shape) # (1599, 12)
print('Filas: ',wines.shape[0]) # 1599
print('Columnas: ',wines.shape[1]) # 12
print('Tamaño de la matriz (filas x columnas): ',wines.size) # 19188
print('Dimensiones: ',wines.ndim)  # 2

calidades=wines[:,-1:]
print('Suma calidades: ',np.sum(calidades)) # 9012.0
print('Máxima calidad: ',np.max(calidades)) # 8.0
print('Mínima calidad: ',np.min(calidades)) # 3.0

