#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt





x = np.array([[0, 2], [1, 1], [2, 0]]).T
print(x)
'''
[[0 1 2]
 [2 1 0]]
'''

# Notese como las dos variables se correlación en sentido inverso, mientras una crece [0,1,2] la otra decrece [2 1 0]

print(np.cov(x))
'''
[[ 1. -1.]
 [-1.  1.]]
'''

# - [0][0] es la covarianza entre X0 y X0 (fila 0)
# - [0][1] Es la covarianza entre X0 y X2
# - [1][0] es la covarianza entre X2 y X1
# - [1][1] es la covarianza entre X2 y X2

# En este caso la correlación entre X0 y X2 es inversa, Esto es, a mayores valores de X0, en promedio tenemos menores valores de X1 y viceversa.

plt.figure(figsize=(10,7))
plt.plot(x[0],x[1],'o',markersize=2)
plt.show()

# En la gráfica podemos ver que la nube de puntos está dispersa 

