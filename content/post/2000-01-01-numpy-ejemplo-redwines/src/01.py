#!/usr/bin/env python3

import csv
with open('winequality-red.csv', 'r') as f:
    wines = list(csv.reader(f, delimiter=';'))

print(wines)
# Imprimimos cabecera
print(wines[0])
# Accedemos a un elemento concreto de un registro
print(wines[1][1])
# Nº de registros 
print(len(wines)) # 1600
print(len(wines[0])) # Nº de campos
# Imprimos los 3 primeros registros 
print(wines[:3])