#!/usr/bin/env python3

import numpy as np

red_wines = np.genfromtxt("winequality-red.csv", delimiter=";", skip_header=1)
white_wines = np.genfromtxt("winequality-white.csv", delimiter=";", skip_header=1)

print(white_wines.shape) # (4898, 12)

all_wines = np.vstack((red_wines, white_wines))
print(all_wines.shape) # (6497, 12)


