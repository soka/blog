#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

wines = np.genfromtxt("winequality-red.csv", delimiter=";", skip_header=1)

qualities = wines[:,-1:]
print(np.histogram(qualities,bins=[1,2,3,4,5,6,7,8,9,10]))

'''
# HISTOGRAMAS 
# En estadística, un histograma es una representación gráfica de una variable en forma de barras, donde la superficie de cada barra es proporcional a la frecuencia de los valores representados
print(np.histogram(qualities))
# (array([ 10,   0,  53,   0, 681,   0, 638,   0, 199,  18], dtype=int64), array([3. , 3.5, 4. , 4.5, 5. , 5.5, 6. , 6.5, 7. , 7.5, 8. ]))
'''

plt.hist(qualities,bins=[1,2,3,4,5,6,7,8,9,10])
plt.title("Histograma de las calidades")
plt.xlabel('Distribución calidades 1-10')
plt.ylabel('Nº Observaciones')
plt.show() 

