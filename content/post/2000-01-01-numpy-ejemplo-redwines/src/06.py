#!/usr/bin/env python3

import numpy as np

wines = np.genfromtxt("winequality-red.csv", delimiter=";", skip_header=1)

mask = wines[:,11] > 7
high_quality=wines[mask,:]
print(high_quality)
print('Nº registros con calidad > 7 : ',high_quality.shape[0]) # 18

