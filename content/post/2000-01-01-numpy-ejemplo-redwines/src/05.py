#!/usr/bin/env python3

import numpy as np

wines = np.genfromtxt("winequality-red.csv", delimiter=";", skip_header=1)

calidades = wines[:,-1:]
print('Media: ',calidades.mean()) # Media:  5.6360225140712945
print('Mediana: ',np.median(calidades)) # Mediana:  6.0
print('Varianza : ',np.var(calidades)) # Varianza :  0.6517605398308277
print('Desviación estándar: ',np.std(calidades)) # Desviación estándar:  0.8073168769639513
