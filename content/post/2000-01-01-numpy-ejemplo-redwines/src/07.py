#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

wines = np.genfromtxt("winequality-red.csv", delimiter=";", skip_header=1)

alcohol_qualities = wines[:,-2:]
alcohol_qualities=alcohol_qualities.T
print(np.cov(alcohol_qualities))

# La función cov retorna una matriz donde 
# - [0][0] es la covarianza entre a1 y a1
# - [0][1]Es la covarianza entre a1 y a2
# - [1][0] es la covarianza entre a2 y a1
# - [1][1] es la covarianza entre a2 y a2
'''
[[1.1356474  0.40978901]
 [0.40978901 0.6521684 ]]
'''
'''
plt.figure(figsize=(10,7))
plt.plot(x,y,'o',markersize=2)
plt.show()
'''

x = wines[:,-1:].T
x=x[0]
print(x)
y = wines[:,10]
print(y)
plt.figure(figsize=(10,7))
plt.title('Relación entre calidad y grados alcohol')
plt.xlabel('Calidad')
plt.ylabel('Grados alcohol')
plt.scatter(x, y)
plt.show()









