---
title: Análisis de datos con NumPy
subtitle: NumPy
date: 2000-01-01
draft: false
description: NumPy
tags: ["Python","NumPy","Pruebas","Evaluación"]
---

<!-- vscode-markdown-toc -->
* 1. [Descripción del dataset](#Descripcindeldataset)
* 2. [Lectura del origen de datos](#Lecturadelorigendedatos)
* 3. [Inspección del objeto ndarray](#Inspeccindelobjetondarray)
	* 3.1. [Forma](#Forma)
	* 3.2. [Tamaño de la matriz](#Tamaodelamatriz)
	* 3.3. [Dimensiones](#Dimensiones)
* 4. [Análisis exploratorio de datos básico](#Anlisisexploratoriodedatosbsico)
	* 4.1. [Suma de valores](#Sumadevalores)
	* 4.2. [Máximo](#Mximo)
	* 4.3. [Mínimo](#Mnimo)
* 5. [Análisis estadístico](#Anlisisestadstico)
	* 5.1. [Media](#Media)
	* 5.2. [Mediana](#Mediana)
	* 5.3. [Varianza](#Varianza)
	* 5.4. [Desviación estándar](#Desviacinestndar)
* 6. [Vinos mejor valorados](#Vinosmejorvalorados)
* 7. [¿Hay alguna relación entre el grado de alcohol y la calidad?](#Hayalgunarelacinentreelgradodealcoholylacalidad)
	* 7.1. [Histograma](#Histograma)
		* 7.1.1. [Computar un histograma en NumPy](#ComputarunhistogramaenNumPy)
		* 7.1.2. [Representación gráfica del histograma con Matplotlib](#RepresentacingrficadelhistogramaconMatplotlib)
* 8. [Enlaces externos](#Enlacesexternos)
* 9. [Recursos](#Recursos)
* 10. [Enlaces externos](#Enlacesexternos-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

![](img/05.png)

Somos una empresa Portuguesa responsable de una conocida marca de vino, con ánimo de de mejorar la calidad de nuestra oferta hemos recogido datos de diferentes atributos ("pH","alcohol",etc.) de nuestro vino junto con la percepción que nuestros clientes tienen de su calidad ("quality" puntuación entre 1 y 10). Ahora con esos datos nos dirigimos a ti como experto analista de datos para facilitar la obtención de algunas conclusiones que nos permitan mejorar nuestro proceso de producción.

##  1. <a name='Descripcindeldataset'></a>Descripción del dataset

Los datos se descargaron de [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/index.php) y están disponibles [aquí](https://archive.ics.uci.edu/ml/datasets/Wine+Quality). Estas son las primeras filas del archivo [winequality-red.csv](https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv), que usaremos a lo largo de este tutorial.

Vista parcial de la cabecera y varios registros:

![](img/04.png)

Se ha almacenado el archivo en el repositorio junto con este artículo:

* [src/winequality-red.csv](src/winequality-red.csv)

Los datos están en lo que voy a llamar formato ssv (**valores separados por punto y coma**): cada registro está separado por un punto y coma (;) y las filas están separadas por una nueva línea. Hay **1600** filas en el archivo, incluida una fila de encabezado y **12 columnas**.

##  2. <a name='Lecturadelorigendedatos'></a>Lectura del origen de datos

Lo primero evidentemente es cargar nuestro _dataset_ en una estructura de datos, debemos leer el archivo CSV usando NumPy, hemos **excluido la primera fila con el encabezado** para facilitar el tratamiento posterior de los datos.

[src/04.py](src/04.py)

```python
import numpy as np

wines = np.genfromtxt("winequality-red.csv", 
                        delimiter=";", 
                        skip_header=1)

print(wines)
``` 

Salida:

```python
[[ 7.4    0.7    0.    ...  0.56   9.4    5.   ]
 [ 7.8    0.88   0.    ...  0.68   9.8    5.   ]
 [ 7.8    0.76   0.04  ...  0.65   9.8    5.   ]
 ...
 [ 6.3    0.51   0.13  ...  0.75  11.     6.   ]
 [ 5.9    0.645  0.12  ...  0.71  10.2    5.   ]
 [ 6.     0.31   0.47  ...  0.66  11.     6.   ]]
```

El resultado es un objeto instanciado de la clase [numpy.ndarray](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html#numpy-ndarray) que contiene a su vez una serie de objetos [numpy.ndarray](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html#numpy-ndarray) para representar los registros o filas. los valores serán de tipo flotante ([numpy.float64](https://numpy.org/doc/stable/reference/arrays.scalars.html#numpy.float64) es una implementación propia de tipos _float_, en este caso además tiene una precisión de 64 bit):

```python
print('Tipo de objeto: ',type(wines)) # <class 'numpy.ndarray'>
print('Tipo de registros internos: ',type(wines[0])) # <class 'numpy.ndarray'>
print('Tipo de los valores: ',wines.dtype) # float64
```

##  3. <a name='Inspeccindelobjetondarray'></a>Inspección del objeto ndarray 

###  3.1. <a name='Forma'></a>Forma 

Imprimir la cantidad de filas y columnas (1599, 12) en nuestros datos usando la propiedad de forma (_shape_) ([numpy.ndarray.shape](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.shape.html#numpy-ndarray-shape)) de las matrices NumPy:

```python
print('Forma de la matriz: ',wines.shape) # (1599, 12)
print('Filas: ',wines.shape[0]) # 1599
print('Columnas: ',wines.shape[1]) # 12
```

###  3.2. <a name='Tamaodelamatriz'></a>Tamaño de la matriz 

Tamaño de la matriz, el método [numpy.ndarray.size](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.size.html#numpy-ndarray-size) nos informa del número de elementos de un arreglo, tratándose de una matriz será el número de filas x columnas (mxn):

```python
print('Tamaño de la matriz (filas x columnas): ',wines.size) # 19188
```

###  3.3. <a name='Dimensiones'></a>Dimensiones

El método [numpy.ndarray.ndim](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.ndim.html#numpy-ndarray-ndim) devuelve un valor con el número de dimensiones, en nuestro caso de estudio es evidente que debería ser 2 tratándose de un arreglo 2-D o matriz:

```python
print('Dimensiones: ',wines.ndim)  # 2
```

##  4. <a name='Anlisisexploratoriodedatosbsico'></a>Análisis exploratorio de datos básico

Vamos a obtener un resumen básico de las calidades para tener una descripción general del _dataset_. Usaremos funciones de NumPy y _slicing_ para filtrar previamente la última columna con las calidades.

###  4.1. <a name='Sumadevalores'></a>Suma de valores

[numpy.ndarray.sum](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.sum.html#numpy-ndarray-sum)

```python
print('Suma calidades: ',np.sum(wines[:,-1:])) # 9012.0
```

###  4.2. <a name='Mximo'></a>Máximo 

[numpy.ndarray.max](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.max.html#numpy-ndarray-max)

```python
print('Máxima calidad: ',np.max(wines[:,-1:])) # 8.0
```

###  4.3. <a name='Mnimo'></a>Mínimo 

[numpy.ndarray.min](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.min.html#numpy-ndarray-min)

```python
print('Mínima calidad: ',np.min(wines[:,-1:])) # 3.0
```

##  5. <a name='Anlisisestadstico'></a>Análisis estadístico

Realizaremos un análisis estadístico básico de las calidades. 

La percepción media que los clientes tienen de nuestros vinos no es muy buena, es un aprobado justo, respecto a la desviación estándar, es un valor bajo que debería indicar que las valoraciones son bastante uniformes siempre cercanas a la media (se puede ver mejor en la gráfica del histograma más abajo).

[src/05.py](src/05.py)

###  5.1. <a name='Media'></a>Media 

[numpy.ndarray.mean](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.mean.html#numpy-ndarray-mean)

```python
print('Media: ',qualities.mean()) # Media:  5.6360225140712945
```

###  5.2. <a name='Mediana'></a>Mediana 

[numpy.median](https://numpy.org/doc/stable/reference/generated/numpy.median.html#numpy-median)

```python
print('Mediana: ',np.median(qualities)) # Mediana:  6.0
```

###  5.3. <a name='Varianza'></a>Varianza

[numpy.var](https://numpy.org/doc/stable/reference/generated/numpy.var.html#numpy-var)

```python
print('Varianza : ',np.var(qualities)) # Varianza :  0.6517605398308277
```

###  5.4. <a name='Desviacinestndar'></a>Desviación estándar 

[numpy.std](https://numpy.org/doc/stable/reference/generated/numpy.std.html#numpy-std)

```python
print('Desviación estándar: ',np.std(qualities)) # Desviación estándar:  0.8073168769639513
```

##  6. <a name='Vinosmejorvalorados'></a>Vinos mejor valorados

[src/06.py](src/06.py)

Ahora queremos obtener sólo los registros (todos los campos) de aquellos vinos que tengan una calificación mayor que 7. 

Podemos usar la indexación booleana para filtrar estos registros.

```python
mask = wines[:,11] > 7
high_quality=wines[mask,:]
print(high_quality)
```

Número de registros del resultado:

```python
print('Nº registros con calidad > 7 : ',high_quality.shape[0]) # 18
```

##  7. <a name='Hayalgunarelacinentreelgradodealcoholylacalidad'></a>¿Hay alguna relación entre el grado de alcohol y la calidad?

https://www.odiolaestadistica.com/estadistica-python/covarianza/

Sospechamos que la graduación alcohólica alta afecta de forma negativa sobre la calidad de nuestros vinos, queremos calcular la covarianza entre el alcohol y la calidad para ver si existe una relación de dependencia entre ambas variables.

La covarianza es un valor que indica el **grado de variación conjunta de dos variables aleatorias respecto a sus medias**. Es el dato básico para determinar si existe una **dependencia entre ambas variables** y además es el dato necesario para estimar otros parámetros básicos, como el coeficiente de correlación lineal o la recta de regresión.

Cuando los valores altos de una de las variables suelen mayoritariamente corresponderse con los valores altos de la otra, y lo mismo se verifica para los pequeños valores de una con los de la otra, se corrobora que tienden a mostrar comportamiento similar lo que se refleja en un valor positivo de la covarianza. Por el contrario, cuando los valores altos de una variable suelen corresponder mayoritariamente a los menores valores de la otra, expresando un comportamiento opuesto, la covarianza es negativa.



###  7.1. <a name='Histograma'></a>Histograma

En estadística, un [**histograma**](https://es.wikipedia.org/wiki/Histograma) es una representación gráfica de una variable en forma de barras, donde la **superficie de cada barra es proporcional a la frecuencia de los valores** representados.

####  7.1.1. <a name='ComputarunhistogramaenNumPy'></a>Computar un histograma en NumPy

La función [numpy.histogram](https://numpy.org/doc/stable/reference/generated/numpy.histogram.html#numpy-histogram) permite computar un histograma de un array de valores:

```python
print(np.histogram(qualities))
# (array([ 10,   0,  53,   0, 681,   0, 638,   0, 199,  18], dtype=int64), array([3. , 3.5, 4. , 4.5, 5. , 5.5, 6. , 6.5, 7. , 7.5, 8. ]))
```

####  7.1.2. <a name='RepresentacingrficadelhistogramaconMatplotlib'></a>Representación gráfica del histograma con Matplotlib 

En Matplotlib usamos la función [matplotlib.pyplot.hist](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.hist.html#matplotlib-pyplot-hist) para generar un histograma, a función recibirá un array con los valores y los mostrará en una gráfica.

![](img/03.png)

##  8. <a name='Enlacesexternos'></a>Enlaces externos

**Histogramas**:

* [https://numpy.org/doc/stable/reference/generated/numpy.histogram.html#numpy.histogram](https://numpy.org/doc/stable/reference/generated/numpy.histogram.html#numpy.histogram)
* [https://www.w3schools.com/python/matplotlib_histograms.asp](https://www.w3schools.com/python/matplotlib_histograms.asp)
* [https://www.geeksforgeeks.org/matplotlib-pyplot-hist-in-python/](https://www.geeksforgeeks.org/matplotlib-pyplot-hist-in-python/)

##  9. <a name='Recursos'></a>Recursos

* [src/winequality-red.csv](src/winequality-red.csv)

##  10. <a name='Enlacesexternos-1'></a>Enlaces externos

* [https://www.dataquest.io/blog/numpy-tutorial-python/](https://www.dataquest.io/blog/numpy-tutorial-python/)
* [https://youtu.be/EmA_TuC2Vdk](https://youtu.be/EmA_TuC2Vdk)
