---
title: Taller - licencias software
subtitle: Peñascal F5 - Curso sistemas informáticos
date: 2022-05-26
draft: true
description: Peñascal F5 - Curso sistemas informáticos
tags: ["Licencias","Software","Formación","Cursos"]
---

<!-- vscode-markdown-toc -->
* 1. [Terminología básica](#Terminologabsica)
	* 1.1. [¿Qué es el Copyright?](#QueselCopyright)
	* 1.2. [Licencia](#Licencia)
	* 1.3. [Dominio público](#Dominiopblico)
	* 1.4. [¿El copyright está matando la creatividad?](#Elcopyrightestmatandolacreatividad)
* 2. [Conceptos adicionales](#Conceptosadicionales)
	* 2.1. [Patentes](#Patentes)
		* 2.1.1. [¿Que es una patente?](#Queesunapatente)
		* 2.1.2. [Problemas del sistema de patentes](#Problemasdelsistemadepatentes)
		* 2.1.3. [Derechos de autor vs Patentes](#DerechosdeautorvsPatentes)
	* 2.2. [Freeware / shareware](#Freewareshareware)
	* 2.3. [Marcas](#Marcas)
* 3. [Copyleft](#Copyleft)
	* 3.1. [Licencias](#Licencias)
* 4. [Código Abierto](#CdigoAbierto)
* 5. [Introducción al Software Libre](#IntroduccinalSoftwareLibre)
	* 5.1. [¿Qué es libertad?](#Queslibertad)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Terminologabsica'></a>Terminología básica

![](img/05.png)

###  1.1. <a name='QueselCopyright'></a>¿Qué es el Copyright?

El [**Copyright**](https://es.wikipedia.org/wiki/Derecho_de_autor) ("derecho de copia") **o derecho de autor** concede a los creadores la **propiedad** intelectual o artística (literaria, artística, musical, científica o didáctica) sobre sus obras y el derecho a **permitir su publicación, copia o modificación** y participar en los **beneficios** que esta genere. 

La legislación sobre derechos de autor **se remonta a 1710** gracias al [**Estatuto de la Reina Ana**](https://es.wikipedia.org/wiki/Estatuto_de_la_Reina_Ana) (reina de Inglaterra), antes, en la Edad Media la reproducción en masa de una obra literaria era muy difícil, esto cambia con la invención de la imprenta (hacia 1450, por Johann Gutenberg) y pone en cuestión los derechos de autor, hasta ese momento los derechos que tenían los editores sobre la explotación de una obra constituían monopolios, la consecuencia más significativa de la aprobación del _"Statute of Anne"_ fue la introducción de un plazo de duración del copyright.

El primer aspecto supone que, por primera vez, **se distingue entre la obra, entendida como el contenido ideal de un texto literario, y los múltiples ejemplares** que de ella se hacen de forma mecánica y repetitiva.

**Enlaces**:

* [http://www.institutoautor.org/es-ES/SitePages/corp-ayudaP2.aspx?i=277](http://www.institutoautor.org/es-ES/SitePages/corp-ayudaP2.aspx?i=277).
* [https://www.eoi.es/wiki/index.php/Historia_de_la_propiedad_intelectual_en_Propiedad_intelectual](https://www.eoi.es/wiki/index.php/Historia_de_la_propiedad_intelectual_en_Propiedad_intelectual).

###  1.2. <a name='Licencia'></a>Licencia

La [**licencia**](https://es.wikipedia.org/wiki/Licencia_de_software) sin embargo se refiere a los **términos bajo los que se distribuye la obra**, es la autorización formal que otorga el autor de una obra a un usuario para poder utilizarla o explotarla de alguna manera, en términos precisos **el software no se vende, se licencia**, se adquieren una serie de derechos sobre el uso que se le puede dar.

###  1.3. <a name='Dominiopblico'></a>Dominio público

![](img/02.png)

Normalmente transcurrido un tiempo legal la protección de una obra expira, en ese momento pasar a ser de [**dominio público**](https://es.wikipedia.org/wiki/Dominio_p%C3%BAblico), esto simplemente significa que no tiene propietario, se puede modificar y hacer una versión propietaria a partir de él.

![](img/06.png)

El plazo para el ingreso de las obras en dominio público se calcula a partir del 1 de enero del año siguiente de la muerte del autor, razón que ha llevado a que en esa fecha se celebre el [Día del Dominio Público](https://es.wikipedia.org/wiki/D%C3%ADa_del_Dominio_P%C3%BAblico).

Enlaces:

* https://news.illinoisstate.edu/2021/01/happy-public-domain-day-2021/
* https://babel.hathitrust.org/cgi/ls?a=srchls;c=1959461780;q1=*

El plazo legal puede variar entre países, **en España** (Real Decreto 1/1996) la duración de estos derechos, según el texto legal. **"durarán toda la vida del autor y setenta años más después de su muerte"** (esa es la razón de que cualquiera pueda editar un libro de El Quijote, por ejemplo).

Enlaces:

* http://www.bne.es/es/Servicios/InformacionBibliografica/AutoresDominioPublico/
* Listado de autores cuya obra ha pasado dominio público en 2022 (fallecidos en 1941)  (XLS) http://www.bne.es/webdocs/Servicios/Informacion_bibliografica/DominioPublico1941-web.xls
* Ficheros de autores en dominio público en 2022 (fallecidos en 1941) (diferentes formatos) https://datos.gob.es/es/catalogo/ea0019768-autores-espanoles-en-dominio-publico-en-2022-fallecidos-en-1941 

[En EEUU](https://es.wikipedia.org/wiki/Ley_de_derechos_de_autor_de_los_Estados_Unidos) (borrar? en la Acta de los Derechos de Autor de 1976 a "75 años tras la publicación de una obra colectiva o la vida del autor más 50 años" y) la **Ley de Extensión del Plazo de Derecho de Autor** (Copyright Term Extension Act o CTEA) de Sonny Bono de **1998** (también llamada despectivamente la "Ley de Protección de Mickey Mouse") lo aumentó, a 95 años para obras colectivas, o la **vida del autor más 70 años**.

Aunque el ratón [Mickey](https://es.wikipedia.org/wiki/Mickey_Mouse) apareció por primera vez el 18 de noviembre de 1928 en el mítico corto cinematográfico Steamboat Willie, el plazo de liberación se ha ido extendiendo, al parecer el ratón Mickey estará libre de derechos en 2024, no esta mal recordar que **su creador Walt Disney murió en 1966**, desde entonces a sido una compañia (Disney) quien se ha lucrado de los _royalties_.

![](img/04.png)

Ojo porque no todos los ratones Mickey quedarán liberados en esa fecha. Solo lo hará el Mickey de 1928, el modelo en blanco y negro de los primeros cortos.

En **1989, Disney demandó a tres guarderías de Florida**, por tener en sus paredes dibujos de Mickey y de otros personajes Disney. Los dibujos tuvieron que ser retirados.

Enlaces:

* https://www.abc.es/xlsemanal/a-fondo/mickey-mouse-derechos-autor-liberacion-2023-propiedad-walt-disney-dominio-publico.html
* https://blog.jipel.law.nyu.edu/2019/12/in-2024-mickey-mouse-will-finally-enter-the-public-domain-sort-of/

Una anécdota curiosa, la película "La noche de los muertos vivientes" (1968), del director George A.Romero que **pertenece al dominio público**, no por deseo expreso del director o la productora ni porque hayan transcurrido unos años y sus derechos hayan caducado, **un error permitió que se distribuyese sin incluir la nota del copyright en los títulos de crédito**. Es por eso que la película ha tenido docenas de lanzamientos de videos caseros de diferentes compañías, ya que todo lo que tenían que hacer es adquirir una copia de la película, y luego podrían lanzarla libremente sin llegar a ningún tipo de acuerdo con Romero o sus colaboradores (ganó más de $30 millones en la taquilla). De hecho, vosotros mismos os la podéis descargar de [Archive.org](https://archive.org/details/night.of.the.-living.-dead.-1968-vose) (con licencia CC).

![](img/03.png)

* https://www.microsiervos.com/archivo/curiosidades/noche-copyright-vivientes.html
* https://lanetaneta.com/como-la-noche-de-los-muertos-vivientes-se-convirtio-accidentalmente-en-dominio-publico/

No hace falta irnos tan lejos en el tiempo para encontrar otros ejemplos, en este caso no es error, un caso peculiar es [SQLite](https://www.sqlite.org/index.html), es un SGBD escrito en C y liberado como de dominio público.

![](img/01.png)

###  1.4. <a name='Elcopyrightestmatandolacreatividad'></a>¿El copyright está matando la creatividad?

* ¿El copyright ha excedido su propósito inicial de retornar un beneficio justo a su autor?
* Desvinculación cada vez mayor que existe entre el ejercicio formal de los derechos conferidos por el derecho de autor y su finalidad económica subyacente, que es **incentivar la creación y la distribución de obras**.
* Los factores que alientan y motivan la creación pueden ser diferentes y no sólo económicos (prestigio, etc).
* El copyright se ha convertido un sistema de cobro por copia. La industria ha gastado millones en sistema anticopia sin mucho éxito ¿Un modelo obsoleto en la era digital? La digitalización de material sometido a derechos de autor permite su copia ilimitada, lo que, para la industria, es una amenaza.

Enlaces:

* https://www.xataka.com/musica/cuando-el-copyright-mato-la-creatividad
* https://www.popmatters.com/is-copyright-killing-creativity-2495438844.html
* https://hipertextual.com/2011/06/asi-es-como-el-copyright-destruye-la-creatividad
* https://elpais.com/diario/2004/10/10/negocio/1097413412_850215.html
* https://freeknowledge.eu/es/los-peligros-del-copyright-en-la-era-digital/
* https://www.gnu.org/philosophy/reevaluating-copyright.es.html
* https://www.tusdj.com/soundcloud-lo-vuelve-a-hacer-esta-vez-ni-creeras-lo-que-hizo/

##  2. <a name='Conceptosadicionales'></a>Conceptos adicionales

###  2.1. <a name='Patentes'></a>Patentes

####  2.1.1. <a name='Queesunapatente'></a>¿Que es una patente?

Se trata de un **monopolio temporal sobre una tecnología o producto, concede la explotación comercial exclusiva** a su creador a cambio de la divulgación de su invención, supuestamente garantizan el retorno de la inversión realizada en el desarrollo del producto.

La protección de **derechos de autor** no se concede a ninguna idea, procedimiento, proceso, sistema, método de operación, concepto, principio o descubrimiento, independientemente de la forma como esté descrito, explicado o materializado.

En la Unión Europea, el **software en sí mismo no es patentable**, hace falta demostrar un “efecto tecnológico adicional” .

####  2.1.2. <a name='Problemasdelsistemadepatentes'></a>Problemas del sistema de patentes


####  2.1.3. <a name='DerechosdeautorvsPatentes'></a>Derechos de autor vs Patentes


###  2.2. <a name='Freewareshareware'></a>Freeware / shareware

Freeware / [shareware](https://es.wikipedia.org/wiki/Shareware):

* Free en el sentido de gratis
* Share en el sentido de comparte tu dinero conmigo

Simplemente **indica restricciones económicas**, no indica ninguna licencia específica. Generalmente las licencias asociadas son propietarias.

###  2.3. <a name='Marcas'></a>Marcas

Cualquier **cooperación humana a gran escala** (ya sea un Estado moderno, una iglesia medieval, una ciudad antigua o una tribu arcaica) **está establecida sobre mitos comunes** que solo existen en la **imaginación colectiva** de la gente. A riesgo de que alguien se moleste, las iglesias se basan en mitos religiosos comunes, las cruzadas que llevaron a miles de europeos a conquistar tierra santa sin conocerse entre ellos son un buen ejemplo, los estados se cimientan sobre la existencia de una nación, una bandera o una patria, las guerras donde la gente muere por un concepto llamado nación son otro buen ejemplo. El capitalismo entre otras se sustenta en las marcas que identifican los productos de consumo, el mismo dinero como valor de cambio no existe más allá de nuestra imaginación común.  

Una [**marca**]() (_brand_) es una identificación comercial primordial o el conjunto de varios identificadores con los que se relaciona y ofrece un producto o servicio en el mercado.

##  3. <a name='Copyleft'></a>Copyleft

En 1984, Richard Stallman, un programador del Laboratorio de IA de MIT (Artificial Intelligence Lab), como reacción contra la decisión tomada por ATT de reclamar derechos de autor sobre UNIX , creó la [FSF](https://www.fsf.org/es) (Free Software Foundation) y propuso la sustitución del concepto de copyright por el de **Copyleft**.

En contraposición al copyright nace el mecanismo del **Copyleft** para producir software transparente, susceptible de modificación y libre que se implementa a través de las licencia libres como la **GPL** o **CC** (Creative Commons).

El **Copyleft protege los derechos ofrecidos en la licencia de intentos subsecuentes de privatización por parte del público** (mientras la obra no pase al dominio público), este efecto se conoce como el "El efecto Midas", [Midas](https://es.wikipedia.org/wiki/Midas) fue un rey griego, de acuerdo con la mitología el monarca tenía la maldición de convertir en oro todo lo que tocara, era un poder y una maldición, no murió de hambre pero casi (curiosamente, a menudo se la achaca eso mismo a las dificultad de comercializar software bajo estas licencias). También se les llama a menudo a [licencias víricas](https://es.wikipedia.org/wiki/Licencia_v%C3%ADrica) por la misma razón.

![](img/07.png)

###  3.1. <a name='Licencias'></a>Licencias


##  4. <a name='CdigoAbierto'></a>Código Abierto

##  5. <a name='IntroduccinalSoftwareLibre'></a>Introducción al Software Libre

###  5.1. <a name='Queslibertad'></a>¿Qué es libertad?


