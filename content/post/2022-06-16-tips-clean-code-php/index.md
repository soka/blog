---
title: Tips para escribir código más limpio en PHP
subtitle: Buenas prácticas en PHP
date: 2022-06-16
draft: false
description: Buenas prácticas en PHP
tags: ["PHP","Programación","Coding","Buenas prácticas","Diseño","Refactoring"]
---

<!-- vscode-markdown-toc -->
* 1. [Patrón de diseño "Return early", evita anidamiento innecesario](#PatrndediseoReturnearlyevitaanidamientoinnecesario)
* 2. [Fail Fast](#FailFast)
* 3. [Desestructurar objetos en los parámetros de las funciones](#Desestructurarobjetosenlosparmetrosdelasfunciones)
* 4. [Pure functions](#Purefunctions)
* 5. [Mantén las funciones simples](#Mantnlasfuncionessimples)
* 6. [Nombres de variables significativos](#Nombresdevariablessignificativos)
* 7. [Usa constantes en lugar de variables hardcoded](#Usaconstantesenlugardevariableshardcoded)
* 8. [No usar la llave de cierre al final de un fichero](#Nousarlallavedecierrealfinaldeunfichero)
* 9. [Alternativa a require o include](#Alternativaarequireoinclude)
* 10. [switch en lugar de if/elseif](#switchenlugardeifelseif)
* 11. [Operadores ternarios](#Operadoresternarios)
* 12. [No añadas contexto innecesario](#Noaadascontextoinnecesario)
* 13. [Encapsular condicionales](#Encapsularcondicionales)
* 14. [Etiqueta PHP abreviada](#EtiquetaPHPabreviada)
* 15. [Reduce el número de argumentos de una función](#Reduceelnmerodeargumentosdeunafuncin)
* 16. [Usa un formato estándar y coherente](#Usaunformatoestndarycoherente)
* 17. [Principio DRY](#PrincipioDRY)
* 18. [Principio KISS](#PrincipioKISS)
* 19. [YAGNI](#YAGNI)
* 20. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Esta entrada no es original de mi puño y letra, está basada en otros artículos cuyas referencias se pueden encontrar al final de este texto, también resulta imprescindible y una fuente valiosa de información el libro "Clean Code" de Robert C.Martin.

## El coste de un código desastroso

Seguramente en algún momento de tu vida como programador has "heredado" código de otra persona que ha ralentizado tus avances (o a veces incluso te ha sucedido cuando has retomado algún proyecto tuyo tiempo después), cuando se trata de un proyecto nuevo normalmente se realizan avances rápidos al principio, pero si no está bien diseñado o no se adoptan unas buenas prácticas con el paso del tiempo nuevos cambios y mejoras cuestan cada vez más. Cada cambio que realizas en el código acaba afectando de manera negativa en otras partes. Ningún cambio es trivial y debería ser sopesado previamente. Con el tiempo el desastre acaba siendo tan grande y profundo que se convierte en imposible limpiarlo o tomar otro camino que ahondar en él.    

![](img/03.png)

[[Fuente](https://woutercarabain.com/webdevelopment/clean-code-in-php-5-tips-to-help-you/)]

A medida que el desastre aumenta la productividad se resiente, el PM puede dirigir recursos adicionales al proyecto para solucionar el atasco pero normalmente esto no hace que mejore la situación. 

Gráfica con la evolución de la productividad en el tiempo (tomada del libro "Clean Code").

![](img/06.png)

Escribir código, es algo más que escribir, la legibilidad es clave.

> "Indeed, the ratio of time spent reading versus writing is well over 10 to 1. We are constantly reading old code as part of the effort to write new code. ...[Therefore,] making it easy to read makes it easier to write." - Robert C. Martin, Clean Code: A Handbook of Agile Software Craftsmanship.    

##  1. <a name='PatrndediseoReturnearlyevitaanidamientoinnecesario'></a>Patrón de diseño "Return early", evita anidamiento innecesario

Usamos el anidamiento de forma constante en nuestros programas, a pesar de que no hay nada inherentemente malo en ello, cuando abusamos a veces dificulta la lectura del código. Una aproximación para evitar el anidamiento en exceso es usar el patrón de diseño **"Return Early"** (retorno temprano o algo así). Consiste en usar la sentencia condicional `if` (_Guard Clause_) para **comprobar errores y salir de la función antes de seguir ejecutando cualquier otro código**. 

> Return early is the way of writing functions or methods so that the expected positive result is returned at the end of the function and the rest of the code terminates the execution (by returning or throwing an exception) when conditions are not met.

Veamos el siguiente código:

```php
// Bad
function deleteItem($item) {
    if ($item != null) {
        echo "Deleting item";
        $item.delete();
    }
}
```

En esencia no hay nada mal en este código pero podemos hacerlo algo más limpio comprobando al inicio de la función el contenido de la variable pasada como parámetro. En lugar de comprobar si el valor de la variable no es nulo, comprobamos la afirmación opuesta.

```php
// Good
function deleteItem($item) {
    if($item==null) return;
    
    echo "Deleting item";
    $item.delete();
}
```

![](img/01.png)

Este es otro ejemplo más avanzado de la misma técnica, la función inicial:

```php
function saveItem($item) {
    if ($item != null) {
        echo "Validating";

        if ($item.isValid()) {
            echo "Saving item";
            $item.save();
        }
    }
}
```

Podemos reducirlo así:

```php
function saveItem($item) {
    if ($item == null) return;
    echo "Validating";

    if (!$item.isValid()) return;
    echo "Saving item";
    $item.save();
} 
```

![](img/02.png)

Observaciones:

* El código sólo tiene un nivel de anidamiento, permite leerlo de forma lineal.
* El resultado correcto esperado se puede encontrar de forma fácil al final de la función.
* Ponemos el foco en detectar los errores al principio y se implementa la lógica después, reduce la posibilidad de _bugs_.
* Similitudes con TDD (Test Driven Development) que facilita el testeo del código.
* La función finaliza inmediatamente ante un error, evitamos ejecutar código sin necesidad.

##  2. <a name='FailFast'></a>Fail Fast

Se les atribuie a Jim Shore y  Martin Fowler el concepto de [**Fail Fast**](https://www.martinfowler.com/ieeeSoftware/failFast.pdf) (PDF) en 2004, este concepto es la base para al patrón "**return early**". Mientras buscamos el fallo rápido el código es más robusto porqué ponemos el foco en las condiciones en las cuales el código puede terminal al principio. Con esta aproximación los _bugs_ son más fáciles de encontrar y reparar.

##  3. <a name='Desestructurarobjetosenlosparmetrosdelasfunciones'></a>Desestructurar objetos en los parámetros de las funciones 

Imaginemos que tenemos una función que recibe un objeto como parámetro, realiza alguna operación con el mismo y retorna un nuevo valor. 

Vamos a analizar esta función:

```php
function getFullName($person) {
    $firstName = $person.firstName;
    $lastName = $person.lastName;
    return "$firstName $lastName";
}
```

La implementación funciona correctamente, pero estamos creando dos variables temporales cuando en realidad no las necesitamos. 

Una forma mejor de implementar esto es usar la desestructuración de objetos, en PHP podemos usar la función `get_object_vars` que devuelve un array asociativo con las propiedades del objeto especificado:


```php
class Person {
    public $firstName;
    public $lastName; 
}

function getFullName($person) {
    return get_object_vars($person)["firstName"]." ".get_object_vars($person)["lastName"];
}

$objPerson = new Person();
$objPerson->firstName = "Iker";
$objPerson->lastName = "Landajuela";

echo "Person data: ".getFullName($objPerson);
```

> The Destructuring Assignment in PHP like in Javascript was introduced in PHP version 7.1.0. In PHP, the destructuring is only possible for Symmetric Array , not for Object. It is an alternative of PHP function list which now can be used with shorthand array syntax([]).

##  4. <a name='Purefunctions'></a>Pure functions

Una [**función pura**](https://en.wikipedia.org/wiki/Pure_function) siempre obtiene el mismo resultado cuando le pasamos los mismos argumentos. El resultado de la función no puede depender de información oculta que puede cambiar durante la ejecución del programa o durante ejecuciones diferentes, tampoco puede depender en una entrada externa de un dispositivo de E/S. La evaluación del resultado no provoca un efecto colateral o salida.

Es suficiente decir que una función en PHP es pura si:

* Si los argumentos son pasados por valor (no pasados por referencia con &).
* No usa miembros de un objeto, los objetos en PHP son pasados por referencia en las funciones.
* No usa variables globales.
* No usa superglobales ($_).
* No genera ninguna salida E/S ni escribe en ficheros, etc.

Hay más casos pero estos son los básicos.

Ejemplo de una función que recibe un objeto como argumento:

```php
class MyClass {
    public $myVar;
}

function test($myClass) { $myClass->myVar = 4; }

$class = new MyClass();
$class->myVar = 3;
test($class); 
echo "Num is: ".$class->myVar;  // Num is: 4
```

Otro ejemplo donde un valor dentro de la función depende de una variable global externa:

```php
$counter = 0; 

function increment(): int {
    GLOBAL $counter;
    return ++$counter;    
}

increment();
echo "Counter is: ".$counter; // Counter is: 1
```

##  5. <a name='Mantnlasfuncionessimples'></a>Mantén las funciones simples

Las funciones realizan acciones, usa **verbos** para nombrarlas (get,set,is,etc.):

```php
function passwordValidation() {
}

function validatePassword() {
}
```

> There are two ways of constructing a software design: one way is to make it so simple that there are obviously no deficiencies, and the other way is to make it so complicated that there are no obvious deficiencies. — C. A. R. Hoare.

Cuando escribimos funciones, puede ser tentador hacer varias cosas en el mismo sitio:

```php
function signUpAndValidate() {

}
```

Pero esto nos puede llevar a crear [código espagueti](https://es.wikipedia.org/wiki/C%C3%B3digo_espagueti) demasiado complejo e incomprensible. Una aproximación más correcta es las funciones sean responsables de una cosa:

```php
function signUp() {
}
function validate() {
}
```

Las funciones deberían ser pequeñas, no hay una norma que indique el número de líneas máximo que debe implementar, antaño en los 80 se tomaba como criterio el tamaño de pantalla, el número de líneas de una función no debería exceder la pantalla.

Estas prácticas en general facilitan la reutilización de las funciones. 

##  6. <a name='Nombresdevariablessignificativos'></a>Nombres de variables significativos


Usa **is** cuando sea un tipo booleano:

```php
$isValidPassword = validatePassword("abcd");
```

Usa plurales para arrays:

```php
$animales = array("gato","perro");
```

##  7. <a name='Usaconstantesenlugardevariableshardcoded'></a>Usa constantes en lugar de variables hardcoded


```php
// Bad
$user->setRole('ROLE_ADMINISTRATOR');

// Good
class User {
    public const ROLE_ADMINISTRATOR = 'ROLE_ADMINISTRATOR';
    public const ROLE_USER = 'ROLE_USER';
}
$user->setRole(User::ROLE_ADMINISTRATOR);
```

##  8. <a name='Nousarlallavedecierrealfinaldeunfichero'></a>No usar la llave de cierre al final de un fichero 

De echo la guía de Zend no lo permite, evita que introduzcamos caracteres por error después del cierre.

> For files that contain only PHP code, the closing tag ("?>") is never permitted. It is not required by PHP, and omitting it´ prevents the accidental injection of trailing white space into the response.

##  9. <a name='Alternativaarequireoinclude'></a>Alternativa a require o include


```php
function include_class($class_name){
    //path to the class file
    $path = ROOT . '/include/' . $class_name . '.php');
    if(file_exists($path)) {
       	require_once( $path );
    }
}

include_class('Configure');
include_class('Database');
```

##  10. <a name='switchenlugardeifelseif'></a>switch en lugar de if/elseif

Usa switch en lugar de repetir sentencias if/elseif repetitivas, la ventaja es que el código se ejecuta más rápido:

```php
if($color == 'yellow'){
	echo "The color is yellow";
}
elseif($color == 'black'){
	echo "The color is black";
}
elseif($color == 'red'){
	echo "The color is red";
}
elseif($color == 'pink'){
	echo "Color is pink";
}else{
	echo "Color is white";
}
```

```php
switch ($color ) {
	case 'yellow' :
	echo "The color is yellow" ;
break ;
	case 'black' :
	echo "The color is black" ;
break ;
	case 'red' :
	echo "The color is red" ;
break ;
	case 'pink' :
	echo "Color is pink" ;
break ;
default:
	echo "Color is white";
}
```

##  11. <a name='Operadoresternarios'></a>Operadores ternarios

```php
$phone = (!empty ( $_POST['phone'] ) ? $_POST['phone'] : 9111111111);
```

##  12. <a name='Noaadascontextoinnecesario'></a>No añadas contexto innecesario

Mal:

```php
class Car
{
    public $carMake;

    public $carModel;

    public $carColor;

    //...
}
```

Bien:

```php
class Car
{
    public $make;

    public $model;

    public $color;

    //...
}
```

##  13. <a name='Encapsularcondicionales'></a>Encapsular condicionales

```php
if ($article->state === 'published') {
    // ...
}
```

```php
if ($article->isPublished()) {
    // ...
}
```

##  14. <a name='EtiquetaPHPabreviada'></a>Etiqueta PHP abreviada

```php
<?php echo 'Some text'; ?>
```

```php
<?= 'Some text'; ?>
```

##  15. <a name='Reduceelnmerodeargumentosdeunafuncin'></a>Reduce el número de argumentos de una función

Cuando una función necesita más de dos o tres argumentos, seguramente algunos de ellos se pueden agrupar mediante una clase.

Mira la diferencia entre las declaraciones de estas dos funciones:

```php
Circle makeCircle(double x, double y, double radius);
Circle makeCircle(Point center, double radius);
```

Reducir el número de argumentos creando objetos puede parecer una mala elección, pero no lo es. En el ejemplo superior x e y pertenecen a un concepto propio, un punto en un plano. 	

Mal:

```php
<?php
function createMenu($title, $body, $buttonText, $cancellable) {
    // ...
}
```

Bien:

```php
<?php
class MenuConfig
{
    public $title;
    public $body;
    public $buttonText;
    public $cancellable = false;
}
$config = new MenuConfig();
$config->title = 'Foo';
$config->body = 'Bar';
$config->buttonText = 'Baz';
$config->cancellable = true;
function createMenu(MenuConfig $config) {
    // ...
}
```

##  16. <a name='Usaunformatoestndarycoherente'></a>Usa un formato estándar y coherente

[http://www.phpformatter.com/](http://www.phpformatter.com/).

##  17. <a name='PrincipioDRY'></a>Principio DRY

![](img/05.jpg)

El principio [**No te repitas**](https://es.wikipedia.org/wiki/No_te_repitas) ("Don't repeat yourself" o [DRY](http://wiki.c2.com/?DontRepeatYourself)) intenta prevenir contra la [duplicación del código fuente](https://es.wikipedia.org/wiki/C%C3%B3digo_duplicado). 

> "Every piece of knowledge must have a single, unambiguous, authoritative representation within a system" - The Pragmatic Programmer.

Según este principio el código duplicado incrementa la dificultad en los cambios, la posibilidad de introducir errores e inconsistencias y la evolución posterior del código en general.

> "Duplicated code is the root of all evil in software design." — Robert C. Martin.

Además dificulta la legibilidad y el mantenimiento, generalmente la funcionalidad no se ve afectada.  
Variaciones del mismo concepto:

* Una vez y sólo una ([Once and Only Once](http://wiki.c2.com/?OnceAndOnlyOnce)). Este principio es similar pero con distinto objetivo, con OnceAndOnlyOnce se te pide que elimines código y funcionalidades duplicadas.
* Duplication Is Evil (DIE).

![](img/04.png)

Conceptos en oposición:

* WET "We Enjoy Typing" o "Write Everything Twice", también se entiende mejor con "waste everyone's time". 

Enlaces adicionales:

* [http://wiki.c2.com/?DontRepeatYourself](http://wiki.c2.com/?DontRepeatYourself).
* [https://enterprisecraftsmanship.com/posts/dry-revisited/](https://enterprisecraftsmanship.com/posts/dry-revisited/).

##  18. <a name='PrincipioKISS'></a>Principio KISS (Keep It Simple Stupid)

##  19. <a name='YAGNI'></a>YAGNI (You Aren’t Gonna Need It)

##  20. <a name='Enlacesexternos'></a>Enlaces externos

* [https://www.instapaper.com/read/1512973378](https://www.instapaper.com/read/1512973378).
* [https://medium.com/swlh/return-early-pattern-3d18a41bba8](https://medium.com/swlh/return-early-pattern-3d18a41bba8).
* [https://www.martinfowler.com/ieeeSoftware/failFast.pdf](https://www.martinfowler.com/ieeeSoftware/failFast.pdf) (PDF).
* [https://webkul.com/blog/destructuring-assignment-of-variables/](https://webkul.com/blog/destructuring-assignment-of-variables/).
* [https://en.php.brj.cz/pure-functions-in-php](https://en.php.brj.cz/pure-functions-in-php).
* [https://en.wikipedia.org/wiki/Pure_function](https://en.wikipedia.org/wiki/Pure_function).
* [https://leanpub.com/functional-php/read](https://leanpub.com/functional-php/read).
* [https://woutercarabain.com/webdevelopment/clean-code-in-php-5-tips-to-help-you/](https://woutercarabain.com/webdevelopment/clean-code-in-php-5-tips-to-help-you/).
* [https://framework.zend.com/manual/1.12/en/coding-standard.php-file-formatting.html](https://framework.zend.com/manual/1.12/en/coding-standard.php-file-formatting.html).
* [https://techmesh.tech/programing-and-development/advance-php-tricks-and-tips-part-one/](https://techmesh.tech/programing-and-development/advance-php-tricks-and-tips-part-one/).
* [https://github.com/jupeter/clean-code-php](https://github.com/jupeter/clean-code-php).
* [https://metabox.io/php-technique-write-clean-readable-code/](https://metabox.io/php-technique-write-clean-readable-code/).
* [https://stackcoder.in/posts/writing-clean-code-in-php](https://stackcoder.in/posts/writing-clean-code-in-php).
* [https://blog.devgenius.io/clean-php-code-57da4cf75537](https://blog.devgenius.io/clean-php-code-57da4cf75537).
* [https://latteandcode.medium.com/php-8-tricks-that-will-help-you-write-cleaner-code-374c71daffb6](https://latteandcode.medium.com/php-8-tricks-that-will-help-you-write-cleaner-code-374c71daffb6).
* [https://www.refactoring.com/catalog/decomposeConditional.html](https://www.refactoring.com/catalog/decomposeConditional.html).
* [https://medium.com/swlh/return-early-pattern-3d18a41bba8](https://medium.com/swlh/return-early-pattern-3d18a41bba8).
