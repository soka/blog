---
title: Formularios con HTML
subtitle: una breve introducción y recursos para hacer pruebas  
date: 2020-11-20
draft: false
description: Diseñamos un form básico y realizamos pruebas con PTS - V2
tags: ["HTML","Forms","Formularios","Pruebas","Herramientas","Testing","PTS","VSCode","Visual Studio Code","Programación"]
---

Hemos diseñado un formulario básico en HTML usando VSCode, la extensión [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) ejecuta un servidor en nuestro equipo local, nos puede ahorrar tiempo refrescando de forma automática el contenido de nuestra página web sin necesidad de andar haciéndolo de forma manual.

```html
<!doctype html>

<html lang="es">
<head>
    <meta charset="utf-8">

    <title>HTML Form Test</title>
    <meta name="description" content="HTML5 Form">
    <meta name="author" content="Iker Landajuela">

</head>

<body>    
    <form action="http://ptsv2.com/t/7wh60-1605860203/post" method="POST">
        <h1>Hello World!</h1>
        <p>Probando un form</p>
        <label for="name">Tu nombre:</label>
        <input type="text" id="name" name="user_name" />
        <label for="city">¿Donde vives?</label>
        <select id="city" name="user_city">
            <option>Bilbo</option>
            <option>Zornotza</option>
        </select>
        <button>Enviar</button>
    </form>
</body>
</html>
```

![](img/01.png)

Ahora necesito un servidor de prueba para mandarle la ejecución del formulario, en este punto viene al rescate [Post Test Server V2](https://ptsv2.com/), está sencilla Web permite a los desarrolladores probar las llamadas HTTP POST y GET. Para empezar debemos buscar un "retrete" libre o generar uno aleatorio para mandar nuestro zurullo de formulario, PTSV2 generá una nueva URL con ún nuevo código en la ruta para las llamadas.

![](img/02.png)

Cuando procesamos el formulario (podemos analizar las llamadas con el inspector del navegador) nos devuelve una frase como resultado. Con el código del baño que estamos usando se puede visualizar un listado con todas las llamadas (en mi caso [https://ptsv2.com/t/7wh60-1605860203](https://ptsv2.com/t/7wh60-1605860203)).

![](img/03.png)

Podemos acceder a cada una y visualizar el detalle de las cabeceras y sobre todo comprobar que enviamos los parámetros correctamente.

![](img/04.png)

Y ya está, gracias al [Bootcamp de Peñascal F5](https://www.peñascalf5.org/) por estos trucos y herramientas que nos facilitan la vida a los desarrolladores.

Otros servicios similares son [https://api.chucknorris.io/](https://api.chucknorris.io/), las llamadas GET devuelven una frase graciosa de Chuck en formato JSON, por ejemplo un GET a [https://api.chucknorris.io/jokes/random] retorna una frase aleatoria.

![](img/chucknorris_logo_coloured_small@2x.png)

Algo parecido hace también [SWAPI - The Star Wars API](https://swapi.dev/).


## Enlaces externos

- [https://ptsv2.com/](https://ptsv2.com/).
- [https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer).
- [https://www.sitepoint.com/a-basic-html5-template/](https://www.sitepoint.com/a-basic-html5-template/): Esqueleto básico HTML5 para una página.
- [https://developer.mozilla.org/es/docs/Learn/HTML/Forms/How_to_structure_an_HTML_form](https://developer.mozilla.org/es/docs/Learn/HTML/Forms/How_to_structure_an_HTML_form).