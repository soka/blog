---
title: Servicio de escritorio remoto sobre Windows Server 2008 [borrador]
subtitle: Administración de Windows Server
date: 2020-03-14
draft: true
description: Administración servidores Windows
tags: ["Windows","Sistemas","Administración","Windows Server","RDP","Escritorio Remoto"]
---

![](img/win-server-logo.png)

## Habilitar Escritorio remoto con fines administrativos

Habilitamos el servicio RDP en un servidor recién instalado:

![](img/02.png)

El propio servidor habilita las reglas del FW que permiten el tráfico RDP (TCP 3389):

![](img/04.png)

Ahora ya podemos comprobar si el servidor RDP está a la escucha con el siguiente comando `netstat /p tcp /a | findstr 3389`, la salida debería ser algo como la captura de abajo: 

![](img/05.png)

También he habilitado el tráfico ICMPv4 de entrada en el FW del servidor para poder hacer ping al mismo:

![](img/06.png)

## Instalar función Terminal Services

![](img/07.png)

![](img/08.png)

![](img/09.png)

![](img/10.png)

![](img/11.png)

## Acceso NAT en el router


## Rol RDS Gateway 



## Enlaces internos

- [Habilitar escritorio remoto y reglas del Firewall](https://soka.gitlab.io/blog/post/2020-01-03-ps-habilitar-rdp-y-reglas-firewall/).
- [Administración de servicios con Powershell](https://soka.gitlab.io/blog/post/2020-01-05-admin-servicios-win-ps/).
- [Administración de usuarios y grupos locales con Powershell](https://soka.gitlab.io/blog/post/2020-01-04-admin-usuarios-y-grupos-locales-windows-ps/).
- [Abrir aplicaciones de administración desde línea de comandos](https://soka.gitlab.io/blog/post/2020-01-03-lanzar-aplicaciones-admin-desde-cli-windows/).
- [Servidor DNS](https://soka.gitlab.io/blog/post/2019-11-23-windows-server-dns-5/).
- [Instalación del rol de servidor DHCP](https://soka.gitlab.io/blog/post/2019-11-09-windows-server-dhcp-4/).
- [Introducción a PowerShell](https://soka.gitlab.io/blog/post/2019-11-08-windows-server-ps-3/).
- [Windows Server 2012, primeros pasos y configuración básica](https://soka.gitlab.io/blog/post/2019-11-06-windows-server-2/).
- [Instalación de Windows Server 2012 virtualizado en Debian](https://soka.gitlab.io/blog/post/2019-11-05-windows-server-1/).

## Referencias externas

