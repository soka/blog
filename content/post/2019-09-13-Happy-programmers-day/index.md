---
title: Happy programmers day!
subtitle: Un excusa para jugar con fechas en R
date: 2019-09-13
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Fechas", "R - Funny","Happy programmers day"]
---

![](images/imagen.png)

Son las 23:04 del 13 de Septiembre (y encima Viernes), tengo algo menos de una hora para explicar el porqué de esta fecha para celebrar el [día del programador](https://es.wikipedia.org/wiki/D%C3%ADa_de_los_Programadores) y hacer algo en **R** aprovechando tan insigne día.

El día del programador **se celebra el día número 256 de cada año** (13 de septiembre durante los años normales y el 12 de septiembre durante los bisiestos), la explicación de porqué este día y no otro se entiende cuando hablamos de programadores, 256 (2 elevado a 8) es la cantidad de valores que puede representarse con 1 byte (8 bits). Además, el número 256 es la mayor potencia de 2 menor que 365 (cantidad de días de un año).

![](images/images.jpeg)

## Calcular el día del año

Usando el operador pipe del paquete ([dplyr](https://cran.r-project.org/web/packages/dplyr/vignettes/dplyr.html)) voy transformando el dato, [`Sys.time()`](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/Sys.time) retorna la fecha-hora actual, a continuación lo convierto en una cadena con `strftime` representado como el día del año (un valor entre 001 a 366) con el formato "%j", al final convierte la cadena en número.

```r
> Sys.time() %>% strftime(, format = "%j") %>% as.numeric()
[1] 256
```

El resto es trivial:

```r
if ( Sys.time() %>% strftime(, format = "%j") %>% as.numeric() == 0x100) {
  "Happy programmers day"
} else {
  "Not today"
}
```

## Calcular el día del programador dado el año

Dado un año calcula la fecha del día del programador, convierto la cadena  

```r
> year <- 2016
> as.Date(sprintf("%d-01-01", year)) + 255
[1] "2016-09-12"
```

## Lubridate: Paquete para manipular fechas

[Lubridate](https://cran.rstudio.com/web/packages/lubridate/) ([PDF](https://cran.rstudio.com/web/packages/lubridate/lubridate.pdf)) es un paquete para trabajar con fechas, `yday` calcula el día del año dada la fecha actual:

```r
> Sys.time() %>% yday()
[1] 256
```

## Código fuente

- r / intro / 54-happy-programmers-day /[happy-programmers-day.R](https://gitlab.com/soka/r/blob/master/intro/54-happy-programmers-day/happy-programmers-day.R).


## Conclusiones 

Son las 00:05 del 14 de Septiembre, objetivo cumplido, compruebo que ya no es el día del programador, que mis algoritmos funcionan. Me voy a ver un capítulo de [Preacher](https://www.filmaffinity.com/es/film314781.html) basada en el [comic](https://en.wikipedia.org/wiki/Preacher_(comics)) de Garth Ennis y Steve Dillon.  

![](images/preacher-comic-you-got-it-hero-image-crop-letterbox.png)

¿Para otro post algo sobre Viernes 13?

![](images/01.png)

#HappyProgrammersDay

## Enlaces externos

- es.wikipedia.org ["Día de los Programadores"](https://es.wikipedia.org/wiki/D%C3%ADa_de_los_Programadores).
- cran.r-project.org ["Do more with dates and times in R"](https://cran.r-project.org/web/packages/lubridate/vignettes/lubridate.html).


