---
title: Inventario de equipos remotos con PowerShell
subtitle: Administración equipos y sistemas
date: 2019-04-03
draft: false
description: PowerShell cmdlets
tags:
  ["PowerShell", "PS", "Scripting", "Monitorización", "Computadoras", "Cmdlet"]
---

**WMI** es una tecnología para recabar información sobre equipos, es especialmente útil para monitorizar diversos parámetros de un equipo remoto, si quieres saber más consulta este [enlace](https://soka.gitlab.io/PSFabrik/networking/wmi/wmi-intro/).

El siguiente cmdlet recaba información de uno o varios equipos en red: Nombre de computadora, fabricante, modelo, procesador, tipo sistema, sistema operativo, versión del sistema operativo, número de serie, dirección de red, último usuario logeado, espacio en disco, etc.

La función `Get-Inventory` recibe como parámetro uno o varios equipos:

```PS
Get-Inventory PC-Servidor, PC-Manu
```

La función lo primero que hace es comprobar que equipos están activos usando la función `Test-Connection`, si están "vivos" guarda su nombre en un fichero de texto "livePCs.txt" (o en "deadPCs.txt" si no está activo), recorre en un bucle el `foreach` los equipos activos y usando la función `Get-WmiObject` recaba los datos necesarios. Finalmente guarda los datos en un fichero CSV "pcsinventory.csv".

# Cmdlet

- PSFabrik / docs / computadora / [Get-Inventory.ps1](https://gitlab.com/soka/PSFabrik/blob/master/docs/computadora/Get-Inventory.ps1).

```PS
Function Get-Inventory {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $True)]
        [string[]]$Computers
    )


    #$exportLocation = "$env:HOMEDRIVE\scripts\"
    $exportLocation = "."

    # Test connection to each computer before getting the inventory info
    foreach ($computer in $Computers) {
        if (Test-Connection -ComputerName $computer -Quiet -count 2) {
            Add-Content -value $computer -path "$exportLocation\livePCs.txt"
        }
        else {
            Add-Content -value $computer -path "$exportLocation\deadPCs.txt"
        }
    }


    # Now that we know which PCs are live on the network
    # proceed with the inventory

    $computers = Get-Content -Path "$exportLocation\livePCs.txt"

    foreach ($computer in $computers) {
        $Bios = Get-WmiObject -Class win32_bios -ComputerName $Computer
        $Hardware = Get-WmiObject -Class Win32_computerSystem -ComputerName $Computer
        $Sysbuild = Get-WmiObject -Class Win32_WmiSetting -ComputerName $Computer
        $OS = Get-WmiObject -Class Win32_OperatingSystem -ComputerName $Computer
        $Networks = Get-WmiObject -Class Win32_NetworkAdapterConfiguration -ComputerName $Computer | Where-Object { $_.IPEnabled }
        $driveSpace = Get-WmiObject -Class win32_volume -ComputerName $Computer -Filter 'drivetype = 3' |
        Select-Object -Property PScomputerName, driveletter, label, @{LABEL = 'GBfreespace'; EXPRESSION = { '{0:N2}' -f ($_.freespace / 1GB) } } |
        Where-Object { $_.driveletter -match 'C:' }
        $cpu = Get-WmiObject -Class Win32_Processor  -ComputerName $computer
        $username = Get-ChildItem "\\$computer\c$\Users" | Sort-Object -Property LastWriteTime -Descending | Select-Object -Property Name, LastWriteTime -First 1
        $totalMemory = [math]::round($Hardware.TotalPhysicalMemory / 1024 / 1024 / 1024, 2)
        $lastBoot = $OS.ConvertToDateTime($OS.LastBootUpTime)

        $IPAddress = $Networks.IpAddress[0]
        $MACAddress = $Networks.MACAddress
        $systemBios = $Bios.serialnumber

        $OutputObj = New-Object -TypeName PSObject
        $OutputObj | Add-Member -MemberType NoteProperty -Name ComputerName -Value $Computer.ToUpper()
        $OutputObj | Add-Member -MemberType NoteProperty -Name Manufacturer -Value $Hardware.Manufacturer
        $OutputObj | Add-Member -MemberType NoteProperty -Name Model -Value $Hardware.Model
        $OutputObj | Add-Member -MemberType NoteProperty -Name Processor_Type -Value $cpu.Name
        $OutputObj | Add-Member -MemberType NoteProperty -Name System_Type -Value $Hardware.SystemType
        $OutputObj | Add-Member -MemberType NoteProperty -Name Operating_System -Value $OS.Caption
        $OutputObj | Add-Member -MemberType NoteProperty -Name Operating_System_Version -Value $OS.version
        $OutputObj | Add-Member -MemberType NoteProperty -Name Operating_System_BuildVersion -Value $SysBuild.BuildVersion
        $OutputObj | Add-Member -MemberType NoteProperty -Name Serial_Number -Value $systemBios
        $OutputObj | Add-Member -MemberType NoteProperty -Name IP_Address -Value $IPAddress
        $OutputObj | Add-Member -MemberType NoteProperty -Name MAC_Address -Value $MACAddress
        $OutputObj | Add-Member -MemberType NoteProperty -Name Last_User -Value $username.Name
        $OutputObj | Add-Member -MemberType NoteProperty -Name User_Last_Login -Value $username.LastWriteTime
        $OutputObj | Add-Member -MemberType NoteProperty -Name C:_FreeSpace_GB -Value $driveSpace.GBfreespace
        $OutputObj | Add-Member -MemberType NoteProperty -Name Total_Memory_GB -Value $totalMemory
        $OutputObj | Add-Member -MemberType NoteProperty -Name Last_ReBoot -Value $lastboot
        $OutputObj | Export-Csv -Path "$exportLocation\pcsinventory.csv" -Append -NoTypeInformation
    }
}
```

# Enlaces internos

- [soka.gitlab.io/PSFabrik/](https://soka.gitlab.io/PSFabrik/): Web estática sobre PS.
- ["Windows Management Instrumentation (WMI)"](https://soka.gitlab.io/PSFabrik/networking/wmi/wmi-intro/): Post sobre WMI en PSFabrik.

# Enlaces externos

- ["Remote Computer Inventory with PowerShell"](https://www.signalwarrant.com/remote-computer-inventory-powershell/): **Fuente de este artículo**.
