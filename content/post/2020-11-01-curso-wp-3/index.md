---
title: Primeros pasos como administrador
subtitle: Sesión nº 1 curso WordPress
date: 2020-11-01
draft: false
description: Descripción de panel de control de administradores
tags: ["Wordpress","WP - Curso","WP","WP - Introducción"]
---

![](img/wordpress-logo-01.png)

## Pad de trabajo

[https://pad.riseup.net/p/penascalf52021](https://pad.riseup.net/p/penascalf52021)

## Contenidos

1. Inicio de sesión en WordPress.
2. Panel administrador.
3. Modificando la información del sitio.

## Pad de trabajo

[https://pad.riseup.net/p/penascalf52021](https://pad.riseup.net/p/penascalf52021)

## Inicio de sesión en WordPress

Por norma general, el inicio de sesión en cualquier sitio creado con WordPress, está en la siguiente URL: https://paginaweb.com/wp-admin ([acceso WP Peñascal F5](https://www.peñascalf5.org/promo2021/wp-admin/)).

![](img/01.png)

Hay excepciones, por supuesto, de hecho **por motivos de seguridad es recomendable cambiarlo**, usar [.htaccess](https://es.wikipedia.org/wiki/.htaccess) como medida adicional es común en [Apache](https://es.wikipedia.org/wiki/Servidor_HTTP_Apache).

## Panel administrador

Una vez dentro vemos el escritorio o el panel de administrador, en la parte superior izquierda siempre aparecerá el nombre del sitio.

![](img/02.png)

En la parte superior está la barra de administrador. Contiene lo siguiente:

![](img/03.png)

- Un menú desplegable con el logo de WP con una serie de enlaces **Acerca de WordPress** (detalles sobre la instalación de WP), [**WordPress.org**](https://es.wordpress.org/), [**Documentación**](https://codex.wordpress.org/) (en codex.wordpress.org), [**Soporte**](https://es.wordpress.org/support/) y [**Sugerencias**](https://es.wordpress.org/support/forum/comunidad/peticiones-y-feedback/).
- Un enlace a la parte pública de tu sitio.
- Actualizaciones y actividad.
- Comentarios de visitantes.
- Añadir: Entrada, medio, página, usuario.
- Finalmente en la esquina derecha sale nuestro perfil de usuario.

También te habrás fijado en pestaña **"Opciones de pantalla"** (que aparecerá en otras muchas ventanas de wp-admin), si haces clic y la despliegas se muestra un _checklist_ de elementos para mostrar o ocultar en la página que estamos. Estas opciones van cambiando en cada página (te invito a que pruebes estas opciones y lo pongas a tu gusto).  

Junto a la pestaña **"Opciones de pantalla"** existe una de **"Ayuda"**.

![](img/04.png)

En el lateral izquierda está el **menu principal vertical**:

![](img/05.png)

Puedes hacer clic sobre cada elemento y acceder al apartado correspondiente o parar el cursor por encima para ver las secciones anidadas.

El menu superior y el menú principal existen en todas las páginas de wp-admin.

## Modificando la información del sitio

Necesitarás cambiar la información general del sitio después de la instalación (el título y una breve descripción), ahora nos centraremos en las opciones principales (**Ajustes | Generales**).

![](img/06.png)

Otra de las opciones que querrás cambiar es la **zona horaria** (Madrid o UTC +1) (es importante sobre todo para la cronología de los _posts_ o para programar su publicación por ejemplo). 

Otra opción que debes considerar es si vas a permitir la suscripción de usuarios y el perfil por defecto.

![](img/07.png)

**Nota**: Acuérdate de salvar los cambios cuando acabes.

Antes de acabar sólo hay un ajuste más a realizar antes de públicar cualquier contenido: los enlaces permanentes (_permalinks_) en "Ajustes > Enlaces permanentes".

![](img/08.png)

## Cómo permitir que nuestros visitantes se registren como usuarios en WordPress

{{< youtube r_M8etd4vsA  >}}

## Enlaces externos

- [https://codex.wordpress.org/es:Introduction_to_Blogging](https://codex.wordpress.org/es:Introduction_to_Blogging).