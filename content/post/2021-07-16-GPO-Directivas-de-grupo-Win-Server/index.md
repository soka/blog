---
title: Directivas de grupo en Windows Server
subtitle: 
date: 2021-07-16
draft: true
description: 
tags: ["Windows","GPO","Windows Server","Políticas","Directivas"]
---

<!-- vscode-markdown-toc -->
* 1. [Enlaces internos](#Enlacesinternos)
* 2. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Las directivas de grupo o GPO del inglés (Group Policy Object) proporcionan una gestión centralizada de un conjunto de reglas que se aplican sobre los usuarios o equipos (sin importar el usuario que inicie sesión) de un dominio AD (Directorio Activo o Active Directory).

Configuraciones:

* Plantillas administrativas basadas en el registro: Componentes de Windows, menú inicio y barra de tareas, escritorio, panel de control, carpetas compartidas, red, sistema. Ejemplos: No mostrar la pantalla de bienvenida al iniciar sesión, configurar ubicaciones de búsqueda de controladores, etc. 
* Configuración de seguridad: Autenticación en red, recursos autorizados, eventos registrados. 
* Instalación de software.
* Secuencia de comandos que se ejecuten automáticamente cuando se inicia o cierra la sesión en un equipo.
* Servicios de instalación remota.
* Mantenimiento de IE para pernalizar el navegador.
* Redireccionamiento. 

Se aplica en AD a nivel de sitio, dominios y OU (Unidad Organizativa). 

##  1. <a name='Enlacesinternos'></a>Enlaces internos

##  2. <a name='Enlacesexternos'></a>Enlaces externos