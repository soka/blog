---
title: Crear una web estática con MkDocs y GitLab Pages [en progreso]
subtitle:  
date: 2022-07-06
draft: false
description: Primera aproximación práctica al mundo de los contenedores y en concreto a Docker
tags: ["GitLab","GitLab Pages","MkDocs","Web","Desarrollo"]
---

## Instalación 

### Python y pip

MkDocs requiere Python y su gestor de paquetes pip. Podemos comprobar si tenemos Python instalado y la versión con el comando `python --version` (mi versión es "Python 2.7.18rc1"). Si necesitas instalar Python en Debian (o cualquier distribución con APT) es tan sencillo como hacer `sudo apt install python3`.

El interprete de Python reside en /usr/bin:

```bash
$ ls /usr/bin/python
python     python2    python2.7  python3    python3.8  
$ ls -l /usr/bin/python
lrwxrwxrwx 1 root root 7 Apr 15  2020 /usr/bin/python -> python2
```

Para usar la última versión crearé un enlace a la versión 3.8 así `sudo ln -s /usr/bin/python3.8 /usr/bin/python`. Ahora si ejecuto `python --version` el resultado es "Python 3.8.2".  

Ahora instalo el gestor de paquetes pip para Python3 con `sudo apt install python3-pip`. Una vez finalizada la instalación puedo comprobar la versión con `pip3 --version`.

### MkDocs

Por fin llega el ansiado momento de instalar MkDocs usando pip:

```bash
pip install mkdocs
```

Puedes comprobar antes si ya lo tienes instalado con `mkdocs --version`, la primera vez es probable que no funcione porqué no encuentra la ruta a la aplicación (`echo $PATH`). Para añadir un directorio a la variable de entorno PATH usamos este comando:

```bash
export PATH=$HOME/.local/bin:$PATH
```

## Creando nuestro primer proyecto


```bash
$ mkdocs new my-project
INFO     -  Creating project directory: my-project
INFO     -  Writing config file: my-project/mkdocs.yml
INFO     -  Writing initial docs: my-project/docs/index.md
$ cd my-project/
```

El comando crea dentro de la carpeta de proyecto un archivo "mkdocs.yml" con los parámetros de configuración necesarios. La carpeta docs contiene los fuentes con las documentación en formato MarkDown.

### Previsualizar en local

MkDocs incluye un servidor web local para visualizar el resultado, ejecutamos `mkdocs serve` dentro de la carpeta de proyecto y abrimos en el naveador la URL http://127.0.0.1:8000, ya tenemos nuestro sitio web en funcionamiento:

![](img/01.png) 

### Configuración mkdocs.yml

El fichero de configuración recién creado sólo contiene una línea con el título del sitio: 

```bash
$ cat mkdocs.yml 
site_name: My Docs
```

Prueba a cambiarlo por otro de tu elección, si estás previsualizando la página web verás que cambia el título inmediatamente. 

### Añadiendo nuevas páginas

Dentro de la carpeta docs del proyecto creamos un nuevo archivo, por ejemplo "about.md", sin necesidad de hacer nada más aparecerá un nuevo elemento en el menú superior horizontal de la página.


### Build

```bash
mkdocs build
```

## Enlaces internos


## Enlaces externos

