---
title: Crear un dominio en Windows Server 2012 R2 [borrador]
subtitle: Administración de Windows Server
date: 2020-03-12
draft: true
description: Administración servidores Windows
tags: ["Windows","Sistemas","PowerShell","Administración","Windows Server","Dominio"]
---

![](img/win-server-logo.png)


## Enlaces internos

- [Administración de servicios con Powershell](https://soka.gitlab.io/blog/post/2020-01-05-admin-servicios-win-ps/).
- [Administración de usuarios y grupos locales con Powershell](https://soka.gitlab.io/blog/post/2020-01-04-admin-usuarios-y-grupos-locales-windows-ps/).
- [Abrir aplicaciones de administración desde línea de comandos](https://soka.gitlab.io/blog/post/2020-01-03-lanzar-aplicaciones-admin-desde-cli-windows/).
- [Servidor DNS](https://soka.gitlab.io/blog/post/2019-11-23-windows-server-dns-5/).
- [Instalación del rol de servidor DHCP](https://soka.gitlab.io/blog/post/2019-11-09-windows-server-dhcp-4/).
- [Introducción a PowerShell](https://soka.gitlab.io/blog/post/2019-11-08-windows-server-ps-3/).
- [Windows Server 2012, primeros pasos y configuración básica](https://soka.gitlab.io/blog/post/2019-11-06-windows-server-2/).
- [Instalación de Windows Server 2012 virtualizado en Debian](https://soka.gitlab.io/blog/post/2019-11-05-windows-server-1/).

## Referencias externas

