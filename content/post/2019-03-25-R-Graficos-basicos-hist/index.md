---
title: R - Gráficos básicos
subtitle: Histogramas con hist()
date: 2019-03-25
draft: false
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "Programación",
    "R - Básico",
    "R - Gráficos",
    "R - Diagramas",
    "Diagramas",
  ]
---

Un **histograma** es una representación gráfica de una variable en forma de barras, donde la superficie de cada barra es proporcional a la frecuencia de los valores representados ([Wikipedia](https://es.wikipedia.org/wiki/Histograma)). Permite observar la distribución de los datos agrupados por clases. El eje horizontal normalmente representa los valores de la variable.

```R
hist(c(2,2,2,2,2,3,3,3,4,4,5,5))
```

Produce este diagrama:

![](images/hist_01.PNG)

Los valores del vector están entre 2 y 5, el eje X se agrupa en intervalos de 0.5, empezando de 2.0. La altura Y indica el número de coincidencias de elementos del vector en ese intervalo, de 2.0 a 2.5 tenemos 5 elementos del vector con valor 2.

Ahora vamos a hacer algo más interesante generando números racionales (con decimales) aleatorios con `runif()` (si queremos generar enteros usamos `sample()`).

```R
x <- runif(100)
hist(x)
```

El diagrama muestra una distribución bastante uniforme de los 100 números.

![](images/hist_02.PNG)

Si vamos añadiendo más y más números aleatorios obtendremos una distribución aproximadamente normal ([distribución de Gauss](https://es.wikipedia.org/wiki/Distribuci%C3%B3n_normal)) con forma acampanada y simétrica.

```R
rm(x)
par(mfrow=c(2,2))
x <- runif(10)
hist(x)
x <- x + runif(100)
hist(x)
x <- x + runif(1000)
hist(x)
x <- x + runif(10000)
hist(x)
```

Histograma (se posicionan gráficos de izquierda a derecha y de arriba abajo):

![](images/hist_03.PNG)

# Parámetros

Vamos a generar un serie de datos que siga la distribución [chi cuadrado](https://es.wikipedia.org/wiki/Prueba_%CF%87%C2%B2) (Pearson)

```R
x <- rchisq(1000,10) # Generar 1000 números distribución chi-cuadrado
hist(x,
     main = "Histograma\nChi-cuadrado",
     xlab = "Valores",
     ylab = "Probabilidades",
     br=c(c(0.5),c(5,15),5*3:6), # Intervalos de las clases
     xlim = c(0,30),  # Límite eje x
     ylim = c(0,0.1),
     col="lightblue",  # Color columnas
     border = "white", # Color de los bordes de las columnas
     prob = T,
     right = T,
     adj = 0,
     col.axis="red"
     )
```

![](images/hist_04.PNG)

# Ejemplos

Supongamos que tenemos un conjunto de datos que representan los minutos dedicados por un grupo de personas a una tarea por ejemplo. En este histograma vamos a introducir dos novedades, definimos el número de clases (`nc = 6`) y los intervalos cerrados a la izquierda (`right = FALSE`), significa que para casa clase o intervalo el valor izquierdo entra dentro de esa clase pero no el derecho, para entenderlo mejor viendo el diagrama por ejemplo para la primera clase se contemplan los datos donde `16 <= x < 18`.

```R
hist(datos, # Conjunto de datos
     nc=6, # Número de clases == 6
     right = FALSE, #Intervalo cerrado a la izquierda
     main = "Histograma",
     xlab = "tiempo (en minutos)",
     ylab = "Frecuencia",
     col=8
     )
```

![](images/hist_05.PNG)

# Ejercicios

- r / intro / 29-hist / [hist.R](https://gitlab.com/soka/r/blob/master/intro/29-hist/hist.R).

# Enlaces

- ["R - Gráficos básicos: Función plot()"](https://soka.gitlab.io/blog/post/2019-03-18-r-graficos-basicos-plot/).
- ["Diagramas de dispersión y modelo de regresión lineal en R"](https://soka.gitlab.io/blog/post/2019-02-05-diagramas-dispersion-en-r/).
- ["Diagramas tipo tarta en R"](https://soka.gitlab.io/blog/post/2019-02-14-diagramas-de-tarta-en-r/): Gráficos para variables cualitativas con pie().
- ["Recetas para hacer diagramas en R como lo hace la BBC"](https://soka.gitlab.io/blog/post/2019-02-19-graficos-r-estilo-bbc/).
- ["Diagramas de barras en R"](https://soka.gitlab.io/blog/post/2019-02-21-diagramas-barras-en-r/): Usando la función barplot().

- ["Gráficos en R: Histogramas"](http://www.dma.ulpgc.es/profesores/personal/stat/cursoR4ULPGC/9c-grafHistograma.html).
- ["Números aleatorios en R utilizando las funciones sample y runif"](https://vivaelsoftwarelibre.com/genera-numeros-aleatorios-r/).
- ["Distribuciones de probabilidad en R"](http://www.dma.ulpgc.es/profesores/personal/stat/cursoR4ULPGC/10-distribProbabilidad.html#ejemplo2:_distribuci%C3%B3n_normal).
