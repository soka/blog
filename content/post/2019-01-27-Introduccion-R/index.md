---
title: R para principiantes
subtitle: Introducción e instalación
date: 2019-01-27
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico"]
---

# ¿Que es R?

[**R**](<https://es.wikipedia.org/wiki/R_(lenguaje_de_programaci%C3%B3n)>) es un lenguaje de programación de código abierto y con [licencia](https://www.r-project.org/Licenses/) libre GNU GPL, es gratis y está ganando más adeptos día a día. Proviene del lenguaje estadístico [**S**](<https://en.wikipedia.org/wiki/S_(programming_language)>).

Está especialmente pensado para la manipulación y análisis estadístico de grandes volúmenes de datos, se usa mucho en el campo de la minería de datos, permite representaciones de forma gráfica y también es muy usado para modelos predictivos, IA o ML.

Lo más importante es que funciona con paquetes (¡R es sus paquetes!) que otras personas han desarrollado y podemos descargar gratis. Todo está disponible en la Web **CRAN - [https://cran.r-project.org/](https://cran.r-project.org/)** (The Comprehensive R Archive Network).

![](images/cran_main_web.PNG)

**Características y ventajas de R**:

- R es abierto y colaborativo.
- Es un lenguaje interpretado.
- Amplia gama de herramientas estadísticas.
- Gráficos de alta calidad.
- Puede manejar grandes volúmenes de datos.
- Se integra con varias bases de datos, tanto SQL como NoSQL.
- Multiplataforma.
- Funcionalidad extensible con paquetes.
- Comunidad muy activa.

# Instalación

## Interprete R

El interprete de R se puede descargar para Linux, Mac o Windows. Yo me he descargado el binario instalador para Win que se puede descargar de este enlace ["Download R 3.5.2 for Windows"](https://cran.r-project.org/bin/windows/base/R-3.5.2-win.exe).

![](images/rgui_01.PNG)

## IDE RStudio

Un complemento indispensable es un IDE o un entorno de desarrollo integrado, [**RStudio**](https://www.rstudio.com/) es el más conocido.

![](images/rstudio_main_web.PNG)

Existen versiones de pago de **RStudio** y también de código abierto para diferentes sistemas operativos, yo me he descargado [RStudio-1.1.463](https://download1.rstudio.org/RStudio-1.1.463.exe) para MS Win.

![](images/rstudio-ide-01.PNG)

**RStudio incluye**:

- Consola.
- Editor de sintaxis.
- Herramientas para el trazado.
- Depuración.
- Gestión del espacio de trabajo.
- Resaltado de sintaxis.
- Autocompletado.
- Sangría inteligente.
- Ejecución del código.
- Como entorno colaborativo: Documentación y soporte integrado, proyectos, visor de datos, etc.
- Autoría de contenidos con R Markdown y [Sweave](https://support.rstudio.com/hc/en-us/articles/200552056-Using-Sweave-and-knitr).

Tanto en la consola de R como en RStudio una de las operaciones que podemos hacer son operaciones aritméticas. Si escribimos una suma inmediatamente se muestra el resultado.

```
> 1+1
[1] 2
>
```

![](images/rgui_suma.PNG)

# Primeros pasos con RStudio

Lo primero es averiguar en que ruta estamos trabajando usando la función [`getwd()`](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/getwd) (get working directory).

```
> getwd()
[1] "C:/Users/i.landajuela/Documents"
```

Si queremos cambiar el directorio podemos usar RStudio o la función [`setwd()`](https://www.statmethods.net/interface/workspace.html)

![](images/rstudio-ide-02.PNG)

```
> setwd("C:/Users/i.landajuela/Documents")
```

Podemos ir ejecutando el código línea a línea usando el botón _Run_ de RStudio, si te gustan los atajos de teclado como a mí puedes usar CTRL+ENTER.

Las cadenas en R se pueden definir con comillas simples o dobles:

```R
"Hola mundo" ; 'Hola mundo'
```

# Ayuda

Usando '?' por delante nos muestra la ayuda:

```R
?solve
```

En el área de consola usando las flechas arriba y abajo podemos recorrer los últimos comandos ejecutados.

Podemos cargar un script de R desde la consola, probablemente primero debamos cambiar la ruta con `setwd`, también se puede cambiar la ruta actual desde la ventana _Files_ -> _Set As Working Directory_ de RStudio.

```R
> source("operadores.R")
```

Veremos como en la ventana entorno de RStudio se cargan las variables del script.

Si quiero cargar los resultados a un fichero en lugar de la consola:

```R
> sink("resultados.log")
```

Volvemos a mostrar por consola con `sink()`.

Con el comando `objects()` podemos ver las variables de nuestro entorno.

```R
> objects()
[1] "a" "x" "y"
```

Podemos borrar las variables con `rm()`:

```R
rm(a,x,y)
```

# Enlaces

- Mi repositorio GitLab con un proyecto para ejercicios en R [https://gitlab.com/soka/r](https://gitlab.com/soka/r).
- [Enlaces posts en R en esta Web](https://soka.gitlab.io/blog/tags/r/).
- [`getwd()`](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/getwd).
- ["Quick-R: The Workspace"](https://www.statmethods.net/interface/workspace.html).

# R vs Python

![](images/R-vs-Python-1-DataCamp.png)

![](images/R-vs-Python-2-DataCamp.png)

![](images/R-vs-Python-Pros-Cons-DataCamp.jpg)

[[Fuente](https://www.stoodnt.com/blog/r-vs-python-metareview-usability-popularity-pros-cons-jobs-salaries/)]
