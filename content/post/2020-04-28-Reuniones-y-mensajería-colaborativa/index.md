---
title: Reuniones y mensajería con herramientas alternativas
subtitle: Herramientas de trabajo colaborativo remoto
date: 2020-04-28
draft: false
description: 
tags: ["Aplicaciones","Web","Videoconferencia","Mensajería","Trabajo colaborativo"]
---

## Jitsi - Aplicación multiplataforma de código abierto para videoconferencias

Esta la conozco desde ayer por recomendación de un compi en Twitter ([https://twitter.com/GaurDaBihar](https://twitter.com/GaurDaBihar)).  [Jitsi](https://jitsi.org/) es una aplicación de videoconferencia, multiplataforma y de código abierto (y 100% gratis).

![](img/01.png)

La aplicación Web no requiere registro previo, usando este enlace [https://meet.jit.si/](https://meet.jit.si/) le damos nombre a la sala y estamos dentro.  Combina videoconferencia y un chat incrustado, permite compartir pantalla con el resto de participantes, invitar al resto usando una URL (protegido con o sin contraseña), el creador de la sesión puede en cualquier momento silenciar el micro del resto o incluso expulsar a otros miembros.

## Matrix

Matrix es un [estándar de código abierto](https://matrix.org/docs/spec) para ofrecer sistemas de comunicación descentralizados, cifrados y en tiempo real.

Matrix se puede probar en Web (Chrome & Firefox) usando este enlace [https://riot.im/app/](https://riot.im/app/) aunque está disponible para teléfonos móviles (iOS & Android) y [aplicaciones de escritorio](https://riot.im/download/desktop/) (macOS, Windows & Linux)

![](img/02.png)

Sus funcionalidades son similares a las de otras aplicaciones de mensajería instantánea, 


## Referencias externas

- [Jitsi Meet](https://jitsi.org/jitsi-meet/).
- [Matrix es un estándar open source que permitirá nuevas aplicaciones de mensajería cifradas y descentralizadas: así puedes probarlo](https://www.genbeta.com/actualidad/matrix-estandar-open-source-que-permitira-nuevas-aplicaciones-mensajeria-cifradas-descentralizadas-asi-puedes-probarlo).
- Librezale.eus [Komunikazio eta elkarlanerako baliabideak](https://librezale.eus/2020/03/15/komunikazio-eta-elkarlanerako-baliabideak/).



