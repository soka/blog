---
title: Vectores en R
subtitle: Tipos de datos
date: 2019-03-11
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico", "R - Datos", "R - Vectores"]
---

![](images/r-logo.PNG)

Los vectores son objetos que almacenan más de un valor. Vamos a empezar creando un vector numérico, para ello usamos la función `c()` que une los argumentos que le pasamos en un vector de salida, este sería el ejemplo más sencillo donde creamos el vector y asignamos a una variable `v1`:

```R
> v1 <- c(1,2,3)
> class(v1)
[1] "numeric"
> is.vector(v1)
[1] TRUE
```

Podemos usar `assign` para hacer lo mismo:

```R
> assign("v1",c(4,5,6))
> length(v1)
[1] 3
```

Incluso podemos asignar con la "flecha" hacia la derecha:

```R
c(1,2.3,3,4,5) -> v1
```

![](images/cheatsheet-creating-vectors.PNG)

## Indexar

Podemos indexar un valor del vector por su posición numérica:

```R
> v1[1]
[1] 1
```

![](images/cheatsheet-select-vector-els.PNG)

## Secuencias

Podemos generar el vector numérico de un rango de valores secuenciales:

```R
> v2 <- c(4:6)
> v2
[1] 4 5 6
```

Podemos hacer lo mismo con la función ´seq()´ donde indicamos el inicio, final y el salto:

```R
> vsec <- seq(1,10,2)
> vsec
[1] 1 3 5 7 9
```

Si hacemos una secuencia descendente usamos un valor negativo para el salto (sino produce error):

```R
> vsec <- seq(10,1,-3)
> vsec
[1] 10  7  4  1
```

## Sumar valores numéricos de dos vectores

Ahora vamos a crear un nuevo vector resultante de sumar los dos anteriores, suma el primer elemento de `v1` con el primero de `v2` y así sucesivamente:

```R
> v3 <- v1+v2
> v3
[1] 5 7 9
```

## Vectores de otros tipos básicos

Usando la misma función podemos crear vectores de carácteres:

```R
v4 <- c("Hola","Mundo")
```

Ojo porque si tratamos de sumar vectores de carácteres produce error (por ejemplo `v1` y `v4`). Para determinar de que tipo es un vector:

```R
> v5 <- c(1,2,5.3,6,-2,4)
> is.numeric(v5)
[1] TRUE
> class(v5)
[1] "numeric"
```

También como no podemos crear un **vector lógico**:

```R
> v6 <- c(TRUE,TRUE,FALSE,TRUE)
> class(v6)
[1] "logical"
```

Creamos un vector de valores lógicos resultante de una comparación de todo el vector contra un único valor:

```R
> vNum <- c(1,2,4,10,5,6)
> vLogico <- vNum > 2
> vLogico
[1] FALSE FALSE  TRUE  TRUE  TRUE  TRUE
```

## Repetición de valores

Otra función útil para producir vectores es `rep()` que devuelve el primer argumento repetido el número de veces indicado por el segundo argumento:

```R
> v7 <- rep(1,10)
> v7
 [1] 1 1 1 1 1 1 1 1 1 1
```

## Vectores dentro de vectores

Vectores dentro de vectores, el resultado es un nuevo vector `num[1:12]`:

```R
> x <- c(1,2,3,4,5)
> y <- c(x,6,7,-x)
> y
 [1]  1  2  3  4  5  6  7 -1 -2 -3 -4 -5
```

## Operaciones vectoriales

Produce un _warning_ pero hace lo que buenamente puede:

```R
> x <- c(1,2)
> y <- c(3,4,5)
> z <- x*y
Warning message:
In x * y : longer object length is not a multiple of shorter object length
> z
[1] 3 8 5
```

Suma 1 a todos los valores del vector:

```R
> x <- c(1,2,4)
> x <- x + 1
> x
[1] 2 3 5
```

## Comparando valores

```
> myVector <- c(1:5)
> myVector
[1] 1 2 3 4 5
> myVector > 2
[1] FALSE FALSE  TRUE  TRUE  TRUE
> which(myVector > 2)
[1] 3 4 5
```

Si queremos obtener los valores que cumplan una condición lógica:

```R
> vNum <- c(1,2,4,10,5,6)
> # Con esto obtenemos los valores que cumplen la condición
> vNum[vNum>2]
[1]  4 10  5  6
```

## Otros operadores y funciones

```R
# otros operadores log, exp, sin, cos, sqrt
sqrt(2)
max(x)
min(x)
# longitud de un vector
length(x)
# Suma de sus valores
sum(x)
# Multiplicación de sus elementos
prod(x)
# La media y equivalente
mean(x); sum(x)/length(x)
# Varianza
var(x); sum((x - mean(x))^2)/(length(x)-1)
# ordenar
order(x); sort.list(x)
x[order(x)]
# Decreciente
x[order(x,decreasing = TRUE)]
```

## Muestras aleatorias y permutaciones con sample

La función `sample` permite construir muestras aleatorias y permutaciones. Para el siguiente ejemplo vamos a simular las tiradas de un dado 1000000 veces.

A la función `sample` le proporcionamos como entrada un vector con valores de 1 a 6. Generamos 1000000 de esos valores, con `replace=TRUE` le indicamos que los valores se pueden repetir, `prob = NULL` produce una muestra balanceada donde la probabilidad de cualquier valor es 1/6.

```R
> sample.dado <- sample(1:6,1000000,replace = TRUE,prob = NULL)
> head(sample.dado,n=100)
  [1] 2 1 2 4 2 2 1 1 4 4 3 6 3 2 6 2 4 2 5 6 2 1 3 3 2 3 5 5 4 3 5 4 2 2 6 2 1 2 6 3 2 6 2
 [44] 5 1 2 2 4 2 4 3 4 4 2 3 3 1 6 3 1 6 2 1 4 3 3 6 2 4 2 4 2 1 5 1 2 6 2 3 4 2 4 2 1 6 6
 [87] 1 5 1 4 1 4 3 6 1 3 5 4 5 5
```

Para “contar” cuántos valores salieron de cada tipo se tiene que:

```R
> levels(fc)
[1] "1" "2" "3" "4" "5" "6"
> table(fc)
fc
     1      2      3      4      5      6
166871 166690 166319 167861 166576 165683
> # Al pedir un sumario de la variable de tipo factor 'fc',
> # el resultado es una tabla que nos muestra los niveles del factor puntos de la tirada
> # junto con el número de muestras de cada tirada.
> summary(fc) / 1000000
       1        2        3        4        5        6
0.166871 0.166690 0.166319 0.167861 0.166576 0.165683
```

Lo que permite visualizar que cada valor tiene una probabilidad de ocurrencia cercana a 1/6.

La función `factor` obtiene una variable categórica con un número finito de valores o niveles. En R los factores se utilizan habitualmente para realizar clasificaciones de los datos, estableciendo su pertenencia a los grupos o categorías determinados por los niveles del factor. Se representan como **vectores unidimensionales** que incluye los valores correspondientes a una variable categórica.

Para obtener un **factor ordenado** usamos la función `ordered()`.

## Ejercicios

- r / intro / 19-Vectores / [vectores.R](https://gitlab.com/soka/r/blob/master/intro/19-Vectores/vectores.R).

## Enlaces
