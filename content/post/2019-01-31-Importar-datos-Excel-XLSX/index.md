---
title: Importar datos XLSX (Excel) 
subtitle: Trabajando con hojas de cálculo en R
date: 2019-01-31
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico","Excel","XLSX"]
---

El formato XLSX es el formato predeterminado de MS Excel para elaborar hojas de cálculo, para poder trabajar con este tipo de ficheros es necesario instalar previamente la librería correspondiente y despúes cargarla en nuestro script.

```R
install.packages("xlsx")
# Apuntamos a la versión x64 en MS Win 10
Sys.setenv(JAVA_HOME="C:\\Program Files\\Java\\jre1.8.0_201")
library(xlsx)
#?read.xlsx
setwd("C:\\Users\\i.landajuela\\Documents\\GitLab\\r\\intro\\09-importar-xlsx")
datos.xlsx = read.xlsx("datos.xlsx",1)
```

Podemos visualizar el contenido formateado como una rejilla en una nueva pestaña de RStudio:

```R
View(datos.xlsx)
```

![](images/view_xlsx.PNG)

# Ejercicios

* r / intro / 09-importar-xlsx / [importar-xlsx.R](https://gitlab.com/soka/r/blob/master/intro/09-importar-xlsx/importar-xlsx.R).
* r / intro / 09-importar-xlsx / [datos.xlsx](https://gitlab.com/soka/r/blob/master/intro/09-importar-xlsx/datos.xlsx).

# Enlaces

**Enlaces externos:**

* ["R xlsx package : A quick start guide to manipulate Excel files in R"](http://www.sthda.com/english/wiki/r-xlsx-package-a-quick-start-guide-to-manipulate-excel-files-in-r). 
* ["Libreria XLSX en R"](https://rstudio-pubs-static.s3.amazonaws.com/282636_7933fef0d33b48248d0f175a6c0778bb.html).