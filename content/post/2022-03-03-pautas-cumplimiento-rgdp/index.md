---
title: Pautas y recursos cumplimiento RGDP
subtitle: Guía
date: 2022-03-03
draft: false
description: Guía
tags: ["RGDP","LOPD","Protección datos","Legal"]
---

<!-- vscode-markdown-toc -->
* 1. [Consentimiento de los clientes](#Consentimientodelosclientes)
* 2. [Plantillas consentimiento cliente](#Plantillasconsentimientocliente)
	* 2.1. [Consentimiento explicito COMERCIAL con_datos completos del interesado](#ConsentimientoexplicitoCOMERCIALcon_datoscompletosdelinteresado)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

La [RGPD 679/2016](https://www.boe.es/doue/2016/119/L00001-00088.pdf) (Reglamento de Protección de Datos) entro en vigor el 2018 para toda la UE (Unión Europea), de obligado cumplimiento para empresas, organizaciones e instituciones públicas. También resulta de interés en este caso la [LOPDGDD 3/2018](https://www.boe.es/boe/dias/2018/12/06/pdfs/BOE-A-2018-16673.pdf) (Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales).

![](img/01.png)


##  1. <a name='Consentimientodelosclientes'></a>Consentimiento de los clientes

Este es un ejemplo de un posible modelo obtenido en Internet ([descarga en PDF](doc/Consentimiento_explicito_COMERCIAL_con_datos_completos_del_interesado.pdf))

![](doc/Consentimiento_explicito_COMERCIAL_con_datos_completos_del_interesado-1.png)

En el preámbulo de identifica al responsable de tratamiento de datos de nuestra empresa y se hace referencia a las leyes vigentes. 

A continuación se indican el **fin y la legitimación del tratamiento**, para ello hace referencia al Artículo 6 UE RGPD "Licitud del tratamiento".

El artículo 6.1 a RGDP dice:

_"el interesado dio su consentimiento para el tratamiento de sus datos personales para uno o varios fines específicos;"_

El artículo 6.1 f RGPD que dice:

_"el tratamiento es necesario para la satisfacción de intereses legítimos perseguidos por el responsable del tratamiento o por un tercero, siempre que sobre dichos intereses no prevalezcan los intereses o los derechos y libertades fundamentales del interesado que requieran la protección de datos personales, en particular cuando el interesado sea un niño."_ 

De este modo justifica el uso de datos para mantener una relación comercial.

Más adelante se informa sobre el [periodo de conservación de datos](https://www.cuatrecasas.com/es/spain/articulo/plazos-maximos-conservacion-datos-personales-atencion-ultimo-informe-aepd) y la comunicación de datos a terceros.

Los derechos que asisten al cliente son los derechos ARCO (Acceso, Rectificación, Cancelación, Oposición) y el de reclamación ante la AEPD (Agencia Española de Protección de Datos).

![](img/02.png)

##  3. <a name='Enlacesexternos'></a>Enlaces externos

* [https://www.adslzone.net/2018/11/05/manual-rgpd-pymes-autonomos/](https://www.adslzone.net/2018/11/05/manual-rgpd-pymes-autonomos/).
* [https://ayudaleyprotecciondatos.es/guia-rgpd/](https://ayudaleyprotecciondatos.es/guia-rgpd/).
* [https://www.incibe.es/protege-tu-empresa/rgpd-para-pymes](https://www.incibe.es/protege-tu-empresa/rgpd-para-pymes).
* [https://www.aepd.es/sites/default/files/2019-11/guia-modelo-clausula-informativa.pdf](https://www.aepd.es/sites/default/files/2019-11/guia-modelo-clausula-informativa.pdf).
* Ejemplo [https://www.incibe.es/proteccion-datos-personales](https://www.incibe.es/proteccion-datos-personales).
* [https://www.aepd.es/es/guias-y-herramientas/herramientas/facilita-rgpd](https://www.aepd.es/es/guias-y-herramientas/herramientas/facilita-rgpd).