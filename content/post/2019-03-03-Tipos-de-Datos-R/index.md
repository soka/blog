---
title: Tipos de datos básicos en R
subtitle: Logical - Character - Integer - Numeric
date: 2019-03-03
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico","R - Datos"]
---
En **R** (como en otros lenguajes de programación) hay dos clases fundamentales de datos: **numéricos y alfanuméricos** (o cadenas, strings en inglés). La función `class()` nos devuelve la clase del objeto. Recordemos que **R** es un lenguaje POO (Programación Orientada a Objeto) y las variables no dejan de ser objetos internamente.

Usamos el símbolo `<-` como operador de asignación. 

Los tipos de datos básicos son cuatro:

* Carácteres.
* Numéricos.
* Enteros.
* Lógicos.

# Cadenas de texto

```R
> Nombre <- "Dora"
> class(Nombre)
[1] "character"
```

# Numéricos

```R
> Num <- 4.3
> class(Num)
[1] "numeric"
```

# Enteros

```R
> x <- as.integer(5)
> class(x)
[1] "integer"
```

# Lógicos

```R
> Bool <- TRUE
> class(Bool)
[1] "logical"
```

# Conversión de tipo

Usando `as.class` podemos forzar la conversión de tipos:

```R
> x <- "1"
> class(x)
[1] "character"
> x <- as.integer(x)
> class(x)
[1] "integer"
```

# Comprobación de tipo 

Podemos comprobar si una variable es de un tipo determinado: 

```R
is.integer(x)
is.numeric(x)
is.character(x)
is.logical(x)
```

# Ejercicios

* r / intro / 18-Tipos-datos-basicos / [tipos-datos-basicos.R](https://gitlab.com/soka/r/blob/master/intro/18-Tipos-datos-basicos/tipos-datos-basicos.R).

# Enlaces

* ["Operadores y aritmética básica en R"](https://soka.gitlab.io/blog/post/2019-01-28-aritmetica-y-objetos-en-r/).

