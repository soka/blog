---
title: Examinar el log de eventos con PowerShell
subtitle: Scripting en MS Win con PowerShell
date: 2021-09-20
draft: false
description: Scripting en MS Win con PowerShell
tags: ["PS","PowerShell","Get-EventLog","Windows","Scripting","Log","Events"]
---

<!-- vscode-markdown-toc -->
* 1. [Todos los eventos en la computadora local](#Todosloseventosenlacomputadoralocal)
* 2. [Obtener X entradas más recientes](#ObtenerXentradasmsrecientes)
* 3. [Filtrar por tipo](#Filtrarportipo)
* 4. [Filtrar por tipo y gravedad](#Filtrarportipoygravedad)
* 5. [Eventos con una palabra clave](#Eventosconunapalabraclave)
* 6. [Mostrar las propiedades de un evento](#Mostrarlaspropiedadesdeunevento)
* 7. [Eventos en un intervalor de tiempo](#Eventosenunintervalordetiempo)
* 8. [Enlaces internos](#Enlacesinternos)
* 9. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Para acceder en modo gráfico Win+R y ejecuta `eventvwr`.

Los siguientes comandos son básicos, PS permite cosas interesantes como consultar el log de eventos de máquinas remotas.



##  1. <a name='Todosloseventosenlacomputadoralocal'></a>Todos los eventos en la computadora local

```ps
Get-EventLog -List 
```

Podemos paginar el resultado con `Get-EventLog -List | more`. 

##  2. <a name='ObtenerXentradasmsrecientes'></a>Obtener X entradas más recientes

```ps
PS C:\Users\informatica> Get-EventLog -LogName System -Newest 5

   Index Time          EntryType   Source                 InstanceID Message
   ----- ----          ---------   ------                 ---------- -------
   65275 Sep 20 15:56  Warning     DCOM                        10016 No se encontró la descripción del id. de evento '10016' en el origen 'DCOM'. Es posible que el ...
   65274 Sep 20 15:44  Information Service Control M...   1073748869 Se instaló un servicio en el sistema....
   65273 Sep 20 15:22  Error       NETLOGON                     5719 Este equipo no pudo establecer una sesión segura con un controlador de...
   65272 Sep 20 15:19  Information Microsoft-Windows...         1500 La configuración de directiva de grupo del equipo se procesó correctamente. No se detectaron ca...
   65271 Sep 20 15:19  Error       Microsoft-Windows...         1129 No se puede procesar la directiva de grupo debido a que no se puede conectar a un controlador d...
```

##  3. <a name='Filtrarportipo'></a>Filtrar por tipo

```ps
Get-EventLog -LogName System -Newest 1000
``` 

##  4. <a name='Filtrarportipoygravedad'></a>Filtrar por tipo y gravedad

```ps
Get-EventLog -LogName System -EntryType Error
```

##  5. <a name='Eventosconunapalabraclave'></a>Eventos con una palabra clave

```ps
Get-EventLog -LogName System -EntryType Error -Newest 10 -Message *dominio*
``` 

##  6. <a name='Mostrarlaspropiedadesdeunevento'></a>Mostrar las propiedades de un evento

```ps
Get-EventLog -LogName System -Index 65271 | Select-Object -Property *

Get-EventLog -LogName System -Newest 1 | Select-Object -Property *
```

##  7. <a name='Eventosenunintervalordetiempo'></a>Eventos en un intervalor de tiempo

```ps
$Begin = Get-Date -Date '9/19/2021 08:00:00'
$End = Get-Date -Date '9/20/2021 17:00:00'
Get-EventLog -LogName System -EntryType Error -After $Begin -Before $End
```


##  8. <a name='Enlacesinternos'></a>Enlaces internos

* [https://soka.gitlab.io/PSFabrik/](https://soka.gitlab.io/PSFabrik/).
* [https://soka.gitlab.io/blog/tags/powershell/](https://soka.gitlab.io/blog/tags/powershell/).

##  9. <a name='Enlacesexternos'></a>Enlaces externos

* [Get-EventLog](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-eventlog?view=powershell-5.1).
* [about_Logging_Windows](https://docs.microsoft.com/es-es/powershell/module/microsoft.powershell.core/about/about_logging_windows?view=powershell-7.1).