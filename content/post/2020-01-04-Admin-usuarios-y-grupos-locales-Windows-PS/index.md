---
title: Administración de usuarios y grupos locales con Powershell
subtitle: Administración de Windows Server con PowerShell
date: 2020-01-04
draft: false
description: Administración servidores Windows
tags: ["Windows","Sistemas","PowerShell","Administración","cmdlets","Scripting","Windows Server"]
---

![](img/win-server-logo.png)

Antes de comenzar he tenido que instalar [WMF (Windows Management Framework) 5.1 para Windows Server 2012 R2](https://support.microsoft.com/es-es/help/3191564/update-for-windows-management-framework-5-1-for-windows-8-1-and-window) y después el módulo necesario `install-module -name LocalAccount` (seguir este [enlace](https://powershell.org/forums/topic/missing-module-microsoft-powershell-localaccounts/)).

Podemos hacer lo mismo usando la interfaz gráfica que se puede lanzar con `lusrmgr.msc`.

El cmdlet [`Get-Module`](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/get-module?view=powershell-6) permite obtener información de los módulos PS importados en la sesión.

```ps
Get-Module -ListAvailable -Name Microsoft.Powershell.LocalAccounts
```

Para listar los cmdlets:

```ps
Get-Command -Module Microsoft.Powershell.LocalAccounts
```

![](img/01.png)

## Administración de usuarios locales

Obtenemos información de las cuentas de usuario locales:

![](img/02.png)

La cuenta "Invitado" está deshabilitada, la cuenta "Administrador" permite administrar el equipo o el dominio. Podemos listar una cuenta específica con el parámetro -Name `Get-LocalUser -Name Administrador`.

Voy a crear una nueva cuenta de usuario sin clave:

![](img/03.png)

En cualquier momento se puede deshabilitar la cuenta con `Disable-LocalUser`. Como el nuevo usuario no tiene contraseña habilitada no va a poder iniciar sesión. 

Ahora creo un usuario nuevo con una clave introducida por línea de comandos (le pongo como clave "Clave2").

```ps
$password = Read-Host -AsSecureString
New-LocalUser 'Usuario2' -Password $password -FullName 'Usuario 2 curso' -Description 'Usuario número 2'
```

![](img/04.png)

Para eliminar una cuenta de usuario usaré [`Remove-LocalUser`](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.localaccounts/remove-localuser?view=powershell-5.1).

```ps
Remove-LocalUser Usuario1
Get-LocalUser | Format-Table Name,Enabled
```

Ahora renombro el usuario y cambio algún atributo:

```ps
Rename-LocalUser -Name 'Usuario2' -Newname 'i.landajuela'
Set-LocalUser -Name 'i.landajuela' -Description 'Responsable TIC'
Get-LocalUser -Name 'i.landajuela' | Set-Localuser -FullName  'Iker Landajuela'
Set-LocalUser -Name 'i.landajuela' –PasswordNeverExpires $False
```

Lo añado a un grupo predeterminado y ahora ya puedo iniciar sesión con el nuevo usuario:

```ps
Get-Localuser -Name 'i.landajuela' | Add-LocalGroupMember -Group 'Usuarios'
Enable-User -Name 'i.landajuela'
```

Para cambiar la contraseña de un usuario existente:

```ps
$password = Read-Host -AsSecureString
$usuario = Get-LocalUser -Name 'i.landajuela'
$usuario | Set-LocalUser -Password $password
```

Para hacerlo algo más compacto:

```ps
$password = Read-Host -AsSecureString
Get-LocalUser -Name 'i.landajuela' | Set-LocalUser -Password $password
```

## Administración de grupos locales

Consulto de nuevo los cmdlets que ofrece el módulo con `Get-Command -Module Microsoft.Powershell.LocalAccounts`.

Obtengo el listado de grupos:

![](img/05.png)

Para consultar un cmdlet específico usare `Get-Help New-LocalGroup -Examples`.

Ahora creo un nuevo grupo:

```ps
New-LocalGroup -name 'Empleados' -description 'Grupo empleados'
```

Añado alguna cuenta al grupo recién creado con [`Add-LocalGroupMember`](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.localaccounts/add-localgroupmember?view=powershell-5.1):

```ps
Add-LocalGroupMember -GroupName Empleados -name i.landajuela -Verbose
```

Los grupo no se habilitan / deshabilitan, por eso no tenemos los comandos que he usado para las cuentas de usuario.

Compruebo:

```ps
Get-LocalGroupMember -Name Empleados
```

Si quisiese añadir el usuario de un dominio al grupo es parecido:

```ps
Add-LocalGroupMember -GroupName 'Empleados' -name 'dominio\i.landajuela'
```

Para eliminar una cuenta de un grupo usaré `Remove-LocalGroupMember -GroupName Empleados -name i.landajuela`.

Finalmente borro el grupo que he creado `Remove-LocalGroup -name Empleados`.


## Enlaces externos

- ["Working with Local Groups Using PowerShell"](https://key2consulting.com/working-with-local-groups-using-powershell/).
- ["Managing Local Users and Groups with PowerShell"](http://woshub.com/manage-local-users-groups-powershell/).