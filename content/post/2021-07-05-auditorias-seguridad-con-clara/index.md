---
title: Auditoria de seguridad en Windows con CLARA
subtitle: Basado en el Esquema Nacional de Seguridad en el ámbito de la Administración Electrónica
date: 2021-07-05
draft: false
description: 
tags: ["Ciberseguridad","Auditorias","CLARA","CNI","CCN-CERT","Windows","ENS","secpol","Políticas","Seguridad"]
---

<!-- vscode-markdown-toc -->
* 1. [Análisis de los resultados de CLARA](#AnlisisdelosresultadosdeCLARA)
	* 1.1. [OP.ACC.4 - Proceso de gestión de derechos de acceso](#OP.ACC.4-Procesodegestindederechosdeacceso)
		* 1.1.1. [Control de cuentas de usuario: comportamiento de la petición de elevación para los administradores en Modo de aprobación de administrador](#Controldecuentasdeusuario:comportamientodelapeticindeelevacinparalosadministradoresenMododeaprobacindeadministrador)
		* 1.1.2. [Control de cuentas de usuario: comportamiento de la petición de elevación para los usuarios estándar](#Controldecuentasdeusuario:comportamientodelapeticindeelevacinparalosusuariosestndar)
		* 1.1.3. [Control de cuentas de usuario: detectar instalaciones de aplicaciones y pedir confirmación de elevación](#Controldecuentasdeusuario:detectarinstalacionesdeaplicacionesypedirconfirmacindeelevacin)
		* 1.1.4. [Control de cuentas de usuario: ejecutar todos los administradores en Modo de aprobación de administrador](#Controldecuentasdeusuario:ejecutartodoslosadministradoresenMododeaprobacindeadministrador)
		* 1.1.5. [Control de cuentas de usuario: elevar sólo aplicaciones UIAccess instaladas en ubicaciones seguras](#Controldecuentasdeusuario:elevarsloaplicacionesUIAccessinstaladasenubicacionesseguras)
		* 1.1.6. [Control de cuentas de usuario: permitir que las aplicaciones UIAccess pidan confirmación de elevación sin usar el escritorio seguro](#Controldecuentasdeusuario:permitirquelasaplicacionesUIAccesspidanconfirmacindeelevacinsinusarelescritorioseguro)
		* 1.1.7. [Control de cuentas de usuario: virtualizar los errores de escritura de archivo y de Registro en diferentes ubicaciones por usuario](#Controldecuentasdeusuario:virtualizarloserroresdeescrituradearchivoydeRegistroendiferentesubicacionesporusuario)
		* 1.1.8. [Control de cuentas de usuario: Modo de aprobación de administrador para la cuenta predefinida Administrador](#Controldecuentasdeusuario:MododeaprobacindeadministradorparalacuentapredefinidaAdministrador)
		* 1.1.9. [Control de cuentas de usuario: cambiar al escritorio seguro cuando se pida confirmación de elevación](#Controldecuentasdeusuario:cambiaralescritoriosegurocuandosepidaconfirmacindeelevacin)
		* 1.1.10. [Control de cuentas de usuario: elevar sólo los archivos ejecutables firmados y validados](#Controldecuentasdeusuario:elevarslolosarchivosejecutablesfirmadosyvalidados)
	* 1.2. [OP.ACC.5 - Mecanismos de autenticación](#OP.ACC.5-Mecanismosdeautenticacin)
		* 1.2.1. [Vigencia mínima de la contraseña](#Vigenciamnimadelacontrasea)
		* 1.2.2. [Vigencia máxima de la contraseña](#Vigenciamximadelacontrasea)
		* 1.2.3. [Longitud mínima de la contraseña](#Longitudmnimadelacontrasea)
		* 1.2.4. [La contraseña debe cumplir los requisitos de complejidad](#Lacontraseadebecumplirlosrequisitosdecomplejidad)
		* 1.2.5. [Exigir historial de contraseñas](#Exigirhistorialdecontraseas)
	* 1.3. [OP.ACC.6 - Acceso local](#OP.ACC.6-Accesolocal)
		* 1.3.1. [Umbral de bloqueo de cuenta](#Umbraldebloqueodecuenta)
		* 1.3.2. [Restablecer el bloqueo de cuenta después de](#Restablecerelbloqueodecuentadespusde)
		* 1.3.3. [Duración del bloqueo de cuenta](#Duracindelbloqueodecuenta)
		* 1.3.4. [Servidor de red Microsoft: tiempo de inactividad requerido antes de suspender la sesión](#ServidorderedMicrosoft:tiempodeinactividadrequeridoantesdesuspenderlasesin)
	* 1.4. [OP.EXP.2 - Configuración de seguridad](#OP.EXP.2-Configuracindeseguridad)
	* 1.5. [OP.EXP.5 - Gestión de cambios](#OP.EXP.5-Gestindecambios)
	* 1.6. [OP.EXP.6 - Protección frente a código dañino](#OP.EXP.6-Proteccinfrenteacdigodaino)
	* 1.7. [OP.EXP.8 - Registro de actividad de los usuarios](#OP.EXP.8-Registrodeactividaddelosusuarios)
	* 1.8. [OP.EXP.10 - Protección de los registros de actividad](#OP.EXP.10-Proteccindelosregistrosdeactividad)
	* 1.9. [MP.EQ.2 - Bloqueo de puesto de trabajo](#MP.EQ.2-Bloqueodepuestodetrabajo)
	* 1.10. [Directivas de servicios del sistema](#Directivasdeserviciosdelsistema)
* 2. [Enlaces externos](#Enlacesexternos)
* 3. [Enlaces externos](#Enlacesexternos-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

[CLARA](https://www.ccn-cert.cni.es/soluciones-seguridad/clara.html) es una herramienta que permite analizar las características de seguridad de un sistema operativo, los criterios empleados por el programa se basan en el [Real Decreto 3/2010, de 8 de enero, por el que se regula el Esquema Nacional de Seguridad en el ámbito de la Administración Electrónica](https://www.boe.es/buscar/act.php?id=BOE-A-2010-1330&p=20151104&tn=2), las plantillas de seguridad que emplea CLARA (serie 800: 850A, 850B, 851A, 851B, 870A, 870B, 899A y 899B) se pueden encontrar en este [enlace](https://www.ccn-cert.cni.es/guias/guias-series-ccn-stic/800-guia-esquema-nacional-de-seguridad.html), la Serie CCN-STIC-800 establece las políticas y procedimientos adecuados para la implementación de las medidas contempladas en el Esquema Nacional de Seguridad (RD 3/2010), al final de esta entrada he dejado enlaces concretos a algunos documentos de la serie 800. En este [enlace](https://www.ccn-cert.cni.es/publico/ens/ens/index.html#!1001) se puede encontrar el RD ordenado y navegable.

Cuando es un equipo integrado en un dominio se pueden aplicar de forma global para todos los equipos usando GPO (directivas de grupo) en el servidor (`gpmc.msc`).

Ejemplo modificación políticas de contraseñas para un dominio en Win Server 2019 con `gpmc.msc`:

![](img/05.PNG)

La aplicación CLARA se puede descargar de este [enlace](http://ccn-cert.net/herramientaclara) (contraseña: CLARA) junto con un manual de uso. La herramienta requiere para su ejecución de una cuenta con privilegios de administrador, no siendo necesaria instalación alguna para su ejecución.

![](img/01.PNG)

La primera vez lo mejor es probarla introduciendo los datos necesarios y con el fichero de configuración por defecto.

![](img/02.PNG)

Cuando finalize si seleccionas informes la aplicación abre la carpeta destino donde genera dos archivos HTML con los informes como resultado del análisis (en una subcarpeta donde hayamos descomprimido la herramienta).

##  1. <a name='AnlisisdelosresultadosdeCLARA'></a>Análisis de los resultados de CLARA

###  1.1. <a name='OP.ACC.4-Procesodegestindederechosdeacceso'></a>OP.ACC.4 - Proceso de gestión de derechos de acceso

Derechos de acceso de cada usuario contemplados en [OP.ACC.4](https://www.ccn-cert.cni.es/publico/ens/ens/index.html#!1090) 

####  1.1.1. <a name='Controldecuentasdeusuario:comportamientodelapeticindeelevacinparalosadministradoresenMododeaprobacindeadministrador'></a>Control de cuentas de usuario: comportamiento de la petición de elevación para los administradores en Modo de aprobación de administrador

Esta configuración de directiva determina el **comportamiento del aviso de elevación de las cuentas que tienen credenciales administrativas*. Permite definir diferentes valores: Elevar sin preguntar, Solicitar credenciales, etc ([fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/user-account-control-behavior-of-the-elevation-prompt-for-administrators-in-admin-approval-mode)). 

**El valor recomendado es "Solicitar el consentimiento en el escritorio seguro"**: Cuando una operación requiere elevación de privilegios, se pide al usuario en el escritorio seguro (capa de seguridad que entra en acción junto con el Control de cuentas de usuario (UAC)) que seleccione Permitir o Denegar. Si el usuario selecciona Permitir, la operación continúa con el privilegio más alto disponible del usuario.

La forma de cambiarlo es abrir la configuración de las políticas de seguridad locales ejecutando `secpol.msc` y accediendo a la ruta "Directivas locales\Opciones de seguridad".

![](img/03.PNG)

####  1.1.2. <a name='Controldecuentasdeusuario:comportamientodelapeticindeelevacinparalosusuariosestndar'></a>Control de cuentas de usuario: comportamiento de la petición de elevación para los usuarios estándar

Este criterio es similar pero para cuentas de usuarios estándar, el **valor recomendado es "Pedir credenciales en el escritorio seguro"**. Cuando una operación requiere elevación de privilegios, se solicita al usuario en el escritorio seguro que escriba un nombre de usuario y una contraseña diferentes ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/user-account-control-behavior-of-the-elevation-prompt-for-standard-users)).

![](img/04.PNG)

####  1.1.3. <a name='Controldecuentasdeusuario:detectarinstalacionesdeaplicacionesypedirconfirmacindeelevacin'></a>Control de cuentas de usuario: detectar instalaciones de aplicaciones y pedir confirmación de elevación

Esta configuración de directiva determina el comportamiento de la detección de instalación de aplicaciones para todo el sistema. Es posible que algún software intente instalarse después de que se le haya concedido permiso para ejecutarse ([fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/user-account-control-detect-application-installations-and-prompt-for-elevation)), **el valor debe ser "Habilitado"** (Los paquetes de instalación de aplicaciones que requieren una elevación de privilegios para instalarse se detectan y se solicitan al usuario credenciales administrativas).

####  1.1.4. <a name='Controldecuentasdeusuario:ejecutartodoslosadministradoresenMododeaprobacindeadministrador'></a>Control de cuentas de usuario: ejecutar todos los administradores en Modo de aprobación de administrador

Permite a la cuenta de administrador integrada elevar privilegios sin iniciar sesión mediante una cuenta diferente ([fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/user-account-control-admin-approval-mode-for-the-built-in-administrator-account)). El valor debe ser "Habilitado".

**NOTA**: Se recomienda **no habilitar la cuenta de administrador integrada en el equipo cliente, sino usar la cuenta de usuario estándar y el control de cuentas de usuario (UAC) en su lugar**. Si desea habilitar la cuenta de administrador integrada para llevar a cabo tareas administrativas, por motivos de seguridad, también debe habilitar el modo de aprobación de administrador.

####  1.1.5. <a name='Controldecuentasdeusuario:elevarsloaplicacionesUIAccessinstaladasenubicacionesseguras'></a>Control de cuentas de usuario: elevar sólo aplicaciones UIAccess instaladas en ubicaciones seguras

El valor debe ser "Habilitado". ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/user-account-control-only-elevate-uiaccess-applications-that-are-installed-in-secure-locations))

####  1.1.6. <a name='Controldecuentasdeusuario:permitirquelasaplicacionesUIAccesspidanconfirmacindeelevacinsinusarelescritorioseguro'></a>Control de cuentas de usuario: permitir que las aplicaciones UIAccess pidan confirmación de elevación sin usar el escritorio seguro

Valor esperado: Deshabilitada.

####  1.1.7. <a name='Controldecuentasdeusuario:virtualizarloserroresdeescrituradearchivoydeRegistroendiferentesubicacionesporusuario'></a>Control de cuentas de usuario: virtualizar los errores de escritura de archivo y de Registro en diferentes ubicaciones por usuario

Valor esperado: Habilitada.

####  1.1.8. <a name='Controldecuentasdeusuario:MododeaprobacindeadministradorparalacuentapredefinidaAdministrador'></a>Control de cuentas de usuario: Modo de aprobación de administrador para la cuenta predefinida Administrador

Habilitado: La cuenta de administrador integrada inicia sesión en el Modo de aprobación de administrador para que cualquier operación que requiera elevación de privilegios muestre un mensaje que proporciona al administrador la opción de permitir o denegar la elevación de privilegios ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/user-account-control-admin-approval-mode-for-the-built-in-administrator-account)).

####  1.1.9. <a name='Controldecuentasdeusuario:cambiaralescritoriosegurocuandosepidaconfirmacindeelevacin'></a>Control de cuentas de usuario: cambiar al escritorio seguro cuando se pida confirmación de elevación

Habilitada: Todas las solicitudes de elevación de forma predeterminada van al escritorio seguro ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/user-account-control-switch-to-the-secure-desktop-when-prompting-for-elevation)).

####  1.1.10. <a name='Controldecuentasdeusuario:elevarslolosarchivosejecutablesfirmadosyvalidados'></a>Control de cuentas de usuario: elevar sólo los archivos ejecutables firmados y validados

Deshabilitada ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/user-account-control-only-elevate-executables-that-are-signed-and-validated)).

###  1.2. <a name='OP.ACC.5-Mecanismosdeautenticacin'></a>OP.ACC.5 - Mecanismos de autenticación

Preámbulo sobre los mecanismo de autenticación (algo que sabes, algo que tienes o algo que eres, etc) [OP.ACC.5](https://www.ccn-cert.cni.es/publico/ens/ens/index.html#!1091) y niveles.

####  1.2.1. <a name='Vigenciamnimadelacontrasea'></a>Vigencia mínima de la contraseña

Establece la antiguedad mínima en días de la contraseña actual para que el usuario pueda cambiarla, el valor recomendado en CLARA es de 2 días, la directiva se encuentra en Directivas de cuenta > Directiva de contraseñas ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/minimum-password-age)). 

####  1.2.2. <a name='Vigenciamximadelacontrasea'></a>Vigencia máxima de la contraseña

Determina el período de tiempo (en días) que se puede usar una contraseña antes de que el sistema requiera que el usuario la cambie, se recomiendan 45 días ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/maximum-password-age)).

####  1.2.3. <a name='Longitudmnimadelacontrasea'></a>Longitud mínima de la contraseña

Establezca la longitud mínima de la contraseña de **al menos 12 carácteres**. Si el número de caracteres se establece en 0, no se requiere ninguna contraseña ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/minimum-password-length#:~:text=En%20la%20mayor%C3%ADa%20de%20los,a%2014%20en%20este%20momento.)).

####  1.2.4. <a name='Lacontraseadebecumplirlosrequisitosdecomplejidad'></a>La contraseña debe cumplir los requisitos de complejidad

Si se habilita esta directiva, las contraseñas deben cumplir los siguientes requisitos mínimos:

* No contener el nombre de cuenta del usuario o partes del nombre completo del usuario en más de dos caracteres consecutivos
* Tener una longitud mínima de seis caracteres
* Incluir caracteres de tres de las siguientes categorías:
	* Mayúsculas (de la A a la Z)
	* Minúsculas (de la a a la z)
	* Dígitos de base 10 (del 0 al 9)
	* Caracteres no alfanuméricos (por ejemplo, !, $, #, %)
	* Estos requisitos de complejidad se exigen al cambiar o crear contraseñas.

Evidentemente **se recomienda que esté habilitada** ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/password-must-meet-complexity-requirements)).

####  1.2.5. <a name='Exigirhistorialdecontraseas'></a>Exigir historial de contraseñas

Determina el número de contraseñas nuevas únicas que deben asociarse con una cuenta de usuario antes de que se pueda volver a usar una contraseña antigua. El **valor recomendado es 24** ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/enforce-password-history)).

**Aún hay más en este apartado que no incluyo**.

###  1.3. <a name='OP.ACC.6-Accesolocal'></a>OP.ACC.6 - Acceso local 

Se considera acceso local al realizado desde puestos de trabajo dentro de las propias instalaciones de la organización. Estos accesos tendrán en cuenta el nivel de las dimensiones de seguridad ([OP.ACC.6](https://www.ccn-cert.cni.es/publico/ens/ens/index.html#!1092)).

####  1.3.1. <a name='Umbraldebloqueodecuenta'></a>Umbral de bloqueo de cuenta

Determina el **número de intentos de inicio de sesión fallidos que provocarán que una cuenta de usuario se bloquee**. Se recomienda **8 intentos**. Afecta a otras directivas:

* Restablecer el nº de recuentos bloqueo de cuenta después de 30 minutos.
* Duración del bloqueo de cuenta. Si la duración del bloqueo de cuenta se establece en 0, la cuenta se bloquea hasta que el administrador la desbloquea explícitamente.

####  1.3.2. <a name='Restablecerelbloqueodecuentadespusde'></a>Restablecer el bloqueo de cuenta después de

La configuración Restablecer contador de bloqueo de cuenta después de la directiva determina el número de minutos que deben transcurrir desde el momento en que un usuario no puede iniciar sesión antes de que el contador de intentos de inicio de sesión fallido se restablezca a 0. Se recomiendan 30 minutos.

####  1.3.3. <a name='Duracindelbloqueodecuenta'></a>Duración del bloqueo de cuenta

El número de minutos que una cuenta bloqueada permanece bloqueada antes de que se desbloquee automáticamente. Valor recomendado -1. ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/account-lockout-duration)).

####  1.3.4. <a name='ServidorderedMicrosoft:tiempodeinactividadrequeridoantesdesuspenderlasesin'></a>Servidor de red Microsoft: tiempo de inactividad requerido antes de suspender la sesión

Esta configuración de seguridad determina el período de tiempo de inactividad continuo que debe transcurrir en una sesión de Bloque de mensajes del servidor (SMB) para que ésta se suspenda por falta de actividad. 15 minutos. ([Fuente](https://docs.microsoft.com/es-es/windows/security/threat-protection/security-policy-settings/microsoft-network-server-amount-of-idle-time-required-before-suspending-session)).

###  1.4. <a name='OP.EXP.2-Configuracindeseguridad'></a>OP.EXP.2 - Configuración de seguridad

[OP.EXP.2](https://www.ccn-cert.cni.es/publico/ens/ens/index.html#!1096)

_NOTA: Pendiente de revisar directivas_

###  1.5. <a name='OP.EXP.5-Gestindecambios'></a>OP.EXP.5 - Gestión de cambios

Firewall de Windows y Nivel de actualización (El sistema está actualizado dentro de los últimos 30 días).

###  1.6. <a name='OP.EXP.6-Proteccinfrenteacdigodaino'></a>OP.EXP.6 - Protección frente a código dañino

###  1.7. <a name='OP.EXP.8-Registrodeactividaddelosusuarios'></a>OP.EXP.8 - Registro de actividad de los usuarios

###  1.8. <a name='OP.EXP.10-Proteccindelosregistrosdeactividad'></a>OP.EXP.10 - Protección de los registros de actividad

###  1.9. <a name='MP.EQ.2-Bloqueodepuestodetrabajo'></a>MP.EQ.2 - Bloqueo de puesto de trabajo

**El puesto de trabajo se bloqueará al cabo de un tiempo prudencial de inactividad**, requiriendo una nueva autenticación del usuario para reanudar la actividad en curso


* Inicio de sesión interactivo: no mostrar el último nombre de usuario: Habilitada.
* Inicio de sesión interactivo: no requerir Ctrl+Alt+Supr: Deshabilitada.
* Inicio de sesión interactivo: mostrar información de usuario cuando se bloquee la sesión:	No mostrar la información del usuario.
* Configuración de usuario/Panel de control/Personalización/Habilitar protector de pantalla: 

### MP.EQ.3 - Protección de equipos informáticos

### MP.COM.3 - Protección de la autenticidad y de la integridad 

### Directivas del sistema de ficheros

No hay datos relevantes que mostrar en esta sección. (???)

###  1.10. <a name='Directivasdeserviciosdelsistema'></a>Directivas de servicios del sistema 

Deshabilitar servicios innecesarios, entre ellos algunos como Xbox que Windows instala por defecto. 

##  2. <a name='Enlacesexternos'></a>Enlaces externos


##  3. <a name='Enlacesexternos-1'></a>Enlaces externos

**800 Guías Esquema Nacional de Seguridad**:

* [CCN-STIC-850A Implantación del ENS en Windows 7 (cliente en dominio)](https://www.ccn-cert.cni.es/series-ccn-stic/800-guia-esquema-nacional-de-seguridad/545-ccn-stic-850a-implantacion-del-ens-en-windows-7-cliente-en-dominio/file.html).
* [CCN-STIC-850B Implementación del ENS en Windows 7 (cliente independiente)](https://www.ccn-cert.cni.es/series-ccn-stic/800-guia-esquema-nacional-de-seguridad/547-ccn-stic-850b-implementacion-del-ens-en-windows-7-cliente-independiente/file.html).
* [CCN-STIC-851A Impantación del ENS en Windows Server 2008 R2 (controlador de dominio y servidor miembro)](https://www.ccn-cert.cni.es/series-ccn-stic/800-guia-esquema-nacional-de-seguridad/551-ccn-stic-851a-impantacion-del-ens-en-windows-server-2008-r2-controlador-de-dominio-y-servidor-miembro/file.html)
* [CCN-STIC-851B Implementación del ENS en Windows Server 2008 R2 (servidor independiente)](https://www.ccn-cert.cni.es/series-ccn-stic/800-guia-esquema-nacional-de-seguridad/556-ccn-stic-851b-implementacion-del-ens-en-widnwos-server-2008-r2-servidor-independiente/file.html)
* [CCN-STIC-870A Implementación del ENS en Windows Server 2012 R2 (controlador de dominio y servidor miembro)](https://www.ccn-cert.cni.es/series-ccn-stic/800-guia-esquema-nacional-de-seguridad/982-870a-implementacion-ens-windows-server-2012-r2-jul15/file.html)
* [CCN-STIC-870B Implementación del ENS en Windows Server 2012 R2 (servidor independiente)](https://www.ccn-cert.cni.es/series-ccn-stic/800-guia-esquema-nacional-de-seguridad/994-ccn-stic-870b-implementacion-del-ens-en-windows-server-2012-r2-servidor-independiente/file.html)