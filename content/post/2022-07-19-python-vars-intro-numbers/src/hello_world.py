#!/usr/bin/env python3

message = "Hello World!"
print(message)

# ==========================================
# Naming and using variables
# ==========================================
# Carácteres permitidos: letras, números, guion bajo
message_1 = "uno"
#2_message = "Dos # No pueden empezar por un número
#message 3 = "tres" # No pueden contener espacios



