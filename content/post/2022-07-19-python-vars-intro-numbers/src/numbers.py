#!/usr/bin/env python3

# =========================================
# Números 
# =========================================

print(type(23)) # <class 'int'>
print(type(2.3)) # <class 'float'>
print(type(False)) # <class 'bool'>

# Operaciones matemáticas básicas con números
print(12+13)
print(1.5*2) # 3.0
print(2**3) # 8

#print(len(str(2 ** 10000000)))

# Además Python cuenta con módulos para trabajar con números
import math
print(math.pi)
print(math.sqrt(4)) # 2.0

import random
print(random.random())

