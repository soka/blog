#!/usr/bin/env python3

print('soy una cadena')

# Almacenando una cadena en una variable
cadena = 'soy una cadena'

print(type(cadena)) # <class 'str'>

# ===============================================
# Longitud de una cadena
# ===============================================
print(len(cadena))

print("cadena vacia tiene longitud",len("")) # 0

# Primer elemento de una cadena, el índice comienza en la posición 0
print(cadena[0]) # s

S = 'uno'
print(S[-1]) # Accedemos al último elemento 'o'
# es lo mismo que:
print(S[len(S)-1])

print(S[-2]) # 'n'


# Parte de un string
print(S[1:3]) # Desde posición 1  'no'

name = "ada lovelace"
print(name.title())

name = "Ada Lovelace"
print(name.upper())
print(name.lower())

# ===================================
# Concatenación
# ===================================
# Lo que es sumar con números es concatenar con strings
print('abc' + 'xyz')

abc = 'abc'
abc + 'xyz'


first_name = 'Iker'
last_name = 'Landajuela'
full_name = first_name + ' ' + last_name
print(full_name)

# ==================================
# Repetición
# ==================================
print('Spam'*3) # 'SpamSpamSpam'

# ==================================
# Cadenas de varias líneas
# ==================================
cad = '''un,
dos,
tres'''
print(cad)

# """This is a multiline block"""

# ==================================
# Inmutabilidad
# ==================================
S = 'Spam'
#S[0] = 'z' # Error

# ==================================
# Añadiendo tabulaciones y nuevas líneas  
# ==================================
print('\tTabed first line\nSecond line')

S='A\nB\tC'
print('Los carácteres de escape suman como uno solo: ' + str(len(S))) # 5

ord('\n') # Retorna valor ASCII, en este caso 10

# ==================================
# Métodos específicos 
# ==================================
# Ademas de las operaciones genéricas (suma, etc.) al tratarse de una clase también
# implementa métodos internos
# https://www.w3schools.com/python/python_ref_string.asp
S = 'Spam'
print(S.find('pa')) # Retorna el offset de la cadena buscada (1)
print(S.replace('pa','XYZ')) # SXYZm

line = 'aaaaa,bbbb,cccc'
# Dividir una cadena basada en un delimitador
print(line.split(',')) # ['aaaaa', 'bbbb', 'cccc']
# Convertir a mayúsculas
S = 'string'
print(S.upper()) # STRING

S = 'abc'
print(S.isalpha()) # TRUE, si usamos 'abc123' sería FALSE. 
S = '123'
print(S.isdigit()) # TRUE

# ==================================
# Formateo de cadena 
# ==================================
name="Conan"
# Ojo que es un ".", la "," se usa para pasar varios parámetros al print
print("The barbarian hero of Hyborian age is {}".format(name))

name="Conan"
place="Cimmeria"
print("{} is from {}".format(name,place))

# puedes imprimir la entrada de un usuario
name=input("What's your name?")
print("Hello {}".format(name))

# Usando listas con números y cadenas
characters = ["Conan",27]
print("{0} is {1} years old".format(*characters))

names = ["Conan","Valeria"]
ages = [27,22]
print("{0[0]} is {1[0]}, {0[1]} is {1[1]}".format(names,ages) )
