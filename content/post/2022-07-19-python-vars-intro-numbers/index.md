---
title: Variables y tipos de datos básicos en Python
subtitle: Tipos de datos
date: 2022-07-19
draft: false
description: Taller Python
tags: ["Cursos","Talleres","Python","Python - Variables","Python - Introducción","Python - Tipos de datos"]
---

![](img/Python_logo-01.png)

A continuación aprenderemos sobre los diferentes tipos de datos que podemos usar en Python. También veremos como almacenar datos en variables y como usar estas variables en nuestros programas. En este artículo me centraré en los tipos de datos básicos.

## Qué sucede cuando ejecutamos hello_world.py 

Cuando ejecutamos el script "hello_world.py" produce la salida "Hello World!", la extensión .py indica que el fichero es un programa escrito en Python, el sistema entonces ejecuta el programa usando el interprete de Python.  

## Variables

Vamos a tratar de usar nuestra primera variable en el mismo script "hello_world.py", modifica el programa así:

```python
message = "Hello World!"
print(message)
```

Si lo ejecutamos la salida es la misma que en la primera versión. Acabamos de añadir una variable llamada "message". La variable almacena un valor, en este caso una cadena de texto. 

## Variables y tipos de datos básicos

Las variables toman un valor usando el operador `=`, **no se necesita una declaración explicita del tipo de dato que albergará**, cosas como estas se pueden hacer:

```python
var_int = 21
var_int ="string"
```

Para averiguar el tipo que está usando:

```python
$ python
Python 2.7.16 (default, Apr  6 2019, 01:42:57) 
[GCC 8.3.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> var_int = 23
>>> type(var_int)
<type 'int'>
```

## Asignación múltiple

Se puede consultar la clase [type](https://docs.python.org/es/3/library/functions.html?highlight=type#type) en la documentación oficial.  

Se pueden asignar más de una variable en una línea como sigue `a,b,c = 12.3,'Hola',4`, para asignar el mismo valor a más de una variable `x = y = z = True`.

## Alcance de las variables

**Alcance de las variables**: Por defecto son locales, una variable declarada dentro de una función sólo existe dentro de la misma (usamos `global` para convertirla en global). 

## Nombres de variables

Reglas para los nombres de variables: 

* Los nombres de variables sólo pueden contener letras, números y el guion bajo. Pueden empezar por una letra o el guion bajo, pero nunca con un número.
* Los espacios no están permitidos en los nombres de variables.
* Evitar usar palabras reservadas del sistema y funciones (como `print`).

## Core Data Types

![](img/02.png)

## Numéricos

Tipos numéricos:

* int: representa números enteros.
* float: números de punto flotante.
* complex: números complejos.
* bool: con dos únicas instancias, True y False.    

Las instancias de los tipos numéricos son objetos inmutables instancias de la clase Number del módulo numbers (documentación oficial [numbers](https://docs.python.org/es/3/library/numbers.html?highlight=numbers%20number#module-numbers)).

```
>>> import numbers
>>> isinstance(33, int)
True
>>> isinstance(33.3, float)
True
>>> isinstance(False, bool)
True
```

## Números enteros

Los números enteros pueden ser representados de diferentes formas (decimal o base 10, binaria, octal y hex).

```
>>> -15 # decimal
-15
>>> -0b1111 # binario, se preceden con 0b o  0B
-15
>>> -0xF # Hex
-15
>>> -0o17 # octal con 0o o 0O
-15
>>> type(-15), type(-0b1111), type(-0xF), type(-0o17) 
(<class 'int'>, <class 'int'>, <class 'int'>, <class 'int'>)
```

## Operaciones matemáticas básicas

Podemos realizar operaciones matemáticas básicas con los tipos numéricos:

```python
>>> 13*3
39
>>> 5+4
9
>>> 2**3
8
```
Además Python cuenta con módulos para trabajar con números (ver documentación [math](https://docs.python.org/es/3/library/math.html?highlight=math#module-math)).

```
import math
print(math.pi)
print(math.sqrt(4)) # 2.0
```

## Obtener la representación en una base


## Conversión de tipos

Para realizar la conversión de tipos existen funciones especializadas 

Se puede borrar la referencia a una variable con `del(var_int)`, si tratamos de imprimir la variable de nuevo al ejecutarlo se interrumpe y muestra un error `NameError: name 'var_int' is not defined`.

Enteros con coma flotante:

```python
float_1, float_2, float_3 = 0.348, 10.5, 1.5e2
print(float_1,type(float_1))
print(float_2, type(float_2))
print(float_3, type(float_3))
```

Se puede convertir de tipo con las funciones `int`, `long`, `float` o `complex`. En el ejemplo de abajo lo convierte a 7.

```python
print(int(7.89))
```

## Cadenas

Las cadenas son carácteres delimitados por " o '. 

```python
>>> 'soy una cadena'
'soy una cadena'
```

Podemos definir cadenas largas de varias líneas con `'''`:

```python
>>> cad='''cadena 
... varias
... lineas'''
>>> print(cad)
cadena 
varias
lineas
>>> type(cad)
<type 'str'>
```

Podemos acceder a sus elementos como una lista o tupla:

```python
>>> var_str='cadena'
>>> print(var_str[0])
c
```

Las cadenas están representadas por dos clases `str` y `unicode`. Podemos acceder a los métodos internos de una clase, por ejemplo:

```python
>>> var_str.capitalize()
'Cadena'
```

Para imprimir una cadena con el valor de una variable dentro:

```python
my_int = 10
print(f"My int is: {my_int}")
```

Para indicar que un string en Python contendrá variables a ser sustituidas debemos anteceder el caracter "f".

Podemos hacer lo mismo recurriendo al método `format()`.

```python
entero = 12
cadena = "Mi entero es: {}"
print(cadena.format(entero))
```

Más ejemplos de impresión de cadenas formateadas:

```python
>>> print("Su pelo era blanco como la {} de la cima del monte {}.".format('nieve','everest'))
Su pelo era blanco como la nieve de la cima del monte everest.

>>> print("*" * 5)
*****

>>> c1,c2,c3,c4='H','o','l','a'
>>> print(c1+c2+c3+c4)
Hola
```

Ahora prueba con esto:

```python
formatter = "{} {} {} {}"
print(formatter.format(1, 2, 3, 4))
print(formatter.format("one", "two", "three", "four"))
print(formatter.format(True, False, False, True))
print(formatter.format(formatter, formatter, formatter,
formatter))
print(formatter.format(
"Try your",
"Own text here",
"Maybe a poem",
"Or a song about fear"
))
```

### Secuencias de escape

![](img/01.png)

Ejemplo:

```python
>>> print("línea1\nlínea2\nlínea3")
línea1
línea2
línea3
>>> cadena_tab = "\tEstoy tabulada."
>>> print(cadena_tab)
	Estoy tabulada.
```

### Operadores aritméticos con cadenas

Podemos sumar cadenas con '+' o multiplicarlas con '*'.

```python
>>> x = 'uno' + ' dos' + ' tres'
>>> print(x)
uno dos tres
>>> print(x*2)
uno dos tresuno dos tres
```

### Obtener datos del usuario

La función `input()` siempre devuelve un valor numérico:

```python
>>> edad = input('Deme su edad: ')
Deme su edad: 41
>>> print(edad)
41
```

La función raw_input() siempre devuelve un valor de cadenas de caracteres:

```bash
>>> cadena_usuario = raw_input('Nombre de usuario:')
Nombre de usuario:Administrador
>>> print(cadena_usuario)
Administrador
```

## Scripts con argumentos

Debemos usar la librería `sys`, dentro de encuentra la lista `argv` con los argumentos pasados al script.

Este script imprime todos los argumentos de entrada:

```python
#!/usr/bin/env python3
import sys
print(sys.argv)
```

Fijate que al ejecutarlo devuelve una lista donde el primer elemento es una cadena con el nombre del script, también accesible con `sys.argv[0]`.

```bash
$ ./script-prnt-all-args.py hola 12
['./script-prnt-all-args.py', 'hola', '12']
```

Podemos comprobar si el número de argumentos es el esperado usando una condicional y la función `len` para obtener el número de elementos de la lista.

```bash
if len(sys.argv) != 3:
    print('error num args')
else:
    print('ok num args')
```

Otro ejemplo:

```python
cadena_nombre_script, arg_1, arg_2 = sys.argv
print("Script",cadena_nombre_script," , arg1:",arg_1," ,arg2:",arg_2)
``

## Booleanos y operadores lógicos

El tipo booleano sólo puede tener dos valores: True (verdadero) y False (falso). 

Inspecciono el tipo de dato:

```python
>>> var_bool = True 
>>> print(type(var_bool))
<type 'bool'>
```

La salida es "El valor de True es:  True":

```python
print('El valor de True es: ',True)
```

Operadores lógicos

```python
bAnd = True and False
```

## Enlaces externos

- [https://docs.python.org/es/3/tutorial/inputoutput.html](https://docs.python.org/es/3/tutorial/inputoutput.html).
- [https://entrenamiento-python-basico.readthedocs.io/es/latest/leccion3/](https://entrenamiento-python-basico.readthedocs.io/es/latest/leccion3/).
- [https://entrenamiento-python-basico.readthedocs.io/es/latest/leccion3/tipo_cadenas.html](https://entrenamiento-python-basico.readthedocs.io/es/latest/leccion3/tipo_cadenas.html).
