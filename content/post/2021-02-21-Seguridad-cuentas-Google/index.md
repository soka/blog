---
title: Seguridad con cuentas de Google
subtitle: ¿Cómo funciona la seguridad de Google?
date: 2021-02-21
draft: false
description: Consejos para proteger mejor nuestra cuenta
tags: ["Aplicaciones","Google","Seguridad","Contraseñas","Ciberseguridad","Google Authenticator","2FA"]
---

<!-- vscode-markdown-toc -->
* 1. [AÑADE UN CORREO ELECTRÓNICO Y UN NÚMERO DE TELÉFONO DE RECUPERACIÓN](#AADEUNCORREOELECTRNICOYUNNMERODETELFONODERECUPERACIN)
* 2. [REVISA LAS SUGERENCIAS DE PRIVACIDAD](#REVISALASSUGERENCIASDEPRIVACIDAD)
* 3. [REVISIÓN DE CONTRASEÑAS](#REVISINDECONTRASEAS)
* 4. [ACTIVA LA VERIFICACIÓN EN 2 PASOS](#ACTIVALAVERIFICACINEN2PASOS)
	* 4.1. [Cómo acceder con códigos de respaldo (2FA)](#Cmoaccederconcdigosderespaldo2FA)
	* 4.2. [Google Authenticator](#GoogleAuthenticator)
* 5. [PROGRAMA DE PROTECCIÓN AVANZADA DE GOOGLE](#PROGRAMADEPROTECCINAVANZADADEGOOGLE)
* 6. [ENLACES INTERNOS](#ENLACESINTERNOS)
* 7. [ENLACES EXTERNOS](#ENLACESEXTERNOS)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

A continuación ofrezco algunos consejos prácticos para proteger una cuenta de Google.

##  1. <a name='AADEUNCORREOELECTRNICOYUNNMERODETELFONODERECUPERACIN'></a>AÑADE UN CORREO ELECTRÓNICO Y UN NÚMERO DE TELÉFONO DE RECUPERACIÓN

La **información de recuperación** se usa para volver a acceder a la cuenta si Google detecta actividad inusual en la cuenta, para recuperar la cuenta si alguien más conoce la contraseña o simplemente para acceder de nuevo a tu cuenta si olvidas las contraseña.

Accede a tu [cuenta de Google](https://myaccount.google.com/), en el panel de navegación de la izquierda, haz clic en "Seguridad". En el panel "Métodos para verificar tu identidad" define un correo electrónico y un número de teléfono.

Si tienes problemas para acceder a tu cuenta puedes usar la [**página de recuperación de la cuenta**](https://accounts.google.com/signin/recovery), sigue estas [sugerencias de Google para completar los pasos de recuperación de la cuenta](https://support.google.com/accounts/answer/7299973), durante el proceso Google te hace una serie de preguntas como la última contraseña que recuerdas.

Bien es cierto que si usais Google Workspace **tu administrador te puede ayudar a restablecer la contraseña**, para evitar malos entendidos también hay que decir que el administrador no puede ver las contraseñás del resto de usuarios.

##  2. <a name='REVISALASSUGERENCIASDEPRIVACIDAD'></a>REVISA LAS SUGERENCIAS DE PRIVACIDAD

Una vez dentro de la cuenta al inicio Google nos alerta si ha detectado problemas de seguridad, accede regularmente a la cuenta para realizar una [revisión de seguridad](https://myaccount.google.com/u/1/security-checkup?continue=https://myaccount.google.com/u/1/) y sigue las recomendaciones.

![](img/18.png)

En la captura inferior se muestra una alerta grave debido a que alguna contraseña ha quedado expuesta en algún servicio que no es de Google (en este caso la alerta no es tal ya que se refería a servicios dentro de la red interna de la empresa).

![](img/25.png)

Acciones recomendadas:

* Retirar / cerrar sesión en dispositivos desconocidos o que llevas largo tiempo sin usar.
* [https://passwords.google.com/](Contraseñas vulneradas).
* Actividad de seguridad.
* Verificación en 2 pasos.
* Acceso a terceros: Aplicaciones con accesos a tus datos.

##  3. <a name='REVISINDECONTRASEAS'></a>REVISIÓN DE CONTRASEÑAS

Puedes averiguar si las contraseñas de tus cuentas de Google pueden haber sido expuestas, son débiles o se utilizan en varias cuentas y cambiar las que no sean seguras para aumentar el nivel de protección de tus cuentas en la siguiente dirección [passwords.google.com](https://passwords.google.com).

![](img/14.png)

Una vez dentro accede a la función "Revisión de contraseñas".

![](img/15.png)

Google nos mostrará un informe se seguridad de todas nuestras contraseñas, si alguna aparece en rojo es aconsejable cambiarla porque significa que está comprometida, Google revisa si nuestra cuenta ha sido usada en algun sitio de terceros que haya sufrido un fallo de seguridad que haya dejado expuesta tu contraseña (Badoo, Last.fm, Dropbox,etc.).

También nos avisa de si estamos reutilizando la misma contraseña en varios servicios Web o si éstas son lo suficientemente seguras o, por el contrario, son demasiado simples y fáciles de adivinar,  Chrome permite [generar contraseñas](https://support.google.com/chrome/answer/7570435) seguras cuando te registras en una nueva Web. Sigue los [consejos de Google para crear contraseñas seguras](https://support.google.com/accounts/answer/32040?hl=es&authuser=1).

##  4. <a name='ACTIVALAVERIFICACINEN2PASOS'></a>ACTIVA LA VERIFICACIÓN EN 2 PASOS

El sitio Web [Google Verificación en dos pasos](https://www.google.com/intl/es/landing/2step/index.html) informa sobre el funcionamiento de la verificación en 2 pasos y ayuda a los usuarios con cuentas de Google a ponerlo en marcha.

Funciones para los casos en que tu teléfono no tenga una conexión de datos: 

![](img/19.png)

Con el botón "Empieza" ponemos el proceso en marcha para configurar la verificación en 2 pasos para una cuenta (accediendo a nuestra cuenta dentro del apartado de seguridad también se puede poner en marcha), es necesario que tengamos esa cuenta configurada en el _smartphone_ (o tener una llave de seguridad o elegir un sistema mediante SMS). Una vez configurada Google nos notifica mediante correo electrónico, también se puede revisar los últimos cambios de configuración revisando la actividad reciente de la cuenta.

![](img/21.png)

La forma habitual de iniciar sesión usando la verificación de dos pasos es [recibir una notificación de Google](https://support.google.com/accounts/answer/7026266?hl=es&authuser=1) en el teléfono.

Se pueden configurar pasos de seguridad adicionales para iniciar sesión cuando tus otras opciones no estén disponibles: códigos imprimibles de un sólo uso, [Google Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=es&gl=US) o una llave de seguridad USB.

Puedes saltarte el segundo paso en los dispositivos de tu confianza, como tu ordenador.

Siempre podemos desactivar la verificación en dos pasos o comprobar su estado en el apartado de seguridad de la cuenta:

![](img/22.png)

Una vez activo el inicio de sesión con tu teléfono cada vez que tengamos que iniciar sesión con nuestra cuenta de Google nos llegará a nuestro móvil la notificación de confirmación. A veces no bastará con desbloquear el móvil y pulsar en Sí, en ocasiones nos preguntará por el número que aparece en pantalla como medida de seguridad adicional.

![](img/23.jpg)

###  4.1. <a name='Cmoaccederconcdigosderespaldo2FA'></a>Cómo acceder con códigos de respaldo (2FA)

Si has perdido el teléfono, te lo han robado o no tienes conexión a Internet, puedes usar los [códigos de respaldo](https://support.google.com/accounts/answer/1187538) si activas esta opción en la verificación en dos pasos de la cuenta.  

Puedes generar 10 códigos de un sólo uso que puedes guardar en un archivo o imprimirlos y guardarlos en un lugar seguro. Si generas un nuevo conjunto el anterior dejará de funcionar.

![](img/24.png)

**Cómo acceder con un código de respaldo**: Accede a un servicio de Google como [Gmail](https://www.gmail.com/), después de ingresa el usuario y la contraseña, cuando se te solicite el código de verificación, haz clic en más opciones y selecciona ingresar uno de tus códigos de respaldo de ocho dígitos.

Esta opción es compatible con cualquier otra función 2FA cómo los mensajes de voz o texto o los mensajes de Google. Combinar varias de estas opciones es una buena estratégia de seguridad.

###  4.2. <a name='GoogleAuthenticator'></a>Google Authenticator

**Google Authenticator** es una app para smartphones para [Android](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2) e [iOS](https://apps.apple.com/es/app/google-authenticator/id388497605), la aplicación genera códigos de un sólo uso que expiran. Es un **mecanismo de verificación en 2 pasos**, la contraseña de nuestra cuenta y el código de 6 dígitos que genera **Google Authenticator**,  se trata de uno de los métodos más seguros de verificar tu identidad fácilmente.

Para usar **Google Authenticator** necesitas instalar la aplicación en el móvil y luego configurar la cuenta que quieres proteger. Puedes hacerlo manualmente, aunque lo más común es escanear un código QR que incluye toda la información de la cuenta y su configuración. Dicho código QR incluye una clave secreta que la aplicación usa para generar códigos numéricos que cambian cada cierto tiempo (por ejemplo, cada 30 segundos). Al pasarse esta clave secreta al inicio, durante la configuración, Google Authenticator no requiere de conexión a Internet para funcionar.

Dentro del apartado de seguridad de la cuenta accedemos a la verificación en dos pasos para configurar **Authenticator**, en mi caso además de **Authenticator** ya tengo configuradas las opciones de mensajes de google (opción predeterminada), mensaje de voz o texto y los códigos de seguridad de un solo uso.

![](img/26.png)

##  5. <a name='PROGRAMADEPROTECCINAVANZADADEGOOGLE'></a>PROGRAMA DE PROTECCIÓN AVANZADA DE GOOGLE

Las personas que tienen archivos particularmente valiosos o información sensible en sus cuentas deberían registrarse en el [**Programa de Protección Avanzada**](https://landing.google.com/intl/es/advancedprotection/). Google recomienda encarecidamente a periodistas, activistas, ejecutivos de empresas y personas que participan en procesos electorales que se registren.

El Programa de Protección Avanzada es un servicio gratuito. Sin embargo, puede que tengas que [comprar una llave de seguridad](https://www.google.com/search?q=YUBICO+U2F+SECURITY+KEY) si aún no la tienes.

El sistema usa dos llaves de seguridad, una puede estar integrada en el teléfono con la aplicación Google Smart Lock ([App Store
](https://apps.apple.com/es/app/google-smart-lock/id1152066360)) integrada de forma nativa en los teléfonos Android.

##  6. <a name='ENLACESINTERNOS'></a>ENLACES INTERNOS

- La clave eres tú. Recursos sobre gestión de contraseñas seguras [https://soka.gitlab.io/blog/post/2021-01-15-la-clave-eres-tu/](https://soka.gitlab.io/blog/post/2021-01-15-la-clave-eres-tu/).
- Manual LastPass [https://soka.gitlab.io/blog/post/2021-01-24-manual-lastpass/](https://soka.gitlab.io/blog/post/2021-01-24-manual-lastpass/).
- Alternativas a LastPass [https://soka.gitlab.io/blog/post/2021-02-20-alternativas-lastpass/](https://soka.gitlab.io/blog/post/2021-02-20-alternativas-lastpass/).

##  7. <a name='ENLACESEXTERNOS'></a>ENLACES EXTERNOS

- [https://www.xatakandroid.com/seguridad/google-lanza-funcion-que-te-avisa-tus-contrasenas-poco-seguras-han-sido-filtradas](https://www.xatakandroid.com/seguridad/google-lanza-funcion-que-te-avisa-tus-contrasenas-poco-seguras-han-sido-filtradas).
- [https://support.google.com/accounts/answer/9457609]https://support.google.com/accounts/answer/9457609).

**Ayuda de Google**:

- Proporcionar un número de teléfono o una dirección de correo electrónico de recuperación [https://support.google.com/accounts/answer/183723?co=GENIE.Platform%3DDesktop&hl=es](https://support.google.com/accounts/answer/183723?co=GENIE.Platform%3DDesktop&hl=es).
- Sugerencias para completar los pasos de recuperación de la cuenta [https://support.google.com/accounts/answer/7299973](https://support.google.com/accounts/answer/7299973).

**Google verificación en dos pasos**:

- Proteger tu cuenta con la verificación en dos pasos [https://support.google.com/accounts/answer/185839?co=GENIE.Platform%3DDesktop&hl=es](https://support.google.com/accounts/answer/185839?co=GENIE.Platform%3DDesktop&hl=es).
- Xataka - Cómo activar la verificación en dos pasos en Google, Facebook, Twitter, Instagram, Microsoft y WhatsApp  [https://www.xataka.com/basics/como-activar-verificacion-dos-pasos-google-facebook-twitter-instagram-microsoft-whatsapp](https://www.xataka.com/basics/como-activar-verificacion-dos-pasos-google-facebook-twitter-instagram-microsoft-whatsapp).
- xatakandroid.com - Así puedes usar tu móvil para iniciar sesión en Google [https://www.xatakandroid.com/tutoriales/como-usar-tu-movil-para-iniciar-sesion-en-google](https://www.xatakandroid.com/tutoriales/como-usar-tu-movil-para-iniciar-sesion-en-google).

**Google Authenticator**:

- [https://www.xataka.com/basics/google-authenticator-que-como-funciona-como-configurarlo](https://www.xataka.com/basics/google-authenticator-que-como-funciona-como-configurarlo).
- Obtener códigos de verificación con Google Authenticator [https://support.google.com/accounts/answer/1066447?co=GENIE.Platform%3DAndroid&hl=es](https://support.google.com/accounts/answer/1066447?co=GENIE.Platform%3DAndroid&hl=es).

**Google Smart Lock**:

- Google Smart Lock: qué es, cómo funciona y cómo puedes usarlo para sincronizar tus contraseñas [https://www.xatakandroid.com/moviles-android/google-smart-lock-que-como-funciona-como-puedes-usarlo-para-sincronizar-tus-contrasenas](https://www.xatakandroid.com/moviles-android/google-smart-lock-que-como-funciona-como-puedes-usarlo-para-sincronizar-tus-contrasenas).
- Qué es Google Smart Lock, cómo funciona y cómo puedes usarlo [https://www.xataka.com/basics/que-google-smart-lock-como-funciona-como-puedes-usarlo](https://www.xataka.com/basics/que-google-smart-lock-como-funciona-como-puedes-usarlo).