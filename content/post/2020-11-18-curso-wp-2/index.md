---
title: Recursos [borrador]
subtitle: Curso WordPress
date: 2020-11-18
draft: false
description: Buscando noticias y recursos 
tags: ["Wordpress","WP - Curso","WP","WP - Plugins","WP - Recursos"]
---

![](img/wordpress-logo-01.png)

## Pad de trabajo

[https://pad.riseup.net/p/penascalf52021](https://pad.riseup.net/p/penascalf52021)

## Oficiales

- [https://codex.wordpress.org/](https://codex.wordpress.org/): Documentación oficial de WP.
- [https://es.wordpress.org/news/](https://es.wordpress.org/news/): Blog de noticias en castellano.
- [https://es.wordpress.org/](https://es.wordpress.org/).
- [https://wordpress.com/](https://wordpress.com/): El servicio comercial para publicar Webs.
- [https://wordpress.tv/](https://wordpress.tv/): Portal de vídeos, existen en castellano.
- [https://wordpress.org/themes/](https://wordpress.org/themes/): Web oficial con plantillas.
- [https://wordpress.org/plugins/](https://wordpress.org/plugins/): Repositorio oficial con 57,942 _plugins_.

## Instalaciones de prueba, alojamiento, etc

- [https://trincheradev.com/](https://trincheradev.com/): Alojamiento gratuito para páginas de WordPress.
- [https://localwp.com/](https://localwp.com/): Permite generar instalaciones de WP locales de forma rápida.

## Sitios Web sobre WP

- [https://desarrollowp.com/](https://desarrollowp.com/).
- [https://decodecms.com/](https://decodecms.com/).
- [https://wpdesdezero.com](https://wpdesdezero.com).
- [https://wpmayor.com/](https://wpmayor.com/).
- [https://wpshout.com/](https://wpshout.com/).
- [https://www.wpbeginner.com/](https://www.wpbeginner.com/): Tutoriales y recursos.
- [https://wptavern.com/](https://wptavern.com/): Noticias, recursos, tutoriales, etc. Cubre todo tipo de temas.
- [https://poststatus.com/](https://poststatus.com/).

A quien seguir:
- [https://imanolteran.com/formacion/](https://imanolteran.com/formacion/).

## Grupos de noticias, RRSS


## Diseño visual

Las mejores plantillas de pago:

- elegantthemes [Divi](https://www.elegantthemes.com/)
- [Visual Composer](https://visualcomposer.com/create-your-wordpress-website/).
- [Studio Press](https://www.studiopress.com/).

Buscadores y repositorios de plantillas:

- [https://themeisle.com/](https://themeisle.com/).
- [https://themeforest.net/](https://themeforest.net/).

Temas gratis, depende del propósito claro.

- https://themeisle.com/wordpress-themes/free/
    - Hestia

## Selección plugins

### Formularios

- [Contact Form 7](https://es.wordpress.org/plugins/contact-form-7/).

De pago:

- [Gravity Forms](https://www.gravityforms.com/): Todos los planes de pago.
- [Ninja Forms](https://ninjaforms.com/).

### Compartir contenidos

- [AddToAny Share Buttons](https://es.wordpress.org/plugins/add-to-any/)

### Específicos

- [WooCommerce](https://woocommerce.com/): Comercio electrónico.
- [PolyLang](https://polylang.pro/): Multidioma.
- [bbPress](https://bbpress.org/): Foro.
- [BuddyPress](https://es.buddypress.org/): Red social.

## Bibliografía 