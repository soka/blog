---
title: Publicando contenidos
subtitle: Sesión nº 1 curso WordPress
date: 2020-11-03
draft: false
description: Como publicar y gestionar nuestros posts
tags: ["Wordpress","WP - Curso","WP","WP - Entradas"]
---

![](img/wordpress-logo-01.png)

## Pad de trabajo

[https://pad.riseup.net/p/penascalf52021](https://pad.riseup.net/p/penascalf52021)

Ahora que tenemos WP instalado y en producción ya estamos preparados para añadir contenidos. En esta sección aprenderás sobre los siguientes tópicos:

- Como escribir un nuevo _post_.
- Como controlar la información asociada a un _post_, no sólo el título y el contenido, también las imágenes y los archivos multimedia.
- Los comentarios, para que sirven y como gestionarlos.
- Mantener el contenido organizado y fácil de buscar usando etiquetas y categorías.

![](img/01.png)

## Lista de posts

¿Aún recuerdas cuales eran las **Opciones de pantalla** en el escritorio?

Cada entrada de la lista contiene el **título de la publicación** que se puede clicar para editarlo, si desplazas el puntero del ratón por encima del título verás además nuevas opciones para editar, ver o eliminarlo.

![](img/02.png)

Si hacemos clic sobre el autor nos mostrará únicamente las que le pertenecen. También podemos filtrar por las categorías y etiquetas. El bocadillo de diálogo muestra el número de comentarios del artículo.

## Publicando contenidos

La actividad central si estamos creando un blog son los _posts_, son como un artículo de una revista, necesita un título, un cuerpo o contenido y un autor (el usuario de WP que permite varios usuarios con diferentes roles). Un _post_ viene ademas acompañado de un montón de información adicional como la fecha, las etiquetas y las categorías. 

### Añadiendo un post sencillo

Tenemos dos formas de añadir un _post_:

1. Ir a **Entradas | Añadir nueva** del menú principal lateral.
2. Hacer clic sobre el botón **+ Añadir** en el menú superior.

### Trabajando con el editor de bloques

El editor visual de bloques es una de las mejores (no todo el mundo piensa lo mismo) y más importantes novedades en WP introducida por defecto en la [versión 5.0](https://wordpress.org/support/wordpress-version/version-5-0/#list-of-files-revised) ("Bebo"), el nuevo editor se denomina [**Gutenberg**](https://es.wordpress.org/plugins/gutenberg/) (en referencia a [Johannes Gutenberg](https://es.wikipedia.org/wiki/Johannes_Gutenberg) inventor de la imprenta).

![](img/04.png)

La captura anterior es la vista principal del editor de bloques, se llama así porque cada elemento dentro del _post_ es un bloque (un párrafo, una imagen, una tabla, etc). La diferencia salta la vista respecto al viejo editor [WYSIWYG](https://es.wikipedia.org/wiki/WYSIWYG) más similar a un editor de documentos de LibreOffice o MS Word:

![](img/05.png)

El nuevo editor de bloques tiene más sentido para la Web ya que cada elemento funciona de forma independiente, por ejemplo para que sea _responsive_ es más fácil reordenar bloques que párrafos de texto o un contenido continuo.

### Entendiendo la interfaz de edición

El editor consiste en tres secciones principales:

![](img/06.png)

1. **El lienzo o _canvas_ principal**. La sección principal de edición de contenidos.
2. **La barra lateral de ajustes**. Permite ajustes más "finos" de los bloques o del documento completo.
3. **El menu superior**.

Una vista más detalla de la barra de menú superior:

![](img/07.png)

Símbolos comenzando desde la izquierda:

- El icono + te permite añadir un nuevo bloque.
- El lápiz de edición / selección de bloques.
- Deshacer o rehacer cambios.
- Información del documento (palabras, encabezados, párrafos, etc).
- Vista previa (en diferentes dimensiones de dispositivos).
- Publicación.
- La rueda dentada para ocultar la barra lateral de ajustes.
- El menú de tres puntos verticales permite añadir o quitar elementos del editor (**editor de código o editor visual**).

### Creando tu primer post

¡Es hora de empezar nuestro primer _post_!

Comienza añadiendo un título:

![](img/08.png)

Cuando lo hayas definido mueve el cursor hacia abajo y comienza a escribir.

![](img/09.png)

Según empiezas a escribir verás que tienes varias herramientas de edición. Puedes alinear el texto a la izquierda, centrado o a la derecha. Puedes ponerlo en negrita o cursiva, o añadir un enlace. Para añadir una lista introduce _Enter_ y comienza la siguiente línea con un guion. 

Para eliminar un bloque o realizar alguna otra acción puedes usar el menú de tres puntos.

![](img/10.png)

Además de eso cuando estás editando un bloque se pueden ver las opciones de configuración del bloque específico. Por ejemplo, para un párrafo puedes ajustar la fuente, capitalizar mostrando la letra inicial grande, etc.

![](img/11.png)

Ahora añadamos una imagen con el icono +.

![](img/12.png)

El bloque de imagen permite subir una nueva imagen, seleccionarla de la galería multimedia o de una URL.

### Bloques 

#### Columnas

Una de las mejoras cosas del editor de bloques es su versatilidad y funcionalidad. Es realmente sencillo mover los elementos y ajustar su posición para que luzcan perfectos.

Añadir un bloque debajo del otro es sólo una cara de la moneda, se pueden arrastrar y soltar o mover arriba y abajo usando las flechas. Otra cosa que podemos hacer es añadir un nuevo bloque inmediatamente antes o después de un bloque concreto.

Para alinear los [bloques en columnas](https://wordpress.com/es/support/editor-wordpress/bloques/bloque-de-columnas/) usamos un bloque concreto para columnas, hasta un máximo de seis crean un efecto de cuadricula.

![](img/13.gif)

Una vez que hayas definido el número de columnas, podrás añadir contenido a cada una de ellas. Lo fantástico del bloque de columnas es que puedes añadir otros bloques dentro de las columnas.

Por ejemplo, puedes usar el bloque de columnas como estructura y añadir bloques de imagen, bloques de encabezado y bloques de párrafo para crear una cuadrícula con los servicios.

![](img/14.png)

[Vídeo](https://en-support.files.wordpress.com/2018/12/Screen-Capture-on-2018-12-19-at-17.31.41.mov)

La pestaña Avanzado te permite añadir una clase CSS a tu bloque, con lo que puedes escribir código CSS personalizado y darle el estilo que quieras.

### Opciones avanzadas de las publicaciones

#### Extractos

![](img/15.png)

## Enlaces externos

- [https://wordpress.com/es/support/editor-wordpress/bloques/bloque-de-columnas/](https://wordpress.com/es/support/editor-wordpress/bloques/bloque-de-columnas/).
- [https://wordpress.com/es/support/editor-wordpress/#adding-a-block](https://wordpress.com/es/support/editor-wordpress/#adding-a-block).





