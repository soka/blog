---
title: HTTP Get con Angular
subtitle: Llamada básica a una URL   
date: 2019-09-10
draft: false
description: 
tags: ["Angular", "HTTP", "API","Web","Programación"]
---

![](images/angular2.png)

[**>> ENLACE AL TUTORIAL COMPLETO <<**](https://soka.gitlab.io/angular/conceptos/http/ejemplo-peticion-http-get/ejemplo-peticion-http-get/).

En este proyecto con **Angular** realizo una llamada básica HTTP GET a una URL que responde con un contenido JSON y obtengo un campo de la respuesta.

He tratado de mantener el código lo más sencillo posible, toda la lógica de la aplicación está contenida en el único componente de la aplicación, el único modulo necesario es `HttpClientModule` del paquete @angular/common/[http](https://angular.io/api/common/http), este paquete ofrece una API observable para crear un cliente HTTP. 

El módulo `HttpClientModule` configura la [inyección de dependencias](https://angular.io/guide/glossary#injector) para la clase [`HttpClient`](https://angular.io/api/common/http/HttpClient) que uso como parámetro del constructor del componente, esta clase implementa el método [observable](https://angular.io/guide/observables) [`get()`](https://angular.io/api/common/http/HttpClient#get), cuando nos suscribimos ejecuta la petición GET contra el servidor, un observable ofrece soporte para intercambiar mensajes entre un publicador y un suscriptor, dentro del método `subscribe` definimos una función para recibir o publicar los valores de forma asíncrona, la ejecución de la aplicación continua adelante, por eso he definido un botón en la vista para mostrar el resultado de la llamada. 

[El resto del tutorial y el código fuente del proyecto](https://soka.gitlab.io/angular/conceptos/http/ejemplo-peticion-http-get/ejemplo-peticion-http-get/) se encuentran en el enlace al comienzo de este post, esto es todo.
