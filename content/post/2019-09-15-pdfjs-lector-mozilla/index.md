---
title: Firefox PDF.js
subtitle: Visor PDF basado en HTML5
date: 2019-09-15
draft: false
description: Visor PDF Web y librería JS para ver PDFs
tags: ["Aplicaciones","Web","PDF","GitHub","Javascript","HTML","JS"]
---

<!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="https://soka.gitlab.io/blog/post/2019-09-15-pdfjs-lector-mozilla/" data-a2a-title="Firefox PDF.js - Visor PDF basado en HTML5 ">
<a class="a2a_button_twitter"></a>
<a class="a2a_button_email"></a>
<a class="a2a_button_linkedin"></a>
<a class="a2a_button_whatsapp"></a>
<a class="a2a_button_telegram"></a>
<a class="a2a_button_google_bookmarks"></a>
<a class="a2a_button_wordpress"></a>
<a class="a2a_button_google_gmail"></a>
</div>
<script>
var a2a_config = a2a_config || {};
a2a_config.locale = "es";
a2a_config.num_services = 22;
</script>
<script async src="https://static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->

![](images/Pdf-js_logo.svg.png)

[**PDF.js**](https://github.com/mozilla/pdf.js) es un visor PDF basado en HTML5 desarrollado por Mozilla, la [demo](https://mozilla.github.io/pdf.js/web/viewer.html) no impresiona especialmente, abre un lector PDF en el navegador Web. En las últimas versiones del navegador Firefox viene incluido de forma nativa, tal vez ya lo hayamos usado o nos resulte familiar. Para otros navegadores como Chrome se puede instalar como [extensión](https://chrome.google.com/webstore/detail/pdf-viewer/oemmndcbldboiebfnladdacbdfmadadm).

## Instalación

```
git clone https://github.com/mozilla/pdf.js.git
cd pdf.js
npm install -g gulp-cli
npm install
gulp server
```

El último comando ejecuta el servidor accesible en [http://localhost:8888/web/viewer.html](http://localhost:8888/web/viewer.html).

Si queremos visualizar alguno de los PDFs de prueba en un panel lateral [http://localhost:8888/test/pdfs/?frame](http://localhost:8888/test/pdfs/?frame)

## Usar PDF.js en una aplicación Web

Los más interesante llega ahora, [**PDF.js**](https://github.com/mozilla/pdf.js) está desarrollado con JS y además su código fuente está disponible bajo una [licencia](https://github.com/mozilla/pdf.js/blob/master/LICENSE) que permite su uso en una aplicación Web propia. 

La forma más fácil de usarlo en nuestra aplicación es hacer referencia a la API PDF.js usando un [CDN](https://www.xatakamovil.com/conectividad/cdn-que-es-para-que-sirve-y-por-que-no-rompe-con-la-neutralidad-de-la-red) (Content Delivery Network o Red de Distribución de Contenido):

- [https://www.jsdelivr.com/package/npm/pdfjs-dist](https://www.jsdelivr.com/package/npm/pdfjs-dist).
- [https://cdnjs.com/libraries/pdf.js](https://cdnjs.com/libraries/pdf.js).
- [https://unpkg.com/pdfjs-dist/](https://unpkg.com/pdfjs-dist/).

## Ejemplos

En este enlace se puede ver un ejemplo básico [https://codesandbox.io/s/pmy914l2kq](https://codesandbox.io/s/pmy914l2kq) obtenido del siguiente post - ["Implement a Simple PDF Viewer with PDF.js"](https://pspdfkit.com/blog/2019/implement-pdf-viewer-pdf-js/).

Lo he reproducido en este blog en:

- blog / content / post / 2019-09-15-pdfjs-lector-mozilla / src / [helloworld](https://gitlab.com/soka/blog/tree/master/content/post/2019-09-15-pdfjs-lector-mozilla/src/helloworld).

El proyecto está formado un fichero HTML y otro JS.

## Enlaces externos

- mozilla.github.io ["PDF.js - Mozilla on GitHub"](https://mozilla.github.io/pdf.js/): Sitio principal del proyecto.
- [mozilla/pdf.js](https://github.com/mozilla/pdf.js): Proyecto en GitHub.
- ["https://mozilla.github.io/pdf.js/web/viewer.html"](https://mozilla.github.io/pdf.js/web/viewer.html): Demo en línea.
- ["Implement a Simple PDF Viewer with PDF.js"](https://pspdfkit.com/blog/2019/implement-pdf-viewer-pdf-js/).
- ["Rendering PDF Files in the Browser with PDF.js"](https://pspdfkit.com/blog/2018/render-pdfs-in-the-browser-with-pdf-js/).