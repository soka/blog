---
title: Hola Python! 
subtitle: Mi primer script 
date: 2022-07-19
draft: false
description: Taller Python
tags: ["Cursos","Talleres","Python","Python - Introducción","Python - Mi primer script"]
---

![](img/Python_logo-01.png)

La mejor forma de comenzar en cualquier lenguaje es imprimiendo por consola un mensaje de bienvenida, el ejercicio típico.

Creo un fichero vacío con el comando `touch hello_world.py` y le doy permisos de ejecución con `chmod +x hello_world.py`. Ahora que está preparado podemos abrirlo con nuestro editor favorito ,nano, Vim, Visual Studio Code o el que quieras.

Nuestro primer programa es muy sencillo, consta de dos líneas, la primera línea es un [_shebang_](https://es.wikipedia.org/wiki/Shebang) que comienza con `#!` seguido luego del interprete del programa.

```python
#!/usr/bin/env python3
print("Hello World!")
```

El código que lee el interprete de Python es la segunda línea realmente, le estamos pasando una cadena a la función `print`. Ejecutamos la aplicación con `./hello_world.py`.

Los comentarios comienzan con `#` y """comentarios en varias lineas""".

## Ayuda integrada

Puedes buscar ayuda en cualquier momento con la función `help`, por ejemplo:

```python
help('print')
```

## Interprete de comandos

Podemos hacer lo mismo desde el interprete de comandos:

```python
$ python
Python 3.7.3 (default, Jul 25 2020, 13:03:44) 
[GCC 8.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> print('hello world!')
hello world!
>>> quit()
```


