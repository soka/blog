---
title: Mapas mentales para equipos creativos
subtitle: Representación gráfica de ideas 
date: 2019-02-04
draft: false
description: Fomenta la creatividad en reuniones de equipo usando mapas mentales
tags: ["Herramientas","Técnicas","Gestión de proyectos","Diagramas","Creatividad"]
---

Este post es el fruto de una conversación informal sobre [pensamiento visual](https://es.wikipedia.org/wiki/Pensamiento_visual), gracias a Eva que siempre me hace ver las cosas desde una perspectiva diferente, enriquecedora y menos técnica sobre todo.

# ¿Qué son los mapas mentales?

Los [**mapas mentales**]((https://es.wikipedia.org/wiki/Mapa_mental)) son una técnica de representación gráfica muy usada para visualizar conceptos ordenados de forma no líneal partiendo o en torno a una idea central, es una **forma muy creativa y flexible** de asociar y ordenar ideas. 

![](images/Tennis-mindmap.png)

[[Fuente](https://es.wikipedia.org/wiki/Mapa_mental)]

Un concepto que inspira la técnica de los mapas mentales es el **el pensamiento irradiante**, se trata de la forma natural de trabajar de nuestro cerebro, las neuronas forman redes y se ramifican de la misma forma que un mapa mental encadenando unas ideas con otras mediante **asociaciones e imágenes** que hacen que las ideas sean **más fáciles de recordar**. 

# Un poco de historia 

Hoy en día estamos acostumbrados a describir una idea de forma líneal, imagina que tuvieses que contar a un amigo el argumento de una película, probablemente te limitarías a narrar una sucesión de echos uno tras otro de forma consecutiva y usando únicamente palabras, no estamos acostumbrados a expresar ideas usando dibujos, las imagenes son relegadas a un segundo plano para complementar un texto, pero la representación mediante dibujos o símbolos no es nueva, civilaciones antiguas como los egipcios usaban jeroglíficos para representar palabras o los mismos griegos con su sistema mnemotécnico de escritura. 

![](images/jeroglifico_01.png)

[[Fuente](https://www.edistribucion.es/anayaeducacion/DEMOS_PIEZA/8421728493370/html/ON_U6/06/Actividades/u05_fin_jugando/index.html?idclase=12795)]

En la era moderna se atribuie a [Tony Buzan](https://es.wikipedia.org/wiki/Tony_Buzan) haber acuñado el término "Mind Maps" y es un autor muy prolífico que ha [escrito gran número de libros sobre el tema](https://www.casadellibro.com/libros-ebooks/tony-buzan/43628).

![](images/use_your_head.gif)

# Como funcionan

Los mapas mentales son una forma eficaz de **hacer trabajar nuestra mente**, para empezar colocamos en el centro la idea principal sobre la que queremos trabajar, a partir de esa idea a su alrededor vamos situando ideas secundarias relacionadas añadiendo nueva información y sobre todo generando ideas nuevas. 

Los mapas mentales comparten una serie de **características o elementos en común**: **Más ideas en menos palabras**, cada idea se intenta expresar con palabras clave de forma sintética, las palabras se enlazan unas con otras dando un sentido a todo, a menudo nos apoyamos en **recursos visuales como imágenes**, simbolos, fotografías o dibujos ya que facilitan el recuerdo de ideas, los **colores** pueden ser usados para destacar diferentes áreas de nuestro mapa.

El uso de estos diagramas aporta una **visión global**, facilitan la **orientación para tomar determinadas decisiones** posicionando las ideas clave sobre un problema a resolver y sus posibles soluciones, también permiten agrupar gran cantidad de **información** de forma muy visual. Los mapas mentales se usan a menudo en reuniones de tormenta de ideas o en el arranque de un nuevo proyecto.

# Cómo elaborar un mapa mental

El uso de mapas mentales estimula la creatividad y favorece la asociación de ideas. En general sirven para concertrarnos separar lo importante de lo trivial, aclarar ideas o facilitar recordarlas más adelante. Podemos usarlos en el ámbito personal para tomar apuntes, resumir una idea o planificar un evento, solucionar un problema o emprender un nuevo proyecto.

Para empezar podemos usar una **hoja de papel y rotuladores de colores**, identificamos una idea central, buscamos una imagen que la represente y la dibujamos en el centro de la hoja, escribimos junto al dibujo nuestra idea fuerza en mayusculas. Partiendo de la idea principal debemos identificar aspectos que nos interesén sobre ella y que creemos que tienen relación con esa idea principal, de la idea central emanan otros conceptos relacionados conectados mediante líneas a la idea central, podemos añadir dibujos a esas ideas básicas también. Podemos continuar ampliando el mapa cuanto queramos ramificando las ideas básicas. 

**Algunas reglas básicas para crear mapas mentales**:

* **Empieza el diagrama en el centro del área de trabajo**, de esta forma podremos desplegar el resto de conceptos relacionados en todas direcciones.
* Dibuja una **imagen que simbolize la idea principal** y llame la atención, como reza el dicho "una imagen vale mas que mil palabras".
* **Usa colores diferentes**, estos despiertan tu creatividad. 
* Partiendo de la idea central **traza ramas hacia el exterior** con las palabras clave más importantes. El grosor de las líneas puede disminuir a medida que nos alejamos del centro.
* Traza **líneas curvas** en lugar de rectas.
* **Reduce a una o dos palabras clave por cada línea**, es más fácil recordar una palabra que una frase.
* **Usa todas las imágenes que puedas**.

# Herramientas digitales

Aquí van una serie de aplicaciones software para elaborar mapas mentales, algunas las puedo recomendar porque las he usado para enfrentarme a problemas reales y retos en equipos de proyecto.

## FreeMind

[**FreeMind**](http://freemind.sourceforge.net/wiki/index.php/Main_Page) es una herramienta de software libre (licencia  GNU GPL V2+) escrita en [Java](https://www.java.com/es/download/) para diseñar mapas mentales, antes de comenzar a usarlo debemos [descargar la aplicación](https://sourceforge.net/projects/freemind/files/latest/download) e instalarla en nuestra computadora.

![](images/freemind_01.PNG)

Permite crear nodos jerarquizados como "hermanos" o "hijos" alrededor del nodo central, podemos incrustar algunos iconos predefinidos por la propia aplicación, también podemos incrustar imágenes como nodos y añadir notas sobre los nodos en un editor aparte. 


## Mindmeister: Mapas Mentales en Línea

Con [**Mindmeister**](https://www.mindmeister.com/es) puedes diseñar mapas mentales en línea. Tiene un plan de precios y una versión básica gratuita con la que podemos crear hast 3 mapas. El registro es muy sencillo usando una cuenta de Google o FB.

![](images/mindmeister_01.PNG)

Permite incrustar imagenes y videos como nodos (incluye un buscador directo a Google) y exportar el resultado a varios formatos, es una aplicación muy sencilla de usar y los mapas generados son muy vistosos, ló único es que **es de pago**.

## Coggle: Simple Collaborative Mind Maps & Flow Charts

[**Coggle**](https://coggle.it/?lang=es) también es de pago y ofrece un plan gratuito para probarlo, podemos generar hasta 3 diagramas privados y un número limitado de ellos públicos. Al igual que el resto de las aplicaciones los trabajos se pueden importar y exportar como ficheros .mm.

![](images/coggle_01.PNG)

Lo bueno de estas herramientas en línea es que los diagramas pueden ser compartidos y así crearlos de forma colaborativa.

## XMind - Mind Mapping Software

[**XMind**](https://es.wikipedia.org/wiki/XMIND) es otra aplicación de escritorio de pago. Permite probarla en modo evaluación.

![](images/xmind_01.PNG)

## Freeplane: Application for Mind Mapping, Knowledge and Project Management

Otra propuesta de [@izaro@mastodon.social](https://mastodon.social/@izaro) es [**Freeplane**](https://www.freeplane.org/wiki/index.php/Home), requiere de instalación local y es de código abierto.

![](images/freeplane_01.PNG)

Permite añadir notas y editar el diseño de los nodos, insertar enlaces e inscrutar imágenes, una herramienta a tener en cuenta.

# MindMup for Google Drive

Finalmente una de las aplicaciones que más uso yo es [**MindMup**](https://www.mindmup.com/) por la simple razón que se integra con Google Drive y funciona en el navegador, es muy sencillo de usar y podemos crear mapas en pocos minutos.

![](images/MindMup_01.PNG)

# Enlaces

* ["Mapa mental - Wikipedia, la enciclopedia libre"](https://es.wikipedia.org/wiki/Mapa_mental).
* Udemy ["Mapas mentales desde cero"](https://www.udemy.com/mapas-mentales-desde-cero/learn/v4/content).
* ["Curso de mapas mentales [Gratis y Certificado] - Edutin University"](https://edutin.com/curso-de-Mapas-Mentales-1466).
* ["Curso Mapas Mentales"](http://videocursosonline.com/curso-mapas-mentales).

**Más aplicaciones**:

* ["Bubbl.us"](https://bubbl.us/).
