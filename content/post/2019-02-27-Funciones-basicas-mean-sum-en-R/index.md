---
title: Funciones básicas en R
subtitle: mean() y sum() 
date: 2019-02-27
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico","Aritmética","R - Funciones"]
---

Vamos a revisar algunas funciones estadísticas y aritméticas básicas. 

# mean() - Media aritmética

La función ´mean()´ cálcula el promedio o la media aritmética de un conjunto de valores normalmente definidos como un vector como parámetro, aunque puede trabajar con una variedad de objetos, la media se obtiene se obtiene a partir de la suma de todos sus valores dividida entre el número de sumandos.

![](https://wikimedia.org/api/rest_v1/media/math/render/svg/45369dd1e80668d3f6df7233fe4e2e0361d5c851)

[[Fuente](https://es.wikipedia.org/wiki/Media_aritm%C3%A9tica)]

Este es el ejemplo más sencillo posible:

```R
> datos <- c(12,40,54,34)
> media <- mean(datos)
> media
[1] 35
```

# sum() - Suma de valores

Está función es aún más sencilla, retorna la suma de valores pasados como parámetros:

```R
> sum(datos)
[1] 140
```

# max() - Valor máximo

```R
> max(datos)
[1] 54
```

# min() - Valor mínimo

```R
> min(datos)
[1] 12
```

# sd() - Desviación estándar

Es una medida que se usa para cuantificar la variación o dispersión de un conjunto de datos numéricos. Una desviación estándar baja indica que la mayor parte de los datos de una muestra tienden a estar agrupados cerca de su media aritmética.

```R
> sd(datos)
[1] 17.47379
```

# Ejercicios

* r / intro / 15-mean-sum / [mean-sum.R](https://gitlab.com/soka/r/blob/master/intro/15-mean-sum/mean-sum.R).

# Enlaces

**Enlaces externos:**

* ["Funciones en R"](https://rpubs.com/osoramirez/93049).
* ["Media aritmética - Wikipedia, la enciclopedia libre"](https://es.wikipedia.org/wiki/Media_aritm%C3%A9tica).