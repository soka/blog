---
title: Trello - Uso de la tarjeta de envejecimiento Power-Up
subtitle: La pizarra digital inspirada en kanban
date: 2019-07-04
draft: false
description: Creando tarjetas en Trello
tags:
  [
    "Trello",
    "Talleres",
    "Cursos",
    "Aplicaciones",
    "Trello - Introducción",
    "Trello - Tarjetas",
    "Trello - Power-Ups",
    "Kanban",
    "Metodologías",
    "Gestión de Proyectos",
    "Herramientas",
  ]
---

![](img/logo-trello.png)

Los **Power-Ups** permiten agregar funcionalidades extra a **Trello** y en ocasiones permiten integrarse con aplicaciones de terceros. **Los Power-Ups están ideados especialmente para las cuentas de pago _Gold_ (3 por tablero) o _Business Class_**.

## Habilitar los Power-Up

Se habilitan desde el menú del tablero que se en el lateral derecho del tablero:

![](img/01.gif)

## Power-Up envejecimiento de tarjetas

Con este **Power-Up** las tarjetas con poca actividad cambian su visualización, es un identificador visual muy útil cuando manejamos un tablero con muchos elementos, el **Power-Up** ofrece dos modos de visualización "Normal" (las tarjetas sin actividad se difuminan) o "Pirata" (las tarjetas se van rompiendo y adquieren un color amarillo).

![](img/02.PNG)

# Enlaces externos

- ["¿Qué son los Power-Ups?"](https://help.trello.com/article/1094-what-are-power-ups).
- ["https://trello.com/power-ups"](https://trello.com/power-ups): Buscador Power-Ups.
- ["Uso de la tarjeta de envejecimiento Power-Up"](https://help.trello.com/article/820-card-aging).
