---
title: Contenedores Docker con R [Borrador en progreso]
subtitle: Aplicaciones R dentro de un contenedor de software
date: 2019-04-13
draft: false
description: R, Docker y GitLab CI
tags: ["R", "Docker", "Aplicaciones", "GitLab", "Software"]
---

![](images/docker_facebook_share.png)

# Integración continua con GitLab, Docker y R

Mi equipo del trabajo es MS Win10 x64 y tengo la última versión estable "Docker for Windows Installer.exe" descargada de este [enlace](https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe).

## Algunos de los comandos más utilizados en Docker

Una vez instalado Docker Desktop puedo abrir un terminal PowerShell y comenzar a introducir comandos (o desde la propia consola de Visual Studio Code):

Versión de la aplicación:

```PS
PS C:\> docker version
Client: Docker Engine - Community
 Version:           18.09.2
 API version:       1.39
 Go version:        go1.10.8
 Git commit:        6247962
 Built:             Sun Feb 10 04:12:31 2019
 OS/Arch:           windows/amd64
 Experimental:      false

Server: Docker Engine - Community
 Engine:
  Version:          18.09.2
  API version:      1.39 (minimum version 1.12)
  Go version:       go1.10.6
  Git commit:       6247962
  Built:            Sun Feb 10 04:13:06 2019
  OS/Arch:          linux/amd64
  Experimental:     false
```

Lista de imágenes:

```PS
PS C:\> docker images
REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
docker4w/nsenter-dockerd   latest              2f1c802f322f        6 months ago        187kB
```

## Imágenes R para Docker

Descargamos la imagen [r-base](https://hub.docker.com/_/r-base/) de :

```PS
PS C:\> docker pull r-base
```

Una vez descargada ya tenemos un prompt de consola de R, sino podemos ejecutar el siguiente comando:

```PS
PS C:\> docker run -ti --rm r-base
```

Si ejecutamos `docker images` ahora tenemos una nueva máquina en la lista.

## GitLab CI

Debemos crear un fichero ".gitlab-ci.yml" (sintaxis [YAML](https://www.google.com/search?q=yml&rlz=1C1GCEU_esES821ES821&oq=yml+&aqs=chrome..69i57j69i61j69i60l2j0l2.2438j0j7&sourceid=chrome&ie=UTF-8)) en el raíz de nuestro repositorio. El fichero contendrá las instrucciones y comandos necesarios para probar un paquete con [testthat - Unit Testing for R](https://testthat.r-lib.org/).

```YML
image: rocker/rstudio
test:
  script:
    - R -e 'install.packages(c("ggplot2"))'
    - R CMD build . --no-build-vignettes --no-manual
    - PKG_FILE_NAME=$(ls -1t *.tar.gz | head -n 1)
    - R CMD check "${PKG_FILE_NAME}" --no-build-vignettes --no-manual
    - R -e 'devtools::test()'
```

Cuando subimos el fichero al repositorio se lanza un nuevo pipeline en GitLab y ejecuta los trabajos.

## Conclusiones

Con GitLab CI podemos realizar tests unitarios de un paquete R o incluso generar una Web estática generada con R.

## Enlaces externos

- ["Docker (software) - Wikipedia, la enciclopedia libre"](<https://es.wikipedia.org/wiki/Docker_(software)>).
- ["Install Docker Desktop for Windows"](https://docs.docker.com/docker-for-windows/install/).
- ["https://github.com/rocker-org/rocker"](https://github.com/rocker-org/rocker): R configurations for Docker.
- ["Developing R Packages with usethis and GitLab CI: Part II"](https://blog.methodsconsultants.com/posts/developing-r-packages-with-usethis-and-gitlab-ci-part-ii/).
- ["Example gitlab-ci.yml for R Projects"](https://www.r-bloggers.com/example-gitlab-ci-yml-for-r-projects/).
- ["Getting started with GitLab CI/CD"](https://docs.gitlab.com/ee/ci/quick_start/).
- ["Using Docker images"](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html).
- ["https://bookdown.org/yihui/blogdown/gitlab-pages.html"](https://bookdown.org/yihui/blogdown/gitlab-pages.html).
- ["Developing R Packages with usethis and GitLab CI: Part II"](https://www.r-bloggers.com/developing-r-packages-with-usethis-and-gitlab-ci-part-ii/).
- ["Developing R Packages with usethis and GitLab CI: Part I"](https://blog.methodsconsultants.com/posts/developing-r-packages-using-gitlab-ci-part-i/).
- ["Creating a Website - Share a set of Distill articles as a website"](https://rstudio.github.io/distill/website.html).
- ["How GitLab can help in research reproducibility"](https://about.gitlab.com/2017/08/25/gitlab-and-reproducibility/).
- [PDF]["continuous integration with gitlab"](https://www.nersc.gov/assets/Uploads/2017-02-06-Gitlab-CI.pdf) - Tony Wildish.
