---
title: Lenguaje de etiquetas HTML
subtitle: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 0
date: 2022-04-25
draft: false
description: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 0
tags: ["PEPHPPAW_3","Cursos","Miriadax","Programación","Web","Desarrollo","HTML"]
---

A continuación encontrarás un vídeo que te permitirá adquirir habilidades en el manejo del lenguaje de etiquetas HTML, el cual será de vital importancia para comprender la estructura de un sitio web.

Parte I:

{{< youtube 7mKN-WGGCts >}}

[https://www.youtube.com/watch?v=7mKN-WGGCts](https://www.youtube.com/watch?v=7mKN-WGGCts).

Estructura de la página web con diversos contenidos:

[src/html&css.html](src/html&css.html)

Parte II:

{{< youtube NoG-n1rSKSI >}}

[https://www.youtube.com/watch?v=NoG-n1rSKSI](https://www.youtube.com/watch?v=NoG-n1rSKSI)

[https://www.w3schools.com/colors/colors_rgb.asp](https://www.w3schools.com/colors/colors_rgb.asp)

## Enlaces externos

* Te recomendamos visitar los siguientes contenidos para ampliar tu perspectiva en HTML [https://www.w3schools.com/html/default.asp](https://www.w3schools.com/html/default.asp).