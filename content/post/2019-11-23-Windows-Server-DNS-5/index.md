---
title: Servidor DNS
subtitle: Windows Server 2012
date: 2019-11-23
draft: false
description: Recursos Windows Server
tags: ["Windows","Sistemas","Windows Server","Windows Server 2012","DNS"]
---

## Un poco de teoría

Un servidor DNS resuelve el nombre de una máquina a una dirección IP.

**Métodos de resolución de nombres**:

- [DNS](https://es.wikipedia.org/wiki/Sistema_de_nombres_de_dominio): Formado por una cadena alfanumérica de hasta 255 carácteres de longitud se compone del nombre de la máquina y el dominio (Nombre [FQDN](https://es.wikipedia.org/wiki/FQDN)).
- [LLMNR](https://en.wikipedia.org/wiki/Link-Local_Multicast_Name_Resolution): Este método se utiliza en Windows 8 y Windows Server 2012 debiendo de activarlo dentro del centro de redes y recursos compartidos utiliza la multidifusión para resolver direcciones IPv6 únicamente en nombres de ordenadores encontrados dentro de la subred. Cuando LLMNR está activado se utiliza antes que Netbios.
- [NetBIOS](https://es.wikipedia.org/wiki/NetBIOS): Los nombres NetBIOS son direcciones de 16 bytes que se utilizan para identificar un recurso de NetBIOS en la red. Los nombres NetBIOS son nombres únicos (exclusivos) o nombres de grupo (no exclusivos). Cuando un proceso NetBIOS se comunica con un proceso específico en un equipo determinado, se utiliza un nombre único. Cuando un proceso NetBIOS se comunica con varios procesos en varios equipos, se utiliza un nombre de grupo.

## Instalación del rol DNS

Como en la anterior ocasión instalando el rol DHCP sólo hay que seguir los pasos del asistente y marcamos la opción "Servidor DNS":

![](img/01.png)

Al marcar la opción detecta las dependencias necesarias así como las herramientas necesarias para administrar el servidor DNS. Una vez instalado se puede visualizar desde el administrador del servidor:

![](img/02.png)

## Configuración de zonas DNS

Para ponerlo en marcha debemos configurar una zona DNS para traducir nombres de máquinas dentro del dominio "curso.com" en direcciones IP. Desde el menú herramientas abro el administrador DNS.

![](img/03.png)

Para traducir nombres de máquinas en IPs tengo que crear una zona de búsqueda directa, con el botón derecho creo una nueva zona. Como el dominio "curso.com" aún no existe selecciono "Zona principal", el nombre de mi zona será "curso.com", se crea un fichero "curso.com.dns" en la carpeta "%SystemRoot%\system32\dns", aún no puedo escoger muchas opciones porque no tengo montado mi servidor de _Active Directory_.

![](img/04.png)

Ahora voy a crear registros para las máquinas dentro de esa zona ("New Host (A or AAAA)..."), primero el propio servidor:

![](img/05.png)

También puedo añadir un registro con la máquina de pruebas que tiene una IP reservada vía DHCP:

![](img/06.png)

Para que los equipos de la red consulten mi servidor DNS cambio las opciones en el administrador DHCP con el nuevo servidor DNS. Dentro del ámbito previamente configurado en la carpeta "Opciones de ámbito" añado la IP del nuevo servidor DNS.

![](img/07.png)

Para que coja la nueva configuración en el cliente libero la IP `ipconfig /release`, si hago un ping a "central.curso.com" no responde, **esto es porque por defecto una máquina Windows Server 2012 tiene el cortafuegos configurado para bloquearlo. Se puede crear una regla o deshabilitar el FW directamente** (eco ICMPv4 de entrada).

![](img/08.png)

## Testear el servidor DNS con nslookup

Usando el comando `nslookup` debería resolver el nombre y la IP del servidor DNS que tengamos configurado en el equipo.

En la imagen inferior se observa que no está resolviendo bien el nombre del servidor DNS, probablemente no está resolviendo un registro PTR (búsqueda inversa de la IP del servidor DNS al nombre del _host_) con el recurso que con el _hostname_ de la dirección IP especificada (el servidor DNS).

![](img/09.jpg)

Una vez dentro con el comando `server` podemos especificar el servidor DNS (Ej: `server central`), a continuación simplemente introduzco el nombre del dominio (Ej: `curso.com`):

![](img/10.jpg)

Otros comandos interesantes:

- `ipconfig /flushdns`  limpia esta caché de registros del equipo pero recuerda que conforme el sistema empiece a realizar consultas DNS de nuevo estas empezaran a guardarse en la cache.
- `ipconfig /displaydns` muestra los valores DNS que se están almacenados en la caché de nuestro sistema.


## Monitorización y resolución de problemas

Usando el visor de eventos:

![](img/12.jpg)

Entre las propiedades del servidor DNS se puede el nivel de logs:

![](img/11.jpg)

En la pestaña "Registro de depuración" se puede habilitar un registro con los paquetes enviados y recibidos por el DNS:

![](img/13.jpg)

La pestaña "Supervisión" permite lanzar una prueba:

![](img/14.jpg)

Después consultaré el fichero de volcado con los paquetes de la consulta.

## Utilidad DNSLint

El comando dnslint es un comando externo, antes de poder ejecutarlo se debe [descargar el archivo](http://download.microsoft.com/download/2/7/2/27252452-e530-4455-846a-dd68fc020e16/dnslint.v204.exe).

![](img/15.png)









