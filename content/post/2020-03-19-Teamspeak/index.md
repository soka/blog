---
title: TeamSpeak chat de voz
subtitle: Aplicaciones m    ensajería alternativa, privacidad, anonimato y libertad
date: 2020-03-19
draft: false
description: 
tags: ["Aplicaciones","Linux","Debian","TeamSpeak"]
---

![](img/logo.png)

[TeamSpeak](https://teamspeak.com/es/) es un software chat de voz sobre IP, permite a los usuarios hablar en un canal de chat con otros usuarios ([Wikipedia](https://es.wikipedia.org/wiki/TeamSpeak)), es muy usado por los adictos a los juegos en línea para para comunicarse dentro de equipos.

Es necesario instalarse un cliente TeamSpeak que se conecta a un servidor para crear un canal y poder hablar. El puerto UDP que utiliza TeamSpeak es el 9987.

## Instalación cliente TeamSpeak en Debian

Descargamos el cliente para Linux x64 3.3.2 del [área de descargas](https://teamspeak.com/es/downloads/).

Le doy permisos de ejecución y lo ejecuto:

```sh
sudo chmod +x  TeamSpeak3-Client-linux_amd64-3.3.2.run
./TeamSpeak3-Client-linux_amd64-3.3.2.run
cd TeamSpeak3-Client-linux_amd64
./ts3client_runscript.sh
```

![](img/01.png)

Parece muy sencillo de usar, se pueden administrar las conexiones, configurar y administrar el micro y otra serie de herramientas como una agenda de contactos o una grabadora muy útil por ejemplo para reuniones o alguna charla.

## Conexión a un servidor

Listas de servidores:

- En la Web oficial por países [https://teamspeak.com/en/more/find-a-host/](https://teamspeak.com/en/more/find-a-host/).
- [https://www.tsviewer.com/](https://www.tsviewer.com/).
- [http://ts3list.2x.to/](http://ts3list.2x.to/).

Ahora usando el menú "Conexiones > Conectarse" 


![](img/02.png)

Ya estoy dentro. Una vez dentro me puedo crear un canal para mi:

![](img/03.png)

