---
title: Bubble Sort en R
subtitle: Algoritmo de ordenación Bubble Sort con R
date: 2019-09-12
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Algoritmos", "R - Básico","R - Funny","Bubble sort"]
---

![](images/out.gif)

A pesar de que no es el algoritmo de ordenación más eficiente es mi preferido, la [**ordenación de la burbuja**](https://es.wikipedia.org/wiki/Ordenamiento_de_burbuja) es muy intuitiva y su lógica es muy fácil de entender. Dada una lista de elementos la recorre y compara cada elemento con el siguiente, si el siguiente es menor intercambiamos las posiciones de los elementos en la lista (si vamos a ordenar de menor de mayor claro), repetimos el mismo proceso varias veces, en cada iteración "emerge" como una burbuja el siguiente elemento con mayor valor, al final de la primera iteración el mayor de todos estará en la última posición, en la siguiente volvemos a recorrer toda la lista excepto el último y así de forma sucesiva, el siguiente algoritmo obtenido de este [enlace](https://rosettacode.org/wiki/Sorting_algorithms/Bubble_sort#R) está implementado en R, para minimizar el número de iteraciones que realiza si no cambia alguna posición en alguna de las vueltas para el proceso completo.

```r
bubblesort <- function(v) {
  itemCount <- length(v)
  repeat {
    hasChanged <- FALSE
    itemCount <- itemCount - 1
    for(i in 1:itemCount) {
      if ( v[i] > v[i+1] ) {
        t <- v[i]
        v[i] <- v[i+1]
        v[i+1] <- t
        hasChanged <- TRUE
      }
    }
    if ( !hasChanged ) break;
  }
  v
}
 
v <- c(9,8,7,3,1,100)
print(bubblesort(v))
```

## Código fuente

- r / intro / 53-bubble-sort / [bubble-sort.R](https://gitlab.com/soka/r/blob/master/intro/53-bubble-sort/bubble-sort.R).

## Enlaces externos

- es.wikipedia.org ["Ordenamiento de burbuja"](https://es.wikipedia.org/wiki/Ordenamiento_de_burbuja).
- ["Bubble sort implemented in pure R"](https://www.r-bloggers.com/bubble-sort-implemented-in-pure-r/): El código está cogido de este post.
- ["Sorting algorithms/Bubble sort"](https://rosettacode.org/wiki/Sorting_algorithms/Bubble_sort#R).

