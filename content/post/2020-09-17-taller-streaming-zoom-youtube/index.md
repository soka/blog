---
title: Como programar un evento en streaming
subtitle: Usando Zoom Pro y Youtube
date: 2020-09-17
draft: false
description: Programar y compartir con antelación
tags: ["Zoom","Youtube","Streaming","Transmisión","Vídeo","Youtube Studio","Multimedia"]
---

## Introducción a Youtube Studio

[**Youtube Studio**](https://studio.youtube.com) son las tripas de nuestro canal de [Youtube](https://www.youtube.com/?hl=es&gl=ES), lo necesitamos para gestionar de manera eficiente nuestro **canal**, es el gestor de contenidos que usamos para **subir y editar videos**, para añadir la **descripción**, la **categoría**, las **etiquetas** que permitan que nuestros usuarios potenciales encuentren nuestros vídeos por esas palabras clave, para **programar emisiones**, **para hacer directos**. Vamos a verlo en profundidad en esta sección con tutoriales detallados.

## Configuración previa para transmitir en vivo desde Zoom a Youtube

**Hay que configurar previamente las dos aplicaciones para poder retransmitir en vivo**, estos pasos sólo son necesarios **una vez**. En las cuentas que usamos habitualmente ya está hecho pero es mejor revisarlo con antelación para evitar sorpresas desagradables, se explica en el apartado de Youtube la razón.

### Habilitar retransmisión en vivo en Zoom (una vez)

Inicia sesión en la Web de [**Zoom**](https://us02web.zoom.us/), accede a "Administración de Cuenta" > "Ajustes de cuenta" en el menú lateral. Ahora vete a la opción "En la reunión (Avanzada)" y comprueba que este habilitada la opción "Permitir transmitir reuniones en vivo", selecciona los servicios que quieras usar, **marca también la opción "Servicio personalizado de transmisión en vivo".**

![](img/01.png)

Enlaces:

- ["Transmisión de una reunión o seminario web en YouTube Live"](https://support.zoom.us/hc/es/articles/360028478292-Transmisi%C3%B3n-de-una-reuni%C3%B3n-o-seminario-web-en-YouTube-Live)

### Youtube Studio (una vez)

Para retransmitir en directo hay que activar esta opción en [**Youtube Studio**](https://studio.youtube.com), es importante saber que **el proceso de activación inicial tarda 24 horas**, por eso es mejor programar el evento con antelación para su difusión y contar con tiempo para habilitar Youtube.

Accedemos al portal de [**Youtube Studio**](https://studio.youtube.com) con nuestra cuenta de Google.

![](img/05.png)

En la esquina superior derecha usamos la opción "Emitir en vivo" y activamos la posibilidad de transmisión.

![](img/06.png)

![](img/02.png)

Enlaces:

- [Introducción a las emisiones en directo](https://support.google.com/youtube/answer/2474026?hl=es).

## Personalizar el canal

![](img/33.png)

## Subir un vídeo

[**Youtube Studio**](https://studio.youtube.com) no sólo permite grabar o retransmitir vídeos en la propia plataforma, podemos grabar un vídeo con cualquier aplicación o dispositivo y luego subirlo a nuestro canal ([descarga vídeo MP4 para pruebas](https://file-examples.com/index.php/sample-video-files/sample-mp4-files/)).

![](img/28.png)

Podemos definir algunos datos como:

- Título.
- Escribir [**descripciones**](https://creatoracademy.youtube.com/page/lesson/descriptions) con palabras clave puede ayudar a que los usuarios encuentren más fácilmente tus vídeos cuando hagan búsquedas. Puedes resumir el contenido del vídeo y añadir algunas palabras clave al principio de la descripción.
- [**Miniatura**](https://support.google.com/youtube/answer/72431?hl=es): Selecciona o sube una imagen que refleje el contenido del vídeo. Una buena miniatura destaca y llama la atención de los usuarios.
- [Listas de reproducción](https://support.google.com/youtube/answer/57792).
- Añadir [**pantallas finales**](https://support.google.com/youtube/answer/6388789?hl=es&ref_topic=9257785).
- Añadir [**tarjetas**](https://support.google.com/youtube/answer/6140493?hl=es&ref_topic=9257785) (¿Sólo si es público?).
- Visibilidad: Elige cuándo se publica el vídeo y quién puede verlo.

## Emisión con Webcam en YouTube Studio

Puedes emitir en directo fácilmente con la Webcam de tu ordenador y sin necesidad de ninguna herramienta adicional. 

En el portatal de [Youtube Studio](https://studio.youtube.com) Haz clic en Emitir en directo.

![](img/06.png)

En el menú de la izquierda, selecciona Webcam.

![](img/10.png)

Introduce un **título** y una **descripción**, y selecciona la **configuración de privacidad**, si es pública cualquiera pueda buscar y verla, si es oculta cualquiera con el enlace puede verla. También puedes programar tu emisión en directo para una fecha posterior (esto lo veremos más adelante con Zoom como emisor de la transmisión en vivo). Si no es un contenido para menores permite habilitar el chat. Más abajo se puede añadir una descripción y asignarlo a una categoría. También podemos configurar los dispositivos de captura de vídeo y audio.

![](img/11.png)

Haz clic en Siguiente. A continuación, la cámara capturará una miniatura, ahora haz clic en Emitir en directo.

![](img/13.png)

Cuando termines de emitir, haz clic en la opción Terminar emisión situada en la parte inferior de la página. Se nos muestra un resumen estadístico y la duración de la emisión. **Todas las transmisiones que duren menos de 12 horas se archivan automáticamente**. Puedes acceder a las emisiones que ya hayas terminado, las que estés realizando y las que tengas planeadas en la [pestaña En directo](https://support.google.com/youtube/answer/9228389#live_tab) (en el portal principal en la sección "Vídeos" la pestaña "En directo").

**Nota**: Es posible que debamos permitir en el navegador Web que Youtube Studio coja el control de la cámara y el micrófono.

**Puedes descargar archivos en formato MP4** de los vídeos que hayas subido a YouTube en 720p o 360p, dependiendo del tamaño del vídeo. También puedes utilizar [Google Takeout](https://takeout.google.com/settings/takeout/custom/youtube) para descargar todos los vídeos que hayas subido.

Enlaces

- [Crear una emisión en directo a través de una webcam](https://support.google.com/youtube/answer/9228389?hl=es&ref_topic=9257984)
- [Cómo usar tu cámara y micrófono](https://support.google.com/chrome/answer/2693767?co=GENIE.Platform%3DDesktop&hl=es-419) en Chrome.

## Edición de un vídeo

El editor de vídeo merece un tutorial aparte o más detallado, entre las funcionalidades que ofrece:

- Recortar.
- Añadir pistas de audio en un momento determinado.
- Añadir _teasers_ o pantallas finales.

![](img/30.png)

## Programar una emisión

### Programar emisión en Youtube Studio

Accedemos a [**Youtube Studio**](https://studio.youtube.com) y usamos la opción "Gestionar".

![](img/15.png)

Ahora pulsamos sobre el botón "PROGRAMAR EMISIÓN".

![](img/16.png)

![](img/04.png)

### Reunión en Zoom

En la misma Web de Zoom programamos una reunión o accedemos a una previamente configurada. Advertencia: Zoom no avisa de posibles incompatibilidades si se solapan varias reuniones).

![](img/26.png)

En la parte inferior **copia y pega la clave de transmisión. URL de transmisión y URL de página de transmisión en vivo** desde la configuración de transmisión a su reunión personalizada o configuración de transmisión en vivo.

![](img/03.png)

**Nota** : Puedes encontrar la URL de la página de transmisión en vivo haciendo clic en la flecha de compartir en el evento de Youtube y copiando el enlace allí.

![](img/17.png)

**La retransmisión de habilita una vez dentro de la reunión.** Usa la opción "Más" en los controles de la reunión. Haz clic en "En vivo en Servicio personalizado de transmisión en vivo". Siempre es preferible conectar Zoom a [**Youtube Studio**](https://studio.youtube.com)  media hora antes y comprobar que todo va bien y la información llega a [**Youtube Studio**](https://studio.youtube.com) .

Una vez que Zoom ha comenzado la transmisión en vivo, nuevamente **en el Panel de [**Youtube Studio**](https://studio.youtube.com) , haz clic en "EMITIR EN DIRECTO"**
, **la emisión no es pública hasta que lo hagamos**.

![](img/21.jpg)

En la URL que permite compartir la emisión en Youtube Studio podemos previsualizar la miniatura y la cuenta atrás para que comience la emisión.

![](img/22.jpg)

Cuando comienza la emisión en abierto la ventana de vídeo de Youtube Studio mostrará la leyenda "EN DIRECTO" junto con el tiempo que llevamos emitiendo.

## Consejos y preguntas frecuentes

**La imagen que se emite es la misma que la del organizador del evento**. Puedes compartir pantalla,cambiar a diferentes formas de visión,etc,. En Youtube se emitirá la pantalla del organizador.

Algo muy básico es **encuadrarse bien**, es importante que nuestra cabeza quede en la parte superior, un error muy común es situar nuestra cabeza justo en el centro de la imagen (somos muy cartesianos), deja mucho espacio por arriba y no queda bien.

**La importancia de las miniaturas** o _thumbnails_ en Youtube Studio,son estas fotos que ilustran el vídeo, es muy importante ya que es lo que ve el usuario antes de hacer clic, si es llamativo invitará a los usuarios a reproducirlo y ya tendremos una visita más, por el contrario si es gris y poco llamativo por muy bueno que sea el vídeo la gente no llega. La imagen de la miniatura personalizada debe tener el mayor tamaño posible, ya que también se utilizará como la imagen de vista previa del reproductor insertado. Para [las miniaturas personalizadas](https://support.google.com/youtube/answer/72431?hl=es) se recomienda lo siguiente: **resolución de 1280x720**, de menos de 2MB, si es posible, utiliza una imagen con una proporción de 16:9.

Relaciones de aspecto o proporciones más usadas:

![](img/27.png)

[Añade tarjetas a vídeos](https://support.google.com/youtube/answer/6140493?hl=es&ref_topic=9257785) para publicitar un canal o dirigir al público hacia una lista de reproducción.

**Podemos búscar que palabras clave, que le gusta a la gente y tendencias están de moda o fenómenos estacionales como una feria o congreso anual para tratar temas de actualidad** usando [Google Trends](https://trends.google.es/trends/?geo=ES), si nuestras publicaciones se basan en alguna palabra clave probablemente las visualizaciones se alinearán con las búsquedas globales de los usuarios.

Por otro lado puedes revisar la popularidad del canal o de los vídeos consultando las [**estadísticas de Youtube Studio**](https://support.google.com/youtube/answer/9002587?hl=es).

![](img/29.png)

Realiza una **prueba de velocidad** ([Test de velocidad de Movistar](https://www.movistar.es/particulares/test-de-velocidad/)), por ejemplo el envío de vídeo HD 1080p requiere 3,0 Mbps (subida/bajada) ([requisitos en la Web de soporte de Zoom](https://support.zoom.us/hc/es/articles/201362023-Requisitos-del-sistema-para-PC-Mac-y-Linux#h_d278c327-e03d-4896-b19a-96a8f3c0c69c)).  

Es mejor **usar dos pantallas cuando queremos compartir contenidos** y configurar la pantalla como escritorio extendido para tener unas notas personales en una y los contenidos que mostramos en otra.

Podemos usar las **listas de reproducción** que podemos crear nosotros mismos y clasificar de una manera personal nuestras creaciones.

La [**latencia de una emisión**](https://support.google.com/youtube/answer/7444635?hl=es) es el tiempo que transcurre entre el momento en que la cámara capta lo que está ocurriendo y el momento en que los espectadores lo ven. Al configurar la emisión en directo, ten presente que el nivel de latencia puede afectar a la visualización. Si interactúas con los espectadores en el chat en directo, es preferible que haya una latencia baja para responder a sus comentarios y preguntas. No obstante, si la latencia es muy baja, es posible que los espectadores experimenten problemas de almacenamiento en búfer durante la reproducción. En el panel de control de la emisión se pueden configurar tres niveles de latencia de emisiones en directo.

![](img/25.png)

Puedes usar la opción de [**moderar el chat en directo**](https://support.google.com/youtube/answer/9826490?hl=es) para promover buenas prácticas y comportamientos.

![](img/31.png)

Configura las siguientes herramientas de moderación para tenerlo todo listo antes de que empiece la sesión de chat en directo. Algunas pueden usarse tanto durante la emisión en directo como después.

- Moderadores: Estos usuarios pueden borrar comentarios, que aparecerán en la página Pendientes de revisión. También pueden moderar los mensajes de tus chats en directo.
- Usuarios aprobados:Los comentarios de estos usuarios se publican automáticamente y no se filtrarán para detectar palabras o enlaces bloqueados, o contenido que pueda ser inadecuado.
- Usuarios ocultos: Los comentarios y los mensajes del chat en directo de estos usuarios no se mostrarán. Pueden pasar varios días hasta que se retiren los comentarios y los mensajes del chat en directo.

![](img/32.png)

Permitir la **reproducción del chat en directo**: Si activas esta opción, los usuarios que vean el contenido después de que haya terminado de emitirse podrán ver tus conversaciones con los fans del chat en directo.

## Herramientas alternativas

Algunas alternativas de videoconferencía y broadcasting.

### Google Meet

[**Google Meet**](https://meet.google.com) permite realizar videoconferencias en grupo, la emisión en vivo se puede compartir mediante un enlace. Para almacenar una grabación en Google Drive se debe habilitar previamente en la [consola de administración](https://admin.google.com) de G Suite (los cambios pueden tardar hasta 24 horas habilitarse), se puede hacer para usuarios concretos, grupos o para toda la unidad organizacional.

Desde Google Calendar se puede crear un evento de emisión en vivo:

![](img/23.jpg)

Una vez dentro de la reunión en la esquina inferior derecha en el menú "Más opciones" iniciamos la grabación:

![](img/18.jpg)

Usando la misma opción detendremos la grabación que se almacena en Google Drive.

![](img/19.jpg)

Una vez acabada se envía un correo electrónico al organizador de la reunión y a la persona que realizó la grabación.

Enlaces:

- [Live stream a video meeting](https://support.google.com/meet/answer/9308630?product_name=UnuFlow&co=GENIE.Platform%3DDesktop&hl=en&visit_id=637367896753790632-2859134783&rd=1&src=supportwidget1).
- [Record a video meeting](https://support.google.com/a/users/answer/9846751?visit_id=637367896753790632-2859134783&rd=1)
- [Turn recording on or off for Meet](https://support.google.com/a/answer/7557052)
- [Set up a live stream with Google Calendar](https://support.google.com/meet/answer/9308630?product_name=UnuFlow&co=GENIE.Platform%3DDesktop&hl=en&visit_id=637367896753790632-2859134783&rd=1&src=supportwidget1)

### StreamYard

[**StreamYard**](https://streamyard.com), los [planes](https://streamyard.com/plan) de pago incluyen una versión gratuita con algunas limitaciones superpone el logo de la herramienta, 20 horas de vídeo al mes y hasta 6 participantes. Permite compartir pantalla.  

![](img/14.png)

### OBS Studio

![](img/obs-logo.jpg)

[**OBS**](https://obsproject.com/) (Open Broadcaster Software) es una aplicación libre y de código abierto para la grabación y transmisión de vídeo en vivo (_streaming_), permite captura de fuentes de video en tiempo real, composición de escena, codificación, grabación y retransmisión. [**OBS**](https://obsproject.com/) emplea el protocolo RTMP (Real Time Messaging Protocol) compatible con Youtube, [Twitch](https://es.wikipedia.org/wiki/Twitch) o Facebook Live entre otros.

![](img/24.png)

El programa funciona a base de “Escenas” que se componen usando diferentes “fuentes”. En cada “Escena” es posible modificar la posición o la cantidad de capas activas de tal forma que durante una sesión de captura y/o emisión se pueden modificar las capas activas.

**Enlaces**

- [OBS Studio - Software libre de grabación y transmisión de vídeo en vivo](https://soka.gitlab.io/blog/post/2020-04-30-obs-1/).

### Cisco Webex Meetings

[Cisco Webex](https://www.webex.com/es/video-conferencing.html) es una solución de videoconferencias profesional, el [plan](https://www.webex.com/es/pricing/index.html) gratuito permite hasta 100 participantes y reuniones de 50 minutos.

### Microsoft Stream

[Microsoft Stream](https://www.microsoft.com/es-es/microsoft-365/microsoft-stream) permite ofrecer eventos en directo y a petición con Microsoft 365.

### Whereby

[Whereby](https://whereby.com/) es un sistema de videoconferencia. Versión gratuita de un organizador, una sala y hasta 4 participantes, permite compartir pantalla y conectar son apps como Trello, Google Drive, etc.

### Otras alternativas

- [Lightstream Studio](https://studio.golightstream.com/welcome): Todos los planes son de pago. 
- Con [Openmeet](https://openmeet.io/) una vídeollamada es simple de organizar. Actualmente ya no tiene versión gratuita. Básicamente, lo único que tenemos que hacer es crear un encuentro en el acto, sin necesidad de cuentas de usuario, y compartir el enlace temporal que obtendremos

## Enlaces

- [Herramientas para teletrabajar de forma cómoda y sencilla](https://soka.gitlab.io/blog/post/2020-06-26-jornada-teletrabajo/) Contenidos de la jornada impartida por Imanol Terán.
- [Guía para el Uso soluciones tecnológicas de Teletrabajo](https://soka.gitlab.io/blog/post/2020-07-20-inplantariak-teletrabajo/).
- [Trabajo remoto y cuidado de equipos](https://soka.gitlab.io/blog/post/2020-07-14-trabajo-remoto-coordinadora-ong/) Coordinadora de organizaciones para el desarrollo.

Software libre, alternativas, euskera...

- [Reuniones y mensajería con herramientas alternativas](https://soka.gitlab.io/blog/post/2020-04-28-reuniones-y-mensajer%C3%ADa-colaborativa/).
- Librezale.eus [Komunikazio eta elkarlanerako baliabideak](https://librezale.eus/2020/03/15/komunikazio-eta-elkarlanerako-baliabideak/).
- [Herramientas de trabajo colaborativo](https://soka.gitlab.io/blog/post/2020-04-07-tabla-aplicaciones-trabajo-colaborativo/).
