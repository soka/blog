---
title: Matriz de riesgos
subtitle: Evaluación de riesgos en nuevos proyectos 
date: 2019-01-23
draft: false
description: Herramientas visuales para la gestión de proyectos 
tags: ["Gestión de proyectos","Herramientas","Técnicas"]
---

# Gestión del riesgo 

![](https://www.mireauxms.com/wp-content/uploads/2018/10/risk-assessment.jpg)

[[Fuente](https://www.mireauxms.com/blog/risk-assessment-101-right-first-time-every-time)]

En los tiempos actuales donde los cambios se producen a una velocidad vertiginosa afrontamos riesgos inevitables, lo que **debemos tratar es de prever que puede ir mal y seguramente sus posibles repercusiones** sobre nuestro proyecto o empresa. 

Cuando realizamos una **gestión del riesgo** la **evaluación e identificación** del riesgo es uno de los pasos **midiendo la magnitud del problema de forma cuantitativa y la probabilidad de que suceda**. El objetivo es minimizar el impacto si en el peor de los casos no podemos evitarlo.

En algunas empresas se confecciona un **checklist basado en problemas previos en el arranque de proyectos pasados**, son riesgos potenciales que hemos podido sufrir en otras ocasiones.

Aplicado a la ingeriería del software actividades que pueden producirnos incertidumbre pueden ser el cambio de los requisitos de un proyecto por parte del cliente o el temor de no cumplir con la planificación (algo muy habitual por cierto).

# Matriz de riesgos 

La **matriz de riesgos** es una **herramienta visual** para ayudarnos a realizar una evaluación de riesgos, un eje representa la probabilidad mientras que el otro mide la severidad (impacto, consecuencias o magnitud del daño por ejemplo en una escala de valores representadas como numéros).

Existen diferentes configuraciones de la matriz en Internet con más o menos grados de magnitud, me he basado en un ejemplo que me ha gustado.

Para completar la matriz debemos definir los aspectos y actividades principales que componen el proyecto, a continuación los identificamos los riesgos asociados a cada actividad, determinamos la posición que ocupacada actividad dentro de la matriz para determinar las medidas de control que tomaremos más adelante, puede ser interesante que en las sesiones para determinar acciones de control para paliar el riesgo participen personas de todas las áreas.

![](images/matriz_riesgo_01.PNG)

Cada celda tiene un valor que representa el riesgo combinado de cruzar la probabilidad y la severidad.

* **E** Riesgo extremo: Contiene niveles inaceptables de riesgo. Se requiere una revisión de la actividad para eliminar el riesgo.
* **H** Alto riesgo: Riegos potenciales que son probables que sucedan.
* **M** Riesgo moderado: Riesgos que son improbables que sucedan.
* **L** Bajo riesgo:  

Para cada actividad de riesgo podemos definir medidas de control y medir su impacto.

![](images/matriz_riesgo_02.PNG)

# Conclusiones

Es una **herramienta visual muy sencilla** de aplicar en reuniones de seguimiento de proyecto combinado con sesiones de tormenta de ideas, especialmente para determinar las medidas de control.

Permite **priorizar** aquellas **actividades** que puedan llevar el proyecto al desastre.

En algunos casos deberiamos **valorar eliminar ciertas actividades o no acometer un proyecto** si el riesgo no es razonable.

# Enlaces 

* Wikipedia ["Gestión de riesgos"](https://es.wikipedia.org/wiki/Gesti%C3%B3n_de_riesgos).
* Wikipedia ["Evaluación de riesgo"](https://es.wikipedia.org/wiki/Evaluaci%C3%B3n_de_riesgo).
* DZONE ["How to Use the Risk Assessment Matrix in Project Management"](https://dzone.com/articles/how-to-use-the-risk-assessment-matrix-in-project-m-1).
* SIGWEB [PDF] [Matriz de Riesgo, Evaluación y Gestión de Riesgos](http://www.sigweb.cl/wp-content/uploads/biblioteca/MatrizdeRiesgo.pdf).
* ntaskmanager.com ["
Using SWOT Analysis for Risk Identification and Risk Management"](https://www.ntaskmanager.com/blog/swot-analysis-for-risk-identification/).
* [NTask Manager](https://www.ntaskmanager.com/product/risk-management-software/): Aplicación Web, freemium.
* slideplayer [Matriz de Riesgos](https://slideplayer.es/slide/5502029/).
* ["Como crear una Efectiva Matriz de Riesgos en tan solo 3 Pasos"](http://www.ceolevel.com/como-crear-una-efectiva-matriz-de-riesgos-en-tan-solo-3-pasos).
* ["Blog Calidad y Excelencia
¿En qué consiste una matriz de riesgos?"](https://www.isotools.org/2015/08/06/en-que-consiste-una-matriz-de-riesgos/).
* slideshare ["Análisis de riesgos en un proyecto de software"](https://www.slideshare.net/angereyesmeet/anlisis-de-riesgos-de-un-proyecto-de-software).
* [PDF] ["Ingeniería del Software II"](https://ocw.unican.es/pluginfile.php/1408/course/section/1803/tema7-gestionRiesgos.pdf).
* 