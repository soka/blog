---
title: Crea elegantes diagramas con Chart.js
subtitle: Diagramas Web con JavaScript, Bootstrap y JQuery
date: 2019-03-25
draft: true
description:
tags:
  [
    "Chart.js",
    "Diagramas",
    "Reportes",
    "Bootstrap",
    "JQuery",
    "Web",
    "Aplicaciones",
    "Desarrollo",
  ]
---

# Enlaces externos

- ["Bootstrap 4 + Chart.js: Example Line, Bar and Donut Charts"](https://medium.com/wdstack/bootstrap-4-chart-js-39006427f08f).
- ["Chart.js | Samples"](https://www.chartjs.org/samples/latest/).
- ["Comenzando Con Chart.js: Gráficas de Línea y Barra - Code Tuts"](https://code.tutsplus.com/es/tutorials/getting-started-with-chartjs-line-and-bar-charts--cms-28384).
- ["10 Chart.js example charts to get you started"](http://tobiasahlin.com/blog/chartjs-charts-to-get-you-started/).
- ["Comenzando Con Chart.js: Gráficas de Pastel, Dona y Burbuja"](https://code.tutsplus.com/es/tutorials/getting-started-with-chartjs-pie-doughnut-and-bubble-charts--cms-28446).
- ["Visualizar Estadísticas De Forma Elegante Con Chart.js | Blog ..."](http://blog.hostdime.com.co/visualizar-estadisticas-de-forma-elegante-con-chart-js/).
- https://www.c-sharpcorner.com/article/create-easy-dashboards-in-html-using-chartjs/
