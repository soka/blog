---
title: Enviar respuestas automáticas en Outlook (OOF)
subtitle: Entradas del blog que nunca pensé que haría
date: 2021-07-29
draft: false
description: 
tags: ["Outlook"]
---

<!-- vscode-markdown-toc -->
* 1. [Aplicación Outlook en Windows](#AplicacinOutlookenWindows)
* 2. [Outlook Web Access (OWA)](#OutlookWebAccessOWA)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


##  1. <a name='AplicacinOutlookenWindows'></a>Aplicación Outlook en Windows

Accede a la pestaña "Archivo" del menú superior en Outlook.

![](img/01.PNG)

Podemos definir un intervalo de tiempo en el que se enviarán las respuestas automáticas (por ejemplo las vacaciones)

![](img/02.PNG)

Podemos definir diferentes respuestas automáticas para correos que nos envíen de nuestra organización y para todos aquellos que provienen de fuera (boletines, proveedores, colegas, etc)

![](img/03.PNG)

Si está todo correcto deberiamos ver una barra como la de abajo informandonos de que la respuesta automática está activada.

![](img/04.PNG)

**NOTAS**: 

* Si las respuestas automáticas están habilitadas, solo se envía una respuesta a cada remitente incluso si un destinatario recibe varios mensajes de un remitente ([Fuente](https://docs.microsoft.com/es-es/exchange/troubleshoot/email-delivery/understand-troubleshoot-oof-replies)).
* Si el remitente no recibe la respuesta que revise la carpeta de SPAM.


##  2. <a name='OutlookWebAccessOWA'></a>Outlook Web Access (OWA)

Acceso: [outlook.office365.com/owa/]

En la parte superior derecha de la pantalla acceder a la configuración y en el cuadro que se despliega "Ver toda la configuración de Outlook".

![](img/05.PNG)

En el apartado "Correo > Respuestas automáticas" configura las opciones: 

![](img/06.PNG)


##  3. <a name='Enlacesexternos'></a>Enlaces externos

* Enviar respuestas automáticas "Fuera de la oficina" de Outlook [https://support.microsoft.com/es-es/office/enviar-respuestas-autom%c3%a1ticas-fuera-de-la-oficina-de-outlook-9742f476-5348-4f9f-997f-5e208513bd67?ns=outlook&version=90&syslcid=3082&uilcid=3082&appver=zol900&helpid=168744&ui=es-es&rs=es-es&ad=es](https://support.microsoft.com/es-es/office/enviar-respuestas-autom%c3%a1ticas-fuera-de-la-oficina-de-outlook-9742f476-5348-4f9f-997f-5e208513bd67?ns=outlook&version=90&syslcid=3082&uilcid=3082&appver=zol900&helpid=168744&ui=es-es&rs=es-es&ad=es)