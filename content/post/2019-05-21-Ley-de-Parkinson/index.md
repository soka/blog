---
title: Ley de Parkinson
subtitle: El trabajo se expande hasta que llena todo el tiempo disponible para su realización
date: 2019-05-21
draft: false
description: Cómo hacer más tareas en la misma cantidad de tiempo
tags: ["Productividad", "Gestión de Proyectos", "Personajes", "Parkinson"]
---

![](img/03.jpg)

[[Fuente](https://blog.devolutions.net/2014/2/sysadminotaur-27-parkinsons-law)]

## Introducción

Este artículo es una revisión de uno similar que se incluye dentro de un taller que suelo impartir sobre productividad y gestión de proyectos con herramientas colaborativas ([enlace](https://soka.gitlab.io/TallerTrello/doc/chapter-productividad/04-parkinson/parkinson.html)), aprovechando que dentro de unas semanas volveré a impartir el taller voy a refrescar algunos datos sobre este singular personaje.

## Cyril Northcote Parkinson (1909-1993)

![](img/01.jpg)

[**Cyril Northcote Parkinson**](https://es.wikipedia.org/wiki/Cyril_Northcote_Parkinson) fue un historiador naval británico, entre sus libros destacan algunos satíricos que retratan el funcionamiento de la burocracia del gran imperio, especialmente la "La ley de Parkinson y otros estudios" ([_Parkinson's Law and other studies_](https://www.amazon.es/Parkinsons-Law-Other-Studies-Administration/dp/0464974364)) publicado en 1957.

![](img/02.png)

## Ley de Parkinson (1957)

Enunciada por el protagonista de este artículo en 1957 dice algo así:

> ...el trabajo se expande hasta llenar el tiempo disponible para que se termine...

En este [enlace](https://www.economist.com/news/1955/11/19/parkinsons-law) se puede encontrar el artículo original en ingles publicado por primera vez en el diario "The Economist".

Cabe contextualizar cual era el **momento histórico y especialmente la situación del imperio británico en la primera mitad del siglo XX**, durante esos años se empieza a producir una descolonización y la presencia del imperio británico en el panorama mundial empieza a decrecer, primero la India y más tarde algunos territorios de Asia y África reclaman su independencia. Mantener el dominio y la maquinaría burocrática sobre territorios tan lejanos cada vez tiene mayor coste social y económico y de forma gradual a medida que estos territorios se emancipan la necesidad de mantener toda esa red burocrática empieza a perder sentido. **Parkinson** se dió cuenta de que, **a pesar de haber cada vez menos trabajo burocrático en la Oficina Colonial británica, el número de funcionarios aumentaba cada año en más de un 5%**.

En una burocracia, la frase de **Parkinson** estaba motivada por dos factores:

1. **"Un funcionario quiere multiplicar sus subordinados, no rivales"**: Un trabajador que considere que tiene una carga excesiva de trabajo tiene básicamente tres posibilidades: resignar, solicitar a un compañero o solicitar a subordinados. De estas tres opciones, prácticamente siempre se tenderá a solicitar y argumentar la tercera solución. Así, el trabajo que antes realizaba un funcionario será dividido en dos subordinados.
2. **"Los funcionarios se crean trabajo unos a otros"**: El trabajo que antes realizaba una persona ahora lo realizarán dos personas. El resultado de los trabajos parciales deberá ser puesto en común y supervisado por la persona original.

De las observaciones que realizó dedujo las **tres leyes fundamentales de Parkinson**:

1. **"El trabajo se expande hasta llenar el tiempo de que se dispone para su realización".**
2. **"Los gastos aumentan hasta cubrir todos los ingresos".**
3. **"El tiempo dedicado a cualquier tema de la agenda es inversamente proporcional a su importancia"**: tendencia a dedicar demasiado tiempo a las minuciosidades.

Se ve muy fácil cuando se encomienda una tarea a cualquiera, si tenemos un mes de plazo, el trabajo se hará en un mes, aunque pueda hacerse en dos semanas. ¿Nunca os ha pasado? Cuando nos han encomendado una tarea o proyecto con una plazo de entrega tanto si empezamos al día siguiente como si lo dejamos para las últimas semanas siempre acabaremos unas horas antes de finalizar el plazo.

**¿Como funciona la Ley de Parkinson?** Básicamente todo el tiempo extra no se rellena con más trabajo que tenga un impacto en la calidad del resultado final, normalmente lo único que provoca es que estemos preocupados o [procastinando](https://es.wikipedia.org/wiki/Procrastinaci%C3%B3n).

![](img/08.jpeg)

[[Fuente](http://thepersonalgrowthlab.com/2-must-use-productivity-techniques-to-get-a-lot-more-done-today-backed-by-psychology/)]

## ¿Como solucionarlo?

Para muchos, cuanto más tiempo se tenga para hacer algo, más divagará la mente y más problemas serán planteados. En el momento de planificar tu trabajo, **marca unos plazos mucho más ajustados**. **Sé optimista en la estimación** y acertarás. Limitar el tiempo hace que te obligues a centrarte en lo importante, a ir al grano.

Debemos **crear una lista de tareas para cada día y acabarlas en un espacio de tiempo determinado**. No debemos pensar en el tiempo disponible (por ejemplo ocho horas de trabajo), sino en el tiempo que creamos que es estrictamente necesario para hacer las tareas.

Cuando hay recursos limitados encontramos creatividad, energía, recursos y foco.

Ley de Parkinson se conecta con nuestra tendencia a hacer las cosas más complejas de lo que son. **Reducir la complejidad de los procesos internos** y la burocracia entre diferentes áreas o equipos de trabajo, visibilizar el trabajo, la automatización de procesos rutinarios pueden ser valsamos para los sistemas complejos o de gran tamaño.

## Conclusiones

![](img/05.jpg)

La ley de Parkinson advierte de dos errores:

1. Crecer por un motivo equivocado.
2. Crear una burocracia interna que se lleve buena parte del tiempo.

Se debe **determinar el alcance de un proyecto o trabajo**. Siempre existe una posibilidad de mejora que provocará que el proyecto siga expandiéndose en el tiempo. **Más allá de cierto tiempo, no hay relación directa con los resultados**.

> ...El 80% del esfuerzo de desarrollo (en tiempo y recursos) produce el 20% de los resultados del proyecto, mientras que el 80% restante es producido con tan solo un 20% del esfuerzo... ([Principio de Pareto](https://es.wikipedia.org/wiki/Principio_de_Pareto))

**La Ley de Parkinson se conecta con la de Pareto** (conocida también como la regla del 80-20), porque nos obliga a identificar cuál es el 20% de la tarea que es más importante.

Estas tres leyes, al igual que otras que Parkinson formuló, como la _ley de la dilación o el arte de perder el tiempo_ y la _ley de la ocupación de los espacios vacíos_: por mucho espacio que haya en una oficina siempre hará falta más, son leyes extraídas de la experiencia cotidiana, mediante las cuales, al tiempo que se describe o pone de manifiesto una determinada realidad, se denuncia la falta de eficiencia del trabajo administrativo.

## Enlaces internos

- ["La ley de Parkinson"](https://soka.gitlab.io/TallerTrello/doc/chapter-productividad/04-parkinson/parkinson.html): Dentro de taller de productividad y gestión de proyectos.
- ["Mapas mentales para equipos creativos"](https://soka.gitlab.io/blog/post/2019-02-04-mapas-mentales/).
- ["Matriz de riesgos - Evaluación de riesgos en nuevos proyectos"](https://soka.gitlab.io/blog/post/2019-01-23-matriz-de-riesgos/).

## Enlaces externos

- sebastianpendino.com ["Ley de Parkinson. Cómo hacer más tareas en la misma cantidad de tiempo."](https://sebastianpendino.com/ley-de-parkinson-productividad/).
- Wikipedia ["Ley de Parkinson"](https://es.wikipedia.org/wiki/Ley_de_Parkinson).
- Wikipedia ["Cyril Northcote Parkinson"](https://es.wikipedia.org/wiki/Cyril_Northcote_Parkinson).
- Wikipedia ["Imperio británico"](https://es.wikipedia.org/wiki/Imperio_brit%C3%A1nico).
- facilethings.com ["Las 3 Leyes de la Productividad"](https://facilethings.com/blog/es/laws).
- mujeresdeempresa.com ["Pareto Y Parkinson: Dos Principios Para Aumentar Tu Productividad"](http://www.mujeresdeempresa.com/pareto-y-parkinson-dos-principios-para-aumentar-tu-productividad/).
- canasto.es ["Cómo ganar 2 horas productivas al día en un instante"](https://canasto.es/blog/2010/03/como-ganar-2-horas-productivas-al-dia-en-un-instante).
- Wikipedia ["Principio de Peter"](https://es.wikipedia.org/wiki/Principio_de_Peter).
- borcobritas.com ["LA LEY DE PARKINSON, O POR QUÉ SIEMPRE TE DEJAS ALGO AL HACER LA MALETA"](https://borcobritas.com/ley-de-parkinson/).
- ["2 Must-Use Productivity Techniques To Get A Lot More Done Today (Backed By Psychology)"](http://thepersonalgrowthlab.com/2-must-use-productivity-techniques-to-get-a-lot-more-done-today-backed-by-psychology/).
