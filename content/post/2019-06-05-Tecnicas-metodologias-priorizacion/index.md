---
title: Técnica de priorización MOSCOW
subtitle: Análisis de requerimientos en gestión de proyectos
date: 2019-06-05
draft: false
description: MoSCoW
tags:
  [
    "Técnicas",
    "Proyectos",
    "Tareas",
    "Gestión",
    "Equipos",
    "Requerimientos",
    "Modelos",
    "MoSCoW",
  ]
---

**Establecer las prioridades** para alcanzar el éxito en un nuevo proyecto no siempre es fácil, requiere un orden claro en el desarrollo de las funcionalidades que lo componen, conocer el impacto de cada uno o saber ponderar la importancia de cada uno de ellos en relación a los otros.

# Técnica de priorización MOSCOW

![](img/03.PNG)

Daig Clegg en 1994 ideó el método **MoSCoW** como técnica para priorizar requerimientos, se aplica en diversos ámbitos como pueden ser análisis de un negocio, la gestión de proyectos o el desarrollo de un nuevo software.

El termino **MoSCoW** es un acrónimo formado por las primeras letras de cada tipo de prioridad, se resume en las siguientes cuatro categorías: _MUST have_ (debe tener); _SHOULD have_ (debería tener); _COULD have_ (podría tener); _WON’T have_ (no tendrá).

En contraste con la forma típica de priorizar (alta, media, baja) donde al final da igual el nivel y todo se debe hacer (además esta forma de priorizar no ofrece una promesa clara sobre que esperar del producto final), con el método **MoSCoW** a medida que analizamos los requisitos nos damos cuenta que no todo es obligatorio ni hay que hacerlo en este momento, **la escala que usamos tiene un significado intrínseco** frente a otras técnicas tradicionales, las elecciones realizadas tendrán un impacto real sobre el producto final.

## Categorías "Must, Should, Could, Won’t"

- **MUST have (debe tener)**: Se pueden considerar como atributos que determinan el MVP ([_minimum viable product_](https://en.wikipedia.org/wiki/Minimum_viable_product)), son **requerimientos indispensables o mínimos** y normálmente no son negociables, sino se logra el conjunto de requerimientos de este tipo se considera que el proyecto no ha tenido éxito.
- **SHOULD have (debería tener)**: Este tipo de requisitos suelen ser **altamente deseables** pero no cruciales. Puede tratarse de funcionalidades que no son tan urgentes en este momento (pero puede que lo sean en un periodo corto de tiempo). Este tipo de atributos se centran en obtener un mayor [retorno de la inversión ROI](https://es.wikipedia.org/wiki/Retorno_de_la_inversi%C3%B3n), se pueden evaluar midiendo la cantidad de usuarios afectados por este requisito. **El producto final aún sigue siendo plenamente funcional** sin estos requerimientos.
- **COULD have (podría tendría)**: También se les conoce como _nice-to-have_. A pesar de que pueden ser interesantes no suelen ser importantes, **no llevarlos a cabo no reduce la viabilidad del proyecto**, no tienen gran impacto en el resultado final. Pueden ser funcionalidades extra que se acometan siempre y cuando quede tiempo y dinero una vez realizadas las anteriores o no exijan excesivo esfuerzo.
- **Won’t have/ Would-have (no tendría)**: Requisitos o deseos que **no merecen aún la pena invertir en ellos**. También se conocen como _would have_ porque una empresa podría implementarlos bajo ciertas condiciones en el futuro.

![](img/04.PNG)

## Conclusiones, pros y contras del modelo MoSCoW

Está técnica se implementa de forma **rápida y es sencilla de aplicar**. Si se realiza en equipo todas las personas integrantes forman parte de la decisión de la prioridad de cada trabajo a realizar y cual debe ser el resultado final del conjunto.

Permite redirigir los recursos (materiales, de personas o tiempo) de los que disponemos a las categorías más importantes, **si en algún momento te quedas sin recursos para seguir trabajando, te aseguras que lo más importante siempre está hecho**.

Los deseos y requisitos nunca son estáticos, dependen de la situación y el momento, **pueden cambiar de categoría a través del tiempo**.

Este método se suele aplicar **cuando existen plazos límite de entrega y debemos poner el foco en los requisitos más importantes**.

Esta técnica está muy **dirigida a aspectos funcionales o atributos que proporcionan un valor añadido sobre un producto final**, puede casar muy bien para definir MVP de un nuevo proyecto.

# Enlaces internos

- ["Ley de Parkinson"](https://soka.gitlab.io/blog/post/2019-05-21-ley-de-parkinson/): El trabajo se expande hasta que llena todo el tiempo disponible para su realización.
- ["Mapas mentales para equipos creativos"](https://soka.gitlab.io/blog/post/2019-02-04-mapas-mentales/).
- ["Matriz de riesgos"](https://soka.gitlab.io/blog/post/2019-01-23-matriz-de-riesgos/): Evaluación de riesgos en nuevos proyectos.
- ["Lo urgente y lo importante: un secreto del ex presidente Eisenhower"](https://soka.gitlab.io/TallerTrello/doc/chapter-productividad/03-eisenhower/eisenhower.html).

# Enlaces externos

- ["The Most Popular Prioritization Techniques and Methods: MoSCoW, RICE, KANO model, Walking Skeleton, and others"](https://www.altexsoft.com/blog/business/most-popular-prioritization-techniques-and-methods-moscow-rice-kano-model-walking-skeleton-and-others/).
- Wikipedia ["MoSCoW method"](https://en.wikipedia.org/wiki/MoSCoW_method).
- ticportal.es ["Método MoSCoW - Una manera uniforme de priorizar con el método MoSCoW"](https://www.ticportal.es/glosario-tic/metodo-moscow).
- ["Técnica de priorización MoSCoW"](https://www.laboratorioti.com/2016/09/26/tecnica-priorizacion-moscow/).
- ["Time Boxing Planning: Buffered Moscow Rules"](https://www.stickyminds.com/article/time-boxing-planning-buffered-moscow-rules).
- agilebusiness.org ["MoSCoW Prioritisation"](https://www.agilebusiness.org/content/moscow-prioritisation)
- https://www.hotpmo.com/management-models/moscow-kano-prioritize
