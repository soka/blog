---
title: Extensiones de navegadores para desarrollo web 
subtitle: CSSViewer, All Online Tools in “One Box”, VisBug
date: 2022-03-02
draft: false
description: CSSViewer, All Online Tools in “One Box”, VisBug
tags: ["Web","Desarrollo Web","Desarrollo","Diseño Web","Web Developer","Extensiones","Aplicaciones","Chrome","Firefox","Herramientas","VisBug","CSSViewer","10015.io"]
---

<!-- vscode-markdown-toc -->
* 1. [VisBug](#VisBug)
* 2. [All Online Tools in “One Box”](#AllOnlineToolsinOneBox)
* 3. [CSSViewer](#CSSViewer)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

En este entrada recopilo 3 extensiones del navegador para desarrolladores web, no son las únicas, seguro que hay otras igual de buenas, no voy a decir que son imprescindibles o que no puedes vivir sin ellas.

##  1. <a name='VisBug'></a>VisBug

[VisBug](https://chrome.google.com/webstore/detail/visbug/cdockenadnadldjbbgcallicgledbeoc) es una extensión creada con JS, carga un panel flotante lateral con un montón de utilidades: regla para medir las distancias, inspeccionar el CSS, test de accesibilidad, mover elementos, editar los textos,etc.

![](img/01.gif)

##  2. <a name='AllOnlineToolsinOneBox'></a>All Online Tools in “One Box”

All Online Tools in “One Box” en [10015.io](https://10015.io/) ofrece una caja de herramientas basadas en web o como extensión ([Chrome](https://chrome.google.com/webstore/detail/online-tools-by-10015io/afbphoagjpegnkpeiliacmiiggojdabo),[Firefox](https://addons.mozilla.org/en-US/firefox/addon/online-tools-by-10015-io/)) con utilidades clasificadas en apartados: herramientas de texto , utilidades para redimensionar imagenes, obtener su color, formateadores de código JS, HTML y muchas más. Genial.


##  3. <a name='CSSViewer'></a>CSSViewer

[CSSViewer](https://chrome.google.com/webstore/detail/cssviewer/ggfgijbpiheegefliciemofobhmofgce?hl=es) es un visor de las propiedades CSS simplemente pasando el cursor por encima de cada elemento.

![](img/02.gif)