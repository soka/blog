---
title: Trello - Crear tarjetas de una Web
subtitle: La pizarra digital inspirada en kanban
date: 2019-07-10
draft: false
description: Crea tarjetas de cualquier sitio Web.
tags:
  [
    "Trello",
    "Talleres",
    "Cursos",
    "Aplicaciones",
    "Trello - Introducción",
    "Trello - Tarjetas",
    "Trello - Navegador",
    "Kanban",
    "Metodologías",
    "Gestión de Proyectos",
    "Herramientas",
  ]
---

![](img/logo-trello.png)

![](img/01.PNG)

En acción:

![](img/02.gif)

## Enlaces externos

- ["https://sitios.xyz/trello"](https://sitios.xyz/trello).
