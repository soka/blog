---
title: Vue.js - Introducción
subtitle: Introducción general
date: 2019-03-18
draft: false
description: The Progressive JavaScript Framework
tags: ["Vue", "JS", "JavaScript", "Programación", "Vue.js"]
---

![](images/logo.png)

**Vue.js es un framework JavaScript progresivo de código abierto** para crear interfaces de usuario, podemos usarlo añadiendo una simple la referencia de la libreria a un proyecto ya existente o podemos hacer cosas más complejas a medida que avanzamos añadiendo lógica de negocio y componentes. **VueJS** tiene un diseño **modular**, está **organizado en diferentes librerías** independientes que permiten ir añadiendo funcionalidad en el momento que las vayamos necesitando, se puede **adoptar de forma incremental** y puede convivir perfectamente con otras librerías y proyectos.

Para añadir la librería usamos una línea como está, la librería core o principal está ideada para diseñar la parte visual:

```JS
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.9/dist/vue.js"></script>
```

![](images/vTZzA.png)

El [**data binding**](https://v1.vuejs.org/guide/syntax.html) (bidireccional) y la **reactividad** son dos de sus características más destacadas, **Vue** se basa en el patrón **[MVVM](https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93modelo_de_vista) (Modelo - Vista, Vista - Modelo)** para crear interfaces (el modelo es el código JS y la vista está representada por el código HTML), cuando los datos del modelo cambian **Vue** se encarga de actualizarlos en toda nuestra aplicación de forma dinámica, a la inversa cuando recibe un evento del interfaz (por ejemplo al hacer clic en un botón) podemos ejecutar código del modelo JS, esto facilita la reusabilidad del código y la abstracción en capas de la lógica y el UI separados.

En **VueJS** se puede trabajar con **componentes**, los componentes son ficheros con extensión .vue que contienen todo el HTML, CSS y JS necesario para el componente, por ejemplo un formulario para introducir datos (se puede empaquetar con Webpack haciendo _"bundling"_), los componentes Vue constan de tres secciones:

1. `<template></template>` con el código HTML.
2. `<script></script>` con el código JS.
3. `<style></style>` el CSS.

Vue hace uso de lo que se llama un ‘DOM Virtual’. Un DOM virtual es una representación virtual del árbol [DOM](https://es.wikipedia.org/wiki/Document_Object_Model).

# Ecosistema VueJS

![](images/1366_2000.png)

El core principal de **VueJS** está formado por una librería encargada de renderizar vistas en el navegador.

![](images/ecosistema.PNG)

[**vue-cli**](https://github.com/vuejs/vue-cli) es un paquete npm creado sobre NodeJS y proporciona una línea de comandos CLI permite crear un proyecto con un boilerplate (o plantilla base) configurado a nuestro gusto de una manera fácil y sencilla.

[**vue-router**](https://github.com/vuejs/vue-router) está ideado para definir la navegabilidad entre componentes asociandolos a diferentes URLs.

**Vuex** es una biblioteca de gestión de estado para la construcción de aplicaciones complejas a gran escala Vue.js

# Ejemplo básico - text interpolation

Este es el ejemplo más sencillo obtenido de la guía oficial (["Getting Started"](https://vuejs.org/v2/guide/#Getting-Started) y se puede consultar en ["JSFiddle Hello World example"](https://jsfiddle.net/chrisvfritz/50wL7mdz/)).

```
<!DOCTYPE html>
<html>
<head>
  <title>My first Vue app</title>
  <script src="https://cdn.jsdelivr.net/npm/vue"></script>
</head>
<body>
  <div id="app">
    {{ message }}
  </div>

  <script>
    var app = new Vue({
      el: '#app',
      data: {
        message: 'Hello Vue!'
      }
    })
  </script>
</body>
</html>
```

[Fuente](https://gitlab.com/soka/vuejs/blob/master/src/intro/01-index-html/index.html).

Para probar la **reactividad** de Vue que enlaza los datos y el DOM podemos hacer una comprobación muy simple, desde la consola del inspector de Chrome por ejemplo modificamos el contenido del mensaje `app.message = "Probando"`, automáticamente cambia el contenido del mensaje visible en el navegador por el nuevo texto.

Usamos la sintaxis de plantillas `{{}}` de VueJS para definir una variable (sintaxis [Mustache](<https://es.wikipedia.org/wiki/Mustache_(motor_de_plantillas)>) ya que recuerda a un bigote inclinado 90 grados), esta es la forma más básica de [**data-binding**](https://v1.vuejs.org/guide/syntax.html).

```JS
<div id="app">
  {{ message }}
</div>
```

El texto y las llaves `{{ message }}` serán reemplazados por nuestro mensaje "Hola Vue!". Lo primero es crear una nueva instancia con la llamada `var app = new Vue( )` pasando como parámetro un objeto JS donde definimos la propiedad `el` para identificar con un selector CSS el elemento al que aplicaremos la instancia que estamos creando.

```JS
var app = new Vue({
  el: "#app",
  data: {
    message: "Hola Vue!"
  }
});
```

# Ejercicios

Además del [ejercicio de arriba](https://gitlab.com/soka/vuejs/blob/master/src/intro/01-index-html/index.html) he preparado [otro ejemplo básico](https://gitlab.com/soka/vuejs/tree/master/src/intro/02-basic-example) con el código JS en otro fichero "index.js" referenciado desde "index.html".

# Depuración con Vue.js devtools para Chrome

[Vue.js devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd) es una extensión para Chrome, para que funcione debemos usar la versión de desarrollo de Vue.js:

```JS
<!-- development version, includes helpful console warnings -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
```

Una vez instalada la extensión es accesible desde el inspector de código:

![](images/vue-devtools.PNG)

# Data binding de un atributo con la directiva v-bind

- vuejs / src / intro / [03-binding-attributes](https://gitlab.com/soka/vuejs/tree/master/src/intro/03-binding-attributes).

No solo podemos podemos hacer data binding con un template (_text interpolation_), también podemos sincronizar atributos de elementos HTML como sigue:

```JS
<div id="app-2">
  <span v-bind:title="message">
    Pasa el ratón por encima unos segundos para ver la fecha y hora de carga
    de la página.
  </span>
</div>
```

```JS
var app2 = new Vue({
  el: "#app-2",
  data: {
    message: "You loaded this page on " + new Date().toLocaleString()
  }
});
```

El atributo `v-bind` es una **directiva** (usa el prefijo `v-` para saber que proviene de Vue).

# Enlaces

- Web oficial [vuejs.org](https://vuejs.org/).
- Repositorio GitHub [github.com/vuejs](https://github.com/vuejs).

**Cursos y tutoriales**:

- Guía oficial Vue.js con una introducción muy recomendable para empezar [enlace](https://vuejs.org/v2/guide/index.html).
- ["Aprende Vue2 y Firebase paso a paso"](https://wmedia.teachable.com/courses/enrolled/140226): **Curso gratuito muy recomendable** para empezar.
- Una serie de screencast tutoriales [Scrimba / Vue.js documentation](https://scrimba.com/playlist/pXKqta).

**General**:

- ["Qué es lo que me gusta de Vue.js"](https://carlosazaustre.es/que-es-lo-que-me-gusta-de-vue-js/).
- Wikipedia [Vue.js](https://en.wikipedia.org/wiki/Vue.js).
- ["Primeros pasos en Vue"](https://carlosazaustre.es/primeros-pasos-en-vue/).
- ["Introducción a Vue.js"](https://www.adictosaltrabajo.com/2017/04/27/introduccion-a-vue-js/).
- ["Conociendo Vuejs 2.0"](https://bites.futurespace.es/2017/06/09/conociendo-vuejs-2-0/).
- ["Por qué elegir VueJS: 5 razones para considerarlo nuestro próximo framework de referencia"](https://www.genbeta.com/desarrollo/por-que-elegir-vuejs-5-razones-para-considerarlo-nuestro-proximo-framework-de-referencia).
- ["VueJS: Conceptos avanzados de vue-router"](https://elabismodenull.wordpress.com/2017/05/24/vuejs-conceptos-avanzados-de-vue-router/).
- ["Vue.js Books You Have to Read"](https://madewithvuejs.com/blog/vue-js-books-you-have-to-read).
- Adictos al trabajo ["Introducción a Vue.js"](https://www.adictosaltrabajo.com/2017/04/27/introduccion-a-vue-js/).
