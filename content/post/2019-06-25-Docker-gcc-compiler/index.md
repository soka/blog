---
title: Entorno compilación GNU gcc con Docker
subtitle: Desarrollo aplicaciones en C/C++
date: 2019-06-25
draft: false
description: GNU/gcc
tags: ["Docker", "GCC", "C", "Programación", "Windows", "Desarrollo"]
---

![](img/gccegg-65.png)

Descargo la [imagen Docker gcc](https://hub.docker.com/_/gcc/) (GNU Compiler Collection) usando [`docker pull`](https://docs.docker.com/engine/reference/commandline/pull/):

```ps
PS> docker pull gcc
```

Compruebo que tengo el repositorio:

```ps
PS> docker images
REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
gcc                        latest              72ca08f8e9cb        13 days ago         1.17GB
```

Pruebo a correr un nuevo contenedor y ejecutar un comando con [`docker run'](https://docs.docker.com/engine/reference/commandline/run/):

```ps
PS> docker run gcc ls
bin
boot
dev
etc
home
```

Podemos abrir una sesión interactiva:

```ps
PS> docker run -i -t gcc bash
root@1e64c8c5e897:/#
```

## Dockerfile

Creo un fichero `Dockerfile`, copia los archivos locales a `/usr/src/hello-world-app` y los compila con gcc:

```ps
FROM gcc:4.9
COPY . /usr/src/hello-world-app
WORKDIR /usr/src/hello-world-app
RUN gcc -o hello-world-app main.c
CMD ["./hello-world-app"]
```

Crea una imagen a partir de un `Dockerfile`, la opción `-t` (o `--tag`) añade un tag opcional, el último "." indica la ruta del `Dockerfile`.

```ps
PS> docker build -t hello-world-app .
```

```ps
PS> docker images
REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
hello-world-app            latest              96fa1b6994c6        43 seconds ago      1.37GB
```

Ejecuto la imagen con consola interactiva con la imagen recién creada con el `Dockerfile`, `-i` (o `--interactive`) mantiene la entrada [STDIN](https://es.wikipedia.org/wiki/Entrada_est%C3%A1ndar) abierta.

```ps
PS> docker run -it --rm --name hello-world-running-app hello-world-app
Hola Mundo!
```

Con `--name` asigno un nombre al contenedor ejecutado (sino lo hubiese definido se le asigna una cadena aleatoria). El último parámetro es el nombre de la imagen que he creado previamente con `docker build`.

## Compilación y ejecución del proyecto dentro de la imagen

Compilación con el nombre del `Dockerfile`:

```ps
PS> docker build -t hello-world-app .
Sending build context to Docker daemon  4.096kB
Step 1/5 : FROM gcc:4.9
 ---> 1b3de68a7ff8
Step 2/5 : COPY . /usr/src/myapp
 ---> Using cache
 ---> b2384734a907
Step 3/5 : WORKDIR /usr/src/myapp
 ---> Using cache
 ---> 2ac74f825562
Step 4/5 : RUN gcc -o myapp main.c
 ---> Using cache
 ---> 3e125aa61f63
Step 5/5 : CMD ["./myapp"]
 ---> Using cache
 ---> 43d866374bb3
Successfully built 43d866374bb3
Successfully tagged my-gcc-app:latest
SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.
```

Arrancamos el nuevo contenedor "hello-world-app":

```ps
PS> docker run -it --rm --name hello-world-running-app hello-world-app
```

## Usar un Makefile

Reemplazar en el `Dockerfile` esta línea:

```
RUN gcc -o myapp main.c
```

Por esta otra:

```
RUN make
```

Y ahora crea

```ps
PS> docker build -t hello-world-app .
PS> docker run -it --rm --name hello-world-running-app hello-world-app
```

## Borrar de imagenes y contenedores

Contenedores:

```ps
PS> docker ps -a
PS> docker rm --force 5352b6eca4fd
```

Primero hemos listado todos los contenedores y después los he ido borrando por su ID.

Imágenes:

```ps
PS> docker images -a
PS> docker rmi gcc
```

En algunos casos si un contenedor está usando un contenedor asociado debamos borrar antes el contenedor.

## Enlaces internos

- ["Docker"](https://soka.gitlab.io/blog/post/2019-04-23-docker-iniciacion/).
- ["Integración continua con GitLab, Docker y R"](https://soka.gitlab.io/blog/post/2019-04-13-docker-contenedor-r/).

## Enlaces externos

- ["frolvlad/alpine-gcc"](https://hub.docker.com/r/frolvlad/alpine-gcc/): The smallest Docker image with gcc and g++ (114MB).
- GitHub ["docker-library/gcc"](https://github.com/docker-library/gcc): Docker Official Image packaging for gcc https://gcc.gnu.org.
- ["docker build"](https://docs.docker.com/engine/reference/commandline/build/): Build an image from a Dockerfile.
- [“Command-Line Interfaces (CLIs)”](https://docs.docker.com/engine/reference/commandline/info/).
- ["Docker run reference"](https://docs.docker.com/engine/reference/run/).
- ["docker run"](https://docs.docker.com/engine/reference/commandline/run/).
- ["C build environment in a docker container"](https://ownyourbits.com/2017/06/20/c-build-environment-in-a-docker-container/).
