---
title: SWAPI - The Star Wars API (Parte 1)
subtitle: Accede al universo Star Wars
date: 2019-05-28
draft: false
description: Desarrollos de un cliente API REST JSON
tags: ["API", "JSON", "Programación", "CSharp"]
---

## ¡Bienvenido al universo Star Wars!

Programar no está reñido con disfrutar, plantear ejercicios como este hace que un tutorial sobre APIs RESTful no sea tan árido. ¿Quieres saber algo más sobre el planeta Tatooine? No hay problema, aquí tienes la forma.

![](img/03.PNG)

[**Star Wars API o "swapi"**](https://swapi.co/) implementa una API Web HTTP pública para consultar datos sobre las películas de Star Wars.

Voy a comenzar inmediatamente realizando mi primera llamada de prueba, se puede copiar y pegar la URL [http://swapi.co/api/planets/1/](http://swapi.co/api/planets/1/) en la barra de direcciones del navegador, usar un plugin para Chrome como [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) o usar alguna utilidad de línea de comandos como [cURL](https://curl.haxx.se/) como hago yo:

Consulto los datos de un planeta por su identificador:

```ps
PS C:\> curl -Uri http://swapi.co/api/planets/1/


StatusCode        : 200
StatusDescription : OK
Content           : {"name":"Tatooine","rotation_period":"23","orbital_period":"304","diameter":"10465","climate":"arid","gravity":"1
                    standard","terrain":"desert","surface_water":"1","population":"200000","residents":["h...
RawContent        : HTTP/1.1 200 OK
                    Transfer-Encoding: chunked
                    Connection: keep-alive
                    Vary: Accept, Cookie
                    X-Frame-Options: SAMEORIGIN
                    Allow: GET, HEAD, OPTIONS
                    Expect-CT: max-age=604800, report-uri="https://report...
Forms             : {}
Headers           : {[Transfer-Encoding, chunked], [Connection, keep-alive], [Vary, Accept, Cookie], [X-Frame-Options, SAMEORIGIN]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 821
```

La misma Web de SWAPI permite realizar consultas introduciendo la URL en un formulario:

![](img/01.PNG)

La **URL base** para construir las consultas es [https://swapi.co/api/](https://swapi.co/api/), la **API no requiere de autenticación previa**, todos los recursos que se pueden consultar tienen un [**esquema JSON**](https://jsonschema.net/), siguiendo el ejemplo de arriba este es el esquema de los planetas si llamamos a [https://swapi.co/api/planets/schema](https://swapi.co/api/planets/schema) devuelve como están estructurados los datos:

```json
{
  "properties": {
    "rotation_period": {
      "description": "The number of standard hours it takes for this planet to complete a single rotation on its axis.",
      "type": "string"
    },
    "population": {
      "description": "The average populationof sentient beings inhabiting this planet.",
      "type": "string"
    },
    "diameter": {
      "description": "The diameter of this planet in kilometers.",
      "type": "string"
    },
    "edited": {
      "description": "the ISO 8601 date format of the time that this resource was edited.",
      "format": "date-time",
      "type": "string"
    },
    "terrain": {
      "description": "the terrain of this planet. Comma-seperated if diverse.",
      "type": "string"
    },
    "name": { "description": "The name of this planet.", "type": "string" },
    "gravity": {
      "description": "A number denoting the gravity of this planet. Where 1 is normal.",
      "type": "string"
    },
    "created": {
      "description": "The ISO 8601 date format of the time that this resource was created.",
      "format": "date-time",
      "type": "string"
    },
    "climate": {
      "description": "The climate of this planet. Comma-seperated if diverse.",
      "type": "string"
    },
    "orbital_period": {
      "description": "The number of standard days it takes for this planet to complete a single orbit of its local star.",
      "type": "string"
    },
    "url": {
      "description": "The hypermedia URL of this resource.",
      "format": "uri",
      "type": "string"
    },
    "films": {
      "description": "An array of Film URL Resources that this planet has appeared in.",
      "type": "array"
    },
    "surface_water": {
      "description": "The percentage of the planet surface that is naturally occuring water or bodies of water.",
      "type": "string"
    },
    "residents": {
      "description": "An array of People URL Resources that live on this planet.",
      "type": "array"
    }
  },
  "$schema": "http://json-schema.org/draft-04/schema",
  "required": [
    "name",
    "rotation_period",
    "orbital_period",
    "diameter",
    "climate",
    "gravity",
    "terrain",
    "surface_water",
    "population",
    "residents",
    "films",
    "created",
    "edited",
    "url"
  ],
  "type": "object",
  "title": "Planet",
  "description": "A planet."
}
```

Para validar las estructuras JSON contra sus Schemas podemos usar esta Web [jsonschemavalidator](https://www.jsonschemavalidator.net/).

![](IMG/02.PNG)

También podemos filtrar resultados como sigue: [https://swapi.co/api/planets/?search=tatooine](https://swapi.co/api/planets/?search=tatooine).

Cabe destacar que los **resultados se devuelven paginados**, por ejemplo consultando la lista de personajes con [https://swapi.co/api/people/](https://swapi.co/api/people/) la cabecera JSON de respuesta ofrece facilidades para navegar entre los resultados:

```json
{
    "count": 87,
    "next": "https://swapi.co/api/people/?page=2",
    "previous": null,
    "results": [
        {
```

La URL base [https://swapi.co/api/](https://swapi.co/api/) provee información de todos los recursos disponibles:

```json
{
  "people": "https://swapi.co/api/people/",
  "planets": "https://swapi.co/api/planets/",
  "films": "https://swapi.co/api/films/",
  "species": "https://swapi.co/api/species/",
  "vehicles": "https://swapi.co/api/vehicles/",
  "starships": "https://swapi.co/api/starships/"
}
```

## Implementando un cliente básico en CSharp

### Usando HttpWebRequest/Response (System.Net)

[Enlace]() al código fuente del ejemplo.

La clase [HttpWebRequest](https://docs.microsoft.com/es-es/dotnet/api/system.net.httpwebrequest?view=netframework-4.8) está heredada de [WebRequest](https://docs.microsoft.com/es-es/dotnet/api/system.net.webrequest?view=netframework-4.8) (MS recomienda usar System.Net.Http.HttpClient en lugar de HttpWebRequest).

Debemos añadir a la cabecera el espacio de nombres para poder usar esta clase:

```chsarp
using System.Net;
```

El método [WebRequest.Create](https://docs.microsoft.com/es-es/dotnet/api/system.net.webrequest.create?view=netframework-4.8) se usa para instanciar la clase y inicializar un nuevo objeto, le pasamos el [URI](https://es.wikipedia.org/wiki/Identificador_de_recursos_uniforme) con el recurso a consultar.

```csharp
HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://swapi.co/api/planets/1/");
```

Leemos la respuesta guardada en una variable de tipo HttpWebResponse:

```chsarp
var response = (HttpWebResponse)request.GetResponse();
```

La palabra clave `var` indica al compilador que infiera el tipo de variable a partir de la asignación de la derecha (ver [Variables locales con asignación implícita de tipos](https://docs.microsoft.com/es-es/dotnet/csharp/programming-guide/classes-and-structs/implicitly-typed-local-variables)).

Lo primero es comprobar el código de estado HTTP devuelto en la cabecera de la llamada para comprobar si ha tenido éxito.

```csharp
response.StatusCode == HttpStatusCode.OK
```

La llamada `HttpWebResponse.GetResponse` retorna un flujo de datos de la clase [Stream](https://docs.microsoft.com/es-es/dotnet/api/system.io.stream?view=netframework-4.8) para leer el contenido de la respuesta.

Como el _stream_ que retorna tiene sintáxis JSON usaremos el paquete [Newtonsoft.Json](https://www.newtonsoft.com/json) para deserializarlo y convertirlo en un objeto .NET.

Instalamos el paquete:

```PS
dotnet add package Newtonsoft.Json
```

También se puede usar el paquete de VSCode [NuGet Package Manager](https://marketplace.visualstudio.com/items?itemName=jmrog.vscode-nuget-package-manager) para gestionar los paquetes con [NuGet](https://www.nuget.org/).

Para deserializar la cadena devuelta en JSON como objeto uso la clase [JsonConvert](https://www.newtonsoft.com/json/help/html/T_Newtonsoft_Json_JsonConvert.htm) y el método [DeserializeObject](https://www.newtonsoft.com/json/help/html/M_Newtonsoft_Json_JsonConvert_DeserializeObject_1.htm) que recibe la cadena como parámetro.

```csharp
var planetData = JsonConvert.DeserializeObject<Planet>(content);
```

El método devuelve un tipo [Object](https://docs.microsoft.com/en-us/dotnet/api/system.object?redirectedfrom=MSDN&view=netframework-4.8).

Planet es una clase parcial que he creado para ver como se mapea la cadena JSON a atributos de la clase.

```csharp
    class Planet {
        public string name;
        public string rotation_period;
    }
```

## Código fuente del ejemplo

- csharp / projects / net / api / [swapi-star-wars-api](https://gitlab.com/soka/csharp/tree/master/projects/net/api/swapi-star-wars-api).

## Enlaces internos

- ["Cliente acceso Webservice PrestaShop con C#"](https://soka.gitlab.io/blog/post/2019-03-05-prestashop-webservice-csharp-client/): Ejemplo de cliente en C# usando HttpWebRequest / HttpWebResponse.

## Enlaces externos

- ["SWAPI - The Star Wars API"](https://swapi.co/): Sitio Web principal.
- ["Documentation"](https://swapi.co/documentation): Documentación oficial SWAPI.
- ["HTTPie"](https://httpie.org/): Command line HTTP client with an intuitive UI, JSON support, syntax highlighting, wget-like downloads, plugins, and more.
- ["A Few Great Ways to Consume RESTful APIs in C#"](https://dzone.com/articles/a-few-great-ways-to-consume-restful-apis-in-c).
- Json.NET supports the JSON Schema standard via the [JsonSchema](https://www.newtonsoft.com/json/help/html/T_Newtonsoft_Json_Schema_JsonSchema.htm) and [JsonValidatingReader](https://www.newtonsoft.com/json/help/html/T_Newtonsoft_Json_JsonValidatingReader.htm) classes. It sits under the [Newtonsoft.Json.Schema namespace](https://www.newtonsoft.com/json/help/html/N_Newtonsoft_Json_Schema.htm).
- ["HttpWebRequest Class (System.Net) | Microsoft Docs"](https://docs.microsoft.com/es-es/dotnet/api/system.net.httpwebrequest?view=netframework-4.8).
- ["Chuck Norris Jokes Api"](https://api.chucknorris.io/): chucknorris.io is a free JSON API for hand curated Chuck Norris facts.
