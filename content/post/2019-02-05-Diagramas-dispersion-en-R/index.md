---
title: Diagramas de dispersión y modelo de regresión líneal en R
subtitle: Modelos estadísticos predictivos
date: 2019-02-05
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico","R - Diagramas","Diagramas"]
---

# Introducción

El [**diagrama de dispersión**](https://es.wikipedia.org/wiki/Diagrama_de_dispersi%C3%B3n) se representa como una nube de puntos o conjunto de datos colocados en unas [coordenadas Cartesianas](https://es.wikipedia.org/wiki/Coordenadas_cartesianas) bidimensionales (plano).

Definición de colección de pares de puntos:

```
(x1, y1),(x2, y2),(x3, y3), . . . ,(xn, yn),
```

Se usa a menudo en **modelos de regresión** para estimar la relación entre dos variables de forma visual, normalmente en el eje horizontal o X se mide el parámetro de control o variable independiente y en el eje vertical Y  representa la variable medida o dependiente.

# Diagrama de dispersión en R

Para ilustrarlo mejor vamos a usar un ejemplo, queremos averiguar si existe alguna relación entre los likes de un artículo y las ventas del mismo en un comercio en línea, sospechamos que a mayor número de likes más ventas se realizan.

 Likes		| Ventas		
------------|------------
221			| 27.75
235			| 50.40
284			| 10.45
351			| 81.29
365			| 191.92
385			| 292.52
394			| 323.60
341			| 103.77
325			| 133.91
384			| 232.59

En el eje vertical situamos las ventas y en el horizontal los likes de un producto en nuestro eCommerce, dado un número de likes queremos predecir sus ventas.

Primero definimos el conjunto de pares de datos datos en dos vectores:

```R
likes   = c(221,235,284,351,365,385,394,341,325,384)
ventas  = c(27.75,50.40,10.45,81.29,191.92,292.52,323.60,103.77,133.91,232.59)
```

Ahora llamamos a la función [**plot**](https://www.rdocumentation.org/packages/graphics/versions/3.5.2/topics/plot) para dibujar la gráfica pasandole pasandole las coordenadas, las etiquetas de los ejes y el título del gráfico:

```R
plot(likes, ventas,
     xlab = "Likes", # Etiqueta eje x
     ylab = "Ventas", # Etiqueta eje y
     #type = "l", # Unir mediante línea los puntos
     sub = "Me gusta artículo XXX por ventas",  # Subtitulo
     main="Gráfica: Diagrama de Dispersión") # Titulo principal del grafico
```

Resultado:

![](images/Rplot01.png)


# Utilidades y datos adicionales

```R
> head( likes ) # muestra los 6 primeros registros
[1] 221 235 284 351 365 385
```

```R
str(ventas) # ofrece un detalle de los datos
 num [1:10] 27.8 50.4 10.4 81.3 191.9 ..
```

La función **`summary()`** muestra la media, mediana, cuartiles, valor mínimo y valor máximo.

```R 
> summary(ventas) #descriptivos básicos para variables cuantitativas
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  10.45   58.12  118.84  144.82  222.42  323.60 
```


# La recta de regresión lineal

En estadística un modelo de [**regresión líneal**](https://es.wikipedia.org/wiki/Regresi%C3%B3n_lineal) es un **modelo matemático para aproximar la relación de dependencia entre dos variables, una dependiente de la otra**. Se trata de hallar la **recta que mejor "encaje" con el conjunto de puntos dados** y así poder predecir el valor de la variable dependiente Y respecto a un nuevo valor de la variable independiente X. Por **regresión** se entiende que parte de datos históricos.

![](images/formula_basica_regresion_lineal_01.PNG) 

La relación no tiene porqué ser líneal, tambien puede responder a una correlación exponencial por ejemplo, donde se añade o resta un porcentaje fijo por cada valor de X.

```R
> modelo_regresion = lm(ventas ~ likes) # Construyo una ecuación de regresión lineal
> abline(modelo_regresion, col = "red")
```

Resultado:

![](images/Rplot02.png)

Una vez obtenida esta línea, seremos capaces de hacer predicciones hipotéticas sobre cuál sería el valor de "Y" (precio venta) dado "X" (número de likes). 

```R
> summary( modelo_regresion ) # coeficientes del modelo de regresión

Call:
lm(formula = ventas ~ likes)

Residuals:
    Min      1Q  Median      3Q     Max 
-97.190 -46.689  -0.466  45.031  80.791 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)   
(Intercept) -346.621    113.469  -3.055  0.01571 * 
likes          1.496      0.340   4.400  0.00229 **
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 63.46 on 8 degrees of freedom
Multiple R-squared:  0.7076,	Adjusted R-squared:  0.6711 
F-statistic: 19.36 on 1 and 8 DF,  p-value: 0.002285
```

De donde deducimos que la ecuación de la recta que mejor se ajusta es:

```
y = 1.496016184 x - 346.6213166 
```

# Coeficiente de determinación (R cuadrado)

El coeficiente de determinación, se define como la proporción de la varianza total de la variable explicada por la regresión. El coeficiente de determinación, también llamado R cuadrado, refleja la bondad del ajuste de un modelo a la variable que pretender explicar.

![](images/r_cuadrado_rangos.png)

La correlación entre dos variables X e Y es perfecta positiva cuando exactamente en la medida que aumenta una de ellas aumenta la otra. Es perfecta negativa cuando exactamente en la medida que aumenta una variable disminuye la otra.

La finalidad de la correlación es examinar la dirección y la fuerza de la asociación entre dos variables cuantitativas. Así conoceremos la intensidad de la relación entre ellas y si, al aumentar el valor de una variable, aumenta o disminuye el valor de la otra variable.

El valor del índice de correlación varía en el intervalo [-1,1], indicando el signo el sentido de la relación:

Valor del Coeficiente de Pearson | Grado de Correlación entre las Variables
---------------------------------|------------------------------------------
r = 0     						 |  no existe relación lineal. Pero esto no necesariamente implica que las variables son independientes: pueden existir todavía relaciones no lineales entre las dos variables.
r = 1							 | existe una correlación positiva perfecta. El índice indica una dependencia total entre las dos variables denominada relación directa: cuando una de ellas aumenta, la otra también lo hace en proporción constante.
0 < r < 1						 | correlación positiva
r = 0							 | no existe relación lineal. Pero esto no necesariamente implica que las variables son independientes: pueden existir todavía relaciones no lineales entre las dos variables
-1 < r < 0						 | existe una correlación negativa
r = -1							 | existe una correlación negativa perfecta. El índice indica una dependencia total entre las dos variables llamada relación inversa: cuando una de ellas aumenta, la otra disminuye en proporción constante

**En el caso que nos ocupa el valor de R Cuadrado es 0.7076, parece indicar que existe una correlación positiva**.

# Uso para predicciones económicas

* [Pronóstico de Demanda haciendo uso de la información histórica de venta de un producto determinado durante los últimos 12 trimestres (3 años)](https://www.gestiondeoperaciones.net/proyeccion-de-demanda/como-utilizar-una-regresion-lineal-para-realizar-un-pronostico-de-demanda/).


# Ejercicios

* r / intro / 11-Graficos-de-dispersion / [diagrama-dispersion.R](https://gitlab.com/soka/r/blob/master/intro/11-Graficos-de-dispersion/diagrama-dispersion.R).
 
# Enlaces

## Enlaces externos

**Estadística y modelos en general**:

* wikipedia ["Diagrama de dispersión"](https://es.wikipedia.org/wiki/Diagrama_de_dispersi%C3%B3n).
* wikipedia ["Coordenadas cartesianas"](https://es.wikipedia.org/wiki/Coordenadas_cartesianas).
* wikipedia ["Análisis de la regresión"](https://es.wikipedia.org/wiki/An%C3%A1lisis_de_la_regresi%C3%B3n).
* ["Tipos de gráficos y diagramas para la visualización de datos"](https://www.ingeniovirtual.com/tipos-de-graficos-y-diagramas-para-la-visualizacion-de-datos/).
* ["Cálculo de regresiones lineales en línea"](http://www.wessa.net/esteq.wasp): This free online software (calculator) computes the multiple regression model based on the Ordinary Least Squares method.
* [xuru.org](http://www.xuru.org/rt/LR.asp): Herramientas de regresión lineal en línea. "This page allows performing linear regressions (linear least squares fittings). For the relation between two variables, it finds the linear function that best fits a given set of data point".
* ["Matemáticas finitas tema en-línea: regresión lineal y exponencial"](https://www.zweigmedia.com/MundoReal/calctopic1/regression.html): **PENDIENTE DE REVISAR**. función lineal de demanda. Suponga que su investigación de mercado muestra las siguientes estadísticas de venta para casas de varios precios durante el año pasado.
* ["Matemáticas finitas y cálculo aplicado resumen del tema: Funciones y aplicaciones: Parte 1 de 2"](https://www.zweigmedia.com/summaryHolder.php?lang=es&chap=FN&part=0): **PENDIENTE DE REVISAR**,


## Recursos R

* ["Diagrama de Dispersión y Regresión en R"](https://rpubs.com/MSiguenas/97848).
* ["Construir una gráfica de dispersión y su regresión lineal con R"](http://www.joserodriguez.info/bloc/construir-una-grafica-de-dispersion-y-su-regresion-lineal-con-r/).
* ["lm function | R Documentation"](https://www.rdocumentation.org/packages/stats/versions/3.5.2/topics/lm): lm is used to fit linear models. It can be used to carry out regression, single stratum analysis of variance and analysis of covariance (although aov may provide a more convenient interface for these)..
* ["summary function | R Documentation"](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/summary): summary is a generic function used to produce result summaries of the results of various model fitting functions. The function invokes particular methods which depend on the class of the first argument..
* ["plot function | R Documentation"](https://www.rdocumentation.org/packages/graphics/versions/3.5.2/topics/plot).