---
title: Introducción a PowerShell
subtitle: Windows Server 2012
date: 2019-11-08
draft: false
description: Recursos Windows Server
tags: ["Windows","Sistemas","Windows Server","Windows Server 2012","PowerShell"]
---

Si realizamos la instalación Core (el modo de instalación por defecto) necesitamos con una consola de comandos potentes, esa consola es PowerShell (en la versión 2012 viene PS 3.0 y la 2012 R2 con PS 4.0). [En mi primer post](https://soka.gitlab.io/blog/post/2019-11-05-windows-server-1/) sobre la instalación de Windows Server 2012 ya usaba algunos comandos de PS.

Lo primero es actualizar toda la información sobre los comandos con `Update-Help`. Todos los comandos siguen la misma norma, un verbo con la acción, ujn guion y un nombre. Para consultar todos los comandos disponibles uso el cmdlet `Get-Command | more`, uso un _pipe_ para pasar el resultado al comando `more` y paginar los resultados.

![](img/01.png)

Algunos comandos son alias de otros, también existen funciones, nos dice a que módulo pertenecen, etc. Cuando añadamos roles irán apareciendo más comandos. Si queremos obtener ayuda sobre un comando específico usamos por ejemplo `Get-Help Get-Command`. Si quisiéramos modificar la configuración IP usamos `New-NetIpAdress`. Si pongo el guion y después uso el tabulador se autocompletan las opciones posibles.

## Configuración de la red

![](img/02.png)

Comandos PS:

`Get-NetAdapter` para ver las tarjetas de red que tenemos, `Get-NetIpAddress` para ver la configuración de red (`Get-NetIpAddress | Format-Table` para verlo "bonito"), se muestran también interfaces de red virtuales. 

Para establecer la configuración de red (en vez `-InterfaceAlias` puedo usar `-InterfaceIndex` obtenido con el comando `Get-NetIpAddress`):

```bash
New-NetIPAddress –IPAddress 192.168.0.99 -InterfaceAlias "Ethernet" -DefaultGateway 192.168.0.1 –AddressFamily IPv4 –PrefixLength 24

Set-DnsClientServerAddress -InterfaceAlias "Ethernet" -ServerAddresses 192.168.0.100
```

## Enlaces

- Mi Web sobre PS [https://soka.gitlab.io/PSFabrik/](https://soka.gitlab.io/PSFabrik/).
- ["IP Addressing and Subnetting for New Users - Cisco"](https://www.cisco.com/c/es_mx/support/docs/ip/routing-information-protocol-rip/13788-3.html).





