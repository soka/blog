---
title: Cómo tomar notas con el método Cornell
subtitle: Un sistema para tomar apuntes
date: 2021-04-01
draft: false
description: 
tags: ["Cornell","Apuntes","Notas","Productividad","GTD"]
---

<!-- vscode-markdown-toc -->
* 1. [INTRODUCCIÓN](#INTRODUCCIN)
* 2. [MANUAL DE USO](#MANUALDEUSO)
* 3. [ENLACES EXTERNOS](#ENLACESEXTERNOS)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='INTRODUCCIN'></a>INTRODUCCIÓN

En esta entrada sólo pretendo recoger de forma muy resumida en qué consiste este **método para tomar notas**, creo que me servirá de utilidad más adelante para llevar conmigo un bloc  donde escribir a mano ideas y resúmenes de cualquier charla o taller de los que suelo asistir.

Este sistema de toma de apuntes fue ideado en los años 50 por un profesor de universidad. Podemos usar este método sobre un cuaderno de papel de tamaño A4 o A5 para recoger información que luego trasladaremos a alguna herramienta digital que nos sirva como bandeja de entrada de la metodología [GTD](https://es.wikipedia.org/wiki/Getting_Things_Done).

El ["**Método Cornell**"](https://es.wikipedia.org/wiki/Notas_Cornell) propone una plantilla en una hoja dividida en varias secciones, empieza con dos columnas, la derecha debe ser el doble de ancha que la izquierda para tomar **notas / apuntes** de la lección que traten de responder a una serie de preguntas promoviendo la reflexión, en la columna izquierda apuntamos esas **preguntas o ideas clave**. En la parte inferior se deja un espacio para realizar un **resumen** corto con nuestras propias palabras.

La imagen inferior de ejemplo está cogida del siguiente [enlace](https://canasto.es/blog/notas-cornell).

![](img/01.jpg)

##  2. <a name='MANUALDEUSO'></a>MANUAL DE USO

En la parte superior de la hoja dejamos un espacio para la fecha y un título descriptivo sobre el contenido.

### Notas

Donde se toman durante la lección los apuntes que consideremos importantes. Debemos usar frases cortas y abreviar lo más posible, podemos acompañar el texto de ilustraciones gráficas, ordenar los puntos en listas, etc.

### Preguntas / palabras clave

Esta sección se rellena inmediatamente o antes de que transcurran 24 horas desde la lección. 

##  3. <a name='ENLACESEXTERNOS'></a>ENLACES EXTERNOS

- [https://canasto.es/blog/notas-cornell](https://canasto.es/blog/notas-cornell)
- [https://es.wikipedia.org/wiki/Notas_Cornell](https://es.wikipedia.org/wiki/Notas_Cornell).
- [http://www.organizadoresgraficos.com/grafico/cornell.php](http://www.organizadoresgraficos.com/grafico/cornell.php).
- [https://blog.mad.es/tomar-apuntes-de-forma-eficiente-metodo-cornell/](https://blog.mad.es/tomar-apuntes-de-forma-eficiente-metodo-cornell/).