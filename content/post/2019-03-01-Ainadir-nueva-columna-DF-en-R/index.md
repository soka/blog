---
title: Añadir una nueva columna cbind()
subtitle: Objetos en R - Data.frames
date: 2019-03-01
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico","R - Data.Frame"]
---

Un Data.frame es muy parecido a una matriz de filas y columnas de dos dimensiones pero cada columna puede almacenar diferentes tipos de datos. 

# Creando un DF

En este caso creamos un DF de 3 columnas y dos filas o registros:

```R
dfPersonas <- data.frame("Id" = 1:2, "Age" = c(21,15), "Name" = c("John","Dora"))
```

# Inspeccionando un DF

```R
> table(dfPersonas$Age) #

15 21 
 1  1
```

Estructura del DF:

```R
> str(dfPersonas) #  2 Observaciones y 3 variables (columnas)
'data.frame':	2 obs. of  3 variables:
 $ Id  : int  1 2
 $ Age : num  21 15
 $ Name: Factor w/ 2 levels "Dora","John": 2 1
```

Clase objeto:

```R
> class(dfPersonas) # "data.frame"
[1] "data.frame"
```

Resumen:

```R
> summary(dfPersonas) # Resumen mínima, media, ...
       Id            Age         Name  
 Min.   :1.00   Min.   :15.0   Dora:1  
 1st Qu.:1.25   1st Qu.:16.5   John:1  
 Median :1.50   Median :18.0           
 Mean   :1.50   Mean   :18.0           
 3rd Qu.:1.75   3rd Qu.:19.5           
 Max.   :2.00   Max.   :21.0 
```

Dimensiones:

```R
> dim(dfPersonas) # Dimensiones filas x columnas
[1] 2 3
```

# Añadiendo una nueva columna

Vamos a añadir un valor booleano como columna nueva, los valores estan condicionados al contenido de otro columna, si la edad es mayor que 20 por ejemplo almacena TRUE para ese registro:

```R
> dfPersonas$AgeGt20 <- dfPersonas$Age > 20
> dfPersonas
  Id Age Name AgeGt20
1  1  21 John    TRUE
2  2  15 Dora   FALSE
```

Podemos añadir una nueva columna con la función `cbind()` como en una matriz:

```R
> dfPersonas <- cbind(dfPersonas,Surname=c("Doe","La exploradora"))
> dfPersonas
  Id Age Name AgeGt20        Surname
1  1  21 John    TRUE            Doe
2  2  15 Dora   FALSE La exploradora
```

# Índices como en matrices y nombres como en listas...

Se pueden utilizar índices numéricos para seleccionar filas y columnas de la misma forma que en las matrices o podemos seleccionar una columna por su nombre.

```R
> dfPersonas[,2] # 2. columna "Age"
[1] 21 15

> dfPersonas[1,] # 1. Registro o fila
  Id Age Name AgeGt20
1  1  21 John    TRUE

> dfPersonas$Name # Acceso por nombre columna
[1] John Dora
Levels: Dora John
```

# División de un DF (submuestra)

```R
> dfSubmuestra <- dfPersonas[dfPersonas$Name == 'John',]
> dfSubmuestra
  Id Age Name AgeGt20 Surname
1  1  21 John    TRUE     Doe
```

# Mezclar dos DFs por un campo común

Supongamos que tenemos otro DF con más datos de los sujetos y ambos DF comparten un campo común ("Name"):

```R
> df2 <- data.frame("Name" = c("John","Dora"),"Colesterol"=c(12.34,34))
> dfNuevo <- merge(dfPersonas,df2)
> dfNuevo
  Name Id Age AgeGt20        Surname Colesterol
1 Dora  2  15   FALSE La exploradora      34.00
2 John  1  21    TRUE            Doe      12.34
```

# Ejercicios

*  r / intro / 16-Columna-nueva-DF / [DF-columna-nueva.R](https://gitlab.com/soka/r/blob/master/intro/16-Columna-nueva-DF/DF-columna-nueva.R).

# Enlaces

**Enlaces externos:**

* ["Objetos en R: Data Frames y Listas"](http://www.dma.ulpgc.es/profesores/personal/stat/cursoR4ULPGC/6g-Data_frames-Listas.html).

