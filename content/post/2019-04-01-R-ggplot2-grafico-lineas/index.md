---
title: geom_line() - Gráficos de líneas
subtitle: Introducción a ggplot2
date: 2019-04-01
draft: false
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "Programación",
    "R - Básico",
    "R - Gráficos",
    "R - Diagramas",
    "R - ggplot2",
    "Diagramas",
  ]
---

![](images/ggplot_hex.jpg)

Vamos a usar un _dataset_ que viene con R, ["ChickWeight"](https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/ChickWeight.html) relaciona el peso y la edad de los pollitos. Contiene 578 filas y 4 columnas, peso, tiempo, pollito y dieta.

```R
> head(ChickWeight)
  weight Time Chick Diet
1     42    0     1    1
2     51    2     1    1
3     59    4     1    1
4     64    6     1    1
5     76    8     1    1
6     93   10     1    1
```

Lo primero como siempre es cargar la librería **ggplot2** con `library("ggplot2")`.

# Ejemplo básico

![](images/sintax.png)

[[Fuente](https://www.sharpsightlabs.com/blog/geom_line/)]

Vamos a dibujar el diagrama para visualizar la evolución del peso (eje Y o “ordenada”) en el tiempo (coordenada cartesiana X o abscisa) de un sujeto del _dataset_, la función `subset()` permite seleccionar un subconjunto de la muestra basado en una condición (ver ["Seleccionar datos con [] y subset"](https://soka.gitlab.io/blog/post/2019-03-15-r-seleecionar-datos-subset/)), en el siguiente ejemplo le pasamos a la función `subset` el _data.frame_ y la condición lógica basada en una columna del _data.frame_ "Chick" para obtener los datos de un determinado pollito.

![](images/aes.png)

[[Fuente](https://www.sharpsightlabs.com/blog/geom_line/)]

A continuación llamamos a `gglot` proporcionando el _data.frame_ y definimos la estética con `aes` asignando las variables a las coordenadas, el resultado se suma a `geom_line` y ya tenemos nuestro diagrama.

```R
chickone <- subset(ChickWeight, ChickWeight$Chick == 1)
ggplot(data=chickone,aes(x=Time,y=weight)) +
  geom_line()
```

![](images/geom-line-01.PNG)

# Una línea por sujeto

Continuando con el ejemplo anterior queremos dibujar una línea para cada pollito, usamos el parámetro group indicandole el criterio de agrupación (columna 'Chick') y definimos los colores basándonos en otra columna 'Diet'. También definimos un título para el diagrama.

```R
ggplot(data=ChickWeight,aes(x=Time,y=weight,group = Chick,colour=Diet)) +
  geom_line() +
  ggtitle("Crecimiento de pollitos por dieta")
```

![](images/geom-line-02.PNG)

# Ejemplos

Mezclando lineas y puntos, simplemente sumamos a `geom_line` `geom_point`:

```R
df <- data.frame(variable=c("A", "B", "C"),
                 valor=c(4.2, 10, 29.5))
head(df)

ggplot(data=df, aes(x=variable, y=valor, group=1)) +
  geom_line()+
  geom_point()
```

![](images/lineas-y-puntos.PNG)

Cambiando estilo de línea:

```R
ggplot(data=df, aes(x=variable, y=valor, group=1)) +
  geom_line(linetype = "dashed")+
  geom_point()
```

![](images/punteada.PNG)

Color de línea rojo:

```R
ggplot(data=df, aes(x=variable, y=valor, group=1)) +
 geom_line(color="red")+
 geom_point()
```

![](images/linea-roja.PNG)

# Ejercicios

- r / intro / 31-ggplot2-geom-line / [geom-line.R](https://gitlab.com/soka/r/blob/master/intro/31-ggplot2-geom-line/geom-line.R).

# Enlaces internos

- ["Gráficos avanzados en R - Introducción a ggplot2"](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-avanzados-ggplot2-1/).

# Enlaces externos

- ["ggplot | geom_line"](http://ggplot.yhathq.com/docs/geom_line.html).
- ["geom_line| Examples | Plotly"](https://plot.ly/ggplot2/geom_line/).
- ["ggplot2 line plot : Quick start guide - R software and data visualization ..."](http://www.sthda.com/english/wiki/ggplot2-line-plot-quick-start-guide-r-software-and-data-visualization).
- ["geom_line function | R Documentation"](https://www.rdocumentation.org/packages/ggplot2/versions/0.9.0/topics/geom_line).
- ["How to use geom_line in ggplot2 - Sharp Sight"](https://www.sharpsightlabs.com/blog/geom_line/).

- ["Quick-R: Subsetting Data"](https://www.statmethods.net/management/subset.html).
