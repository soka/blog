---
title: Trello - crear un equipo
subtitle: La pizarra digital inspirada en kanban
date: 2019-07-03
draft: false
description: Equipos en Trello
tags:
  [
    "Trello",
    "Talleres",
    "Cursos",
    "Aplicaciones",
    "Trello - Introducción",
    "Kanban",
    "Metodologías",
    "Gestión de Proyectos",
    "Herramientas",
  ]
---

![](img/logo-trello.png)

**¿Que vamos a aprender en esta clase?**

- Cambiar las notificaciones por correo electrónico.
- Crear un equipo en Trello.
- Completar el perfil del equipo.
- Añadir miembros al equipo.

## Cambiar la frecuencia de notificación por correo electrónico

Esta clase la vamos a dedicar a crear el grupo de trabajo en **Trello**. Antes de comenzar voy a cambiar la configuración de las notificaciones que **Trello** me envía por correo electrónico. Volvemos a las opciones de configuración de nuestro usuario y vamos a la pestaña de configuración:

![](img/01.PNG)

Seleccionamos la opción "Cambiar la frecuencia de los correos electrónicos de notificación...":

![](img/02.PNG)

A mi me gusta recibir las modificaciones en cualquier proyecto al instante.

## Crear nuestro grupo de trabajo

Justo al lado del perfil hacemos clic sobre el icono "+" y seleccionamos la opción "Crear equipo..."

![](img/03.PNG)

![](img/04.PNG)

Aprovechamos para cambiar el logotipo:

![](img/05.PNG)

## Añadir nuevos miembros al equipo

Podemos añadir usuarios existentes en **Trello** (nombre que figura en el perfil creado por **Trello**) o bien mandarles vía correo electrónico una invitación:

![](img/06.PNG)

Por eso es tan importante el avatar ya que nos ayudará a identificar la persona que buscamos.

La persona que crea el equipo será el administrador del mismo pero demos elevar los privilegios de otros usuarios para que sean administradores.

![](img/07.PNG)

Podemos distinguir que usuarios son administradores porque en la imagen del perfil se superpone un pequeño icono de doble flecha azul hacia arriba.

El usuario recién añadido recibirá una notificación informando de que ha sido incluido en el nuevo equipo (icono de campana junto a perfil).

![](img/08.PNG)

El usuario también recibe la misma notificación por correo electrónico.

## Configuración del equipo

Si vemos junto al logo del equipo hay un candado que nos dice que es privado, solamente los miembros del equipo podrán ver los tableros que se creen en **Trello**.

![](img/09.PNG)

Podemos ajustar la visibilidad del equipo para hacerlo público:

![](img/10.PNG)

Al igual que nuestro perfil personal podemos modificar los datos del grupo y por ejemplo añadir un sitio Web:

![](img/11.PNG)

Si nos vamos a la pestaña "Tableros" logicamente aún no tenemos ningún _board_ creado:

![](img/12.PNG)
