---
title: Genesis Blocks 
subtitle: colección de bloques para WordPress
date: 2020-11-14
draft: false
description: Plugin gratuito de StudioPress para diseñar contenidos
tags: ["Wordpress","WP - Curso","WP","WP - Plugins","WP - Editor","WP - Bloques","WP - Diseño","WP - Constructor de páginas"]
---

![](img/13.png)

[**Genesis Blocks**](https://wordpress.org/plugins/genesis-blocks/) es un plugin para **WordPress** que ofrece una colección de diseños de página (4 diseños completos de página y 8 secciones) y 14 nuevos bloques para el editor Gutenberg, además se integra con [Mailchimp](https://mailchimp.com/) para funcionar como servicio de noticias por correo electrónico.

El plugin está valorado bastante bien (4 de 5 estrellas), es compatible con las últimas versiones de WP (5.3 o superior), cuenta con 6.000 instalaciones activas y está actualizado recientemente.

## Bloques

Sólo voy a mencionar un par de ellos, se puede encontrar la documentación completa de todos los bloques disponibles en este [enlace](https://developer.wpengine.com/genesis-blocks/).

![](img/08.png)

### Columnas avanzadas

El [bloque de columnas](https://developer.wpengine.com/genesis-blocks/advanced-columns-block/) es flexible y avanzado para crear atractivos diseños. Primero seleccionamos el número de columnas: 

![](img/03.png)

A continuación la relación de aspecto:

![](img/04.png)

Ajustes del bloque:

- Espacio entre columnas.
- _Margin and Padding_.
- Colores de texto y fondo.
- Imagen de fondo.
- CSS Avanzado.

Ejemplo con 3 columnas, con imagen de fondo y algunos ajustes de _padding_, queda bastante molón:

![](img/05.png)

Este [vídeo](https://share.getcloudapp.com/yAuPxRoN) resume bien que permite hacer.

### Iconos para compartir en RRSS

El [bloque para compartir](https://developer.wpengine.com/genesis-blocks/sharing-block/) es un poco limitado, ofrece las siguientes RRSS Twitter, FB, Pinterest, LinkedIn, Reddit y correo electrónico, se puede realizar una configuración básica del diseño.

![](img/06.png)

### Rejilla de entradas o páginas

La [rejilla de entradas o páginas](https://developer.wpengine.com/genesis-blocks/post-and-page-grid-block) permite mostrar X entradas resumidas ordenadas de forma cronológica o una serie de páginas determinadas, las entradas se pueden filtrar por categoría. Podemos ajustar el número de elementos y columnas a mostrar. La imagen destacada de la entrada se puede mostrar o no, lo mismo con el resto de atributos como el autor o la fecha, etc.

![](img/07.png)

### Bloque de contenedores

El [bloque de contenedores](https://developer.wpengine.com/genesis-blocks/container-block/) permite agrupar varios contenedores dentro de un contenedor "padre" para crear diferentes estilos de diseño.

### Botones

[Botones](https://developer.wpengine.com/genesis-blocks/button-block/) personalizables cuadradados o con bordes redondeados. **No ofrece efectos dinámicos avanzados ni es especialmente espectacular.

![](img/09.png)

### Notificaciones visuales

El bloque de [notificaciones](https://developer.wpengine.com/genesis-blocks/notice-block/) permite mostrar un aviso o mensaje destacado que se puede cerrar.

![](img/10.png)

## Diseños

Cuando creamos una página se muestra un nuevo elemento de menú en la parte superior con las nuevas opciones.

![](img/01.png)


El selector de diseño está divido en pestañas para secciones, diseños de página y colecciones.

![](img/02.png)

### Secciones

Las secciones son pequeños grupos de bloques prediseñados que se pueden añadir a una página o entrada junto con otros bloques.

![](img/11.png)

### Diseños

El plugin ofrece diseños para páginas completas.

![](img/12.png)

## Enlaces externos

- [https://wordpress.org/plugins/genesis-blocks/](https://wordpress.org/plugins/genesis-blocks/).
- [https://www.studiopress.com/genesis-blocks/](https://www.studiopress.com/genesis-blocks/).
- [https://developer.wpengine.com/genesis-blocks/](https://developer.wpengine.com/genesis-blocks/)
- [https://es.wordpress.org/plugins/tags/genesis/](https://es.wordpress.org/plugins/tags/genesis/).
- [https://es.wordpress.org/plugins/browse/popular/](https://es.wordpress.org/plugins/browse/popular/)

Las ilustraciones usadas de ejemplo están tomadas de [https://www.pinterest.es/pin/311874342946742868/](https://www.pinterest.es/pin/311874342946742868/).