---
title: Montar un servidor local para videoconferencias con JITSI y Docker
subtitle: 
date: 2020-03-17
draft: false
description: 
tags: ["JITSI","Docker","Debian","Aplicaciones","Comunicaciones"]
---

Estas pruebas están realizadas sobre Debian.

- Clonar el repositorio GitHub:

`git clone https://github.com/jitsi/docker-jitsi-meet && cd docker-jitsi-meet`

- Crear un fichero de configuración: `cp env.example .env`
- Crear los directorios de configuración necesarios: `mkdir -p ~/.jitsi-meet-cfg/{web/letsencrypt,transcripts,prosody,jicofo,jvb}`
- Ejecutar: `docker-compose up -d`

Ya debería ser accesible en [https://localhost:8443](https://localhost:8443).

Para permitir la gestión de documentos con Etherpad usar `docker-compose -f docker-compose.yml -f etherpad.yml up`.

Como podemos saber los contenedores que tenemos en ejecución `docker ps`.

```
root@diablo:/home/popu/Documentos/GitLab/blog/docker-jitsi-meet# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                                              NAMES
61ea389d4c11        jitsi/jicofo        "/init"             5 hours ago         Up 5 hours                                                             docker-jitsi-meet_jicofo_1
ff90a44528e2        jitsi/jvb           "/init"             5 hours ago         Up 5 hours          0.0.0.0:4443->4443/tcp, 0.0.0.0:10000->10000/udp   docker-jitsi-meet_jvb_1
7d8bb79c557d        jitsi/prosody       "/init"             5 hours ago         Up 5 hours          5222/tcp, 5269/tcp, 5280/tcp, 5347/tcp             docker-jitsi-meet_prosody_1
1ca2fa9445ae        jitsi/web           "/init"             5 hours ago         Up 5 hours          0.0.0.0:8000->80/tcp, 0.0.0.0:8443->443 tcp        docker-jitsi-meet_web_1
``` 

Para detener los contenedores `docker stop CONTAINER-ID`, los borramos con `docker rm CONTAINER-ID`.

Para ver las imágenes que tenemos `docker images`.

Para borrar las imágenes `docker rmi jitsi/jvb jitsi/jicofo jitsi/prosody jitsi/web`.


## Configuración Jitsi

Si editamos el fichero .env es necesario borrar los ficheros creados previamente en "~/.jitsi-meet-cfg/": `rm -rf ~/.jitsi-meet-cfg/{web/letsencrypt,transcripts,prosody,jicofo,jvb}`

Editamos .env:

- `DOCKER_HOST_ADDRESS`: IP del _host_ Docker_, necesario para una red local. Le pongo la IP de la máquina Debian donde ejecuto docker.

## Referencias internas

**Docker**:

- [Contenedores Docker con R [Borrador en progreso]](https://soka.gitlab.io/blog/post/2019-04-13-docker-contenedor-r/)
- [Docker](https://soka.gitlab.io/blog/post/2019-04-23-docker-iniciacion/): Introducción en MS Win.
- [Entorno compilación GNU gcc con Docker](https://soka.gitlab.io/blog/post/2019-06-25-docker-gcc-compiler/).
- [Montar un servidor Web básico con NGINX y Docker](https://soka.gitlab.io/blog/post/2019-07-08-docker-imagenes-y-contenedores/).

## Referencias externas

- [https://github.com/jitsi/docker-jitsi-meet](https://github.com/jitsi/docker-jitsi-meet): This repository contains the necessary tools to run a Jitsi Meet stack on Docker using Docker Compose.
