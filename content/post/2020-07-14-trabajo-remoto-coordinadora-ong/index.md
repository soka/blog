---
title: Trabajo remoto y cuidado de equipos
subtitle: Coordinadora de organizaciones para el desarrollo
date: 2020-07-14
draft: false
description: Cosecha del encuentro online 15 de Junio 2020
tags: ["Aplicaciones","Formación","Jornadas","Web","Teletrabajo","Herramientas","Trabajo","Tabla de contenidos","Colaborativo"]
---

![](img/cropped-Logo_Horizontal-actualizado-2.png)

Está [presentación](https://view.genial.ly/5ee906085b53640d758881ab) creada por la [Coordinadora de ONG para el Desarrollo](https://coordinadoraongd.org/) y elaborada en [Genially](https://www.genial.ly/es) ha llegado casualmente a mi correo, alguna de las herramientas que se mencionan a continuación se pueden encontrar en otro post anterior en este blog ["Herramientas para teletrabajar de forma cómoda y sencilla"](https://soka.gitlab.io/blog/post/2020-06-26-jornada-teletrabajo/) fruto de la jornada impartida por [Imanol Terán](https://www.itermar.io/) 

## Herramientas

Sólo voy a poner referencias a algunas de las herramientas que me han llamado la atención, en el post mencionado arriba se pueden encontrar seguramente menciones al resto.

### Comunicación 

Skype, Whatsapp, Teams, Zoom, Jitsi, GoogleMeet, Telegram, Sharepoint.

- [Microsoft Stream](https://www.microsoft.com/es-es/microsoft-365/microsoft-stream): Ofrecer eventos en directo y a petición con Microsoft 365.
- [https://www.happydonia.com/](https://www.happydonia.com/): Red social privada, Chat empresarial, registro horario, etc. Todos los planes son de pago.
- [https://whereby.com/](https://whereby.com/): Videoconferencia. Versión gratuita de un organizador, una sala y hasta 4 participantes, permite compartir pantalla y conectar son apps como Trello, Google Drive, etc.
- **Cisco [https://www.webex.com/es/index.html](https://www.webex.com/es/index.html)**. Videoconferencia profesional. Recomendable. Plan gratuito hasta 100 participantes y 50 minutos. Compartir escritorio. Funciones de preguntas y respuestas, encuestas y pedir el turno. Pizarras interactivas. 
- [https://www.bluejeans.com/es](https://www.bluejeans.com/es): Videoconferencia.

**Chat**:

Hangout, 

- [https://slack.com/intl/es-es/](https://slack.com/intl/es-es/)
- **[https://twist.com/](https://twist.com/)**
- **[https://discord.com/](https://discord.com/)**.
- [https://zadarma.com/es/](https://zadarma.com/es/): telefonía en la nube.

### Tableros

Trello.

- [https://gladys.com/es](https://gladys.com/es). Versión free  hasta 5 usuarios, 2 áreas de trabajo.
- Post-it (app)  [https://play.google.com/store/apps/details?id=com.mmm.postit&hl=es](https://play.google.com/store/apps/details?id=com.mmm.postit&hl=es).

### PDI

- **[https://conceptboard.com/](https://conceptboard.com/)** La versión gratuita parece suficiente. 
- **[https://awwapp.com/](https://awwapp.com/)**.
- **[https://app.diagrams.net](https://app.diagrams.net)**.
- [https://www.mural.co/](https://www.mural.co/). Todos los planes de pago.
- [https://miro.com/](https://miro.com/)
- [https://www.microsoft.com/es-es/microsoft-365/microsoft-whiteboard/digital-whiteboard-app](https://www.microsoft.com/es-es/microsoft-365/microsoft-whiteboard/digital-whiteboard-app).
- [https://www.ipevo.com/](https://www.ipevo.com/).
- Jamboard (Hardware)
- [https://en.linoit.com/](https://en.linoit.com/).
- [https://es.padlet.com/dashboard](https://es.padlet.com/dashboard).

### Retrospectivas

- **[https://retrotool.io/](https://retrotool.io/)**
- **[http://retroo.io/](http://retroo.io/)**
- [https://www.retrium.com/](https://www.retrium.com/)
- [https://funretro.io/](https://funretro.io/)

## Gestión de proyectos

- [https://tasks.office.com/](https://tasks.office.com/)
- [https://monday.com/](https://monday.com/)


### Trabajo colaborativo

Onedrive, padlet, Calendar, googleDrive, Moodle, Forms, **Trello**, Lumen (??), Miro.

- **[https://www.xmind.net/](https://www.xmind.net/)**: Mapas conceptuales open source.

### Gestión de equipos

Trello...

### Combinadas

Microsoft 365, OneNote.

- [https://www.mentimeter.com/](https://www.mentimeter.com/): aplicación utilizada para crear presentaciones con comentarios en tiempo real, concursos, cuestionarios.
- [https://gsuite.google.es/intl/es/products/jamboard/](https://gsuite.google.es/intl/es/products/jamboard/) PDI.



### Tareas

- [https://todol.ink/](https://todol.ink/). "Una web en la que solo tenemos que entrar, ponerle nombre a la lista e incluir las cosas que debemos hacer. Sin registro ni instalación de ninguna app (aunque es posible registrarse para guardar las listas creadas), todo lo que hagamos se guardará en un link único disponible en la parte inferior"  [1](https://wwwhatsnew.com/2020/07/13/la-forma-mas-sencilla-de-crear-una-lista-de-tareas-y-compartirla/).

### Otras

Evernote. 

- Pixabay [https://pixabay.com/es/](https://pixabay.com/es/) banco de imágenes.
- [https://www.canva.com/](https://www.canva.com/): Creación de diseños. Versión gratuita.
- [https://www.photopea.com/](https://www.photopea.com/): Editor de imágenes Web. El plan gratuito inserta elementos externos.


## Presentación

![](img/01.png)

![](img/02.png)

![](img/03.png)

![](img/04.png)

![](img/05.png)

![](img/06.png)

![](img/07.png)

![](img/08.png)

![](img/09.png)

![](img/10.png)

![](img/11.png)

![](img/12.png)

![](img/13.png)

![](img/14.png)

![](img/15.png)

![](img/16.png)

![](img/17.png)

![](img/18.png)

![](img/19.png)

![](img/21.png)

![](img/22.png)


