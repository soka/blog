---
title: Espanso autocompleta textos 
subtitle: Ahorra tiempo escribiendo siempre los mismos textos
date: 2021-10-13
draft: true
description: Ahorra tiempo escribiendo siempre los mismos textos
tags: ["Herramientas","Desktop","Windows","Aplicaciones","Typing","Escritura"]
---

<!-- vscode-markdown-toc -->
* 1. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Espanso es una aplicación para supervitamninar nuestros hábitos de escritura, usando palabras clave podemos insertar textos predeterminados.

Una vez instalada la aplicación podemos comprobar su correcto funcionamiento, Epanso se ejecuta en segundo plano, para probar abrimos un nuevo correo electrónico por ejemplo y escribimos: 

![](img/01.gif)

En Windows el fichero con 

##  1. <a name='Enlacesexternos'></a>Enlaces externos

* [https://espanso.org/docs/get-started/](https://espanso.org/docs/get-started/).