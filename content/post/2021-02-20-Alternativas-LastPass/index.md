---
title: Alternativas a LastPass
subtitle: Evaluación gestores de contraseñas
date: 2021-02-20
draft: false
description: Evaluación diferentes aplicaciones de gestión de contraseñas
tags: ["LastPass","Aplicaciones","Comparativas","Seguridad","Contraseñas","Web","Ciberseguridad","Bitwarden"]
---

<!-- vscode-markdown-toc -->
* 1. [Bitwarden](#Bitwarden)
* 2. [KeePassXC](#KeePassXC)
* 3. [KeePass](#KeePass)
* 4. [LogMeOnce](#LogMeOnce)
* 5. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

[**LastPass**](https://www.lastpass.com/es/) [anunció](https://blog.lastpass.com/2021/02/changes-to-lastpass-free/) recientemente algunos cambios en sus condiciones de uso para los usuarios de las cuentas gratuitas, ahora que cuenta con más de 20 millones de usuarios presumo que su intención es "capturar" a esos usuarios y convertirlos en usuarios de pago. Si usas una cuenta gratuita en adelante sólo podrás usar LastPass en la computadora o en dispositivos móviles, eso no quiere decir que LastPass ya no sirva y que debamos cambiarnos a otra aplicación, a pesar de esta nueva limitación LastPass sigue siendo una aplicación a tomar en cuenta. La mayoría de gestores de contraseñas que comento a continuación no ofrecen funcionalidades que siguen haciendo LastPass tan popular, especialmente el completado automático de formularios de acceso y que sea una aplicación Web.

Para esta comparativa he escogido alternativas completamente gratuitas o aquellas que siendo de pago ofrecen versiones gratuitas sin apenas limitaciones.

##  1. <a name='Bitwarden'></a>Bitwarden

[Bitwarden](https://bitwarden.com/) es otra aplicación _freemium_ como LastPass. En este enlace se pueden comparar las [funcionalidades de la versión gratuita frente a las de pago](https://bitwarden.com/pricing/business/). Bitwarden es un software 100% de código abierto. El código fuente de Bitwarden está alojado en [GitHub](https://github.com/bitwarden) y cualquier persona puede revisarlo libremente y auditarlo o contribuir al conjunto de código de Bitwarden.

Características destacadas de la versión gratuita:

- Aplicación de escritorio multiplataforma, móvil y Web.
- 2 Usuarios.
- Sincronizado entre dispositivos.
- Generador de contraseñas seguras.
- Autenticación 2FA.

A primera vista el interfaz es minimalista. El funcionamiento es similar a cualquier otro gestor de contraseñas, podemos ordenar los elementos por carpetas (sólo un nivel de carpetas sin embargo).

![](img/01.png)

Los elementos contienen campos similares a LastPass, un nombre que describa el contenido, la URL del servicio, usuario, contraseña, etc.

![](img/02.png)

Una vez guardado podemos abrir el sitio Web usando el icono de configuración.

**Bitwarden también se integra con el navegador** usando extensiones ([Chrome](https://chrome.google.com/webstore/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb), [Firefox](https://addons.mozilla.org/es/firefox/addon/bitwarden-password-manager/)). En este [vídeo](https://bitwarden.com/browser-start/) explican como funciona. 

La extensión para navegador permite que la aplicación complete de forma automática el formulario de autenticación, si estamos registrándonos por primera vez permite añadir la nueva entrada o generar una nueva contraseña.

Si no queremos alojar nuestras contraseñas en la nube Bitwarden **también ofrece una aplicación de escritorio** que se puede [descargar](https://bitwarden.com/download/) para Windows, macOS y Linux.

Por último, si vas a usar Bitwarden **no olvides revisar las opciones de configuración de la cuenta**, especialmente la autenticación de dos pasos, se puede usar el correo electrónico para que te envíe un código de verificación o una aplicación móvil que genera códigos de un sólo uso como [Authy](https://play.google.com/store/apps/details?id=com.authy.authy) o [Google Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2).

![](img/03.png)

##  2. <a name='KeePassXC'></a>KeePassXC

[KeePassXC](https://keepassxc.org/) es una aplicación de escritorio con licencia de código abierto (GNU GPL). 

La aplicación está disponible para múltiples plataformas y el código fuente está colgado en [GitHub](https://github.com/keepassxreboot/).

Para sincronizar los datos entre dispositivos, podemos recurrir a plataformas como Dropxbox/OneDrive o similares.

![](img/04.png)

También ofrecen una [extensión para navegadores](https://github.com/keepassxreboot/keepassxc-browser) [Firefox](https://addons.mozilla.org/en-US/firefox/addon/keepassxc-browser/) or [Chrome/Chromium](https://chrome.google.com/webstore/detail/keepassxc-browser/oboonakemofpalcgghocfoadofidjkkk) y [Microsoft Edge](https://microsoftedge.microsoft.com/addons/detail/pdffhmdngciaglkoonimfcmckehcpafo).

El funcionamiento una vez instalado se parece mucho a KeePass que veremos más adelante, la aplicación crea una base de datos cifrada que se almacena en un archivo (formato KDBX compatible con KeePass) que guardamos donde queramos en nuestro equipo, la bóveda está protegida por una contraseña maestra.

Este es el aspecto que luce la aplicación recien instalada:

![](img/05.png)

El espacio de trabajo está dividido en dos secciones, si tenemos más de una base de datos se ordenan en varias pestañas. El panel de la izquierda permite navegar entre grupos donde ordenamos las entradas.

![](img/07.png)

La barra de herramientas contiene accesos rápidos para realizar tareas habituales con la base de datos, en orden de izquierda a derecha:

- Nueva base de datos.
- Abrir una base de datos existente.
- Guardar los cambios realizados en la base de datos.
- Añadir una nueva entrada.
- Ver/Editar una entrada.
- Eliminar entrada.
- Generador de contraseñas.
- ...

El sistema para añadir una nueva entrada es similar a otros gestores de contraseñas:

![](img/08.png)

Una vez introducida una nueva entrada desde el menú contextual de la entrada se puede abrir la URL en el navegador que tengamos configurado por defecto. 

##  3. <a name='KeePass'></a>KeePass

##  4. <a name='LogMeOnce'></a>LogMeOnce

[**LogMeOnce**](https://www.logmeonce.com) es un gestor de contraseñas online, este es un producto _freemium_ que ofrece la posibilidad de una cuenta gratuita personal con las siguientes funcionalidades:

- Número de contraseñas ilimitadas.
- Autorrellenado de formularios.
- **Sin limitación de dispositivos sincronizados**.
- Autenticación de dos factores.
- Generador de contraseñas seguras.

##  5. <a name='Enlacesexternos'></a>Enlaces externos

- Genbeta - Bitwarden y otros tres gestores de contraseñas gratuitos para sustituir a Lastpass y 1Password [https://www.genbeta.com/herramientas/bitwarden-otros-tres-gestores-contrasenas-gratuitos-para-sustituir-a-lastpass-1password](https://www.genbeta.com/herramientas/bitwarden-otros-tres-gestores-contrasenas-gratuitos-para-sustituir-a-lastpass-1password).

**KeePassXC**

- [https://keepassxc.org/docs/KeePassXC_GettingStarted.html](https://keepassxc.org/docs/KeePassXC_GettingStarted.html).