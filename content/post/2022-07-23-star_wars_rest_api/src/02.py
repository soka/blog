#!/usr/bin/env python3

import requests
api_url = "https://swapi.dev/api/"
response = requests.get(api_url)
print(type(response.json())) # <class 'dict'>

# print response
print(response) # <Response [200]>

# print json content
print(response.json())
'''
{'people': 'https://swapi.dev/api/people/', 'planets': 'https://swapi.dev/api/planets/', 'films': 'https://swapi.dev/api/films/', 'species': 'https://swapi.dev/api/species/', 'vehicles': 'https://swapi.dev/api/vehicles/', 'starships': 'https://swapi.dev/api/starships/'}
'''


