#!/usr/bin/env python3

import requests
import json

api_url = "https://swapi.dev/api/people/"
people_response = requests.get(api_url)
people_data= people_response.json()
for item in people_data['results']:
    print(item['name'])