---
title: SWAPI The Star Wars API
subtitle: API REST interactuando con servicios Web
date: 2022-09-23
draft: true
description: API REST interactuando con servicios Web
tags: ["Python","Retos","Aplicaciones","Juegos","ASCII","Hackathon","Dungeon crawl","Proyectos"]
---


<!-- vscode-markdown-toc -->
* 1. [Requests](#Requests)
* 2. [Códigos de estado HTTP](#CdigosdeestadoHTTP)
* 3. [Métodos de la respuesta](#Mtodosdelarespuesta)
* 4. [GET](#GET)
* 5. [Acceso a los elementos individuales](#Accesoaloselementosindividuales)
* 6. [Iterar recorriendo el objeto con la respuesta](#Iterarrecorriendoelobjetoconlarespuesta)
* 7. [Pandas Dataframe](#PandasDataframe)
* 8. [Exportar resultados a Excel](#ExportarresultadosaExcel)
* 9. [Recorrer los resultados paginados](#Recorrerlosresultadospaginados)
* 10. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Requests'></a>Requests

Para escribir código que interactue con una API REST usamos el módulo requests, tal vez debamos instalarlo previamente:

```bash
$ python -m pip install requests
```

##  2. <a name='CdigosdeestadoHTTP'></a>Códigos de estado HTTP

Una vez que una API REST recibe y procesa una solicitud HTTP, devolverá una **respuesta HTTP**. En esta respuesta se incluye un [**código de estado HTTP**](https://es.wikipedia.org/wiki/Anexo:C%C3%B3digos_de_estado_HTTP). Este código proporciona **información sobre los resultados de la solicitud**. Una aplicación que envía solicitudes a la API puede comprobar el código de estado y realizar acciones en función del resultado. Estas acciones podrían incluir el manejo de errores o mostrar un mensaje de éxito a un usuario.

![](img/01.png)

[src/01.py](src/01.py)

```python
#!/usr/bin/env python3

import requests

api_url = "https://swapi.dev/api/"
response = requests.get(api_url)
print('GET '+api_url+' status code:'+str(response.status_code)) # 200
```

##  3. <a name='Mtodosdelarespuesta'></a>Métodos de la respuesta

![](img/03.png)

##  4. <a name='GET'></a>GET

GET es uno de los [métodos HTTP](https://es.wikipedia.org/wiki/Protocolo_de_transferencia_de_hipertexto#GET) más comunes que usará cuando trabaje con API REST. Este método le permite **recuperar recursos** de una API determinada. GET es una **operación de solo lectura**, por lo que no debe usarla para modificar un recurso existente.

[src/02.py](src/02.py)

```python
#!/usr/bin/env python3

import requests
api_url = "https://swapi.dev/api/"
response = requests.get(api_url)
print(type(response.json())) # <class 'dict'>

# print response
print(response) # <Response [200]>

# print json content
print(response.json())
'''
{'people': 'https://swapi.dev/api/people/', 'planets': 'https://swapi.dev/api/planets/', 'films': 'https://swapi.dev/api/films/', 'species': 'https://swapi.dev/api/species/', 'vehicles': 'https://swapi.dev/api/vehicles/', 'starships': 'https://swapi.dev/api/starships/'}
'''
```

Este código llama a `requestes.get()` para enviar una solicitud GET a [https://swapi.dev/api/](https://swapi.dev/api/), que responde con todos los elementos de la API. 

![](img/02.png)

La función response.json() devuelve un objeto JSON del resultado (si el resultado se escribió en formato JSON, si no, genera un error).

##  5. <a name='Accesoaloselementosindividuales'></a>Acceso a los elementos individuales

Buscamos los carácteres que intervienen en las películas [https://swapi.dev/api/people/](https://swapi.dev/api/people/).

[src/03.py](src/03.py)

```python
data = response.json() # <class 'dict'>
print(data['people'])
people_data = requests.get(data['people'])
print(people_data.json())
```

##  6. <a name='Iterarrecorriendoelobjetoconlarespuesta'></a>Iterar recorriendo el objeto con la respuesta

[src/04.py](src/04.py)

```python
#!/usr/bin/env python3

import requests
import json

api_url = "https://swapi.dev/api/people/"
people_response = requests.get(api_url)
people_data= people_response.json()
for item in people_data['results']:
    print(item['name'])
``` 

Salida:

```
Luke Skywalker
C-3PO
R2-D2
Darth Vader
Leia Organa
Owen Lars
Beru Whitesun lars
R5-D4
Biggs Darklighter
Obi-Wan Kenobi
```

##  7. <a name='PandasDataframe'></a>Pandas Dataframe

##  8. <a name='ExportarresultadosaExcel'></a>Exportar resultados a Excel

[src/05.py](src/05.py)

```python
#!/usr/bin/env python3

import requests
import json
import pandas as pd

api_url = "https://swapi.dev/api/people/"
people_response = requests.get(api_url)
people_data= people_response.json()
#print(people_data['results'])
df = pd.DataFrame.from_dict(people_data['results'])
#print(df)
df.to_excel("output.xlsx",sheet_name='Sheet_name_1')  
```

##  9. <a name='Recorrerlosresultadospaginados'></a>Recorrer los resultados paginados

La API ofrece los resultados paginados, en la clave "next" de la respuesta JSON podemos obtener el enlace a los siguientes resultados: 

[src/06.py](src/06.py)

```json
#!/usr/bin/env python3

import requests
import json
import pandas as pd

page_num=1
api_url = "https://swapi.dev/api/people/?page="+str(page_num)
response = requests.get(api_url)
result_lst=[]
while response.status_code == 200:    
    people_data = response.json()          
    for item in people_data['results']:
        result_lst.append(item)       
    page_num+=1
    api_url = "https://swapi.dev/api/people/?page="+str(page_num)
    response = requests.get(api_url)

df = pd.DataFrame(result_lst)
df.to_excel("output.xlsx",sheet_name='people') 
```


##  10. <a name='Enlacesexternos'></a>Enlaces externos

* [https://realpython.com/api-integration-in-python/](https://realpython.com/api-integration-in-python/)
* [https://realpython.com/python-requests/](https://realpython.com/python-requests/)
* [https://www.w3schools.com/PYTHON/ref_requests_get.asp](https://www.w3schools.com/PYTHON/ref_requests_get.asp)
* [https://www.geeksforgeeks.org/response-json-python-requests/](https://www.geeksforgeeks.org/response-json-python-requests/)
* [https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.from_dict.html?highlight=from_dict](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.from_dict.html?highlight=from_dict)


