---
title: Conectar Google y WordPress [borrador]
subtitle: Integra formularios y otros productos
date: 2020-11-27
draft: true
description: 
tags: ["Wordpress","WP - Curso","WP","WP - Google"]
---

![](img/wordpress-logo-01.png)

## Pad de trabajo

[https://pad.riseup.net/p/penascalf52021](https://pad.riseup.net/p/penascalf52021)

## Habilitando las analíticas de Google

![](img/01.png)

[**Google Analytics**](https://es.wikipedia.org/wiki/Google_Analytics) es una herramienta de analítica de tráfico sitios Web, es una de las aplicaciones más usadas en marketing digital para analizar el comportamiento de nuestros visitantes, la información se puede segmentar y filtrar por diversos criterios como la localización de origen, las franjas horarias o el tiempo de permanencia dentro de la Web. La integración se realiza insertando un código que proporciona Analytics en nuestra Web, hoy en día está labor se facilita gracias a plugins.

Necesitarás [registrarte en Google](https://accounts.google.com/signup) para poder usar Analytics, dentro de la aplicación hay que crear una cuenta.

![](img/02.png)

Asignamos un nombre de cuenta junto con algunos datos adicionales y aceptamos los términos GDPR. Podemos obtener datos de diferentes fuentes, seleccionamos Web e introducimos la URL y el nombre del sitio para crear un flujo de comunicaciones entre nuestra plataforma WP y Analytics.

![](img/03.png)

**NOTA**: Al crear la propiedad debemos usar las opciones avanzadas para activar "Create a Universal Analytics Property".

### Plugin MonsterInsights Lite

Al finalizar nos ofrece un código de identificación que debemos introducir en WP, existen muchos complementos en el mercado, una de las mejores opciones es el plugin [Google Analytics Dashboard Plugin for WordPress by MonsterInsights](https://es.wordpress.org/plugins/google-analytics-for-wordpress/), con una valoración muy buena y más de 2 millones de instalaciones activas. Si solo necesitas las funciones básicas de una herramienta de análisis web, MonsterInsights ofrece una versión Lite gratuita, las funcionalidades avanzadas son de pago.

Una vez instalado y activado se habilita una nueva opción de menú "Insights" en el escritorio de WP. Para la configuración inicial usaremos el proceso guiado.

![](img/04.png)

**NOTA**: Pueden transcurrir 24 horas hasta que se sincronice y recibamos los primeros datos.

## Integrar un formulario

Los [formularios de Google](https://www.google.com/intl/es_es/forms/about/) tienen algunas ventajas frente a otros basados en plugins como [Contact Form 7](https://soka.gitlab.io/blog/post/2020-11-14-curso-wp-7/), un formulario de Google no tiene dependencia respecto a WP, se puede distribuir mediante un enlace o incrustado en cualquier Web o correo electrónico, no requiere plugins adicionales que pueden afectar al rendimiento del sitio Web, podemos a invitar a colaborar a otros usuarios diseñando el formulario Web sin necesidad de crear más usuarios en WP, como las respuestas quedan registradas en una hoja de cálculo de Google se pueden tratar los datos fácilmente y obtener informes de resumen.

![](img/17.png)

Ahora sólo tenemos que pegar el código HTML en una entrada o página.

**Enlaces**:

- [https://kinsta.com/es/blog/insertar-google-form/](https://kinsta.com/es/blog/insertar-google-form/)
- [https://wordpress.org/plugins/wpgform/](https://wordpress.org/plugins/wpgform/).

## Mapas

![](img/16.png)

Los mapas son un elemento muy habitual sitios Web que ofrecen una ubicación geolocalizada como una oficina que se muestra en el apartado de contacto, puede servir también para especificar un punto de encuentro u otros usos, **una de sus ventajas es que son interactivos y se puede navegar por ellos** (frente a una imagen estática o una simple dirección), **ofrecen información adicional** como medios de transporte cercanos a la ubicación y otros servicios.

Existen 3 formas de integrar un mapa de [**Google Maps**](https://www.google.com/maps) en WP:

1. Insertar el _frame_ o el código HTML que nos ofrece Google Maps en una página de nuestro sitio.
2. La mayoría de las plantillas suelen ofrecer está opción introduciendo la ubicación.
3. Usando un plugin como [WP Google Maps](https://wordpress.org/plugins/wp-google-maps/).

### Incrustar un frame de Google Maps

Esta opción es la más sencilla, buscamos una ubicación en Google Maps y usamos la opción compartir, copiamos el código HTML y lo pegamos en nuestra entrada o página de WP.

### Mapas personalizados

Con nuestra cuenta de Google podemos crear mapas personalizados destacando coordenadas o creando varias capas para separar por categorías.

![](img/13.png)

### Servicios para generar mapas

Existen multitud de servicios para personalizar un mapa de Google y exportarlo para introducirlo en WP sin necesidad de usar añadidos.

[addmap.net](https://addmap.net/) es muy sencillo, se introduce la direccióne, el zoom inicial, el tipo de mapa y el alto y ancho del mapa, la salida es código HTML para incrustar en WP.

![](img/12.png)

[Embed Google Map](https://www.embedgooglemap.net/) es otro ejemplo que hace exactamente lo mismo.

### Plugins para Google Maps

#### WP Google Maps (Lite)

![](img/06.png)

Como es habitual este plugin tiene versión gratuita (Lite) y versión de pago (Pro) que permite hacer más cosas. Una vez instalado tenemos a nuestra disposición un creador de mapas en el menú principal del escritorio.

Antes de comenzar debes crear una [**un identificador API JS en Google**](https://developers.google.com/maps/documentation/javascript/get-api-key) para poder usar los mapas en otras aplicaciones como WP.

![](img/09.png)

Una vez obtenido lo introducimos en **Mapas | Configuración**:

![](img/10.png)

El plugin viene con una mapa de ejemplo que podemos explorar para comprender su funcionamiento.

![](img/07.png)

El mapa se puede insertar en una entrada o en una página usando un _shortcode_

![](img/08.png)

Así es como se muestra el mapa de ejemplo que viene con el plugin:

![](img/11.png)

El mensaje "_For development purposes only_" se refiere a que Google Maps ha empezado a poner [precio](https://cloud.google.com/maps-platform/pricing/) a su servicio de mapas. Se debe habilitar algún método de pago para quitar la leyenda, si nuestro sitio no recibe miles de visitas probablemente nunca llegarás a pagar.

WP Google Maps también está disponible como Widget para inscrustar en una barra lateral, en el pie de la Web o en cualquier sitio que permita la plantilla.

Existen otros servicios de mapas alternativos:

- [OpenStreetMap](https://www.openstreetmap.org/): Proyecto colaborativo para crear mapas editables y libre.
- [OpenLayers](https://openlayers.org/): Librería JS de código abierto (licencia BSD), **este generador de mapas se puede usar en WP Google Maps**.

#### Google Maps Easy (By supsystic.com)

[Google Maps Easy](https://wordpress.org/plugins/google-maps-easy/)

De nuevo debemos recurrir a la [clave API de Google](https://supsystic.com/documentation/create-google-maps-api-key/) en la [consola de desarrolladores](https://cloud.google.com/maps-platform/#get-started) creando un nuevo proyecto.

![](img/14.png)

Insertamos la clave API en ajustes del plugin:

![](img/15.png)

## Enlaces externos

- [https://codex.wordpress.org/WordPress_Menu_User_Guide](https://codex.wordpress.org/WordPress_Menu_User_Guide).
- [https://wordpress.com/support/advanced-menu-settings/](https://wordpress.com/support/advanced-menu-settings/)
- [https://wpza.net/how-to-open-menu-link-in-a-new-tab-in-wordpress/](https://wpza.net/how-to-open-menu-link-in-a-new-tab-in-wordpress/).
- [https://www.hostinger.es/tutoriales/insertar-google-maps-en-wordpress/](https://www.hostinger.es/tutoriales/insertar-google-maps-en-wordpress/).
- [https://www.monsterinsights.com/how-to-add-a-property-in-google-analytics/](https://www.monsterinsights.com/how-to-add-a-property-in-google-analytics/).
- [https://www.hostinger.es/tutoriales/insertar-google-maps-en-wordpress/](https://www.hostinger.es/tutoriales/insertar-google-maps-en-wordpress/)