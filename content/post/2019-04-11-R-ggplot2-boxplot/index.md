---
title: Diagramas de caja
subtitle: Introducción a ggplot2
date: 2019-04-11
draft: false
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "Programación",
    "R - Básico",
    "R - Gráficos",
    "R - Diagramas",
    "R - ggplot2",
    "Diagramas",
  ]
---

![](images/ggplot_hex.jpg)

_"También conocido como diagrama de caja y bigote o boxplot. Es un método estandarizado para representar gráficamente una serie de datos numéricos a través de sus cuartiles. De esta manera, el diagrama de caja muestra a simple vista la mediana y los cuartiles de los datos, pudiendo también representar los valores atípicos de estos"_. ([Wikipedia](https://es.wikipedia.org/wiki/Diagrama_de_caja)).

Pertenece al área de la estadística descriptiva.

![](images/boxplot-diagram-01.PNG)

# Ejemplo con dataset mpg

Vamos a volver a utilizar el _dataset_ mpg que viene con **ggplot2**, en este primer ejemplo vamos a relacionar el tipo de coche (_class_) con su consumo (_hwy_).

```R
ggplot(mpg,aes(x=class,y=hwy)) +
  geom_boxplot(aes(color=class)) +
  ggtitle("Consumición por clase")
```

La línea en el centro de la caja representa la mediana, si no está en el centro del rectángulo la distribución no es simétrica. Podemos ver por ejemplo que los modelos "subcompact" la caja está algo más arriba que el resto por lo que deducimos que consumen más. El punto superior representa el máximo y el inferior el mínimo. La parte superior de la caja es el 3º cuartil y la inferior el 1º cuartil.

¿Qué es un **cuartil**? Los cuartiles sirven **para identificar fronteras**. Imaginemos que tenemos 100 muestras de datos con sus valores, ordenamos de menor a mayor sus valores, haces 4 grupos de 25 muestras cada una, el valor de la última muestra de cada grupo es el **cuartil**.

Siguiendo el mismo ejemplo la muestra 25 es el cuartil 1, el 50 es el Q2 y el 75 el Q3.

La dimensión de la caja viene definida por el Q1 y el Q3. La distancia que hay entre estos dos valores es el **rango intercuartílico**.

Los puntos son **valores raros o outliers** son valores muy grandes o muy pequeños que dibuja como puntos.

![](images/ggplot2-boxplot-01.PNG)

Este tipo de diagrama es muy útil ya que nos provee de mucha información de forma visual.

# Ejercicios

- r / intro / 34-ggplot2-boxplot / [ggplot2-boxplot.r](https://gitlab.com/soka/r/blob/master/intro/34-ggplot2-boxplot/ggplot2-boxplot.r).

# Enlaces internos

- Categoría de artículos sobre **ggplot** en esta Web [#R - ggplot2](https://soka.gitlab.io/blog/tags/r-ggplot2/).

# Enlaces externos

- ["Diagrama de caja - Wikipedia, la enciclopedia libre"](https://es.wikipedia.org/wiki/Diagrama_de_caja).
- ["Cuartil - Wikipedia, la enciclopedia libre"](https://es.wikipedia.org/wiki/Cuartil).
- ["Como ser más rápido estudiando variables numéricas: El Boxplot – Estadística descriptiva Parte2"](https://conceptosclaros.com/como-ser-mas-rapido-boxplot-estadistica-descriptiva-parte2/).
- ["Diagrama BoxPlot - Proyectos Gestión Conocimiento"](https://www.pgconocimiento.com/diagrama-boxplot/).
