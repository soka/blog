---
title: Herramientas para teletrabajar de forma cómoda y sencilla
subtitle: Contenidos de la jornada impartida por Imanol Terán
date: 2020-06-26
draft: false
description: 
tags: ["Aplicaciones","Formación","Jornadas","SPRI","Web","Teletrabajo","Herramientas","Trabajo","Tabla de contenidos","Colaborativo"]
---

Jornada impartida por [Imanol Terán](https://www.itermar.io/) donde revisa una serie herramientas que facilitan el teletrabajo. La mayoría de los recursos son de pago pero quiero revisarlos para valorar su implantación para alguna de las empresas que presto servicios. El contenido de la jornada está disponible para su descarga en la Web del autor junto con otros recursos super interesantes.

En [este enlace](https://www.itermar.io/teletrabajo/) además se pueden encontrar vídeos agrupados por tipos de herramientas, gestión de tareas y proyectos, comunicaciones, herramientas colaborativas, control remoto, automatización de tareas y seguridad y privacidad.

[Jornada_-_Teletrabajo.pdf](./Jornada_-_Teletrabajo.pdf)

Para empresas con otra filosofía existen alternativas libres en muchos casos [https://soka.gitlab.io/blog/post/2020-04-07-tabla-aplicaciones-trabajo-colaborativo/](https://soka.gitlab.io/blog/post/2020-04-07-tabla-aplicaciones-trabajo-colaborativo/). Muchas de estas herramientas están obtenidas de [https://librezale.eus/wiki/KomunikazioEtaElkarlanerakoTresnak](https://librezale.eus/wiki/KomunikazioEtaElkarlanerakoTresnak), si además de herramientas libres buscas que tengan el interfaz disponible en Euskera esta página es una parada obligatoria.

No he podido resistir la tentación de incrustar una tabla de [Airtable](https://airtable.com/) a ver que pasaba:

<iframe class="airtable-embed" src="https://airtable.com/embed/shr8meui8zGaCRl9S?backgroundColor=cyan&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>

## Control del tiempo

- [https://toggl.com/](https://toggl.com/)
- [https://www.timedoctor.com](https://www.timedoctor.com)
- [https://www.rescuetime.com/](https://www.rescuetime.com/)
- [https://www.getharvest.com/](https://www.getharvest.com/)
- [https://everhour.com/](https://everhour.com/)
- [https://hubstaff.com/](https://hubstaff.com/)
- [https://desktime.com/](https://desktime.com/)
- [https://www.getintention.com/](https://www.getintention.com/)

## Gestión de proyectos y tareas

- **[https://trello.com/](https://trello.com/)**
- **[https://basecamp.com/](https://basecamp.com/)**: La versión personal gratuita permite hasta 3 proyectos, 20 usuarios y 1GB de espacio.
- [https://www.plutio.com/](https://www.plutio.com/)
- [https://www.holded.com/es](https://www.holded.com/es)
- [https://www.teamwork.com](https://www.teamwork.com)
- [https://asana.com/es/](https://asana.com/es/): Gestión de proyectos y tareas en equipo. Gratis para equipos de hasta 15 integrantes. Con tablero estilo Kanban o Trello.
- [https://sunsama.com/](https://sunsama.com/)
- [https://www.favro.com/](https://www.favro.com/)
- **[https://lettucemeet.com/](https://lettucemeet.com/)**: Para acordar una cita. Similar a [https://doodle.com/es/](https://doodle.com/es/).
- [https://clickup.com/](https://clickup.com/)
- [https://www.productive.io/](https://www.productive.io/)
- [https://www.freshbooks.com/](https://www.freshbooks.com/)
- [https://www.paymoapp.com/](https://www.paymoapp.com/)
- [https://wooboard.com/](https://wooboard.com/)
- Wrike
- [https://es-la.workplace.com/](https://es-la.workplace.com/)

- [https://zenkit.com/en/todo/](https://zenkit.com/en/todo/) [1](https://www.genbeta.com/a-fondo/zenkit-to-do-quizas-mejor-alternativa-a-wunderlist-para-gestionar-tus-listas-tareas-forma-simple)

## Comunicaciones

- **[https://slack.com/intl/en-es/](https://slack.com/intl/en-es/)**
- **[https://discord.com/](https://discord.com/)**
- **[https://meet.jit.si/](https://meet.jit.si/)**.
- **[https://openmeet.io/](https://openmeet.io/)**: Videollamada simple de organizar. Tiene versión gratuita. Básicamente, lo único que tenemos que hacer es crear un encuentro en el acto, **sin necesidad de cuentas de usuario**, y compartir el enlace temporal que obtendremos.
- **[https://www.loom.com/](https://www.loom.com/)**: Comparte mensajes de vídeo. Se integra como extensión de Chrome. Plan básico gratuito. Permite grabar y combinar la Webcam con la pantalla, se puede envíar el vídeo como enlace y se integra con el correo de Gmail. Muy interesante para evitar escribir largos correos o para mandar un videotutorial breve o una comunicación interna por ejemplo. Una alternativa gratuita como extensión de Chrome [https://chrome.google.com/webstore/detail/free-video-email-by-cloud/hfacaegmldmpapapbipahgpdeboingpk](https://chrome.google.com/webstore/detail/free-video-email-by-cloud/hfacaegmldmpapapbipahgpdeboingpk).
- [https://www.yac.com/](https://www.yac.com/): Mensajes de voz y compartir pantalla para equipos. Existe una versión gratuita (pendiente de revisar limitaciones)
- [https://www.around.co/](https://www.around.co/)
- [https://www.intercom.com/](https://www.intercom.com/): Todos los planes son de pago.
- **[https://meet.google.com/](https://meet.google.com/)**
- **[https://zoom.us/](https://zoom.us/)**
- [https://www.skype.com/es/](https://www.skype.com/es/)
- [https://www.microsoft.com/es-es/microsoft-365/microsoft-teams/group-chat-software](https://www.microsoft.com/es-es/microsoft-365/microsoft-teams/group-chat-software)
- [https://whereby.com/](https://whereby.com/)
- [https://www.join.me/](https://www.join.me/): Todos los planes son de pago.
- [https://www.bluejeans.com/](https://www.bluejeans.com/): Todos los planes son de pago.
- [https://aircall.io/](https://aircall.io/)
- [https://www.intheloop.io/](https://www.intheloop.io/): Todos los planes son de pago.
- [https://www.fastmail.com/](https://www.fastmail.com/): Todos los planes son de pago.
- [https://sparkmailapp.com/es](https://sparkmailapp.com/es): Spark prioriza de forma inteligente tu correo. **Por el momento para iPhone, iPad, Mac y Android.**

## Formación a distancia

- **[https://streamyard.com/](https://streamyard.com/)**: Permite crear transmisiones de video en vivo en (RTMP) FB, Youtube, etc. La versión gratuita permite hasta 6 participantes.
- **[https://storyxpress.co/](https://storyxpress.co/)**: Grabación de vídeo pantalla y Webcam. Plan gratuito para 1 usuario.Funciona como extensión para Chrome. 
- [https://www.loom.com/](https://www.loom.com/): Mencionada más arriba. 
- [https://livestorm.co/](https://livestorm.co/): Plan gratuito para 10 personas registradas máximo a webinar y 20 minutos de duración.
- [https://webinarninja.com/](https://webinarninja.com/): Todos los planes son de pago.
- [https://www.classonlive.com/](https://www.classonlive.com/): Todos los planes son de pago.
- [https://home.webinarjam.com/](https://home.webinarjam.com/): Todos los planes son de pago.
- [https://www.crowdcast.io/discover](https://www.crowdcast.io/discover): Todos los planes son de pago.
- [https://www.bigmarker.com/](https://www.bigmarker.com/): Todos los planes son de pago.
- [https://livestream.com/](https://livestream.com/): Vimeo. 
- [https://www.gotomeeting.com/es-es](https://www.gotomeeting.com/es-es): Todos los planes son de pago.


- [https://speakerdeck.com/](https://speakerdeck.com/): Permite subir un PDF para presentarlo online.

## Almacenamiento en la nube

- [https://drive.google.com/](https://drive.google.com/)
- [https://www.dropbox.com/](https://www.dropbox.com/)
- [https://mega.nz/](https://mega.nz/)
- [https://www.office.com/](https://www.office.com/)
- [https://wetransfer.com/](https://wetransfer.com/)

## Herramientas colaborativas

- **[https://www.notion.so/](https://www.notion.so/)**
- **[https://miro.com/](https://miro.com/)**: Pizarra colaborativa online. Versión gratuita sin limitación de miembros pero sólo 3 tableros. Ideal para reuniones de equipo, diseño de flujos de trabajo.
- **[https://es.padlet.com/](https://es.padlet.com/)**. Pizarra colaborativa. La versión gratuita permite hasta 3 padlets y 10MB.
- [https://www.mural.co/](https://www.mural.co/): Sólo planes de pago.
- [https://screen.so/](https://screen.so/)
- [https://www.pandadoc.com/](https://www.pandadoc.com/): Contratos, ofertas, firma digital.
- **[https://airtable.com/](https://airtable.com/)**
- [https://coda.io/welcome](https://coda.io/welcome)
- Page Marker [https://chrome.google.com/webstore/detail/page-marker/jfiihjeimjpkpoaekpdpllpaeichkiod](https://chrome.google.com/webstore/detail/page-marker/jfiihjeimjpkpoaekpdpllpaeichkiod) para Chrome.
- **[https://tryshift.com/](https://tryshift.com/)**: App de escritorio. Varios servicios en cuenta en un sitio (Slack, Gmail, Trello, etc). La versión gratuita básica sólo permite conectar 2 cuentas y una App.
- [https://www.worldtimebuddy.com/](https://www.worldtimebuddy.com/)
- [https://timezone.io/](https://timezone.io/): Mantiene un registro de donde se encuentra tu equipo y la hora local.

## Control remoto

- [https://www.teamviewer.com/](https://www.teamviewer.com/)
- [https://anydesk.com/es](https://anydesk.com/es)
- [https://guacamole.apache.org/](https://guacamole.apache.org/)

## Automatización de tareas

- **[https://beeftext.org/](https://beeftext.org/)**: App sustitución de texto de código abierto. App Windows para escritorio (versión portable!).
- **[https://www.trankynam.com/atext/](https://www.trankynam.com/atext/)**: Reemplaza abreviaciones por frases que usemos frecuentemente. Para Win y Mac OS. 
- [https://zapier.com/](https://zapier.com/): Conecta apps Webs mediante eventos programados que provocan acciones. La versión gratuita permite 100 tareas al mes.
- [https://www.integromat.com/en/](https://www.integromat.com/en/): Similar al anterior, recetas para automatizar tareas entre apps. La versión gratuita permite 1000 operaciones, 100MB de transferencia de datos y 15 minutos.
- [https://automate.io/](https://automate.io/): En la versión gratuita permite 250 operaciones y 5 bots.
- [https://ifttt.com/](https://ifttt.com/).
- [https://textexpander.com/]/(https://textexpander.com/): Solo 30 días de prueba.

## Seguridad y privacidad

- **[https://www.lastpass.com/es/](https://www.lastpass.com/es/)**:  Gratis para 1 usuario. Guardar y autocompletar contraseñas en sitios Web. Se integra con navegador Web. 
- **[https://www.enpass.io/](https://www.enpass.io/)**: Gratuito como app de escritorio.
- **[https://bitwarden.com/](https://bitwarden.com/)**: De código abierto para escritorio.
- [https://www.dashlane.com/es](https://www.dashlane.com/es): La versión gratuita permite almacenar hasta 50 contraseñas, llenado automático de formularios y de datos de pago, uso compartido seguro de hasta 5 cuentas.
- [https://www.logalty.com/](https://www.logalty.com/)
- [https://www.dottedsign.com/](https://www.dottedsign.com/).
- [https://www.hellosign.com/](https://www.hellosign.com/)
- [https://www.docusign.com.es/](https://www.docusign.com.es/)
- [https://1password.com/es/](https://1password.com/es/): Todos los planes son de pago.
- [https://www.remembear.com/](https://www.remembear.com/): La versión gratuita es sólo para un dispositivo y no incluye sincronización ni copias de seguridad
- [https://acrobat.adobe.com/es/es/sign.html](https://acrobat.adobe.com/es/es/sign.html): Firma documentos electrónicamente. Versión gratuita de prueba.

- [https://www.logmeonce.com](https://www.logmeonce.com)
- **[https://keepass.info/](https://keepass.info/)**: Yo suelo usar esta app de escritorio.

## Otros

- **[https://getsharex.com/](https://getsharex.com/)**: Captura de pantalla. App de escritorio de código abierto para Win. Permite subir captura (Google Photos, Imgur, Flickr...)
- **[https://filepush.co/](https://filepush.co/)**: Permite subir y compartir ficheros usando la línea de comandos y HTTP (cURL y wget o Powershell en Win). 32MB de límite y se borran después de 7 días.
- **[https://www.speakreader.com/](https://www.speakreader.com/)**: [1](https://wwwhatsnew.com/2020/07/03/transforma-cualquier-texto-en-audio-de-forma-inmediata-y-gratuita/) Si tenéis un texto pendiente de leer y preferís escucharlo para poder ir haciendo otra tarea al mismo tiempo. Solo tenemos que pegar el texto deseado y elegir el idioma y el narrador, así como la velocidad de lectura. **Sin necesidad de registro**. La voz es robotizada y poco natural. Es posible incluso importar archivos PDF o libros electrónicos. En lo que respecta a la privacidad, indican que el texto no se envía a ninguna parte, y el sitio no utiliza cookies ni análisis para rastrear nada.
- [https://www.typeform.com](https://www.typeform.com): Crear formularios interactivos. Sólo planes de pago y periodo de evaluación. 

## Presentación

![](img/Jornada_-_Teletrabajo-01.png)

![](img/Jornada_-_Teletrabajo-02.png)

![](img/Jornada_-_Teletrabajo-03.png)

![](img/Jornada_-_Teletrabajo-04.png)

![](img/Jornada_-_Teletrabajo-05.png)

![](img/Jornada_-_Teletrabajo-06.png)

![](img/Jornada_-_Teletrabajo-07.png)

![](img/Jornada_-_Teletrabajo-08.png)

![](img/Jornada_-_Teletrabajo-09.png)

![](img/Jornada_-_Teletrabajo-10.png)

![](img/Jornada_-_Teletrabajo-11.png)

![](img/Jornada_-_Teletrabajo-12.png)

![](img/Jornada_-_Teletrabajo-13.png)

![](img/Jornada_-_Teletrabajo-14.png)

![](img/Jornada_-_Teletrabajo-15.png)

![](img/Jornada_-_Teletrabajo-16.png)

![](img/Jornada_-_Teletrabajo-17.png)

![](img/Jornada_-_Teletrabajo-18.png)

![](img/Jornada_-_Teletrabajo-19.png)

![](img/Jornada_-_Teletrabajo-20.png)

![](img/Jornada_-_Teletrabajo-21.png)

![](img/Jornada_-_Teletrabajo-22.png)

![](img/Jornada_-_Teletrabajo-23.png)

![](img/Jornada_-_Teletrabajo-24.png)

![](img/Jornada_-_Teletrabajo-25.png)

![](img/Jornada_-_Teletrabajo-26.png)

![](img/Jornada_-_Teletrabajo-27.png)

![](img/Jornada_-_Teletrabajo-28.png)

![](img/Jornada_-_Teletrabajo-29.png)

![](img/Jornada_-_Teletrabajo-30.png)

![](img/Jornada_-_Teletrabajo-31.png)

![](img/Jornada_-_Teletrabajo-32.png)

![](img/Jornada_-_Teletrabajo-33.png)

![](img/Jornada_-_Teletrabajo-34.png)

![](img/Jornada_-_Teletrabajo-35.png)

![](img/Jornada_-_Teletrabajo-36.png)

![](img/Jornada_-_Teletrabajo-37.png)

![](img/Jornada_-_Teletrabajo-38.png)

![](img/Jornada_-_Teletrabajo-39.png)

![](img/Jornada_-_Teletrabajo-40.png)

![](img/Jornada_-_Teletrabajo-41.png)

![](img/Jornada_-_Teletrabajo-42.png)

![](img/Jornada_-_Teletrabajo-43.png)

![](img/Jornada_-_Teletrabajo-44.png)

![](img/Jornada_-_Teletrabajo-45.png)

![](img/Jornada_-_Teletrabajo-46.png)

![](img/Jornada_-_Teletrabajo-47.png)

![](img/Jornada_-_Teletrabajo-48.png)

![](img/Jornada_-_Teletrabajo-49.png)

![](img/Jornada_-_Teletrabajo-50.png)

![](img/Jornada_-_Teletrabajo-51.png)

![](img/Jornada_-_Teletrabajo-52.png)

![](img/Jornada_-_Teletrabajo-53.png)

![](img/Jornada_-_Teletrabajo-54.png)

![](img/Jornada_-_Teletrabajo-55.png)

![](img/Jornada_-_Teletrabajo-56.png)

![](img/Jornada_-_Teletrabajo-57.png)

![](img/Jornada_-_Teletrabajo-58.png)

![](img/Jornada_-_Teletrabajo-59.png)

![](img/Jornada_-_Teletrabajo-60.png)

![](img/Jornada_-_Teletrabajo-61.png)

![](img/Jornada_-_Teletrabajo-62.png)

![](img/Jornada_-_Teletrabajo-63.png)

![](img/Jornada_-_Teletrabajo-64.png)

![](img/Jornada_-_Teletrabajo-65.png)

![](img/Jornada_-_Teletrabajo-66.png)

![](img/Jornada_-_Teletrabajo-67.png)

![](img/Jornada_-_Teletrabajo-68.png)