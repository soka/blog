---
title: Mastodon API
subtitle: Cliente Node.js para Mastodon (Parte I)
date: 2018-11-20
draft: false
description: Como usar un cliente Node.js para accedera a la API
tags: ["Aplicaciones", "Node.js","Mastodon","Programación","JavaScript","Clientes","Electron","API"]
---

Me he vuelto un poco loco y he decidido montar un mini prototipo de un cliente gráfico de escritorio con [Electron](https://soka.gitlab.io/electron/) y usando una librería Node.js interactuar con la [API de **Mastodon**](https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md).

Existe una página dedicada a las [librerias](https://github.com/tootsuite/documentation/blob/master/Using-the-API/Libraries.md) en diferentes lenguajes de programación para interactuar con **Mastodon**. Entre todas he elegido una basada en algún lenguaje del que tenga una mínima noción (PHP, Node, JS, R, C#), si además quiero usarla en una app de ventanas basada en [**Electron**](https://soka.gitlab.io/electron/) las opciones se reducen, finalmente me ha convencido [**Mastodon** API](https://github.com/vanita5/mastodon-api) ya que su documentación está bastante elaborada.

![](images/mastodon-api-github.PNG)

# Instalación Mastodon API

Con el gestor de paquetes NPM

```
npm install --save mastodon-api
```

# Autorización 

Consultamos el ejemplo [ examples/authorization.js](https://github.com/vanita5/mastodon-api/blob/master/examples/authorization.js) en el repositorio de GitHub del proyecto.

Ejecución: `node authorization.js`.

![](images/mastodon-api-01.PNG)

Si copiamos la última URL y la introducimos en el navegador nos dirige a una Web donde debemos autorizar el acceso de la app:

![](images/authorization-01.PNG) 

Si aceptamos nos proporciona un código de autorización que volvemos a pegar en la aplicación de consola `authorization.js`.

Finalmente nos proporciona un token de acceso, en la cadena 'xxx..." metemos el código de authorización de acceso a nuestra app en la Web de **Mastodon**, la cadena 'yy..." es generada por nuestra app.

```
C:\>node authorization.js
Please save 'id', 'client_id' and 'client_secret' in your program and use it from now on!
{ id: '376947',
  name: 'mastodon-node',
  website: null,
  redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
  client_id: 'c5d61b232a39786afdee4c08edd4469d63e1f0794c271077492cd61c91a2858f',
  client_secret: 'd8decc6d8e87f6ee2635126be220be74f9f1464b8f2fe17acaae016ea106ce43' }
This is the authorization URL. Open it in your browser and authorize with your account!
https://mastodon.social/oauth/authorize?redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&response_type=code&client_id=c5d61b232a39786afdee4c08edd4469d63e1f0794c271077492cd61c91a2858f&scope=read%20write%20follow
Please enter the code from the website: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
This is the access token. Save it!
yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
```

Ahora si consultamos la configuración en **Mastodon** entre las aplicaciones autorizadas debería figurar una nueva como `mastodon-node`:

![](images/authorized-apps-mastodon.PNG)

# Fuente authorization.js

He comentado la importación de las librerias en la cabecera y añadido notación actual sino falla ejecución:

```
const readline = require("readline");
//import readline from 'readline'

const Mastodon = require("mastodon-api");
//import Mastodon from '../lib/mastodon'

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

let clientId
let clientSecret

Mastodon.createOAuthApp()
    .catch(err => console.error(err))
    .then((res) => {
        console.log('Please save \'id\', \'client_id\' and \'client_secret\' in your program and use it from now on!')
        console.log(res)

        clientId = res.client_id
        clientSecret = res.client_secret

        return Mastodon.getAuthorizationUrl(clientId, clientSecret)
    })
    .then(url => {
        console.log('This is the authorization URL. Open it in your browser and authorize with your account!')
        console.log(url)
        return new Promise((resolve) => {
            rl.question('Please enter the code from the website: ', code => {
                resolve(code)
                rl.close()
            })
        })
    })
    .then(code => Mastodon.getAccessToken(clientId, clientSecret, code))
    .catch(err => console.error(err))
    .then(accessToken => {
        console.log(`This is the access token. Save it!\n${accessToken}`)
    })
```

# Enlaces externos

* ["Getting an access_token with the oauth package"](https://github.com/jhayley/node-mastodon/wiki/Getting-an-access_token-with-the-oauth-package).
* ["Programación en Electron - Introducción básica"](https://soka.gitlab.io/blog/post/2018-10-31-electron-programacion-basica/).
* ["Electron"](https://soka.gitlab.io/electron/).
