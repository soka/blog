---
title: Breve historia de Trello (III)
subtitle: Hitos y referencias sobre su nacimiento y evolución
date: 2019-07-02
draft: false
description: Aplicación
tags: ["Trello", "Talleres", "Cursos", "Aplicaciones", "Trello - Introducción"]
---

![](img/logo-trello.png)

## 2011 - Fog Creek anuncia Trello en TechCrunch

Hace 8 años [**Joel Spolsky**](http://www.crunchbase.com/person/joel-spolsky-2) desarrollador y cofundador de **Fog Creek** (["Fog Creek is now Glitch!"](https://glitch.com/about/fog-creek-is-now-glitch/)) anuncia que se libera Trello durante un evento de [TechCrunch](https://en.wikipedia.org/wiki/TechCrunch) (foro sobre tecnologías disruptivas y empresas emergentes) en San Francisco, se resume como una plataforma para gestionar flujos de trabajo (_workflows_) y gestionar listas de tareas (_todo lists_) (["Joel Spolsky’s Trello Is A Simple Workflow And List Manager For Groups"](https://techcrunch.com/2011/09/13/joel-spolskys-trello-is-a-simple-workflow-and-list-manager-for-groups/) - TechCrunch ).

![](img/01.jpg)

[[Fuente](https://techcrunch.com/2011/09/13/joel-spolskys-trello-is-a-simple-workflow-and-list-manager-for-groups/)]

Según **Spolsky** uno de lo mayores retos de una compañía es realizar un seguimiento de los trabajos en curso y conocer en que está trabajando cada persona.

El aplicativo Web está diseñado para ofrecer un sistema central donde los equipos de trabajo puedan repartir sus tareas y realizar un seguimiento de su evolución de forma colaborativa.

Trello centraliza el trabajo en tableros o _boards_ donde los usuarios organizados en grupos pueden crear listas de tareas a realizar (_todo lists_) y asignarse trabajos entre sí.

Como otras aplicaciones de gestión de flujos de trabajo **Trello** es visual, el aplicativo funciona en tiempo real y se actualiza en el navegador Web sin necesidad de recargar la página creando una comunicación eficiente.

**Trello** tiene características especificamente ideadas para negocios y organizaciones, se anuncia como completamente gratuito.

Se distribuie como aplicativo Web y para dispositivos iOS.

![](img/06.PNG)

Ese mismo año [Wired Magazine](https://www.wired.com) lo define como una de las 7 startups más prometedoras (["The 7 Coolest Startups You Haven't Heard of Yet"](https://www.wired.com/2011/09/startups-techcrunch-disrupt/)).

> _"Trello - Trello is a simple, powerful and free tool for team collaboration from well-known programmer/entrepreneur Joel Spolsky at Fog Creek Software. Projects are broken down into "cards", which can be assigned to team members, with to-do lists on each."_

> _"Team members can see the entire board, keeping an eye on who is working on what, and what the progress of the project is. It's akin to Basecamp and Pivotal Tracker, both online services popular with software teams, but Trello's intended to be useful for all sorts of project management, from class projects to running a company. Expect this to be widely popular as its free features are hard to pass up and premium features are expected in the future."_

## Sobre Joel Spolsky

["Joel Spolsky"](https://www.linkedin.com/in/joelspolsky/) además de ser cofundador de Trello también es CEO del popular sitio [Stack Overflow](https://stackoverflow.com/) que es una de las mayores comunidades de desarrolladores donde se plantean dudas y se resuelven problemas.

![](img/05.PNG)

## 2012 Trello llega a Android

En 2012 se lanzó la [versión para Android en Google Play Store](https://play.google.com/store/apps/details?id=com.trello).

![](img/07.PNG)

[[Fuente](https://blog.trello.com/trello-android-app-available-for-download)]

¡Ya son 500.000 usuarios los que usan Trello!

## 2014 Trello Inc

En julio de 2014, Trello se separa de Fog Creek y se convierte en Trello, Inc. Asimismo, se nombra al cofundador de Fog Creek, [Michael Pryor](https://www.linkedin.com/in/michaelhpryor), su director ejecutivo. La empresa gana 10,3 millones en una ronda de financiación (["Acerca de Trello"](https://trello.com/about)).

Trello tiene más de 4,75 millones de usuarios.

## 2017 Atlassian adquiere Trello

![](img/08.png)

En Junio de 2017 [Atlassian](https://es.atlassian.com/) adquiere Trello por 425 millones de dolares.

En los 5 años que han transcurrido desde su lanzamiento los Smartphones han cambiado la forma en que trabajamos y como nos comunicamos, la cultura de trabajo está evolucionado de las reuniones y el cara a cara hacia herramientas digitales colaborativas que permiten a los equipos de trabajo trabajar desde cualquier lugar y en cualquier momento.

**Enlaces**:

- Pryor, Michael. ["Trello Is Being Acquired By Atlassian"](https://blog.trello.com/trello-atlassian).Retrieved February 2, 2017.
- Lardinois, Frederic (January 9, 2017). ["Atlassian acquires Trello for 425M"](https://techcrunch.com/2017/01/09/atlassian-acquires-trello/). TechCrunch. AOL. Retrieved January 9, 2017.
- ["Atlassian + Trello: changing the way teams work"](https://www.atlassian.com/blog/2017/01/atlassian-plus-trello). JANUARY 9, 2017. MIKE CANNON-BROOKES.

## Material adicional

El siguiente video a pesar de que no tiene que ver con Trello es una parodia de como usamos aún hoy el correo electrónico para comunicarnos.

{{< youtube id="HTgYHHKs0Zw" autoplay="true" >}}

[[Fuente](https://www.youtube.com/watch?v=HTgYHHKs0Zw)]
