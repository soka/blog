---
title: Vim - Múltiples ventanas y pestañas
subtitle:  Trucos Vim
date: 2022-07-11
draft: false
description: Trucos Vim
tags: ["Vim","Vim - Múltiples ventanas","Vim - Ventanas","Vim - Pestañas","Vim - Windows","Vim - Tabs"]
---

## Abrir múltiples ventanas desde la línea de comandos

```bash
$ vim -o file1 file2
```

## Abrir múltiples ventanas sin salir de Vim

El comando `:split` seguido del nombre del archivo. Para abrir una ventana separada verticalmente:

```
:vsplit file2
```

Sino especificamos el nombre del fichero abre el actual dos veces.

Resumen de comandos para trabajar con ventanas:

![](img/01.png)

## Moverse a través de las ventanas

```
CTRL-W {Navigation key – j, k, h, l}
```

![](img/02.png)

![](img/03.png)

## Pestañas

Ademas de abrir múltiples ventanas podemos trabajar con varias pestañas.

```
:tabnew filename
```

Para cerrar la pestaña actual usamos `:tabclose`, para cerrar todas excepto la actual `:tabonly`.

Navegación por pestañas:

* :tabs         list all tabs including their displayed windows
* :tabm 0       move current tab to first
* :tabm         move current tab to last
* :tabm {i}     move current tab to position i+1
* :tabm +{i}    move current tab right to current position+i
* :tabm -{i}    move current tab left to current position-i
* :tabn         go to next tab
* :tabp         go to previous tab
* :tabfirst     go to first tab
* :tablast      go to last tab

## Enlaces internos


## Enlaces externos

* [https://webdevetc.com/blog/tabs-in-vim/](https://webdevetc.com/blog/tabs-in-vim/) 
* [https://vim.fandom.com/wiki/Using_tab_pages](https://vim.fandom.com/wiki/Using_tab_pages) 
