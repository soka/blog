---
title: Instalación del rol de servidor DHCP
subtitle: Windows Server 2012
date: 2019-11-09
draft: false
description: Recursos Windows Server
tags: ["Windows","Sistemas","Windows Server","Windows Server 2012","DHCP"]
---

Un servidor DHCP (Dynamic Host Configuration Protocol) permite **desplegar de forma rápida una red local** y **modificar de forma global una serie de parámetros de red** (son raros lo casos de una red empresarial o red local que no implemente este servicio), el servidor DHCP provee una IP y una máscara (y otros parámetros adicionales si así lo deseamos) a cualquier dispositivo de la red local que lo solicite.

DHCP se usa sobre todo en los equipos de trabajo. Los servidores, NAS o impresoras pueden tener un IP fija para hacer referencia a ellos de forma más fácil.

## Instalación

En la administración del servidor desde el panel de control principal debemos agregar un nuevo rol y seguir los pasos para seleccionar la opción "Servidor DHCP", a parte del rol instalará las herramientas necesarias para administrarlo (**recordatorio**: El servidor debería tener un IP estática asignada previamente).

![](img/01.png)

Cuando finalice el asistente de instalación tendremos un nuevo elemento en el menú lateral del administrador del servidor.

![](img/02.png)

Desde el menú herramientas podemos acceder a la consola DHCP:

![](img/03.png)

Usando PS:

```ps
Install-WindowsFeature DHCP
```

Herramienta de administración:

```ps
Install-WindowsFeature RSAT-DHCP
```

**Si el servidor que hospeda DHCP forma parte de un bosque Active Directory, debe estar, además, autorizado para los administradores miembros del grupo Administradores de empresas y aquellos que hayan recibido los permisos de administración de DHCP.**

## Configuración

Una vez instalado no hace nada, debemos **configurar un ámbito para que reparta direcciones IP dentro de un rango de forma automática**. Para comprobar que funciona correctamente necesito montar otra máquina virtual con Windows 7, 8 o 10 por ejemplo. Yo me he descargado una ISO de evaluación de Windows 10.

De momento voy a crear la configuración más básica que es ámbito, botón derecho del ratón sobre IPv4 y selecciono "Ámbito nuevo".

![](img/04.png)

Defino un nombre para el ámbito y una descripción, a continuación el rango de IPs dinámicas:

![](img/05.png)

En la siguiente pantalla podemos excluir IPs dentro del rango si sabemos que ya están ocupadas, incluso podemos hacer que el servidor DHCP se retrase un poco en asignar las direcciones. 

![](img/06.png)

Las direcciones IP se entregan en "alquiler", podemos definir el tiempo que pueden tener esa concesión, si tenemos muchos equipos móviles o conexiones nuevas normalmente se recomienda una duración corta. Lo dejo por defecto en 8 días.

Cuando el servidor DHCP entrega la IP puede también fijar otros parámetros como la puerta de enlace, el nombre de dominio, servidores DNS o la configuración [WINS](https://es.wikipedia.org/wiki/Windows_Internet_Naming_Service), selecciono que sí:

Puerta de enlace:

![](img/07.png)

Servidores DNS, por defecto aparece el que tiene el propio servidor (en mi la puerta de enlace), podemos añadir nuevos, los proveedores de Internet suelen ofrecer los suyos propios, añado los de Google como ejemplo:

![](img/08.png)

En el dominio primario especificamos el dominio de la empresa (Ejemplo: "curso.com").

Los servidores WINS ya no se usan, no defino nada en la siguiente pantalla (se recomienda no utilizar servidores WINS, salvo en caso necesario).

Al final activo la configuración, puede ver el nuevo ámbito:

![](img/09.png)

Una vez añadido aún podemos cambiar opciones, botón derecho del ratón "Opciones del ámbito" -> "Configurar opciones", una de las opciones es por ejemplo añadir un nombre de dominio DNS o la puerta de enlace (enrutador).

![](img/10.png)

En la carpeta concesión de direcciones puedo observar que aún no ha entregado ninguna, arranco mi MV con Win10 y con DHCP configurado y automáticamente ya tengo mi primera IP asignada ("Concesiones de direcciones").

Comandos útiles en el cliente:

```
ipconfig /release
ipconfig /renew
ipconfig /all
```

Compruebo que el ya tengo varias máquinas dadas de alta:

![](img/12.png)

Si queremos podemos añadir una reserva para que una máquina concreta siempre tenga la misma IP, usando el botón derecho del ratón sobre la máquina selecciono la opción "Añadir a reserva". Las reservas resultan muy útiles para las impresoras de red, los dispositivos administrables (tales como switch...) y, en ocasiones, para ciertas máquinas cuya dirección IP se autorice y utilice en la configuración
de alguna aplicación.

Puede ser interesante jugar con álgunos parámetros globales de IPv4, con el botón de derecho del ratón abrimos las propiedades.

![](img/15.png)

Se puede definir el número de intentos que realiza el servidor para comprobar si una IP está libre antes de condecerla a un cliente. También podemos "forzar" la actualización de los registros DNS del cliente.

El registro de la auditoria se almacena en C:\Windows\system32\dhcp.

## Comandos de utilidad

El comando [Netsh](https://docs.microsoft.com/es-es/windows-server/networking/technologies/netsh/netsh-contexts) permite cambiar la configuración de la red, se puede volcar la configuración a un fichero externo:

```ps
NETSH DHCP SERVER DUMP > DHCPCONFIG.CMD
```

Para usar PS se debe cargar previamente el módulo correspondiente `import-module dhcpserver`, para enumerar los componentes del módulo uso `get-command -module dhcpserver`. En este enlace ["Bringing PowerShell to DHCP Server"](https://blogs.technet.microsoft.com/teamdhcp/2012/07/15/bringing-powershell-to-dhcp-server/) se pueden ver algunos ejemplos prácticos.

## Directivas

Las directivas permiten configurar de manera particular los dispositivos, por ejemplo asignando un rango IP dependiendo de criterios como la clase `Vendor`, la clase `User`, la dirección MAC (lo más sencillo), etc. Usando las directivas adecuadas se pueden gestionar de forma diferente teléfonos IP, impresoras, portátiles, PCs de sobremesa, etc.

Creo una nueva directiva con un nombre cualquiera "Directiva01" y le agrego la MAC de un cliente:

![](img/16.png)

Selecciono los parámetros de directiva especiales para esas MAC, por ejemplo puertas de enlace diferentes (enrutadores).

El PS se puede obtener la MAC con `Get-NetAdapter`.

## Monitor de rendimiento

Abro el monitor de rendimiento (_perfomance monitor_):

![](img/11.png)

Añado nuevos contadores "Servidor DHCP":

![](img/13.png)

El visor de eventos (_event viewer_) también proporciona información muy valiosa, dentro "Registros de aplicaciones y servicios" > "Microsoft" > "Windows" > "DHCP-Server" abrimos la tercera opción "Microsoft-Windows-DHCP Server Events/Operativo":

![](img/14.png)

Por ejemplo el último evento es la asignación de IP a un cliente.

