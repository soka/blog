---
title: Sección desplegable con Divi en WordPress
subtitle: Diseñar capas ocultas con Divi
date: 2021-04-13
draft: true
description: 
tags: ["Divi","WordPress","CSS","Diseño","JS","JQuery"]
---

<!-- vscode-markdown-toc -->
* 1. [BOTÓN](#BOTN)
* 2. [CAPA OCULTA CON TEXTO](#CAPAOCULTACONTEXTO)
* 3. [CÓDIGO](#CDIGO)
* 4. [ENLACES EXTERNOS](#ENLACESEXTERNOS)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Necesito algún mecánismo para desplegar una capa de contenidos cuando el usuario interactúa con un botón, para ilustrar el ejemplo he creado un botón que al pulsarlo muestra un módulo oculto, si se vuelve a pulsar vuelva a ocultarlo.

##  1. <a name='BOTN'></a>BOTÓN

La URL del enlace del botón será "#". El disparador del evento está en el control del botón, en las propiedades del control añado dos clases CSS nuevas:

![](img/01.png)

##  2. <a name='CAPAOCULTACONTEXTO'></a>CAPA OCULTA CON TEXTO

Ahora añado un control de tipo texto, le pongo como ID CSS "newhere" que usaré luego con JQuery para referenciar el control.

![](img/02.png)


##  3. <a name='CDIGO'></a>CÓDIGO

Ahora añado un módulo de tipo código:

![](img/03.png)

Dentro del código hay dos secciones, la primera para definir el CSS y la segunda el código JS.

En el CSS defino dos subclases 

```js
<style type="text/css">
.nh_button.closed:after {content:"\33";}
.nh_button.opened:after{content:"\32";}
</style>

<script type="text/javascript">
	jQuery(document).ready(function() {
		// Hide the div
		jQuery('#newhere').hide();
		jQuery('.nh_button').click(function(e){
			e.preventDefault();jQuery("#newhere").slideToggle();
			jQuery('.nh_button').toggleClass('opened closed');
		});
	});
</script>
```

##  4. <a name='ENLACESEXTERNOS'></a>ENLACES EXTERNOS

- [https://www.creaweb2b.com/en/custom-toggle-section-in-divi/](https://www.creaweb2b.com/en/custom-toggle-section-in-divi/).
- [https://medium.com/insider-inc-engineering/delightful-ways-to-write-reusable-css-using-subclasses-903e90c9cf87](https://medium.com/insider-inc-engineering/delightful-ways-to-write-reusable-css-using-subclasses-903e90c9cf87).
- [https://divinotes.com/reveal-a-hidden-divi-section-row-or-module-on-button-click/?cn-reloaded=1](https://divinotes.com/reveal-a-hidden-divi-section-row-or-module-on-button-click/?cn-reloaded=1).
- [https://getme.marketing/divi-theme-tips/how-to-create-collapsible-modules-sections-rows-using-divi/](https://getme.marketing/divi-theme-tips/how-to-create-collapsible-modules-sections-rows-using-divi/).
- [https://www.elegantthemes.com/documentation/divi/cta/](https://www.elegantthemes.com/documentation/divi/cta/).
- [https://diviwebsiteexpert.com/how-to-create-divi-social-share-buttons/](https://diviwebsiteexpert.com/how-to-create-divi-social-share-buttons/).
- [https://www.b3multimedia.ie/how-to-create-a-monarch-shortcode/](https://www.b3multimedia.ie/how-to-create-a-monarch-shortcode/).