---
title: Docker
subtitle: Introducción
date: 2019-04-23
draft: false
description: Despliegue de aplicaciones y contenedores software
tags: ["Docker", "Aplicaciones"]
---

![](images/docker_logo.png)

## Descarga e instalación

Para sistemas MS Win se puede descargar “Docker for Windows Installer.exe” de este [enlace](https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe).

![](images/docker-web-download-01.PNG)

Anteriormente **Docker** se apoyaba en VBox (Virtual Box), en las versiones más recientes **Docker** se ejecuta de forma nativa usando la característica de virtualización Hyper-V de Microsoft. Desde el administrador de Hyper-V podemos configurar todas las propiedades de la máquina virtual, memoria RAM que puede consumir, los interfaces de red y otros parámetros.

![](images/hyper-v-01.PNG)

Una vez instalado desde el área de notificación de Windows indica si **Docker** está en ejecución y podemos acceder a la configuración.

![](images/docker-desktop-01.PNG)

Docker se ejecuta como un servicio de Windows.

## Comandos básicos con PowerShell

Si todo ha ido bien ahora ya podemos abrir una consola PowerShell y comenzar a interactuar con **Docker**.

Muestra información sobre nuestra instalación de Docker:

```PS
PS C:\> docker info
```

Muestra las versiones de **Docker** cliente / servidor, **Docker** está basado en el lenguaje de programación [Go](<https://es.wikipedia.org/wiki/Go_(lenguaje_de_programaci%C3%B3n)) de Google.

```PS
PS C:\> docker version
Client: Docker Engine - Community
 Version:           18.09.2
 API version:       1.39
 Go version:        go1.10.8
 Git commit:        6247962
 Built:             Sun Feb 10 04:12:31 2019
 OS/Arch:           windows/amd64
 Experimental:      false

Server: Docker Engine - Community
 Engine:
  Version:          18.09.2
  API version:      1.39 (minimum version 1.12)
  Go version:       go1.10.6
  Git commit:       6247962
  Built:            Sun Feb 10 04:13:06 2019
  OS/Arch:          linux/amd64
  Experimental:     false
```

Ahora listamos las imágenes que tenemos instaladas (también podemos hacerlo con `docker images`):

```PS
PS C:\> docker image ls
REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
gitlab/gitlab-ee           latest              7e535337db84        11 days ago         1.98GB
r-base                     latest              62c848eeb175        3 weeks ago         649MB
hello-world                latest              fce289e99eb9        3 months ago        1.84kB
docker4w/nsenter-dockerd   latest              2f1c802f322f        6 months ago        187kB
```

## Docker Kitematic

[**Kitematic**](https://kitematic.com/) es un gestor gráfico de Docker ideal para aquellas personas que no se apañan con la línea de comandos, no está incluido por defecto con **Docker Desktop** pero facilita su descarga ofreciendo una opción de menú para ello.

Para sistemas MS Win se recomienda instalarlo en 'C:\Program Files\Docker\Kitematik'.

![](images/kitematik-01.PNG)

En mi MS Win 10 lo debo ejecutar como admin y aún así no detecta la instalación nativa de Docker, tampoco de Virtual Box que usaba anteriormente (actualmente usa Hyper-V para MS Win) (["Common Issues and Fixes"](https://github.com/docker/kitematic/wiki/Common-Issues-and-Fixes)).

## Contenedores vs imágenes

**Imágenes**:

- Plantilla de solo lectura vacía o con una aplicación preinstalada para la creación de contenedores.
- Creadas por nosotros o por la comunidad ([Docker Hub](https://hub.docker.com/)) ([Docker search](https://hub.docker.com/search/?type=image)).

**Contenedores**:

- Contiene todo lo necesario para ejecutar nuestras aplicaciones.
- Basado en una o más imágenes.

En resumen las imágenes son la base para crear nuestros contenedores.

![](images/contenedor-vs-imagen-01.PNG)

## Imágenes en Docker Hub y Docker Search

¿De donde nos descargamos las imágenes? Para eso existe [**Docker Hub**](https://hub.docker.com/) con repositorios con infinidad de imágenes que podemos descargar. Por ejemplo si buscamos por la palabra clave "Ubuntu" tenemos a nuestra disposición 62.421 resultados.

![](images/docker-hub-ubuntu-search.PNG)

Podemos hacer lo mismo desde la línea de comandos con `docker search`.

```R
PS C:\Users\i.landajuela> docker search ubuntu
NAME                                                   DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
ubuntu                                                 Ubuntu is a Debian-based Linux operating sys…   9439                [OK]
dorowu/ubuntu-desktop-lxde-vnc                         Docker image to provide HTML5 VNC interface …   293                                     [OK]
rastasheep/ubuntu-sshd                                 Dockerized SSH service, built on top of offi…   212                                     [OK]
consol/ubuntu-xfce-vnc                                 Ubuntu container with "headless" VNC session…   173                                     [OK]
ubuntu-upstart                                         Upstart is an event-based replacement for th…   97                  [OK]
ansible/ubuntu14.04-ansible                            Ubuntu 14.04 LTS with ansible                   96                                      [OK]
neurodebian                                            NeuroDebian provides neuroscience research s…   56                  [OK]
1and1internet/ubuntu-16-nginx-php-phpmyadmin-mysql-5   ubuntu-16-nginx-php-phpmyadmin-mysql-5          50                                      [OK]
ubuntu-debootstrap                                     debootstrap --variant=minbase --components=m…   40                  [OK]
nuagebec/ubuntu                                        Simple always updated Ubuntu docker images w…   23                                      [OK]
i386/ubuntu                                            Ubuntu is a Debian-based Linux operating sys…   17
ppc64le/ubuntu                                         Ubuntu is a Debian-based Linux operating sys…   13
1and1internet/ubuntu-16-apache-php-7.0                 ubuntu-16-apache-php-7.0                        13                                      [OK]
eclipse/ubuntu_jdk8                                    Ubuntu, JDK8, Maven 3, git, curl, nmap, mc, …   10                                      [OK]
darksheer/ubuntu                                       Base Ubuntu Image -- Updated hourly             5                                       [OK]
codenvy/ubuntu_jdk8                                    Ubuntu, JDK8, Maven 3, git, curl, nmap, mc, …   5                                       [OK]
pivotaldata/ubuntu                                     A quick freshening-up of the base Ubuntu doc…   2
smartentry/ubuntu                                      ubuntu with smartentry                          1                                       [OK]
paasmule/bosh-tools-ubuntu                             Ubuntu based bosh-cli                           1                                       [OK]
1and1internet/ubuntu-16-sshd                           ubuntu-16-sshd                                  1                                       [OK]
1and1internet/ubuntu-16-php-7.1                        ubuntu-16-php-7.1                               1                                       [OK]
ossobv/ubuntu                                          Custom ubuntu image from scratch (based on o…   0
pivotaldata/ubuntu-gpdb-dev                            Ubuntu images for GPDB development              0
1and1internet/ubuntu-16-healthcheck                    ubuntu-16-healthcheck                           0                                       [OK]
pivotaldata/ubuntu16.04-build                          Ubuntu 16.04 image for GPDB compilation         0
```

Si por ejemplo queremos descargar la primera de la lista usamos el comando `pull`:

```PS
PS C:\> docker pull ubuntu
```

Ahora si ejecutamos `docker images` ya podemos ver la imagen de Ubuntu recien descargada. Los [tags](https://hub.docker.com/_/ubuntu?tab=tags) asociados a las imágenes permiten categorizar y buscar con mayor detalle, por ejemplo:

```PS
PS C:\> docker search ubuntu:latest
PS C:\> docker search ubuntu:14.04
```

También podemos aplicar los tags al comando `pull`:

```PS
PS C:\> docker pull ubuntu:14.04
```

## Mi primer container Hello-World

El contenedor [hello-world](https://hub.docker.com/_/hello-world) es usado para comprobar que **Docker** está corriendo y funciona:

![](images/hello-world.PNG)

```R
docker pull hello-world
```

Ahora ejecutamos nuestro contenedor:

```R
PS C:\> docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/

PS C:\>
```

Lo único que hace es mostrar un mensaje por pantalla. Si queremos hacer algo más ambicioso podemos lanzar un terminal Bash de Linux/Ubuntu con `docker run -it ubuntu bash`.

Como podemos saber los contenedores que tenemos en ejecución:

```PS
PS C:\> docker ps
```

No muestra el contenedor hello-world ya que lo único que hace es ejecutarse y finalizar. Si usamos el parámetro `-a` muestra el estado de ejecución de cada contenedor.

```PS
PS C:\> docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS
62868a1003f4        ubuntu              "bash"              4 minutes ago       Exited (0) 2 minutes ago
35f3bc3b7835        hello-world         "/hello"            12 minutes ago      Exited (0) 12 minutes ago
05ca82432084        hello-world         "/hello"            40 hours ago        Exited (0) 40 hours ago
396f71112737        hello-world         "/hello"            40 hours ago        Exited (0) 40 hours ago
```

## Ejecutando comandos en contenedores con Docker Run

Ejecutamos el comando `ls` sobre el contenedor "ubuntu":

```R
PS C:\> docker run ubuntu ls
bin
boot
dev
etc
home
lib
lib64
media
mnt
opt
proc
root
run
sbin
srv
sys
tmp
usr
var
```

Inmediatamente después de ejecutar el comando `ls` finaliza la ejecución. ¿Como hacemos que se ejecute indefinidamente?

```R
PS C:\> docker run -i -t ubuntu bash
root@2dcfbb7e06b9:/#
```

La opción `-i` indica que queremos que sea interactivo, la opción `-t` reserva un emulador TTY de entrada / salida de texto.

Una vez dentro ya podemos ejecutar cualquier comando de Linux. Si abrimos una nueva terminal de PowerShell podemos comprobar que el contenedor está en ejecución:

![](images/docker-ps.PNG)

## Crear contenedores interactivos en Docker

Ejecutamos el contenedor "ubuntu" con un terminal interactivo:

```PS
PS C:\> docker run -ti ubuntu
```

Después salimos del Bash de Ubuntu con `exit`, si ejecutamos `docker ps` ninguna imagen está en ejecución, si hacemos `docker ps -a` y obtenemos el ID podemos volver a arrancarlo:

```PS
PS C:\> docker start 384431ce115c
```

Usamos el parámetro `attach` para entrar nuevamente en el Bash:

```R
PS C:\> docker attach 384431ce115c
root@384431ce115c:/#
```

Probamos a crear un fichero una vez dentro con `touch test.txt`, cuando salimos y volvemos a entrar el fichero sigue en su lugar.

Podemos nombrar los contenedores:

```PS
PS C:\> docker run --name miubuntu -ti ubuntu
```

## Iniciar o detener contenedores

Listar las imágenes:

```PS
PS C:\> docker images
```

Listar los contenedores que ya están creados:

```PS
PS C:\> docker ps -a
```

Crear un contenedor:

```PS
PS C:\> docker run -ti ubuntu
```

Con CTRL+P+Q nos salimos del contenedor sin detenerlo.

```PS
PS C:\> docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS              PORTS               NAMES
849c88b8925d        ubuntu              "/bin/bash"         About a minute ago   Up About a minute                       mystifying_clarkes
```

Ahora detemos el contenedor:

```PS
PS C:\> docker stop 849c88b8925d
849c88b8925d
```

Si volvemos a ejecutar `docker ps` vemos que ya se ha detenido.

Volvemos a arrancarlo:

```PS
PS C:\> docker start 849c88b8925d
849c88b8925d
```

Nos conectamos al contenedor:

```PS
PS C:\> docker attach 849c88b8925d
root@849c88b8925d:/#
```

## Personalizar containers

Seguimos con la imagen previa de Ubuntu, si habeis probado algunos comandos como `ifconfig` vereis que no lo reconoce. Lo primero es actualizar los paquetes con `apt-get update`.

Ahora ya podemos seguir instalando paquetes, por ejemplo mi editor de consola preferido `apt-get install vim`.

## Crear una imagen a partir de un contenedor con Docker commit

Voy a crear una imagen a partir de un contenedor ya modificado y así poder seguir desplegando nuevos contenedores. Seguimos personalizando nuestro contenedor de Ubuntu, instalando aplicaciones y creando carpetas y ficheros.

Salimos del contenedor sin detenerlo (CTRL+P+Q), ahora usamos el comando `commit` para crear la imagén referenciando por el ID, le damos un nombre a la nueva imagen.

```PS
PS C:\> docker commit 849c88b8925d ubuntu-iker
PS C:\> docker images
REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
ubuntu-iker                latest              6ef0d3c27e2f        33 seconds ago      174MB
gitlab/gitlab-ee           latest              7e535337db84        13 days ago         1.98GB
r-base                     latest              62c848eeb175        4 weeks ago         649MB
ubuntu                     latest              94e814e2efa8        6 weeks ago         88.9MB
hello-world                latest              fce289e99eb9        3 months ago        1.84kB
docker4w/nsenter-dockerd   latest              2f1c802f322f        6 months ago        187kB
```

Ahora ya podemos crear un contenedor de nuestra imagén:

```PS
PS C:\> docker run -it ubuntu-iker
root@f4f1a658c2e5:/#
```

Seguimos con el ejemplo anterior, ahora vamos a instalar Apache:

```PS
root@f4f1a658c2e5:/# apt-get update
root@f4f1a658c2e5:/# apt-get install apache2
```

Arrancamos el servicio:

```PS
root@f4f1a658c2e5:/# service apache2 start
root@f4f1a658c2e5:/# service apache2 status
```

Ahora si quisiesemos crear una nueva imagén con `commit` deberiamos usar un _Dockerfile_ para crear una imagén a medida, en este caso en lugar de arrancar el Bash queremos que cuando se cree el contenedor automaticámente se ejecute Apache.

Por el momento lo haremos pasandole una serie de parámetros para crear la imagén "apache2" usando el parámetro `--change` le indicamos que ejecute el comando apache2ctl para arrancar el servidor Web, con `EXPOSE` le decimos que exponga el puerto 80.

```PS
PS C:\> docker commit --change='CMD ["apache2ctl","-D","FOREGROUND"]' -c "EXPOSE 85" f4f1a658c2e5 apache2
sha256:f31023ec48932ee6554ead0b5a64ac69a9fa7f831a26249fe9c2824147a743d6
```

Verificamos que se ha creado la nueva imagén:

```PS
PS C:\> docker images
REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
apache2                    latest              f31023ec4893        2 minutes ago       264MB
ubuntu-iker                latest              6ef0d3c27e2f        13 hours ago        174MB
gitlab/gitlab-ee           latest              7e535337db84        13 days ago         1.98GB
r-base                     latest              62c848eeb175        4 weeks ago         649MB
ubuntu                     latest              94e814e2efa8        6 weeks ago         88.9MB
hello-world                latest              fce289e99eb9        3 months ago        1.84kB
docker4w/nsenter-dockerd   latest              2f1c802f322f        6 months ago        187kB
```

Arrancamos un nuevo contenedor:

```PS
PS C:\> docker run -d -p 5000:80 apache2
```

**ERROR**: Por alguna razón arranca, se para y no puedo visualizar http://localhost:5000.

## Crear una imagen con un DockerFile

Es más facil crear una imagen con un fichero DockerFile que usando el commit.


## Un poco de limpieza, borrado

En MS Win10 el disco duro virtual que usa Hyper-V está en "C:\Users\Public\Documents\Hyper-V\Virtual hard disks\MobyLinuxVM.vhdx", este disco duro virtual aloja las imágenes y contenedores, después de hacer pruebas es recomendable hacer un limpieza para no quedarnos sin espacio en el disco duro. También puede ser recomendable guardar el disco duro virtual en otra partición o disco independiente.

**Borra un contenedor** en ejecución:

```PS
PS C:\> docker rm --force ubuntu
```

## Enlaces internos

- ["Contenedores Docker con R"](https://soka.gitlab.io/blog/post/2019-04-13-docker-contenedor-r/).

## Enlaces externos

- ["Command-Line Interfaces (CLIs)"](https://docs.docker.com/engine/reference/commandline/info/).
- [https://docs.docker.com/docker-for-windows/](https://docs.docker.com/docker-for-windows/).
