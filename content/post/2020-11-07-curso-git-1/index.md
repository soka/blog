---
title: Control de versiones con Git
subtitle: Sesión nº 1 curso Git
date: 2020-11-07
draft: false
description: Control de versiones con Git y GitLab
tags: ["Cursos","Curso Git","Código","Control Versiones","GitLab","Git","SCM"]
---

![](img/01.png)

## Control de versiones con Git

[Git](https://git-scm.com/) es un sistema distribuido de [control de versiones](https://es.wikipedia.org/wiki/Control_de_versiones) (SCM - Source Code Management) gratuito y libre bajo la licencia [GNU General Public License version 2.0](https://opensource.org/licenses/GPL-2.0).

## Terminología básica

### Repositorio

El repositorio es el lugar en el que se almacenan los datos actualizados e históricos de cambios. Existen repositorios remotos (_origin_) alojados en Internet en un servidor como GitLab o GitHub o en un servidor personal. El repositorio local en nuestro equipo es un subdirectorio `.git` que contiene todos los archivos necesarios.

### Revisión o version

Una revisión es una versión determinada de la información que se gestiona. Git identifica las revisiones mediante un código de detección de modificaciones con [SHA1](https://es.wikipedia.org/wiki/SHA1). A la última versión se le suele identificar de forma especial con el nombre de **HEAD**. Para marcar una revisión concreta se usan los **rótulos o tags**.

Una suma de comprobación o [_hash_](https://es.wikipedia.org/wiki/Funci%C3%B3n_hash) tiene como entrada un conjunto de elementos, que suelen ser cadenas, y los convierte en un rango de salida finito, normalmente cadenas de longitud fija. SHA-1 produce una salida resumen de 160 bits (20 bytes), prueba con otros como `md5sum` con [MD5](https://es.wikipedia.org/wiki/MD5):

```bash
$ sha1sum README.md 
900b32de056a34638f800528f2619234aeba3e62  README.md
```

Para obtener el _hash_ del último __commit_ en Git podemos hacer:

```bash
$ git rev-parse HEAD
093931d59864bf09541c7e364899ca390721df75
```

_Hash_ SHA256 de las distribuciones [ISO de Linux Mint v20](https://mirrors.evowise.com/linuxmint/stable/20/sha256sum.txt) para verificar la integridad y autenticidad de la descarga:

![](img/08.png)

### Etiquetado

Las etiquetas son referencias que apuntan a puntos específicos en el historial de Git. Generalmente, el etiquetado se usa para capturar un punto en el historial que se utiliza para una publicación de versión marcada, por ejemplo, v1.0.1, una [_release_](https://es.wikipedia.org/wiki/Release_Management) o entrega de software. Se usa el comando [`git tag`](https://git-scm.com/docs/git-tag).

### Publicar en local

El comando [`git commit`](https://git-scm.com/docs/git-commit) captura una instantánea de los cambios preparados en ese momento del proyecto (incluye los cambios en el HEAD, pero aún no en tu repositorio remoto). Antes de la ejecución de git commit, se utiliza el comando git add para pasar o "preparar" los cambios en el proyecto que se almacenarán. Estos dos comandos, git commit y git add, son dos de los que se utilizan más frecuentemente.

![](img/06.png)

## Actualizar repositorio remoto

El comando [`git push`](https://git-scm.com/docs/git-push) se usa para cargar contenido del repositorio local a un repositorio remoto. El envío es la forma de transferir commits desde tu repositorio local a un repositorio remoto.

![](img/07.png)

### Branch o rama

Un módulo puede ser **branched** o bifurcado en un instante de tiempo de forma que, **desde ese momento en adelante se tienen dos copias (ramas) que evolucionan de forma independiente** siguiendo su propia línea de desarrollo. El módulo tiene entonces 2 (o más) "ramas". La ventaja es que se puede hacer un "merge" de las modificaciones de ambas ramas, posibilitando la creación de "ramas de prueba" que contengan código para evaluación, si se decide que las modificaciones realizadas en la "rama de prueba" sean preservadas, se hace un "merge" con la rama principal. Son motivos habituales para la creación de ramas la creación de nuevas funcionalidades o la corrección de errores.
- **Integración o fusión ("merge")**: Una integración o fusión une dos conjuntos de cambios sobre un fichero o un conjunto de ficheros en una revisión unificada de dicho fichero o ficheros. 

![](img/03.png)

## Instalación

```bash
$ sudo apt install git
```

Comprobar la versión:

```bash
$ git --version
git version 2.20.1
```

## Configuración inicial

Git viene acompañado de [`git config`](https://git-scm.com/docs/git-config) que permite configurar la aplicación. 

```bash
$ man git config
```

La configuración se puede almacenar en sitios diferentes:

1. `/etc/gitconfig`: Contiene los valores para todos los usuarios del sistema y para todos los repositorios. Se usa con el parámetro `--system`.
2. `~/.gitconfig` o `~/.config/git/config`: Específico del usuario con el parámetro `--global`.
3. `.git/config` dentro del propio repositorio con `--local`.

```bash
popu@diablo:~/Documentos/GitLab/penascalf52021$ cat .git/config 
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
[remote "origin"]
	url = https://gitlab.com/soka/penascalf52021.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
```

Cada nivel tiene prioridad sobre las variables definidas en el anterior, lo que quiere decir que los valores en `.git/config` tienen más peso que los de `/etc/gitconfig`. 

## Tu identidad

```bash
$ git config --global user.name "soka"
$ git config --global user.email ikernaix@gmail.com
```

Lo comprobamos en el archivo:

```bash
$ cat /home/popu/.gitconfig 
[user]
	name = soka
	email = ikernaix@gmail.com
```

## Define tu editor preferido

Podemos definir VSCode como nuestro editor (por ejemplo para los _commits_):

```bash
$ git config --global core.editor "code --wait"
```

Ahora puedes ejecutar `git config --global -e` para editar la configuración con VSCode. 

Para ver las diferencias entre versiones añado al fichero de configuración:

```bash
[diff]
    tool = default-difftool
[difftool "default-difftool"]
    cmd = code --wait --diff $LOCAL $REMOTE
```

Ahora lo probamos [`git difftool`](https://git-scm.com/docs/git-difftool).

Por línea de comandos explicitado VSCode: `git difftool -x "code --wait --diff"`.

Hacemos lo mismo para los _merge_:

```bash
[merge]
    tool = code
[mergetool "code"]
    cmd = code --wait $MERGED
```	

## Revisando los ajustes

Muestra los ajustes de los tres sitios de configuración:

```bash
$ git config --list
user.name=soka
user.email=ikernaix@gmail.com
core.editor=code --wait
diff.tool=default-difftool
difftool.default-difftool.cmd=code --wait --diff $LOCAL $REMOTE
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
remote.origin.url=https://gitlab.com/soka/penascalf52021.git
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.master.remote=origin
branch.master.merge=refs/heads/master
```

También se puede comprobar cada valor:

```bash
$ git config user.name
soka
```

## Obteniendo ayuda

```bash
$ git help <verb>
$ git <verb> --help
$ man git-<verb>
```

Por ejemplo:

```bash
$ git help clone
```

No olvides que en la Web de Git puede encontrar una excelente documentación en [https://git-scm.com/doc](https://git-scm.com/doc).

## Enlaces externos

- [https://git-scm.com/](https://git-scm.com/).
- [https://git-scm.com/docs/](https://git-scm.com/docs/): La documentación oficial.
- [https://www.atlassian.com/es/git/tutorials](https://www.atlassian.com/es/git/tutorials): En castellano y muy bien explicados los comandos.
- [https://es.wikipedia.org/wiki/Control_de_versiones](https://es.wikipedia.org/wiki/Control_de_versiones).
- [https://code.visualstudio.com/docs/editor/versioncontrol](https://code.visualstudio.com/docs/editor/versioncontrol).
- [https://medium.com/faun/using-vscode-as-git-mergetool-and-difftool-2e241123abe7](https://medium.com/faun/using-vscode-as-git-mergetool-and-difftool-2e241123abe7).

## Autor

Iker Landajuela 

## Licencia

[GNU Free Documentation License Version 1.3](https://www.gnu.org/licenses/fdl-1.3-standalone.html).