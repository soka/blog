---
title: Servidor de alojamiento Nextcloud con Docker
subtitle: Tu servidor de almacenamiento con Docker.
date: 2019-09-08
draft: false
description: Pon en marcha un contenedor con Docker
tags: ["Docker", "Aplicaciones", "NextCloud","Debian"]
---

A raíz de este enlace ["EU turns from American public clouds to Nextcloud private clouds"](https://www.zdnet.com/article/eu-turns-from-american-public-clouds-to-nextcloud-private-clouds/) se ha reavivado mi interés en montar una nube privada en casa, hace tiempo hice algunas pruebas con Owncloud sobre Virtual Box (ver posts [[1]](https://ikerlandajuela.wordpress.com/2018/02/25/owncloud-tu-nube-privada-de-datos/) y [[2]](https://ikerlandajuela.wordpress.com/2015/04/19/owncloud-nuestra-nube-privada/)), sin embargo usar Docker para desplegar la aplicación tiene grandes ventajas, se pone en marcha un contenedor en pocos minutos, respecto a las máquinas virtuales convencionales consume menos recursos y es fácilmente portable.

Nextcloud puede ser una alternativa interesante para implantar en el trabajo y compartir la información entre delegaciones y trabajadores. Cuando lo tenga claro tal vez lo proponga.

## Instalación de Docker en Debian 10

![](images/docker_facebook_share.png)

Siguiendo los pasos de este artículo ["How To Install and Use Docker on Debian 10"](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-debian-10) he instalado Docker sin mayores contratiempos, aquí resumo los comandos: 

```bash
# apt update
# apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
# curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
# add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
# apt update
# apt-cache policy docker-ce
# apt install docker-ce
# systemctl status docker

# docker -v
Docker version 19.03.2, build 6a30dfc
```

## Contenedor Docker Nextcloud

Nextcloud es un servicio de almacenamiento de datos con interfaz Web, te lo puedes instalar en tu propio servidor para montar tu nube privada.

![](images/índice.png)

Entre sus funcionalidades más destacadas:

- Extensible mediante aplicaciones que aportan funcionalidades extendidas, mención especial al conjunto de aplicaciones dirigidas al trabajo colaborativo entre usuarios (calendario, una especie de tablero Kanban, contactos...)
- Entorno multiusuario y colaborativo.
- Fácil de compartir entre usuarios con cuenta en el servidor o mediante enlaces.
- Cliente de escritorio multiplataforma y para smartphones.

Descargo la imagen:

```bash
# docker pull nextcloud
```

Arranco el contenedor:

```bash
# docker run -d -p 8080:80 nextcloud
```

Si abrimos el enlace [http://localhost:8080/](http://localhost:8080/) ya podemos comenzar el proceso de instalación definiendo una cuenta de administrador:

![](images/01.png)

![](images/02.png)

Los archivos del servidor y los datos se almacenan en /var/lib/docker/volumes/, cada usuario tendrá una carpeta con su nombre de usuario donde se almacenan sus ficheros.

## Cliente de escritorio

El cliente de escritorio permite mantener sincronizados los datos de la computadora local con el servidor de Nextcloud de forma automática.

```bash
# apt install nextcloud-desktop
```

En el momento de escribir este post el cliente gráfico se cierra de forma inesperada, estoy consultando algunos foros, por el momento si lo ejecuto con GDB funciona.

![](images/03.png)

## QOwnNotes

Mi próximo paso es usar un editor de textos MarkDown para tomar notas y que se sincronice con mi cuenta en Nextcloud, con toda probabilidad optaré por [QOwnNotes](https://www.qownnotes.org/) porqué ofrece integración con Nextcloud ([enlace](https://www.qownnotes.org/installation) instrucciones de instalación), alguna la vez lo probé y me gusto como funciona, [Joplin](https://joplinapp.org/) parece también una alternativa interesante.

![](images/QOwnNotes-Main-Screen-Linux.png)

## Enlaces internos

[Todos mis posts sobre Docker](https://soka.gitlab.io/blog/tags/docker/).

## Enlaces externos

- [https://hub.docker.com/_/nextcloud/](https://hub.docker.com/_/nextcloud/).
- [https://docs.nextcloud.com/server/16/admin_manual/](https://docs.nextcloud.com/server/16/admin_manual/).
- [https://apps.nextcloud.com/](https://apps.nextcloud.com/).
- [https://nextcloud.com/devices/](https://nextcloud.com/devices/).