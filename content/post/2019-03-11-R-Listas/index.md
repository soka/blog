---
title: Listas en R
subtitle: Tipos de datos
date: 2019-03-11
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico", "R - Datos"]
---

![](images/r-logo.PNG)

La diferencia entre un vector y una lista es la siguiente, generalmente en un vector los datos que contiene son todos del mismo tipo, sin embargo una lista te permite juntar elementos de diferentes tipos.

Vamos a crear nuestra primera lista:

```R
> lst <- list(nombre='Pedro',esposa='Maria',num.hijos=3,edad.hijos=c(12,18,25))
> lst
$nombre
[1] "Pedro"

$esposa
[1] "Maria"

$num.hijos
[1] 3

$edad.hijos
[1] 12 18 25

> # Indexamos el primer elemento por su posición
> lst[[1]]
[1] "Pedro"
> # Por el nombre
> lst$nombre
[1] "Pedro"
> # Equivalente por indexación
> lst[[4]][2]
[1] 18
```

Si observamos la captura de abajo, `lst` es de tipo `list` de 4 elementos, nombre es un array de cadenas de _character_ de un elemento, `edad.hijos` es un array de 4 elementos de tipo _double_ (valores reales en doble precisión u 8 bytes).

```R
> typeof(lst$edad.hijos)
[1] "double"
```

![](images/listas-01.PNG)

Para crear una lista usamos la función `list()`. Vamos a crear una lista combinando diferentes tipos de objectos que ya conocemos, vectores y matrices.

```R
v1 <- c(1:10)
mx1 <- matrix(1:9,nrow = 3, ncol = 3)
v2 <- c("Hola","Mundo")
l1 <- list(v1,mx1,v2)
```

Podemos acceder a los elementos por su índice, en las listas la indexación se hace con dobles corchetes.

```R
> l1[3]
[[1]]
[1] "Hola"  "Mundo"
```

## Concatenación de listas

```R
l2 <- list(apellidos="Marin",peso=78)
lst3 <- c(lst,l2)
```

## Listado de listas

```R
lst1 <- list(1,2,3)
lst2 <- list(4,5,6)
lst.total <- list(lst1,lst2)
```

Si queremos acceder a un elemento, accedemos a la segunda lista primero elemento

```R
> lst.total[[2]][[1]]
[1] 4
```

![](images/listas-02.PNG)

## Ejercicios

- r / intro / 21-Listas / [listas.R](https://gitlab.com/soka/r/blob/master/intro/21-Listas/listas.R).
