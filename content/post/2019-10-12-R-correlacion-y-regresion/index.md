---
title: Correlación y regresión en R [borrador]
subtitle: Diferencias entre ambas y ejemplos básicos en R
date: 2019-10-12
draft: true
description: Ciencia de datos con R
tags: ["R","R - Básico","R - Estadística"]
---

![](images/diff.png)

[[Fuente](https://www.datasciencecentral.com/profiles/blogs/difference-between-correlation-and-regression-in-one-picture)]

## Correlación de Pearson

Se llama [**correlación**]((https://es.wikipedia.org/wiki/Correlaci%C3%B3n)) al grado de dependencia mutua entre dos variables, el coeficiente de correlación intenta **medir la fuerza o intensidad y la dirección** de una **relación lineal** entre dos variables estadísticas.

Se considera que dos variables cuantitativas están correlacionadas cuando los valores de una de ellas varían sistemáticamente con respecto a los valores de la otra variable (no implica por si misma ninguna relación de [causalidad](https://es.wikipedia.org/wiki/Causalidad_(filosof%C3%ADa)) ["Cum hoc ergo propter hoc"](https://es.wikipedia.org/wiki/Cum_hoc_ergo_propter_hoc)).  

```r
> xdata <- c(2,4.4,3,3,2,2.2,2,4)
> ydata <- c(1,4.4,1,3,2,2.2,2,7)
> cov(xdata,ydata) # Covarianza
[1] 1.479286
> cov(xdata,ydata)/(sd(xdata)*sd(ydata)) # Correlación calculada
[1] 0.7713962
> cor(xdata,ydata) # Correlación
[1] 0.7713962
> plot(xdata,ydata,
+      pch=13, # pch = 13, forma de los puntos
+      cex=1.5) # cex : tamaño de los puntos
```

La nube de puntos resultante:

![](images/01.png)

Usando `ggplot` sería algo así:

```r
install.packages("ggplot2")
library(ggplot2)
df <- data.frame(xdata,ydata)
names(df) <- c("x","y")
ggplot(data = df) +
  geom_point(mapping = aes(x = x,y = y))
```

El coeficiente de correlación estima la naturaleza de la una relación lineal entre  los dos conjuntos de observaciones, si observamos el patrón de puntos del diagrama debemos imaginar la línea recta que representa todos los puntos, podemos determinar la fuerza de la asociación lineal basada en lo cerca que están los puntos de la línea, **los puntos más cercanos a la línea tendrán un valor de coeficiente de correlación cercano a -1 o 1**. La dirección está determinada por la pendiente de la línea, si la pendiente crece de izquierda a derecha indica una correlación positiva, una tendencia negativa se muestra como una pendiente en descenso hacia la derecha. Tomando esto en consideración la correlación estimada para el diagrama de arriba tiene bastante sentido, parece  que el valor de ambas observaciones crece al mismo tiempo.

Otro ejemplo, el _dataset_ `quakes` recoge datos de terremotos en la isla de Fiji desde 1964, creamos un _scatter plot_ con dos variables, la magnitud de cada evento y el número de estaciones de observaciones que lo detectaron.

```r
plot(quakes$mag,quakes$stations,
     xlab="Magnitud",ylab="Nº de estaciones")
```

La correlación positiva es evidente a simple vista, eventos de mayor magnitud son detectados por un número mayor de estaciones de medición.

![](images/02.png)

Aplicando la función `cor` se demuestra que la asociación linear entre ambas variables es fuerte:

```r
> cor(quakes$mag,quakes$stations)
[1] 0.8511824
```

Un ejemplo de correlación negativa usando el _dataset_ `mtcars` que contiene 32 observaciones (modelos de vehículos) de 11 variables numéricas, buscamos la correlación entre los caballos del motor y el tiempo en recorrer un cuarto de milla.

```r
plot(mtcars$hp,mtcars$qsec,xlab="Gross horsepower",ylab = "1/4 mile time")
```

Observando el diagrama se puede deducir que a mayor potencia menos tarda en recorrer un distancia:

![](images/03.png)

Y así es:

```r
> cor(mtcars$hp,mtcars$qsec)
[1] -0.7082234
```

![](https://image.jimcdn.com/app/cms/image/transf/none/path/s075f076504dfea8d/image/i5b4d913fcf35fde8/version/1397760391/image.png)

[[Fuente](https://www.ingenieriaindustrialonline.com/herramientas-para-el-ingeniero-industrial/pron%C3%B3stico-de-ventas/regresi%C3%B3n-lineal/	)] 


Valor del Coeficiente de Pearson | Grado de Correlación entre las Variables
---------------------------------|------------------------------------------
r = 0     						 |  no existe relación lineal. Pero esto no necesariamente implica que las variables son independientes: pueden existir todavía relaciones no lineales entre las dos variables.
r = 1							 | existe una correlación positiva perfecta. El índice indica una dependencia total entre las dos variables denominada relación directa: cuando una de ellas aumenta, la otra también lo hace en proporción constante.
0 < r < 1						 | correlación positiva
r = 0							 | no existe relación lineal. Pero esto no necesariamente implica que las variables son independientes: pueden existir todavía relaciones no lineales entre las dos variables
-1 < r < 0						 | existe una correlación negativa
r = -1							 | existe una correlación negativa perfecta. El índice indica una dependencia total entre las dos variables llamada relación inversa: cuando una de ellas aumenta, la otra disminuye en proporción constante

## Curva de regresión

El concepto de correlación está directamente relacionado con el concepto de **curva de regresión**, mediante la regresión simple mínimo cuadrática se expresa como la curva mejor posible que se ajusta a los pares de observaciones de dos variables (minimizando la varianza del error).

![](images/formula_basica_regresion_lineal_01.PNG) 

Para entender mejor el modelo de regesión simple y darle un aspecto práctico usaremos un **ejemplo** muy común en internet, **buscamos la relación entre el precio medio de una vivienda y el número de habitaciones** por hogar, dibujamos una gráfica con los diferentes pisos en venta, donde el eje "X" representa el número de habitaciones y el eje "Y" a su vez el precio, cada punto representa una vivienda en diferentes barrios de la ciudad. Logicamente existe una relación entre el número de habitaciones y el precio de la vivienda, a mayor número de habitaciones en el eje "X" el precio sube en el eje "Y".  

El [**diagrama de dispersión**](https://es.wikipedia.org/wiki/Diagrama_de_dispersi%C3%B3n) (nube de puntos) resultante podría tener este aspecto por ejemplo:

![](images/grafica_regresion_linea_simple_01.PNG)

Ahora se trata de hallar la recta que mejor "encaje" con el conjunto de puntos dados. Para esto podremos usar métodos como el de los [**"mínimos cuadrados"**](https://es.wikipedia.org/wiki/M%C3%ADnimos_cuadrados) que busca minimizar la distancia vertical de todos los puntos a la línea.

![](images/grafica_regresion_linea_simple_02.PNG) 

Una vez obtenida esta línea, seremos capaces de hacer predicciones hipotéticas sobre cuál sería el valor de "Y" (precio de la vivienda) dado "X" (número de habitaciones). 

![](images/formula_basica_regresion_lineal_01.PNG)

La [recta](https://es.wikipedia.org/wiki/Recta) (una sucesión de puntos en la misma dirección) se puede reducir a una formula (la forma simplificada) mediante la ecuación de arriba (es un polinomio de primer grado) y se representa gráficamente como una línea recta en un plano en [coordenadas Cartesianas](https://es.wikipedia.org/wiki/Coordenadas_cartesianas)**. Partiendo de una serie de observaciones "X" e "Y" se busca una línea recta que describa cada uno de los pares, en este caso la variable "X" es la independiente, regresiva o predictora y "Y" es la respuesta para cada valor específico de "Xi".

La descripción de la formula matemática para una recta es la siguiente.

El **termino independiente o secante** "b" muestra a que altura la recta corta el eje "Y". 

La variable "m" es la **pendiente que graficamente define la inclinación de la recta respecto al eje "X" y conceptualmente la relación entre la variable "X" y la variable de salida "Y"**, es el cambio esperado en "Y" por cada incremento unitario en "X", conforme "m", la pendiente, aumenta, la recta se hace más empinada. Cuando el valor absoluto de "m" se acerca a cero, la pendiente se aplana. Para determinar la pendiende de una recta se requiere de sólo dos coordenadas o puntos.

![](images/grafica_regresion_linea_simple_03.PNG) 

Es facil comprenderlo usando números enteros, he encontrado este [enlace](https://www.desmos.com/calculator) que permite definir una formular y visualizarla gráficamente.

Ahora **debemos buscar la recta que satisfaga los criterios anteriores**. Uno de los métodos es el de [**mínimos cuadrados ordinarios**](https://es.wikipedia.org/wiki/M%C3%ADnimos_cuadrados) (atribuido a Gauss).

Rescatando el diagrama de dispersión previo, para un punto "X" nuestra recta predice un valor de "Y" pero sin embargo el valor real de nuestros datos es otro, esa distancia representa el error, debemos aplicar este criterio para todos los datos de nuestro modelo y calcular la media.

![](images/grafica_regresion_linea_simple_error_01.PNG)

Si nos fijamos lo que estamos haciendo al elevar al cuadrado el error es calcular el área de coste para cada punto y realizar la media, jugando con el termino independiente y la pendiente de la función de la función de la recta debemos buscar la combinación de parámetros que minimize esta suma de cuadrados.

En siguiente ejemplo dibujo la nube de puntos y a continuación calculo la recta de regresión líneal con [`lm`](https://www.rdocumentation.org/packages/stats/versions/3.6.1/topics/lm), utilizo la función [`abline`](https://www.rdocumentation.org/packages/graphics/versions/3.6.1/topics/abline) para dibujar la recta.

```r
xdata <- c(2,4.4,3,3,2,2.2,2,4)
ydata <- c(1,4.4,1,3,2,2.2,2,7)
plot(xdata,ydata,pch=13,cex=1.5)
modelo_regresion = lm(xdata ~ ydata)
abline(modelo_regresion, col = "red")
```

![](images/04.png)

## Código fuente

- r / intro / 56-correlacion-regresion / [correlacion-regresion.R](https://gitlab.com/soka/r/blob/master/intro/56-correlacion-regresion/correlacion-regresion.R).


## Enlaces internos

- ["Diagramas de dispersión y modelo de regresión líneal en R"](https://soka.gitlab.io/blog/post/2019-02-05-diagramas-dispersion-en-r/).

## Enlaces externos

- datasciencecentral.com ["Difference Between Correlation and Regression in One Picture"](https://www.datasciencecentral.com/profiles/blogs/difference-between-correlation-and-regression-in-one-picture) - Posted by Stephanie Glen on October 10. Fuente de la imagen de cabecera de este post.
