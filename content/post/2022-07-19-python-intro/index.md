---
title: Python 
subtitle: Primeros pasos, preparación del entorno y herramientas
date: 2022-07-19
draft: false
description: Plugin para compartir contenidos
tags: ["Cursos","Talleres","PyCharm","Visual Studio Code","Python","Python - Introducción","Python - Instalación","Python - Entorno"]
---

![](img/Python_logo-01.png)

## Instalación

Usando el gestor de paquetes APT (disposnible en Debian, Ubuntu, Mint y distros derivadas) sólo hay que introducir `sudo apt install python`. 

Para comprobar que la instalación es correcta y la versión que tenemos instalada usamos `python -V` (o lo que es lo mismo `python --version`):

```bash
$ python -V
Python 2.7.16
```

En mi sistema conviven Python 2 y 3, puedo usar el interprete de la versión 3 con `python3`.  
r ejemplo `python3 -V` (3.7.3). Para usar siempre la versión 3 reemplazo el enlace con `sudo ln -s /usr/bin/python3.7 /usr/bin/python`.

## Interprete de comandos

Podemos acceder al interprete de Python simplemente introduciendo `python` (`quit()` para salir).

## PyCharm CE

[PyCharm](https://www.jetbrains.com/es-es/pycharm/) es un IDE para desarrolladores de Python, se puede instalar de dos maneras, usando Snap o descargando la versión 

### Instalación con Snap

Instalo la versión CE (Community Edition gratuita) usando Snap:

```bash
$ sudo snap install pycharm-community --classic
```

Ahora ya puedo ejecutarlo con este comando `pycharm-community`.

Para desinstalar `sudo snap remove pycharm-community`.

**Nota**: Debo revisarlo porque la ejecución no abre el IDE.

### Usando el instalador

Descargo "pycharm-community-2020.2.3.tar.gz" de la Web oficial en la carpeta Descargas. 

Descomprimo el archivo tar gz y sigo las siguientes instrucciones:

```bash
tar -xzf pycharm-community-2020.2.3.tar.gz
cd pycharm-community-2020.2.3
cd bin
chmod u+x pycharm.sh
sh pycharm.sh
```

Después de algunas pantallas aceptando los términos de la licencia y permitiendo o no el envío de datos anónimos a JetBrains para mejorar la herramienta pasamos a configurar el IDE.

![](img/01.png)

¡Algunos ajustes opcionales más y ya está!

## Mi primer proyecto con PyCharm

Creo un nuevo proyecto:

![](img/02.png)

Voy a guardar mi primer proyecto "first-pycharm-prj" en la carpeta "Documentos/GitLab/python/basics" donde tengo un repositorio preparado (`git clone https://gitlab.com/soka/python.git`).

Borro todo el contenido de ejemplo en main.py y empiezo de cero escribiendo mi primera línea `print("Hello Python!")`,  ejecuto con el atajo "Mayús + F10".
## Python con Visual Studio Code

Instalo las siguientes extensiones:

- [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python): Linting, Debugging (multi-threaded, remote), Intellisense, Jupyter Notebooks, code formatting, refactoring, unit tests, snippets, and more..
- [Python Extension Pack](https://marketplace.visualstudio.com/items?itemName=donjayamanne.python-extension-pack).
- [Python for VSCode](https://marketplace.visualstudio.com/items?itemName=tht13.python).

Creo mi primer script de ejemplo:

```python
msg = "Hello Python!"
print(msg)
```

Abro la paleta de comandos de VSCode y selecciono "> Python: Run Python File in Terminal". También puedo hacer lo mismo dentro del fichero abierto con el botón derecho del ratón. Para abrir el interprete de Python uso en la paleta de comandos "Python: Start REPL".

Para depurar debo añadir una configuración para Python en el archivo launch.json (**Depurar | Agregar configuración**), automáticamente me añade todo lo necesario para depurar mi aplicación:

![](img/05.png)

## Repl.it 

[Repl](https://repl.it/) es un IDE basado en el navegador para multitud de lenguajes, entre ellos Python. Puede resultar de gran utilidad para probar _snippets_ y compartir código para un taller. Me he creado una cuenta [aquí](https://repl.it/@IkerLandajuela2/).

![](img/06.png)

## Vim y Python



## Enlaces externos

- [https://es.wikipedia.org/wiki/Python](https://es.wikipedia.org/wiki/Python).
- [https://www.python.org/](https://www.python.org/).
- [https://es.python.org/](https://es.python.org/): Python España.

**Visual Studio Code y Python**:

- [https://code.visualstudio.com/docs/python/python-tutorial](https://code.visualstudio.com/docs/python/python-tutorial).
- [https://code.visualstudio.com/docs/python/environments](https://code.visualstudio.com/docs/python/environments).

**PyCharm**:

- [https://www.jetbrains.com/es-es/pycharm/](https://www.jetbrains.com/es-es/pycharm/).
- [https://snapcraft.io/install/pycharm-community/debian](https://snapcraft.io/install/pycharm-community/debian).
- [https://www.jetbrains.com/help/pycharm/installation-guide.html#standalone](https://www.jetbrains.com/help/pycharm/installation-guide.html#standalone).
