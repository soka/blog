---
title: Mapeo automático de carpetas de Red usando GPO
subtitle: Administración de dominio con Windows Server 
date: 2021-07-09
draft: false
description: 
tags: ["Windows","GPO","Windows Server","Políticas","Red","Usuarios","Mapeo","Unidades de red"]
---

<!-- vscode-markdown-toc -->

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

En esta entrada voy a trabajar con el mapeo automático de una carpeta de red compartida previamente con un grupo de usuarios del dominio.

Se puede acceder a la gestión GPO desde el panel de "Administración del servidor" en el menú herramientas > Administración de directivas de grupo o simplemente ejecutando el siguiente comando: `gpmc.msc`.

![](img/01.PNG)

Una vez dentro vamos a crear una nueva política GPO en la carpeta "Objetos de directiva de grupo" con el botón derecho del ratón, la llamaremos "Mapeo de unidades".

Haciendo clic sobre la nueva política accedemos a los ajustes:

![](img/02.PNG)

**Vamos a aplicar esta política a un grupo de usuarios específico**, para eso dentro del apartado "Filtrado de seguridad" voy a quitar a los Usuarios autentificados. 

![](img/03.PNG)

Ahora voy a añadir el grupo que yo quiero con "Añadir". 

Ahora necesitamos editar los permisos de este grupo dentro de esta política. Dentro de la pestaña "Delegación" podemos ver los permisos que tienen usuarios y grupos para esta GPO, usamos la opción "Opciones avanzadas" para ver el nivel de permisos de cada usuario-grupo.

![](img/04.PNG)

Comprobamos que el grupo que acabamos de añadir tiene permisos de lectura y para aplicar la directiva de grupo.

Ahora falta aplicar esta política a nuestro dominio, para eso vamos a la carpeta superior con el dominio y haciendo clic sobre el con el botón derecho del ratón usamos la opción "Vincular un GPO existente". 

![](img/05.PNG)

Ahora vamos a realizar los ajustes de nuestra nueva política, volvemos sobre la política y seleccionamos editar en el menu contextual que aparece con el clic derecho del ratón. 

![](img/06.PNG)

Dentro vamos a Configuración de usuarios > Preferencias > Configuración de Windows > Asignaciones de unidades.

![](img/07.PNG)

Hacemos clic derecho y seleccionamos Nuevo > Unidad asignada.

Seleccionamos la opción Reemplazar para que en caso de que este creada la reemplaze, añadimos la ruta estilo "\\IP\recurso-compartido"  y marcamos reconectar para que se reconecte en caso de que se interrumpa. Añadimos una etiqueta que será el nombre que aparecerá la unidad mapeada en la maquina cliente. Marcamos que asigne la letra de unidad automaticamente empezando el sondeo de las que estan libres desde la A. 

![](img/08.PNG)

Nuestra nueva política ya está lista para ser desplegada, para aplicarla abro una venta de CMD y ejecuto `gpupdate /force`, lo hacemos también en la máquina cliente (nos pedirá cerrar y volver a entrar en la sesión). Podemos comprobar con usuarios que no están en el grupo que no les monta la unidad. 




