---
title: Iniciar sesión con PS en ServerCore
subtitle: PowerShell y Windows Server
date: 2019-11-30
draft: false
description: Administración servidores Windows
tags: ["Windows","Sistemas","PowerShell","Administración","cmdlets","Scripting"]
---

![](img/logo2.png)

## Deshabilitar interfaz gráfico

En cualquier momento se puede deshabilitar el sistema gráfico y trabajar sobre nuestro servidor exclusivamente usando la línea de comandos, para deshabilitar la interfaz gráfica:

```ps
Get-WindowsFeature -Name *gui* | Remove-WindowsFeature -Restart
```

Para volver a habilitarla sólo tengo que revertir el proceso:

```ps
Get-WindowsFeature -Name *gui* | Install-WindowsFeature -Restart
```

## Iniciar la sesión en PS

Hay que modificar un campo del registro, ejecutando regedit puedo administrar el registro de forma gráfica, en la ruta "HKEY_LOCAL_MACHINE" > "SOFTWARE" > "Microsoft"  > "Windows NT" > "CurrentVersion" > "WinLogon", edito la clave de registro "Shell", el valor por defecto es "explorer.exe", en su lugar usaré "powershell.exe", reinicio el equipo para aplicar los cambios `shutdown -r -t 0`.

![](img/01.png)

## Enlaces

- [How to switch between GUI and Core in Windows Server 2012 using PowerShell](https://www.techrepublic.com/article/how-to-switch-between-gui-and-core-in-windows-server-2012-using-powershell/).

