import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http'; // <----

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'httpget';
  totalAngularPackages; // <---
  //url: string = 'https://api.npms.io/v2/search?q=scope:angular'; // <---
  url: string = 'https://api.chucknorris.io/jokes/random';
  error;

  constructor(private http: HttpClient) { } // <---

  ngOnInit() {   // <---
    this.http.get<any>(this.url).subscribe(data => {
       this.totalAngularPackages = data.total;
    },error => this.error = error);
  }

  onSubmit() { // <----
    console.log("onSubmit(): this.totalAngularPackages: "+this.totalAngularPackages);
  }


}
