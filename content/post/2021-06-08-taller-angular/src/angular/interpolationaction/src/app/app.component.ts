import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'interpolationaction';
  nombre:string;

  onClickMe() {
    this.nombre = "Iker"; // Activamos interpolación a placer
  }

}
