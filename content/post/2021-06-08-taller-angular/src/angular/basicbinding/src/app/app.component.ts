import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  template: `
    <h1>{{ titulo }}</h1>
    <h2>Soy: {{ nombre }}</h2>
    {{ metodoComponente() }}
  `
})
export class AppComponent {
  titulo = "One way binding";
  nombre: string = "Iker";
  metodoComponente() {
    console.log("Metodo componente");
  }
}
