/**
 * CLASES JS Y TS
 */
class Speaker {
    private nombre:string=""; // Esto en JS no existe
    public pub:number=6;

    hola(nombre: string) {
        console.log("Hola "+nombre);
        this.nombre = nombre;
    }
}

// instancia de la clase (objeto)
let mySpeaker:Speaker = new Speaker();
let myName:string = "Iker";
mySpeaker.hola(myName);

//mySpeaker.nombre; // Property 'nombre' is private and only accessible within class 'Speaker'.
mySpeaker.pub; // OK! 


var person: {name: string};
person = {name: 'iker'};
person.name ='mikel';

// ================================================================
// HERENCIA
// ================================================================
class Persona {
    protected nombre:string;
    protected edad:number;

    constructor(nombre:string,edad:number) {
        this.nombre = nombre;
        this.edad = edad;
    }

    imprimir() {
        console.log("Nombre: "+this.nombre+" , edad: "+this.edad);    
    }
}

class Empleado extends Persona {
    private sueldo:number;
    
    constructor(nombre:string,edad:number,sueldo:number) {
        super(nombre,edad);
        this.sueldo = sueldo;
    }

    imprimir() {
        super.imprimir();
        console.log("Sueldo: "+this.sueldo);
    }
}

let myEmpleado:Empleado = new Empleado("Iker",42,2000);
myEmpleado.imprimir();



