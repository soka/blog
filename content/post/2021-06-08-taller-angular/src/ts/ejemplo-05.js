var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * CLASES JS Y TS
 */
var Speaker = /** @class */ (function () {
    function Speaker() {
        this.nombre = ""; // Esto en JS no existe
        this.pub = 6;
    }
    Speaker.prototype.hola = function (nombre) {
        console.log("Hola " + nombre);
        this.nombre = nombre;
    };
    return Speaker;
}());
// instancia de la clase (objeto)
var mySpeaker = new Speaker();
var myName = "Iker";
mySpeaker.hola(myName);
//mySpeaker.nombre; // Property 'nombre' is private and only accessible within class 'Speaker'.
mySpeaker.pub; // OK! 
// ================================================================
// HERENCIA
// ================================================================
var Persona = /** @class */ (function () {
    function Persona(nombre, edad) {
        this.nombre = nombre;
        this.edad = edad;
    }
    Persona.prototype.imprimir = function () {
        console.log("Nombre: " + this.nombre + " , edad: " + this.edad);
    };
    return Persona;
}());
var Empleado = /** @class */ (function (_super) {
    __extends(Empleado, _super);
    function Empleado(nombre, edad, sueldo) {
        var _this = _super.call(this, nombre, edad) || this;
        _this.sueldo = sueldo;
        return _this;
    }
    Empleado.prototype.imprimir = function () {
        _super.prototype.imprimir.call(this);
        console.log("Sueldo: " + this.sueldo);
    };
    return Empleado;
}(Persona));
var myEmpleado = new Empleado("Iker", 42, 2000);
myEmpleado.imprimir();
