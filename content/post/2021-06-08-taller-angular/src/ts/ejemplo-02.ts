/*
 * INFERENCIA DE TIPO
 * Asigna un tipo sin necesidad de escribirlo, se infiere de la variable asignada a partir de un análisis
 * estático del programa. 
 **/
var foo = "hello";
console.log(foo);
console.log(typeof foo); // string
foo = 5; // error TS2322: Type 'number' is not assignable to type 'string'.

