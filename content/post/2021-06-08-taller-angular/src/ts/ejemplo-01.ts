/*
 * VARIABLES Y TIPADO
 **/ 

//====================================================================
// TIPADO ESTÁTICO
//====================================================================
// En la mayoría de lenguajes que he conocido el tipo suele preceder el nombre de la variable.
// VENTAJAS:
//  - Otra ventaja es que ayuda en el IDE a autocompletar métodos y propiedades
//  - Detecta errores antes de ejecutar mediante análisis estático
let cadena:string;
cadena = "cadena prueba";
console.log(cadena);
// Produce error en análisis estático editor.
//cadena = 5; // Error: Type 'number' is not assignable to type 'string'

//====================================================================
// TIPADO DINÁMICO
//====================================================================
//var num = 5; // Si hacemos esto el IDE ya sabe que tipo de dato es, produce error al transpilar
// Así no produce error en transpilacion porque no hemos asignado la variable en la declaración
var num;
num = 5;
console.log(typeof num); // number
num = "kaka";
console.log(typeof num); // string

// Tipado estático de una función
function sum(a: number, b:number) {
  return a + b
}

console.log(sum(1, 2));

var aBoolean = false;
console.log(typeof aBoolean); // "boolean"

//Produde error en TS, tipado estático
//aBoolean = "Tom";
//console.log(typeof aBoolean); // "string"

//====================================================================
// ÁMBITO DE VARIABLES
//====================================================================

// Cuando definimos una variable con let sabemos que el alcance que tendrá está delimitado por el bloque donde se ha creado.
let i:boolean = true; 

if (i===true) {
    let a:number = 5
}
//  error TS2304: Cannot find name 'a'.
console.log(a);

//----------------------------------------------------------------------------------

// Con var es como una variable global
var cadena_nombre:string = "Iker";

function escribe_nombre() {
  console.log("Mi nombre es: "+cadena_nombre);
}

escribe_nombre();

//----------------------------------------------------------------------------------

// El acto de introducir una nueva variable de un bloque anidado con el mismo nombre que una variable de un bloque //// superior se llama shadowing (evitar como la peste salvo con causa justificada en contadas ocasiones)
let isOn:boolean = true; 

if (isOn===true) {
    var myNum:number = 5
}
console.log("Haciendo shawdowing es "+myNum);

// -----------------------------------------------------------------------------------
// En JS se puede prescindir de "var" en la declaración, eso la convierte en global, 
// una practica peligrosa ya que si la olvidamos se puede sobreescribir su valor fuera del ámbito

function suma(a,b) {
  total = a + b;
  return total;
}
total = 3;
console.log("total: " + total);




