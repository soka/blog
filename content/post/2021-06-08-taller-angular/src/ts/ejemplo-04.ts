/**
 *  FUNCIONES
 */
// Podemos usar la palabra reservada function o la sintaxis de flechas

// https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Functions
// VS
// https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#functions
function suma1(a,b) {
    return a+b;
}
console.log(suma1('2',34)); // al cachondeo! y el resultado es .... 234! WTF! 

// ============================================================================
//  FUNCIONES FLECHA
// ============================================================================
// Equivalente función flecha https://www.tutorialsteacher.com/typescript/arrow-function
let suma2 = (a,b) => a+b;
suma2(2,3);

// hasta ahora todo es igual que JS
// el principal cambio está en la declaración de tipos, volvemos a escribir la función suma...
function suma3(a:number,b:number) {
    return a+b;
}
console.log(suma3('2',34)); // te pille! Error en compilación

// Otro ejemplo....
function saluda_cabron(name: string) {
    console.log("Hello, " + name.toUpperCase() + "!!");
}
saluda_cabron("iker");

// Tipo del retorno!
function suma4(a:number,b:number): number {
    return a+b;
    //return 'kaka'; // vengaaaa mas errores en compilación
}
// Todos los argumentos son obligatorios a menos que especifiquemos lo contrario
//console.log(suma4(4)); // EN JS esto traga como una serpiente aunque retorna NaN

// ----------------------------------------------------------------------------------
// Los argumentos opcionales se pueden especoificar como sigue
function opcional(a?:string) {
    console.log("string="+a);
}
opcional(); // undefined
opcional("cadena");


