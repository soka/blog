/**
 * DECLARACIÓN DE TIPOS DE VARIABLES
 */
// https://www.typescriptlang.org/docs/handbook/2/everyday-types.html
//=========================================================================
// NOMBRES DE VARIABLES
//=========================================================================
// Los nombres de variables deben seguir las mismas reglas que en JS
//  - El 1º caracter debe ser: una letra, _ o $
//  - El resto pueden ser los mismos y digitos.
var v1 = 1;
var _v2 = 2;
var $v3 = 3;
let;
4;
v = 4; // error TS1351: An identifier or keyword cannot immediately follow a numeric literal.
// ===================  DECLARACIÓN =======================================
// La sintaxis es la siguiente
// let identificar: tipo 
// identificador hace referencia al nombre de la variable
// como en JS podemos usar let, var y const. Const por supuesto es para declarar constantes
// los tipos de datos primitivos son number, string y boolean
//-------------------------------------------------------------------------
// Cadenas
//-------------------------------------------------------------------------
var variable = 'uno';
console.log(typeof variable); // string
var cStr = new String("Kaixo mundua!");
console.log("Length: " + cStr.length); // 13
console.log("Typeof: " + typeof cStr); // object
// podemos usar const, var y let
var cte = 42; // el autoespista...
// El IDE ya nos avisa... Error!
// en JS sin embargo... es un var sin más :(
//cte = 100;
// diferencia entre let y var 
// https://victorroblesweb.es/2016/08/27/diferencia-let-var-es6-typescript/
// para iniciarse mejor let
/* ====================================================== */
// Tipos básicos: string, number y boolean
var OtraCadena;
var a, b;
a = 6;
b = 0xFF; // 2 Hex 4 bits = 1 octal o byte 8 bits = 0...255
console.log(a + b); // ??????????? = 261!
var isOn;
isOn = true;
console.log(!isOn); // Negarlo todo siempre
// ----------------------------------------------------------------------------------
// ARREGLOS ESTÁTICOS
// ----------------------------------------------------------------------------------
// contienen elementos del mismo tipo
// no especifico tamaño siempre que almacene cadenas en este caso
// https://www.tutorialspoint.com/typescript/typescript_arrays.htm
var arreglo;
arreglo = ["uno", "dos"];
console.log(arreglo[1]); // ¿cual imprime?
console.log(arreglo.length); // 2
console.log(arreglo[2]); // "tres"
// Declaración e inicialización
var nums = [1, 2, 3, 3];
// =============================================== ANY ====================================
// Entonces como decimos en TS que una variable puede ser de cualquier tipo como en JS?
var comodin;
console.log(typeof (comodin)); // pues eso: undefined
// aunque también podemos hacerlo así, any simplemente es un azucarillo sintáctico
var comodin1;
console.log(typeof (comodin1)); // undefined también! 
comodin = 1;
comodin = 'bla';
