/**
 * INTERFACES POO
 */
// Si la anotación de tipos se vuelve compleja se puede usar un interface para simplificar la 
// las interfaces son un mecanismo de la programación orientada a objetos que trata de suplir la carencia de herencia múltiple (una clase hereda de varias) de TS 

// Una clase puede extender otra clase, heredando sus propiedades y métodos y declarar que implementa cualquier número de interfaces. La diferencia de las clases que extiendes con respecto a las interfaces es que las interfaces no contienen implementación de sus métodos, por lo que la clase que implementa una interfaz debe escribir el código de todos los métodos que contiene. Por este motivo, se dice que las interfaces son como un contrato, en el que se especifica las cosas que debe contener una clase para que pueda implementar una interfaz o cumplir el contrato declarado por esa interfaz.

interface Person {
    name: string;
    altura: number;
}
// Como ves, solo hemos indicado los tipos y los métodos, pero no hemos indicado sus valores.

var myperson: Person = { name:'iker', altura:1.70 };

