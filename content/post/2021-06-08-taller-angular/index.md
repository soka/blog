---
title: Taller Angular
subtitle: Angular y TypeScript
date: 2021-06-08
draft: false
description: 
tags: ["Angular","TypeScript","Web","Desarrollo","Programación"]
---

<!-- vscode-markdown-toc -->
* 1. [JavaScript](#JavaScript)
* 2. [TypeScript](#TypeScript)
	* 2.1. [Recursos](#Recursos)
	* 2.2. [Características principales](#Caractersticasprincipales)
	* 2.3. [Instalación](#Instalacin)
	* 2.4. [Ejercicios de ejemplo](#Ejerciciosdeejemplo)
		* 2.4.1. [01 - Tipado estático de datos](#Tipadoestticodedatos)
		* 2.4.2. [02 - Inferencia de tipo](#Inferenciadetipo)
		* 2.4.3. [03 - Declaración de tipos de variables primitivas](#Declaracindetiposdevariablesprimitivas)
		* 2.4.4. [04 - Funciones](#Funciones)
		* 2.4.5. [05 - Encapsulación y herencia de clases](#Encapsulacinyherenciadeclases)
		* 2.4.6. [Interfaces](#Interfaces)
		* 2.4.7. [Módulos](#Mdulos)
* 3. [Angular](#Angular)
	* 3.1. [Instalar Angular CLI](#InstalarAngularCLI)
	* 3.2. [Mi primer proyecto](#Miprimerproyecto)
		* 3.2.1. [Creación del entorno de trabajo](#Creacindelentornodetrabajo)
		* 3.2.2. [Ejecutar la aplicación](#Ejecutarlaaplicacin)
		* 3.2.3. [Estructura de un proyecto](#Estructuradeunproyecto)
		* 3.2.4. [Realizando modificaciones](#Realizandomodificaciones)
	* 3.3. [Interpolación](#Interpolacin)
		* 3.3.1. [Interpolación de cadenas, del componente al DOM con {{}}](#InterpolacindecadenasdelcomponentealDOMcon)
		* 3.3.2. [Comunicación bidireccional entre vista y modelo](#Comunicacinbidireccionalentrevistaymodelo)
	* 3.4. [HTTP y servicios REST](#HTTPyserviciosREST)
		* 3.4.1. [ Requisitos para realizar una petición HTTP en Angular](#RequisitospararealizarunapeticinHTTPenAngular)
		* 3.4.2. [Llamada HTTP Get en el componente](#LlamadaHTTPGetenelcomponente)
		* 3.4.3. [Un botón en la vista para mostrar los resultados](#Unbotnenlavistaparamostrarlosresultados)
	* 3.5. [Routing](#Routing)
		* 3.5.1. [Creación de una app con enrutado habilitado](#Creacindeunaappconenrutadohabilitado)
		* 3.5.2. [Definiendo el menú de navegación](#Definiendoelmendenavegacin)
		* 3.5.3. [Crear componentes adicionales para la navegación](#Crearcomponentesadicionalesparalanavegacin)
		* 3.5.4. [Crear la lista de rutas disponibles](#Crearlalistaderutasdisponibles)
		* 3.5.5. [Recursos](#Recursos-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='JavaScript'></a>JavaScript

[JavaScript](https://es.wikipedia.org/wiki/JavaScript) (también conocido como ECMAScript) nace (1995) como un pequeño lenguaje interpretado para navegadores Netscape inicialmente.

JS  normalmente se ejecuta en el lado del cliente (interactúa con la página usando el Document Object Model (DOM)) para mejorar la UI o interfaces dinámicas, estaba pensado para incrustar pequeños trozos o snippets de código en una Web.

En 1997 se propone que se adopte como estándar por el ECMA (European Computer Manufacturers 'Association) con el nombre de ECMAScript.

Tabla de compatibilidad Node.js con ECMAScript en [https://node.green/](https://node.green/).

* [https://medium.com/javascript-scene/encapsulation-in-javascript-26be60e325b4](https://medium.com/javascript-scene/encapsulation-in-javascript-26be60e325b4).

![](img/02.png)

* Se define como **orientado a objeto** (?¿?).
* **Imperativo** porque ejecuta las instrucciones una a una, le dice cómo tiene que hacerlo VS declarativo que dice que tiene que hacer, por ejemplo HTML es un lenguaje declarativo, el lenguaje de consulta SQL también. 
* **Débilmente tipado**: asigna tipos internamente pero no es tan estricto como otros lenguajes ([**duck typing**](https://es.wikipedia.org/wiki/Duck_typing) is the ability of a language to determine types at runtime).

> "Cuando veo un ave que camina como un pato, nada como un pato y suena como un pato, a esa ave yo la llamo un pato"

##  2. <a name='TypeScript'></a>TypeScript

Publicado por MS en 2012 [TypeScript](https://es.wikipedia.org/wiki/TypeScript) es de código abierto (licencia Apache), es un superconjunto de JS que esencialmente añade tipos estáticos y objetos basados en clases, uno de los desarrolladores principales también fue el diseñador de C#, Delphi y Turbo Pascal.

[Most Loved, Dreaded, and Wanted Languages (2018)](https://insights.stackoverflow.com/survey/2018#most-loved-dreaded-and-wanted).

![](img/11.PNG)

Muchas empresas usan TS, entre ellas se encuentra su creadora MS y también empresas como Google con Angular. 

El enorme número de herramientas con las que cuenta para su desarrollo como autocompletado IntelliSense con VS Code,  detección de errores,  refactoring, la excelente documentación, etc,. Están entre las razones principales para usarlo.

Las características de TS se pueden dividir en 3 grupos basados en su relación con JS. Las dos primeras están relacionadas con [ECMA-262](https://www.ecma-international.org/publications-and-standards/standards/ecma-262/)

![](img/12.PNG)


![](img/03.png)

[Fuente](https://subscription.packtpub.com/book/application_development/9781787280038/1/ch01lvl1sec12/typescript-features)



###  2.1. <a name='Recursos'></a>Recursos

###  2.2. <a name='Caractersticasprincipales'></a>Características principales

* Diseñado para apps a gran escala y proyectos duraderos VS JS.
* Es la base de _frameworks_ de desarrollo como Angular y Vue.
* **Superconjunto de JS**, reconoce cualquier código JS. **¿Es TS un Azucarillo sintáctico de JS?**
* **Tipado estático**: permite evitar errores en tiempo de ejecución, hace el código fuente más explícito y claro.
* Objetos basados en clases.
* **Encapsulamiento** `public`... (Un modelo utilizado desde hace tiempo es simplemente utilizar propiedades con nombres que empiezan por _. Realmente no produce ningún tipo de efecto real).
* **Interfaces**.
* **Herencia** con `extends`.
* **Transpilador** (traduce de un lenguaje fuente a otro) que genera código universal JS.  No genera código superfluo. Permite detectar errores de forma temprana en la transpilación. Eso nos permite probar las últimas funcionalidades de JS sin necesidad que el navegador las reconozca aún. El compilador de TypeScript está escrito asimismo en TypeScript, compilado a JavaScript y con Licencia Apache 2.

Ejemplo de la documentación de la [función Ajax en JQuery](https://api.jquery.com/jquery.ajax/) donde no se sabe que tipo de parámetros debemos pasar a una función.

###  2.3. <a name='Instalacin'></a>Instalación

Se puede instalar desde VSCode, NuGet o desde la línea de comandos con Node.js ([paquete npm](https://www.npmjs.com/package/typescript)).

```bash
# npm install -g typescript
```

[https://github.com/microsoft/TypeScript/releases](https://github.com/microsoft/TypeScript/releases).

```bash
$ ts -v
Version 4.3.2
```

MS [Announcing TypeScript 4.0](https://devblogs.microsoft.com/typescript/announcing-typescript-4-0/).

###  2.4. <a name='Ejerciciosdeejemplo'></a>Ejercicios de ejemplo

Si te tienes problemas no es necesario o imprescindible configurar un entorno local, se pueden probar scripts en el Playground de TS [https://www.typescriptlang.org/play](https://www.typescriptlang.org/play). Permite ver el código JS que se está generando al vuelo.

No hay un entorno de ejecución de TS, lo que se hace es transpilarlo a JS y ejecutarlo.  Podemos aprovechar nuestro navegador preferido para ejecutarlo también (IDEs [https://jsconsole.com/](https://jsconsole.com/), [https://playcode.io/new/](https://playcode.io/new/)...).

####  2.4.1. <a name='Tipadoestticodedatos'></a>01 - Tipado estático de datos

Si has programado en C#, C, Java o otros lenguajes seguramente lo que viene a continuación te va a parecer evidente.

El primer ejercicio ilustra una de las características clave de TS, el tipado estático de las variables produce un error al tratar de asignar un tipo a una variable destinada a otro tipo.

```bash
ts ejemplo-01.ts
```

La ejecución produce como salida un fichero transpilado a JS, se puede ejecutar con `node ejemplo-01-js` y no producirá error alguno.

Para declarar variables como en JS usamos las palabras clave `let`, `var` y `const`. 

* `let`: El **ámbito de la variable** es local al bloque.
* `const`: Como su nombre indica su valor es inmutable.

La ejecución produce como salida un fichero traspilado a JS, se puede ejecutar con “node ejemplo-01-js” y no producirá error alguno.

[src/ts/ejemplo-01.ts](src/ts/ejemplo-01.ts)

####  2.4.2. <a name='Inferenciadetipo'></a>02 - Inferencia de tipo

Asigna un tipo sin necesidad de escribirlo, se infiere de la variable asignada en la declaración a partir de un análisis estático del programa.

[src/ts/ejemplo-02.ts](src/ts/ejemplo-02.ts)

####  2.4.3. <a name='Declaracindetiposdevariablesprimitivas'></a>03 - Declaración de tipos de variables primitivas

Diferentes posibilidades para declarar una variable:

![](img/04.PNG)

Los tipo básicos son `boolean`, `number` y `string`.

[src/ts/ejemplo-03.ts](src/ts/ejemplo-03.ts)

####  2.4.4. <a name='Funciones'></a>04 - Funciones

[src/ts/ejemplo-04.ts](src/ts/ejemplo-04.ts)

####  2.4.5. <a name='Encapsulacinyherenciadeclases'></a>05 - Encapsulación y herencia de clases

En JS se introduce el concepto de [clases](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Classes) con el [ECMAScript](https://es.wikipedia.org/wiki/ECMAScript) 2015 v6, hoy días la declaración de una clase básica es igual en JS y TS.

EN JS no se declarán los atributos de una clase y su visibilidad ([**encapsulación**](https://es.wikipedia.org/wiki/Encapsulamiento_(inform%C3%A1tica))) `private/public/protected`.

![](img/01.png)

[src/ts/ejemplo-05.ts](src/ts/ejemplo-05.ts)

####  2.4.6. <a name='Interfaces'></a>Interfaces

[src/ts/ejemplo-05.ts](src/ts/ejemplo-06.ts)

####  2.4.7. <a name='Mdulos'></a>Módulos

 Los módulos es otro de los puntos fuertes de TypeScript para mejorar la organización y la reutilización de código. En C# se conoce como NameSpace, y comparte también la característica de que puede estar distribuido en varios ficheros.

##  3. <a name='Angular'></a>Angular

###  3.1. <a name='InstalarAngularCLI'></a>Instalar Angular CLI

Angular cuenta con un interface de línea de comandos para crear nuestros proyectos, para instalarlo ejecutar:

```bash
npm install -g @angular/cli
```

###  3.2. <a name='Miprimerproyecto'></a>Mi primer proyecto

####  3.2.1. <a name='Creacindelentornodetrabajo'></a>Creación del entorno de trabajo

Lo primero es crear el entorno de trabajo que puede contener uno o más proyectos.

Crear un nuevo entorno de trabajo con [`ng new`](https://angular.io/cli/new):

```bash
ng new myapp
```

Aceptamos todas las opciones por defecto.

Angular CLI	instala los paquetes npm necesarios y todas las dependencias necesarias.

Después de unos minutos ya tenemos el espacio de trabajo en la carpeta "myapp" creado con todos los paquetes npm y dependencias, dentro en el directorio "src" tendremos un esqueleto de proyecto inicial.

####  3.2.2. <a name='Ejecutarlaaplicacin'></a>Ejecutar la aplicación

El comando ng serve ejecuta el servidor y recompila la aplicación refrescando el contenido de la aplicación Web a medida que realizamos cambios en los ficheros. EL parámetro --open abre la aplicación en el navegador (http://localhost:4200/).

```bash
cd myapp
ng serve --open
```

####  3.2.3. <a name='Estructuradeunproyecto'></a>Estructura de un proyecto

Cuando creamos un nuevo espacio de trabajo Angular genera una estructura de carpetas y crea un montón de archivos (mi carpeta "my-app" ocupa 350MB en disco y tiene 26.634 archivos), parece algo desproporcionado para una aplicación tan sencilla y en la mayoría de los casos no modificaremos la mayoría de los archivos que forman el proyecto. Vamos a repasar por encima el propósito de algunos de estos ficheros y como se estructura el proyecto.

![](img/05.png)

* La carpeta **"./node_modules"** alberga los módulos Node.js
* la carpeta **"./src"** es donde trabajaremos sobre nuestra aplicación la mayor parte del tiempo.

El archivo **"./angular.json"** apunta a "main.ts" que es el punto de entrada de ejecución de nuestra aplicación.

**"src/index.html"** es la página que contendrá toda nuestra aplicación SPA, entre otros parámetros también hace referencia a la hoja de estilos CSS que usamos ("styles": ["src/styles.css"],).

![](img/06.png)

El archivo **"package.json"** define las dependencias que tiene nuestro proyecto.

![](img/07.png)

####  3.2.4. <a name='Realizandomodificaciones'></a>Realizando modificaciones

Dentro de la carpeta **"./src/app"** podemos encontrar los componentes que forman la página.

![](img/08.png)

* app.component.ts: El código de la clase componente en TS.
* app.component.html: La plantilla del componente en HTML.
* app.component.css: Los estilos privados CSS.

Ahora vamos a cambiar el título de la aplicación en "./src/app/app.component.ts":

![](img/09.png)

Abre la plantilla "app.component.html", borra el contenido y reemplazalo:

```html
<h1>{{title}}</h1>
```

Las llaves dobles `{{}}` se reconocen como la sintaxis de interpolación o _binding_.

###  3.3. <a name='Interpolacin'></a>Interpolación 

![](img/10.png)

* Interpolación: Modelo a vista del componente al DOM.
* Enlazado de propiedades: Modelo a vista.
* Event binding
* Two way binding

####  3.3.1. <a name='InterpolacindecadenasdelcomponentealDOMcon'></a>Interpolación de cadenas, del componente al DOM con {{}}

La interpolación de cadenas en Angular es el mecanismo para sustituir una expresión por un valor de tipo string en la plantilla (template).

Cuando Angular se encuentra una expresión entre {{}} en la plantilla lo evalúa y trata de sustituir el contenido por el valor de la propiedad del componente con el mismo nombre. La interpolación es dinámica. Quiere decir que, si cambia el valor de la propiedad del componente, Angular se dará el trabajo de cambiar todos los lugares donde se está haciendo uso de esa propiedad

Este tipo de vinculación o binding es unidireccional, vincula el valor del elemento a la propiedad del componente.

##### Ejemplo básico

Para estos ejemplos no vamos a usar estilos CSS, el contenido de la vista lo incrustaremos en el propio componente en TypeScript para ver como funciona de un solo vistazo, borro [app.component.html].

```bash
ng new basicbinding
cd basicbinding
ng serve --open
```

Encerrar el texto entre (`) nos permite dividir el texto en múltiples líneas para facilitar su lectura. Si cambia el valor de la propiedad Angular hace lo que corresponde y refresca la vista con los cambios.

Hemos explicitado el tipo de variable como cadena (string) pero en realidad se infiere su tipo en la declaración.

También se puede usar la interpolación para definir el valor de una propiedad de una etiqueta.

```html
<a href="{{ miHref }}">Enlace</a>
```

##### Evaluar una expresión

También podemos usar la interpolación para evaluar una expresión:

```html
<p>La suma de 1 + 1 es {{ 1 + 1 }}.</p>
```
Incluso expresiones cuyo valor es calculado por un método del componente. 

```html
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  template: `
    <h1>{{ titulo }}</h1>
    <h2>Soy: {{ nombre }}</h2>
    {{ metodoComponente() }}
	<p>Llamada a método retorna: {{ 1 + 1 + dameValor() }}.</p>
  `
})
export class AppComponent {
  titulo = "One way binding";
  nombre: string = "Iker";

  metodoComponente() {
    console.log("Metodo componente");
  }

  dameValor(): number {
    return 2;
  }
}
```

##### ¡Interpolación en acción!

Para ver la interpolación en el momento que se produce voy a cambiar la propiedad del componente cuando se pulse un botón, **Angular de forma automática cambia el contenido en la plantilla cuando cambia el valor de la propiedad**:

app.component.html:

```
<h2>Soy: {{ nombre }}</h2>
<button (click)="onClickMe()">Click me!</button>
```

app.component.ts:

```
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'interpolationaction';
  nombre:string;

  onClickMe() {
    this.nombre = "Iker"; // Activamos interpolación a placer
  }

}
```

####  3.3.2. <a name='Comunicacinbidireccionalentrevistaymodelo'></a>Comunicación bidireccional entre vista y modelo

La directiva [ngModel](https://angular.io/api/forms/NgModel), comunica un modelo con un formulario, es especial porque permite comunicarlos de forma bidireccional, crea una instancia de FormControl.

**NOTA**: Para poder usarl ngModel es necesario importar FormsModule de "@angular/forms" en app.module.ts `import { FormsModule } from '@angular/forms';`.

```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Importar FormsModule
import { FormsModule } from '@angular/forms'; // <----------

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule  // <----------    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Ejemplo básico, declaro un atributo de tipo cadena de texto para guardar el nombre de un producto:

```
productName: string;
```

Diseño la vista con una etiqueta y un control de tipo campo de edición, uso la directiva `ngModel` con la sintaxis _banana in a box_ `[(...)]` (esta notación es lo que llaman _syntactic sugar_ o azucarillo sintáctico)

```
<label for="productNameInput">Product Name</label>
<input
  type="text"
  id="productNameInput"
  placeholder="Product Name"
  name="productName"
  [(ngModel)]="productName"
/>
```

Si queremos ver como actúa podemos añadir la siguiente línea en la vista:

```
<p>{{ productName }}</p>
```

###  3.4. <a name='HTTPyserviciosREST'></a>HTTP y servicios REST

####  3.4.1. <a name='RequisitospararealizarunapeticinHTTPenAngular'></a> Requisitos para realizar una petición HTTP en Angular

Debemos añadir el módulo `HttpClientModule` al array `imports` de `AppModule`.

####  3.4.2. <a name='LlamadaHTTPGetenelcomponente'></a>Llamada HTTP Get en el componente

Para el ejemplo usaré la llamada [https://api.npms.io/v2/search?q=scope:angular](https://api.npms.io/v2/search?q=scope:angular) para obtener el número de paquetes NPM relacionados con Angular, se puede comprobar la respuesta JSON abriendo el enlace en el navegador, dentro de la respuesta JSON queremos obtener el total de paquetes y guardarlo en una nueva variable `totalAngularPackages` de la clase.

[HttpClient](https://angular.io/api/common/http/HttpClient) de @angular/common/[http](https://angular.io/api/common/http) ofrece un cliente HTTP API para facilitar las llamadas. 

En el constructor de la clase se inyecta [HttpClient](https://angular.io/api/common/http/HttpClient).

####  3.4.3. <a name='Unbotnenlavistaparamostrarlosresultados'></a>Un botón en la vista para mostrar los resultados

```
<button type="button" (click)="onSubmit()">Click Me!</button> 
```

###  3.5. <a name='Routing'></a>Routing

En un sitio Web normalmente los enlaces de navegación apuntan a diferentes rutas, cuando pinchamos sobre ellos el servidor Web recibe la petición, carga la nueva página y entrega la página solicitada en la nueva ruta, por ejemplo este artículo pertenece al sitio raíz https://soka.gitlab.io/angular/, cuando pinchamos sobre un enlace en el menú lateral como por ejemplo https://soka.gitlab.io/angular/intro/getting-started/getting-started/, el servidor carga la página en una nueva ruta en "/angular/intro/getting-started/getting-started/".

Las Web construidas con Angular siguen el concepto SPA (Single Page Application), sólo tenemos una página principal "index.html", todas las vistas asociadas a cada componente se cargan sobre este index.html. Para facilitar la navegación entonces usamos el sistema de enrutado con rutas "virtuales".

####  3.5.1. <a name='Creacindeunaappconenrutadohabilitado'></a>Creación de una app con enrutado habilitado

La opción de enrutado no está habilitada por defecto cuando creamos un nuevo proyecto, hay que indicar explicitamente que queremos usarla en las opciones de creación del proyecto.

```
ng new routing --routing
```

Una vez creado el proyecto en [src/app/app.module.ts] Angular importa de forma automática la clase "AppRoutingModule" del componente donde se configuran las rutas:

```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from "./app-routing.module"; // <---- Route

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

####  3.5.2. <a name='Definiendoelmendenavegacin'></a>Definiendo el menú de navegación

En [src/app/app.component.html] definimos la vista con el menú de navegación con el atributo [routerLink](https://angular.io/api/router/RouterLink) definimos la ruta de cada opción, los enlaces comienzan por "/" para referirse al raíz.

```
<nav>
  <a href="#" routerLink="/">Home</a> |
  <a href="#" routerLink="/contacto">Contacto</a> |
</nav>

<router-outlet></router-outlet>
```

Más abajo la directiva `<router-outlet>` inyecta un componente u otro dependiendo de la ruta elegida.

####  3.5.3. <a name='Crearcomponentesadicionalesparalanavegacin'></a>Crear componentes adicionales para la navegación

Voy a crear el componente "contacto" al que hace referencia el menú.

```
ng g c contacto
```

He creado un nuevo componente [src/app/contacto/contacto.component.ts]. Usando el mismo sistema he creado un componente "home" con ng g c home.

####  3.5.4. <a name='Crearlalistaderutasdisponibles'></a>Crear la lista de rutas disponibles

En la cabecera de [src/app/app-routing.module.ts] se importan las clases necesarias:

```
import { ContactoComponent } from "./contacto/contacto.component"; // <---
import { HomeComponent } from "./home/home.component"; // <---
```

En el cuerpo se declara routes que es un array de objetos de tipo Routes, inicialmente vacío añado al array los componentes que corresponden a cada ruta en el menú.

```
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactoComponent } from "./contacto/contacto.component"; // <---
import { HomeComponent } from "./home/home.component"; // <---


const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "contacto", component: ContactoComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]  
})
export class AppRoutingModule { }
```

####  3.5.5. <a name='Recursos-1'></a>Recursos

- [https://academia-binaria.com/paginas-y-rutas-angular-spa/](https://academia-binaria.com/paginas-y-rutas-angular-spa/).
- [https://angular.io/tutorial/toh-pt5#add-navigation-with-routing](https://angular.io/tutorial/toh-pt5#add-navigation-with-routing).










