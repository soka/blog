---
title: Python OCR
subtitle: crear entornos virtuales
date: 2024-02-22
draft: false
description: crear entornos virtuales
tags: ["Python","venv"]
---

<!-- vscode-markdown-toc -->

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


```bash
sudo apt update
sudo apt install tesseract-ocr
``` 

```bash
tesseract --version
``` 

```bash
pip install pytesseract pillow
``` 


```python
import pytesseract
from PIL import Image
import os

# Configuración de pytesseract. Asegúrate de que el ejecutable de Tesseract esté en el PATH o especifica su ubicación completa.
# pytesseract.pytesseract.tesseract_cmd = r'/ruta/completa/a/tesseract.exe'  # para Windows

def extract_text_from_images(input_folder, output_file):
    # Lista para almacenar el texto extraído de cada imagen
    extracted_texts = []

    # Recorre todos los archivos en la carpeta de entrada
    for filename in os.listdir(input_folder):
        # Construye la ruta completa del archivo
        file_path = os.path.join(input_folder, filename)
        
        # Verifica si el archivo es una imagen (puedes agregar más formatos si es necesario)
        if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.gif', '.tiff')):
            try:
                # Abre la imagen y aplica OCR
                with Image.open(file_path) as img:
                    text = pytesseract.image_to_string(img, lang='eng')  # Puedes cambiar 'eng' por el código de idioma apropiado
                    extracted_texts.append(f"## {filename}\n\n{text}\n")
            except Exception as e:
                print(f"No se pudo procesar {filename}: {e}")

    # Escribir el texto extraído en un archivo Markdown
    with open(output_file, 'w', encoding='utf-8') as f:
        f.write("\n".join(extracted_texts))

# Uso del script
input_folder = 'ruta/a/tu/carpeta/con/imagenes'
output_file = 'salida.md'
extract_text_from_images(input_folder, output_file)
```



