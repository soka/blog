---
title: Izan Ahobizi digital!
subtitle: Hizkuntz ohiturak aldatzeko baliabideak
date: 2020-11-21
draft: false
description: Aplikazioak eta gailuak nola konfiguratu, aplikazio eta baliabideak, etab
tags: ["Euskaraldia","Euskara"]
---

Dagoeneko Euskaraldian murgilduta gaude! Hizkuntz ohiturak aldatzeko autua euskal hiztun askok hartu dugu, kalean bai baina tamalez askotan mundu digitalean ez da horren "erraza", nahi bai baina, erabiltzen ditugun gailu eta erraminta gehienak ez dakigu nola konfiguratu, hona hemen zenbait baliabide eta informazio iturri gure hizkuntz eskubideak bermatzeko.

[Hemen aurkezpena deskargatzeko](https://gitlab.com/soka/blog/-/blob/master/content/post/2020-11-21-izan-ahobizi-digital/aurkezpena-v01-20201122a.zip) ZIP formatoan, **bukatu gabe dago oraindik**, deskonprimatu ondoren index.html fitxategia zabaldu behar de nabigatzailean. [Revealjs](https://revealjs.com/)-rekin eginda dago beraz edozeinek editatu eta aldatu dezake.

## Aurkibidea

1. Sakeleko telefonoa edo mugikorra
2. Nabigatzailea
3. Sistema eragileak
4. Beste aplikazio batzuk

## Sakeleko telefonoa edo mugikorra

Mugikor berria erosi behar baduzu aztertu eta lehenetsi euskaraz konfiguratu daitekeen bat, [lehenhitza.eus](https://www.lehenhitza.eus/mugikorrak/) webgunearen informazioari erreparatzen badiogu %68.97 euskaraz jarri daitezke.

![](img/01.png)

Webguneak ere badu bilatzaile bat marka eta modelen arabera ondo aztertzeko. iPhone bat baduzue zoritzarrekoak zuek karkarkar.

![](img/02.png)

Android bat baduzu eman beharreko urratsak oso errazak dira, **Ezarpenak > Ezarpen osagarriak > Hizkuntza eta idazketa > Hizkuntzak** atalean aldatu daikete.

**Android mugikorrerako aplikazioak**:

- [Xuxen](https://play.google.com/store/apps/details?id=net.eleka.xuxen&feature=search_result#?%20%20t=W251bGwsMSwyLDEsIm5ldC5lbGVrYS54dXhlbiJd) zuzentzaile ortografikoa.
- [Elhuyar Hiztegiak](https://play.google.com/store/apps/details?id=org.elhuyar.hiztegia20).
- [Itzultzailea](https://play.google.com/store/apps/details?id=eus.elhuyar.itzuli).
- [Itzuli](https://play.google.com/store/apps/details?id=com.ejgv.itzuli).

## Nabigatzaileak

Berriro ere [lehenhitza.eus](https://www.lehenhitza.eus/about#diagnostikoa) Webgunera joz **EHko internauten %3-5 inguruk soilik du nabigatzailea euskaraz konfiguratuta**, **Euskaldunen artean ere, %10 inguruan** dago nabigatzailea euskaraz daukatenen kopurua (2017ko datuak).

Beheko laukitxoak esango dizue nabigatzailea euskaraz duzuen konfiguratuta:

<iframe src="https://www.lehenhitza.eus/iframe_laukia" width="300" height="250" frameborder="0" scrolling="no"></iframe>

Zure hizkuntza ezarpenen detailea [hemen frogatu dezakezu](https://www.lehenhitza.eus/aukera).

![](img/06.png)

### Firefox

![](img/03.png)

**Firefox nabigatzaileetan horrela konfiguratu behar da euskara lehenesteko**, goiko eskumako menuan: Hobespenak > Orokorra. 

![](img/05.png)

#### Pluginak

Idazten dugunean **akats ortografikoak zuzentzeko aukera** du baita, saguaren eskumako botoiari klik emanda aukeratu euskara zuzentzailea. Ez baduzu euskara topatzen aukeren artean hiztegia jeitsi eta instalatu dezakezu [hemen](https://addons.mozilla.org/es/firefox/language-tools/)  

![](img/04.png)

[**Euskalbar**](https://addons.mozilla.org/es/firefox/addon/euskalbar/) plugina ere erabili dezakegu, Euskara hiztegiak eta bestelako baliabide eta tresnak (corpusak, zuzentzaileak, bilatzaileak...) eskeintzen ditu.

![](img/07.png)

[**Lehen hitza euskaraz**](https://addons.mozilla.org/eu/firefox/addon/lehen-hitza/) plugin-ak Webgune bat bisitatzean zure nabigatzaileak euskarazko bertsioa lehenetsiko dizu.

### Chrome

**Oharra**: Google Chrome ezin da euskaraz erabili, baina bai adierazi ahal duzue euskaraz nabigatzen ari zaretela

![](img/13.jpg)

Goian aipatutako plugina [**Lehen hitza euskaraz**](https://chrome.google.com/webstore/detail/lehen-hitza-euskaraz/blabaldcgjcicckhojgankkjjfcjjefh?utm_source=lehenhitza.eus&utm_medium=website) Chrome nabigatzailerako dago eskuragarri.

### Edge

## Sistema eragileak

### Windows

Eusko Jaurlaritzak [pakete hauek](https://www.euskadi.eus/euskarazko-softwarea-deskargatzea/web01-a2eutres/eu/eragilea.html) ditu eskuragarri Windows sistema eragilearentzako.

![](img/12.png)

## Ofimatika

### LibreOffice

### Microsoft 365

Microsoft-en Ofimatika paketea nagibatzailean euskaratu daitekeela frogatu dut. Zure [kontuaren ezarpenetan](https://myaccount.microsoft.com/) sartu behar duzu eta hizkuntza hautatu.

![](img/10.png)

### Google

Google-ek ere [kontuaren ezarpenetan](https://myaccount.google.com/u/1/data-and-personalization) euskaraz jartzeko aukera dauka:

![](img/11.png)

## Beste aplikazio batzuk

Etorkizun hurbilera begirako proiektu interesgarriak ezagutu nahi badituzue, [Librezaleko](https://librezale.eus/2020/06/23/euskarazko-erantzun-sinpleak-ulertzea/) taldeari esker izan nuen proiektu honen berri, geroz eta ohikoagoa dira ahots asistente digitalak, gailuei euskaraz hitz egiten diezunean ulertzea nahi baduzu [**Common Voice**](https://commonvoice.mozilla.org/eu) Mozillak bultzatutako proiektua bisitatu, ahotsaren ezagutza aplikazio ireki bat garatzea dute helburu jendearen laguntzarekin (grabaketa asko behar dira horrelako sistemak garatzeko), laguntza emateko bi aukera dituzue, zuen ahotsak grabatu esaldi batzuk irakurtzen edo entzun eta lagundu ahotsak balioztatzen.

![](img/08.png)

Librezaleko lagunek euskaratzen dituzten hainbat aplikazio ezagutu nahi badituzue [hemen zerrenda bat](https://librezale.eus/wiki/Proiektuak).

![](img/09.png)

Niri bereziki gustatzen zait [komunikazio eta elkarlanerako tresnak](https://librezale.eus/wiki/KomunikazioEtaElkarlanerakoTresnak) atala, 
bertan zerrenda ederra dago, konferentziak egiteko aplikazioak, txatak eta beste asko, denak software librean oinarrituta eta asko euskaratura daudenak.

[Eusko Jaurlaritzak](https://www.euskadi.eus/euskarazko-softwarea-deskargatu/web01-a2eutres/eu/) zerrenda bat argitaratu du,  ordenagailuarekin euskaraz lan egiteko hainbat oinarrizko tresna eskaintzen dira doan. Besteak beste hauek nabarmendu nahiko nituzke:

- [Zuzentzaileak](https://www.euskadi.eus/web01-a2eutres/eu/contenidos/informacion/euskarazko_soft_2015/eu_9567/xuxen.html): [Hobelex](http://www.uzei.eus/hobelex/) ([Office](https://www.uzei.eus/hobelex/ms_office/), [LibreOffice](http://www.uzei.eus/hobelex/libreoffice/)), [Xuxen Zuzentzaile ortografikoa](https://www.euskadi.eus/r59-20660/eu/contenidos/informacion/euskarazko_softwarea/eu_9567/xuxen.html).

## Webguneak

- [https://eu.wikipedia.org/](https://eu.wikipedia.org/).Wikipedia entziklopedia askea.
- [https://hiztegiak.elhuyar.eus/](https://hiztegiak.elhuyar.eus/).
- [https://www.euskadi.eus/traductor/](https://www.euskadi.eus/traductor/): Itzultzaile neuronal magikoa.

## Ondorioak

Software librean oinarritutako aplikazio eta herramientak malgutasun gehiago erakusten dute komunitarera zabaltzeko eta elkarlanean euskaratzeko.

## Estekak eta baliabideak

- [https://librezale.eus/2018/12/01/izan-ahobizi-digital/](https://librezale.eus/2018/12/01/izan-ahobizi-digital/).
- [https://www.lehenhitza.eus/mugikorrak/](https://www.lehenhitza.eus/mugikorrak/).
- Zabaldu mezua: Nabigatu euskaraz [https://sustatu.eus/1291280951](https://sustatu.eus/1291280951) - sustatu.eus (2010-12-03).
- [https://librezale.eus/wiki/Firefox](https://librezale.eus/wiki/Firefox).
- [https://iparra.eus/wp-content/uploads/2018/10/euskara-teknologian-iparra.eus_.pdf](https://iparra.eus/wp-content/uploads/2018/10/euskara-teknologian-iparra.eus_.pdf)

