---
title: Pipelines-ak GitLab-en 
subtitle: Licencias  
date: 2024-02-19
draft: true
description: Licencias
tags: ["GitLab","Python","Pipelines","CI/CD"]
---

<!-- vscode-markdown-toc -->
* 1. [Introducción](#Introduccin)
* 2. [Prueba básica paso a paso](#Pruebabsicapasoapaso)
	* 2.1. [Crear un nuevo proyecto en GitLab](#CrearunnuevoproyectoenGitLab)
	* 2.2. [Clonar el proyecto en local](#Clonarelproyectoenlocal)
	* 2.3. [Fichero YAML](#FicheroYAML)
	* 2.4. [Script Python de prueba](#ScriptPythondeprueba)
	* 2.5. [Primera ejecución](#Primeraejecucin)
* 3. [Agregar test con Pytest](#AgregartestconPytest)
	* 3.1. [process_dataframe.py](#process_dataframe.py)
	* 3.2. [test_process_dataframe.py](#test_process_dataframe.py)
	* 3.3. [YAML](#YAML)
* 4. [Anexos](#Anexos)
	* 4.1. [Gitlab Runner local](#GitlabRunnerlocal)
		* 4.1.1. [Descarga](#Descarga)
		* 4.1.2. [Instalación](#Instalacin)
	* 4.2. [Crear un proyecto en GitLab](#CrearunproyectoenGitLab)
		* 4.2.1. [Registrar un Runner](#RegistrarunRunner)
* 5. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Introduccin'></a>Sarrera

![](img/01.png)

##  2. <a name='Pruebabsicapasoapaso'></a>Oinarrizko proba, urratsez urrats

###  2.1. <a name='CrearunnuevoproyectoenGitLab'></a>Sortu proiektu berri bat GitLab-en


###  2.2. <a name='Clonarelproyectoenlocal'></a>Lokalean proiektua klonatzea

```bash
git clone https://gitlab.com/soka/gitlabrunners.git
``` 

```bash
cd gitlabrunners/
``` 


###  2.3. <a name='FicheroYAML'></a>YAML fitxategia


```bash
nano .gitlab-ci.yml
```

```
# Use the official Python image from Docker Hub
image: python:3.11

# Define the stages of the pipeline
stages:
  - test

# Define a job named "run_script" that will run in the "test" stage
run_script:
  stage: test
  script:
    - python script.py
```

###  2.4. <a name='ScriptPythondeprueba'></a>Script Python de prueba

```bash
$ nano script.py
```

```bash
# script.py

def main():
    print("Hello, GitLab CI/CD!")

if __name__ == "__main__":
    main()
``` 

###  2.5. <a name='Primeraejecucin'></a>Primera ejecución 

```
git add .; git commit -m "..."; git push
```

![](img/05.png)

![](img/06.png)


##  3. <a name='AgregartestconPytest'></a>Agregar test con Pytest

###  3.1. <a name='process_dataframe.py'></a>process_dataframe.py


```python
# process_dataframe.py

import pandas as pd

def process_dataframe(df):
    # Realizar algunas operaciones básicas
    df['total'] = df['quantity'] * df['price']
    return df

if __name__ == "__main__":
    data = {
        'product': ['apple', 'banana', 'orange'],
        'quantity': [4, 3, 10],
        'price': [0.5, 0.2, 0.8]
    }
    df = pd.DataFrame(data)
    result_df = process_dataframe(df)
    print(result_df)
``` 

###  3.2. <a name='test_process_dataframe.py'></a>test_process_dataframe.py

```python
# test_process_dataframe.py

import pandas as pd
from process_dataframe import process_dataframe

def test_process_dataframe():
    # Definir un DataFrame de ejemplo
    data = {
        'product': ['apple', 'banana', 'orange'],
        'quantity': [4, 3, 10],
        'price': [0.5, 0.2, 0.8]
    }
    df = pd.DataFrame(data)
    
    # Ejecutar el script de procesamiento
    result_df = process_dataframe(df)
    
    # Verificar que los datos sean correctos
    assert result_df.shape == (3, 4)
    assert result_df['total'].tolist() == [2.0, 0.6, 8.0]
```


###  3.3. <a name='YAML'></a>YAML

```
image: python:3.11

stages:
  - test

before_script:
  # Instalar pandas y pytest
  - pip install pandas pytest

test_script:
  stage: test
  script:
    - pytest
```


##  4. <a name='Anexos'></a>Anexos

<!-- 

https://guidocutipa.blog.bo/como-ampliar-memoria-swap-linux/

swapon --show
free -h


sudo swapoff -a

sudo fallocate -l 10G /swapfile

sudo chmod 600 /swapfile

sudo mkswap /swapfile

sudo swapon /swapfile

-->

###  4.1. <a name='GitlabRunnerlocal'></a>Gitlab Runner local


####  4.1.1. <a name='Descarga'></a>Descarga

```bash
$ dpkg --print-architecture
amd64
# Replace ${arch} with any of the supported architectures, e.g. amd64, arm, arm64
# A full list of architectures can be found here https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/index.html
# curl -LJO "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/deb/gitlab-runner_${arch}.deb"
$ curl -LJO "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/deb/gitlab-runner_amd64.deb"
```

####  4.1.2. <a name='Instalacin'></a>Instalación

```bash
$ sudo chmod +x gitlab-runner_amd64.deb
$ sudo dpkg -i gitlab-runner_amd64.deb
```

Crea un usuario exclusivo:

```bash
$ id gitlab-runner
uid=999(gitlab-runner) gid=988(gitlab-runner) groups=988(gitlab-runner)
$ ls /home/
gitlab-runner 
```

Comprobación de estado del servicio:

```bash
$ sudo gitlab-runner status
Runtime platform                                    arch=amd64 os=linux pid=1268 revision=fe451d5a version=17.1.0
gitlab-runner: Service is running
```

###  4.2. <a name='CrearunproyectoenGitLab'></a>Crear un proyecto en GitLab

- GitlabRunners	

####  4.2.1. <a name='RegistrarunRunner'></a>Registrar un Runner

![](img/02.png)

[https://docs.gitlab.com/runner/register/index.html#register-with-a-runner-authentication-token](https://docs.gitlab.com/runner/register/index.html#register-with-a-runner-authentication-token)

```bash
gitlab-runner register
  --url https://gitlab.com
  --token glrt-CFhhrgKWq8-4GPmFZuAn
```

```bash
nagusi@ubuntuserver:~$ gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=1316 revision=fe451d5a version=17.1.0
WARNING: Running in user-mode.                     
WARNING: The user-mode requires you to manually start builds processing: 
WARNING: $ gitlab-runner run                       
WARNING: Use sudo for system-mode:                 
WARNING: $ sudo gitlab-runner...                   
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
glrt-CFhhrgKWq8-4GPmFZuAn
Verifying runner... is valid                        runner=CFhhrgKWq
Enter a name for the runner. This is stored only in the local config.toml file:
[ubuntuserver]: runner-test-01 
Enter an executor: shell, ssh, parallels, instance, custom, docker, docker-windows, docker+machine, kubernetes, docker-autoscaler, virtualbox:
docker
Enter the default Docker image (for example, ruby:2.7):
python:3.11 
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
 
Configuration (with the authentication token) was saved in "/home/nagusi/.gitlab-runner/config.toml" 
```


Manually verify that the runner is available to pick up jobs

```bash
gitlab-runner run
```

```bash
sudo gitlab-runner list
``` 

![](img/03.png)


##  5. <a name='Enlacesexternos'></a>Enlaces externos

* [https://blog.develat.io/pipelines-ci-cd/](https://blog.develat.io/pipelines-ci-cd/)
* [https://blog.develat.io/pipelines-en-gitlab-el-camino-hacia-el-ci-cd-parte-i/](https://blog.develat.io/pipelines-en-gitlab-el-camino-hacia-el-ci-cd-parte-i/)
* [https://docs.gitlab.com/runner/install/](https://docs.gitlab.com/runner/install/)
* [https://docs.gitlab.com/runner/install/linux-manually.html](https://docs.gitlab.com/runner/install/linux-manually.html)	
* [https://devopstales.github.io/home/introduction-to-gitlab-ci-cd/](https://devopstales.github.io/home/introduction-to-gitlab-ci-cd/)