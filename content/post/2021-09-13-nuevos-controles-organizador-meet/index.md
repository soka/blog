---
title: Nuevos controles para los organizadores de Meet
subtitle: Finalizar la reunión para todos y otros ajustes
date: 2021-09-13
draft: false
description: Finalizar la reunión para todos y otros ajustes
tags: ["Google","Meet","Updates","Actualizaciones","Google Workspace","Novedades","Workspace"]
---

<!-- vscode-markdown-toc -->
* 1. [Enlaces internos](#Enlacesinternos)
* 2. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Google ha anuncidado hace unos días una nueva opción disponible para los organizadores de una reunión que permite [**finalizar la reunión para todas las personas asistentes**](https://workspaceupdates.googleblog.com/2021/09/end-meeting-for-all-through-security-investigation-tool.html). 

Ahora cuando uses la opción de Salir de la llamada tendrás varias opciones, "Salir de la llamada" hace que abandonemos la reunión pero el resto de participantes pueden seguir, "Finalizar la llamada" afecta a todas las personas participantes.

![](img/02.PNG)


Además de la opción de finalizar la llamada ahora tenemos nuevas opciones para los organizadores de la reunión, en la esquina inferior derecha con un icono representado como un escudo (ver imagen inferior), también es accesible en la configuración de la reunión (menu desplegable de 3 puntos).

![](img/01.PNG)

Cuando activamos la **gestión de anfitriones** podemos evitar que el resto de participantes compartan pantalla o usen el chat. Si desactivamos el acceso rápido las personas invitadas mediante enlace tendrán que solicitar unirse y el organizador de la reunión debe permitirlo.

![](img/03.PNG)


##  1. <a name='Enlacesinternos'></a>Enlaces internos

* [Comparte tu ubicación de trabajo desde Google Calendar](https://soka.gitlab.io/blog/post/2021-09-06-comparte-ubicac%C3%ADon-trabajo-desde-google-calendar/).
* [Google Calendar permite especificar si asistirás virtualmente o de forma presencial](https://soka.gitlab.io/blog/post/2021-07-19-google-calendar-opcion-asistencia-presencial-o-virtual/).
* [Algunas novedades interesantes en Google Workspace](https://soka.gitlab.io/blog/post/2021-07-02-google-workspace-novedades/).
* [Integración de Google Meet en Slack](https://soka.gitlab.io/blog/post/2021-06-23-slack-integracion-google-meet/).
* [Google Calendar para Slack](https://soka.gitlab.io/blog/post/2021-06-21-slack-integracion-google/).
* [Meet for Slack](https://soka.gitlab.io/blog/post/2021-04-26-meet-for-slack/).
* [Seguridad con cuentas de Google](https://soka.gitlab.io/blog/post/2021-02-21-seguridad-cuentas-google/).

##  2. <a name='Enlacesexternos'></a>Enlaces externos

* [New “End meeting for all” action in the security investigation tool](https://workspaceupdates.googleblog.com/2021/09/end-meeting-for-all-through-security-investigation-tool.html).
* [Añadir coanfitriones en Google Meet](https://support.google.com/meet/answer/10885841?hl=es).
* [Use the investigation tool to end meetings](https://support.google.com/a/answer/11028503).
* [About the security investigation tool](https://support.google.com/a/answer/7575955).

