---
title: Integración de GMail en Slack
subtitle: Apps para Slack
date: 2021-06-23
draft: true
description: Envía correos electrónicos de GMail a Slack
tags: ["Slack","Google","Productividad","Aplicaciones","Gmail"]
---

<!-- vscode-markdown-toc -->
* 1. [Enlaces internos](#Enlacesinternos)
* 2. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


**Con Slack para Gmail puedes**:

* Reenviar correos electrónicos de Gmail a Slack
* Adjuntar archivos o notas para tener contexto
* Coordinar las respuestas de correo electrónico con tus compañeros de equipo

##  1. <a name='Enlacesinternos'></a>Enlaces internos

* Meet for Slack [https://soka.gitlab.io/blog/post/2021-04-26-meet-for-slack/](https://soka.gitlab.io/blog/post/2021-04-26-meet-for-slack/).
* Slack - Comunicación colaborativa para el trabajo en equipo [https://soka.gitlab.io/blog/post/2021-04-01-taller-slack-spri-enpresa-digitala/](https://soka.gitlab.io/blog/post/2021-04-01-taller-slack-spri-enpresa-digitala/).

##  2. <a name='Enlacesexternos'></a>Enlaces externos

