---
title: Recarga los cambios de tu página HTML/JS/CSS de forma automática
subtitle: NodeJS Live Server
date: 2019-03-17
draft: false
description: Desarrollo proyectos Web
tags: ["NPM", "Programación", "Web", "NodeJS", "Testing", "Live Server"]
---

[**Live Server**](http://tapiov.net/live-server/) es un desarrollo **NodeJS** que permite recargar de forma automática una página HTML/CSS/JS, detecta los cambios que realizamos y refresca la página, está recomendado para la etapa de desarrollo de nuestro proyecto.

Instalación usando el gestor de paquetes NPM:

```NPM
npm install -g live-server
```

Ahora solo tenemos que ir a la ruta de nuestro proyecto y ejecutar:

```DOS
C:\> live-server
```

Inmediatamente carga un pequeño servidor en la dirección `http://127.0.0.1:8080/` y abre nuestro navegador Web.

Evidentemente el comando puede aceptar parámetros para modificar el puerto, el navegador que usa por defecto, etc. Se puede consultar en el [repositorio GitHub del proyecto](https://github.com/tapio/live-server). Yo uso el parámetro `-V` o `--verbose` para que **Live Server** muestre más información de su ejecución.

Podemos crear un fichero `live-server.json` para definir la configuración y evitar recordarla, cuando lanzamos **Live Server** buscará este fichero.

También podemos ejecutar **Live Server** desde Node en un script JS. Te puede pasar como me ha pasado a mi en MS Win10, he tenido que definir antes una variable de entorno para que **NodeJS** encuentre el modulo **Live Server** con el siguiente comando:

```DOS
set NODE_PATH=%APPDATA%\npm\node_modules\
```

Ahora ya puedo ejecutar el fichero "live-server.js" de abajo con el comando `C:\> node live-server.js`.

```JS
var liveServer = require("live-server");

var params = {
  port: 8080,
  host: "0.0.0.0",
  open: true,
  file: "index.html",
  logLevel: 2,
  middleware: [
    function(req, res, next) {
      next();
    }
  ]
};
liveServer.start(params);
```

# Enlaces

- NPM [Live Server](https://www.npmjs.com/package/live-server).
- GitHub [tapio/live-server](https://github.com/tapio/live-server).
- Web estática en GitHub [tapiov.net/live-server/](http://tapiov.net/live-server/).
- [Instalar Node.js en Windows](https://luismasdev.com/instalar-nodejs-en-windows/).
- [Solución – NPM: Error, cannot find module](http://www.nodehispano.com/2012/06/solucion-npm-error-cannot-find-module/).
- [Start a local, live-reload web server with one command](https://medium.com/@svinkle/start-a-local-live-reload-web-server-with-one-command-72f99bc6e855).
