---
title: Seleccionar datos con [] y subset
subtitle: Trabajando con datasets en R
date: 2019-03-15
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico","R - Datos","R - Objetos"]
---

Vamos a repasar algunos conceptos que ya han aparecido en otros posts para acceder a elementos de datasets y listas, usando los corchetes `[]` o la función específica `subset()`. Para probar estos ejemplos usamos el dataset "iris" que viene con **R**, para consultar sobre datasets disponibles usar `library(help = "datasets")`.

# Seleccionando con []

Un elemento determinado:

```R
> iris[1,2]
[1] 3.5
```

Filas 1 y 2, Columna 2:

```R
> iris[1:2,2]
[1] 3.5 3.0
```
Y esto también funciona:

```R
> iris[1:2,1:2]
  Sepal.Length Sepal.Width
1          5.1         3.5
2          4.9         3.0
```

Tres filas no consecutivas de la segunda columna:

```R 
> iris[c(2,4,6),2]
[1] 3.0 3.1 3.9
```

Seleccionamos una columna por su nombre:

```R
iris["Sepal.Width"]
iris$Sepal.Width
```

Seleccionamos varias columnas por su nombre con un vector:

```R
iris[c("Sepal.Width","Sepal.Length")]
```

# subset()

Ahora haremos lo mismo usando subset(), retorna un subconjunto de datos de vectores, matrices o data frames que cumplan unas determinadas condiciones:

```R
iris1 <- subset(iris,iris$Sepal.Width > 3 & iris$Species == 'setosa')
```

# Listas

```R
v1 <- c(1,2,3)
v2 <- c("a","b","c")
v3 <- c(TRUE,FALSE,TRUE)
l1 <- list(v1,v2,v3)
# Primer objeto de la lista
l1[[1]] 
l1[1,2] # Esto NO funciona
l1[1:2]
# Del primer vector 2. posición
l1[[1]][2]
l1[[1]][1:2]
```


# Ejercicios

* r / intro / 24-Seleccion-datos-subset / [seleccion-datos-subset.R](https://gitlab.com/soka/r/blob/master/intro/24-Seleccion-datos-subset/seleccion-datos-subset.R).

# Enlaces

* ["Matrices en R"](https://soka.gitlab.io/blog/post/2019-03-11-r-matrices/).
* ["Data Frames en R"](https://soka.gitlab.io/blog/post/2019-03-12-r-data-frame/).


