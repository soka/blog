---
title: Integrar Google Analytics en Hugo
subtitle: Template Hugo para blog de GitLab
date: 2019-01-30
draft: false
description: Template Hugo para crear Webs estáticas escritas en Markdown
tags: ["Hugo","GitLab","Google Analytics","Blog","Web","Templates"]
---

Es tan sencillo como acceder a Google Analytics y dar de alta un nuevo sitio Web, en mi caso la URL principal de este humilde blog [https://soka.gitlab.io/](https://soka.gitlab.io/), copiamos el ID de la Web registrada que nos proporciona Analytics, alfo así como "UA-45068880-9" y editamos el fichero "config.toml", añadimos la siguiente línea al fichero:

```
googleAnalytics = "UA-45068880-9"
```

![](images/config.PNG)

Con esto creo que es suficiente y empieza a obrar el milagro.

![](images/analytics_01.PNG)


