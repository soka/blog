---
title: Política de contraseñas en directorio activo
subtitle: Windows Server 2012 R2
date: 2021-07-27
draft: false
description: Configurando PSO (Password Settings Objects) para un grupo de seguridad 
tags: ["Windows","AD","Dominio","Windows Server","Windows Server 2012","Contraseñas","Políticas"]
---

Antes de nada he creado un grupo de seguridad llamado "gs_trabajadores" dentro de "Usuarios y equipos de Active Directory" (`dsa.msc`), en este grupo iré introduciendo las cuentas de usuarios a los que aplicaré las políticas de contraseñas.

A continuación abro Herramientas administrativas > Centro de administración de Active Directory.

Una vez abierto debo acceder a "Yourdomain (local)\System\Password Settings Container", en el lado derecho una la opción para crear una nueva configuración de contraseña.

![](img/01.PNG)

Le asigno un nombre y defino precedencia como 1 (las de menor precedencia sobreescriben otras con un número superior). 

Campos:

* Longitud mínima de contraseñas = 12 (el INCIBE hace tiempo que ya recomienda no usar de 8 sino de 12).
* Exigir historial de contraseñas determina el número de contraseñas nuevas únicas que deben asociarse con una cuenta de usuario antes de que se pueda volver a usar una contraseña antigua. Establezco a 24 como recomienda la herramienta de auditoria CLARA del INCIBE.
* La contraseña debe cumplir los requisitos de complejidad = Habilitada. 
* Almacenar contraseñas con cifrado reversible = Deshabilitada.
* Vigencia mínima de la contraseña = 2.
* Vigencia máxima de la contraseña = 45.
* Número de intentos se sesión incorrectos permitidos = 8.
* Restablecer el bloqueo de cuenta después de = 30 minutos.
* La cuenta se bloqueará hasta que el administrador la desbloquee de forma manual.

En la parte inferior **asigno la política al grupo de seguridad** recien creado "gs_trabajadores". 




