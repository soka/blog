---
title: R - Gráficos básicos
subtitle: Función barplot()
date: 2019-03-19
draft: false
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "Programación",
    "R - Básico",
    "R - Gráficos",
    "R - Diagramas",
    "Diagramas",
  ]
---

Ahora vamos a revisar el gráfico de barras, el uso de la función `barplot()` es muy similar a `plot()`, por eso no me voy a detener en cada posible opción del diagrama de barras, ademas tengo un post previo sobre `barplot()` en este [enlace](https://soka.gitlab.io/blog/post/2019-02-21-diagramas-barras-en-r/).

```R
ventas <- c(12,34,10,45)
barplot(ventas,
        main="Ventas\n2017-2019", # Título principal
        xlab = "Tiendas", #Título eje x
        names.arg = c("Vi","Bi","Ss","Ma"), #Nombres de cada columna
        ylab = "Millones", #Título eje y
        border = "deeppink4", #Color del borde de barras
        col = "grey", #Color del relleno de barras
        legend.text=c("Vitoria",
                      "Bilbao",
                      "San Sebastian",
                      "Madrid"),
        cex.names = 0.8  # Reducimos tamaño fuente nombres columnas
        )
```

![](images/barplot_01.PNG)

Ahora voy a usar la función `table()` para hacer un diagrama de barras con frecuencias, la función `table()` retorna una tabla con el número de apariciones de cada elemento en el vector inicial `x`.

```R
x <- c(1,2,3,1,3,2,1,4,5,3,2,1)
datos <- table(x)
barplot(datos,main = "Frecuencia relativa",xlab = "Valores de la variable")
```

![](images/barplot_02.PNG)

A continuación asignamos nombres a las categorías de las variables y giramos el diagrama:

```R
barplot(datos,main = "Frecuencia relativa",
        xlab = "Valores de la variable",
        horiz = TRUE,
        names.arg = c("18 o menos","18 a 25","45 a 60","60 a 70","70 en adelante")
        )
```

![](images/barplot_03.PNG)

También podemos agrupar las barras en torno a variables.

```R
barplot(height = cbind(x=c(4,9),y=c(8,2),z=c(3,7)),
        beside = TRUE,
        width=c(45,50,35),
        col=c(1,2),
        legend.text = c("Hombre","Mujer"))
```

![](images/barplot_04.PNG)

# Ejercicios

- r / intro / 28-barplot / [barplot.R](https://gitlab.com/soka/r/blob/master/intro/28-barplot/barplot.R).

# Enlaces

- ["R - Gráficos básicos: Función plot()"](https://soka.gitlab.io/blog/post/2019-03-18-r-graficos-basicos-plot/).
- ["Diagramas de dispersión y modelo de regresión lineal en R"](https://soka.gitlab.io/blog/post/2019-02-05-diagramas-dispersion-en-r/).
- ["Diagramas tipo tarta en R"](https://soka.gitlab.io/blog/post/2019-02-14-diagramas-de-tarta-en-r/): Gráficos para variables cualitativas con pie().
- ["Recetas para hacer diagramas en R como lo hace la BBC"](https://soka.gitlab.io/blog/post/2019-02-19-graficos-r-estilo-bbc/).
- ["Diagramas de barras en R"](https://soka.gitlab.io/blog/post/2019-02-21-diagramas-barras-en-r/): Usando la función barplot().
