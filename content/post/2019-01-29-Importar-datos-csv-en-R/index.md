---
title: Importar datos de un CSV
subtitle: Data frames en R
date: 2019-01-29
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico"]
---

Exportar e importar datos de un CSV sigue estando a la orden del día, en la mayoria de los casos los datos provienen de otro sistema y los hemos exportado a un fichero [**CSV**](https://es.wikipedia.org/wiki/Valores_separados_por_comas) de texto plano para explotarlos en **R**.

# La función read.csv()

Este es un pequeño ejemplo de como podemos cargar la información de un **CSV** usando la función **read.csv()**. 

El fichero CSV de muestra contiene las estaciones de medición de la calidad del aire en 2019 en Euskadi (obtenido de [Open Data Euskadi](http://opendata.euskadi.eus/)). Contiene el nombre de la localización de la estación metereológica, una descripción, la provincia a la que pertenece, la población, la dirección y las coordenadas.

![](images/csv_data_file.PNG)

Lo primero es establecer el directorio de trabajo con la función `setwd()` que ya hemos mencionado anteriormente. Luego cargamos los datos usando la función `read.csv()` donde le pasamos como parámetros: 

* El nombre del fichero.
* Le indicamos que existe una cabecera inicial con la descripción de las columnas.
* El carácter separador de columnas.
* El simbolo decimal.

**Truco**: Si escribes y ejecutas la función con el signo de interrogación por delante muestra la ayuda en RStudio `?read.csv`.

```R
setwd("C:\\Users\\i.landajuela\\Documents\\GitLab\\r\\intro\\07-importar-datos-csv")

df1 <- read.csv(file='./estaciones_calidad_aire.csv',
                    header=TRUE,
                    sep=';',
                    dec=',')

```

Si quisiesemos podriamos leer directamente el fichero CSV en un enlace URL

```R
df3 <- read.csv(file='http://opendata.euskadi.eus/contenidos/ds_informes_estudios/calidad_aire_2019/es_def/adjuntos/estaciones.csv')
```

# Submuestras de una base de datos

Queremos **obtener un subconjunto de los datos** leidos del fichero CSV, vamos a obtener las líneas o registros cuyo campo `Town` este definido como `Bilbao` (como no podía ser de otra forma) y guardamos el nuevo conjunto en una nueva variable.

```
df2 <- df1[df1$Town == 'Bilbao', ]
```

# Características de un data frame

Usando la función [`dim`](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/dim) obtenemos las **dimensiones** del objeto, el primer valor corresponde al número de filas y el segundo al de columnas.

```
> dim(df2)
[1] 6 9
```

Para obtener el **número de registros** de un DF.

```R
> dim(df2)[1]   # Nº de registros
[1] 6
```

La estructura (**str**ucture) de un DF se explora con la función [`str()`](https://www.rdocumentation.org/packages/utils/versions/3.5.2/topics/str), muestra información sobre las variables que contiene, de que tipo son, etc.

```
> str(df2)
'data.frame':	6 obs. of  9 variables:
 $ Name                  : Factor w/ 57 levels "3 DE MARZO","ABANTO",..: 8 13 23 25 35 34
 $ Description           : logi  NA NA NA NA NA NA
 $ Province              : Factor w/ 3 levels "Araba/Álava",..: 2 2 2 2 2 2
 $ Town                  : Factor w/ 36 levels "Abadiño","Abanto y Ciérvana-Abanto Zierbena",..: 13 13 13 13 13 13
 $ Address               : Factor w/ 57 levels "Avda. Ategorrieta, 71",..: 30 39 42 25 10 22
 $ Coordenates.X..ETRS89.: num  503209 503791 507924 504257 505260 ...
 $ Coordenates.Y..ETRS89.: num  4788084 4792020 4789127 4790269 4790524 ...
 $ Latitude              : num  43.2 43.3 43.3 43.3 43.3 ...
 $ Longitude             : num  -2.96 -2.95 -2.9 -2.95 -2.94 ...
```

Nuestra variable DF pertenece a la **clase "data frame" y su tipo es una lista**.

```R
> class(df2)
[1] "data.frame"
> typeof(df2)
[1] "list"
```

En los DF las variables son las **columnas**, podemos **obtener sus nombres** con la función [`names()`](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/names)

```
> names(df2) # Variables o columnas de un DF
[1] "Name"                   "Description"            "Province"              
[4] "Town"                   "Address"                "Coordenates.X..ETRS89."
[7] "Coordenates.Y..ETRS89." "Latitude"               "Longitude" 
``` 

# Atributos y metadatos de un DF

Un DF en **R** puede contener una serie de atributos y metadatos sobre el mismo que podemos visualizar o modificar.

La función [**`attributes()`**](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/attributes) permite acceder a los atributos de un DF. 

Para acceder a atributos concretos usamoa la función **`attr()`**, podemos añadir comentarios descriptivos del contenido del DF y redefinir los nombres de las columnas con funciones como **`comment()`**.

```R
# ***** METADATOS Y ATRIBUTOS DE UN DF *****
?attributes
attributes(df2) # Visualizar los atributos, viriables o columnas, rownames (actualmente no tienen), dimensión, class,...

# Para manipular atributos individuales usamos attr()
?attr
label <- "Descripión del DF" #Podemos definir nuevos atributos
attr(df2,'datalabel') <- label # Definir una etiqueta descriptiva del contenido
attr(df2,'datalabel') # Mostrar descripción 
# En ocasiones definir la fecha de creación o versión del DF puede ser de gran utilidad
attr(df2,'times.stamp') <- '2019/01/30 09:30:40' 
attr(df2,'times.stamp')
attr(df2,'version') <- '1.0.2' 
attr(df2,'version')

?comment
comment(df2) <- "adding a comment to a data frame"
comment(df2) #Visualizar comentario 
# colnames(): Redefinir nombres de columnas
```


# Acceso a los datos dentro de un DF

Por ejemplo, si queremos ver sólo los registros (filas) 3 a 6, escribiríamos:

```
> df2[3:6,]
            Name Description Province   Town                                Address Coordenates.X..ETRS89.
23        EUROPA          NA  Bizkaia Bilbao           Parque Europa. Bº Txurdinaga               507924.2
25 FERIA (meteo)          NA  Bizkaia Bilbao         C/ Rafael Moreno Pitxitxi, s/n               504257.2
34     MAZARREDO          NA  Bizkaia Bilbao C/ Alameda Mazarredo, s/n (Guggenheim)               505259.7
40  Mª DIAZ HARO          NA  Bizkaia Bilbao                 C/ Mª Díaz de Haro, 60               504410.8
   Coordenates.Y..ETRS89. Latitude Longitude
23                4789127 43.25491 -2.902376
25                4790269 43.26522 -2.947544
34                4790524 43.26751 -2.935188
40                4789557 43.25880 -2.945657
> 
```

Si queremos seleccionar la primera columna:

```
> df2[,1] #columna 1
[1] ARRAIZ (Monte)   BANDERAS (meteo) EUROPA           FERIA (meteo)    MAZARREDO        Mª DIAZ HARO    
57 Levels: 3 DE MARZO ABANTO AGURAIN ALGORTA (BBIZI2) ALONSOTEGI ANDOAIN AÑORGA ARRAIZ (Monte) ... ZUMARRAGA
```
También podemos referirnos por el nombre de la columna:

```
> df2$Town
[1] Bilbao Bilbao Bilbao Bilbao Bilbao Bilbao
36 Levels: Abadiño Abanto y Ciérvana-Abanto Zierbena Agurain/Salvatierra Aia Alonsotegi Amorebieta-Etxano ... Zumarraga
> df2[,"Town"]  #Equivalente
```

# Ejercicios

* r/intro/07-importar-datos-csv/[importar-csv.R](https://gitlab.com/soka/r/blob/master/intro/07-importar-datos-csv/importar-csv.R).
* ["Calidad del aire en Euskadi durante el 2019"](https://gitlab.com/soka/r/blob/master/intro/07-importar-datos-csv/estaciones_calidad_aire.csv): He quitado algunos campos al fichero inicial [Fuente](http://opendata.euskadi.eus/catalogo/-/calidad-aire-en-euskadi-2019/).

He subido algunos ficheros CSV descargados de Internet. Se pueden obtener datos interesantes del portal Web [Open Data Euskadi](http://opendata.euskadi.eus/inicio/), es de donde he obtenido algunos de los ejemplos.

# Enlaces 

* ["Enlaces posts en R en esta Web"](https://soka.gitlab.io/blog/tags/r/).

**Enlaces externos**:

* ["Read CSV in R - RProgramming.net"](http://rprogramming.net/read-csv-in-r/).
* ["How to Use read.csv() to Import Data in R - dummies"](https://www.dummies.com/programming/r/how-to-use-read-csv-to-import-data-in-r/).
* ["This R Data Import Tutorial Is Everything You Need (article) - DataCamp"](https://www.datacamp.com/community/tutorials/r-data-import-tutorial).
* [https://rextester.com/l/r_online_compiler](https://rextester.com/l/r_online_compiler): Compilador de R en línea.
* [dim](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/dim).
* ["Read CSV From The Web"](http://www.programmingr.com/examples/read-csv-web/).
* Tutorialspoint ["R - Data Frames"](https://www.tutorialspoint.com/r/r_data_frames.htm).
* ["Objetos en R: Data Frames y Listas"](http://www.dma.ulpgc.es/profesores/personal/stat/cursoR4ULPGC/6g-Data_frames-Listas.html).
* ["Data Frame"](http://www.r-tutor.com/r-introduction/data-frame).
* One R Tip a Day [@RLangTip ‏](https://twitter.com/RLangTip): Noticias sobre **R** en Twitter. Sigue también el hashtag [#rstats](https://twitter.com/hashtag/rstats?src=hash).
* ["names function | R Documentation"](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/names).
* ["Data structures · Advanced R."](http://adv-r.had.co.nz/Data-structures.html).
* ["How to Play With Attributes in R - dummies"](https://www.dummies.com/programming/r/how-to-play-with-attributes-in-r/).

