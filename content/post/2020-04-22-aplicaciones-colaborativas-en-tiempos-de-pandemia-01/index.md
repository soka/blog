---
title: Aplicaciones colaborativas en tiempos de pandemia (part 1)
subtitle: Aplicaciones mensajería, reuniones, videoconferencia, documentación...
date: 2020-04-22
draft: true
description: 
tags: ["Aplicaciones"]
---

La lógica del sistema actual no ha podido responder a la crisis que vivimos. La búsqueda de beneficio ademas no diferencia entre necesidades 

Hemos descubierto que somos interdependientes

Se ha descubierto el sistema de clase, no es el mismo el confinamiento.

Fracaso de la globalización, mascarillas robadas y fraudes entre países [1], era más barato producir en China y por eso dejamos de producir muchos productos.

1. https://elpais.com/sociedad/2020-04-02/la-guerra-de-las-mascarillas-enfrenta-a-europa-y-eeuu.html


## Crisis, anonimato y privacidad

Si algo ha crecido en tiempos de confinamiento, más que los datos que consumimos son los datos que producimos. Por encima de la necesidad de sentirnos informarnos está la necesidad de relacionarnos, cada una de nuestras interacciones se convierte en dato. ¿De quien son estos datos?¿En manos de quien están?¿Que herramientas estamos "alimentando" con nuestros datos?  
 
El COVID-19 ha puesto de nuevo sobre la mesa el debate, gobiernos e instituciones publicas se alían con empresas privadas poniendo en marcha prácticas cuando menos dudosas.

Francia pide a Apple que rebaje su seguridad en sus smartphone para poder coger el control del bluethoot y poner en marcha un sistema de rastreo, echando mano de la neolengua [1] (referencia a la novela de 1984 de Orwell) lo llaman apps “de desconfinamiento” [2].

Pero esto no es nuevo, el episodio de los _«vanishing books»_ uno de los casos más sonados y que resulta paradójico es Amazon, en 2009 el gigante de ventas de todo tipo de productos en línea borro de los lectores electrónicos Kindle de miles de personas las novelas 1984 y Rebelión en la granja, de George Orwell [3][4], esto ilustra el poco control que tenemos sobre "nuestros" dispositivos. El hecho viene a ser lo mismo que si de repente entrase alguien en tu casa y se llevase un sofá sin preguntar ni decir nada, de repente no sabemos si el producto es nuestro o nos lo alquila Amazon y puede disponer de el como quiera.


**Enlaces**:

1. es.wikipedia.org [Neolengua](https://es.wikipedia.org/wiki/Neolengua).
2. elpais.com [Francia pide a Apple y a Google que limiten la privacidad de los usuarios para crear su ‘app’ de rastreo](https://elpais.com/tecnologia/2020-04-23/francia-pide-a-apple-y-google-que-limiten-la-privacidad-de-los-usuarios-para-crear-su-app-de-rastreo.html).
3. hipertextual.com [Amazon habría usado datos de vendedores para copiar sus productos y convertirse en la competencia](https://hipertextual.com/2020/04/amazon-copia-vendedores-coronavirus).
4. [Amazon borra 1984 y Rebelión en la granja de sus lectores electrónicos Kindle](https://www.libertaddigital.com/internet/amazon-borra-1984-y-rebelion-en-la-granja-de-sus-lectores-electronicos-kindle-1276365399/).
5. [Amazon, el Kindle y la mano negra que se lleva tus libros]().

## ¿Tienen algo que ver el software libre y el COVID-19?

"La naturaleza distribuida, colaborativa, y de escala lateral de IoT alterará de manera radical la manera de manufacturar, comercializar y enviar productos en la era que se aproxima." [La Sociedad de coste marginal cero - Jeremy Rifkin].

Pues probablemente más de lo que pensamos, las TICs en general están jugando un papel crucial para facilitar las comunicaciones en general, en partícular destacan su papel en el ámbito médico para el desarrollo rápido de nuevas soluciones o en la enseñanza por decir algunos.

En este momento de crisis dos modelos económicos se ven confrontados más que nunca, 

Mientras el gobierno discutía sobre quien compraba o como distribuía el material médico makers de todo el estado ya estaban organizados usando redes telemáticas. En Telegram han proliferado canales "CoronavirusMakers" incluso organizados por localidades o provincias.

Cuando hablamos del mundo de la cultura libre y el movimiento maker, la iniciativa más mediática probablemente ha sido los talleres de [makers](https://es.wikipedia.org/wiki/Cultura_maker) y [Fab lab-s](https://es.wikipedia.org/wiki/Fab_lab) de comunidades locales donde han sabido organizar para fabricar pantallas faciales, este es un ejemplo de solidaridad y trabajo en común resultado del esfuerzo de cientos de personas trabajando con impresoras 3D personales, en Internet se pueden encontrar modelos de respiradores y pantallas que se pueden descargar y usar libremente.- 

Mientras la industria del software está tratando de hacer su parte, dando acceso a los usuarios a versiones de prueba de programas propietarios. Pero, antes de empezar y aprovecharse de este gesto tan generoso, deberías leer la letra pequeña. Lo que parece una gran ayuda ahora, podría convertirse en una carga mañana. [3]

### Enlaces

1. Wikipedia [Proyecto RepRap](https://es.wikipedia.org/wiki/Proyecto_RepRap): El autor del proyecto describe la autorreplicación como la habilidad de producir los componentes necesarios para construir otra versión de sí misma. 
2. [Web oficial de RepRap](https://reprap.org/wiki/RepRap).
3. fsfe.org [¡Freeware, Software Libre y la Crisis del Coronavirus - Elige tus herramientas con sabiduría!](https://fsfe.org/news/2020/news-20200330-01.es.html).
4. [Conocimiento libre e impresión 3D](http://diwo.bq.com/conocimiento-libre-e-impresion-3d/).

Esta iniciativa que surge desde abajo no hubiese sido posible sin el espíritu de cooperación que emana de las licencias de software y hardware libres.

Estos ejemplos confrontados a los modelos centralizados gubernamentales dejan aún más al descubierto y en evidencia la falta de operatividad y coordinación de los segundos.

En tiempos ... cuando todo el planeta está sufriendo está situación surgen nuevas oportunidades 

Volver a la "normalidad"? Volver a la "normalidad" no debería ser una opción, la situación actual debería usarse como motor tractor para cambiar el mundo aunque sea un poco. Dejar una huella.  Esa normalidad donde controla al ciudadano y se convierte en un producto de consumo.

https://fsfe.org/news/2020/news-20200403-01.es.html
https://fsfe.org/news/2020/news-20200330-01.html

https://wiki.fsfe.org/Activities/FreeSoftware4RemoteWorking


## Introducción a la comunicación

_“Bajo toda arquitectura de información se esconde una estructura de poder”_ (eslogan ciberpunk).

En tiempos de confinamiento si quieres colaborar o trabajar con otras personas sólo tienes la opción de abrazar las tecnologías de comunicaciones y las nuevas TIC que te ayudarán a acortar distancias.

Estas crisis está pasando un examen sorpresa sobre transformación digital a la sociedad en general, ha condicionado la forma de trabajar de muchos sectores, desde la enseñanza al pequeño comercio y logísticas de reparto creativas y espontaneas como las redes comunitarias.

La verdad es que en muchos aspectos, lo más fácil es comunicarse de cara a cara. Cuando tus interlocutores están frente a ti es más fácil dedicarles toda tu atención, notar señales no verbales y crear un vínculo por medio de experiencias compartidas. A mi mismo me pasa, mi atención salta de un estimulo a otro con diferentes pantallas, tropecientas pestañas abiertas en el navegador y varios proyectos en marcha en mi cabeza, la videoconferencia es otra pestaña más que tengo abierta.

Depender de Internet para comunicarse puede ser complicado. **El 93% de la comunicación es [no verbal](https://es.wikipedia.org/wiki/Comunicaci%C3%B3n_no_verbal)** por lo que la tecnología actual puede traducir el espectro de la expresión humana sólo de manera parcial, la importancia del lenguaje no verbal en la comunicación se expresa fácilmente como la regla 55-38-7 de Albert Mehrabian.

Es posible que en algún momento hayas escuchado (o leído) que sólo el **7% del potencial de la comunicación recae en las palabras (lenguaje verbal)**, frente al 93% restante, que recae en el lenguaje corporal (55%) y el lenguaje paraverbal o uso de la voz (38%)…Dicho en otras palabras, la comunicación es un 93% no verbal.

Si esto fuera cierto, cuando lees los mensajes de tu aplicación de mensajería predilecta, el potencial del mensaje se vería reducido a un 7% ya que sólo hay lenguaje en palabras.

Incluso las videoconferencias, en las que sí es posible captar expresiones faciales y cierto lenguaje corporal (que constituye el [55% de la comunicación](https://es.wikipedia.org/wiki/Comunicaci%C3%B3n_no_verbal)), tienen sus limitaciones. Durante la conversación te estás dirigiendo a una cabeza flotante en la pantalla y si a eso le sumas que a veces tu conexión no es muy buena y a cada rato se está cargando el video, descifrar las expresiones faciales y el lenguaje corporal se vuelve bastante complicado.

## Cultura del trabajo remoto

https://blog.trello.com/es/trabajo-remoto
https://blog.trello.com/es/teletrabajo-desde-casa

## Herramientas

¿Cómo puedes crear una infraestructura que imponga orden pero que también fomente la creatividad?  Consigue las herramientas apropiadas.

https://www.magicpad.io/

https://wwwhatsnew.com/2019/12/25/como-crear-un-mensaje-completamente-secreto-en-internet/?utm_source=feedburner&utm_medium=email&utm_campaign=Feed%3A+WwwhatsNew+%28Wwwhat%27s+new%3F+-+Tecnolog%C3%ADa%2C+apps+y+marketing+online%29

https://swiso.org/

## Enlaces

**Comunicación general:**

- Wikipedia ["Comunicación no verbal"](https://es.wikipedia.org/wiki/Comunicaci%C3%B3n_no_verbal).
- ["Comunicación"](https://es.wikipedia.org/wiki/Comunicaci%C3%B3n).
- ["La regla 55-38-7 y la importancia del lenguaje no verbal"](https://www.google.com/search?client=firefox-b-e&q=Comunicaci%C3%B3n+no+verbal+porcentaje).

**Equipos de trabajo:**

- ["Atención equipos: !Las reuniones de trabajo efectivas han llegado para quedarse!"](https://blog.trello.com/es/reuniones-trabajo-efectivas-trello).


**Software libre, makers... y COVID-19:**

- ["Europa abandona su aplicación de código abierto contra el covid-19"](https://www.elsaltodiario.com/coronavirus/europa-abandona-su-aplicacion-de-codigo-abierto-sin-rastreo-frente-a-google-y-apple).
- ["700 'makers' elaboran en Navarra 14.000 máscaras de protección facial desde casa con impresoras 3D"](https://www.noticiasdenavarra.com/navarra/2020/04/01/7oo-makers-han-elaborado-14000/1035198.html).


- ["FreeSoftware4RemoteWorking"](https://wiki.fsfe.org/Activities/FreeSoftware4RemoteWorking).

https://github.com/gnab/remark
https://www.pinterest.es/pin/480477853975546945/



https://trello.com/es/remote-work-guide?hsCtaTracking=7debbf18-1f19-49fb-b3ac-ef52ccb28588%7Ce486d00f-a46c-4354-9903-d2a3f052f7dd

https://blog.trello.com/es/dinamicas-de-team-building?utm_source=newsletter&utm_medium=email&utm_campaign=trello-es_newsletter

https://blog.trello.com/es/desventajas-del-teletrabajo?utm_source=newsletter&utm_medium=email&utm_campaign=trello-es_newsletter


https://www.bilib.es/documentos/Taller_de_Migracion.pdf


https://www.fsf.org/blogs/community/how-to-livestream-a-conference-in-just-under-a-week


https://www.clarin.com/cultura/byung-chul-vamos-feudalismo-digital-modelo-chino-podria-imponerse_0_QqOkCraxD.html


https://vimeo.com/409485360


https://www.eduappcenter.com/

http://www.feccoo-madrid.org/noticia:475416--CCOO_denuncia_que_la_Consejeria_de_Educacion_ha_cedido_datos_personales_a_Microsoft_Teams


https://clickclickclick.click/#9df5a52a8efa33477fa70dedc0a07af6

https://es.wikipedia.org/wiki/Kiwix

https://h5p.org/


https://lamiradadelreplicante.com/2014/08/15/richard-stallman-en-el-tedxgeneva-libertad-en-la-era-digital/

https://commons.wikimedia.org/wiki/File:Free_software,_free_society-_Richard_Stallman_at_TEDxGeneva_2014.webm

https://gitlab.com/soka/charla-soberania-tecnologica



