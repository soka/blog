---
title: Algunas novedades interesantes en Google Workspace
subtitle: smart chips y listas de comprobación
date: 2021-07-02
draft: false
description: 
tags: ["Google","Novedades","Workspace"]
---

![](img/01.png)

<!-- vscode-markdown-toc -->
* 1. [Smart Chips](#SmartChips)
* 2. [Checklists](#Checklists)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='SmartChips'></a>Smart Chips

_smart chips_ es una funcionalidad super interesante, permite incrustar información de otras aplicaciones de Google en un documento, usando la `@` por delante podemos incrustar un contacto de otro usuario con el que compartimos el archivo, una cita de colendario, una hoja de cálculo, etc.

![](img/02.gif)

##  2. <a name='Checklists'></a>Checklists

Los _checklists_ o listas de comprobación son como otras listas con la salvedad de que pueden ser marcadas:

![](img/03.gif)

## Presentar contenidos en Meet sin salir del documnento

La última novedad es un botón nuevo junto al botón clásico de compartir para mostrar el contenido de un archivo abierto directamente en una reuníón de Meet.

##  3. <a name='Enlacesexternos'></a>Enlaces externos

- [https://www.blog.google/outreach-initiatives/education/Google-Workspace-for-Education-collaboration-and-security/](https://www.blog.google/outreach-initiatives/education/Google-Workspace-for-Education-collaboration-and-security/)