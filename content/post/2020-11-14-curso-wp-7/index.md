---
title: Contact Form 7
subtitle: formularios para WordPress
date: 2020-11-14
draft: false
description: Plugin para crear formularios
tags: ["Wordpress","WP - Curso","WP","WP - Plugins","WP - Formularios"]
---

![](img/04.png)

[**Contact Form 7**](https://es.wordpress.org/plugins/contact-form-7/) puede gestionar múltiples formularios de contacto, además, puedes personalizar el formulario y el contenido de los correos electrónicos de manera sencilla mediante un simple marcado.

En el [repositorio de plugins de WordPress](https://es.wordpress.org/plugins/) está valorado con 4 estrellas sobre 5, es compatible con las últimas versiones de WordPress y cuenta con **más de 5 millones de instalaciones activas**.

## Formulario de ejemplo

Recién instalado ya incorpora un formulario de ejemplo que podemos incrustar en una publicación con el _shortcode_ `[contact-form-7 id="99" title="Formulario de contacto 1"]`.

![](img/02.png)

Este es el resultado:

![](img/03.png)

## Personalizando un formulario

Los campos se añaden mediante etiquetas (cadenas de texto entre corchetes), existen dos tipos de etiquetas, para la plantilla del formulario y para la plantilla de correo electrónico. La sintáxis de ambas es diferente, por ejemplo usamos `[text* your-name]` en el formulario y `[your-name]` para el correo electrónico.

### Etiquetas de formulario

![](img/05.png)

- [Campos de texto](https://contactform7.com/text-fields/) (text, text*, email, email*, tel, tel*, url, url*, textarea and textarea*).
- [Números](https://contactform7.com/number-fields/) (number, number*, range and range*).
- [Selector de fecha](https://contactform7.com/date-field/) (date and date*).
- [Checkboxes, radio buttons, and menus](https://contactform7.com/checkboxes-radio-buttons-and-menus/) (checkbox, checkbox*, radio, select and select*).
- [Subida de archivos](https://contactform7.com/file-uploading-and-attachment/) (file and file*).
- [Cuestionario](https://contactform7.com/quiz/)  (quiz).
- [Casilla de verificación](https://contactform7.com/acceptance-checkbox/) (acceptance).
- [Botón de aceptar](https://contactform7.com/submit-button/).
- Menú desplegable (select*).
- Correo electrónico.

### Correo electrónico

La plantilla de correo es flexible, se pueden definir los campos típicos de un correo electrónico, el destinatario, el remitente, asunto, cabeceras adicional y el cuerpo del mensaje.

![](img/06.png)

En pestaña **Mensajes** se pueden definir diferentes mensajes de respuesta que visualiza el usuario cuando procesa el formulario.

## Enlaces externos

- [https://contactform7.com/docs/](https://contactform7.com/docs/).
- [https://contactform7.com/](https://contactform7.com/)
- [https://es.wordpress.org/plugins/contact-form-7/](https://es.wordpress.org/plugins/contact-form-7/).
- [https://es.wordpress.org/plugins/browse/popular/](https://es.wordpress.org/plugins/browse/popular/).
- [https://contactform7.com/editing-form-template/](https://contactform7.com/editing-form-template/).
- [https://contactform7.com/tag-syntax/](https://contactform7.com/tag-syntax/).
- [https://contactform7.com/setting-up-mail/](https://contactform7.com/setting-up-mail/).

**Otros _plugins_ a explorar para crear formularios**:

- [Ninja Forms Contact Form – The Drag and Drop Form Builder for WordPress](https://es.wordpress.org/plugins/ninja-forms/).
- [Gravity Forms](https://www.gravityforms.com/): Plugin de pago, seguramente el más potente y el que más opciones ofrece. 

[Creadores de _Popup_](https://webdesign.tutsplus.com/es/articles/20-best-popup-opt-in-wordpress-plugins--cms-28617) que quiero probar para combinar con Contact Form 7.
