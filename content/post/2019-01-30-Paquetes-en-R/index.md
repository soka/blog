---
title: Paquetes en R
subtitle: Como instalarlos y usarlos
date: 2019-01-30
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico"]
---

Lo primero es **ver los paquetes que tenemos instalados**, cuando lo ejecutamos inmediatamente se abre una nueva pestaña en RStudio con el listado de paquetes.

```
library()
```

En RStudio podemos verlos también en todo momento en la pestaña "Packages".

![](images/rstudio_packages_01.PNG)

# Instalar un paquete
 
Buscamos una función para dibujar gráficas que no tenemos instalada con [`?qplot`](https://www.rdocumentation.org/packages/ggplot2/versions/3.1.0/topics/qplot), para instalar el paquete usamos la instrucción `install.packages` (con el nombre del paquete entre comillas), **R** descarga el paquete de la Web de [CRAN](https://cran.r-project.org/).

```
install.packages("ggplot2")
```

Para poder usar el paquete después de instalado debemos **cargarlo** (sin comillas):

```
library(ggplot2)
```

Ahora ya deberiamos visualizar la ayuda del comando y plotear una gráfica de puntos. La función `ggplot` inicializa los datos necesarios del DF de entrada. No voy a detenerme en como funciona este paquete ya que se merece un post propio.  

```
df1 <- read.csv(file='http://kbroman.org/datacarp/portal_data_reduced.csv',
                header=TRUE,
                sep=',')

ggplot(df1, aes(x = weight, y = hindfoot_length)) + geom_point()
```

# Cabecera de un paquete

```R
> packageDescription('ggplot2')
Package: ggplot2
Version: 3.1.0
Title: Create Elegant Data Visualisations Using the Grammar of Graphics
Description: A system for 'declaratively' creating graphics, based on "The
         Grammar of Graphics". You provide the data, tell 'ggplot2' how to map
         variables to aesthetics, what graphical primitives to use, and it takes
         care of the details.
Authors@R: c( person("Hadley", "Wickham", , "hadley@rstudio.com", c("aut",
         "cre")), person("Winston", "Chang", , role = "aut"), person("Lionel",
         "Henry", , role = "aut"), person("Thomas Lin", "Pedersen", role =
         "aut"), person("Kohske", "Takahashi", role = "aut"), person("Claus",
         "Wilke", role = "aut"), person("Kara", "Woo", role = "aut"),
         person("RStudio", role = c("cph")) )
Depends: R (>= 3.1)
Imports: digest, grid, gtable (>= 0.1.1), lazyeval, MASS, mgcv, plyr (>= 1.7.1),
         reshape2, rlang (>= 0.2.1), scales (>= 0.5.0), stats, tibble,
         viridisLite, withr (>= 2.0.0)
Suggests: covr, dplyr, ggplot2movies, hexbin, Hmisc, lattice, mapproj, maps,
         maptools, multcomp, munsell, nlme, testthat (>= 0.11.0), vdiffr,
         quantreg, knitr, rgeos, rpart, rmarkdown, sf (>= 0.3-4), svglite (>=
         1.2.0.9001)
Enhances: sp
License: GPL-2 | file LICENSE
URL: http://ggplot2.tidyverse.org, https://github.com/tidyverse/ggplot2
BugReports: https://github.com/tidyverse/ggplot2/issues
LazyData: true
Collate: 'ggproto.r' 'ggplot-global.R' 'aaa-.r' 'aes-calculated.r' .....
VignetteBuilder: knitr
RoxygenNote: 6.1.0
Encoding: UTF-8
NeedsCompilation: no
Packaged: 2018-10-24 18:49:13 UTC; hadley
Author: Hadley Wickham [aut, cre], Winston Chang [aut], Lionel Henry [aut],
         Thomas Lin Pedersen [aut], Kohske Takahashi [aut], Claus Wilke [aut],
         Kara Woo [aut], RStudio [cph]
Maintainer: Hadley Wickham <hadley@rstudio.com>
Repository: CRAN
Date/Publication: 2018-10-25 04:30:25 UTC
Built: R 3.5.2; ; 2019-01-24 19:25:51 UTC; windows

-- File: C:/Users/i.landajuela/Documents/R/win-library/3.5/ggplot2/Meta/package.rds 
> 
```

Documentación [packageDescription function | R Documentation](https://www.rdocumentation.org/packages/utils/versions/3.5.2/topics/packageDescription).

# Datasets de un paquete

```R
data(package='ggplot2')
```
Se abre en una nueva pestaña de RStudio:

```
Data sets in package ‘ggplot2’:

diamonds                   Prices of 50,000 round cut diamonds
economics                  US economic time series
economics_long             US economic time series
faithfuld                  2d density estimate of Old Faithful data
luv_colours                'colors()' in Luv space
midwest                    Midwest demographics
mpg                        Fuel economy data from 1999 and 2008 for 38 popular
                           models of car
msleep                     An updated and expanded version of the mammals sleep
                           dataset
presidential               Terms of 11 presidents from Eisenhower to Obama
seals                      Vector field of seal movements
txhousing                  Housing sales in TX
```

# Contenido del paquete

```R
help(package='ggplot2')
```
![](images/rstudio_package_help_01.PNG)

# Paquetes recomendados

**Generales**:

* [**"fBasics: Rmetrics - Markets and Basic Statistics"**](https://cran.r-project.org/web/packages/fBasics/index.html): **Provides a collection of functions to explore and to investigate basic properties of financial returns and related quantities**. The covered fields include techniques of explorative data analysis and the investigation of distributional properties, including parameter estimation and hypothesis testing. Even more there are several utility functions for data handling and management.
* ["car: Companion to Applied Regression"](https://cran.r-project.org/web/packages/car/index.html): Functions to Accompany J. Fox and S. Weisberg, An R Companion to Applied Regression, Third Edition, Sage, in press.
* ["Hmisc: Harrell Miscellaneous"](https://cran.r-project.org/web/packages/Hmisc/index.html): Contains many functions useful for data analysis, high-level graphics, utility operations, functions for computing sample size and power, importing and annotating datasets, imputing missing values, advanced table making, variable clustering, character string manipulation, conversion of R objects to LaTeX and html code, and recoding variables.
* ["UsingR: Data Sets, Etc. for the Text "Using R for Introductory Statistics", Second Edition"](https://cran.r-project.org/web/packages/UsingR/index.html): A collection of data sets to accompany the textbook "Using R for Introductory Statistics," second edition.
* ["MASS: Support Functions and Datasets for Venables and Ripley's MASS"](https://cran.r-project.org/web/packages/MASS/index.html): Functions and datasets to support Venables and Ripley, "Modern Applied Statistics with S" (4th edition, 2002).
* ["vcd: Visualizing Categorical Data"](https://cran.r-project.org/web/packages/vcd/index.html): Visualization techniques, data sets, summary and inference procedures aimed particularly at categorical data. Special emphasis is given to highly extensible grid graphics. The package was package was originally inspired by the book "Visualizing Categorical Data" by Michael Friendly and is now the main support package for a new book, "Discrete Data Analysis with R" by Michael Friendly and David Meyer (2015).
* ["DescTools: Tools for Descriptive Statistics"](https://cran.r-project.org/web/packages/DescTools/index.html): A collection of miscellaneous basic statistic functions and convenience wrappers for efficiently describing data. The author's intention was to create a toolbox, which facilitates the (notoriously time consuming) first descriptive tasks in data analysis, consisting of calculating descriptive statistics, drawing graphical summaries and reporting the results. The package contains furthermore functions to produce documents using MS Word (or PowerPoint) and functions to import data from Excel.
* ["DAAG: Data Analysis and Graphics Data and Functions"](https://cran.r-project.org/web/packages/DAAG/index.html): Various data sets used in examples and exercises in the book Maindonald, J.H. and Braun, W.J. (2003, 2007, 2010) "Data Analysis and Graphics Using R".

**Especializados:**

* ["nortest: Tests for Normality"](https://cran.r-project.org/web/packages/nortest/index.html).
* ["binom: Binomial Confidence Intervals For Several Parameterizations"](https://cran.r-project.org/web/packages/binom/index.html).
* ["binGroup: Evaluation and Experimental Design for Binomial Group Testing"](https://cran.r-project.org/web/packages/binGroup/index.html).

# CRAN Task Views

Para facilitar la búsqueda de paquetes por categorías o vistas podemos consultar la sección [Task Views](https://cran.r-project.org/web/views/) de CRAN.

![](images/cran_web_task_views_01.PNG)

El paquete "ctv" nos ayuda a instalar los paquetes de una vista o categoría.

Con el siguiente comando se instala un conjunto de paquetes de la categoría ["Econometrics"](https://cran.r-project.org/web/views/Econometrics.html):

```R
install.packages("ctv") #
library(ctv)
??ctv
install.views("Econometrics")
``` 


# Ejercicios

* r \ intro \ 08-paquetes \ [paquetes.R](https://gitlab.com/soka/r/blob/master/intro/08-paquetes/paquetes.R).

# Enlaces 

* [Mi repositorio **R** en GitLab](https://gitlab.com/soka/r).
* [Posts sobre **R** en este blog](https://soka.gitlab.io/blog/tags/r/).
* [Posts básicos sobre **R** ](https://soka.gitlab.io/blog/tags/r-b%C3%A1sico/): He creado una subcategoría para los posts básicos sobre R.

**Enlaces externos**:

* ["HOW TO INSTALL AND LOAD GGPLOT2 IN R"](https://www.dummies.com/programming/r/how-to-install-and-load-ggplot2-in-r/).
* Departamento de Matemáticas, ULPGC ["Librerías en R"](http://www.dma.ulpgc.es/profesores/personal/stat/cursoR4ULPGC/5-librerias.html).