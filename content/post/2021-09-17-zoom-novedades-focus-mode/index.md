---
title: Nuevas funciones en Zoom 
subtitle: Modo enfoque para que los asistentes se mantengan concentrados
date: 2021-09-17
draft: false
description: Modo enfoque para que los asistentes se mantengan concentrados
tags: ["Zoom","Novedades","Updates"]
---

<!-- vscode-markdown-toc -->
* 1. [Modo Enfoque](#ModoEnfoque)
* 2. [Otras novedades](#Otrasnovedades)
* 3. [Enlaces internos](#Enlacesinternos)
* 4. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Agosto y Septiembre han sido dos meses llenos de novedades e interesantes funcionalidades.

##  1. <a name='ModoEnfoque'></a>Modo Enfoque

El [9 de Agosto](https://support.zoom.us/hc/en-us/articles/4406810842509-Release-notes-for-August-9-2021) se suma una nueva función, el [**Modo enfoque**](https://support.zoom.us/hc/en-us/articles/360061113751) permite que los asistentes se mantengan concentrados, cuando el administrador activa esta función **los participantes de una reunión no podrán ver al resto** (solo al anfitrion, coanfitriones y a sí mismos), está pensado especialmente para entornos educativos donde los alumnos y alumnas bajo supervisión puedan estar desarrollando una tarea o escuchando una lección, sin ser distraidos por sus compañeras. Otro escenario posible es un Webinar donde la figura principal que queremos destacar es una ponente y/o l@s participantes no se conocen entre si ni necesitan conversar o interactuar. 

Antes de comenzar se debe [habilitar esta función](https://support.zoom.us/hc/en-us/articles/360061113751) entre los ajustes de la cuenta.

![](img/01.PNG)

Una vez habilitado durante es muy fácil habilitarlo durante una reunión, en la barra de controles en el menú "More" se activa.

![](img/02.PNG)

Cuando comienza el modo enfoque todos los participantes reciben una notificación:

![](img/03.PNG)

Así se vería después de activar esta función:

![](img/04.PNG)

##  2. <a name='Otrasnovedades'></a>Otras novedades

[22 de Agosto](https://support.zoom.us/hc/en-us/articles/4407041545997-Release-notes-for-August-22-2021):

* [**Bloquear compartir pantalla**](https://blog.zoom.us/focus-mode-limit-screen-sharing-seamless-meeting-transfer/): Para prevenir que se comparta información sensible los administradores pueden aplicar restricciones a grupos o usuarios específicos para prevenir que compartan la pantalla. 
* [**Transferir la reunión entre el ordenador y el móvil**](https://support.zoom.us/hc/en-us/articles/4407041545997-Release-notes-for-August-22-2021). .



##  3. <a name='Enlacesinternos'></a>Enlaces internos

* [La nueva vista inmersiva de Zoom](https://soka.gitlab.io/blog/post/2021-04-28-zoom-vista-inmersiva/).
* [Curso de Zoom [borrador]](https://soka.gitlab.io/blog/post/2020-10-11-taller-zoom/).
* [Como programar un evento en streaming](https://soka.gitlab.io/blog/post/2020-09-17-taller-streaming-zoom-youtube/).

##  4. <a name='Enlacesexternos'></a>Enlaces externos

* [Release notes for August 9, 2021](https://support.zoom.us/hc/en-us/articles/4406810842509-Release-notes-for-August-9-2021).
* [https://support.zoom.us/hc/en-us/articles/360061113751-Focus-mode-](https://support.zoom.us/hc/en-us/articles/360061113751-Focus-mode-).
* [https://support.zoom.us/hc/en-us/sections/201214205-Release-Notes](https://support.zoom.us/hc/en-us/sections/201214205-Release-Notes).
* [Incoming Updates! Start Using Focus Mode, Limit Screen Sharing, and Seamlessly Transfer Meetings](https://blog.zoom.us/focus-mode-limit-screen-sharing-seamless-meeting-transfer/).
* [ZOOM SUMA MÁS FUNCIONES PARA MEJORAR LA DINÁMICA DE LAS REUNIONES](https://wwwhatsnew.com/2021/08/28/zoom-suma-mas-funciones-para-mejorar-la-dinamica-de-las-reuniones/).