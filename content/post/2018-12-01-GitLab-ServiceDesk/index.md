---
title: GitLab - utilidad ServiceDesk
subtitle: Gestión de solicitudes de usuarios por correo
date: 2018-12-01
draft: false
description: ServiceDesk para subscripciones de pago 
tags: ["Aplicaciones","ServiceDesk","GitLab"]
---

![](images/242779.gif)

Recientemente he reanudado mi búsqueda de **aplicaciones de ticketing para la gestión de solicitudes de usuario**, del estilo de [Mantis](https://www.mantisbt.org/) o [osTicket](https://docs.osticket.com/en/latest/), que tenga comportamiento social como menciones entre miembros del equipo o un backend visual inspirado en los tableros Kanban para visualizar el trabajo en curso (como [Trello](https://trello.com)). Buscando en repositorios de GitHub y **GitLab** he descubierto una funcionalidad llamada **ServiceDesk** que ofrece **GitLab** para subscripciones de pago, por fortuna los planes de pago son muy económicos y yo tengo una cuenta básica contratada para [mis proyectos](https://gitlab.com/soka).

La funcionalidad **ServiceDesk** ofrece la **integración del correo electrónico para abrir solicitudes de usuarios sin necesidad de contar con una cuenta en GitLab**, cada proyecto tiene asociada una dirección de correo electrónico que permite a los usuarios solicitar cualquier tipo de soporte enviando un simple correo (admite formato [MarkDown](https://es.wikipedia.org/wiki/Markdown)). Cada correo enviado a esa dirección abre un issue o solicitud para el proyecto, lo interesante de la cuestión es que la comunicación funciona en sentido inverso, cuando añadimos nuevos comentarios al issue los envía por correo al remitente original posibilitando que este a su vez responda al correo con nuevos comentarios.  

![](images/new-issue.PNG)

Cuando escribe el primer correo el remitente recibe como respuesta de forma automática otro correo de confirmación con el identificador del ticket (también la opción de para dejar de subscribirse y de recibir más correos), en GitLab se crea un nuevo issue donde podemos añadir nuestros comentarios que le llegan como correos al remitente original.    

![](images/new-issue-edit.PNG)

Los issues de GitLab poseen atributos editables básicos de un sistema de gestión de tickets, se pueden asignar a otros usuarios, asociar a un hito concreto, asociarlas con una o mas **etiquetas con colores para categorizarlas** (por prioridad por ejemplo), además podemos seguir la actividad del issue por correo con las **notificaciones** habilitadas. En los comentarios se permiten hacer **menciones a otros usuarios** de GitLab en el proyecto. Se puede establecer una fecha de vencimiento y tiene algunas funciones de [time tracking](https://gitlab.com/help/workflow/time_tracking.md) incrustado en los propios comentarios. 

Todas las solicitudes se pueden ver dipuestas en un tablero visual ordenadas en columnas estilo Kanban.  

![](images/board.PNG)   

Como conclusión, el gestor de tareas es bastante básico pero espero que sigan agregando nuevas funcionalidades en nuevas versiones de GitLab, el servicio **ServiceDesk** se puede completar con la sección **Wiki** (bastante básico también) con guías o manuales internos para los agentes que atienden los tickets.

# Enlaces externos

* [Demo - GitLab Service Desk](https://about.gitlab.com/2017/05/09/demo-service-desk/) May 9, 2017  - Victor Wu: n 9.1, we introduced our new Service Desk feature, allowing your customers to reach you inside GitLab simply by using a support email address.
* [Service Desk [PREMIUM]](https://gitlab.com/help/user/project/service_desk).
* GitLab Documentation > User documentation > Projects > [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html).
* [The 8 Best Free and Open Source Help Desk Software Tools](https://blog.capterra.com/the-7-best-free-help-desk-software-tools/).
 

