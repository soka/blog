---
title: Pizarras digitales colaborativas
subtitle: Herramientas de trabajo colaborativo remoto
date: 2020-04-28
draft: false
description: 
tags: ["Aplicaciones","Web","Pizarra digital","Lienzo","Trabajo colaborativo"]
---

He aquí algunas pizarras digitales Web para trabajar de forma colaborativa, la mayoría de las herramientas que he probado siguen un modelo _freemium_, se pueden usar de forma gratuita con algunas restricciones y ofrecen funcionalidades avanzadas previo pago.

## Witeboard

Nada más sencillo para comenzar a trabajar, simplemente accede a este enlace [https://witeboard.com/](https://witeboard.com/) y se genera una nueva pizarra con un identificador nuevo en la URL, para añadir nuestros colaboradores es tan fácil como compartir el enlace. Las opciones de edición y las herramientas que ofrece son muy básicas pero a veces no hace falta más.

![](img/02.png)

A favor que no requiere registro previo, en pocos segundos estamos trabajando de forma colaborativa.

## AWW - A Web Whiteboard

[AWW](https://awwapp.com/) es muy parecida a la anterior, ofrece entre sus herramientas un lápiz para dibujar de forma libre aunque **con algunas opciones adicionales de grosor y tipo de lápiz**. Se comparte con otras personas usando un enlace que permite que el resto del equipo pueda editar y realizar aportaciones.

![](img/03.png)

## Ziteboard

Otra similar a las anteriores es [Ziteboard](https://ziteboard.com/) pero creo que es con la que me quedo.

![](img/04.png)

## Referencias externas

- [AWW A web whiteboard](https://awwapp.com/).
- [Witeboard](https://witeboard.com/).
- [Ziteboard](https://ziteboard.com/).


