---
title: El sesgo cognitivo de Dunning-Kruger
subtitle: Tip ciberseguridad Nº 1
date: 2021-06-29
draft: true
description: 
tags: ["Dunning-Kruger","Sesgos","Cognición"]
---

Seguramente en algún momento te has topado con alguna persona que cree saberlo todo, pero en realidad no sabe nada, te seguirás encontrando con personas así toda tu vida. Esto ocurre por los sesgos cognitivos, cuando alguien sobrestima sus capacidades y desconoce su escasa habilidad está siendo "victima" del efecto [Dunning-Kruger](https://es.wikipedia.org/wiki/Efecto_Dunning-Kruger), esto se puede explicar en pocas palabras, mientras más creemos que sabemos más ignorantes somos.

En 1995 McArthur Wheeler atracó dos bancos a plena luz del día, no se oculto especialmente y las cámaras de vigilancia tomaron buenas imágenes de su rostro, ese mismo día fue detenido. En el momento de su atraco Wheeler se mostró incrédulo porque le hubiesen cogido, no comprendía cómo había sido reconocido si se había echado zumo de limón en la cara, lo que, a su juicio, lo debería hacer invisible, la policía concluyó que su capacidad no estaba mermada ni estaba bajo el efecto de las drogas, simplemente estaba terriblemente equivocado.

Los psicólogos sociales David Dunning y Justin Kruger realizaron varios experimentos, los individuos competentes tienden a asignar tareas difíciles a individuos que no tienen habilidad suficiente para completarlas en la creencia de que dichas tareas son sencillas de realizar, mientras que los individuos incompetentes tienden a acometer tareas para las que no están preparados, y pueden no ser capaces de reconocer su fracaso.  

> "la mala calibración del incompetente se debe a un error sobre uno mismo, mientras que la mala calibración del altamente competente se debe a un error sobre los demás"

Seguramente parte del problema es que para evaluar una competencia un individuo utiliza el conocimiento que tiene sobre la misma, si su conocimiento es pobre no tendrá herramientas para juzgarlo de forma objetiva. 

Estudio Universidad de Wellington donde 80% conductores pensaba que conducía mejor que la media, cosa que es imposible.

## Enlaces externos

- [https://es.wikipedia.org/wiki/Efecto_Dunning-Kruger](https://es.wikipedia.org/wiki/Efecto_Dunning-Kruger)