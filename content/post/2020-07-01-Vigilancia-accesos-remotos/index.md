---
title: Jornada - Vigilancia de accesos remotos
subtitle: Contenidos de la jornada impartida por Talio
date: 2020-07-01
draft: false
description: 
tags: ["Sistemas","Seguridad","Vigilancia","Accesos"]
---

Jornada impartida por [http://www.talio.it/](http://www.talio.it/).

Las empresas y organizaciones han tenido históricamente el concepto de que lo que estaba en la organización era “bueno” o confiable y el exterior era “malo” o no confiable. Cada vez, más proveedores tienen acceso a nuestros activos desde el exterior. El teletrabajo se une a esta problemática de accesos remotos. El perímetro de la organización está roto. Ahora, la defensa ante incidentes está en la vigilancia.

La jornada tiene como propósito concienciar sobre la necesidad de tomar medidas de seguridad en el nuevo escenario que tenemos.  

El acceso remoto es una puerta de entrada a nuestra organización y como tal tenemos que tomar medidas para reducir el riego ante un incidente.

![](img/Vigilancia_de_accesos_remotos-01.png)

![](img/Vigilancia_de_accesos_remotos-02.png)

![](img/Vigilancia_de_accesos_remotos-03.png)

![](img/Vigilancia_de_accesos_remotos-04.png)

![](img/Vigilancia_de_accesos_remotos-05.png)

![](img/Vigilancia_de_accesos_remotos-06.png)

![](img/Vigilancia_de_accesos_remotos-07.png)

![](img/Vigilancia_de_accesos_remotos-08.png)

![](img/Vigilancia_de_accesos_remotos-09.png)

![](img/Vigilancia_de_accesos_remotos-10.png)

![](img/Vigilancia_de_accesos_remotos-11.png)

![](img/Vigilancia_de_accesos_remotos-12.png)

![](img/Vigilancia_de_accesos_remotos-13.png)

![](img/Vigilancia_de_accesos_remotos-14.png)

![](img/Vigilancia_de_accesos_remotos-15.png)

![](img/Vigilancia_de_accesos_remotos-16.png)

![](img/Vigilancia_de_accesos_remotos-17.png)

![](img/Vigilancia_de_accesos_remotos-18.png)

![](img/Vigilancia_de_accesos_remotos-19.png)

![](img/Vigilancia_de_accesos_remotos-20.png)

![](img/Vigilancia_de_accesos_remotos-21.png)

![](img/Vigilancia_de_accesos_remotos-22.png)

![](img/Vigilancia_de_accesos_remotos-23.png)

![](img/Vigilancia_de_accesos_remotos-24.png)

![](img/Vigilancia_de_accesos_remotos-25.png)

![](img/Vigilancia_de_accesos_remotos-26.png)

![](img/Vigilancia_de_accesos_remotos-27.png)

![](img/Vigilancia_de_accesos_remotos-28.png)

![](img/Vigilancia_de_accesos_remotos-29.png)

![](img/Vigilancia_de_accesos_remotos-30.png)

![](img/Vigilancia_de_accesos_remotos-31.png)

![](img/Vigilancia_de_accesos_remotos-32.png)

![](img/Vigilancia_de_accesos_remotos-33.png)

![](img/Vigilancia_de_accesos_remotos-34.png)

![](img/Vigilancia_de_accesos_remotos-35.png)

![](img/Vigilancia_de_accesos_remotos-36.png)


