---
title: La clave eres tú
subtitle: Recursos sobre gestión de contraseñas seguras
date: 2021-01-15
draft: false
description: 
tags: ["Seguridad","Contraseñas","Sysadmin","Usuarios","Ciberseguridad","INCIBE"]
---

<!-- vscode-markdown-toc -->
* 1. [LO BÁSICO SOBRE UN SISTEMA DE IDENTIFICACIÓN, AUTENTICACIÓN Y AUTORIZACIÓN](#LOBSICOSOBREUNSISTEMADEIDENTIFICACINAUTENTICACINYAUTORIZACIN)
* 2. [TIPOS DE AUTENTICACIÓN](#TIPOSDEAUTENTICACIN)
* 3. [AUTENTICACIÓN MULTIFACTOR](#AUTENTICACINMULTIFACTOR)
* 4. [BUENAS PRÁCTICAS Y CONTRASEÑAS SEGURAS](#BUENASPRCTICASYCONTRASEASSEGURAS)
	* 4.1. [No compartir las contraseñas](#Nocompartirlascontraseas)
	* 4.2. [Contraseñas robustas](#Contraseasrobustas)
		* 4.2.1. [Características de una contraseña robusta](#Caractersticasdeunacontrasearobusta)
		* 4.2.2. [Generadores de contraseñas](#Generadoresdecontraseas)
		* 4.2.3. [Cómo utilizar la nemotecnia para crear y recordar contraseñas complejas y seguras](#Cmoutilizarlanemotecniaparacrearyrecordarcontraseascomplejasyseguras)
		* 4.2.4. [Herramientas para medir la fortaleza de una contraseña](#Herramientasparamedirlafortalezadeunacontrasea)
	* 4.3. [No reutilizar las contraseñas](#Noreutilizarlascontraseas)
		* 4.3.1. [direcciones de correo electrónico temporales o desechables](#direccionesdecorreoelectrnicotemporalesodesechables)
	* 4.4. [Caducidad y renovación periódica](#Caducidadyrenovacinperidica)
	* 4.5. [No usar el recordatorio de contraseñas](#Nousarelrecordatoriodecontraseas)
	* 4.6. [Autenticación de doble factor (2FA)](#Autenticacindedoblefactor2FA)
		* 4.6.1. [Algunas aplicaciones con 2FA](#Algunasaplicacionescon2FA)
	* 4.7. [Técnicas de autenticación externas](#Tcnicasdeautenticacinexternas)
* 5. [Controversia y antipatrones](#Controversiayantipatrones)
	* 5.1. [Anti-patron 1: Contraseñas largas](#Anti-patron1:Contraseaslargas)
* 6. [HERRAMIENTAS](#HERRAMIENTAS)
	* 6.1. [Gestores de contraseñas](#Gestoresdecontraseas)
* 7. [GESTIÓN PARA ADMINS](#GESTINPARAADMINS)
* 8. [Recursos del INCIBE (Insituto Nacional de Ciberseguridad)](#RecursosdelINCIBEInsitutoNacionaldeCiberseguridad)
	* 8.1. [Kit de concienciación para empresas (INCIBE)](#KitdeconcienciacinparaempresasINCIBE)
	* 8.2. [Políticas de seguridad para la PYME (INCIBE)](#PolticasdeseguridadparalaPYMEINCIBE)
	* 8.3. [Otros recursos INCIBE](#OtrosrecursosINCIBE)
* 9. [Basque Cybersecurity Centre (BCSC)](#BasqueCybersecurityCentreBCSC)
* 10. [Material formación "Hardening Windows"](#MaterialformacinHardeningWindows)
* 11. [Catálogo de herramientas](#Catlogodeherramientas)
	* 11.1. [Herramientas de detección y eliminación de virus online](#Herramientasdedeteccinyeliminacindevirusonline)
* 12. [Enlaces](#Enlaces)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Este post es una recopilación de diversas fuentes con algunos consejos básicos sobre contraseñas.

##  1. <a name='LOBSICOSOBREUNSISTEMADEIDENTIFICACINAUTENTICACINYAUTORIZACIN'></a>LO BÁSICO SOBRE UN SISTEMA DE IDENTIFICACIÓN, AUTENTICACIÓN Y AUTORIZACIÓN

A diario nos **identificamos, autenticamos y autorizamos** en varios sistemas, sin embargo muchas personas confunden el significado de estas palabras.

La **identificación es la capacidad de identificar de forma exclusiva** a un usuario de un sistema o una aplicación que se está ejecutando en el sistema, algunos ejemplos de la vida real: Número de la tarjeta de crédito, el IMEI de un dispositivo, la matrícula de nuestro coche o el número de DNI.

La [**autenticación**](https://es.wikipedia.org/wiki/Autenticaci%C3%B3n#Tipos_de_autenticaci%C3%B3n) (o autentificación) o es el **acto o proceso de confirmar que algo (o alguien) es quien dice ser**, autorizar o acreditar (dar seguridad de que alguien o algo es lo que representa o parece según la RAE) a algo o alguien, proceso de verificar la identidad de una persona.

La autentificación sin identificación previa no tiene sentido; no tendría sentido comenzar a verificar antes de que el sistema supiera de quién es la autenticidad. Uno tiene que presentarse primero.

Por último, la **autorización** es la función de especificar los derechos o privilegios de acceso a los recursos, protege los recursos importantes de un sistema, ya que limita el acceso solamente a los usuarios autorizados y a sus aplicaciones. Impide que los recursos se utilicen sin la autorización necesaria.

Un ejemplo sencillo es como iniciamos una sesión en Google, primero introducimos nuestro correo electrónico que nos identifica como usuario de forma inequívoca ya que es único en el sistema, después Google nos solicita una contraseña, si coincide con la que tiene almacenada Google para ese usuario procedemos a autenticarnos. Finalmente el sistema nos concede el derecho a leer el correo y demás. Esta es una autorización.  

La característica principal de un sistema de **autenticación** es que debe soportar con cierto éxito los ataques (además de la usabilidad, precio, etc.). Algunas técnicas muy comunes para comprometer una cuenta con clave son por ejemplo los [ataques de fuerza bruta](https://es.wikipedia.org/wiki/Ataque_de_fuerza_bruta), [ataques de diccionario](https://es.wikipedia.org/wiki/Ataque_de_diccionario), [ingeniería social](https://es.wikipedia.org/wiki/Ingenier%C3%ADa_social_(seguridad_inform%C3%A1tica)) (los usuarios son el "eslabón débil") etc.

##  2. <a name='TIPOSDEAUTENTICACIN'></a>TIPOS DE AUTENTICACIÓN

Todo sistema de **autenticación** de usuarios se basa en la utilización de uno o varios de estos **factores** o categorías combinadas:

* **algo que sabes o conoces**: contraseñas, preguntas personales, etc.

![](img/27.jpg)

* **algo que eres**:  una característica física del usuario o un acto involuntario del mismo. Medidas [biométricas](https://es.wikipedia.org/wiki/Biometr%C3%ADa) (en el sentido literal y más simple biometría significa la medición de un ser vivo) como huellas digitales, los equipos más modernos suelen incorporar esta característica junto con MS Windows 10 (patrones oculares, verificación de voz, etc). Este es un método rápido y que no requiere ningún acto consciente (llevar algo encima como un tarjeta de identificación o tratar de recordar la contraseña), ya por curiosidad hace poco leí que se estaba usando el reconocimiento de las venas.
* **algo que posees**: [_tokens_ criptográficos](https://es.wikipedia.org/wiki/Token_de_seguridad) (_One Time Passwords_), la tarjeta de coordenadas del banco, _smartcard_ (tarjetas inteligentes) con circuitos integrados y RFID, etc.

##  3. <a name='AUTENTICACINMULTIFACTOR'></a>AUTENTICACIÓN MULTIFACTOR

![](img/01.png)

La **autenticación multifactor** ([MFA](https://es.wikipedia.org/wiki/Autenticaci%C3%B3n_de_m%C3%BAltiples_factores) en inglés) consiste en combinar varios de los métodos arriba descritos, la más conocida es la autenticación de dos factores (A2F), un ejemplo típico es usando la contraseña personal (algo que sabes) y un código enviado al teléfono móvil (algo que tienes) mediante SMS o una notificación de alguna APP.

![](img/02.png)

**Normalmente la contraseña es el más utilizado de los factores (algo que sabes o conoces)**, por eso me he centrado en una gestión de contraseñas segura.

##  4. <a name='BUENASPRCTICASYCONTRASEASSEGURAS'></a>BUENAS PRÁCTICAS Y CONTRASEÑAS SEGURAS

Sin más dilación voy a ofrecer algunos consejos sobre la gestión y uso de contraseñas seguras.

###  4.1. <a name='Nocompartirlascontraseas'></a>No compartir las contraseñas

> "Seña **secreta** que permite el acceso a algo, a alguien o a un grupo de personas antes inaccesible." [[RAE]](https://dle.rae.es/contrase%C3%B1a)

Siempre que me meto con temas de seguridad y contraseñas me viene a la cabeza [El inquietante caso del Ministro del Interior y su foto oficial con la contraseña «123456» en un post-it](https://www.microsiervos.com/archivo/seguridad/ministro-griego-contrasena.html), al parecer un ministro griego (de interior nada menos para mas mofa) posó para una foto con tan poca fortuna que debajo del monitor se puede observar un post-ip con una contraseña que además es <<123456>>, comete dos errores demenciales, usar una contraseña debil y dejarla expuesta a los ojos de cualquiera.

![](img/ministro-griego.jpeg)

Aunque la contraseña podría pertenecer a cualquier cosa: el WiFi, correo electrónico, etc. Hay que recordar que <<12345>> es también conocida como «la contraseña que cualquier capullo le pondría a su maleta» ([Spaceballs](https://www.youtube.com/watch?v=a6iW-8xPw3k)). Al poco tiempo [la Web del Ministerio de Seguridad](http://www.yptp.gr/index.php?option=ozo_content&perform=view&id=4287&Itemid=407&lang=GR&lang=?option=ozo_search&lang=EN) publicaba la misma foto pero recortada.

{{< youtube a6iW-8xPw3k >}}

La siguiente es variante es muy común, a menudo la **comodidad está reñida con la seguridad**, probablemente algún trabajador harto de olvidar la contraseña opto por facilitar su introducción sin necesidad de memorizarla, para el y para cualquiera claro.  

![](img/03.png)

Recuerdo un truco de un proveedor cuando me dedicaba a programar máquinas industriales, este consistía en dejar la contraseña del autómata a la vista pero sobraban algunos carácteres que sólo las personas autorizadas conocían, ¿simple pero efectivo no?.

Siguiendo con políticos, a [Boris Johnson](https://twitter.com/BorisJohnson/status/1244985949534199808?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1244985949534199808%7Ctwgr%5E&ref_url=https%3A%2F%2Fcyberprotectiongroup.com%2Fuk-prime-minister-leaks-meeting-id-on-zoom%2F) le debio parecer una buena idea publicar una captura de una videoconferencia, el problema es que en la esquina superior izquierda se ve el identificador de la sesión:

![](img/04.png)

Algo parecido hace [Kanye West desbloqueando su iPhone delante de las cámaras](https://www.youtube.com/watch?v=H_3ltF7tCoM), no sólo usa una contraseña fácil de adivinar (<<000000>>), además todo el planeta la sabe ahora gracias a que lo mostró a toda la TV.

{{< youtube H_3ltF7tCoM >}}

Aunque sean referencias en clave de humor (a pesar de tener la contraseña no creo que sea tan fácil acceder a algunos de estos sistemas) permiten recordarnos de forma fácil la importancia de usar contraseñas robustas, renovarlas periódicamente, etc. Aquí también cabe hablar sobre contraseñas inseguras contra gente insegura (lo siento señor ministro), sobre la formación y concienciación de los usuarios.

* **Es fundamental que LAS CONTRASEÑAS SEAN SECRETAS**, no debemos anotarlas ni compartirlas. Si otra persona conocedora de tu contraseña hiciera algo con tus credenciales de acceso, podrías ser responsable:
	* Las contraseñas son un tipo de **INFORMACIÓN PERSONAL E INTRANSFERIBLE**, nadie a excepción de su propietario debe conocerlas.
* ¡no debemos apuntarlas en papeles o post-it señor ministro!
	* Es necesario implantar una POLÍTICA DE MESAS LIMPIAS.
* no debemos  escribir  nuestras  contraseñas  en  correos  electrónicos  ni  en formularios web cuyo origen no sea confiable

Recuerda **siempre usar un sistema de bloqueo de sesión automático cuando no estés presente delante de la computadora**, no sea que te pase como en la película [Mision Imposible](https://www.youtube.com/watch?v=ar0xLps7WSY) donde el ordenador está ubicado en una sala super segura pero sin embargo la sesión no está bloqueada ni requiere un triste usuario y clave para acceder.

{{< youtube ar0xLps7WSY >}}

###  4.2. <a name='Contraseasrobustas'></a>Contraseñas robustas

_“I changed my password everywhere to ‘incorrect.’ That way when I forget it, it always reminds me, ‘Your password is incorrect'."_ - Anonymous.

Si lo que realmente quieres es lanzar un misil nuclear probablemente tengas más suerte probando con <<00000000>>, es la que usaron los [americanos para manejar los silos nucleares de los misiles en la base de Minuteman durante veinte años](https://news.sophos.com/es-es/2013/12/13/durante-20-anos-el-codigo-para-el-lanzamiento-de-misiles-nucleares-americano-era-00000000/).

Ni siquiera el propio Mark Zuckerberg está libre de culpa, [unos hackers publicaron su contraseña de LinkedIn](https://www.asianage.com/technomics/mark-zuckerberg-password-linkedin-was-dadada-103) y resulta que era 'dadada', sólo espero que no usase la misma para administrar Facebook.

![](img/30.gif)

En el siguiente [enlace](https://www.welivesecurity.com/la-es/2018/12/19/las-25-contrasenas-mas-populares-del-2018/) se puede encontrar una lista de las peores contraseñas del 2018 comúnmente usadas en la Web, que casualidad que <<123456>> esté en la primera posición. El problema no es que lo fuese en 2018 sino que durante 5 años las mismas contraseñas figuran en las primeras posiciones ([2015](https://www.teamsid.com/worst-passwords-2015/), [2014](https://www.teamsid.com/worst-passwords-of-2014/), [2013](https://www.teamsid.com/worst-passwords-of-2013/), [2012](https://www.teamsid.com/worst-passwords-of-2012/), [2011](https://www.teamsid.com/worst-passwords-of-2011/)), lo que no deja de ser preocupante porque no se aprecia una tendencia hacia la mejora. Unido al _factor comodidad_ probablemente **no hay una conciencia de seguridad**, la gente sigue pensando que a ellos no les va a pasar. Con la _democratización_ de las nuevas TICs Otro aspecto importante a tener en cuenta es el **tipo de usuario**, podemos encontrarnos desde menores a personas mayores.

[Wikipedia:10,000 most common passwords](https://en.wikipedia.org/wiki/Wikipedia:10,000_most_common_passwords).

![](img/10.jpg)

####  4.2.1. <a name='Caractersticasdeunacontrasearobusta'></a>Características de una contraseña robusta

* **Longitud**: tienen que ser largas (**más de 8 caracteres mínimo**, aunque en muchos sitios ya recomiendan que sean de 12-15).
* **Complejidad**: la contraseña debe tener no solo letras, sino números, mayúsculas y minúsculas y caracteres especiales (no usarlos sólo al principio o final). Un sistema muy conocido (y por eso mismo ha dejado de ser seguro) es reemplazar las vocales por número, la A es 4, E es 3, i es 1, etc.
* **NO deben contener** los siguientes tipos de palabras:
	* palabras sencillas en cualquier idioma (palabras de diccionarios).
	* **difícil de adivinar**: no usar nombres propios, fechas, lugares o datos de carácter personal. Se pueden averiguar o llegar a ellos usando cualquier dato público o de tu perfil en una RRSS.
	* Nunca usar palabras relacionadas entre usuario y contraseña.
	* palabras que estén formadas por caracteres próximos en el teclado, el típico ejemplo es "qwerty".
	* palabras excesivamente cortas.

![](img/12.jpg)

![](img/13.jpg)


También es importante que utilices **contraseñas fáciles de recordar pero difíciles de adivinar**. Un medio muy eficaz es el de utilizar combinaciones de varias palabras, que aunque aparentemente no tengan relación lógica entre ellas tú puedas relacionar para recordar. 

![](img/16.png)

Viñeta de [xkcd](https://xkcd.com/936/) sobre la fortaleza de contraseñas, y cómo varias palabras simples pueden ser más efectivas que sólo dos con símbolos y números.

Se ha comprobado que esta técnica es más efectiva que la de simplemente combinar mayúsculas, minúsculas, números y caracteres especiales en una contraseña corta. Estas no sólo son fórmulas predefinidas de esas que ya hemos recomendado no utilizar, sino que acaban siendo tan intrincadas que a veces acaban siendo tan difíciles de recordar que pierden todo el sentido.

El siguiente tuit no tiene desperdicio, coincidiendo con la celebración del [Día Mundial de la Contraseña](https://nationaltoday.com/world-password-day/) (primer jueves de mayo) Nutella publicó lo siguiente con el peor consejo posible _“Choose a word that’s already in your heart”_. Seguramente los diseñadores de la campaña estaban despedidos el día siguiente:

![](img/Nutella-Social-Media-Fail.png)

####  4.2.2. <a name='Generadoresdecontraseas'></a>Generadores de contraseñas

Existen **servicios para generar nuevas contraseñas**, esta Web es la que uso yo habitualmente [https://passwordsgenerator.net/](https://passwordsgenerator.net/), es muy sencilla de usar. LastPass ofrece esta Web para generar contraseñas [https://www.lastpass.com/es/password-generator](https://www.lastpass.com/es/password-generator).

![](img/08.png)

[Esta Web](https://www.passwordcard.org/es) permite crear una tarjeta con una matriz de carácteres que podamos usar para "esconder" la clave y guardarla impresa en algún lugar seguro.

![](img/generatecard-do-1.png)

####  4.2.3. <a name='Cmoutilizarlanemotecniaparacrearyrecordarcontraseascomplejasyseguras'></a>Cómo utilizar la nemotecnia para crear y recordar contraseñas complejas y seguras

Una **forma de conseguir contraseñas robustas y fáciles de recordar sin recurrir a un generador de contraseñas** es utilizar reglas nemotécnicas aplicadas a una frase (obtenido del kit de concienciación de INCIBE):

1. Seleccionamos una frase: «en un lugar de la mancha»
2. Hacemos uso de mayúsculas: «En un lugar de la Mancha»
3. Incluimos el servicio: «En un lugar de la Mancha Correo»
4. Añadimos números: «En un lugar de la Mancha Correo de 2019»
5. Añadimos caracteres especiales: «En un lugar de la Mancha Correo de 2019!»
6. La comprimimos utilizando la primera letra de cada palabra: «EuldlMCd2019!»

Otra forma típica es añadir números y símbolos a la clave, si por ejemplo usase mi fecha de nacimiento "100579" (ojo, es un dato personal fácil de averiguar) le puedo **añadir complejidad** metiendo guiones, corchetes y cambiando la forma, por ejemplo "[10-mayo-79]", esto al menos es infinitamente mejor que como he empezado.

Debemos usar diferentes contraseñas en cada servicio, en ese caso podemos partir de una base y añadir algo más relativo al servicio, por ejemplo para Twitter "12345EsUnPasswordMalo!ParajoAzul".

En este enlace de Genbeta ofrecen una receta para crear una contraseña segura y como para crear una [contraseña compleja a partir de una base](https://www.genbeta.com/seguridad/especial-contrasenas-seguras-consejos-para-mejorar-la-seguridad-de-tus-contrasenas).

Un simpático vídeo muy instructivo es esta [entrevista con Edward Snowden](https://www.youtube.com/watch?v=yzGzB-yYKcc) donde recomienda usar **frases complejas** como  "margaretthatcheris110%sexy". Ya que menciono a Snowden aprovecho para recomendar su libro ["Vigilancia Permanente"](https://www.casadellibro.com/libro-vigilancia-permanente/9788408215561/9852272).

####  4.2.4. <a name='Herramientasparamedirlafortalezadeunacontrasea'></a>Herramientas para medir la fortaleza de una contraseña

Si quieres **medir la fortaleza de tu contraseña** existen diferentes servicios. En la Web [https://www.my1login.com/resources/password-strength-test/](https://www.my1login.com/resources/password-strength-test/)introduces la contraseña y te dice cuanto tiempo podría llevar adivinarla (entiendo que mediante la técnica de fuerza bruta probando combinaciones), voy a probar la contraseña del ministro <<123456>>:

![](img/07.png)

Si buscas una contraseña lo más fuerte posible puedes utilizar recursos como el estimador [ZXCVBN](https://blogs.dropbox.com/tech/2012/04/zxcvbn-realistic-password-strength-estimation/), una herramienta de código abierto creada por Dropbox para estimar la fuerza de contraseñas. Puedes probarla en esta [demo online](https://lowe.github.io/tryzxcvbn/). En ella puedes escribir tu contraseña, y debajo te pondrá el tiempo que puede tardar en resolverse. También tienes un campo guesses_log10 en el que cuanto mayor sea la cifra resultante más segura será la contraseña que estés probando ([codigo en GitHub](https://github.com/dropbox/zxcvbn)). _match_ enumera los patrones que detecta usando diferentes diccionarios (palabras en inglés, nombres y apellidos, [Burnett's Ten Millon Passwords](https://xato.net/today-i-am-releasing-ten-million-passwords-b6278bbe7495), años, secuencias, etc.

![](img/17.png)

Karpesky hace algo similar en este enlace [https://password.kaspersky.com/es/](https://password.kaspersky.com/es/). Esta Web [https://www.uic.edu/apps/strong-password/](https://www.uic.edu/apps/strong-password/) me gusta mucho porque además ofrece un informe detallado indicando las debilidades o fortalezas de la contraseña escogida, otra opción más es [https://howsecureismypassword.net/](https://howsecureismypassword.net/).

Existen multitud de herramientas como estas, en muchos casos son la antesala para proponerte un gestor de contraseñas que anuncian. Eso mismo es lo que haré mas adelante, enumerar una serie de herramientas útiles.

###  4.3. <a name='Noreutilizarlascontraseas'></a>No reutilizar las contraseñas

**Nunca utilizarías la misma cerradura para varias puertas, ¿verdad?** Pues lo mismo ocurre con nuestras contraseñas.

![](img/24.png)

Una [encuesta de Google](https://services.google.com/fh/files/blogs/google_security_infographic.pdf) arroja que sólo el 35% de las personas usamos una cuenta diferente para todas nuestras cuentas. Nunca  debemos utilizar  la  misma  contraseña  para  diferentes  servicios.  **Tampoco  utilizaremos  las mismas contraseñas para uso profesional y doméstico**. De esta forma evitaremos tener que cambiar todas nuestras contraseñas en el caso de que solo una haya sido comprometida.

**¿Por qué es tan peligroso reutilizar contraseñas?**

[**Credential stuffing**](https://es.wikipedia.org/wiki/Credential_stuffing) (relleno de contraseñas) es una técnica que está ganando mucha popularidad, **los atacantes aprovechan bases de datos con credenciales de acceso a servicios que se han visto comprometidos o han sufrido una brecha de seguridad para usarlas para acceder a otros sitios**, en el mercado negro se pueden comprar bases de datos con cuentas de usuario, números de tarjeta de crédito, etc.

Incluso empresas reconocidas son vulnerables, en el [2013 un ataque contra Adobe](https://krebsonsecurity.com/2013/10/adobe-to-announce-source-code-customer-data-breach/) permitio a los atacantes obtener millones de datos de los usuarios registrados, parece que esto no representa una grave amenaza para los usuarios de Adobe, normalmente no guardamos datos críticos en una cuenta de Adobe, pero usando estas cuentas los atacantes prueban la combinación usuario/contraseña en otros servicios, redes sociales, correo electrónico o banca electrónica.

A pesar de que la red social Facebook no ha sufrido ataques recientes cuando se descubrió el robo de credenciales de Adobe inmediatamente cotejo las cuentas comprometidas con las suyas propias y tomo acciones para habilitar controles de seguridad adicionales para las cuentas que estaban bajo un riesgo potencial.

![](img/26.png)

####  4.3.1. <a name='direccionesdecorreoelectrnicotemporalesodesechables'></a>direcciones de correo electrónico temporales o desechables

Además de las contraseñas, también podemos **aumentar nuestra seguridad a la hora de dar nuestro correo electrónico. ¿Por qué? Porque es la identificación que damos en casi todos los sitios**, y que por lo tanto vincula todas nuestras cuentas. Y que todas nuestras cuentas tengan un identificador común no es demasiado bueno que digamos. ¿Entonces, qué qué podemos hacer para evitarlo?.

Existen **herramientas que permiten crear direcciones de correo electrónico temporales o desechables** para darnos de alta en servicios Web. Además de generar una dirección de correo permite ver si hemos recibido algún correo en la nueva dirección. Para los usuarios de Gmail existe un truco, en lugar de usar el correo habitual como el mio que es "ikerlandajuela@gmail.com" también funciona "ikerlandajuela+loquesea@gmail.com".

* Firefox está destacando los últimos tiempos por publicar herramientas dirigidas a nuestra privacidad y anonimato, [Firefox Relay](https://relay.firefox.com/) por el momento sólo está disponible solicitando una invitación, permite crear un alias de un correo electrónico que reenvía todo a nuestro correo electrónico real.
* [https://temp-mail.org/es/](https://temp-mail.org/es/).
* [https://www.emailondeck.com](https://www.emailondeck.com).
* [https://www.mailinator.com/](https://www.mailinator.com/).

###  4.4. <a name='Caducidadyrenovacinperidica'></a>Caducidad y renovación periódica

Para garantizar la confidencialidad de nuestras contraseñas estas deben ser **cambiadas periódicamente** (envejecimiento de contraseñas).

La periodicidad dependerá de la criticidad de la información a la que dan acceso. **No deben utilizarse contraseñas que  hayan sido usadas con anterioridad**. Pueden utilizarse sistemas que fuercen al cambio de contraseña en el plazo elegido. La nueva RGPD recomienda que se haga como mínimo una vez al año (cuando he trabajado en estos ámbitos forzabamos al usuario a cambiar la contraseña cada 3 meses).

Es importante cambiar tus contraseñas cada cierto tiempo. **Proteger tus contraseñas no siempre depende de ti al 100%, ya que puede haber filtraciones que las expongan online**. Por eso es importante ir cambiando tus contraseñas para que en el caso de que estas acaben filtrándose evites que alguien pueda utilizarlas.

###  4.5. <a name='Nousarelrecordatoriodecontraseas'></a>No usar el recordatorio de contraseñas

**La mayoría de los navegadores tienen un gestor de contraseñas integrado**, aunque lo podamos considerar de utilidad porque nos permite ser más ágiles a la hora de acceder a los servicios, tenemos que tener en cuenta que de esta manera podríamos estar facilitando acceso a personal no autorizado.

Instrucciones según el navegador para acceder a las contraseñas guardadas:

* Mozilla: Preferencias > Seguridad > Contraseñas guardadas.
* Chrome: Configuración (ajustes) > Configuración Avanzada > Contraseñas y Formularios > Administrar Contraseñas.
* Microsoft Edge: Panel de Control > Microsoft Credential Manager. 	

En este [enlace](https://www.osi.es/es/actualidad/blog/2013/10/11/como-eliminar-contrasenas-almacenadas-en-el-navegador) del OSI (Oficina de Seguridad del Internauta) explica de forma breve como eliminar las contraseñas guardadas en la mayoría de navegadores Web conocidos.

**Chrome** no sale precisamente bien parado, el navegador más usado del mundo **no implementa ningún mecanismo para proteger las contraseñas guardadas**, una persona con acceso físico a tu máquina puede leer las contraseñas guardadas.

El navegador de Mozilla recomienda a los usuarios que comparten un PC [establecer un password maestro](https://support.mozilla.org/en-US/kb/use-master-password-protect-stored-logins), mientras que Safari e IE piden una contraseña de sistema. Chrome no tiene ninguna de estas restricciones.

Contraseña maestra en Firefox:

![](img/29.png)

Visto lo fácil que es la opción más segura es usar un gestor de contraseñas, por supuesto mantén su navegador actualizado y cuidado con las extensiones y los permisos que les concedes sobres tus datos.

* [Chrome web dev plugin with 1m+ users hijacked, crams ads into browsers](https://www.theregister.com/2017/08/02/chrome_web_developer_extension_hacked/) (2017).
* "Particle" Chrome Extension Sold to New Dev Who Immediately Turns It Into Adware [https://www.bleepingcomputer.com/news/security/-particle-chrome-extension-sold-to-new-dev-who-immediately-turns-it-into-adware/](https://www.bleepingcomputer.com/news/security/-particle-chrome-extension-sold-to-new-dev-who-immediately-turns-it-into-adware/).

###  4.6. <a name='Autenticacindedoblefactor2FA'></a>Autenticación de doble factor (2FA)

![](img/doblefactor_dr.png)

La autenticación en dos pasos, abreviada 2FA (autenticación de dos factores) es un tipo de autenticación multifactor que requiere la combinación de dos componentes diferentes para confirmar la identidad. Tu cajero automático utiliza 2FA. Esos factores se basan todos en la premisa de que **sería muy difícil que un ente no autorizado puede hacerse con los dos al mismo tiempo**. Un _smartphone_ es un medio muy cómodo como parte de la 2FA, es algo que siempre llevamos con nosotros y casi siempre conectado a Internet para facilitar la autenticación.

En [TwoFactorAuth.org](https://twofactorauth.org/) puedes comprobar los servicios de todo tipo que utilizan doble factor y que mecanismos ofrecen.

![](img/11.png)

Cada servicio ofrece soluciones diferentes, por ejemplo [Google Authenticator](https://es.wikipedia.org/wiki/Google_Authenticator) es un software dedicado para _smartphone_ ([app en Google Play](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=es)) que ofrece [**contraseñas de un sólo uso**](https://es.wikipedia.org/wiki/Autenticaci%C3%B3n_con_contrase%C3%B1a_de_un_solo_uso) (OTP del inglés _One-Time Password_), la aplicación genera un código que el usuario debe proporcionar junto con su usuario y contraseña, en este sistema las claves son válidas durante un periodo de tiempo corto.  

Aunque probablemente los nuevos _smartphone_ tenderán a integrar esta tecnología existen **llaves de seguridad físicas dedicadas cuyo único propósito es generar _tokens_ de seguridad**. Google por ejemplo ha diseñado sus [llaves Titan](https://www.xataka.com/analisis/google-titan-security-key-analisis-anadir-capa-extra-seguridad-a-nuestras-cuentas-sencillo-como-parece), en lugar de recibir un código en el móvil o en una app, consiste en introducir una llave en el puerto USB del ordenador o conectarla por Bluetooth. No sólo es compatible con los servicios de Google, se puede usar en otros servicios que cumplan el [estándar FIDO](https://www.xatakamovil.com/desarrollo/fido2-asi-estandar-que-quiere-que-te-olvides-contrasenas-movil). La [lista](https://fidoalliance.org/members/) es amplia e incluye los servicios de Google, Facebook, eBay, Dropbox, Amazon, Apple, PayPal o Twitter, entre muchos otros. Existen otros productos como el [Yubikey](https://www.yubico.com/products/), una llave USB que funciona como factor de autenticación adicional.

**La mayor desventaja de la autenticación empleando algo que el usuario posea y otro factor cualquiera es que el token físico utilizado —una memoria USB, tarjeta bancaria, llave o similar— debe ser llevado siempre encima**. Además, si se lo roban, se pierde, o si el usuario simplemente no lo tiene consigo, el acceso es imposible.

####  4.6.1. <a name='Algunasaplicacionescon2FA'></a>Algunas aplicaciones con 2FA

[https://evanhahn.com/2fa/](https://evanhahn.com/2fa/)

##### Twitter

En las opciones de [privacidad y seguridad](https://twitter.com/settings/security) de la cuenta se puede habilitar 2FA.

![](img/28.png)

Se pueden usar diferentes métodos:

* SMS.
* Una app de autenticación.
* Una llave de seguridad.
* Códigos de respaldo de un sólo uso.
* Contraseña temporal.

###  4.7. <a name='Tcnicasdeautenticacinexternas'></a>Técnicas de autenticación externas

En los últimos años están emergiendo mecanismos  de  autenticación  descentralizados  que permiten el uso de contraseñas únicas para acceder a varios servicios.

Por ejemplo es muy común acceder a otros sitios Web usando las credenciales de un servicio como Google, a esto se le llama [**Single Sign-On**](https://es.wikipedia.org/wiki/Single_Sign-On).

##  5. <a name='Controversiayantipatrones'></a>Controversia y antipatrones

No todos coinciden en algunas de las medidas y buenas prácticas mencionadas, aquí empieza la controversia y el lio. En el documento Microsoft Password Guidance (PDF) describe algunos de estos "anti-patrones" que ponen en tela de juicio todo lo que nos inculcaron sobre contraseñas.

###  5.1. <a name='Anti-patron1:Contraseaslargas'></a>Anti-patron 1: Contraseñas largas

Contraseñas excesivamente largas puede producir un resultado que es predecible y poco deseable. 

##  6. <a name='HERRAMIENTAS'></a>HERRAMIENTAS

###  6.1. <a name='Gestoresdecontraseas'></a>Gestores de contraseñas

_"La velocidad cada vez mayor de los ordenadores los ha hecho exponencialmente más rápidos adivinando contraseñas mediante la fuerza bruta: probar contraseñas hasta encontrar la correcta. Mientras tanto, la longitud y la complejidad típicas de las contraseñas que el ciudadano medio desea y puede recordar se ha mantenido constante. El resultado son contraseñas que eran seguras hace diez años, y que hoy son inseguras."_ ([Haz clic aquí para matarlos a todos](https://www.casadellibro.com/libro-haz-clic-aqui-para-matarlos-a-todos/9788499987538/9383027) - Bruce Schneier).

_"“Your mind is for having ideas, not holding them”_ - David Allen.

Debemos considerar el uso de gestores de contraseñas en aquellos casos en los que tengamos que recordar un gran número de ellas para acceder a muchos servicios. En estos casos es muy recomendable elegir un gestor cuyo control quede  bajo nuestra supervisión, **que cifre las credenciales e implantar doble factor de autenticación para acceder al mismo**.

[Este vídeo](https://youtu.be/XEQAMvZoKNA) del OSI (Oficina de Seguridad del Internauta) explica de forma breve en que consiste un gestor de contraseñas.

{{< youtube XEQAMvZoKNA >}}

Puntos a favor:

* Se almacenan todas la claves bajo una contraseña maestra
* Se sincroniza entre equipos y dispositivos
* Permite autocompletar los formularios de acceso a los servicios Web.

[**KeePass**](https://keepass.info/) es una herramienta es de código abierto y gratuita, la he usado a menudo. El único inconveniente es que es una aplicación para escritorio, esto complica un poco que podamos usar la herramienta entre varias personas o computadoras de forma sencilla. Cuenta con numerosos [_plugins_](https://keepass.info/plugins.html) para extender su funcionalidad, recuerdo especialmente uno para realizar copias de seguridad de la base de datos de contraseñas y otro para sincronizar automáticamente todo en Google Drive.

![](img/09.png)

Últimamente estoy reemplazando [**KeePass**](https://keepass.info/) por la versión gratuita de [**LastPass**](https://www.lastpass.com/es/)

Otras alternativas:

* [Dashlane](https://www.dashlane.com/es): Planes de pago y uno gratuito que permite almacenar hasta 50 contraseñas y llenado automático de formularios y de datos de pago.
* [1Password](https://1password.com/es/): Todos los planes son de pago.
* [Enpass](https://www.enpass.io/): Gratuito como app de escritorio.
* [SplashID](https://splashid.com/)
* [**bitwarden**](https://bitwarden.com/): De código abierto para escritorio.
* [**RememBear**](https://www.remembear.com/): La versión gratuita es sólo para un dispositivo y no incluye sincronización ni copias de seguridad
* [**LogmeOnce**](https://www.logmeonce.com/)
* [**RoboForm**](https://www.roboform.com/es):  Disponible en Windows, MacOS, Android, iOS y Linux, RoboForm se integra con todas las extensiones de navegador más populares, incluyendo Internet Explorer, Mozilla Firefox, Google Chrome, Microsoft Edge, Safari y Opera.

##  7. <a name='GESTINPARAADMINS'></a>GESTIÓN PARA ADMINS

Este punto corresponde más a los administradores de sistemas.

* **Identificar los distintos equipos, servicios y aplicativos** para los que  es necesario activar credenciales de acceso. Esto no suele ser fácil ya que suele ser una combinación de servicios Web y aplicaciones locales, ademas a menudo algunas áreas o departamentos usan aplicaciones muy específicas.
* Definir la **manera con la que se generarán las claves**, así como su formato. Yo soy un poco vago y a menudo uso esta Web [https://passwordsgenerator.net/](https://passwordsgenerator.net/) para generar nuevas claves
* Distribuirlas claves generadas a los usuarios correspondientes, usar sistemas de mensajería móvil no es lo más recomendable, es fácil de reenviar o obtenerla si nos sustraen el terminal. Debemos tener en cuenta:
	* si esta distribución ha de ser cifrada y con qué método
	* cómo se activarán las claves.
* **Almacenar las claves** en repositorios seguros, considerando la necesidad de realizar **copias de respaldo**. Más adelante propondré algunas herramientas para gestionar claves.
* Determinar quién puede acceder a estos repositorios y cómo (roles).
* Establecer el **periodo de validez para cada tipo de clave** (renovación periódica de forma automatizada).
* Registrar: motivo por el que se genera una clave, fecha de creación, responsable de la custodia, periodo de validez, posibles observaciones, incidentes, etc.

##  8. <a name='RecursosdelINCIBEInsitutoNacionaldeCiberseguridad'></a>Recursos del INCIBE (Insituto Nacional de Ciberseguridad)

El Instituto Nacional de Ciberseguridad de España (INCIBE) ofrece una [guía en PDF](./res/contrasenas-incibe.pdf) sobre la gestión de contraseñas. El preámbulo del documento hace hincapié sobre el ciclo de vida de las cuentas de usuario, debemos garantizar que las **credenciales de autenticación** se generan, actualizan y revocan de forma óptima y segura. Este último punto que consiste en deshabilitar la cuenta bien eliminando o bloqueando el acceso es el más espinoso en muchas organizaciones, **requiere de procedimientos estándarizados y una comunicación eficaz entre áreas** ya que normalmente es el responsable del trabajador o el departamento de personas quien debe desencadenar el proceso y comunicarlo a su debido tiempo, a menudo esto no es una prioridad y una persona que no ya no trabaja con nosotros sigue teniendo accesos a los sistemas y recursos de la organización, además **normalmente hay que gestionar la contraseña de varios servicios** como pueden ser el directorio activo de la red local, servicios Web y recursos en la nube como el correo electrónico, sitios Web, ERP, CRM, etc.

A continuación he sacado del documento una tabla que permite establecer un control sobre las políticas de contraseñas que tenemos implantadas en nuestra organización.

El nivel se refiere a la complejidad entre básico (B) y avanzado (A). Los controles podrán tener el siguiente **alcance**:

* **Procesos (PRO)**: aplica a la dirección o al personal de gestión.
* **Tecnología (TEC)**: aplica al personal técnico especializado.
* **Personas (PER)**: aplica a todo el personal.

| Nivel | Alcance | Control | Resultado |
|-------|---------|---------|-----------|
| A | PRO/TEC | **Gestión de contraseñas**. Defines   un   sistema   de   gestión   de contraseñas avanzado que contempla todos los aspectos relativos a su ciclo de vida  | Sí/No |
| A | PRO/TEC | **Técnicas de autenticación externas**. Consideras la utilización de sistemas de autenticación externos descentralizados | Sí/No |
| A | TEC | **Herramientas  para  garantizar  la seguridad de las contraseñas**. Te ayudas de técnicas y herramientas informáticas para garantizar la seguridad de las contraseñas | Sí/No |
| B | TEC | **No utilizar las contraseñas por defecto**. Cambias  las  contraseñas  que  vienen incluidas por defectopara el acceso a aplicaciones y sistemas | Sí/No |
| B | TEC | **Doble factor para servicios críticos**. Incorporas sistemas de autenticación multifactor en los accesos a servicios con información muy sensible | Sí/No |
| B | PER | **No compartir las contraseñas con nadie**. Mantienes en secreto tus claves y evitas compartirlas | Sí/No |
| B | PER | **Las contraseñas deben de ser robustas**. Generas tus contraseñas teniendo en cuenta su fortaleza | Sí/No |
| B | PER | **No  utilizar  la  misma  contraseña para  servicios diferentes**. Te aseguras de elegir distintas contraseñas para cada uno de los servicios que utilizas | Sí/No |
| B | PER | **Cambiar las contraseñas periódicamente**. Haces que se modifiquen las contraseñas cada... | Sí/No |
| B | PER | **No hacer uso del recordatorio de contraseñas**. No utilizas  nunca  las  opciones  de  recordatorio  de contraseñas de navegadores y aplicaciones  | Sí/No |
| B | PER | **Utilizar gestores de contraseñas**. Usas  gestores  de  contraseñas segurospara  poderlas recordar  | Sí/No |

Esta lista de comprobación puede usarse como escala y nos permite reconocer donde debemos reforzar nuestra política de gestión de contraseñas.

El documento sigue desarrollando algunos puntos clave. Más adelante mencionare algunas herramientas para gestionar las contraseñas, comprobar su robustez, etc.

###  8.1. <a name='KitdeconcienciacinparaempresasINCIBE'></a>Kit de concienciación para empresas (INCIBE)

El [Kit de concienciación para empresas](https://www.incibe.es/protege-tu-empresa/kit-concienciacion) ofrece posters, trípticos y recursos formativos en general para usar en nuestro centro de trabajo. El manual también incluye una especie de auditoría de seguridad con ataques simulados.

En lo que a contraseñas concierne dentro del apartado de formación del manual se incluye la temática sobre seguridad que nos remite a una carpeta completa con instrucciones detalladas, los elementos más destacados que he descomprimido y guardado:

* [05_Test_Contraseinas.pdf](./res/kit-concienciacion/05_Test_Contraseinas.pdf): Son 10 preguntas que podemos usar al final de una formación para medir los conocimientos adquiridos.
* [05_Contraseinas.pptx](./res/kit-concienciacion/05_Contraseinas.pptx)

[**Descarga de todo el Kit comprimido**](https://files.incibe.es/incibe/kit_concienciacion/kit_concienciacion.zip).

###  8.2. <a name='PolticasdeseguridadparalaPYMEINCIBE'></a>Políticas de seguridad para la PYME (INCIBE)

Con el propósito de mejorar la ciberseguridad [INCIBE](https://www.incibe.es/protege-tu-empresa/herramientas/politicas) a recopilado una serie de documentos que se pueden descargar [politicas-pyme.zip](./res/politicas-pyme.zip).

Las políticas abarcan un gran número de temas relativos a la seguridad, se dividen en 3 grandes bloques por roles, dentro de cada uno se pueden encontrar diferentes temas, no voy a copiar todos aquí, sólo he mantenido los más destacados respecto al tema de las contraseñas.

* [Políticas para el empresario](https://www.incibe.es/protege-tu-empresa/herramientas/politicas#empresario)
    * **Concienciación y formación** [PDF](https://www.incibe.es/sites/default/files/contenidos/politicas/documentos/concienciacion-y-formacion.pdf)
* [Políticas para el personal técnico](https://www.incibe.es/protege-tu-empresa/herramientas/politicas#tecnico)
    * **Control de acceso** [[PDF](https://www.incibe.es/sites/default/files/contenidos/politicas/documentos/control-de-acceso.pdf)]
* [Políticas para el empleado](https://www.incibe.es/protege-tu-empresa/herramientas/politicas#empleado)
	* **Protección del puesto de trabajo** [PDF](https://www.incibe.es/sites/default/files/contenidos/politicas/documentos/proteccion-puesto-trabajo.pdf). Destacan los puntos "Programas un bloqueo automático de sesión en los equipos al no detectarse actividad del usuario en un corto periodo de tiempo", la _"Política de mesas limpias"_ y _"Obligación de bloqueo de sesión y  apagado de equipo".
	* **Contraseñas** [PDF](https://www.incibe.es/sites/default/files/contenidos/politicas/documentos/contrasenas.pdf) [CHECKLIST EDITABLE](https://www.incibe.es/sites/default/files/contenidos/politicas/fichas/contrasenas.pdf)

###  8.3. <a name='OtrosrecursosINCIBE'></a>Otros recursos INCIBE

Existe un canal de publicación de noticias en **Telegram** para estar al día de cualquier novedad relativa a la seguridad [https://t.me/ProtegeTuEmpresa](https://t.me/ProtegeTuEmpresa), lo recomiendo encarecidamente para técnicos y administradores de sistemas.

##  9. <a name='BasqueCybersecurityCentreBCSC'></a>Basque Cybersecurity Centre (BCSC)

El [centro vasco de ciberseguridad](https://www.basquecybersecurity.eus/es/) también ofrece recursos interesantes a tomar en cuenta, en su mayoría son infografías y píldoras formativas. También te puedes [suscribir al boletín de correo electrónico](https://www.basquecybersecurity.eus/es/boletin/). En general la información es menos profusa, recomiendo los recursos de INCIBE para hacer algo más serio.

El [libro blanco de la ciberseguridad en Euskadi](https://www.basquecybersecurity.eus/archivos/201902/bcsc_libro_blanco_v7.pdf?3) [PDF] ofrece un listado de empresas que trabajan en este ámbito. 

##  10. <a name='MaterialformacinHardeningWindows'></a>Material formación "Hardening Windows"

Los siguientes documentos corresponden a la presentación de unas jornadas impartidas por [Diego Gil](https://www.linkedin.com/in/diegogilfer/) de [JakinCode](https://www.jakincode.com/)

* [Formacion-Windows-Hardening-1.pdf](./res/Formacion-Windows-Hardening-1.pdf)
* [Formacion-Windows-Hardening-2.pdf](./res/Formacion-Windows-Hardening-2.pdf)
* [Formacion-Windows-Hardening-3.pdf](./res/Formacion-Windows-Hardening-3.pdf)

##  11. <a name='Catlogodeherramientas'></a>Catálogo de herramientas

###  11.1. <a name='Herramientasdedeteccinyeliminacindevirusonline'></a>Herramientas de detección y eliminación de virus online

* [ESET Online Scanner](https://www.eset.com/uk/home/online-scanner/). Sólo disponible para sistemas MS Win.
* [chrome://settings/cleanup](chrome://settings/cleanup): Sólo para navegadores Chrome claro.
* [https://www.virustotal.com/gui/] Analyze suspicious files and URLs to detect types of malware.
* [F-Secure Online Scanner](https://www.f-secure.com/gb-en/home/free-tools/online-scanner).
* [VirScan](https://virscan.org/): VirScan te permite cargar un archivo para su análisis.

##  12. <a name='Enlaces'></a>Enlaces

* OSI–Recuerda una y tenlas todas. [https://www.osi.es/es/actualidad/blog/2015/09/04/recuerda-una-y-tenlas-todas](https://www.osi.es/es/actualidad/blog/2015/09/04/recuerda-una-y-tenlas-todas) 
* [BCSC Libro Blanco](https://www.basquecybersecurity.eus/archivos/201902/bcsc_libro_blanco_v7.pdf?3) [PDF]: Libro blanco de la ciberseguridad en Euskadi, da  cabida  a  todos  los agentes del mercado de la ciberseguridad y a su oferta de productos y servicios.  Esta primera edición del catálogo pretende servir de resumen o instantánea del mercado de la  ciberseguridad  en  Euskadi a  fecha  dediciembre  de  2018.
* [https://www.incibe.es/protege-tu-empresa/blog/desempolvando-politicas-seguridad-las-contrasenas](https://www.incibe.es/protege-tu-empresa/blog/desempolvando-politicas-seguridad-las-contrasenas)
* RedIRIS Autenticación de usuarios [https://www.rediris.es/cert/doc/unixsec/node14.html](https://www.rediris.es/cert/doc/unixsec/node14.html).

**Navegadores Web**:

* [https://www.osi.es/es/actualidad/blog/2013/10/11/como-eliminar-contrasenas-almacenadas-en-el-navegador](https://www.osi.es/es/actualidad/blog/2013/10/11/como-eliminar-contrasenas-almacenadas-en-el-navegador)
* [https://www.fayerwayer.com/2013/08/chrome-no-protege-las-contrasenas-guardadas-en-el-navegador/](https://www.fayerwayer.com/2013/08/chrome-no-protege-las-contrasenas-guardadas-en-el-navegador/)
* [https://support.mozilla.org/en-US/kb/use-primary-password-protect-stored-logins-and-pas?redirectslug=use-master-password-protect-stored-logins&redirectlocale=en-US](https://support.mozilla.org/en-US/kb/use-primary-password-protect-stored-logins-and-pas?redirectslug=use-master-password-protect-stored-logins&redirectlocale=en-US)

**Contraseñas debiles**:

* OSI-¿Sabías que el 90% de las contraseñas son vulnerables? [https://www.osi.es/es/actualidad/blog/2019/02/06/sabias-que-el-90-de-las-contrasenas-son-vulnerables](https://www.osi.es/es/actualidad/blog/2019/02/06/sabias-que-el-90-de-las-contrasenas-son-vulnerables)
* Genbeta [https://www.genbeta.com/seguridad/cuanto-tiempo-lleva-siendo-123456-la-peor-contrasena-posible-y-por-que-nunca-lograremos-eliminarla](https://www.genbeta.com/seguridad/cuanto-tiempo-lleva-siendo-123456-la-peor-contrasena-posible-y-por-que-nunca-lograremos-eliminarla).
* dropbox.tech - zxcvbn: realistic password strength estimation [https://dropbox.tech/security/zxcvbn-realistic-password-strength-estimation](https://dropbox.tech/security/zxcvbn-realistic-password-strength-estimation).

**Trucos para crear nuestras propias contraseñas**:

* Genbeta [https://www.genbeta.com/a-fondo/como-utilizar-la-nemotecnia-para-crear-y-recordar-contrasenas-complejas-y-seguras](https://www.genbeta.com/a-fondo/como-utilizar-la-nemotecnia-para-crear-y-recordar-contrasenas-complejas-y-seguras).
* Genbeta [https://www.genbeta.com/seguridad/frases-en-lugar-de-palabras-asi-es-la-contrasena-perfecta-segun-snowden](https://www.genbeta.com/seguridad/frases-en-lugar-de-palabras-asi-es-la-contrasena-perfecta-segun-snowden).
* Xataka. Cómo crear una contraseña segura y cómo gestionarla después para proteger tus cuentas [https://www.xataka.com/basics/como-crear-contrasena-segura-como-gestionar-despues-para-proteger-tus-cuentas](https://www.xataka.com/basics/como-crear-contrasena-segura-como-gestionar-despues-para-proteger-tus-cuentas).

**Gestores de contraseñas**:

* Genbeta [https://www.genbeta.com/seguridad/usar-o-no-usar-un-gestor-de-contrasenas-he-ahi-el-dilema](https://www.genbeta.com/seguridad/usar-o-no-usar-un-gestor-de-contrasenas-he-ahi-el-dilema)
* Genbeta [https://www.genbeta.com/seguridad/especial-contrasenas-seguras-consejos-para-mejorar-la-seguridad-de-tus-contrasenas](https://www.genbeta.com/seguridad/especial-contrasenas-seguras-consejos-para-mejorar-la-seguridad-de-tus-contrasenas)
* [https://www.pcmag.com/picks/the-best-free-password-managers](https://www.pcmag.com/picks/the-best-free-password-managers)

**2FA**:

* [Dos mejor que uno: doble factor para acceder a servicios críticos](https://www.incibe.es/protege-tu-empresa/blog/dos-mejor-uno-doble-factor-acceder-servicios-criticos) Publicado el 22/02/2017, por INCIBE.
* [https://www.xataka.com/basics/google-authenticator-que-como-funciona-como-configurarlo](https://www.xataka.com/basics/google-authenticator-que-como-funciona-como-configurarlo)
* [https://www.google.com/intl/en-US/landing/2step/features.html](https://www.google.com/intl/en-US/landing/2step/features.html)
* [https://www.xataka.com/analisis/google-titan-security-key-analisis-anadir-capa-extra-seguridad-a-nuestras-cuentas-sencillo-como-parece](https://www.xataka.com/analisis/google-titan-security-key-analisis-anadir-capa-extra-seguridad-a-nuestras-cuentas-sencillo-como-parece)
* Genbeta [https://www.genbeta.com/seguridad/por-que-no-debes-confiar-en-la-autenticacion-en-dos-pasos-a-traves-de-sms](https://www.genbeta.com/seguridad/por-que-no-debes-confiar-en-la-autenticacion-en-dos-pasos-a-traves-de-sms)

**Google Hacking**:

* Google Hacking & Dorks (46 ejemplos): cómo consigue un hacker contraseñas usando sólo Google. Google puede ser tu peor enemigo. [https://antoniogonzalezm.es/google-hacking-46-ejemplos-hacker-contrasenas-usando-google-enemigo-peor/]

**Credential stuffing**

* [https://es.wikipedia.org/wiki/Credential_stuffing](https://es.wikipedia.org/wiki/Credential_stuffing).
* [https://blog.cloudflare.com/validating-leaked-passwords-with-k-anonymity/](https://blog.cloudflare.com/validating-leaked-passwords-with-k-anonymity/).
* [https://arstechnica.com/information-technology/2013/11/how-an-epic-blunder-by-adobe-could-strengthen-hand-of-password-crackers/](https://arstechnica.com/information-technology/2013/11/how-an-epic-blunder-by-adobe-could-strengthen-hand-of-password-crackers/).
* [https://krebsonsecurity.com/2013/10/adobe-to-announce-source-code-customer-data-breach/](https://krebsonsecurity.com/2013/10/adobe-to-announce-source-code-customer-data-breach/).
* [https://krebsonsecurity.com/2013/11/facebook-warns-users-after-adobe-breach/](https://krebsonsecurity.com/2013/11/facebook-warns-users-after-adobe-breach/).
* [https://michael-coates.blogspot.com/2013/11/how-third-party-password-breaches-put.html](https://michael-coates.blogspot.com/2013/11/how-third-party-password-breaches-put.html).
* [https://owasp.org/www-community/attacks/Credential_stuffing](https://owasp.org/www-community/attacks/Credential_stuffing).
* [https://blog.nivel4.com/noticias/que-es-el-credential-stuffing-o-relleno-de-contrasenas/](https://blog.nivel4.com/noticias/que-es-el-credential-stuffing-o-relleno-de-contrasenas/).
* [https://www.welivesecurity.com/la-es/2018/05/03/por-que-es-tan-riesgoso-reutilizar-tu-contrasena/](https://www.welivesecurity.com/la-es/2018/05/03/por-que-es-tan-riesgoso-reutilizar-tu-contrasena/).

**Bibliografía**:

Perfect Password: Selection, Protection, Authentication - Mark Burnett.

![](img/20.jpg)