---
title: Series de tiempo en R
subtitle: Time Series ts()
date: 2019-04-16
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico"]
---

![](images/r-logo.PNG)

Creamos una muestra aleatoria de datos que representan temperaturas en un intervalo de -10 a 35 grados, generamos 100 muestras donde se pueden repetir los casos:

```R
v1 <- sample(-10:35,72,replace = T)
```

Ahora pasamos el vector con la muestra de datos a la función `ts`, indicamos que comienza en Enero del 2011 y finaliza en Diciembre del 2016, `frecuency` indica el número de observaciones por unidad de tiempo, la frecuencia de los datos va a ser mensual.

```R
> timeserie <- ts(v1,start=c(2011,01),end = c(2016,12),frequency = 12)
> timeserie
     Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec
2011  26   2  12  31  -9  26  -1  35  31   7  22  -4
2012   2   3   3  22  -5  12  16   8  24  -6  24   6
2013  13  20   9   3   9   6  -5  13  34  11  11  25
2014   5  34  -9  -8  -5  15   2   0  -1   0  -9   3
2015  28  -3  33  18  32  -4  23  32   3  23  35  16
2016  16  20  -3  30  24  -3  24  -4  24  19  10  19
```

Las 72 observaciones se han ordenado mes a mes hasta el momento final, obviamente no tiene mucho sentido una temperatura de -1 en Julio.

![](images/ts-01.PNG)

Ahora hacemos un sencillo diagrama con `plot`:

```R
plot(timeserie)
```

![](images/ts-plot-01)

Si solo queremos una muestra de 1 año usamos la orden `window`:

```R
> ts2 <- window(timeserie,start=c(2014,01),end=c(2014,12))
> ts2
     Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec
2014  30  -1  10  20  34   2  -2   6  31  18  24   6
```

## Ejercicios

- r / intro / 38-ts-time-series / [time-series.R](https://gitlab.com/soka/r/blob/master/intro/38-ts-time-series/time-series.R).
