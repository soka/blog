---
title: AddToAny Share Buttons
subtitle: comparte contenidos de WordPress
date: 2020-11-15
draft: true
description: Plugin para compartir contenidos
tags: ["Wordpress","WP - Curso","WP","WP - Plugins","WP - Compartir"]
---

![](img/01.png)

El _plugin_ WordPress [**AddToAny Share Buttons**](https://es.wordpress.org/plugins/add-to-any/)permite compartir entradas y páginas en otros servicios como Facebook, Twitter, LinkedIn, etc.

Lo primero es revisar **Ajustes | AddToAny**,  

## Enlaces externos

- [https://es.wordpress.org/plugins/add-to-any/](https://es.wordpress.org/plugins/add-to-any/).

## Siguientes _posts !!!!!


- Cookies RGPD + políticas legales + aspectos legales (ecommerce también?)
    https://ayudawp.com/plugin-consentimientos-rgpd-cookies-casi-perfecto/
- Child themes?
    https://wpmayor.com/ways-to-customize-your-wordpress-footer/
    Mirar ppt imanol teran sobre el tema    
- Integrar Google Analítics, forms, calendarios????

Probar themes:

- https://athemes.com/collections/free-wordpress-themes/
- https://wordpress.org/themes/newsmag/

Plugins

- https://wordpress.org/plugins/nextgen-gallery/

- https://wpshout.com/wordpress-custom-css/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+Nometech+%28WPShout.com%29

Forms:

- https://designmodo.com/design-email-signup-forms/?ref=ewebdesign.com#2-put-your-signup-form-in-the-right-place