---
title: Tres formas de renombrar columnas en R
subtitle: Usando el paquete dplyr o funciones de R base grep y colnames
date: 2019-10-21
draft: false
description: Ciencia de datos con R
tags: ["R","R - dplyr","R - General","R - Base"]
---

Este es un post muy breve que resume como realizar una operación muy concreta, **renombrar los nombres de las variables (columnas) de un _dataset_**, con las funciónes de R base [`colnames`](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/row%2Bcolnames), [`grep`](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/grep) o usando la función [`rename`](https://www.rdocumentation.org/packages/plyr/versions/1.8.4/topics/rename) del paquete dplyr. No me quiero atribuir el código ni el tutorial que tiene origen en este enlace ["Rename Columns | R"](https://finderding.com/rename-columns-r/) ([Terry](https://finderding.com/author/finder8_wp/)).

Para los ejemplos usaré el _dataset_ `cars` que contiene 50 observaciones de dos variables, la distancia `dist` de frenado dada la velocidad `speed` de un vehículo.

```r
cars[,1] # speed numeric Speed (mph)
cars[,2] # dist numeric	Stopping distance (ft)
```

Durante los ejemplos borro la variable de entorno con el _dataset_ después de realizar modificaciones para obtener de nuevo el dato original usando la función `rm(cars)`.

## Usando colnames

Las funciones [`row+colnames`](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/row%2Bcolnames) permiten obtener o establecer los nombres de filas o columnas de un objeto tipo matriz.

```r
> colnames(cars) 
[1] "speed" "dist"
```

Ahora usando esa misma función cambio el nombre de la variable:

```r
> colnames(cars)[2] <-"Distancia frenado"
> colnames(cars) 
[1] "speed"             "Distancia frenado"
```

## grep: búsqueda de patrones y reemplazo

Las funciones de la "familia" [`grep`](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/grep) permiten buscar patrones dentro de vectores de carácteres, estas funciones aceptan [expresiones regulares](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/regex) que permiten realizar búsquedas complejas.

Este ejemplo busca cualquier carácter que no sea 'a', 'b' o 'c', retorna los indices de las cadenas que cumplen la condición:

```r
> str <- c("a","t","ab","r")
> grep("[^abc]",str)
[1] 2 4
```

Ahora volviendo sobre el objetivo inicial uso la función [`grep`](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/grep) para buscar la variable "dist" del _dataset_, el resto es igual que el ejemplo basado sólo en `colnames`:

```r
colnames(cars)[grep("dist", colnames(cars))] <-"Distancia frenado"
```

## Usando dplyr

El paquete [dplyr](https://dplyr.tidyverse.org/) proporciona una gramática para manipular y operar con _data frames_. 

Usando la función `rename` es así de sencillo y elegante:

```r
storms %>% 
  rename(viento = wind) %>% 
  names()
```  

## Código fuente

- r / intro / 57-renombrar-columnas / [renombrar-columnas.R](https://gitlab.com/soka/r/blob/master/intro/57-renombrar-columnas/renombrar-columnas.R)
