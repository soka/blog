---
title: Bloquea o desbloquea personas en Google Drive
subtitle: Incrementa la seguridad bloqueando el intercambio con usuarios sospechosos
date: 2021-09-22
draft: false
description: Incrementa la seguridad bloqueando el intercambio con usuarios sospechosos
tags: ["Google","Drive","Updates","Workspace","Novedades"]
---

<!-- vscode-markdown-toc -->
* 1. [Funcionamiento](#Funcionamiento)
	* 1.1. [Bloquear un usuario desde el correo electrónico](#Bloquearunusuariodesdeelcorreoelectrnico)
	* 1.2. [Bloquear el propietario del archivo en Google Drive](#BloquearelpropietariodelarchivoenGoogleDrive)
	* 1.3. [Desbloquear una persona](#Desbloquearunapersona)
* 2. [Enlaces internos](#Enlacesinternos)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Cuando hablamos de compartir Google es un sistema amigable que permite compartir los archivos con otros usuari@s determinados o de forma anónima mediante un enlace, esta facilidad de uso puede entrañar un coste en seguridad en forma de _spam_ o contenidos maliciosos. Google está intentando controlar este tipo de prácticas con la nueva funcionalidad que se describe más abajo. 

Desde el 22 de Julio (fecha del [anuncio](https://workspaceupdates.googleblog.com/2021/07/block-shares-from-user-google-drive.html)) **Google Drive permite bloquear a otros usuari@s**, si se bloquea un usuari@ no podrá compartir ningún elemento de Drive contigo, tus elementos compartidos a su vez tampoco podrán ser compartidos con ese usuari@ (aunque se hubiesen compartido previamente). Como salvedad hay que tomar en cuenta que no podrás bloquear usuari@s de tu unidad organizacional.

##  1. <a name='Funcionamiento'></a>Funcionamiento

###  1.1. <a name='Bloquearunusuariodesdeelcorreoelectrnico'></a>Bloquear un usuario desde el correo electrónico

Podemos usar las notificaciones de correo electrónico para evitar que un usuario comparta algo con nosotros, dentro del correo al pie encontramos la opción de bloquear un usuario.

![](img/01.PNG)

![](img/02.PNG)

###  1.2. <a name='BloquearelpropietariodelarchivoenGoogleDrive'></a>Bloquear el propietario del archivo en Google Drive

Clic con el botón derecho del ratón sobre el archivo en cuestión:

![](img/03.PNG)

###  1.3. <a name='Desbloquearunapersona'></a>Desbloquear una persona

En Drive en la esquina superior derecha haz clic sobre tu foto de perfil > [gestionar tu cuenta de Google](https://myaccount.google.com/).

![](img/04.PNG)

##  2. <a name='Enlacesinternos'></a>Enlaces internos

* Publicaciones internas dentro de la categoría Google [https://soka.gitlab.io/blog/tags/google/][https://soka.gitlab.io/blog/tags/google/].

##  3. <a name='Enlacesexternos'></a>Enlaces externos

* Google Drive Help [Block & unblock people in Google Drive](https://support.google.com/drive/answer/10613533?p=utu_web&visit_id=637661169083115744-1123611800&rd=1).
*  Google Workspace Updates [Block shares from another user in Google Drive](https://workspaceupdates.googleblog.com/2021/07/block-shares-from-user-google-drive.html).