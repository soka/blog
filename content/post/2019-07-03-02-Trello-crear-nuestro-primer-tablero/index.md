---
title: Trello - crear nuestro primer tablero
subtitle: La pizarra digital inspirada en kanban
date: 2019-07-03
draft: false
description: Tableros en Trello
tags:
  [
    "Trello",
    "Talleres",
    "Cursos",
    "Aplicaciones",
    "Trello - Introducción",
    "Kanban",
    "Metodologías",
    "Gestión de Proyectos",
    "Herramientas",
  ]
---

![](img/logo-trello.png)

## Crear un tablero

Crear un tablero es muy sencillo en **Trello**, lo único que tenemos que hacer es usar el botón "+" junto a nuestro perfil o usar el la opción "Tableros" en la esquina superior izquierda.

![](img/01.PNG)

Podemos elegir una imagen de fondo aunque personalmente prefiero usar colores sólidos y algún tipo de criterio con los colores, por ejemplo rosa para los personales, naranja para talleres o tableros públicos...

![](img/02.PNG)

En el menu lateral derecho podemos ver la actividad del tablero.

# Creando listas

A modo de ejemplo voy a crear un tablero llamado "Quienes somos" simulando un directorio de empresa, cada lista pertenecerá a un trabajador con tarjetas para datos personales, proyectos en curso o incluso aficiones.

![](img/03.PNG)

Puedo añadir infinitas listas en horizontal, una para cada miembro del equipo. Si por ejemplo es una organización bastante grande podemos usar las listas como áreas funcionales o departamentos:

![](img/04.PNG)

En cualquier momento podemos arrastrar y soltar las listas de izquierda a derecha (_drag and drop_), el icono del ratón cambia cuando arrastramos la lista.

![](img/05.gif)

## Añadir miembros del equipo al tablero

Ahora mismo otro integrante del equipo ya puede ver el tablero porque pertenece al equipo, puede unirse al tablero o recibir una invitación explicita del administrador del equipo.

![](img/06.gif)

**Puedes añadir miembros a Trello de dos tipos:**

- **No es usuario**: usas su email para mandarle una invitación que le llevará a registrarse y a tu tablero.
- **Usuario**: puedes añadirlo por el email también o por su nombre de usuario de Trello si lo conoces.

**Puedes añadir miembros de varias maneras:**

- a un **tablero específico dentro de un equipo**: no quieres que forme parte del equipo pero sí de un tablero en el que van a colaborar
- a un **equipo**: quieres que el miembro sea parte del equipo y que pueda ver todos los tableros. Puedes invitar de manera individual, a muchos a la vez o a través de un enlace (útil para meter a gente por Whatsapp por ejemplo)
- a **miembros del equipo**: pertenece al equipo pero no está en ese tablero específico (puede que sea de reciente creación), cuando le das al botón de añadir miembro del tablero te saldrán los miembros del equipo que no estén unidos al tablero.

Cuando añades miembros le das un **tipo de permiso**:

- Normal: puede ver o editar tarjetas y cambiar algunos aspectos de configuración del tablero
- Administrador: puede ver o editar tarjetas, quitar miembros y cambiar toda la configuración del tablero

## Cerrar un tablero

**Cierra un tablero** seleccionando "Cerrar tablero" en el menú del tablero (abrir el menú, clic en el botón "Más" para expandir las opciones del menú), tienes que ser un administrador de un tablero para cerrarlo.

![](img/07.gif)

Una vez cerrado un tablero, ya no será visible en tu lista de Tableros y sólo se podrá acceder haciendo clic en el botón "Tableros" situado en la parte superior izquierda de tu página de Trello y seleccionando "Ver tableros cerrados..."

![](img/08.gif)

## Borrar un tablero

**Para eliminar un tablero, tendrás que cerrarlo antes**. Cerrar un tablero es parecido a archivar una tarjeta: puedes dejarlo en tu lista de "Tableros cerrados" si crees que algún día puedas querer usarlo de nuevo, o puedes borrar el tablero permanentemente una vez esté cerrado.

Eliminar un tablero es permanente y no se pueden recuperar tablas eliminadas.

## Ejercicio: Crear un directorio de empresa

Como presentación inicial crearemos una lista con los datos de cada persona integrante del equipo. En el siguiente post veremos como crear tarjetas en **Trello**.

## Enlaces internos

- Tablero ["Quienes somos"](https://trello.com/b/SWvglOtz) en Trello (visible para el equipo).
- Tablero ["TallerTrello - Ejemplo central de equipo"](https://trello.com/b/XhaVartD).
