---
title: Introducción WSB (Windows Server Backup) [borrador]
subtitle: Administración de Windows Server con PowerShell
date: 2019-12-06
draft: true
description: Administración servidores Windows
tags: ["Windows","Sistemas","PowerShell","Administración","cmdlets","Scripting","WSB","Backup","Windows Server"]
---

![](img/win-server-logo.png)

**WSB (Windows Server Backup)** es el sucesor de NTBACKUP presente en versiones previas de Windows.

## Instalación

En mi sistema ya viene pre-instalado y no figura en la lista para agregar nuevos roles en el administrador del servidor, usando PowerShell también se puede instalar desde la consola:

```ps
Install-WindowsFeature Windows-Server-Backup
```

## Acceso consola backup

Una vez instalada ahora se puede abrir la interfaz gráfica desde varios sitios, usando la línea de comandos ejecuto `Wbadmin.msc`, también se puede acceder desde el menú "Herramientas" del administrador del servidor o simplemente desde el inicio buscando "Copias de seguridad".

![](img/01.png)

## Hacer una copia de seguridad del servidor

Usando la aplicación Wbadmin me posiciono sobre el elemento "Copia de seguridad local" y con la opción del menú "Acción" o con el botón derecho del ratón despliego un menú y selecciono "Programar copia de seguridad":

![](img/02.png)

En lugar de hacer una copia del servidor completo escojo la opción personalizada, selecciono las opciones imprescindibles para restaurar más tarde el sistema operativo, aunque puedo salvar también otros volúmenes con datos por ejemplo.

![](img/03.png)

Haciendo clic sobre el botón Configuración avanzada, en la pestaña Exclusiones es posible definir archivos y carpetas a evitar en el plan de copia, por ejemplo la carpeta $Recycle.bin, que almacena los archivos eliminados.


Desde la línea de comandos también puedo iniciar una copia de seguridad y almacenarla en una unidad de red compartida por ejemplo:

```ps
wbadmin start backup
-backuptarget :\\otroservidor\carpetascompartidas\copiasdeseguridad
-include:c: -allcritical -vssfull -quiet
```

Se creará una carpeta llamada "WindowsImageBackup". A continuación se creará automáticamente una carpeta con el nombre del servidor. **Cualquier copia de seguridad anterior se borrará de forma automática**.


## Restauración

La restauración completa puede hacerse utilizando el DVD de instalación (opción restauración).


## Enlaces externos

- expertosensistemas.com [Copias de seguridad en Windows Server 2012](http://www.expertosensistemas.com/copias-de-seguridad-en-windows-server-2012/).
- [Windows Server Backup: Copia de seguridad y restaurar - Solvetic](https://www.solvetic.com/tutoriales/article/2612-windows-server-backup-copia-de-seguridad-y-restaurar/).