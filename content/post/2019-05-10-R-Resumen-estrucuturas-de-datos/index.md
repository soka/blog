---
title: Estructuras de datos en R
subtitle: Como usarlas de forma eficiente
date: 2019-05-10
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Básico", "R - Datos", "Programación"]
---

![](images/r-logo.PNG)

Hoy es mi cumpleaños y con el día libre en el trabajo quiero revisar algunos conceptos fundamentales de **R** (hoy no toca Angular ni C#), en el post que sigue revisaré las estructuras de datos en **R** y como usarlas de forma eficiente, es el regalo que me hago a mi mismo.

Las estructuras de datos en **R** se pueden clasificar por su **dimensión** o por el **tipo de datos** que almacenan, llamaremos homogéneos a aquellos que solo pueden contener datos del mismo tipo, heterogéneos cuando se pueden mezclar diferentes tipos.

- Homogéneos: Vectores atómicos (1d), Matrices (2d), Arreglos (nd).
- Heterogéneos: Listas, _Data frame_.

![](images/data_structs.png)

[[Fuente](https://www.datasciencecentral.com/profiles/blogs/understand-basic-to-advance-data-structure-used-in-r-to-use-it)]

En **R** no existen las variables cuantitativas o escalares, una variable de tipo numérico podría parecerlo pero en realidad es un vector de longitud 1.

La mejor forma de inspeccionar el contenido de un objeto es usar la función `str()`, proporciona un resumen legible de la estructura de datos de una variable:

```R
> x <- 1
> str(x)
 num 1
```

## Vectores

Para saber más sobre vectores redacte un post previo en este ["enlace"](https://soka.gitlab.io/blog/post/2019-03-11-r-vectores/).

En **R** es la estructura básica de datos, los vectores pueden ser atómicos o listas. Tienen tres tipos de propiedades básicas:

- El tipo de datos que contiene se puede obtener con `typeof()`.
- Número de elementos que tiene con `length()`.
- Atributos o metadatos que podemos obtener con `attributes()`

```R
> v1 <- c(1,2,3)
> str(v1)
 num [1:3] 1 2 3
> typeof(v1)
[1] "double"
> length(v1)
[1] 3
> attributes(v1)
NULL
```

Los **elementos de un vector atómico deben ser del mismo tipo** mientras que los elementos de una lista pueden diferir. Podemos comprobarlo con `is.atomic()`, o con `is.list()` para listas.

```R
> is.atomic(v1)
[1] TRUE
```

### Vectores atómicos

Existen **cuatro tipos de vectores atómicos**: lógicos, números enteros o _integer_, _double_ (llamados también numéricos) y caracteres.

Los vectores se crean normalmente con `c()` (podemos usar `assign()` también).

De tipo _double_ o _numeric_:

```R
dbl_var <- c(1, 2.5, 4.5)
```

Usando prefijo "L" creamos de tipo entero:

```R
int_var <- c(1L, 6L, 10L)
```

Vector lógico, se puede abreviar TRUE como T y FALSE como F:

```R
log_var <- c(TRUE, FALSE, T, F)
```

_Character_:

```R
chr_var <- c("these are", "some strings")
```

Los vectores son siempre atómicos, aunque los declaremos de forma anidada:

```R
> c(1, c(2, c(3, 4)))
[1] 1 2 3 4
```

Los valores no definidos se especifican como `NA`, se pueden definir de un tipo específico como `NA_real_` (a double), `NA_integer_` y `NA_character`.

Como dar nombre a los campos de un vector:

```R
# Nombres a los campos
v1 <- c(uno='a',dos='b')
str(v1)
# Equivalente
v1 <- c('a','b')
names(v1) <- c("dno","dos")
str(v1)
```

## Vectores: Tipos y tests

Dado un vector se puede obtener su tipo con `typeof()` o comprobar si es de un tipo específico con `is.character()`, `is.double()`, `is.integer()`, `is.logical()`, o el más genérico `is.atomic()`.

```R
> int_var <- c(1L, 6L, 10L)
> typeof(int_var)
[1] "integer"

> is.integer(int_var)
[1] TRUE
> is.atomic(int_var)
[1] TRUE
```

Ojo: `is.numeric()` retorna TRUE para números reales y enteros.

## Vectores: Coerción

Todos los elementos de un vector atómico deben ser del mismo tipo, cuando tratamos de definir un vector con elementos de diferente tipo se produce la coerción convirtiendo todos al tipo más flexible (de menor a mayor: _logical_, _integer_, _double_ y _character_).

```R
> v2 <- c(1,"e",2)
> typeof(v2)
[1] "character"
> v2
[1] "1" "e" "2"
> str(v2)
 chr [1:3] "1" "e" "2"
```

Cuando lo hacemos con elementos de tipo lógico TRUE se convierte en 1 y FALSE en 0.

```R
> x <- c(FALSE, FALSE, TRUE)
> as.numeric(x)
[1] 0 0 1
```

Número total de TRUE:

```R
> sum(x)
[1] 1
```

Proporción de TRUE:

```R
> mean(x)
[1] 0.3333333
```

Podemos comprobar si todos los elementos son _TRUE_ o si alguno lo es:

```R
> v3 <- c(T,F,F)
> all(v3) # Chequea si todos T, [1] FALSE
[1] FALSE
> any(v3) # Chequea alguno es T, [1] TRUE
[1] TRUE
```

La coerción a menudo se produce de forma automática, por ejemplo realizando operaciones matemáticas (+,log,abs,etc) los convierte en real o entero, lo mismo sucede con los operadores lógicos (&, |, any, etc).

La coerción se puede hacer de forma explicita con `as.character()`, `as.double()`, `as.integer()`, o `as.logical()`.

## Listas

Post previo de ["Listas"](https://soka.gitlab.io/blog/post/2019-03-11-r-listas/).

Las listas son diferentes a los vectores ya que pueden contener elementos de cualquier tipo mezclados (incluyendo listas anidadas dentro de listas). Se declaran usando `list()`.

```R
> x <- list(1:3, "a", c(TRUE, FALSE, TRUE), c(2.3, 5.9))
> str(x)
List of 4
 $ : int [1:3] 1 2 3
 $ : chr "a"
 $ : logi [1:3] TRUE FALSE TRUE
 $ : num [1:2] 2.3 5.9
```

También se llaman vectores recursivos porque pueden declararse listas dentro de listas, cosa que no sucedía con los vectores atómicos.

```R
> x <- list(list(list(list())))
> str(x)
List of 1
 $ :List of 1
  ..$ :List of 1
  .. ..$ : list()
> is.recursive(x)
[1] TRUE
```

Si usamos `c()` con listas las combina y produce una nueva lista como resultado.

```R
> v1 <- c(list(1,2),list(3,4))
> View(v1)
```

Podemos obtener el tipo con `typeof()` (para una lista es "list"). Se puede comprobar si es una lista con `is.list()` y convertir en lista con `as.list()`.

## Atributos

Todos los objetos pueden tener un número arbitrario de atributos, se suele usar para almacenar metadatos de sobre un objeto. Se puede acceder a ellos de forma individual con `attr()` o a todos como una lista con `attributes()`.

```R
> y <- 1:10
> attr(y, "my_attribute") <- "This is a vector"
> attr(y, "my_attribute")
[1] "This is a vector"

> str(attributes(y))
List of 1
 $ my_attribute: chr "This is a vector"
```

## Factores

Un factor es un vector que sólo puede contener valores predefinidos, se usan para almacenar datos categóricos.

```R
> x <- factor(c("a", "b", "b", "a"))
> x
[1] a b b a
Levels: a b

> class(x)
[1] "factor"

> levels(x)
[1] "a" "b"

> x[2] <- "c"
Warning message:
In `[<-.factor`(`*tmp*`, 2, value = "c") :
  invalid factor level, NA generated

> x
[1] a    <NA> b    a
Levels: a b
```

## Matrices y arreglos

Añadiendo un atributo `dim` a un vector atómico se convierte en un arreglo multidimensional. Un caso especial de un _array_ son las matrices que tienen dos dimensiones.

Las matrices y arrays se crean usando `matrix()` o `array()`.

Ejemplo de creación de una matriz:

```R
a <- matrix(1:6, ncol = 3, nrow = 2)
```

El primer parámetro es una secuencia de números de 1 a 6, los otros dos parámetros son argumentos escalares con el número de columnas y filas. Es importante tener en cuenta que los datos se distribuyen siguiendo las columnas.

```R
> a
     [,1] [,2] [,3]
[1,]    1    3    5
[2,]    2    4    6
```

Ahora creamos un _array_ describiendo sus dimensiones con un vector pasado como parámetro en el segundo argumento:

```R
b <- array(1:12, c(2, 3, 2))
```

Podemos convertir un objeto como un vector en una matriz al vuelo con `dim()`:

```R
c <- 1:6  # Vector con secuencia de 1 a 6...

dim(c) <- c(3, 2) # Le damos dimension 3 filas y 2 columnas
> c
     [,1] [,2]
[1,]    1    4
[2,]    2    5
[3,]    3    6

> dim(c) <- c(2, 3)
> c
     [,1] [,2] [,3]
[1,]    1    3    5
[2,]    2    4    6
```

La función `length()` de un vector se traduce como `nrow()`, `ncol()` y `dim()`:

```R
> nrow(c)
[1] 2
> ncol(c)
[1] 3
> dim(c)
[1] 2 3
```

Podemos obtener los nombres de filas y columnas con `rownames()`, `colnames()` y `dimnames()`. Si ejecutamos alguna de estas funciones sobre la variable `c` arroja NULL ya que no están definidas, usa estas mismas funciones también podemos usarlas para fijar los nombres:

```R
rownames(c) <- c("A", "B")
colnames(c) <- c("a", "b", "c")
View(c)

> c
  a b c
A 1 3 5
B 2 4 6

> dimnames(c) <- list(c("Row01", "Row02"), c("Col01", "Col02", "Col03"))
> c
      Col01 Col02 Col03
Row01     1     3     5
Row02     2     4     6
```

`cbind()` y `rbind()` combinan una secuencia de vectores, matrices o _data-frames_ por columnas o por filas respectivamente.

```R
> m1 <- matrix(1:6,nrow = 2,ncol = 3)
> v1 <- c(1:2)
> m1 <- cbind(m1,v1)
> m1
           v1
[1,] 1 3 5  1
[2,] 2 4 6  2
```

## Data frames

Los _data.frame_ son probablemente las estructuras de datos más usadas, no dejan de ser más que una lista formada por vectores de la misma longitud. La estructura bidimensional de un _data.frame_ comparte propiedades de una matriz y una lista, esto significa que un _data.frame_ tiene `names()`, `colnames()` y `rownames()`.

Creación de un _data.frame_ con un vector como argumento de entrada con los datos:

```R
> df <- data.frame(x = 1:3, y = c("a", "b", "c"))
> str(df)
'data.frame':	3 obs. of  2 variables:
 $ x: int  1 2 3
 $ y: Factor w/ 3 levels "a","b","c": 1 2 3
```

El comportamiento por defecto de un _data.frame_ es convertir las cadenas de texto (_string_) en factores, usamos `stringsAsFactors = FALSE` para evitar este comportamiento.

```R
> df <- data.frame(x = 1:3,y = c("a", "b", "c"),stringsAsFactors = FALSE)
> str(df)
'data.frame':	3 obs. of  2 variables:
 $ x: int  1 2 3
 $ y: chr  "a" "b" "c"
```

## Ejercicios

- r / intro / [44-resumen-estructura-datos](https://gitlab.com/soka/r/tree/master/intro/44-resumen-estructura-datos).

## Enlaces internos

### Tipos de datos, objetos y variables

- ["Tipos de datos básicos en R"](https://soka.gitlab.io/blog/post/2019-03-03-tipos-de-datos-r/).
- ["Vectores"](https://soka.gitlab.io/blog/post/2019-03-11-r-vectores/).
- ["Matrices"](https://soka.gitlab.io/blog/post/2019-03-11-r-matrices/).
- ["Listas"](https://soka.gitlab.io/blog/post/2019-03-11-r-listas/).
- ["Data.Frame"](https://soka.gitlab.io/blog/post/2019-03-12-r-data-frame/).
- Data.frame ["Añadir una nueva columna"](https://soka.gitlab.io/blog/post/2019-03-01-ainadir-nueva-columna-df-en-r/).
- ["Objetos en R - Propiedades y conversión de tipo"](https://soka.gitlab.io/blog/post/2019-03-14-r-conversi%C3%B3n-de-objetos/).
- ["Seleccionar datos con [] y subset"](https://soka.gitlab.io/blog/post/2019-03-15-r-seleecionar-datos-subset/).
- ["Operaciones con Data Frames"](https://soka.gitlab.io/blog/post/2019-04-22-r-operaciones-con-data-frames/).
- ["Funciones"](https://soka.gitlab.io/blog/post/2019-03-15-r-funciones-intro/).
- ["Bucles"](https://soka.gitlab.io/blog/post/2019-03-17-r-bucles/).
- ["Objetos ts Series de tiempo"](https://soka.gitlab.io/blog/post/2019-04-16-r-series-de-tiempo/).

## Enlaces externos

- ["Understand Basic to Advanced Data Structures Used in R to use it Efficiently"](https://www.datasciencecentral.com/profiles/blogs/understand-basic-to-advance-data-structure-used-in-r-to-use-it):
  Fuente de donde voy a tomar la mayoría de información.
- ["OO field guide"](http://adv-r.had.co.nz/OO-essentials.html#oo).

```

```
