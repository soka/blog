---
title: FilePush comparte archivos usando la línea de comandos
subtitle: Sube archivos desde la línea de comandos y comparte mediante una URL
date: 2020-07-04
draft: false
description: Sube archivos desde la línea de comandos y comparte mediante una URL
tags: ["Aplicaciones","Archivos","Web","CLI","Compartir","FilePush","CURL","WGET","Comandos"]
---

[https://filepush.co/](https://filepush.co/) es un servicio Web que permite compartir pequeños archivos (32MB tamaño máximo) usando la línea de comandos para subirlos y descargarlos, los archivos se borran de forma automática transcurridos 7 días. Puede resultar de utilidad cuando queremos compartir algo rápido y sin complicarnos la vida accediendo a un servicio como Google Drive o Firefox Send por ejemplo. Además al funcionar por línea de comandos se presta a hacer un script para automatizar las subidas y bajadas.

![](img/01.png)

## Linux

## Subida con curl

Ejemplo básico en Linux para subir un archivo con [curl](https://curl.haxx.se/):

```bash
$ curl --upload-file hello.txt  https://filepush.co/upload/hello.txt
https://filepush.co/JYJG/hello.txt
```

El servidor responde con una URL única de donde podemos descargarlo. 

## Definir un alias 

Edito .bashrc para crear un alias: 

```bash 
filepush() {
    curl --progress-bar --upload-file "$1" https://filepush.co/upload/$(basename "$1") | tee /dev/null;
    echo
}

alias filepush=filepush
```

## Agregarlo a Thunar

En en el explorador de archivos Thunar "Editar >> Configurar acciones personalizadas" se puede añadir un menu contextual con un acción para subir el fichero, la URL de descarga se guarda en un fichero temp.txt:

```bash
sh -c `$file=%f; echo %f >> temp.txt; curl -v --upload-file %f  https://filepush.co/upload/$file >> temp.txt`
```

## Enlaces externos

- [https://filepush.co/usage/#ulinux](https://filepush.co/usage/#ulinux): Ejemplos básicos en Linux.
- [https://filepush.co/usage/#uwin](https://filepush.co/usage/#uwin): Ejemplos en Windows con PowerShell.
- [https://texnote.c43.co/](https://texnote.c43.co/): Publicar textos.
- [https://github.com/dutchcoders/transfer.sh/blob/master/examples.md](https://github.com/dutchcoders/transfer.sh/blob/master/examples.md): Ejemplos de un servicio similar transfer.sh que no funciona actualmente, me he basado para crear la función en .bashrc.


