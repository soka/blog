---
title: R - paquete dplyr
subtitle: Introducción básica
date: 2019-07-11
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Paquetes", "R - dplyr"]
---

![](images/r-logo.PNG)

El paquete **dplyr** proporciona una "gramática" para trabajar con _data frames_.

## Instalación

Instalamos de la forma habitual y cargamos el paquete en la sesión:

```r
install.packages("dplyr")
library(dplyr)
```

Para consultar la ayuda sobre el paquete:

```r
?dplyr
```

## Resumen de funcionalidades

- [**select()**](https://dplyr.tidyverse.org/reference/select.html): Selecciona un conjunto de columnas / variables.
- [**mutate()**](https://dplyr.tidyverse.org/reference/mutate.html): Añade nuevas variables / columnas o transforma variables existentes.
- [**filter()**](https://dplyr.tidyverse.org/reference/filter.html): Filtrar filas que cumplan una determinada condición.
- [**summarise()**](https://dplyr.tidyverse.org/reference/summarise.html): Crea una o más variables escalares a modo de resumen a partir de variables del DF.
- ["group_by()"](https://dplyr.tidyverse.org/reference/group_by.html):.
- [**arrange()**](https://dplyr.tidyverse.org/reference/arrange.html): Reordena las filas del DF.

Pero de todo lo que ofrece **dplyr** seguramente habrás visto en un montón de ejemplos de código el operador de tubería (_pipe_ en adelante) con la forma `%>%`. Tanta curiosidad me suscitaba en su momento que necesitaba escribir algo sobre ello.

## Dataset de ejemplo

![](images/01.PNG)

Los _datasets_ que vienen con R generalmente son super aburridos, para darle un poco de alegría al asunto me he encontrado un CSV con los protagonistas de Star Wars, obtengo el DF directamente de una URL y selecciono la variable/columna _gender_ (género).

El _dataset_ está obtenido de este [enlace](https://github.com/hyzhangsf/stat133-1/blob/master/datasets/starwars.csv), por si acaso también lo he descargado y lo he dejado junto a los scripts R en GitLab [aquí](https://gitlab.com/soka/r/tree/master/paquetes/dplyr).

![](images/03.PNG)

Otro dataset similar en esta [enlace](https://gist.github.com/jncraton/68beb88e6027d9321373) sobre figuras de acción (creo) de Star Wars.

## Pipes con %>%

El operador _pipeline_ `%>%` permite concatenar varias operaciones, no resulta nada innovador, con la sintaxis básica se puede hacer lo mismo pero este operador facilita la legibilidad.

Este es uno de los ejemplos más sencillos que se me ocurren, antes para crear un vector y sumar sus elementos hubieses hecho esto:

```r
sum(c(1,2,4,5))
```

Con el operador _pipe_:

```r
c(1,2,4,5) %>% sum()
```

Lo de arriba sólo es una muestra de como funciona, imagina que seguimos anidando nuevas funciones una dentro de otra, con el operador `%>%` se leer mucho más claro.

## Select(): Seleccionar columnas

```r
starWarsDF <- read.csv(file='https://raw.githubusercontent.com/hyzhangsf/stat133-1/master/datasets/starwars.csv')
genderDF <- select(starWarsDF,gender)
str(genderDF)
```

Podemos seleccionar más de una variable `select(starWarsDF,gender,eyecolor)`.

Podemos utilizar la notación ":" para seleccionar un rango de columnas `select(starWarsDF,gender:eyecolor)`.

Podemos invertir las variables de la selección `select(starWarsDF, -(gender:eyecolor))`.

Columnas cuyo nombre empieza `select(starWarsDF,starts_with("h"))` o acaba con ciertos carácteres `select(starWarsDF,ends_with("n"))`.

## filter(): Filtrar filas según una condición

![Chewbacca](images/02.PNG)

¿Protagonistas con sobrepeso? Chewbacca y Grievous (he tenido que mirar quien era este), aunque considerando sus alturas puede ser normal:

```r
filter(starWarsDF, weight >= 100)
```

Ahora vamos a combinar varias condiciones:

```r
filter(starWarsDF, weight >= 50, name %in% c('Anakin Skywalker','Padme Amidala','Chewbacca'))
```

- Operadores lógicos: &, \, xor, !, any, all.
- Comparadores: <, >, ==, <=, >=, !=, %in%, is.na, !is.na.

Otro ejemplo usando el operador lógico & (AND):

```r
filter(starWarsDF, weight>=50 & weight<90)
```

## arrange(): Ordenar las filas de acuerdo a una o varias columnas/variables

Por defecto `arrange()` ordena las filas por orden ascendente.

Ordenar por orden ascendente y descendente por una variable/columna:

```r
arrange(starWarsDF, homeland)
arrange(starWarsDF, desc(homeland))
```

Ordenar según varias variables:

```r
arrange(starWarsDF, homeland, species)
```

## rename(): Renombrar una/s variable

```r
rename(starWarsDF, nombre = name, color_pelo = haircolor)
colnames(starWarsDF)
```

## mutate()



## Código fuente

- GitLab: r / paquetes / [dplyr](https://gitlab.com/soka/r/tree/master/paquetes/dplyr).

## Enlaces externos

- ["https://dplyr.tidyverse.org/"](https://dplyr.tidyverse.org/): Recomiendo empezar por aquí.
- Cheatsheet [PDF]["data transformation with dplyr::cheatsheet"](https://github.com/rstudio/cheatsheets/blob/master/data-transformation.pdf).
- cran.r-project.org ["Introduction to dplyr - R Project"](https://cran.r-project.org/web/packages/dplyr/vignettes/dplyr.html).
- ["Manipulación de Data Frames con el paquete dplyr"](https://rsanchezs.gitbooks.io/rprogramming/content/chapter9/index.html).
- ["fdelaunay/tutorial-dplyr-es"](https://github.com/fdelaunay/tutorial-dplyr-es/blob/master/R/tutorial-dplyr.md).
- R para Ciencia de Datos ["5 Transformación de datos"](https://es.r4ds.hadley.nz/transform.html).
- ["Pipes in R Tutorial For Beginners"](https://www.datacamp.com/community/tutorials/pipe-r-tutorial).
- r-bootcamp.netlify.com ["Chapter 3: Introduction to dplyr"](https://r-bootcamp.netlify.com/chapter3).
- [https://www.rstudio.com/wp-content/uploads/2015/02/data-wrangling-cheatsheet.pdf](https://www.rstudio.com/wp-content/uploads/2015/02/data-wrangling-cheatsheet.pdf).
