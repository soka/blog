---
title: Recursos y fuentes de información
subtitle: Taller contraseñas seguras
date: 2021-03-04
draft: false
description: 
tags: ["Seguridad","Contraseñas","Ciberseguridad","Taller","Recursos"]
---

<!-- vscode-markdown-toc -->
* 1. [ENTIDADES E INSTITUCIONES OFICIALES](#ENTIDADESEINSTITUCIONESOFICIALES)
	* 1.1. [INCIBE](#INCIBE)
	* 1.2. [OSI](#OSI)
	* 1.3. [CCN-CERT](#CCN-CERT)
	* 1.4. [Basque Cybersecurity Centre - BCSC](#BasqueCybersecurityCentre-BCSC)
* 2. [ENLACES INTERNOS](#ENLACESINTERNOS)
* 3. [ENLACES EXTERNOS](#ENLACESEXTERNOS)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='ENTIDADESEINSTITUCIONESOFICIALES'></a>ENTIDADES E INSTITUCIONES OFICIALES

###  1.1. <a name='INCIBE'></a>INCIBE

![](img/05.png)

El [INCIBE](https://www.incibe.es/) (2006) es el Instituto Nacional de Ciberseguridad Español, se dedica a dar soporte de seguridad informática a empresas y a la ciudadanía en general, desarrolla un gran de número de actividades en este ámbito, entre otras cosas organiza eventos, jornadas, imparte formación especializada o ofrece soportes formativos.

Si quieres estar al día deberías conocer algunos de los servicios que ofrecen:

* Un [blog de noticias](https://www.incibe.es/protege-tu-empresa/blog) generales.
* [Avisos de seguridad](https://www.incibe.es/protege-tu-empresa/avisos-seguridad): Informa de campañas de phising, avisos de de seguridad, vulnerabilidades en productos software, etc.
* Servicio de [**suscripción a boletines de INCIBE**](https://www.incibe.es/suscripciones) para recibir periódicamente un correo electrónico con la información más destacable de los nuevos contenidos de INCIBE.
* **Canal oficial del servicio Protege tu empresa de INCIBE en Telegram** [https://t.me/ProtegeTuEmpresa](https://t.me/ProtegeTuEmpresa).
* [Línea de ayuda en Ciberseguridad](https://www.incibe.es/linea-de-ayuda-en-ciberseguridad). INCIBE pone a disposición de empresas, ciudadanos, padres, menores y educadores una línea telefónica gratuita de ayuda en ciberseguridad: 017. En este [enlace](https://www.incibe-cert.es/respuesta-incidentes) también otros medios de contacto que ofrecen respuestas antes incidentes.

Ademas de los servicios citados arriba te recomiendo que explores el resto de apartados del menú de su Web con manuales, infografías sobre todo tipo de temas, aquí resumo algunos enlaces con información destacada, en general todos los recursos e información está dirigido al público en general, no se requieren conocimientos muy técnicos ni especializados.

* [Kit de concienciación](https://www.incibe.es/protege-tu-empresa/kit-concienciacion) para empresas que se puede descargar en formato comprimido. Ofrece un gran número de manuales, entre ellos existe una carpeta que trata específicamente el tema de las contraseñas.

###  1.2. <a name='OSI'></a>OSI

![](img/06.jpeg)

El [**OSI**](https://www.osi.es/es) es la Oficina de Seguridad del Internauta de INCIBE proporcionan la información y el soporte necesarios para evitar y resolver los problemas de seguridad que pueden existir al navegar por Internet.

Una de las actividades más destacadas del OSI son las [campañas](https://www.osi.es/es/campanas) de concienciación donde podemos encontrar una sobre [contraseñas seguras](https://www.osi.es/es/campanas/contrasenas-seguras).

Artículos y recursos de la campaña contraseñas seguras:

* Artículo del blog [¿Sabías que el 90% de las contraseñas son vulnerables?](https://www.osi.es/actualidad/blog/2019/02/06/sabias-que-el-90-de-las-contrasenas-son-vulnerables).
* Vídeo [¿Cuánto tardarían en averiguar mis contraseñas?](¿Cuánto tardarían en averiguar mis contraseñas?).
* Historia real [Una contraseña fácil, ¿pero cuántas cuentas comprometidas?](https://www.osi.es/actualidad/historias-reales/2019/02/13/una-contrasena-facil-pero-cuantas-cuentas-comprometidas).
* Infografía [Ataques a las contraseñas](https://www.osi.es/es/campanas/contrasenas-seguras/ataques-contrasenas).
* Artículo del blog [Típicos errores que cometemos al usar nuestras contraseñas, y cómo corregirlos](https://www.osi.es/actualidad/blog/2019/02/20/tipicos-errores-que-cometemos-al-usar-nuestras-contrasenas-y-como).
* Recurso pedagógico [Mejora tus contraseñas](https://www.osi.es/campanas/contrasenas-seguras/mejora-tus-contrasenas).
* Artículo del blog [El factor de autenticación doble y múltiple](https://www.osi.es/actualidad/blog/2019/02/27/el-factor-de-autenticacion-doble-y-multiple).
* Recurso Pedagógico [El camino seguro](https://www.osi.es/campanas/contrasenas-seguras/el-camino-seguro).
* Recurso Pedagógico [Crea tu contraseña segura paso a paso](https://www.osi.es/es/campanas/crea-tu-contrasena-segura).
* Artículo de blog [Gestores de contraseñas: ¿cómo funcionan?](https://www.osi.es/es/actualidad/blog/2021/01/27/gestores-de-contrasenas-como-funcionan).
* Artículo de blog [Registrarte con tu cuenta de Google, Facebook o Twitter: ventajas e inconvenientes](https://www.osi.es/es/actualidad/blog/2021/02/12/registrarte-con-tu-cuenta-de-google-facebook-o-twitter-ventajas-e).

Estas infografías están sacadas de la misma campaña.

[Ataques a las contraseñas](https://www.osi.es/es/campanas/contrasenas-seguras/ataques-contrasenas):

![](img/02.jpg)

[Crea tu contraseña segura paso a paso](https://www.osi.es/es/campanas/crea-tu-contrasena-segura):

![](img/03.png)

También hay un catálogo interesante con [herramientas gratuitas](https://www.osi.es/es/herramientas), las dedicadas al [cifrado y gestión de contraseñas](https://www.osi.es/es/herramientas-gratuitas?herramienta_selec%5B0%5D=118):

* [Password Boss](https://www.osi.es/es/herramientas-gratuitas/password-boss) es un gestor de contraseñas. Además implementa protección antirrobo con borrado remoto, alertas de seguridad, identificación con doble factor de autenticidad, acceso seguro a páginas web mediante un navegador propio y un generador de... 
* [Keeweb](https://www.osi.es/es/herramientas-gratuitas/keeweb): KeeWeb es una herramienta de gestor de contraseñas. Está disponible tanto como herramienta de escritorio y como aplicación web online. Incluye un generador de contraseñas.
* [Dashlane](https://www.osi.es/es/herramientas-gratuitas/dashlane) es un administrador de contraseñas. Permite iniciar sesión automáticamente en cualquier sitio web y desde todos los dispositivos. También integra las funciones de billetera digital).
* [Cifrado de Windows (BitLocker)](https://www.osi.es/es/herramientas-gratuitas/cifrado-de-windows-bitlocker): BitLocker está desarrollado por Microsoft y está incluido por defecto en las versiones Ultimate y Enterprise desde Windows Vista en adelante. Permite mantener a salvo todo, desde documentos hasta contraseñas, ya que cifra toda la unidad en la que Windows y...
* [My Lockbox](https://www.osi.es/es/herramientas-gratuitas/my-lockbox) es una aplicación que nos permite tener una carpeta oculta y protegida con contraseña de una manera fácil y rápida.
* [LastPass Manager](https://www.osi.es/es/herramientas-gratuitas/lastpass-manager) es un gestor de contraseñas especialmente enfocado a almacenar contraseñas de distintas páginas web. Por ello, permite instalar la herramienta como un plugin del navegador.
* [KeePass Password Safe](https://www.osi.es/es/herramientas-gratuitas/keepass-password-safe) es un gestor de contraseñas que permite proteger las distintas contraseñas de forma segura. Para ello, se introducen las distintas contraseñas en una base de datos y quedan protegidas por una  única contraseña maestra, con la que es...

###  1.3. <a name='CCN-CERT'></a>CCN-CERT

![](img/04.png)

[CCN-CERT](https://www.ccn-cert.cni.es/) es la Capacidad de Respuesta a incidentes de Seguridad de la Información del [Centro Criptológico Nacional](https://www.ccn.cni.es/index.php/es/), CCN, adscrito al Centro Nacional de Inteligencia, CNI.

_"Su misión, por tanto, es contribuir a la mejora de la ciberseguridad española, siendo el centro de alerta y respuesta nacional que coopere y ayude a responder de forma rápida y eficiente a los ciberataques y a afrontar de forma activa las ciberamenazas, incluyendo la coordinación a nivel público estatal de las distintas Capacidades de Respuesta a Incidentes o Centros de Operaciones de Ciberseguridad existentes."_

Servicios CCN-CERT:

* Gestión y [notificación de incidentes](https://www.ccn-cert.cni.es/gestion-de-incidentes/notificacion-de-incidentes.html).
* [Sistema de Alerta Temprana (SAT)](https://www.ccn-cert.cni.es/gestion-de-incidentes/sistema-de-alerta-temprana-sat.html).
* Seguridad al día > [**Alertas CCN-CERT**](https://www.ccn-cert.cni.es/seguridad-al-dia/alertas-ccn-cert.html).
* Seguridad al día > [**Avisos CCN-CERT**](https://www.ccn-cert.cni.es/seguridad-al-dia/avisos-ccn-cert.html).
* Seguridad al día > [Noticias de actualidad](https://www.ccn-cert.cni.es/seguridad-al-dia/noticias-seguridad.html).
* **Canal de difusión en Telegram** [https://t.me/CCNCERT](https://t.me/CCNCERT).
* Guías ([CCN-STIC-436 Herramientas de Análisis de Contraseñas](https://www.ccn-cert.cni.es/series-ccn-stic/400-guias-generales/113-ccn-stic-436-herramientas-de-analisis-de-contrasenas/file.html) con registro previo).

###  1.4. <a name='BasqueCybersecurityCentre-BCSC'></a>Basque Cybersecurity Centre - BCSC

![](img/07.png)

El [**Centro Vasco de Ciberseguridad**](https://www.basquecybersecurity.eus/eu/) (BCSC) es la organización designada por el Gobierno Vasco para promover la ciberseguridad en Euskadi. 

**Servicios**:

* [Asesoramiento frente a incidentes](https://www.basquecybersecurity.eus/es/bcsc/asesoramiento-frente-incidentes.html).
* Última hora [Alertas destacadas](https://www.basquecybersecurity.eus/es/alertas/).
* [Infografías](https://www.basquecybersecurity.eus/es/publicaciones/infografias/?pag=2) > [Verificación en dos pasos](https://www.basquecybersecurity.eus/archivos/201906/dos_pasos_verificacion.pdf?1).

![](img/dos_pasos_verificacion-1.png)

##  2. <a name='ENLACESINTERNOS'></a>ENLACES INTERNOS

* [Taller contraseñas seguras](https://soka.gitlab.io/blog/post/2021-02-25-taller-contraseinas-seguras/).
* [La clave eres tú](https://soka.gitlab.io/blog/post/2021-01-15-la-clave-eres-tu/).
* [Manual LastPass](https://soka.gitlab.io/blog/post/2021-01-24-manual-lastpass/).
* [Alternativas a LastPass](https://soka.gitlab.io/blog/post/2021-02-20-alternativas-lastpass/).
* [Conoce a tu enemigo](https://soka.gitlab.io/blog/post/2021-02-23-conoce-a-tu-enemigo_tipos_ataque_contraseinas/).
* [Seguridad con cuentas de Google](https://soka.gitlab.io/blog/post/2021-02-21-seguridad-cuentas-google/).
* [Recursos y fuentes de información](https://soka.gitlab.io/blog/post/2021-03-04-recursos-taller-seguridad-contrase%C3%B1as/).