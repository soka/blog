---
title: OpenBoard, pizarra digital interactiva multiplataforma [borrador]
subtitle: Herramientas de trabajo colaborativo remoto
date: 2020-04-28
draft: false
description: 
tags: ["Aplicaciones","Web","Pizarra digital","Lienzo","Trabajo colaborativo","OpenBoard"]
---

[**OpenBoard**](http://openboard.ch/index.en.html) es una pizarra digital interactiva (PDI en adelante) multiplataforma para sistemas operativos Win,Linux y macOS. Las pizarras interactivas pueden ser un recurso muy útil para la docencia a distancia o para una sesión de tormenta de ideas en una reunión online. [**OpenBoard**](http://openboard.ch/index.en.html) es una aplicación de escritorio, se distribuye como **software libre** y se puede **descargar de forma gratuita**. La aplicación se instala muy rápido, La primera vez que lo ejecuto tiene este aspecto:

![](img/01.jpg)

## Controles básicos

En la barra inferior de controles se puede seleccionar lápiz o subrayador para comenzar a dibujar, también existe un borrador para borrar cualquier parte, la opción deshacer también está presente claro.

Con la flecha podemos seleccionar un área de la pantalla.

La mano señalando permite interactuar con los elementos y moverlos, con la mano se puede mover el lienzo. También tiene una lupa para aumentar o reducir la imagen.

En el momento de hacer un recorte nos permite seleccionar donde añadirlo, un documento en [**OpenBoard**](http://openboard.ch/index.en.html) está formado por varios lienzos o páginas.

El puntero láser puede ser muy útil para señalar alguna parte del lienzo, también se puede introducir texto y definir la fuente, su tamaño o color.

En el menú superior se pueden seleccionar propiedades del lápiz, color y grosor. Se puede visualizar una rejilla de fondo, muy útil si queremos hacer dibujo geométrico.

## Atajos de teclado

Se pueden los botones del menú superior o las teclas:

- "Page Up" Se mueve a la página siguiente.
- "Page Down" Se mueve a la página anterior.
- "Insert" (Ins) Inserta una nueva página en blanco.
- "End" Se mueve a la última página.
- "Home" o inicio se mueve a primera página.

## Modo Web

El modo Web de [**OpenBoard**](http://openboard.ch/index.en.html) permite 

##

## Vídeos

OpenBoard. Herramientas digitales básicas para docentes inexpertos:

<iframe width="560" height="315" src="https://www.youtube.com/embed/utqZZMwLOQA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Alternativas Xournal++

https://github.com/xournalpp/xournalpp

## Enlaces internos

- [Pizarras digitales colaborativas](https://soka.gitlab.io/blog/post/2020-04-28-un-par-de-pizarras-digitales/): Witeboard, AWW - A Web Whiteboard, Ziteboard.

## Enlaces externos

- [OpenBoard](http://openboard.ch/index.en.html): Web principal del proyecto.


