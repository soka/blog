---
title: Taller - Introducción a Docker y Kubernetes
subtitle:  Primera aproximación práctica al mundo de los contenedores y en concreto a Docker
date: 2022-06-15
draft: true
description: Primera aproximación práctica al mundo de los contenedores y en concreto a Docker
tags: ["Docker","Contenedores","Formación","Talleres","Aplicaciones","Sistemas"]
---

Programa:

* Introducción a los contenedores y a Docker
* Comandos y operaciones básicas
* Dockerfile
* Imágenes
* Contenedores, comandos avanzados
* Redes
* Docker-compose
* Introducción a Kubernetes (minikube)

## Instalación en Win10

Seguir los pasos de este tutorial: [Install Docker Desktop on Windows](https://docs.docker.com/desktop/windows/install/). Descargar "Docker Desktop Installer.exe", previamente instalar WSL (Windows Subsystem for Linux) ([Install Linux on Windows with WSL](https://docs.microsoft.com/en-us/windows/wsl/install)) con PS como admin de la máquina.

Inicio proceso instalación usando WSL como backend:

![](img/01.png)

## Repositorio para introducirse en Docker

![](img/02.png)

```
PS> docker container ls -a
CONTAINER ID   IMAGE        COMMAND                  CREATED         STATUS                     PORTS     NAMES
0861475c72d1   alpine/git   "git clone https://g…"   3 minutes ago   Exited (0) 3 minutes ago             repo

PS> docker container rm 0861475c72d1

PS> docker run -d -p 80:80 docker/getting-started
```

![](img/03.png)

Podemos abrir [http://localhost/tutorial/](http://localhost/tutorial/) con el contenedor en ejecución para iniciar el tutorial.

![](img/04.png)


## Comandos básicos

Listamos las imágenes que tenemos instaladas:

```
docker image ls -a
``` 

Contenedores que tenemos en ejecución:

```
docker ps
```

Para manejar contenedores:

```
docker container ls --help
docker container run alpine echo "hola mundo"
```

Los contenedores tienen un ID asociado único aleatorio.

Damos un nombre al contenedor para identificarlo bien:

```
docker container run --name mi_contenedor alpine echo "hola mundo!"
```

Mapear puertos, del 80 de Nginx al 8080 de mi maquina:

```
docker container run --name mi nginx -d -p 8080:80 -d nginx
docker container ls -a
```

Ahora podemos abrir [http://localhost:8080](http://localhost:8080) para comprobar.

```
docker container logs 2ec355039f5d
```

```
docker container commit -m "mi contenedor..." 2ec355039f5d
```

Línea de comandos interactiva (ctrl+d para salir):

```
docker container run -it --name debi debian:latest /bin/bash
```

## Dockerfile

```
FROM ubuntu

RUN apt update && apt install -y cowsay fortune
```

docker build . -t /usr/games/cowsay "Hello World"

## Kubernetes



## Enlaces internos

* [Entradas etiquetadas con Docker](https://soka.gitlab.io/blog/tags/docker/).

## Enlaces externos

* [https://github.com/docker/getting-started](https://github.com/docker/getting-started).



