---
title: Acceso a APIs con R
subtitle: Cliente REST usando httr y jsonlite
date: 2019-05-18
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Básico", "R - Redes", "Programación"]
---

![](images/r-logo.PNG)

En este POST voy a tratar sobre un tema que siempre me apasiona en cualquier lenguaje de programación, como crear un cliente en **R** para interactuar con un servidor REST HTTP JSON.

## Como montar un servidor JSON de pruebas en pocos minutos

Para montar un servidor que acepte todo tipo de operaciones no me he complicado la vida, [**JSON Server**](https://www.npmjs.com/package/json-server) se instala con NPM y en pocos minutos tenemos un servidor REST HTTP JSON básico funcionando de forma local.

Estoy redactando un post más específico sobre **JSON Server** en este [enlace](https://soka.gitlab.io/angular/otros/json-server/json-server/) pero por el momento basta con saber un par de cosas.

La instalación con el gestor de paquetes NPM es trivial:

```npm
npm install -g json-server
```

El servidor no tiene dependencias con bases de datos externas ni otras infraestructuras, los datos con sintaxis JSON se almacenan en un fichero local, creamos un fichero de ejemplo "bd.json":

```json
{
  "posts": [{ "id": 1, "title": "json-server", "author": "typicode" }],
  "comments": [{ "id": 1, "body": "some comment", "postId": 1 }],
  "profile": { "name": "typicode" }
}
```

Ya está, podemos arrancar el servidor:

```npm
json-server --watch db.json
```

Ya podemos probarlo en nuestro navegador introduciendo [http://localhost:3000/posts](http://localhost:3000/posts) obtendremos algo así:

```json
[
  {
    "id": 1,
    "title": "json-server",
    "author": "typicode"
  }
]
```

![](images/01.PNG)

Podemos obtener un registro por su ID por ejemplo [http://localhost:3000/comments/1](http://localhost:3000/comments/1).

Pero **JSON Server** no se reduce a aceptar peticiones GET, también podemos realizar operaciones POST, PUT, PATCH o DELETE que modifican nuestro fichero "db.json". Para probar el resto de operaciones yo recomiendo [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) para Chrome y así entender como funcionan.

La imagen inferior corresponde a una consulta POST (como un INSERT en SQL), debemos definir el formato de _Body_ como _application/json_ y de texto plano _raw_.

![](images/02.PNG)

Si se ejecuta con éxito retorna un [código de estado HTTP](https://www.tutorialspoint.com/http/http_status_codes.htm) 201 que significa que el recurso se ha creado.

## Llamada básica con httr::GET()

La llamada de arriba deserializa el JSON y lo convierte en un _data.frame_, si queremos hacer una llamada HTTP con ["httr"](https://cran.r-project.org/web/packages/httr/index.html):

```R
require("httr")

> url_posts <- "http://localhost:3000/posts"
> url_posts_GET <- httr::GET(url = url_posts )
> url_posts_GET
Response [http://localhost:3000/posts]
  Date: 2019-05-18 16:06
  Status: 200
  Content-Type: application/json; charset=utf-8
  Size: 77 B
[
  {
    "id": 1,
    "title": "json-server",
    "author": "typicode"
  }
```

La función [GET](https://www.rdocumentation.org/packages/httr/versions/1.4.0/topics/GET) retorna todo el contenido HTTP de respuesta en una variable tipo lista.

Que podemos encontrar dentro de los resultados `httr::GET()`:

- `Date`: Fecha y hora de la petición.
- `Status`: [Código de estado HTTP](https://www.tutorialspoint.com/http/http_status_codes.htm) devuelto, "200" indica que se ha ejecutado OK.
- `Content-Type`: El tipo de contenido devuelto, en este caso es JSON codificado como UTF-8.
- `Size`: Tamaño del área de datos.

## Estado de la petición con httr::http_status()

Es una buena práctica usar `httr::http_status` para comprobar como ha ido nuestra llamada:

```R
> httr::http_status(url_posts_GET)
$category
[1] "Success"

$reason
[1] "OK"

$message
[1] "Success: (200) OK"
```

## Leyendo JSON, jsonlite al rescate

Ahora vamos a realizar la mismas operaciones pero en **R** , usaremos la librería [jsonlite](https://cran.r-project.org/web/packages/jsonlite/index.html). Lo primero es cargarla en nuestro script (si no la tenéis instalada ``):

```R
library(jsonlite)
```

Vamos a realizar nuestra primera llamada para obtener los _posts_:

```R
posts <- fromJSON("http://localhost:3000/posts")
```

La función [`fromJSON`](https://www.rdocumentation.org/packages/jsonlite/versions/1.6/topics/toJSON%2C%20fromJSON) acepta una cadena JSON o como en el ejemplo de arriba podemos invocar una URL directamente.

El resultado es un _data.frame_:

```R
> # Clase
> base::class(posts)
[1] "data.frame"
> # Estructura
> posts %>% str()
'data.frame':	1 obs. of  3 variables:
 $ id    : int 1
 $ title : chr "json-server"
 $ author: chr "typicode"
> # Visualizador
> posts %>% View()
> # Nombres de columnas
> posts %>% names()
[1] "id"     "title"  "author"
> posts$title
[1] "json-server"
```

La `%>%` no es más que una tubería o _pipe_ del paquete ["dplyr"](https://www.rdocumentation.org/packages/dplyr/versions/0.7.8).

Vamos a realizar una consulta GET filtrando el resultado, he usado la función [`str_c`](https://www.rdocumentation.org/packages/stringr/versions/1.4.0/topics/str_c) de la librería [`stringr`](https://cran.r-project.org/web/packages/stringr/vignettes/stringr.html) para concatenar los parámetros de la llamada, así queda más fácil para reutilizar para futuras llamadas.

```R
url_root <- stringr::str_c("http://", "localhost:3000")
url_filter <- stringr::str_c(url_root,"/posts?id=1")
posts_filter <- fromJSON(url_filter)
```

## Ejercicios

- r \ intro \ 45-R-Acceso-APIs \ [45-R-Acceso-APIs.R](https://gitlab.com/soka/r/blob/master/intro/45-R-Acceso-APIs/45-R-Acceso-APIs.R).

## Enlaces internos

## Enlaces externos

- programmableweb.com ["How to Access Any RESTful API Using the R Language"](https://www.programmableweb.com/news/how-to-access-any-restful-api-using-r-language/how-to/2017/07/21).
- ["Fetching JSON data from REST APIs"](https://cran.r-project.org/web/packages/jsonlite/vignettes/json-apis.html).
- storybench.org ["How to access APIs in R"](http://www.storybench.org/how-to-access-apis-in-r/).
- rdocumentation.org ["jsonlite v1.6"](https://www.rdocumentation.org/packages/jsonlite/versions/1.6).
- rdocumentation.org ["toJSON, fromJSON"](https://www.rdocumentation.org/packages/jsonlite/versions/1.6/topics/toJSON%2C%20fromJSON).
- medium.com ["Create A REST API With JSON Server"](https://medium.com/codingthesmartway-com-blog/create-a-rest-api-with-json-server-36da8680136d) - Sebastian Eschweiler.
- ["Pipes in R Tutorial For Beginners (article) - DataCamp"](https://www.datacamp.com/community/tutorials/pipe-r-tutorial).
- ["Curso de R | Manipulación de data frames con 'dplyr' - Mauricio ..."](https://mauricioanderson.com/curso-r-dplyr/).
