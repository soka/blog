---
title: geom_bar() - Gráficos de barras
subtitle: Introducción a ggplot2
date: 2019-04-04
draft: true
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "Programación",
    "R - Básico",
    "R - Gráficos",
    "R - Diagramas",
    "R - ggplot2",
    "Plotly",
    "Diagramas",
  ]
---

![](images/ggplot_hex.jpg)

# Ejercicios

# Enlaces internos

# Enlaces externos
