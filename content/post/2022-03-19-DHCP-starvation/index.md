---
title: DHCP starvation [borrador en progreso]
subtitle: Kali Linux
date: 2022-03-18
draft: false
description: Kali Linux
tags: ["Seguridad Informática","SEIN","SEINF","Kali Linux","DHCP","DoS"]
---

<!-- vscode-markdown-toc -->
* 1. [Introducción](#Introduccin)
* 2. [Montando el ataque](#Montandoelataque)
* 3. [Detección](#Deteccin)
* 4. [Prevención](#Prevencin)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Introduccin'></a>Introducción

En un ataque de **agotamiento DHCP (DHCP Starvation)** un usuario malicioso inunda con peticiones DHCP el servidor utilizando diferentes direcciones MAC. Con esto puede llegar a agotar por completo el espacio de direcciones asignables por los servidores DHCP (Dynamic Host Configuration Protocol) por un período indefinido de tiempo. Los clientes de la red de la víctima entonces no obtendrán automáticamente una IP para acceder a la red por lo que este ataque podría ser clasificado como una denegación de servicio.

##  2. <a name='Montandoelataque'></a>Montando el ataque

##  3. <a name='Deteccin'></a>Detección


##  4. <a name='Prevencin'></a>Prevención