---
title: API pública de Twitter
subtitle: Acceso a redes sociales con R
date: 2019-04-24
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico", "R - Twitter", "Twitter"]
---

![](images/r-logo.PNG)

Para poder usar la API de **Twitter** necesitamos lógicamente tener una cuenta creada y debemos crear unas claves en [https://developer.twitter.com/en/apps](https://developer.twitter.com/en/apps) para autorizar el acceso a nuestra aplicación R, al finalizar el alta tenemos una API _key_ para autenticarnos desde nuestro script R, también generaremos un _access token_.

**Twitter** impone unas condiciones de servicio que se pueden consultar en este enlace: [https://developer.twitter.com/en/docs/basics/rate-limiting.html](https://developer.twitter.com/en/docs/basics/rate-limiting.html), conviene leerlas con detenimiento.

Todos los entresijos de la API y su sintaxis se pueden consultar en [https://developer.twitter.com/en/docs.html](https://developer.twitter.com/en/docs.html).

![](images/twitter-01.PNG)

## Paquete twitteR

Para interactuar con la API de Twitter usaremos el paquete de CRAN [**twitteR**](https://www.rdocumentation.org/packages/twitteR) como cliente para lenguajes basados en R. El manual de uso de **twitteR** en PDF lo he dejado junto al ejercicio en mi repositorio GitLab en este [enlace](https://gitlab.com/soka/r/blob/master/intro/41-twitter/twitteR.pdf)

![](images/twitterr-cran-01.PNG)

Lo primero que vamos a hacer es instalar el paquete:

```R
install.packages("twitteR")
library(twitteR)
```

En RStudio también se puede hacer desde el menú "Tools":

![](images/rstudio-tools.PNG)

Guardamos las claves proporcionadas por Twitter en variables:

```R
consumer_key <- "cC6jy8FtttIbABH8gxuwJw96W5"
consumer_secret <- "R5ZLnjKiojyBSy8AxAfiyP291S8fCeWdPllg4ZeICqAxBHKuqv"
access_token <- "1120954979651656628-upZywPZ510qsZaMUDytX4mNcEySOod"
access_secret <- "2tw8YsDs9MzFgj6u3dB7hNvT59xmgpUOWMfWgOq7OTKNv"
```

Lo primero es llamar a la función `setup_twitter_oauth` para autenticarnos con la Web usando [OAuth](https://es.wikipedia.org/wiki/OAuth) (Open Authorization).

```R
setup_twitter_oauth(consumer_key,consumer_secret,access_token,access_secret)
```

R nos pregunta si queremos usar un fichero local ".httr-oauth" para guardar las credenciales entre sesiones.

La función `getCurRateLimitInfo()` proporciona los límites de uso como un _data.frame_:

```R
> getCurRateLimitInfo()
                                 resource limit remaining               reset
1                             /lists/list    15        15 2019-04-24 09:12:23
2                      /lists/memberships    75        75 2019-04-24 09:12:23
3                 /lists/subscribers/show    15        15 2019-04-24 09:12:23
4                          /lists/members   900       900 2019-04-24 09:12:23
5                    /lists/subscriptions    15        15 2019-04-24 09:12:23
6                             /lists/show    75        75 2019-04-24 09:12:23
7                       /lists/ownerships    15        15 2019-04-24 09:12:23
8                      /lists/subscribers   180       180 2019-04-24 09:12:23
9                     /lists/members/show    15        15 2019-04-24 09:12:23
10                        /lists/statuses   900       900 2019-04-24 09:12:23
11         /application/rate_limit_status   180       178 2019-04-24 09:11:20
12                  /friendships/outgoing    15        15 2019-04-24 09:12:23
13                      /friendships/list   200       200 2019-04-24 09:12:23
14           /friendships/no_retweets/ids    15        15 2019-04-24 09:12:23
15                    /friendships/lookup    15        15 2019-04-24 09:12:23
16                  /friendships/incoming    15        15 2019-04-24 09:12:23
17                      /friendships/show   180       180 2019-04-24 09:12:23
18                           /blocks/list    15        15 2019-04-24 09:12:23
19                            /blocks/ids    15        15 2019-04-24 09:12:23
20                    /geo/similar_places    15        15 2019-04-24 09:12:23
21                      /geo/id/:place_id    75        75 2019-04-24 09:12:23
22                   /geo/reverse_geocode    15        15 2019-04-24 09:12:23
23                            /geo/search    15        15 2019-04-24 09:12:23
24                     /users/report_spam    15        15 2019-04-24 09:12:23
25            /users/contributors/pending  2000      2000 2019-04-24 09:12:23
26                        /users/show/:id   900       900 2019-04-24 09:12:23
27                          /users/search   900       900 2019-04-24 09:12:23
28               /users/suggestions/:slug    15        15 2019-04-24 09:12:23
29            /users/contributees/pending   200       200 2019-04-24 09:12:23
30                    /users/derived_info    15        15 2019-04-24 09:12:23
31                  /users/profile_banner   180       180 2019-04-24 09:12:23
32       /users/suggestions/:slug/members    15        15 2019-04-24 09:12:23
33                          /users/lookup   900       900 2019-04-24 09:12:23
34                     /users/suggestions    15        15 2019-04-24 09:12:23
35                         /followers/ids    15        15 2019-04-24 09:12:23
36                        /followers/list    15        15 2019-04-24 09:12:23
37               /statuses/retweeters/ids    75        75 2019-04-24 09:12:23
38               /statuses/retweets_of_me    75        75 2019-04-24 09:12:23
39                /statuses/home_timeline    15        15 2019-04-24 09:12:23
40                     /statuses/show/:id   900       900 2019-04-24 09:12:23
41                /statuses/user_timeline   900       900 2019-04-24 09:12:23
42                      /statuses/friends    15        15 2019-04-24 09:12:23
43                 /statuses/retweets/:id    75        75 2019-04-24 09:12:23
44            /statuses/mentions_timeline    75        75 2019-04-24 09:12:23
45                       /statuses/oembed   180       180 2019-04-24 09:12:23
46                       /statuses/lookup   900       900 2019-04-24 09:12:23
47                              /help/tos    15        15 2019-04-24 09:12:23
48                    /help/configuration    15        15 2019-04-24 09:12:23
49                          /help/privacy    15        15 2019-04-24 09:12:23
50                         /help/settings    15        15 2019-04-24 09:12:23
51                        /help/languages    15        15 2019-04-24 09:12:23
52                 /friends/following/ids    15        15 2019-04-24 09:12:23
53                /friends/following/list    15        15 2019-04-24 09:12:23
54                          /friends/list    15        15 2019-04-24 09:12:23
55                           /friends/ids    15        15 2019-04-24 09:12:23
56 /account/login_verification_enrollment    15        15 2019-04-24 09:12:23
57                /account/update_profile    15        15 2019-04-24 09:12:23
58            /account/verify_credentials    75        75 2019-04-24 09:12:23
59                      /account/settings    15        14 2019-04-24 09:10:49
60                        /favorites/list    75        75 2019-04-24 09:12:23
61            /saved_searches/destroy/:id    15        15 2019-04-24 09:12:23
62               /saved_searches/show/:id    15        15 2019-04-24 09:12:23
63                   /saved_searches/list    15        15 2019-04-24 09:12:23
64                         /search/tweets   180       180 2019-04-24 09:12:23
65                        /trends/closest    75        75 2019-04-24 09:12:23
66                      /trends/available    75        75 2019-04-24 09:12:23
67                          /trends/place    75        75 2019-04-24 09:12:23
```

Los recursos de agrupan por familias, los más interesantes para este ejemplo son _"users"_, _"followers"_ o _"friends"_.

```R
> resource_families
 [1] "account"        "application"    "blocks"         "direct_message" "favorites"
 [6] "followers"      "friends"        "friendships"    "geo"            "help"
[11] "lists"          "saved_searches" "search"         "statuses"       "trends"
[16] "users"
```

```R
getCurRateLimitInfo(c("users","followers","account"))
```

Por ejemplo para el recurso `/users/search` tenemos un límte de 900 llamadas, nos quedan pendientes aún 900 y cuando de resetea de nuevo.

## Obteniendo información de un usuario

Obtenemos información de un usuario y transformamos el objeto devuelto en un `data.frame`.

```R
usuario <- getUser('ILandajuela')
usuarioDF <- as.data.frame(usuario)
View(usuarioDF)
```

![](images/twitter-02.PNG)

![](images/twitter-03.PNG)

Comprobamos el ID de usuario:

```R
> usuarioDF$id
[1] "1120954979651657728"
> names(usuarioDF)
 [1] "description"       "statusesCount"     "followersCount"    "favoritesCount"
 [5] "friendsCount"      "url"               "name"              "created"
 [9] "protected"         "verified"          "screenName"        "location"
[13] "lang"              "id"                "listedCount"       "followRequestSent"
[17] "profileImageUrl"
> usuarioDF$screenName
[1] "ILandajuela"
```

Podemos referenciar el usuario de forma alternativa por su ID:

```R
usuarioDF <- as.data.frame(getUser(1120954979651657728))
```

## Buscando comentarios con un hashtag

Usaremos la función `searchTwitter` para buscar una cadena. Buscamos por ejemplos tweets relacionados con el hashtag de las próximas elecciones del 28 de Abril, buscamos sólo en idioma español.

```R
rdmTweets <- searchTwitter('#28A ', n=500,lang = 'es')
```

Podemos aplicar filtros como una fecha a partir de la cual queremos buscar (o una fecha límite), tweets originados en una geolocalización determinada, etc.

El resultado es una lista de objetos de tipo `twitteR::status`.

![](images/twitter-03.PNG)

No podemos convertir directamente la lista en un DF, para eso el paquete **twitteR** contiene una orden especial llamada `twListToDF()`.

```R
tweetsDF <- twListToDF(rdmTweets)
```

## Tendencias

La función `getTrends` nos permite interactuar son los tópicos más populares o _trending topics_, a la función se le debe proporcionar un [WOEID](https://en.wikipedia.org/wiki/WOEID)(Where On Earth IDentifier) para asociar la tendencia a una localización, se pueden buscar en esta [WOEID Lookup](http://woeid.rosselliot.co.nz/lookup/bilbao) introduciendo un código postal o una ciudad por ejemplo.

![](images/twitter-05.PNG)

Entre los primeros resultados como no está el fubtol tratándose de Bilbao.

![](images/twitter-06.PNG)

## Ejercicios

- r / intro / 41-twitter / [twitter.R](https://gitlab.com/soka/r/blob/master/intro/41-twitter/twitter.R).
- r / intro / 41-twitter / [twitteR.pdf](https://gitlab.com/soka/r/blob/master/intro/41-twitter/twitteR.pdf).

## Enlaces externos

- ["twitteR package | R Documentation"](https://www.rdocumentation.org/packages/twitteR/versions/1.1.9).
- ["Accessing Data from Twitter API using R – Michael Galarnyk – Medium"](https://medium.com/@GalarnykMichael/accessing-data-from-twitter-api-using-r-part1-b387a1c7d3e).
- ["Collecting Twitter Data Using R – Abigail Hipolito – Medium"](https://medium.com/@ahipolito94/collecting-twitter-data-using-r-cd6cd062dca4).
- ["Using the R twitteR package - Dave Tang's blog"](https://davetang.org/muse/2013/04/06/using-the-r_twitter-package/).
- ["Getting Started With Twitter Analysis in R | R-bloggers"](https://www.r-bloggers.com/getting-started-with-twitter-analysis-in-r/).
- ["Example: Social media analysis (Twitter)"](https://quanteda.io/articles/pkgdown/examples/twitter.html).
- ["A beginner's guide to collecting and mapping Twitter data using R ..."](https://opensource.com/article/17/6/collecting-and-mapping-twitter-data-using-r).
- ["Twitter data Mining 101 using R"](http://categitau.com/twitter-data-mining-101-using-r/).
- ["rtweet-Exploration"](https://github.com/mjhendrickson/rtweet-Exploration): Work in progress. Exploring rtweet functionality while learning R Markdown. Feedback on either topic is encouraged and appreciated.
