---
title: Ataque ARP Spoofing [borrador, en progreso]
subtitle: Kali Linux
date: 2022-03-15
draft: false
description: Kali Linux
tags: ["Seguridad Informática","LAN","ARP Spoofing","ARP Poisoning","ARPspoof","Kali","SEIF","ARP"]
---

<!-- vscode-markdown-toc -->
* 1. [Protocolo ARP](#ProtocoloARP)
	* 1.1. [Comandos de utilidad en Windows](#ComandosdeutilidadenWindows)
		* 1.1.1. [getmac](#getmac)
		* 1.1.2. [ipconfig](#ipconfig)
	* 1.2. [Comandos en Linux](#ComandosenLinux)
		* 1.2.1. [arping](#arping)
* 2. [ARPspoof con Kali Linux](#ARPspoofconKaliLinux)
	* 2.1. [Primeros pasos](#Primerospasos)
		* 2.1.1. [Captura de una sesión FTP](#CapturadeunasesinFTP)
		* 2.1.2. [Capturando una sesión Telnet](#CapturandounasesinTelnet)
	* 2.2. [Modo gráfico](#Modogrfico)
* 3. [Detección](#Deteccin)
* 4. [Prevención](#Prevencin)
* 5. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

La [**suplantación de ARP**](https://es.wikipedia.org/wiki/Suplantaci%C3%B3n_de_ARP)  (en inglés  _ARP spoofing_ o _ARP poisoning_) es uno de los tipos de ataque MITM (del inglés [_Man in the Middle_](https://es.wikipedia.org/wiki/Ataque_de_intermediario) consiste en interceptar la información intercambiada entre dos victimas sin su conocimiento) más conocidos. De forma resumida se trata de asociar la dirección MAC del atacante con la dirección IP de otro nodo, por ejemplo la puerta de enlace o _gateway_ predeterminada. Cualquier tráfico dirigido a la dirección IP de ese nodo, será erróneamente enviado al atacante, en lugar de a su destino real. El atacante, puede entonces elegir, entre reenviar  el tráfico a la puerta de enlace predeterminada real (ataque pasivo o escucha), o modificar los datos antes de reenviarlos (ataque activo).

##  1. <a name='ProtocoloARP'></a>Protocolo ARP

[**ARP**](https://es.wikipedia.org/wiki/Protocolo_de_resoluci%C3%B3n_de_direcciones) (_Address Resolution Protocol_ traducido como protocolo de resolución de direcciones) es un **protocolo** de capa 2 en el modelo OSI de comunicaciones, que básicamente **se encarga de resolver direcciones IP y MAC**.

Cuando se envía un paquete de un host a otro hay que indicar en su cabecera la dirección física (MAC), que es un identificador fijo y único asignado a cada tarjeta de red. Cuando una aplicación se quiere comunicar con otra a través de una red usará el protocolo IP para identificar la máquina de destino, pero teniendo en cuenta que las direcciones  IP pueden variar se hace imprescindible asociarlas a las direcciones físicas (MAC). Para ello, se utiliza el protocolo ARP, de modo que cuando un paquete llega a una máquina, esta comprueba que en la cabecera se indique su MAC y si no coincide con la suya, ignorará el paquete.

Para **ver la tabla ARP en Windows** introducimos el siguiente comando: `arp -a`.

```
C:\Users\ir_inf>arp -a

Interfaz: 192.168.1.134 --- 0x12
  Dirección de Internet          Dirección física      Tipo
  192.168.1.1           64-68-0c-6b-a3-67     dinámico
  192.168.1.128         50-46-5d-b5-2a-74     dinámico
  192.168.1.135         08-00-27-95-bd-54     dinámico
  192.168.1.255         ff-ff-ff-ff-ff-ff     estático
  224.0.0.22            01-00-5e-00-00-16     estático
  224.0.0.251           01-00-5e-00-00-fb     estático
  224.0.0.252           01-00-5e-00-00-fc     estático
  239.255.255.250       01-00-5e-7f-ff-fa     estático
  255.255.255.255       ff-ff-ff-ff-ff-ff     estático
```

**La tabla ARP que asocia la dirección IP y la MAC de la red se genera de forma dinámica** (al iniciar el PC, esa tabla se encuentra vacía), cuando queremos enviar un paquete a una IP que no se encuentra en dicha tabla nuestra máquina lanza una consulta a la red con un paquete especial dirigido a la MAC ff:ff:ff:ff:ff:ff con la IP especial 255.255.255.255 (broadcast). Cuando las máquinas de la red vean este paquete dirigido a esa dirección MAC especial, leerán el mensaje y únicamente la máquina que tenga la dirección IP por la que se pregunta responderá con otro paquete. Lo interesante, es que **todas las máquinas de la red recibirán ese paquete, lo leerán y actualizarán sus tablas de IP y MAC con la nueva información**, no solo la que hizo la pregunta.

Podemos borrar entradas de la tabla ARP con la opción -d seguida de la IP, por ejemplo cuando ejecutamos `arp -d 192.168.1.128` borra la entrada correspondiente, pasado un rato la entrada volverá a aparecer de forma dinámica, se puede comprobar con `arp -a`.

###  1.1. <a name='ComandosdeutilidadenWindows'></a>Comandos de utilidad en Windows

####  1.1.1. <a name='getmac'></a>getmac

[getmac](https://docs.microsoft.com/es-es/windows-server/administration/windows-commands/getmac) devuelve la dirección MAC para todas las interfaces de red de un equipo local o en red.

El uso más sencillo es ejecutarlo sin opciones para obtener las direcciones MAC de nuestro equipo:

```
C:\Windows\system32>getmac

Dirección física    Nombre de transporte
=================== ==========================================================
B8-6B-23-E7-9A-B6   \Device\Tcpip_{F922C016-DACB-4FBD-91B4-55F77253CA66}
34-E6-AD-41-37-91   Medios desconectados
0A-00-27-00-00-09   \Device\Tcpip_{80E2DC8E-4FB4-4989-A999-6F3DAFD281E8}
```

En mi equipo "B8-6B-23-E7-9A-B6" corresponde al adaptador de red local, "0A-00-27-00-00-09" es el adaptador de red de Virtual Box.

####  1.1.2. <a name='ipconfig'></a>ipconfig

Podemos obtener la información de la MAC local usando el comando `ipconfig /all` para obtener todos los detalles de la configuración de red de nuestro equipo local. He recortado la salida del comando para mostrar sólo la configuración del adaptador de red local:

```
Adaptador de Ethernet Ethernet:

   Sufijo DNS específico para la conexión. . :
   Descripción . . . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Dirección física. . . . . . . . . . . . . : B8-6B-23-E7-9A-B6
   DHCP habilitado . . . . . . . . . . . . . : sí
   Configuración automática habilitada . . . : sí
   Vínculo: dirección IPv6 local. . . : fe80::f0c5:b345:8387:431c%18(Preferido)
   Dirección IPv4. . . . . . . . . . . . . . : 192.168.1.134(Preferido)
   Máscara de subred . . . . . . . . . . . . : 255.255.255.0
   Concesión obtenida. . . . . . . . . . . . : martes, 22 de marzo de 2022 9:27:37
   La concesión expira . . . . . . . . . . . : viernes, 25 de marzo de 2022 9:27:32
   Puerta de enlace predeterminada . . . . . : 192.168.1.1
   Servidor DHCP . . . . . . . . . . . . . . : 192.168.1.1
   IAID DHCPv6 . . . . . . . . . . . . . . . : 163080995
   DUID de cliente DHCPv6. . . . . . . . . . : 00-01-00-01-28-C1-02-FD-B8-6B-23-E7-9A-B6
   Servidores DNS. . . . . . . . . . . . . . : 192.168.1.1
   NetBIOS sobre TCP/IP. . . . . . . . . . . : habilitado
```

###  1.2. <a name='ComandosenLinux'></a>Comandos en Linux

El comando arp se comporta de la misma forma que en Windows.

####  1.2.1. <a name='arping'></a>arping

El comando arping es parecido a un ping ICMP, permite descubrir equipos en red mandando paquetes ARP. El comando no viene instalado de serie por lo que debemos instalarlo primero. En distribuciones Debian/Linux (Kali también) usaremos el gestor de paquetes apt como sigue: `sudo apt install arping net-tools`.

Si conocemos la dirección IP de la máquina destino ahora podemos consultar su MAC, por ejemplo desde la máquina victima del ataque ARP Spoof consultamos la MAC del gateway del router doméstico:

```
$ arping 192.168.1.1

```


##  2. <a name='ARPspoofconKaliLinux'></a>ARPspoof con Kali Linux

Me he [descargado](https://www.kali.org/get-kali/#kali-virtual-machines) la distribución de Kali Linux de su [web oficial](https://www.kali.org/) preparada para Virtual Box (kali-linux-2022.1-virtualbox-amd64.ova), no me voy a detener en los pasos necesarios en VBox ya que no revisten ninguna complejidad, en [este manual](https://www.kali.org/docs/virtualization/install-virtualbox-guest-vm/) explican como crean las máquinas paso a paso (no es necesario si partimos de la imagen ova que yo he usado). El usuario y contraseña por defecto para autenticarnos es "kali".

Antes de comenzar actualizar el listado de repositorios de paquetes: `sudo apt update`.

**Nota**: Para la conexión de la VM **en lugar de NAT he usado "Adaptador puente" para que mi máquina virtual obtenga una IP por DHCP del router de casa**.

Equipos de mi red:

| Hostname | IP               | MAC               | Descripción                                      |
|----------|------------------|-------------------|--------------------------------------------------|
|          | 192.168.1.1      | 64:68:0C:6B:A3:67 | Router _gateway_ con DHCP habilitado             |
| diablo   | 192.168.1.128    | 50:46:5D:B5:2A:74 | PC Victima con Debian instalado                  |
| 008-HZ307498 | 192.168.1.134 |  B8:6B:23:E7:9A:B6 | Portátil Win10 con Kali Linux en VBox          |
| kali | 192.168.1.135 | 08:00:27:95:BD:54 | Kali Linux virtualizado |

###  2.1. <a name='Primerospasos'></a>Primeros pasos

Instala el paquete [dSniff](https://en.wikipedia.org/wiki/DSniff): `sudo apt install dsniff`.

Comprueba que tu máquina Kali puede hacer ping a otros _hosts_ de red y que accede a Internet.

Ahora ejecutamos el ataque con el siguiente comando:

```
sudo arpspoof -i eth0 -c both -t 192.168.1.128 192.168.1.1
```

Donde:

* [arpspoof](https://www.unix.com/man-page/debian/8/ARPSPOOF/): Es la herramienta o aplicación que usaremos (instalada con dSniff).
* -i (interface): usaremos el interface de red eth0.
* -c both.
* -t: Especificamos las máquinas de la red victimas del ataque. 192.168.1.128 es un PC de sobremesa en mi casa, 192.168.1.1 es el router que funciona como _gateway_ de salida a Internet.

Podemos acceder a toda la ayuda de arpspoof desde la línea de comandos con `man arpspoof`.

El ataque está en marcha:

![](img/06.png)

Vamos a comprobar en la máquina victima del ataque (192.168.1.128) a hacer un arpping al router (192.168.1.1) para comprobar la tabla ARP:

```
$ arping 192.168.1.1
ARPING 192.168.1.1
60 bytes from 64:68:0c:6b:a3:67 (192.168.1.1): index=0 time=421.314 usec
60 bytes from 64:68:0c:6b:a3:67 (192.168.1.1): index=1 time=464.129 usec
60 bytes from 08:00:27:95:bd:54 (192.168.1.1): index=2 time=936.169 msec
60 bytes from 64:68:0c:6b:a3:67 (192.168.1.1): index=3 time=440.231 usec
60 bytes from 64:68:0c:6b:a3:67 (192.168.1.1): index=4 time=476.024 usec
60 bytes from 08:00:27:95:bd:54 (192.168.1.1): index=5 time=944.191 msec
60 bytes from 64:68:0c:6b:a3:67 (192.168.1.1): index=6 time=713.553 usec
60 bytes from 64:68:0c:6b:a3:67 (192.168.1.1): index=7 time=455.721 usec
60 bytes from 08:00:27:95:bd:54 (192.168.1.1): index=8 time=981.577 msec
60 bytes from 64:68:0c:6b:a3:67 (192.168.1.1): index=9 time=429.738 usec
60 bytes from 64:68:0c:6b:a3:67 (192.168.1.1): index=10 time=455.201 usec
```

Es curioso, parece que Kali no ha suplantado de todo el router, la respuesta de arping intercala las MAC del router 
(64:68:0C:6B:A3:67) y la de Kali (08:00:27:95:BD:54) suplantándolo. La conexión a Internet de la victima se interrumpe de forma intermitente.

Consulto la tabla ARP en la máquina victima:

```
arp -a
? (192.168.1.1) at 08:00:27:95:bd:54 [ether] on enp2s0
? (192.168.1.134) at b8:6b:23:e7:9a:b6 [ether] on enp2s0
? (192.168.1.135) at 08:00:27:95:bd:54 [ether] on enp2s0
```

**La MAC de Kali a reemplazado la del router con éxito**.

Ahora podemos usar [Wireshark](https://www.wireshark.org/) en Kali para espiar el tráfico de red.

![](img/07.png)

Podemos filtrar sólo el tráfico de la victima aplicando un filtro por IP "ip.addr == 192.168.1.128". También podemos filtrar por protocolo, por ejemplo "arp".

####  2.1.1. <a name='CapturadeunasesinFTP'></a>Captura de una sesión FTP

Ahora voy a tratar de abrir una conexión FTP desde la victima y capturar el tráfico con WireShark desde Kali. Con WireShark abierto filtro el protocolo "ftp" exclusivamente.

En la máquina victima abro la conexión FTP a un servidor de prueba cualquiera: test.rebex.net (195.144.107.198) con usuario "demo" y contraseña "password". He escogido el servicio FTP ya que los datos se transmiten en texto plano sin cifrar, sin la conexión entre el cliente y el servidor FTP esta cifrada con un certificado SSL/TLS los datos no serán visibles a simple vista.

####  2.1.2. <a name='CapturandounasesinTelnet'></a>Capturando una sesión Telnet

[Telnet](https://es.wikipedia.org/wiki/Telnet) es un protocolo que permite abrir una terminal de comandos en una máquina remota. Al igual que el protocolo FTP los datos enviados no van cifrados así que es fácil interferir y capturar las comunicaciones, incluida la contraseña que podemos usar después con propósitos maliciosos.


###  2.2. <a name='Modogrfico'></a>Modo gráfico

Ejecutamos desde el menú "9. Sniffing & Spoofing" > "Ettercap-graphical" (la clave de _root_ es "kali" otra vez).

![](img/02.png)

Debería abrirse una ventana como esta:

![](img/03.png)

El siguiente paso es seleccionar los parámetros básicos de Ettercap, podemos dejarlo con los parámetros por defecto, es decir, que empiece a hacer sniffing al inicio, seleccionaremos la tarjeta de red que nosotros queramos, por defecto es eth0. El resto de opciones las dejamos como están, y pulsamos en el botón de la parte superior derecha para aceptar los cambios.

Una vez que hayamos iniciado el programa, tendremos que pinchar sobre la «lupa» que veis en la parte superior izquierda, lo que hará Ettercap es escanear toda la red local a la que estamos conectados en busca de los diferentes dispositivos que hay conectados, y por tanto, alguna víctima a la que atacar

Ahora deberían salirnos todos los hosts disponibles usando el botón "Hosts Lists" junto al botón "Scan for hosts" que hemos usado previamente.

![](img/04.png)

Nuestro ataque va dirigido contra un solo host, queremos suplantar la puerta de enlace para monitorizar las conexiones de la victima.

* **Target 1** – Seleccionamos la IP del dispositivo a monitorizar, en este caso, el dispositivo víctima (192.168.1.134, un PC casero dentro de la red), y pulsamos sobre dicho botón.
* **Target 2** – Pulsamos la IP que queremos suplantar, en este caso, la de la puerta de enlace (192.168.1.1).

Todo listo. Ahora solo debemos elegir el menú «MITM» de la parte superior y, en él, escoger la opción "ARP Poisoning".

![](img/05.png)

Nos aparecerá una pequeña ventana de configuración, en la cual debemos asegurarnos de marcar "Sniff Remote Connections". También debemos dejar desmarcada la opción de "only poison one-way", esta opción no realizará el ARP Poisoning en las dos direcciones sino solamente en una, por tanto, no tenemos comunicación bidireccional. Es muy importante dejar esta opción "only poison one-way" desmarcada, es decir, tal y como está por defecto.

Pulsamos sobre "Ok" y el ataque dará lugar. Ahora ya podemos tener el control sobre el host que hayamos establecido como "Target 1".

Ahora desde el menú de tres puntos en la esquina superior derecha podemos visualizar información interesante con la opción de menú "View", la imagen inferior corresponde a las conexiones que está realizando la victima "Target 1" (192.168.1.134), por ejemplo podemos ver que la máquina se está conectando a facebook.com (IP 31.13.83.36).

##  3. <a name='Deteccin'></a>Detección 



##  4. <a name='Prevencin'></a>Prevención

##  5. <a name='Enlacesexternos'></a>Enlaces externos

* [https://www.redeszone.net/tutoriales/seguridad/que-es-ataque-arp-poisoning/](https://www.redeszone.net/tutoriales/seguridad/que-es-ataque-arp-poisoning/).
* [https://www.welivesecurity.com/la-es/2014/02/11/como-funciona-arpspoof/](https://www.welivesecurity.com/la-es/2014/02/11/como-funciona-arpspoof/).
* [arpspoof(8) [debian man page]](https://www.unix.com/man-page/debian/8/ARPSPOOF/).

Otros:

* [arpspoof - A simple ARP spoofer for Windows](https://github.com/alandau/arpspoof).
