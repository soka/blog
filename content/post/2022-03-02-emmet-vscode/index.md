---
title: Deberías usar Emmet si trabajas en desarrollo web
subtitle: Reduce tus tiempos de desarrollo con Emmet
date: 2022-03-02
draft: false
description: Reduce tus tiempos de desarrollo con Emmet
tags: ["Emmet","Web","Desarrollo Web","Desarrollo","VSCode","Visual Studio Code","Diseño Web","Web Developer","Extensiones","Aplicaciones"]
---

<!-- vscode-markdown-toc -->
* 1. [Introducción](#Introduccin)
* 2. [Algunos ejemplos](#Algunosejemplos)
	* 2.1. [Número de elementos](#Nmerodeelementos)
	* 2.2. [Elementos hijo](#Elementoshijo)
	* 2.3. [Hermanxs](#Hermanxs)
	* 2.4. [Formularios](#Formularios)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Introduccin'></a>Introducción

[Emmet](https://emmet.io/) funciona como una extensión, está disponible para los editores de texto más populares. 

¿Para que vale? [Emmet](https://emmet.io/) permite crear _snippets_ de código y agilizar tus tiempos de desarrollo ahorrándote el tener que escribir una y otra vez estructuras habituales. [Emmet](https://emmet.io/)  Funciona con una gran variedad lenguajes de marcas, lo hace por supuesto con HTML, CSS, XML o SaSS entre otros.

Instalarlo en Visual Studio Code es trivial, usando el [gestor extensiones](https://marketplace.visualstudio.com/) se instala en un solo clic.

![](img/01.png)

Usando [Emmet](https://emmet.io/) el código se expande usando  [abreviaciones](https://docs.emmet.io/abbreviations/) inspiradas en los selectores CSS. Veamos nuestro primer ejemplo, con [Emmet](https://emmet.io/) instalado en VSCode creamos un fichero HTML para nuestras pruebas, a medida que escribimos nuestro código cuando [Emmet](https://emmet.io/) que coincide con una abreviación el editor nos sugerirá usarla.

Comenzamos a escribir "html.." e inmediatamente nos sugiere usar la abreviación `html:5` que introduce un esqueleto de una web básica: 

![](img/02.gif)

Podemos hacer lo mismo introduciendo el signo de exclamación `!`.

![](img/03.gif)

En la web oficial de [Emmet](https://emmet.io/) podemos encontrar un [_cheat sheet_ o chuleta de resumen](https://docs.emmet.io/cheat-sheet/) que podemos descargar para llevar con nosotros hasta que nos familiaricemos.

![](img/04.png)

##  2. <a name='Algunosejemplos'></a>Algunos ejemplos

###  2.1. <a name='Nmerodeelementos'></a>Número de elementos

`p*5` devuelve 5 veces `<p>`:

![](img/09.gif)

###  2.2. <a name='Elementoshijo'></a>Elementos hijo

Usando `>` para crear hijos y el signo de multiplicación `*` podemos crear una tabla o una lista por ejemplo.

![](img/05.gif)

![](img/06.gif)

###  2.3. <a name='Hermanxs'></a>Hermanxs

Usando el operador `+` (_sibling_) podemos crear varios elementos al mismo nivel:

![](img/07.gif)

###  2.4. <a name='Formularios'></a>Formularios

![](img/08.gif)

##  3. <a name='Enlacesexternos'></a>Enlaces externos

* [https://emmet.io/](https://emmet.io/). Página principal del proyecto.
* [https://docs.emmet.io/cheat-sheet/](https://docs.emmet.io/cheat-sheet/).
* [https://blog.pleets.org/article/emmet-plugin-para-html-y-css](https://blog.pleets.org/article/emmet-plugin-para-html-y-css).
* [https://www.adictosaltrabajo.com/2014/09/09/emett/](https://www.adictosaltrabajo.com/2014/09/09/emett/).
* [https://code.visualstudio.com/docs/editor/emmet](https://code.visualstudio.com/docs/editor/emmet).




