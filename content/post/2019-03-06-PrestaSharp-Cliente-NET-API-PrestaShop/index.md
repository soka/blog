---
title: Administrar la API de PrestaShop con PrestaSharp
subtitle: Librería .Net C# Para la API Web de PrestaShop
date: 2019-03-06
draft: true
description: API de PrestaShop
tags: ["PrestaShop","Webservice","CSharp","Servicios Web","API","HTTP","Visual Studio","C#",".NET","PrestaSharp"]
---
	


# Enlaces internos

* ["Usar servicios Web en PrestaShop v1.7.0.4"](https://soka.gitlab.io/blog/post/2019-03-04-prestashop-webservice/).
* ["Cliente acceso Webservice PrestaShop con C#"](https://soka.gitlab.io/blog/post/2019-03-05-prestashop-webservice-csharp-client/).

# Enlaces externos 

**Librerías y paquetes:**

* ["Bukimedia/PrestaSharp"](https://github.com/Bukimedia/PrestaSharp): CSharp .Net client library for the PrestaShop API via web service.

