---
title: Matrices en R
subtitle: Tipos de datos
date: 2019-03-11
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico", "R - Datos"]
---

![](images/r-logo.PNG)

Una matriz es una tabla bidimensional de valores ordenados en filas y columnas. La función `matrix` crea una matriz con los valores suministrados como argumentos. Vamos a intentar crear una matriz de 3x3 que contenga los valores de 1 a 9.

```R
> mx1 <- matrix(1:9,nrow = 3,ncol = 3)
> mx1
     [,1] [,2] [,3]
[1,]    1    4    7
[2,]    2    5    8
[3,]    3    6    9
```

![](images/matrices-cheatsheet.PNG)

Fijate que la matriz se llena a lo largo de las columnas. Podemos invertir ese patrón con el argumento `byrow=TRUE`.

Podemos comprobar las dimensiones de la matriz con `dim()`, el valor de retorno es un vector con el número de filas y columnas.

```R
> dim(mx1)
[1] 3 3
```

La función `summary()` opera en cada columna de la matriz como si fueran vectores:

Para añadir nuevas columnas usamos `cbind()` o para añadir filas `rbind()`.

```R
mx1 <- cbind(mx1,80:82) # Nueva columna
mx1 <- rbind(mx1,100:103) # Nueva fila
```

# Seleccionando elementos

Si quisiesemos seleccionar un elemento por fila y columna:

```R
> mx1[1,2]
[1] 4
```

O una fila entera (para una columna usamos el segundo índice):

```R
> mx1[1,]
[1] 1 4 7
```

Podemos seleccionar varias filas o columnas:

```R
mx1[c(1,2),]
```

O extraer los valores de forma condicional:

```R
mx2 <- mx1[mx1[,3]>7,]
```

# Operaciones

Como con los tipos básicos podemos realizar operaciones con matrices, por ejemplo multiplicar todos los elementos por dos:

```R
> mx1*2
     [,1] [,2] [,3]
[1,]    2    8   14
[2,]    4   10   16
[3,]    6   12   18
```

# Ejercicios

- r / intro / 20-Matrices / [matrices.R](https://gitlab.com/soka/r/blob/master/intro/20-Matrices/matrices.R).
