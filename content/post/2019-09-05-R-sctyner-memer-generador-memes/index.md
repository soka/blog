---
title: Generador de memes para R [en progreso]
subtitle: El lado divertido de R
date: 2019-09-05
draft: false  
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Paquetes", "Memes", "Humor"]
---

![](images/01.PNG)

**R** puede ser divertido, este post puede ser útil para mostrar la cara más "amable" de R durante un taller.

![](images/memer-logo.png)

[memer](https://github.com/sctyner/memer) es un paquete para crear memes de forma sencilla, [memer](https://github.com/sctyner/memer) depende especialmente de [magick](https://cran.r-project.org/web/packages/magick/index.html), paquete especializado en procesamiento de imágenes, que a su vez es una adaptación del software [ImageMagick](https://imagemagick.org/), sino conoces [ImageMagick](https://imagemagick.org/) merece la pena que le eches un vistazo, tal vez se convierta en una de tus herramientas imprescindibles.

## Instalando memer

Antes de empezar con ejemplos de [memer](https://github.com/sctyner/memer) quiero hacer un pequeño aparte para describir algunos problemas que he sufrido para instalar el paquete en Debian 10.

El comando de instalación es el siguiente:

```r
devtools::install_github("sctyner/memer")
```

Este comando no es el habitual usado hasta el momento, [memer](https://github.com/sctyner/memer) no está alojado en el repositorio oficial [CRAN](https://cran.r-project.org/) sino en [GitHub](https://github.com/), para usar la función [install_github](https://www.rdocumentation.org/packages/devtools/versions/1.13.6/topics/install_github) es necesario instalar el paquete [devtools](https://www.rdocumentation.org/packages/devtools/versions/1.13.6):

```r
install.packages("devtools")
```



## Código fuente

- r / paquetes / sctyner-memer / [memer.R](https://gitlab.com/soka/r/blob/master/paquetes/sctyner-memer/memer.R).


## Enlaces externos

- GitHub [sctyner/memer](https://github.com/sctyner/memer).