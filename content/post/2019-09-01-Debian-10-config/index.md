---
title: Debian 10 Buster aplicaciones imprescindibles 
subtitle: Aplicaciones que uso en el día a día como 
date: 2019-09-01
draft: true
description: Configurando mi computadora 
tags: ["Debian","GNU/Linux","Debian 10","Debian Buster"]
---

![](images/Debian_logo.png)

Este post es una guía personal 

# Visual Studio Code

[Visual Studio Code](https://code.visualstudio.com/) es mi editor predilecto durante este último año, lo uso para programar en Angular o para redactar este blog en Markdown entre otras muchas cosas, [instalarlo en Debian](https://code.visualstudio.com/docs/setup/linux) es sencillo siempre y cuando tengas **instalada la versión de 64 bit del sisema operativo**, simplemente [descarga](https://go.microsoft.com/fwlink/?LinkID=760868) el paquete y ejecuta el siguiente comando:

```
# apt install ./code_1.37.1-1565886362_amd64.deb.deb
``` 

# Git

```
# apt install git
```

https://git-scm.com/book/es/v1/Empezando-Configurando-Git-por-primera-vez


Launch VS Code Quick Open (Ctrl+P):

```
ext install Hirse.vscode-ungit
```

# NPM

https://tecadmin.net/install-latest-nodejs-npm-on-debian/


```
# apt-get install curl software-properties-common
# curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
# apt-get install nodejs
```

```
# node -v
v10.16.3
# npm -v
6.9.0
```


# Angular

```
# npm install -g @angular/cli

npm ERR! code EAI_AGAIN
npm ERR! errno EAI_AGAIN
npm ERR! request to https://registry.npmjs.org/@angular%2fcli failed, reason: getaddrinfo EAI_AGAIN registry.npmjs.org registry.npmjs.org:443

npm ERR! A complete log of this run can be found in:
npm ERR!     /home/popu/.npm/_logs/2019-08-31T07_27_08_092Z-debug.log
```

```
vi /etc/hosts
```

104.16.24.35    registry.npmjs.org


# R

```
# apt-key adv --keyserver keys.gnupg.net --recv-key 'E19F5F87128899B192B1A2C2AD5F960A256A04AF'
# add-apt-repository 'deb https://cran.rediris.es/  bin/linux/debian buster-cran35/'
# apt install r-base

https://www.digitalocean.com/community/tutorials/how-to-install-r-on-debian-10


# RStudio


https://www.rstudio.com/code-signing/
https://www.rstudio.com/products/rstudio/download/#download

```
# gpg --keyserver keys.gnupg.net --recv-keys 3F32EE77E331692F
# wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.2.1335-amd64.deb
# apt install dpkg-sig
# dpkg-sig --verify rstudio-1.2.1335-amd64.deb
# dpkg -i rstudio-1.2.1335-amd64.deb
```

Igual problemas con libclang-dev.

```
# apt install libclang-dev
# apt --fix-broken install
```

```
$ rstudio
``` 

Se produce fallo "qt.qpa.plugin: Could not load the Qt platform plugin "xcb""

```
# apt install libxkbcommon-x11-dev
``` 


















