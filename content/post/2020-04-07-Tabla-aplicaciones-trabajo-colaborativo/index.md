---
title: Herramientas de trabajo colaborativo [actualizado 23/04/2020]
subtitle: Aplicaciones mensajería, reuniones, videoconferencia, documentación...
date: 2020-04-07
draft: false
description: 
tags: ["Aplicaciones","Enlaces","Web","Tabla de contenidos","Trabajo","Colaborativo"]
---

## Videoconferencia

| Nombre        | Web                                          | Notas                                                        |
| ------------- |:--------------------------------------------:| ------------------------------------------------------------:|
| **JITSI Meet**    |  [https://meet.jit.si/](https://meet.jit.si/). Otros servidores [jitsi.eus](jitsi.eus) [vc.autistici.org](vc.autistici.org) [meet.mayfirst.org](meet.mayfirst.org) [meet.guifi.net](meet.guifi.net) [framatalk.org](framatalk.org) [calls.disroot.org](calls.disroot.org) [jitsi.riot.im](jitsi.riot.im) | **Aplicación Web** para videoconferencias en grupo. **No requiere instalación ni registro previo**. Sólo pinchar sobre el enlace y mandar la dirección a participantes. Disponible para smartphones. Por el momento mi preferida. |
| **BigBlueButton**  | [https://demo.bigbluebutton.org/](https://demo.bigbluebutton.org/) | Similar a la anterior pero requiere registro previo. Tiene muy buena pinta.   |
| **JumpChat** | [https://jumpch.at/c/](https://jumpch.at/c/)      |    **No requiere cuenta**, **basado en Web**. |
| **Talky** | [https://talky.io/](https://talky.io/) | **No requiere registro. Basado en Web.** |
| Jami | [https://jami.net/](https://jami.net/) | |


## Mensajería, chat y voz

| Nombre        | Web                                          | Notas                                                        |
| ------------- |:--------------------------------------------:| ------------------------------------------------------------:|
| **Telegram**  | [https://web.telegram.org/](https://web.telegram.org/) | Mensajería instantanea. Requiere registro y está asociado al móvil, se puede usar en navegador Web.
| **Riot.im** | [https://about.riot.im/](https://about.riot.im/) | Plataforma de mensajería. Permite llamadas de voz. Para navegadores y smartphones. Recomendado |
| **TeamSpeak** | | |
| **Mumble** | | |
| **RocketChat** | | |
| **Discord** | | |
| **Signal** | | |
| **Cryptodog** | [https://cryptodog.github.io/cryptodog/](https://cryptodog.github.io/cryptodog/) | Chat de mensajería web cifrado con sistemas clave pública-privada. [Extensión para Firefox](https://addons.mozilla.org/es/firefox/addon/cryptodog/) | 

## Compartir archivos

| Nombre        | Web                                          | Notas                                                        |
| ------------- |:--------------------------------------------:| ------------------------------------------------------------:|
| **Firefox Send**  | [https://send.firefox.com/](https://send.firefox.com/) | Ficheros hasta 2,5GB. Se pueden proteger con clave. Definir cuando expira enlace. | 
| **Framadrop**     | [https://framadrop.org/](https://framadrop.org/) | Permite subir archivos (cifradas en el servidor). Sin registro previo. Definir caducidad (máximo 60 días). Borrado en la primera descarga. |
| **Riseup** | [https://share.riseup.net/](https://share.riseup.net/) | |
| **Lufi** | [https://upload.disroot.org/](https://upload.disroot.org/) | |
| **Tori.eus** | [https://tori.eus/](https://tori.eus/)  | | 
| 

## Citas, reuniones y calendarios

| Nombre        | Web                                          | Notas                                                        |
| ------------- |:--------------------------------------------:| ------------------------------------------------------------:|
| **Framadate**     | [https://framadate.org/](https://framadate.org/) | Permite crear encuestas y elegir una fecha en equipo. Muy recomendable |  
| **Framagenda** | [https://framagenda.org](https://framagenda.org) | Servicio en línea de gestión de calendarios |


## Escritura colaborativa y documentación

| Nombre        | Web                                          | Notas                                                        |
| ------------- |:--------------------------------------------:| ------------------------------------------------------------:|
| **Paper**     | [https://paper.komun.org/](https://paper.komun.org/) | Escritura colaborativa Web sin registro previo. Sólo abrir un nuevo documento. [Más sitios que usan Etherpad](https://github.com/ether/etherpad-lite/wiki/Sites-that-run-Etherpad) |
| **CryptPad**  | [https://cryptpad.fr/](https://cryptpad.fr/) | Permite usuarios anónimos. Basada en Web. Permite crear diferentes tipos de trabajos (encuestas, documentos, ), subida de archivos. |
| **Disroot** | Pad [https://pad.disroot.org/](https://pad.disroot.org/), Hoja de cálculo [https://calc.disroot.org/](https://calc.disroot.org/) | |

## Almacenamiento

| Nombre        | Web                                          | Notas                                                        |
| ------------- |:--------------------------------------------:| ------------------------------------------------------------:|
| Nextcloud     | [https://nextcloud.com/providers/](https://nextcloud.com/providers/) | |


## Encuestas y formularios

| Nombre        | Web                                          | Notas                                                        |
| ------------- |:--------------------------------------------:| ------------------------------------------------------------:|
| **Framaforms** | [https://framaforms.org/](https://framaforms.org/) |  |
| **Framadate** | [https://poll.disroot.org/](https://poll.disroot.org/) | |
| **Disroot** | [https://disroot.org/en/services/polls](https://disroot.org/en/services/polls) | Basado en Framadate |
| **Typeform**  | | |

## Pizarras digitales colaborativas

| Nombre        | Web                                          | Notas                                                        |
| ------------- |:--------------------------------------------:| ------------------------------------------------------------:|
| **Drawchat**  | [https://draw.chat/](https://draw.chat/)     | Pizarra colaborativa, gratuita, sin registros y que además ofrece chat y videollamadas, prueba con Draw Chat. Esta app te permite hacer dibujo libre o sobre otras imágenes, hacer anotaciones en archivos PDF, y hasta crear indicaciones en un mapa. |
| **Witeboard** | [https://witeboard.com/](https://witeboard.com/) | Nada más sencillo para comenzar a trabajar, simplemente accede a este enlace https://witeboard.com/ y se genera una nueva pizarra con un identificador nuevo en la URL, para añadir nuestros colaboradores es tan fácil como compartir el enlace. Las opciones de edición y las herramientas que ofrece son muy básicas pero a veces no hace falta más. |
| **AWW - A Web Whiteboard** | [https://awwapp.com/](https://awwapp.com/) | AWW es muy parecida a la anterior, ofrece entre sus herramientas un lápiz para dibujar de forma libre aunque con algunas opciones adicionales de grosor y tipo de lápiz. Se comparte con otras personas usando un enlace que permite que el resto del equipo pueda editar y realizar aportaciones. |
| **Ziteboard** | [https://ziteboard.com/](https://ziteboard.com/) | Otra similar a las anteriores es Ziteboard pero creo que es con la que me quedo. |

## Otros

| Nombre        | Web                                          | Notas                                                        |
| ------------- |:--------------------------------------------:| ------------------------------------------------------------:|
| **Brush Ninja** | [https://brush.ninja/](https://brush.ninja/) | Brush Ninja es una app para crear animaciones dibujadas a mano, es extremadamente fácil de usar, es gratis, y te permite usar el ratón o un stylus para dibujar y añadir hasta 1000 cuadros por cada animación que puedes exportar en formato GIF |




## Enlaces internos

- [Pizarras digitales colaborativas](https://soka.gitlab.io/blog/post/2020-03-13-un-par-de-pizarras-digitales/).

## Referencias externas

- Librezale.eus [KomunikazioEtaElkarlanerakoTresnak](https://librezale.eus/wiki/KomunikazioEtaElkarlanerakoTresnak)
- Genbeta [15 geniales aplicaciones web completamente gratis que puedes usar en cualquier sistema operativo](https://www.genbeta.com/web/15-geniales-aplicaciones-web-completamente-gratis-que-puedes-usar-cualquier-sistema-operativo-1?utm_source=NEWSLETTER&utm_medium=DAILYNEWSLETTER&utm_content=POST&utm_campaign=13_Apr_2020+Genbeta&utm_term=CLICK+ON+TITLE). 
- [Graphite, una alternativa a Google Docs descentralizada, cifrada y basada en blockchain](https://www.genbeta.com/ofimatica/graphite-alternativa-a-google-docs-descentralizada-cifrada-basada-blockchain).
