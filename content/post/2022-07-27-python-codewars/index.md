---
title: Python Codewars
subtitle: Pon a prueba tus conocimientos de programación
date: 2022-07-27
draft: false
description: Pon a prueba tus conocimientos de programación
tags: ["Python","Codewars","Ejercicios"]
---

<!-- vscode-markdown-toc -->
* 1. [01](#)
	* 1.1. [Enunciado](#Enunciado)
	* 1.2. [Solución](#Solucin)
* 2. [02](#-1)
	* 2.1. [Enunciado](#Enunciado-1)
	* 2.2. [Solución](#Solucin-1)
* 3. [03](#-1)
	* 3.1. [Enunciado](#Enunciado-1)
	* 3.2. [Solución](#Solucin-1)
* 4. [04](#-1)
	* 4.1. [Enunciado](#Enunciado-1)
	* 4.2. [Solución](#Solucin-1)
* 5. [05](#-1)
	* 5.1. [Enunciado](#Enunciado-1)
	* 5.2. [Solución](#Solucin-1)
* 6. [06](#-1)
	* 6.1. [Enunciado](#Enunciado-1)
	* 6.2. [Solución](#Solucin-1)
* 7. [07](#-1)
	* 7.1. [Enunciado](#Enunciado-1)
	* 7.2. [Solución](#Solucin-1)
* 8. [08](#-1)
	* 8.1. [Enunciado](#Enunciado-1)
	* 8.2. [Solución](#Solucin-1)
* 9. [09](#-1)
	* 9.1. [Enunciado](#Enunciado-1)
	* 9.2. [Solución](#Solucin-1)
* 10. [10](#-1)
	* 10.1. [Enunciado](#Enunciado-1)
	* 10.2. [Solución](#Solucin-1)
* 11. [11](#-1)
	* 11.1. [Enunciado](#Enunciado-1)
	* 11.2. [Solución](#Solucin-1)
* 12. [12](#-1)
	* 12.1. [Enunciado](#Enunciado-1)
	* 12.2. [Solución](#Solucin-1)
* 13. [13](#-1)
	* 13.1. [Enunciado](#Enunciado-1)
	* 13.2. [Solución](#Solucin-1)
* 14. [14](#-1)
	* 14.1. [Enunciado](#Enunciado-1)
	* 14.2. [Solución](#Solucin-1)
* 15. [15](#-1)
	* 15.1. [Enunciado](#Enunciado-1)
	* 15.2. [Solución](#Solucin-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

**Instrucciones**.

**Sólo se puede usar un editor Python en línea para probar los ejercicios:**




##  1. <a name=''></a>01

###  1.1. <a name='Enunciado'></a>Enunciado

Timmy y Sarah creen que están enamorados, pero en el lugar donde viven, solo lo sabrán una vez que recojan una flor cada uno. Si una de las flores tiene un número par de pétalos y la otra tiene un número impar de pétalos, significa que están enamorados.

Escribe una función que tome el número de pétalos de cada flor y devuelva verdadero si están enamorados y falso si no lo están.

```python
def lovefunc( flower1, flower2 ):
    # ...
```

###  1.2. <a name='Solucin'></a>Solución

[src/ej_01.py](src/ej_01.py)

```python
def lovefunc( flower1, flower2 ):
    if (flower1%2==0 and flower2%2!=0) or (flower1%2!=0 and flower2%2==0): return True
    return False

print(lovefunc(2,3)) # True
print(lovefunc(2,2)) # False
print(lovefunc(5,6)) # True
``` 

Otras soluciones:

```python
def lovefunc(flower1, flower2):
    return flower1 % 2 != flower2 % 2
```

[https://www.codewars.com/kata/555086d53eac039a2a000083/solutions/python](https://www.codewars.com/kata/555086d53eac039a2a000083/solutions/python)

##  2. <a name='-1'></a>02

###  2.1. <a name='Enunciado-1'></a>Enunciado

¡Los trolls están atacando tu sección de comentarios!

Una forma común de lidiar con esta situación es eliminar todas las vocales de los comentarios de los trolls, neutralizando la amenaza.

Su tarea es escribir una función que tome una cadena y devuelva una nueva cadena con todas las vocales eliminadas.

Por ejemplo, la cadena "This website is for losers LOL!" se convertiría en "Ths wbst s fr lsrs LL!".

Nota: para este kata "y" no se considera una vocal.

```python
def disemvowel(string_):
    return string_
```	

###  2.2. <a name='Solucin-1'></a>Solución

[src/ej_02.py](src/ej_02.py)

```python
def disemvowel(string_):
    for i in "aeiouAEIOU":
        string_ = string_.replace(i,"")
    return string_

print(disemvowel("This website is for losers LOL!")) # Ths wbst s fr lsrs LL!
``` 

[https://www.codewars.com/kata/52fba66badcd10859f00097e/solutions/python](https://www.codewars.com/kata/52fba66badcd10859f00097e/solutions/python)

##  3. <a name='-1'></a>03

###  3.1. <a name='Enunciado-1'></a>Enunciado

Escribe una función que tome una matriz de números y devuelva la suma de los números. Los números pueden ser negativos o no enteros. Si la matriz no contiene ningún número, debe devolver 0.

**Ejemplos**

``` 
Input: [1, 5.2, 4, 0, -1]
Output: 9.2

Input: []
Output: 0

Input: [-2.398]
Output: -2.398
``` 

**Suposiciones**

* Puedes asumir que solo te dan números.
* No puede asumir el tamaño de la matriz.
* Puede suponer que obtiene una matriz y, si la matriz está vacía, devuelve 0.

```python
def sum_array(a):
``` 	

###  3.2. <a name='Solucin-1'></a>Solución 

[src/ej_03.py](src/ej_03.py)

```python
def sum_array(a):
    sum=0
    for i in a:
        sum+=i
    return sum        
```

Otras soluciones:

```python
def sum_array(a):
  return sum(a)
```

```python
def sum_array(a):
    return sum(a) if a else 0
```

```python
sum_array = lambda a: sum(a)
```

[https://www.codewars.com/kata/53dc54212259ed3d4f00071c/solutions/python](https://www.codewars.com/kata/53dc54212259ed3d4f00071c/solutions/python)


##  4. <a name='-1'></a>04

###  4.1. <a name='Enunciado-1'></a>Enunciado

Su equipo está escribiendo un nuevo y elegante editor de texto y se le ha asignado la tarea de implementar la numeración de líneas.

Escriba una función que tome una lista de cadenas y devuelva cada línea precedida por el número correcto.

La numeración comienza en 1. El formato es "n: cadena". Observe los dos puntos y el espacio en el medio.

Ejemplos: (Entrada --> Salida)

```python
[] --> []
["a", "b", "c"] --> ["1: a", "2: b", "3: c"]
```

```python
def number(lines):
    #your code here
```	

###  4.2. <a name='Solucin-1'></a>Solución

[src/ej_04.py](src/ej_04.py)

```python
def number(lines):
    new_lst=[]
    cnt=1
    for item in lines:
        new_lst.append(str(cnt)+': '+item)
        cnt+=1
    return new_lst
            
print(number(["a", "b", "c"]))  # ['1: a', '2: b', '3: c']
``` 

[https://www.codewars.com/kata/54bf85e3d5b56c7a05000cf9/solutions/python](https://www.codewars.com/kata/54bf85e3d5b56c7a05000cf9/solutions/python)


##  5. <a name='-1'></a>05 

###  5.1. <a name='Enunciado-1'></a>Enunciado

Piedra Papel tijeras

¡Vamos a jugar! ¡Tienes que devolver qué jugador ganó! En caso de empate devolver "Empate!".

Ejemplos (Entrada1, Entrada2 --> Salida):

```
"scissors", "paper" --> "Player 1 won!"
"scissors", "rock" --> "Player 2 won!"
"paper", "paper" --> "Draw!"
```

```python
def rps(p1, p2):
    pass
```    

###  5.2. <a name='Solucin-1'></a>Solución 

[src/ej_05.py](src/ej_05.py)

```python
def rps(p1, p2):
    if p1 == p2: return "Draw!"
    if (p1 == "scissors" and p2 == "paper") or (p1 == "paper" and p2 == "rock") or (p1 == "rock" and p2 == "scissors"): return "Player 1 won!"
    
    if (p1 == "scissors" and p2 == "rock") or (p1 == "paper" and p2 == "scissors") or (p1 == "rock" and p2 == "paper"): return "Player 2 won!"    
    
print(rps("scissors","paper")) #Player 1 won!
```

**Otras soluciones**:

```python
def rps(p1, p2):
    if p1 == p2:
        return 'Draw!'
    elif (p1 == 'rock' and p2 == 'scissors') or (p1 == 'scissors' and p2 == 'paper') or (p1 == 'paper' and p2 == 'rock'):
        return 'Player 1 won!'
    else:
        return 'Player 2 won!'
        
```  

[https://www.codewars.com/kata/5672a98bdbdd995fad00000f/solutions/python](https://www.codewars.com/kata/5672a98bdbdd995fad00000f/solutions/python)

##  6. <a name='-1'></a>06 

###  6.1. <a name='Enunciado-1'></a>Enunciado 

Crea una clase llamada `Cuenta` que tendrá los siguientes atributos: `titular` (que es una persona) y `cantidad` (puede tener decimales). El `titular` será obligatorio y la `cantidad` es opcional. Construye los siguientes métodos para la clase:

* Un constructor, donde los argumentos con los atributos arriba descritos pueden estar vacíos.
* Los setters y getters (decoradores con '@') para cada uno de los atributos. El atributo `cantidad` (privado) no se puede modificar directamente, sólo ingresando o retirando dinero.
* `mostrar()`: Muestra los datos de la cuenta.
* `ingresar(cantidad)`: se ingresa una cantidad a la cuenta, si la cantidad introducida es negativa, no se hará nada.
* `retirar(cantidad)`: se retira una cantidad a la cuenta. La cuenta puede estar en números rojos.

###  6.2. <a name='Solucin-1'></a>Solución 

[src/ej_06.py](src/ej_06.py)

```python
class Cuenta():
    
    def __init__(self,titular,cantidad=0):
        self.titular=titular
        self.__cantidad=cantidad
    
    @property
    def titular(self):
        return self.__titular
    
    @property
    def cantidad(self):
        return self.__cantidad

    @titular.setter
    def titular(self,titular):
        self.__titular=titular

       
    def mostrar(self):
        return "Cuenta\n"+"Titular:"+self.titular+" - Cantidad:"+str(self.cantidad)
    
    def ingresar(self,cantidad):
        if cantidad > 0:
            self.__cantidad = self.__cantidad + cantidad
    
    def retirar(self,cantidad):
        if cantidad > 0:
            self.__cantidad = self.__cantidad - cantidad


micuenta = Cuenta('iker',28.36)
micuenta.titular = micuenta.titular + ' landajuela'
print(micuenta.titular)
print(micuenta.mostrar())
micuenta.ingresar(10)
micuenta.retirar(20)
print(micuenta.mostrar())
```

##  7. <a name='-1'></a>07

###  7.1. <a name='Enunciado-1'></a>Enunciado

Escriba una función llamada `setAlarm` que reciba dos parámetros. El primer parámetro, `employed` (empleado), es verdadero siempre que esté empleado y el segundo parámetro, `vacation` (vacaciones), es verdadero siempre que esté de vacaciones.

La función debería devolver verdadero si está empleado y no está de vacaciones (porque estas son las circunstancias en las que necesita configurar una alarma). De lo contrario, debería devolver falso. Ejemplos:

```
setAlarm(true, true) -> false
setAlarm(false, true) -> false
setAlarm(false, false) -> false
setAlarm(true, false) -> true
```

```python
def set_alarm(employed, vacation):
    # Your code here
```   

###  7.2. <a name='Solucin-1'></a>Solución

[src/ej_07.py](src/ej_07.py)

```python
def set_alarm(employed, vacation):
    if employed==True and vacation==False:
        return True
    return False

print(set_alarm(True, True)) #False
print(set_alarm(False, True)) #False
print(set_alarm(False, False)) #False
print(set_alarm(True, False)) #True
```

Otras soluciones:

```python
def set_alarm(employed, vacation):
    return employed and not vacation
```     

```python
def set_alarm(employed, vacation):
    return employed==True and vacation==False
```

```python
def set_alarm(employed, vacation):
    # Your code here
    if employed:
        if vacation:
            return False
        return True
    return False
```

[https://www.codewars.com/kata/568dcc3c7f12767a62000038/solutions/python](https://www.codewars.com/kata/568dcc3c7f12767a62000038/solutions/python)

##  8. <a name='-1'></a>08

###  8.1. <a name='Enunciado-1'></a>Enunciado

A Nathan le encanta andar en bicicleta.

Como Nathan sabe que es importante mantenerse hidratado, bebe 0,5 litros de agua por hora de ciclismo.

Te dan el tiempo en horas y necesitas devolver el número de litros que beberá Nathan, redondeado al valor más pequeño.

Por ejemplo:

```
time = 3 ----> litres = 1
time = 6.7---> litres = 3
time = 11.8--> litres = 5
```

```python
def litres(time):
    return 0
```    

###  8.2. <a name='Solucin-1'></a>Solución

[src/08.py](src/08.py)

```python
def litres(time):
    return (time * 0.5) // 1

print(litres(3))
print(litres(6.7))
print(litres(11.8))
``` 

Otras soluciones:

```python
def litres(time):
    return time // 2
```

```
def litres(time):
    return int(time*0.5)
```

##  9. <a name='-1'></a>09

###  9.1. <a name='Enunciado-1'></a>Enunciado 

Cuenta el número de divisores de un entero positivo n.

Las pruebas aleatorias van hasta n = 500000.

Ejemplos (entrada --> salida)

```python
4 --> 3 (1, 2, 4)
5 --> 2 (1, 5)
12 --> 6 (1, 2, 3, 4, 6, 12)
30 --> 8 (1, 2, 3, 5, 6, 10, 15, 30)
```

Tenga en cuenta que solo debe devolver un número, el recuento de divisores. Los números entre paréntesis se muestran solo para que usted vea qué números se cuentan en cada caso.

```python
def divisors(n):
    pass
```

###  9.2. <a name='Solucin-1'></a>Solución 

[src/ej_09.py](src/ej_09.py)

```python
def divisors(n):
    cnt=0
    for i in range(1,n+1):
        print(i)
        if n % i == 0: cnt+=1
    return cnt

print(divisors(4))
``` 

Otras soluciones:

```python
def divisors(n):
    return sum(n%i==0 for i in range(1,n+1))
```     

[https://www.codewars.com/kata/542c0f198e077084c0000c2e/solutions](https://www.codewars.com/kata/542c0f198e077084c0000c2e/solutions)

##  10. <a name='-1'></a>10

###  10.1. <a name='Enunciado-1'></a>Enunciado 

Cree una función que devuelva la suma de los dos números positivos más bajos dada una matriz de mínimo 4 enteros positivos. No se pasarán números flotantes ni enteros no positivos.

Por ejemplo, cuando se pasa una matriz como [19, 5, 42, 2, 77], la salida debería ser 7.

```python
def sum_two_smallest_numbers(numbers):
    #your code here
```    

###  10.2. <a name='Solucin-1'></a>Solución 

[src/ej_10.py](src/ej_10.py)

```python
def sum_two_smallest_numbers(numbers):
    return sum(sorted(numbers)[:2])
    
print(sum_two_smallest_numbers([19, 5, 42, 2, 77]))
``` 

[https://www.codewars.com/kata/558fc85d8fd1938afb000014/solutions](https://www.codewars.com/kata/558fc85d8fd1938afb000014/solutions)

##  11. <a name='-1'></a>11

###  11.1. <a name='Enunciado-1'></a>Enunciado

Hubo un examen en tu clase y lo pasaste. ¡Felicidades!
Pero eres una persona ambiciosa. Quiere saber si es mejor que el estudiante promedio de su clase.

Recibe una matriz con los puntajes de las pruebas de sus compañeros. ¡Ahora calcule el promedio y compare su puntaje!

¡Devuelve True si estás mejor, de lo contrario False!

Nota:

Sus puntos no están incluidos en la matriz de puntos de su clase. ¡Para calcular el punto promedio, puede agregar su punto a la matriz dada!

```python
def better_than_average(class_points, your_points):
    # Your code here
```     

###  11.2. <a name='Solucin-1'></a>Solución

[src/ej_11.py](src/ej_11.py)

```python
def better_than_average(class_points, your_points):
    class_average=0
    for class_student in class_points:
        class_average+=class_student
    class_average = class_average / len(class_points)
    if your_points > class_average: return True
    return False
    
    
print(better_than_average([1,7,8,9,2,6],7))  # True
```

Otras soluciones:

```python
def better_than_average(class_points, your_points):
    return your_points > sum(class_points) / len(class_points)
```    

[https://www.codewars.com/kata/5601409514fc93442500010b/solutions/python](https://www.codewars.com/kata/5601409514fc93442500010b/solutions/python)

##  12. <a name='-1'></a>12

###  12.1. <a name='Enunciado-1'></a>Enunciado

Crea una función “is_palindrome” que reciba una palabra como argumento y retorne verdadero si es palíndromo o falso si no lo es. Un palíndromo es aquella palabra que se lee igual en ambos sentidos (Ejemplo: “anna”).  **La comprobación y el retorno se debe realizar en una sola línea usando slicing**.

```python
def is_palindrome(phrase):
    pass
```

###  12.2. <a name='Solucin-1'></a>Solución 

[src/ej_12.py](src/ej_12.py)

```python
def is_palindrome(phrase):
    return phrase == phrase[::-1]

print(is_palindrome('anna')) # True
print(is_palindrome('arma')) # False
```

##  13. <a name='-1'></a>13

###  13.1. <a name='Enunciado-1'></a>Enunciado

Programa en Python para obtener el número de dígitos de un número pasado como argumento a la función.

```python
def count_digits(num):
   pass
```

###  13.2. <a name='Solucin-1'></a>Solución

[src/ej_13.py](src/ej_13.py)

```python
def count_digits(num):
    count=0
    i=num

    while(i>0):
        count+=1
        i=i//10
    
    return count

print(count_digits(123)) # 3
```

##  14. <a name='-1'></a>14

###  14.1. <a name='Enunciado-1'></a>Enunciado

Te van a dar una palabra. Su trabajo es devolver el carácter medio de la palabra. Si la longitud de la palabra es impar, devuelve el carácter del medio. Si la longitud de la palabra es par, devuelve los 2 caracteres del medio.  

Ejemplos:

```
get_middle("test") should return "es"
get_middle("testing") should return "t"
get_middle("middle") should return "dd"
```

```python
def get_middle(s):
    pass
``` 

###  14.2. <a name='Solucin-1'></a>Solución

[src/ej_14.py](src/ej_14.py)

```python
def get_middle(s):
    if len(s)%2==0:        
        return s[(len(s)//2) - 1]+s[len(s)//2]
    else:
        return s[len(s)//2]    

print(get_middle('test')) # es
print(get_middle('testing')) # t
print(get_middle('middle')) # dd
```

**Otras soluciones**:

```python
def get_middle(s):
    #your code here
    x = len(s)
    y = int(x/2)
    if x%2==0:
        return s[y-1:y+1]
    else:
        return s[y:y+1]
``` 

```python
def get_middle(s):
    i = (len(s) - 1) // 2
    return s[i:-i] or s
``` 

##  15. <a name='-1'></a>15

###  15.1. <a name='Enunciado-1'></a>Enunciado

Escriba una función para dividir una cadena y convertirla en una matriz de palabras.

Ejemplos (Entrada ==> Salida):

```
"Robin Singh" ==> ["Robin", "Singh"]
"I love arrays they are my favorite" ==> ["I", "love", "arrays", "they", "are", "my", "favorite"]
```

```python
def string_to_array(s):
    # your code here
```  

**Nota**: Si recibe una cadena vacía debe retornar una lista con un elemento que representa una cadena vacía.

###  15.2. <a name='Solucin-1'></a>Solución

[src/ej_15.py](src/ej_15.py)

```python
def string_to_array(s):
    return s.split(' ')

print(string_to_array("")) # ['']
print(string_to_array("Robin Singh")) # ['Robin', 'Singh']
``` 

