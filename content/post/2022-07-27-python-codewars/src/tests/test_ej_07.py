#!/usr/bin/env python3

import pytest

import ej_07

def test_ej_07():   
    assert ej_07.set_alarm(True, True) == False
    assert ej_07.set_alarm(False, True) == False
    assert ej_07.set_alarm(False, False) == False
    assert ej_07.set_alarm(True, False) == True
    
    
    