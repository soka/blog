#!/usr/bin/env python3

import pytest

import ej_03

def test_ej_03():    
    assert ej_03.sum_array([1,2,5]) == 8
    assert ej_03.sum_array([]) == 0
    assert ej_03.sum_array([5,-4]) == 1
    assert ej_03.sum_array([1.1]) == 1.1
    assert ej_03.sum_array([1.1,2]) == 3.1
    #assert ej_03.sum_array([2.2,-1]) == 0.1 # OJO 1.2000000000000002
    