#!/usr/bin/env python3

import pytest

import ej_08

def test_ej_08():   
    assert ej_08.litres(3) == 1.0
    assert ej_08.litres(6.7) == 3.0
    assert ej_08.litres(11.8) == 5.0
    
    
    