#!/usr/bin/env python3

import pytest

import ej_12

def test_ej_12():   
    assert ej_12.is_palindrome('anna') == True
    assert ej_12.is_palindrome('mikel') == False
    
    
    