#!/usr/bin/env python3

import pytest

import ej_01

def test_ej_01():    
    assert ej_01.lovefunc(2,3) == True    
    assert ej_01.lovefunc(2,2) == False
    assert ej_01.lovefunc(5,2) == True    
    assert ej_01.lovefunc(5,5) == False 