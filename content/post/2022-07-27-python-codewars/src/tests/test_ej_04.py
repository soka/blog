#!/usr/bin/env python3

import pytest

import ej_04

def test_ej_04():    
    assert ej_04.number(["a", "b", "c"]) == ['1: a', '2: b', '3: c'] 
    assert ej_04.number(["1", "1", "1"]) == ['1: 1', '2: 1', '3: 1'] 
    
    