#!/usr/bin/env python3

import pytest

import ej_09

def test_ej_09():   
    assert ej_09.divisors(4) == 3 
    assert ej_09.divisors(5) == 2 
    assert ej_09.divisors(12) == 6
    assert ej_09.divisors(30) == 8
    
    
    