#!/usr/bin/env python3

import pytest

import ej_02

def test_ej_02():    
    assert ej_02.disemvowel("This website is for losers LOL!") == "Ths wbst s fr lsrs LL!"
    assert ej_02.disemvowel("") == ""
    