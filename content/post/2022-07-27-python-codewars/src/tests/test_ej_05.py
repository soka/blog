#!/usr/bin/env python3

import pytest

import ej_05

def test_ej_05():    
    assert ej_05.rps("scissors","paper") == "Player 1 won!"
    assert ej_05.rps("paper","paper") == "Draw!"
    assert ej_05.rps("paper","rock") == "Player 1 won!"
    assert ej_05.rps("rock","paper") == "Player 2 won!"
    
    