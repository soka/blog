#!/usr/bin/env python3

import pytest

import ej_06

def test_ej_06():   
    micuenta = ej_06.Cuenta('iker',28.36)
    assert micuenta.titular == 'iker'
    micuenta.titular = micuenta.titular + ' landajuela'
    assert micuenta.titular == 'iker landajuela'
    assert micuenta.mostrar() == 'Cuenta Titular:iker landajuela - Cantidad:28.36'
    micuenta.ingresar(10)
    assert micuenta.mostrar() == 'Cuenta Titular:iker landajuela - Cantidad:38.36'
    micuenta.retirar(50)
    assert micuenta.mostrar() == 'Cuenta Titular:iker landajuela - Cantidad:-11.64'
    
    
    
    
    