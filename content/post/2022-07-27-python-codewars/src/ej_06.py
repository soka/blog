#!/usr/bin/env python3
class Cuenta():
    
    def __init__(self,titular,cantidad=0):
        self.titular=titular
        self.__cantidad=cantidad
    
    @property
    def titular(self):
        return self.__titular
    
    @property
    def cantidad(self):
        return self.__cantidad

    @titular.setter
    def titular(self,titular):
        self.__titular=titular

       
    def mostrar(self):
        return "Cuenta "+"Titular:"+self.titular+" - Cantidad:"+str(self.cantidad)
    
    def ingresar(self,cantidad):
        if cantidad > 0:
            self.__cantidad = self.__cantidad + cantidad
    
    def retirar(self,cantidad):
        if cantidad > 0:
            self.__cantidad = self.__cantidad - cantidad


micuenta = Cuenta('iker',28.36)
micuenta.titular = micuenta.titular + ' landajuela'
print(micuenta.titular)
print(micuenta.mostrar())
micuenta.ingresar(10)
micuenta.retirar(50)
print(micuenta.mostrar())


