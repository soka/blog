#!/usr/bin/env python3

def is_palindrome(phrase):
    return phrase == phrase[::-1]

#print(is_palindrome('anna')) # True
#print(is_palindrome('arma')) # False

