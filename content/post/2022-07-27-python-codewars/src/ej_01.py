#!/usr/bin/env python3

def lovefunc( flower1, flower2 ):
    if (flower1%2==0 and flower2%2!=0) or (flower1%2!=0 and flower2%2==0): return True
    return False

#print(lovefunc(2,3)) # True
#print(lovefunc(2,2)) # False
#print(lovefunc(5,6)) # True
        

