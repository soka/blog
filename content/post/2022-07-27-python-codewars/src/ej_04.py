#!/usr/bin/env python3

def number(lines):
    new_lst=[]
    cnt=1
    for item in lines:
        new_lst.append(str(cnt)+': '+item)
        cnt+=1
    return new_lst
            
print(number(["a", "b", "c"]))  # ['1: a', '2: b', '3: c']