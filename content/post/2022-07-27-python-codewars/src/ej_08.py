#!/usr/bin/env python3

def litres(time):
    return (time * 0.5) // 1

#print(litres(3)) # 1.0
#print(litres(6.7)) # 3.0
#print(litres(11.8)) # 5.0