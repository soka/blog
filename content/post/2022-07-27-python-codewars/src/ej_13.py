#!/usr/bin/env python3


def count_digits(num):
    count=0
    i=num

    while(i>0):
        count+=1
        i=i//10
    
    return count

#print(count_digits(123)) # 3

