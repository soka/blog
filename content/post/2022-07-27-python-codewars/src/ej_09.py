#!/usr/bin/env python3

def divisors(n):
    cnt=0
    for i in range(1,n+1):
        #print(i)
        if n % i == 0: cnt+=1
    return cnt

#print(divisors(4)) # 3
