#!/usr/bin/env python3

def string_to_array(s):
    return s.split(' ')

print(string_to_array("")) # ['']
print(string_to_array("Robin Singh")) # ['Robin', 'Singh']




