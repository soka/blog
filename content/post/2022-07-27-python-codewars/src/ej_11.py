#!/usr/bin/env python3

def better_than_average(class_points, your_points):
    class_average=0
    for class_student in class_points:
        class_average+=class_student
    class_average = class_average / len(class_points)
    if your_points > class_average: return True
    return False
       
#print(better_than_average([1,7,8,9,2,6],7))  # True

