---
title: Marvel developer API
subtitle: Consulta desde R tus datos preferidos del universo Marvel Comics
date: 2019-04-27
draft: false
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "R - digest",
    "R - httr",
    "R - jsonlite",
    "Marvel",
    "API",
    "Programación",
  ]
---

![](images/marvel-dev-portal-web.PNG)

Hoy me he acordado de un taller de [React Native](https://facebook.github.io/react-native/) donde usábamos datos de [Marvel](https://developer.marvel.com/) para una aplicación de ejemplo. ¿Porqué no tratar de hacerlo en R?

Antes comenzar y si vas a poner en practica el siguiente ejemplo básico es necesario darse de alta en el portal Web de desarrolladores en [developer.marvel.com](https://developer.marvel.com/), una vez dados de alta nos proporciona una clave pública / privada para interactuar con la API.

![](images/marvel-dev-portal-web-01.PNG)

Las claves las almaceno en un fichero independiente llamado "marvel-api-key.R", no lo encontrareis en mi repositorio ya que por seguridad lo mantengo de forma local sin sincronizar en GitLab.

El fichero debería tener este aspecto:

```R
marvel_public_key <- "31ed3b3edf50a53cgd00fbb28dc76e12"
marvel_private_key <- "ebfd3ab310728993c32267d629ac9add2a36e451e"
```

Para cargar las claves desde el script principal:

```R
source('marvel-api-key.R')
```

## Paquetes necesarios de R

Necesitamos cargar los siguientes paquetes, el servicio es una API RESTful con los datos codificados en JSON. La URL base para las llamadas es `http(s)://gateway.marvel.com/`.

```R
library(jsonlite)
library(httr)
library(digest)
```

Se puede acceder a diferentes tipos de recursos, para acceder a los datos de un personaje nos centraremos en _Characters_.

## Acceso a la API

Para acceder a la API debemos definir dos parámetros adicionales además de las claves pública / privada:

- `ts`: Una marca de tiempo (_timestamp_).
- `hash`: Una suma de comprobación [MD5](https://es.wikipedia.org/wiki/MD5) de `ts`, la clave privada y pública (ejemplo: `md5(ts+privateKey+publicKey)`).

Por ejemplo, con una clave pública "1234" y privada "abcd" se construie una llamada como esta: "http://gateway.marvel.com/v1/public/comics?ts=1&apikey=1234&hash=ffd275c5130566a2916217b101f26150" (el valor hash MD5 es 1abcd1234).

Vamos a crear una función llamada `marvel_hash_params` para crear los parámetros de arriba.

```R
marvel_hash_params <- function() {

  ts <- round(as.numeric(Sys.time())*1000) # can totally be just Sys.time(), too
  to_hash <- sprintf("%s%s%s",
                     ts,
                     marvel_private_key,
                     marvel_public_key)

  list(
    ts=ts,
    hash=digest::digest(to_hash, "md5", FALSE),
    apikey=marvel_public_key
  )
}
```

[`Sys.time`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/Sys.time.html) retorna la fecha-hora actual como un tipo ""POSIXct" "POSIXt"".

```R
> Sys.time()
[1] "2019-04-27 11:55:48 CEST"
```

Convertimos el objeto a un número con `as.numeric` (el resultado es algo así como "1556358977" por ejemplo).

A continuación creamos una cadena concatenando la marca de tiempo la clave privada y la pública.

La función retorna una lista pasando previamente la cadena por MD5.

## Consultar datos de un personaje

La API sólo acepta consultas usando el método HTTP GET (el equivalente a un SELECT de SQL). Vamos a crear una función `get_characters` que pasando como parámetro de entrada el nombre del personaje retorne sus datos.

Dentro de la función se llama en primer lugar a `marvel_hash_params` para obtener la cadena hash, el `timestamp` y la clave pública que se pasarán como parámetros a la URL de la llamada.

```R
get_characters <- function(name) {

  params <- marvel_hash_params()
  params$name <- name

  res <- httr::GET("https://gateway.marvel.com:443/v1/public/characters",
                   query=params)

  httr::stop_for_status(res)

  httr::content(res, as="parsed")
}
```

## Ejecución

Ahora ya podemos llamar a la función `get_characters`:

```R
spider_man <- get_characters("spider-man")
```

La función retorna una lista con todos los datos de nuestro personaje.

![](images/marvel-dev-portal-web-03.PNG)

## Códigos HTTP errores autorización

![](images/marvel-dev-portal-web-02.PNG)

## Ejercicios

- r / intro / 43-marvel-dev-api / [marvel-dev-api.R](https://gitlab.com/soka/r/blob/master/intro/43-marvel-dev-api/marvel-dev-api.R).

## Enlaces externos

- developer.marvel.com ["AUTHORIZING AND SIGNING REQUESTS"](https://developer.marvel.com/documentation/authorization).
- developer.marvel.com ["GENERAL API INFORMATION"](https://developer.marvel.com/documentation/generalinfo).
- developer.marvel.com ["RESULT STRUCTURE"](https://developer.marvel.com/documentation/apiresults).
- [OAI/OpenAPI-Specification](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md).
