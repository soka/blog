---
title: Añadir una cuenta a un grupo de Active Directory con PowerShell
subtitle: Administración de grupos de directorio activo
date: 2019-03-04
draft: false
description: cmdlet Add-ADGroupMember
tags: ["PowerShell","PS","Scripting","Active Directory","Domain","Windows","MS"]
---

Hoy me ha tocado añadir una cuenta de usuario a un grupo de **Active Directory**, ni corto ni perezoso he decidido hacerlo con **PowerShell**.

# Acceso remoto con PS

Lo primero es establecer una sesión remota de **PowerShell** al servidor de dominio (algunos datos los he cambiado para no mostrar mi nombre de usuario o el nombre del servidor):

```
PS> enter-pssession -computername servidor-ad -credential Cuenta-Usuario
```

A continuación cargo el módulo de Active Directory en PS para acceder a las utilidades.


```
[servidor-ad]: PS C:\> import-module activedirectory
```


# Grupos en Active Directory

Se cual es el nombre del grupo, una vez dentro del servidor de dominio hago una búsqueda:

```
[servidor-ad]: PS C:\> Get-ADGroup -Filter {name -like "gsdirap*"} -Properties Description | Select Name,Description

Name                                                                                            Description
----                                                                                            -----------
gsDirAplicaciones
```

Ahora que se cual es el nombre del grupo ('gsDirAplicaciones') obtengo todas las cuentas AD que pertenecen al mismo:

```
[servidor-ad]: PS C:\> Get-ADGroupMember -identity 'gsDirAplicaciones' -recursive | select name
```

Compruebo que la cuenta que quiero dar acceso al grupo no está ya incluida. Ejecuto el comando `Add-ADGroupMember` para incluir la cuenta "b.sinclair" (ficticia para este ejemplo):

```
[servidor-ad]: PS C:\> Add-ADGroupMember -Identity gsDirAplicaciones -Members b.sinclair
```







