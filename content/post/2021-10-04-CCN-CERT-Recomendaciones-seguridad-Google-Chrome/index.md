---
title: Recomendaciones de seguridad para Google Chrome
subtitle: Informe buenas prácticas CCN-CERT Mayo 2021
date: 2021-10-04
draft: true
description: Informe buenas prácticas CCN-CERT Mayo 2021
tags: ["Google","CCN-CERT","Chrome","Navegadores","Ciberseguridad","Seguridad","Buenas prácticas","Sysadmin","Google"]
---

<!-- vscode-markdown-toc -->
* 1. [Ajustes de configuración](#Ajustesdeconfiguracin)
	* 1.1. [Sección Google y tú](#SeccinGoogleyt)
		* 1.1.1. [Sincronización y servicios de Google](#SincronizacinyserviciosdeGoogle)
	* 1.2. [Sección autocompletar](#Seccinautocompletar)
* 2. [Conclusiones](#Conclusiones)
* 3. [Enlaces internos](#Enlacesinternos)
* 4. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

![](img/01.PNG)

Esta entrada no es más que un resumen punto por punto de la guía [CCN-CERT_BP-19_Google_Chrome.pdf](doc/CCN-CERT_BP-19_Google_Chrome.pdf) [publicada por el CCN-CERT](https://www.ccn-cert.cni.es/seguridad-al-dia/novedades-ccn-cert/10982-recomendaciones-de-seguridad-en-google-chrome.html) con sugerencias para **incrementar la seguridad de los navegadores Google Chrome**, el paquete además del documento incluye un archivo de configuración "Preferences" de Chrome para aplicar estas medidas.

Introduciendo [chrome://settings/](chrome://settings/) en la dirección URL del navegador accedemos a los ajustes de Chrome donde haremos todos los cambios, podemos hacer lo mismo usando el menú de los tres puntos de Chrome.

##  1. <a name='Ajustesdeconfiguracin'></a>Ajustes de configuración

###  1.1. <a name='SeccinGoogleyt'></a>Sección Google y tú

####  1.1.1. <a name='SincronizacinyserviciosdeGoogle'></a>Sincronización y servicios de Google

![](img/03.PNG)

Desmarcar las opciones:

* ["Permitir el inicio de sesión en Chrome"](https://support.google.com/chrome/a/answer/7572556?hl=es): Si esta opción está desactivada, puedes iniciar sesión en sitios de Google, como Gmail, sin hacerlo en Chrome.
* ["Autocompletar búsquedas y URLs"](https://support.google.com/programmable-search/answer/4542657?hl=es): Envía algunas cookies y búsquedas desde la barra de direcciones y el cuadro de búsqueda a tu buscador predeterminado.

Estos cambios requieren el reinicio del navegador.

###  1.2. <a name='Seccinautocompletar'></a>Sección autocompletar


##  2. <a name='Conclusiones'></a>Conclusiones

**Es importante mantener Chrome actualizado así como el resto de aplicaciones instaladas en nuestro equipo**, en Chrome podemos consultar la versión introduciendo en la casilla de las direcciones Web `chrome://settings/help`.

![](img/02.PNG)


##  3. <a name='Enlacesinternos'></a>Enlaces internos

* [Publicaciones internas dentro de la categoría Google](https://soka.gitlab.io/blog/tags/google/) .

##  4. <a name='Enlacesexternos'></a>Enlaces externos

* [https://www.ccn-cert.cni.es/seguridad-al-dia/novedades-ccn-cert/10982-recomendaciones-de-seguridad-en-google-chrome.html](https://www.ccn-cert.cni.es/seguridad-al-dia/novedades-ccn-cert/10982-recomendaciones-de-seguridad-en-google-chrome.html).