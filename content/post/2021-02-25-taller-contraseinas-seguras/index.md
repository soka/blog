---
title: Taller contraseñas seguras
subtitle: Resumen de contenidos
date: 2021-02-25
draft: false
description: Consejos para proteger mejor nuestra cuenta
tags: ["Seguridad","Contraseñas","Ciberseguridad","Taller","Contenidos"]
---

<!-- vscode-markdown-toc -->
* 1. [INTRODUCCIÓN Y BREVE REFLEXIÓN](#INTRODUCCINYBREVEREFLEXIN)
* 2. [HECHOS CLAVE](#HECHOSCLAVE)
* 3. [CONTENIDOS](#CONTENIDOS)
* 4. [ENLACES INTERNOS](#ENLACESINTERNOS)
* 5. [ENLACES EXTERNOS](#ENLACESEXTERNOS)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='INTRODUCCINYBREVEREFLEXIN'></a>INTRODUCCIÓN Y BREVE REFLEXIÓN

La captura inferior me la mando un compañero después de impartir un taller similar a este, la noticia es reciente, en este caso olvidar una contraseña le puede costar caro, unos 200 millones de euros para ser más precisos.

![](img/01.png)

Seguramente no temas perder 200 millones, pero ¿y si pierdes o se ve comprometida toda la información de tu negocio?.

* ¿Alguna vez has olvidado alguna contraseña? Siendo sinceros ¿A quien no le ha pasado?
* ¿Sabes cuáles son las características que debe tener una contraseña para considerarse segura y robusta?
* ¿Sabes si tus contraseñas han sido expuestas y están en malas manos?
* ¿Utilizas la misma contraseña para todos los dispositivos, herramientas y servicios que manejas?
* ¿Conoces algún gestor de contraseñas? Que no sea una tabla Excel fea y difícil de mantener (la que se te olvida actualizar, donde no apuntaste aquel sitio donde creaste una cuenta la última vez,etc).
* ¿Sabes en qué consiste la autenticación de múltiples factores (MFA)?

##  2. <a name='HECHOSCLAVE'></a>HECHOS CLAVE

[**¿Sabías que el 90% de las contraseñas son vulnerables?**](https://www.osi.es/es/actualidad/blog/2019/02/06/sabias-que-el-90-de-las-contrasenas-son-vulnerables), bien sea por comodidad o por desconocimiento de lo fácil que puede ser vulnerar una contraseña poco robusta, a pesar de esto parece que no desaparecerán pronto.

La aparición de servicios en la nube hace que el [**usuario medio deba manejar y recordar una media de 22 contraseñas**](https://www.bbc.com/news/business-20726008), como no podía ser de otra manera esto entra en conflicto con nuestra capacidad limitada para recordarlas todas y hace que recurramos a prácticas poco seguras para facilitar su uso diario y para nuestra propia comodidad. Por otro lado la velocidad cada vez mayor de los ordenadores y las técnicas cada vez más sofisticadas hace que seamos hoy más vulnerables, cada día se registran nuevos ataques y brechas de seguridad, se producen violaciones de datos que deja expuestas miles de contraseñas que pueden afectar a nuestro negocio y que pueden suponer un impacto desastroso para nuestra empresa.

Las siguientes infografías son el resumen de varias encuestas realizadas por Gooogle y [Harris Poll](https://theharrispoll.com/), empresa especializada en análisis de mercado.

![](img/google_security_infographic-1.png)

**Sólo un 24% usan un gestor de contraseñas**, a pesar de que muchas personas dicen necesitar algún sistema mejor de registrar las contraseñas, **un 52% reutilizan la misma contraseña para muchas (no todas) de sus cuentas** (el 13% usan la misma contraseña en todas sus cuentas). 

![](img/PasswordCheckup-HarrisPoll-InfographicFINAL-1.png)

Otra [encuesta en EEUU](https://storage.googleapis.com/gweb-uniblog-publish-prod/documents/PasswordCheckup-HarrisPoll-InfographicFINAL.pdf) dice que el **75% de las personas que respondieron dijeron tener problemas para gestionar y mantener sus contraseñas**. La misma encuesta también dice que **un 24% de los encuestados han usado contraseñas inseguras** como "abc123", "Password", "Admin", etc. Además **un 59% incorporan un nombre (su nombre propio, el de la mascota, etc.) o la fecha de nacimiento a su contraseña**.

##  3. <a name='CONTENIDOS'></a>CONTENIDOS

Aprovechando el [test de evaluación](./res/kit-concienciacion/05_Test_Contraseinas.pdf) sobre contraseñas del kit de concienciación de INCIBE he preparado un [cuestionario en Quizizz](https://quizizz.com/admin/quiz/5f3a84fe9a522d001b7df239).

Estoy preparando una presentación muy resumida y simplificada usando [Reveal.JS](https://revealjs.com/), se puede descargar el primer borrador en [este enlace presentacion-20210226.zip](./presentacion-20210226.zip) comprimido en ZIP, sólo es necesario descomprimirlo y abrir en el navegador el archivo index.html.

* [La clave eres tú](https://soka.gitlab.io/blog/post/2021-01-15-la-clave-eres-tu/).
* [Manual LastPass](https://soka.gitlab.io/blog/post/2021-01-24-manual-lastpass/).
* [Alternativas a LastPass](https://soka.gitlab.io/blog/post/2021-02-20-alternativas-lastpass/).
* [Conoce a tu enemigo](https://soka.gitlab.io/blog/post/2021-02-23-conoce-a-tu-enemigo_tipos_ataque_contraseinas/).
* [Seguridad con cuentas de Google](https://soka.gitlab.io/blog/post/2021-02-21-seguridad-cuentas-google/).
* [Recursos y fuentes de información](https://soka.gitlab.io/blog/post/2021-03-04-recursos-taller-seguridad-contrase%C3%B1as/).

##  4. <a name='ENLACESINTERNOS'></a>ENLACES INTERNOS


##  5. <a name='ENLACESEXTERNOS'></a>ENLACES EXTERNOS

* ¿Sabías que el 90% de las contraseñas son vulnerables?. OSI (2019). [https://www.osi.es/es/actualidad/blog/2019/02/06/sabias-que-el-90-de-las-contrasenas-son-vulnerables](https://www.osi.es/es/actualidad/blog/2019/02/06/sabias-que-el-90-de-las-contrasenas-son-vulnerables).
* Internet users 'need 22 passwords' [https://www.bbc.com/news/business-20726008](https://www.bbc.com/news/business-20726008).
* Think your password will protect your data? Think again. Deloitte (2013). [https://www2.deloitte.com/ca/en/pages/press-releases/articles/think-your-password-will-protect-your-data-think-again.html](https://www2.deloitte.com/ca/en/pages/press-releases/articles/think-your-password-will-protect-your-data-think-again.html).
* [https://storage.googleapis.com/gweb-uniblog-publish-prod/documents/PasswordCheckup-HarrisPoll-InfographicFINAL.pdf](https://storage.googleapis.com/gweb-uniblog-publish-prod/documents/PasswordCheckup-HarrisPoll-InfographicFINAL.pdf).

**Otros**

* [https://safe.page/](https://safe.page/).
* [https://safe.page/quiz/](https://safe.page/quiz/). Test de preguntas sobre Webs seguras (HTTPS, etc). 