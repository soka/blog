---
title: Importar datos en R - JSON
subtitle: fromJSON() 
date: 2019-02-26
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico","JSON"]
---

Vamos a importar datos en formato JSON, ahora no vamos a entrar en detalles sobre su sintaxis. Me he [descargado](https://json.org/example.html) un fichero de ejemplo cualquiera al azar.

![](images/json-file.PNG)

Como siempre comenzamos instalando y cargando el paquete "rjson", el paquete tiene dos funciones `fromJSON` para leer el fichero y `toJSON` para realizar el proceso inverso.

```R 
install.packages("rjson")
library(rjson)
```

Ahora cargamos los datos del fichero JSON y los guardamos en una variable con la función `fromJSON()`, la función deserializa el fichero JSON en objetos R para facilitar su tratamiento posterior.

```R 
data.json <- fromJSON(file="datos.json")
```

El objeto devuelto es de tipo lista (colección de elementos que pueden ser de distintos tipos y que generalmente están identificados por un nombre), lo convertimos en un Data Frame (parecido a una matriz de 2 dimensiones con filas y columnas pero a diferencia de las matrices cada columna puede almacenar diferentes tipos de datos).

```R 
data.json <- data.frame(data.json,row.names = NULL)
```

Otro uso muy interesante es leer el JSON desde una URL directamente usando la función `readLines()` para leer el fichero y luego la función `paste()` para compactar todas las líneas en una única cadena de texto (exigencia de la función `fromJSON`).



# Ejercicios

* r / intro / 14-JSON / [json.R](https://gitlab.com/soka/r/blob/master/intro/14-JSON/json.R).
* r / intro / 14-JSON / [datos.json](https://gitlab.com/soka/r/blob/master/intro/14-JSON/datos.json).

# Enlaces

**Enlaces externos:**

* ["fromJSON function | R Documentation"](https://www.rdocumentation.org/packages/RJSONIO/versions/1.3-1.1/topics/fromJSON).
* ["data.frame function | R Documentation"](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/data.frame): The function data.frame() creates data frames, tightly coupled collections of variables which share many of the properties of matrices and of lists, used as the fundamental data structure by most of R's modeling software..