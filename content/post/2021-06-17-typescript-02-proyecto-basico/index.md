---
title: Proyecto básico con TypeScript
subtitle: TS interactúa con HTML 
date: 2021-06-17
draft: false
description: TypeScript
tags: ["TypeScript","TypeScript - Básico","TypeScript - tsconfig","Programación","TypeScript - DOM","TypeScript - Proyecto"]
---

<!-- vscode-markdown-toc -->
* 1. [ESQUELETO DEL PROYECTO](#ESQUELETODELPROYECTO)
* 2. [CÓDIGO TS](#CDIGOTS)
* 3. [CONFIGURACIÓN](#CONFIGURACIN)
* 4. [PÁGINA HTML](#PGINAHTML)
* 5. [EJECUCIÓN](#EJECUCIN)
* 6. [ENLACES EXTERNOS](#ENLACESEXTERNOS)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='ESQUELETODELPROYECTO'></a>ESQUELETO DEL PROYECTO

```ts
proyecto/
 L build/
   L index.html
 L src/
   L index.ts
 L tsconfig.json 
```

##  2. <a name='CDIGOTS'></a>CÓDIGO TS

```ts
const helloPlace = (place: string): string => {
    return 'Hello ' + place + '!';
} 

document.write(helloPlace('World'));
```

Las [función flecha](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Functions/Arrow_functions) `=>` a es una alternativa compacta a una expresión de `function` tradicional (con algunas limitaciones de uso).

Ejemplo:

```ts
// Función tradicional
function (a){
  return a + 100;
}

// Desglose de la función flecha

// 1. Elimina la palabra "function" y coloca la flecha entre el argumento y el corchete de apertura.
(a) => {
  return a + 100;
}

// 2. Quita los corchetes del cuerpo y la palabra "return" — el return está implícito.
(a) => a + 100;

// 3. Suprime los paréntesis de los argumentos
a => a + 100;
```

##  3. <a name='CONFIGURACIN'></a>CONFIGURACIÓN

```ts
{
    "compilerOptions": {
      "module": "es6",
      "target": "es5",
      "outDir": "./build/",
      "sourceMap": true
    },
    "include": [
      "src/**/*"
    ],
    "exclude": [
      "node_modules"
    ]
}
```

##  4. <a name='PGINAHTML'></a>PÁGINA HTML

```ts
<!DOCTYPE html>
<html lang="en">
<head>
  <title>TypeScript</title>
</head><body>
  <script src="./index.js"></script>
</body>
</html>
```

##  5. <a name='EJECUCIN'></a>EJECUCIÓN

Para compilar el proyecto usando el archivo `tsconfig.json` sólo debemos usar el comando `tsc` en el raíz del proyecto y abrir la página 'index.html' para ver el resultado (`firefox build/index.html`).

Una forma más cómoda es usar `tsc --watch` o `tsc -w` para monitorizar cualquier cambio en los ficheros y compilar el proyecto de forma automática.

## PACKAGE.JSON


```ts
{
    "name": "src",
    "version": "1.0.0",
    "description": "",
    "scripts": {
        "build": "tsc",
        "start": "tsc -w"
    }
}
```

```bash
npm run start
```

##  6. <a name='ENLACESEXTERNOS'></a>ENLACES EXTERNOS

* Artículo original fuente de este proyecto [https://itnext.io/a-typescript-startup-guide-for-the-impatient-43dc4063d1cb](https://itnext.io/a-typescript-startup-guide-for-the-impatient-43dc4063d1cb).