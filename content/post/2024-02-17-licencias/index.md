---
title: Breve introducción a las licencias software
subtitle: Licencias  
date: 2024-02-17
draft: false
description: Licencias
tags: ["Licencias"]
---

<!-- vscode-markdown-toc -->
* 1. [¿Qué es una licencia?](#Quesunalicencia)
* 2. [Los derechos de autor a lo largo de la historia](#Losderechosdeautoralolargodelahistoria)
	* 2.1. [En la antiguedad](#Enlaantiguedad)
	* 2.2. [La imprenta de Guttenberg](#LaimprentadeGuttenberg)
	* 2.3. [Estatuto de la Reina Ana](#EstatutodelaReinaAna)
	* 2.4. [Tratado de Berna](#TratadodeBerna)
	* 2.5. [Epoca contemporanea](#Epocacontemporanea)
	* 2.6. [Obras de dominio público](#Obrasdedominiopblico)
* 3. [Origen de las licencias software](#Origendelaslicenciassoftware)
* 4. [Origen del copyleft](#Origendelcopyleft)
* 5. [Otro mundo es posible](#Otromundoesposible)
	* 5.1. [¿El copyright no se adapta a Internet?](#ElcopyrightnoseadaptaaInternet)
		* 5.1.1. [Sistemas de compensación alternativos al copyright](#Sistemasdecompensacinalternativosalcopyright)
	* 5.2. [Libertad de conocimiento](#Libertaddeconocimiento)
	* 5.3. [¿La protección de derechos de autor realmente fomenta nuevas creaciones?](#Laproteccindederechosdeautorrealmentefomentanuevascreaciones)
	* 5.4. [Duración de los derechos de autor](#Duracindelosderechosdeautor)
* 6. [Modelos de negocio basados en software libre](#Modelosdenegociobasadosensoftwarelibre)
* 7. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Quesunalicencia'></a>¿Qué es una licencia?

Citando a la [RAE](https://dle.rae.es/licencia) una licencia es una **autorización o consentimiento (entre otros acepciones) para una determina actividad**, por ejemplo una licencia de conducir o para realizar una obra en casa. Más concretamente en el ámbito que nos interesa es:

> Autorización que se concede para explotar con fines industriales o comerciales una patente, marca o derecho.

En el ámbito del derecho es un **contrato** o acuerdo mediante el cual una persona recibe de otra una seria de derechos sobre un bien (derecho de uso, de copia, de distribución...).

La [**licencia**](https://es.wikipedia.org/wiki/Licencia_de_software) sin embargo se refiere a los **términos bajo los que se distribuye la obra**, es la autorización formal que otorga el autor de una obra a un usuario para poder utilizarla o explotarla de alguna manera, en términos precisos **el software no se vende, se licencia**, se adquieren una serie de derechos sobre el uso que se le puede dar.

##  2. <a name='Losderechosdeautoralolargodelahistoria'></a>Los derechos de autor a lo largo de la historia

###  2.1. <a name='Enlaantiguedad'></a>En la antiguedad

El origen histórico de las licencias se remonta a tiempos antiguos, cuando las sociedades comenzaron a desarrollar sistemas legales y de regulación para regular el comportamiento y proteger los derechos de las personas.

Los **derechos de autor** tal como los entendemos hoy en día, como un sistema legal para proteger la propiedad intelectual, son un **concepto relativamente moderno**. Sin embargo, a lo largo de la historia, han existido diferentes formas de protección de la propiedad intelectual en diversas culturas y sociedades. Por ejemplo, en la antigua Roma y Grecia, los autores tenían ciertos derechos sobre sus obras, aunque estos no se asemejaban necesariamente a los derechos de autor contemporáneos.  

**En la antigüedad, no existía un sistema formal de derechos de autor como lo entendemos en la actualidad**. Sin embargo, algunas civilizaciones tenían prácticas y normas que ofrecían cierta protección a los autores y creadores. Por ejemplo, en la antigua Roma y Grecia, los escritores y poetas tenían ciertos derechos sobre sus obras, como el reconocimiento de autoría y la capacidad de controlar la reproducción y distribución de sus escritos. Estos derechos no eran tan amplios ni estaban tan codificados como los derechos de autor modernos, pero representaban un intento inicial de proteger la propiedad intelectual.

En la antigua Roma, por ejemplo, los autores podían reclamar el derecho a ser reconocidos como los creadores de sus obras, lo que les otorgaba cierto prestigio y reconocimiento social. Además, tenían cierto control sobre la reproducción y distribución de sus textos mediante acuerdos informales con los copistas y editores. Por ejemplo, el poeta romano Virgilio pudo haber tenido algún tipo de control sobre la reproducción de sus obras, aunque no en el sentido legal que conocemos hoy.

En la antigua Grecia, se puede mencionar el caso de Homero, autor de la Ilíada y la Odisea. Aunque no se tiene certeza sobre su existencia histórica, se cree que Homero o los poetas que escribieron sus obras tenían cierta protección sobre la autoría y la transmisión oral de sus poemas épicos. La tradición oral de la época implicaba un respeto por la autoría de los poetas, aunque no existían leyes formales al respecto.

Estos son ejemplos de cómo en la antigüedad existían ciertas prácticas y normas que reconocían de alguna manera los derechos de los autores sobre sus obras, aunque no se parecían a los derechos de autor modernos en términos de legalidad y protección legal.

###  2.2. <a name='LaimprentadeGuttenberg'></a>La imprenta de Guttenberg

Anterior a la invención de la imprenta, en la edad media la distribución de obras literarias tenía muchas limitaciones debido a la dificultad de reproducir varios ejemplares a la vez, que permitiese una venta que generase ingresos considerables para los autores y editores. Hasta entonces los [copistas](https://es.wikipedia.org/wiki/Copista) o amanuenses reproducian los libros a mano, escribir una manuscrito podía ocuparle varios meses de trabajo, normalmente estas figuras eran monjes o frailes por lo que la iglesia también se otorgaba el papel de censor y determinaba que temas que se podían difundir.

![](img/01.jpeg)

<small>Copista medieval</small>

Hacia 1440 surgió la imprenta moderna de la mano de  Johannes Gutenberg, el impacto en la historia de la humanidad fue gigantesco, los libros podían copiarse con una rapidez nunca antes vista, permitio abaratar los costes de producción y que la difusión de conocimientos llegase a un público enorme, se produjo una **democratización de la cultura y la información** hasta entonces en manos de algunas elites (iglesia y monarquías aristocráticas principalmente).

![](img/02.jpeg)

<small>Imprenta europea del siglo XV</small>

Mientras que los monjes tenían el poder de controlar los escritos en toda Europa los **índices de alfabetización** eran ínfimos. Una vez el copiado de libros paso a ser realizado por las imprentas, estas se regían por los temas que más se solicitaban e imprimían por encargo. Una vez superada la censura previa, había libertad para imprimir libros de distintas temáticas y este círculo se fue abriendo con el paso de los años. Una vez la iglesia y las monarquías absolutas perdieron el poder de controlar absolutamente todo lo que se imprimía, **la difusión de ideas contrarias al feudalismo y a la religión establecida corrieron por toda Europa**. La imprenta se convirtió en un poderoso **mecanismo de igualación social, de nivelación de las desigualdades y de democratización de la sociedad**. 

**Con la imprenta pasamos a la identidad conocidísima del autor y del impresor** hasta entonces anónima.


###  2.3. <a name='EstatutodelaReinaAna'></a>Estatuto de la Reina Ana

![](img/03.jpeg)

El [«Estatuto de la Reina Ana»](https://es.wikipedia.org/wiki/Estatuto_de_la_Reina_Ana) promulgado en **1710** en Gran Bretaña, constituye un **antecedente histórico de gran importancia en el ámbito de los derechos de propiedad intelectual**. Fue la primera norma legal que reconoció lo que se conoce en el derecho anglosajón como **_copyright_ donde se le reconoce al autor su derecho de propiedad y por consiguiente los derechos que de ella derivan**.

Se establece un plazo de protección de 14 años, lapso prorrogable por otros 14 años si es que el autor aún se encontraba con vida, estas condiciones establecen las bases de los derechos de autor modernos como los conocemos hoy en día, tema del que hablaremos más adelante.

La imprenta y la producción en masa propició las [editoriales piratas](https://conversacionsobrehistoria.info/2021/03/05/los-editores-piratas-en-el-siglo-de-la-ilustracion/) (a menudo producidas como ediciones de "bolsillo" más baratas) por todo Europa y un tráfico inusitado de libros entre paises, quizás lo que sobresale de este panorama es el surgimiento del editor como participante en el campo cultural: un nuevo especialista a la hora de componer ediciones, que al mismo tiempo financia y pone al alcance del público. Además, libros que estaban prohibidos en Francia, que no sólo contenían obras filosóficas, sino escritos sediciosos, ataques a la Iglesia y hasta pornografía— se producían y enviaban clandestinamente.

![](img/11.jpeg)

<small>El Editor Pirata (1886) satiriza la situación existente en la que un editor podría beneficiarse simplemente robando obras recién publicadas de un país y publicándolas en otro, y viceversa.</small>

###  2.4. <a name='TratadodeBerna'></a>Tratado de Berna

El [Tratado de Berna (Suiza)](https://es.wikipedia.org/wiki/Convenio_de_Berna_para_la_Protecci%C3%B3n_de_las_Obras_Literarias_y_Art%C3%ADsticas)(1886), es un tratado internacional sobre la protección de los derechos de autor sobre obras literarias y artísticas, promovido por [Victor Hugo](https://es.wikipedia.org/wiki/Victor_Hugo), establece principios fundamentales de protección de derechos de autor, incluyendo el **derecho automático de protección sin necesidad de registro formal** de la obra y a cualquier obra derivada, la **duración mínima de protección** (generalmente la vida del autor más 50 o 70 años después de su muerte), y el reconocimiento mutuo de derechos entre los países miembros.

###  2.5. <a name='Epocacontemporanea'></a>Epoca contemporanea

A medida que la impresión y la publicación se volvieron más globales, se reconocieron los derechos de autor a nivel internacional. En la era contemporánea, las licencias se han convertido en una parte integral de la regulación gubernamental en una amplia gama de campos.

![](img/04.svg)

El [**copyright​**](https://es.wikipedia.org/wiki/Copyright) o [**derecho de autor​**](https://es.wikipedia.org/wiki/Derecho_de_autor) es un tipo de propiedad intelectual que le otorga a su dueño el derecho exclusivo de copiar, distribuir, adaptar, exhibir, y producir obras creativas, generalmente por un tiempo limitado.

El copyright otorga al creador del software, o a la entidad que posee los derechos de autor sobre el mismo, ciertos derechos exclusivos, como:

1. **Derecho de reproducción**: El derecho de hacer copias del software, ya sea en forma impresa o en formato digital.
2. **Derecho de distribución**: El derecho de distribuir copias del software al público, ya sea mediante la venta, alquiler u otros medios.
3. **Derecho de modificación**: El derecho de realizar modificaciones en el software, conocido como derecho de creación de obras derivadas.
4. **Derecho de exhibición y ejecución pública**: El derecho de mostrar o ejecutar el software públicamente, según sea el caso.
5. **Derecho de control sobre la ingeniería inversa**: El derecho de prohibir o regular la ingeniería inversa del software, según lo establecido por la ley.

Estos derechos permiten al titular del copyright controlar cómo se utiliza su software y protegerlo de la copia no autorizada o la distribución ilegal.

###  2.6. <a name='Obrasdedominiopblico'></a>Obras de dominio público

![](img/05.svg)

El [**dominio público**](https://es.wikipedia.org/wiki/Dominio_p%C3%BAblico) se refiere a un estado en el que las obras y recursos están libres de derechos de propiedad intelectual y están disponibles para su uso por parte del público en general sin restricciones. Esto significa que cualquiera puede usar, copiar, distribuir o modificar libremente los elementos que se encuentran en el dominio público, sin necesidad de obtener permiso o pagar regalías al creador original.

Las obras pueden ingresar al dominio público de varias maneras:

**Expiración del plazo de derechos de autor**: En muchos países, las obras están protegidas por derechos de autor por un período limitado de tiempo después de la muerte del autor. Una vez que este período expira, la obra pasa al dominio público y se vuelve de uso público.

En EEUU muchas obras pueden estar protegidas por la ley de derechos de autor 70 años después de la muerte del autor, o 120 años después de la creación o 95 años después de la publicación, ciertamente es un poco exagerado, casos como los derechos de Elvis Presley que han sido explotados después de su muerte por sus herederos y después comprados por una marca generan millones de beneficios anuales en [royalties](https://es.wikipedia.org/wiki/Regal%C3%ADa).

![](img/06.jpg)

El lobby de Disney consiguió gracias a la ley conocida como "Ley de Protección de Mickey Mouse" alargar sus derechos hasta 2024 (publicadas entre 1923 y 1977), por comentar Walt Disney murió en 1966, no se los derechos de quien protege si su creador está muerto o congelado.

**Renuncia de derechos de autor**: Un creador puede optar por renunciar a sus derechos de autor y colocar su obra directamente en el dominio público.

![](img/08.png)

<small>SQLite es un sistema de gestión de bases de datos relacional de dominio público1​</small>

**Falta de requisitos de protección**: Algunas obras nunca estuvieron sujetas a derechos de autor o no cumplían con los requisitos para la protección de derechos de autor en primer lugar, como obras que nunca fueron registradas o que carecían del aviso de derechos de autor requerido.

![](img/07.jpg)

Es muy anecdótico el caso de la mítica película de terror ["La noche de los muertos vivientes"](https://es.wikipedia.org/wiki/La_noche_de_los_muertos_vivientes) (1968), del director George A.Romero que pertenece al dominio público, no por deseo expreso del director o la productora ni porque hayan transcurrido unos años y sus derechos hayan caducado, un error permitió que se distribuyese sin incluir la nota del copyright en los títulos de crédito.

##  3. <a name='Origendelaslicenciassoftware'></a>Origen de las licencias software

Debemos situarnos a finales de la década de los sesenta, principios de los setenta.
En aquellos tiempos las grandes compañías de ordenadores no daban el valor que hoy día se da al software. **En su gran mayoría eran fabricantes de ordenadores que obtenían sus principales ingresos vendiendo sus grandes máquinas** (_mainframes_), a las que incorporaban algún tipo de sistema operativo y aplicaciones. Las universidades tenían permiso para coger y estudiar el código fuente del sistema operativo para fines docentes. Los mismos usuarios podían pedir el código fuente de drivers y programas para adaptarlos a sus necesidades. Se consideraba que el software no tenía valor por sí mismo si no estaba acompañado por el hardware que lo soportaba. En este entorno, los laboratorios Bell (AT&T) diseñaron un sistema operativo llamado **UNIX**, caracterizado por la buena gestión de los recursos del sistema, su estabilidad y su **compatibilidad con el hardware de diferentes fabricantes** (para homogeneizar todos sus sistemas). Este último hecho fue importantísimo (hasta entonces todos los fabricantes tenían sus propios operativos incompatibles con los otros), ya que devino el factor que le proporcionó mucha popularidad.

Poco a poco, las grandes empresas empezaron a tomar conciencia del valor del software: primero fue IBM la que en 1965 dejó de dar el código fuente de su sistema operativo, a finales de los setenta Digital Research empezó a vender el suyo, etc. Este hecho hizo que todas las compañías se dieran cuenta de que el software podía ser muy rentable y les podía aportar grandes beneficios. A partir de este hecho, la mayoría de empresas empezaron a poner reticencias a dejar el código fuente de sus programas y sistemas operativos y empezaron a vender sus programas como un valor añadido a su hardware. 

##  4. <a name='Origendelcopyleft'></a>Origen del copyleft

En este entorno cada vez más cerrado, Richard Stallman (que trabajaba en el MIT, Massachusetts Institute of Technology) se sintió indignado al comprobar que cada vez era más difícil conseguir el código fuente de los programas que utilizaba para adaptarlos a sus necesidades, tal como había hecho hasta entonces.

##  5. <a name='Otromundoesposible'></a>Otro mundo es posible

###  5.1. <a name='ElcopyrightnoseadaptaaInternet'></a>¿El copyright no se adapta a Internet?

Mientras los ciudadanos tienden cada vez más a rechazar y desobedecer las restricciones que les impone el copyright "por su propio bien", los gobiernos añaden aún más restricciones y, mediante nuevas y más severas sanciones, trata de atemorizar al público para que obedezca.

En la era de las tecnologías de la información donde compartir y copiar es una de sus bases han provocado el estado incierto de la idea misma de "robar". 

**Evitar que las personas puedan compartir va contra la naturaleza humana**, el ser humano posee un fuerte impulso hacia la cooperación, la propaganda Orwelliana que dice que "compartir es robar" normalmente cae en saco roto. ([Acabando con la Guerra Contra la Práctica de Compartir](https://stallman.org/articles/end-war-on-sharing.es.html) Richard Stallman).

####  5.1.1. <a name='Sistemasdecompensacinalternativosalcopyright'></a>Sistemas de compensación alternativos al copyright

Los modelos comerciales deberían adaptarse a la nueva realidad en lugar de criminizalizar tecnologías completas como P2P, un caso muy sonado es de  The Pirate Bay que ha sido perseguido por media europa por ofrecer enlaces BitTorrent.

* El grupo frances Association des audionautes propone un sistema de compensación alternativo a los artistas mediante un **recargo en las tarifas de los proveedores de servicios de Internet**. 
* **Sistemas de financiamiento colectivo** (Crowdfunding): Plataformas de crowdfunding como Kickstarter, Patreon o GoFundMe permiten a los creadores de contenido recibir financiamiento directo de sus seguidores y audiencia. Esto puede tomar la forma de donaciones únicas o pagos recurrentes en apoyo al trabajo creativo de los artistas.
* **Modelos de suscripción o membresía**: Algunos creadores ofrecen su contenido a través de modelos de suscripción o membresía, donde los usuarios pagan una tarifa regular para acceder a contenido exclusivo o privilegios adicionales.
* **Fondos públicos y subvenciones**: En algunos países, se utilizan fondos públicos para financiar proyectos creativos y culturales. 
* **Modelos basados en publicidad o patrocinio**: Algunos creadores obtienen ingresos a través de publicidad en sus plataformas en línea, ya sea a través de anuncios directos o de programas de afiliados.
* **Sistemas de remuneración equitativa o de remuneración de derechos**: Algunas propuestas buscan establecer sistemas de remuneración equitativa donde los usuarios pagan una tarifa o impuesto por el acceso a contenido cultural, y luego estos fondos se distribuyen entre los creadores en función de varios factores, como la popularidad o la cantidad de reproducciones.

###  5.2. <a name='Libertaddeconocimiento'></a>Libertad de conocimiento

Las sociedades avanzan en base a la cooperación, son un freno al progreso humano, los derechos de autor chocan de forma frontal contra la "libertad del conocimiento". El conocimiento debería compartirse con solidaridad como un derecho fundamental, especialmente en la educación.

###  5.3. <a name='Laproteccindederechosdeautorrealmentefomentanuevascreaciones'></a>¿La protección de derechos de autor realmente fomenta nuevas creaciones?

Uno los argumentos clásicos de favor del copyright es que al garantizar un monopolio transitorio sobre sus obras se promueve el desarrollo y la creatividad al brindarle al creador fuentes de ingresos, normalmente esto se traduce como algún tipo de [regalía](https://es.wikipedia.org/wiki/Regal%C3%ADa). Esto posibilitaría que el autor pudiese vivir de estos estipendios para crear más obras. El problema a menudo es que los cárteles intermediarios distribuidores son quienes se llevan la mayor parte.

###  5.4. <a name='Duracindelosderechosdeautor'></a>Duración de los derechos de autor

![](img/09.svg)

El Partido Pirata aboga por la eliminación de las patentes y entre las medidas concretas de su programa se encuentra la modificación de la actual ley del copyright europeo. **Proponen que una vez muerto el autor la obra pase a ser de dominio público al cabo de cinco años (y no 70 como sucede actualmente)**. No están a favor de las sanciones por las transacciones de ficheros y apoyan una infraestructura de la información abierta para compartir la cultura. Otra de sus principales preocupaciones es la protección de los datos personales de los ciudadanos.

![](img/10.jpeg)

<small>Manifestación en Suecia en apoyo del intercambio de archivos, 2006.</small>

Enlaces:

* [https://www.gnu.org/philosophy/misinterpreting-copyright.es.html](https://www.gnu.org/philosophy/misinterpreting-copyright.es.html)
* [https://academia-lab.com/enciclopedia/criticas-a-los-derechos-de-autor/](https://academia-lab.com/enciclopedia/criticas-a-los-derechos-de-autor/)
* [http://www.culturelink.org/news/members/2005/members2005-011.html](http://www.culturelink.org/news/members/2005/members2005-011.html)
* [https://questioncopyright.org/promise/](https://questioncopyright.org/promise/)


##  6. <a name='Modelosdenegociobasadosensoftwarelibre'></a>Modelos de negocio basados en software libre

> Los desarrolladores de software propietario cuentan con la ventaja que proporciona el dinero; los de software libre deben idear sus propias ventajas. Richard Stallman - Software libre para una sociedad libre.

Existe la creencia equivocada de que el software libre está reñido con los negocios y la obtención de beneficio, **la filosofía del software libre rechaza una práctica empresarial concreta y muy generalizada**, pero no está en contra del éxito siempre que la empresa respete las libertades fundamentales de sus usuarios.

La propia FSF obtiene la mayoría de sus beneficios distribuyendo copias de su software (también mediante donativos pero en menor medida), otras vías de ingresos pueden ser la formación o la adaptación de software a otros entornos o la personalización para el cliente.

Red Hat (1993) es un ejemplo de empresa de éxito cuyo producto está basado en software libre, ofrecen una distribución del sistema operativo Linux de pago y servicios adicionales como soporte, capacitación o consultoría para el sector empresarial, en paralelo para usuarios particulares han creado la distribución derivada Fedora. Red Hat mantiene y contribuye muchos proyectos de software libre. En 2018, IBM anunció su intención de adquirir Red Hat por 34.000 millones de dolares. Otras empresas como Red Hat mantienen una dualidad entra la liberación de su producto con versiones comunitarias y licencias comerciales.

Hoy en día existen otras empresas que basan su negocio en servicios relacionados con el software libre, por ejemplo ofreciendo soporte técnico y mantenimiento, consultoría o la implantación de aplicaciones empresariales basadas en software libre. A menudo adoptar un software libre empresarial nos libera de los costes de las licencias pero no quiere decir que sea gratis, una implantación eficaz y adecuada suele requerir un socio tecnológico que nos acompañe.

Muchas pequeñas y medianas empresas “satélite” viven de desarrollar funcionalidades extendidas y plugins para productos concretos como WordPress, Odoo, etc.

##  7. <a name='Enlacesexternos'></a>Enlaces externos

* [https://dle.rae.es/licencia](https://dle.rae.es/licencia).
* [https://es.wikipedia.org/wiki/Licencia](https://es.wikipedia.org/wiki/Licencia)
* [https://es.wikipedia.org/wiki/Estatuto_de_la_Reina_Ana](https://es.wikipedia.org/wiki/Estatuto_de_la_Reina_Ana)
* [https://colnal.mx/noticias/la-imprenta-revoluciono-nuestros-modos-de-entender-el-mundo-concepcion-company-company/](https://colnal.mx/noticias/la-imprenta-revoluciono-nuestros-modos-de-entender-el-mundo-concepcion-company-company/)
* [https://web.archive.org/web/20120703002402/http://www.historyofinformation.com/expanded.php?id=3389](https://web.archive.org/web/20120703002402/http://www.historyofinformation.com/expanded.php?id=3389)
* [Acabando con la Guerra Contra la Práctica de Compartir](https://stallman.org/articles/end-war-on-sharing.es.html) por Richard Stallman 2009-10-13.
* [Interpretación incorrecta del copyright: una serie de errores](https://www.gnu.org/philosophy/misinterpreting-copyright.es.html) por Richard Stallman.
* [Licencias de Software: Antecedentes](https://www.redalyc.org/pdf/4026/402640448007.pdf) [PDF].
* [Historia de los derechos de autor](https://es.wikipedia.org/wiki/Historia_de_los_derechos_de_autor).
* [The Future of Copyright](https://www.cato-unbound.org/2008/06/09/rasmus-fleischer/future-copyright/)
* [El copyright frente a la comunidad en la era de las redes informáticas](https://www.gnu.org/philosophy/copyright-versus-community.es.html)
* AcademiaLab [Críticas a los derechos de autor](https://academia-lab.com/enciclopedia/criticas-a-los-derechos-de-autor/)
* [The Surprising History of Copyright and The Promise of a Post-Copyright World](https://questioncopyright.org/promise/)
* Wikipedia [Anticopyright](https://es.wikipedia.org/wiki/Anticopyright)
* Wikipedia [Historia de los derechos de autor](https://es.wikipedia.org/wiki/Historia_de_los_derechos_de_autor)
* [https://conversacionsobrehistoria.info/2021/03/05/los-editores-piratas-en-el-siglo-de-la-ilustracion/](https://conversacionsobrehistoria.info/2021/03/05/los-editores-piratas-en-el-siglo-de-la-ilustracion/)
* [https://historia.nationalgeographic.com.es/a/gutenberg-inventor-que-cambio-mundo_11140](https://historia.nationalgeographic.com.es/a/gutenberg-inventor-que-cambio-mundo_11140)