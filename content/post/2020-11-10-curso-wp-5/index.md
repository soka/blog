---
title: Opciones avanzadas de publicación
subtitle: Sesión nº 2 curso WordPress
date: 2020-11-10
draft: false
description: Como publicar y gestionar nuestros posts
tags: ["Wordpress","WP - Curso","WP","WP - Entradas"]
---

![](img/wordpress-logo-01.png)

## Pad de trabajo

[https://pad.riseup.net/p/penascalf52021](https://pad.riseup.net/p/penascalf52021)

## Opciones avanzadas de las publicaciones

### Extractos

Los [extractos](https://wordpress.com/support/excerpts/) son ideales para crear descripciones de una o dos oraciones del contenido de una página o una entrada. Los distintos temas pueden utilizar esos extractos como una vista previa, en lugar de mostrar el contenido completo de la entrada o la página.

![](img/01.png)

Ejemplos típicos donde pueden aparecer los extractos:

- Resultados de búsquedas.
- Una página de resumen de publicaciones de un blog.

Se puede usar algo de HTML básico en los extractos por ejemplo negritas con `<strong>texto</strong>`, enlaces o saltos de línea.

{{< youtube RGnLzGTwNKc >}}

**Enlaces**:

- [Excerpts](https://wordpress.com/support/excerpts/).
- Temas que aceptan extractos [https://wordpress.com/themes/filter/blog-excerpts](https://wordpress.com/themes/filter/blog-excerpts).

### Comentarios

Los comentarios se pueden configurar de forma global en **Ajustes | Comentarios** del menú principal del escritorio. Se puede definir por defecto si queremos admitir comentarios para todas las entradas.

![](img/03.png)

Se puede ajustar la opción de permitir comentarios para cada publicación en la pestaña **Documento** del propio post.

![](img/02.png)

Un **pingback** es un sistema automático de comunicación mediante el cual enviamos un aviso a un sitio web de que ha sido enlazado desde el nuestro. Un **trackback** es un proceso de comunicación muy parecido al pingback pero que se realiza de forma manual y que no supone ninguna comprobación por parte del post enlazado. Si están activados saldrán como comentarios en nuestro blog.

Inicialmente la funcionalidad de pingback y trackback estaba ideada para proveer de una forma de comunicación entre sitios Webs - para permitir a otros sitios saber que hemos publicado un _post_ mencionándolos, o a la inversa (normalmente usando un enlace). Actualmente ha caído en desuso y la mayoría de los bloggers apenas se preocupan de estas notificaciones.

Todos los comentarios de nuestro sitio Web en WP se puede administrar de forma centralizada en el menú principal del escritorio:

![](img/04.png)

**Enlaces**:

- [Habilitar e inhabilitar los comentarios de las próximas entradas](https://wordpress.com/es/support/habilitar-inhabilitar-comentarios-de-proximas-entradas/).

## Trabajando con las revisiones de una publicación

WP permite un control de versiones básico de las entradas, cada vez que actualizas el contenido en vez de sobrescribir el contenido previo crea una nueva revisión y almacena la versión anterior en la base de datos. A pesar de que esta funcionalidad parece que carece de utilidad cobra sentido cuando los contenidos del sitio son gestionados por más de una persona.

Las revisiones se pueden ver en la pestaña **Documento** en la barra de ajustes lateral, muestra el número de revisiones actuales. 

![](img/05.png)

Si hacemos clic sobre la etiqueta nos dirige a una página donde podemos comparar revisiones individuales, podemos restaurar una versión previa o comparar dos cualesquiera, usando el control _slider_ nos movemos entre versiones.

**Enlaces**:

- [https://wordpress.com/es/support/editores/revisiones-entradas-paginas/](https://wordpress.com/es/support/editores/revisiones-entradas-paginas/)

## Cambiando el autor de la entrada

![](img/06.png)

## Vídeo: crear un índice de forma manual

 {{< youtube XUKdyg1rRp0 >}}


