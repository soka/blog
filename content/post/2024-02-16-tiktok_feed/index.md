---
title: Iluntasunera Bultzatuak TikTok-en
subtitle: Nola lotzen zaitu TikTok-en diseinuak?
date: 2024-02-16
draft: true
description: Nola lotzen zaitu TikTok-en diseinuak?
tags: ["UX","Diseinua","TikTok","Feed"]
---

<!-- vscode-markdown-toc -->
* 1. [Informazio iturriak](#Informazioiturriak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

44 urte ditut, zaharregia agian (edo alferregia) TikTok-ekin hasteko, hori izango da. Lan egun bateko  edozein arratsalde izan zitekeen, laneko gauzak bukatu ditut baina oraindik etxea jaso eta beste gauza batzuk egiteke ditut, hori egin beharrean nire burua topatu dut Facebookeko _reels_ (bideo motzak) irensten bata bestearen atzetik, hain da erreza asperdura keinu batekik batetik bestera pasatzea, orain janari exotiko baten errezeta bat, orain bi errusiar edo batek daki nongo batzuk borrokan kalean, katutxoak nola ez... Gutxitan egiten badut ere hori nahiko nukeena baino gehiago da. Dudan adina izanda, ingeniari informatikoa izanda ere, horrek ez nau babesten, nire alde emozionalaren errora jotzen bait dute, hor niri alde analitiko eta errazionalak ezer gutxi egin dezake.

Sean Parker, Facebookeko zuzendari ohia, 2017an aitortu zuen "like" botoia «denbora gehien kontsumitzeko» estrategiak jendeari arreta kontzientea emateko aukera». Diseinua erabiltzaileei txiki bat emateko asmoarekin egin zuen dopamina-kolpea, giza psikea: gizarte balioztapenaren beharra. Gehitu zuen: «Jainkoak bakarrik daki zer egiten ari zaion gure seme-alabak».

Orain lau urte, 2020an EEUUko kongresuko ikerketa baten Facebook-eko konpainiaren exekutibo ohi batek sare sozial erraldoiari leporatu zion desinformazioaren hedapena errazten duten algoritmoak eraikitzea, osasun mental krisialdi baterako oinarriak ezarriz, ez hori bakarrik, Facebook-ek hasieratik egin nahi izan du zerbitzua adikzio, eta tabakoaren industria adibide moduan hartu zuten. Facebook-ek ahalik eta arretarik handiena lortzea bilatzen zuen aurrekaririk gabeko etekinak lortzeko, beste edozerren gainetik, zu zeu ere.

Berriki Aministia Internazionala erakundeak txosten bat argitaratu, "Iluntasunera Bultzatuak" izenarekin, TikTok plataformak gazteen osasun mentalean izan dezakeen eraginaz.   

Hemen bereziki fokua jarri nahi dut aplikazio baten diseinuak nola modu konszientean erabiltzen dituzten menpekotasuna sortzeko, eta ez bereziki edo bakarrik gazteengan.




##  1. <a name='Informazioiturriak'></a>Informazio iturriak

* Datanomics: Todos los datos personales que das sin darte cuenta y todo lo que las empresas hacen con ellos.  Paloma Llaneza.
* [https://www.businessinsider.es/facebook-diseno-ser-adictivo-como-tabaco-exejecutivo-724243](https://www.businessinsider.es/facebook-diseno-ser-adictivo-como-tabaco-exejecutivo-724243)
* [doc/POL4073502023SPANISH.pdf](doc/POL4073502023SPANISH.pdf)
* [https://www.amnesty.org/en/documents/pol40/7350/2023/es/](https://www.amnesty.org/en/documents/pol40/7350/2023/es/)
* [https://uxplanet.org/how-tiktok-design-hooks-you-up-6c889522c7ed](https://uxplanet.org/how-tiktok-design-hooks-you-up-6c889522c7ed)
* [https://es.wikipedia.org/wiki/Ley_de_Hick](https://es.wikipedia.org/wiki/Ley_de_Hick)
* [https://es.wikipedia.org/wiki/Nir_Eyal](https://es.wikipedia.org/wiki/Nir_Eyal)