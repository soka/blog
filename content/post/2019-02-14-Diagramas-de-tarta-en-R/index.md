---
title: Diagramas tipo tarta en R
subtitle: Gráficos para variables cualitativas con pie()
date: 2019-02-14
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico", "XML", "R - Diagramas", "Diagramas"]
---

Los diagramas de sectores (o de pastel) se usan a menudo para **representar distribuciones de frecuencias de variables cualitativas**. Se representa en un círculo cuyas proporciones (sectores circulares) tienen un área proporcional a las frecuencias absolutas de las modalidades de la variable. En muchas webs no recomiendan este tipo de diagramas ya que nuestro ojo no percibe bien los volumenes, en su lugar se recomiendan los diagramas de barras.

# Diagrama simple de tarta

```R
png(file = "01.png")
proporciones <- c(10, 12,4, 16, 8) # creamos un vector con proporciones
etiquetas <- c("Uno", "Dos", "Tres", "Cuatro", "Cinco") # vector con etiquetas
pie(proporciones, labels = etiquetas, main="Diagrama de tarta")
dev.off() # Guardamos diagrama
```

El resultado es el siguiente:

![](images/01.png)

```R
pie(x, labels, radius, main, col, clockwise)
```

Sintaxis básica:

- `x`: Vector numérico con los valores.
- `labels`: Descripción de las porciones.
- `radius`: Indica el radio (un valor entre -1 y 1).
- `main`: Título.
- `col`: Paleta de colores.
- `clockwise`: Valor lógico que indica si se dibuja en sentido horario.

# Con porcentajes anotados y colores

Para añadir los porcentajes a las etiquetas debemos hacer unas operaciones previas claro, calculamos el porcentaje de cada varible y las redondeamos con [`round`](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/Round).

Con la función [`paste`](https://www.rdocumentation.org/packages/base/versions/3.5.2/topics/paste) (concatena vectores convertiendolos en cadenas de texto previamente) vamos componiendo las etiquetas.

La función `rainbow` la usamos para colorear las porciones.

```R
png(file = "02.png")
proporciones <- c(10, 12,4, 16, 8) # creamos un vector con proporciones
etiquetas <- c("Uno", "Dos", "Tres", "Cuatro", "Cinco") # vector con etiquetas
pct <- round(proporciones/sum(proporciones)*100)
etiquetas <- paste(etiquetas, pct) # Añadimos porcentajes a etiquetas
etiquetas <- paste(etiquetas,"%",sep="") # Añadimos el símbolo de %

pie(proporciones,labels = etiquetas,
    col=rainbow(length(etiquetas)),
    main="Diagrama de tarta")

# Añadimos un cuadro con leyendas
legend("topright", c("Uno","Dos","Tres","Cuatro","Cinco"), cex = 0.8,
       fill = rainbow(length(proporciones)))

dev.off() # Guardamos diagrama
```

![](images/02.png)

# Ejemplos

El siguiente ejemplo usa la función `rainbow()` para generar la paleta de colores, pasando como parámetro de entrada cuantos colores queremos generar retorna como salida un vector con los colores.

```R
ventas <- c(1,5,7,2,10,12)
pie(ventas,col = rainbow(length(ventas)))
```

![](images/04.PNG)

# Diagramas 3D con plotrix

La función `pie3D( )` del paquete [plotrix](https://cran.r-project.org/web/packages/plotrix/index.html) proporciona diagramas 3D:

```R
library(plotrix)
proporciones <- c(10, 12,4, 16, 8) # creamos un vector con proporciones
etiquetas <- c("Uno", "Dos", "Tres", "Cuatro", "Cinco") # vector con etiquetas
pie3D(proporciones,labels=etiquetas,
      explode=0.1,
      main="Diagrama de tarta")
```

![](images/03.png)

# Ejercicios

- r / intro / 12-Graficos-tarta / [grafico-tarta.R](https://gitlab.com/soka/r/blob/master/intro/12-Graficos-tarta/grafico-tarta.R).
- r / intro / 12-Graficos-tarta / [incidencias.csv](https://gitlab.com/soka/r/blob/master/intro/12-Graficos-tarta/incidencias.csv).

# Enlaces

- ["Importar datos de un CSV"](https://soka.gitlab.io/blog/post/2019-01-29-importar-datos-csv-en-r/).

**Enlaces externos**:

- ["R - Pie Charts"](https://www.tutorialspoint.com/r/r_pie_charts.htm).
- ["Pie Charts"](https://www.statmethods.net/graphs/pie.html).
- ["Adding and removing columns from a data frame"](http://www.cookbook-r.com/Manipulating_data/Adding_and_removing_columns_from_a_data_frame/).
- ["plyr: Tools for Splitting, Applying and Combining Data"](https://cran.r-project.org/web/packages/plyr/index.html): A set of tools that solves a common set of problems: you need to break a big problem down into manageable pieces, operate on each piece and then put all the pieces back together. For example, you might want to fit a model to each spatial location or time point in your study, summarise data by panels or collapse high-dimensional arrays to simpler summary statistics. The development of 'plyr' has been generously supported by 'Becton Dickinson'.
- ["Tutorial: dealing with time in R"](http://stcorp.nl/R_course/tutorial_time_in_r.html).
- ["R Is Not So Hard! A Tutorial, Part 14: Pie Charts"](https://www.theanalysisfactor.com/r-tutorial-part-14/).
- ["How to make pie charts in R using plotly."](https://plot.ly/r/pie-charts/).
