---
title: Cambiar la contraseña de usuario con PowerShell
subtitle: Administración de usuarios en Active Directory
date: 2019-02-25
draft: true
description: cmdlet Reset-ADUserPwd.ps1 
tags: ["PS","PowerShell","Scripting","Active Directory","Windows","MS","Domain","Usuarios"]
---

Este es uno de los cmdlet que más uso de forma rutinaria, por políticas de seguridad las contraseñas de las cuentas de usuarios expiran cada 90 días y a menudo no las recuerdan a la hora de renovarlas o simplemente no las recuerdan (sobre todo los Lunes). El cmdlet lo tengo copiado en el servidor de dominio MS Windows Server 2008, accedo a el usando la sesión remota de línea de comandos interactiva de **Powershell** sin necesidad de usar el interface gráfico.

```
PS> Enter-PSSession -ComputerName COMPUTER -Credential USER
````

# Reset-ADUserPwd.ps1

Para usar las utilidades para gestionar cuentas de **AD (Active Directory)** lo primero es cargar el modulo con la siguiente instrucción:

```
import-module activedirectory
```

El resto es muy sencillo, solicitamos el nombre de la  cuenta de usuario (security accounts manager (SAM) account name) de AD y a continuación la nueva contraseña, con estos datos llamamos a la función `Set-ADAccountPassword` pasandole estos dos parámetros. La cadena introducida con la nueva contraseña como texto plano se debe convertir en texto seguro previamente usando `ConvertTo-SecureString`.

Para generar la nueva contraseña suelo usar esta Web [Secure Password Generator](https://passwordsgenerator.net/) que permite generar cadenas aleatorias de longitud variable formadas por carácteres, números y simbolos.

En muchos casos la nueva contraseña es temporal y se le proporciona al usuario para que pueda acceder y establezca una nueva de su elección y sólo el o ella la conozca. 
 
```
import-module activedirectory

Write-Host "Cree una contraseña segura usando un servicio como https://passwordsgenerator.net/"
$UserId = Read-Host "Deme Identidad del usuario (ejemplo: 'b.sinclair')"

$Newpwd = Read-Host "Deme nueva clave (compleja)" 

Set-ADAccountPassword $UserId -NewPassword (ConvertTo-SecureString -AsPlainText -String "$Newpwd" -force)

$YesNoResp = Read-Host "Quiere que deba establecer una nueva en primer acceso? [s/n]"

if ( $YesNoResp  -eq 's' ) {
	Write-Host "Estableciendo que deba definir nueva clave en primer login"
	Set-ADUser -Identity $UserId -ChangePasswordAtLogon $true
} 
``` 
 
# Enlaces

* **cmdlet**: PSFabrik / docs / src / active_directory / users / [Reset-ADUserPwd.ps1](https://gitlab.com/soka/PSFabrik/blob/master/docs/src/active_directory/users/Reset-ADUserPwd.ps1).
* Post en mi repositorio de GitLab dedicado a PS [PSFabrik](https://soka.gitlab.io/PSFabrik/) ["Resetear la clave de usuario"](https://soka.gitlab.io/PSFabrik/active-directory/cuentas-usuarios/#resetear-la-clave-de-usuario).

**Enlaces externos**:

* ["Set-ADAccountPassword - Microsoft Docs"](https://docs.microsoft.com/en-us/powershell/module/addsadministration/set-adaccountpassword?view=win10-ps): Modifies the password of an Active Directory account.
* ["ConvertTo-SecureString - Microsoft Docs"](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/convertto-securestring?view=powershell-6): Converts encrypted standard strings to secure strings. It can also convert plain text to secure strings. It is used with ConvertFrom-SecureString and Read-Host..
