---
title: Telegram ofrece en su última actualización encuestas
subtitle: Últimas novedades Telegram
date: 2019-01-08
draft: false
description: Polls - Bringing Choice to Communities
tags: ["Telegram","Aplicaciones","Mensajería"]
---

![](images/01.PNG)

Telegram vuelve a ofrecer nuevas características en su última actualización, en este caso han creado un **soporte nativo para realizar encuestas anónimas**, hasta ahora sólo se podían implementar usando bots como [@vote](https://t.me/vote). Las encuestas pueden ser reenviadas para alcanzar a mayor número de personas posibles 

Por el momento parece que no funciona para el [interface Web](https://web.telegram.org/) ni la aplicación de escritorio en MS Win. **En Android las encuestas se crean desde el icono del clip para adjuntar elementos**. 

![](images/05.jpg)

Las opciones se reducen a crear preguntas con varias opciones seleccionables como respuesta:

![](images/04.jpg)

A medida que responden los usuarios la barra de progreso muestra el avance de cada opción:

![](images/03.jpg)


# Enlaces externos

* ["Polls: Bringing Choice to Communities"](https://telegram.org/blog/polls).