---
title: KeePass
subtitle: Gestor centralizado de contraseñas
date: 2018-11-05
draft: false
description: Aplicación para MS Win para almacenar cuentas de forma segura
tags: ["Aplicaciones", "Seguridad","Sistemas","Windows"]
---

[**KeePass**](https://keepass.info/) es una aplicación para gestionar de forma centralizada todas las contraseñas que usamos a diario para acceder a infinidad de sitios. [**KeePass**](https://keepass.info/) es **gratuito y de código abierto para sistemas operativos MS Windows**, almacena todas las claves en una sola base de datos (encriptada con [AES](https://es.wikipedia.org/wiki/Advanced_Encryption_Standard) y [Twofish](https://es.wikipedia.org/wiki/Twofish)) que se gestionan a traves de la aplicación gráfica, la propia aplicación está protegida por una contraseña maestra para acceder, de esta forma solo debes recordar una contraseña para acceder al resto. 

![](images/main_big.png)

# Descarga y configuración inicial

Desde en [enlace de descarga](https://keepass.info/download.html) se puede optar por la versión instalable o una portable que no requiere instalación, yo personalmente suele optar por las versiones portables, es más facil moverla o ejecutarla en cualquier sitio y el resto del equipo que usa la aplicación tampoco necesita tenerlo instalado, además no "ensucio" el sistema operativo probando aplicaciones.

![](images/web_downloads.png)

Cuando arrancamos por primera vez debemos crear una base de datos para albergar la información. Le mejor es guardarla en un subcarpeta dentro de donde está la aplicación portable, así podemos llevarnos o mover la aplicación y las contraseñas copiando toda la carpeta de KeePass. La base de datos se almacena con formato y extensión [KDBX](https://keepass.info/help/kb/kdbx_4.html). Después definimos la contraseña maestra.

![](images/master_pwd.png)

A continuación podemos definir algunas características de la base de datos, algoritmo de encriptación, compresión y otros parámetros que podemos dejar tal cual están probablemente.

![](images/db_settings.png)
 
Después de estos sencillos pasos ya podemos empezar a usar la aplicación.

# Uso

![](images/main_gui.png)

Las entradas con las cuentas se puede ordenas en carpetas y subcarpetas usando el criterio que queramos, cuando añadimos una nueva entrada (CTRL+I) podemos definir la información básica de la cuenta ordenada en pestañas. 

![](images/add_entry.png)

Para empezar con rellenar los campos principales como el título el usuario y la cave es suficiente, la sección de notas es muy interesante para añadir información de interés y la URL acabas usandola en muchos casos ya que hoy día muchas cuentas que manejamos son de aplicativos Web. 

# Plugins

**KeePass** es sencillo de usar a primera vista, cumple una función muy concreta, cuando nos hayamos familiarizado deberiamos consultar la extensa [lista de plugins](https://keepass.info/plugins.html) disponibles. Son muchos los plugins pero algunos son realmente interesantes en cualquier caso. 

![](images/web_plugins.png)

El primero que deberiamos instalar es algun plugin para realizar copias de seguridad de la base de datos KDBX con las entradas que hemos creado, [SimpleDatabaseBackup](https://keepass.info/plugins.html#simpledbbackup) es muy sencillo, genera una copia de la base de datos cada vez que la abrimos y la salvamos, guarda hasta 5 copias máximo (entra el más nuevo y se borra el más antiguo), **como pega sólo crea copias locales**, se puede consultar todos los detalles del funcionamiento en el [repositorio GitHub del plugin](https://github.com/jnko/SimpleDatabaseBackup). 

Para mantener una copia de la base de datos sincronizada en Google Drive usaremos [KPGoogleSync](https://keepass.info/plugins.html#kpgsync), si usamos OneDrive de MS usaremos [KeePassOneDriveSync](https://keepass.info/plugins.html#kpodsync). 

Cuando tenemos una copia de la base de datos en alguno de estos servicios de almacenamiento podemos acceder a ella desde un móvil Android con [Keepass2Android Password Safe](https://play.google.com/store/apps/details?id=keepass2android.keepass2android&hl=es), es muy útil cuando debemos desplazarnos a menudo y gestionamos un gran número de claves de diferentes sistemas.

![](images/keepass_android.png)


