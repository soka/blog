
# Crear una lista con listas anidadas inicializadas a un valor
print([['a','b']]*3)   
# [['a', 'b'], ['a', 'b'], ['a', 'b']]

# Forma compacta de crear una lista de listas usando List Comprehension
width=3
height=2
grid=[['i'] * width for i in range(height)]  
print(grid)
# [['i', 'i', 'i'], ['i', 'i', 'i']]

