#!/usr/bin/env python3

from random import randint

DEFAULT_CELL_WIDTH=10 
DEFAULT_CELL_HEIGHT=10
DEFAULT_INIT_ALIVE_CELLS_NUM=20
DEFAULT_GAME_TURNS=10


ALIVE = '*'
DEAD = '.' 

class GameOfLife:
    
    def __init__(self, width=DEFAULT_CELL_WIDTH, height=DEFAULT_CELL_HEIGHT,init_alive_cells_num=DEFAULT_INIT_ALIVE_CELLS_NUM,game_turns=DEFAULT_GAME_TURNS):
        self.width = width
        self.height = height              
        self.init_alive_cells_num = init_alive_cells_num   
        self.game_turns =  game_turns   
        self.grid=[[DEAD] * width for i in range(height)]                
        
    def draw_grid(self):
        for y in range(0,self.height):
            for x in range(0,self.width):
                print(self.grid[y][x]+' ',end='')
            print()
            
    def set_init_alive_cells(self):               
        for i in range(self.init_alive_cells_num):
            empty_cell_found = False 
            while not empty_cell_found:             
                y = randint(0,self.height-1)                    
                x = randint(0,self.width-1)
                if self.grid[y][x] == DEAD:
                    self.grid[y][x] = ALIVE
                    empty_cell_found = True                                    
                                    


game=GameOfLife()
game.set_init_alive_cells()
game.draw_grid() 