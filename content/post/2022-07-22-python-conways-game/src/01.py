#!/usr/bin/env python3

DEFAULT_CELL_WIDTH=10 
DEFAULT_CELL_HEIGHT=10
DEFAULT_INIT_ALIVE_CELLS_NUM=20
DEFAULT_GAME_TURNS=10


ALIVE = '*'
DEAD = '.' 

class GameOfLife:
    
    def __init__(self, width=DEFAULT_CELL_WIDTH, height=DEFAULT_CELL_HEIGHT,init_alive_cells_num=DEFAULT_INIT_ALIVE_CELLS_NUM,game_turns=DEFAULT_GAME_TURNS):
        self.width = width
        self.height = height              
        self.init_alive_cells_num = init_alive_cells_num   
        self.game_turns =  game_turns   
        self.grid=[[DEAD] * width for i in range(height)]                

game=GameOfLife()



    

