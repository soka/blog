#!/usr/bin/env python3

from random import randint

DEFAULT_CELL_WIDTH=10 
DEFAULT_CELL_HEIGHT=10
DEFAULT_INIT_ALIVE_CELLS_NUM=20
DEFAULT_GAME_TURNS=10

ALIVE = '*'
DEAD = '.' 

class GameOfLife:
    
    def __init__(self, width=DEFAULT_CELL_WIDTH, height=DEFAULT_CELL_HEIGHT,init_alive_cells_num=DEFAULT_INIT_ALIVE_CELLS_NUM,game_turns=DEFAULT_GAME_TURNS):
        self.width = width
        self.height = height      
        self.grid=[[DEAD] * width for i in range(height)]        
        self.init_alive_cells_num = init_alive_cells_num   
        self.game_turns =  game_turns   

        
    def draw_grid(self):
        for y in range(0,self.height):
            for x in range(0,self.width):
                print(self.grid[y][x]+' ',end='')
            print()
            
    def set_init_alive_cells(self):               
        for i in range(self.init_alive_cells_num):
            empty_cell_found = False 
            while not empty_cell_found:             
                y = randint(0,self.height-1)                    
                x = randint(0,self.width-1)
                if self.grid[y][x] == DEAD:
                    self.grid[y][x] = ALIVE
                    empty_cell_found = True                                    
                                    
    def get_cell_living_neighbors(self,x,y):
        numNeighbors = 0
        #print('get_cell_living_neighbors(x='+str(x)+',y='+str(y)+')')

        # esquina superior izquierda
        if y>0 and x>0:
            if self.grid[y-1][x-1]==ALIVE:
                numNeighbors+=1
                #print('esquina superior izquierda') 
        # superior
        if y>0:
            if self.grid[y-1][x]==ALIVE:
                numNeighbors+=1
                #print('superior') 
        # esquina superior derecha
        if x < (self.width-1) and y>0:
            if self.grid[y-1][x+1]==ALIVE:
                numNeighbors+=1
                #print('esquina superior derecha') 
        
        # izquierda
        if x>0:
            if self.grid[y][x-1]==ALIVE:
                numNeighbors+=1
                #print('izquierda') 

        # derecha
        if x<(self.width-1):
            if self.grid[y][x+1]==ALIVE:
                numNeighbors+=1
                #print('derecha') 
        
        # esquina inferior izquierda
        if x>0 and y<(self.height-1):
            if self.grid[y+1][x-1]==ALIVE:
                numNeighbors+=1
                #print('esquina inferior izquierda')
        # inferior
        if y<(self.height-1):
            if self.grid[y+1][x]==ALIVE:
                numNeighbors+=1
                #print('inferior')
        
        # esquina inferior derecha
        if x<(self.width-1) and y<(self.height-1):
            if self.grid[y+1][x+1]==ALIVE:
                numNeighbors+=1
                #print('esquina inferior derecha')
        
        #print('get_cell_living_neighbors: numNeighbors='+str(numNeighbors))
        return numNeighbors

    
    def next_turn_grid(self):
        next_grid=[[DEAD] * self.width for i in range(self.height)]  
        for y in range(0,self.height):
            for x in range(0,self.width):
                numNeighbors=self.get_cell_living_neighbors(x,y)
                if self.grid[y][x] == ALIVE:
                    if numNeighbors<2:
                        next_grid[y][x]=DEAD
                    if numNeighbors>=2 and numNeighbors<=3:
                        next_grid[y][x]=ALIVE
                    if numNeighbors>3:
                        next_grid[y][x]=DEAD
                else:
                    if numNeighbors==3:
                        next_grid[y][x]=ALIVE
        self.grid = next_grid

game=GameOfLife()
game.set_init_alive_cells()
game.draw_grid() 
game.next_turn_grid()
print()
game.draw_grid() 

