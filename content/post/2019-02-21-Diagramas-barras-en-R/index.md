---
title: Diagramas de barras en R
subtitle: Usando la función barplot()
date: 2019-02-21
draft: false
description: Manipulación de datos y análisis estadístico  
tags: ["R","Programación","R - Básico","R - Diagramas","Diagramas"]
---

Los diagramas de barras nos permiten comparar frecuencias de una variable cualitativa. La función básica para crear un digrama de barras es **`barplot()`**. Como siempre podemos consultar la ayuda desde la propia línea de comandos con `?barplot` o `help(barplot)`.

# Ejemplo básico

Este es el ejemplo más sencillo posible, le pasamos como argumento un vector de valores con las alturas de las barras.

```R
peras <- 45
manzanas <- 90

barplot(c(peras,manzanas))
```

El resultado es un poco "espartano":

![](images/barplot01.png)

# Añadiendo un poco de texto al diagrama

Definimos las etiquetas de texto de cada columna, una leyenda para el eje vertical (argumento `ylab`) y un título principal para el diagrama con el parámetro `main`. El argumento `names.arg` es un vector de nombres de las columnas. 

Podemos jugar con el tamaño de letra de las etiquetas de las columnas usando el argumento `cex.names = 0.7`.

Añadimos una leyenda (caja con texto y color asociado) utilizando el argumento `legend.text`.

```R
barplot(c(peras,manzanas),
        names.arg = c("Peras","Manzanas"),
        main = "Stock frutería\n1990 - 1998",
        ylab = "Frecuencia absoluta (conteo)",
        cex.names = 0.7,  # Reducimos tamaño fuente nombres columnas
        legend.text=c("Peras","Manzanas") 
        )
```

![](images/barplot02.png)

La leyenda que que acabamos de colocar “pisa” una de las barras del gráfico. En la gráfica patrón podemos observar que el eje de ordenadas tiene una amplitud mayor, que va de 0 a 90 unidades. Para cambiar los límites del eje de ordenadas utilizamos el argumento `ylim`.

# Colores

Definimos los colores de las barras con un vector que debe tener las mismas dimensiones que el numero de variables que estamos graficando. Podemos definir los colores por nombre o definiendo valores hexadecimales.

```R
barplot(c(peras,manzanas),
        names.arg = c("Peras","Manzanas"),
        main = "Stock frutería\n1990 - 1998",
        ylab = "Frecuencia absoluta (conteo)",
        col = c("coral","azure3")
)
```

![](images/barplot03.png)

# Cambiando la orientación

Podemos cambiar la orientación del diagrama usando el parámetro `horiz = TRUE`. Adicionalmente, el argumento las = 1 permite colocar en posición vertical los nombres en el eje y (las = 2 hace lo mismo en ambos ejes).

```R
barplot(c(peras,manzanas),
        names.arg = c("Peras","Manzanas"),
        main = "Stock frutería\n1990 - 1998",
        xlab = "Frecuencia absoluta (conteo)",
        horiz = TRUE,
        las = 1 # 1 = Cambia orientación texto columnas, 2  = gira texto de ambos ejes
)
```

![](images/barplot04.png)


# Ejercicios

* r / intro / 13-Diagramas-barras / [barplots.R](https://gitlab.com/soka/r/blob/master/intro/13-Diagramas-barras/barplots.R).


# Enlaces externos

* **R** incluye una demo sobre gráficos ejecutando `demo("graphics")`.
* ["Diagrama de barras - Wikipedia, la enciclopedia libre"](https://es.wikipedia.org/wiki/Diagrama_de_barras).
* ["Gráficos en R: Diagramas de barras (barplots)"](http://www.dma.ulpgc.es/profesores/personal/stat/cursoR4ULPGC/9b-grafBarplot.html): Angelo Santana & Carmen Nieves Hernández, 
Departamento de Matemáticas, ULPGC.
* ["Gráfico de barras (parte 1)"](https://picandoconr.wordpress.com/2016/03/15/grafico-de-barras-parte-1/).
* ["GGPLOT2. Gráfico de barras IV - Reordenar barras"](https://rpubs.com/Rortizdu/140196).
* 