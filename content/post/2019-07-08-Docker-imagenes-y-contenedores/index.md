---
title: Montar un servidor Web básico con NGINX y Docker
subtitle: Resumen de comandos básicos
date: 2019-07-08
draft: false
description: Montar un servidor Web NGINX sobre Docker
tags: ["Docker", "Aplicaciones", "NGINX", "Sistemas", "Web"]
---

![](docker_logo.png)

Cuando trabajamos con **Docker** es importante tener un conocimiento básico de como funcionan las **imágenes y contenedores**. Los contenedores **Docker** se crean a partir de imágenes. Voy a revisar algunos comandos básicos para crear estos contenedores a partir de imágenes.

![](img/sharing-layers.jpg)

- **Imagen**: Es una plantilla de solo lectura con instrucciones para crear el contenedor. Es una combinación del sistema de ficheros y parámetros. A menudo una imagen se crea a partir de otra imagen con algunas adaptaciones o cambios.
- **Contenedor**: Es una instancia ejecutable de una imagen. Podemos crear tantos contenedores como queramos de una imagen. Un contenedor está aislado del _host_ por defecto, podemos modificar su comportamiento usando la red, montando volúmenes de datos, etc. Una vez creado un contenedor podemos pararlo, arrancarlo o borrarlo.

## Descargar una imagen

Se puede descargar una imagen de dos formas:

1. Usando el commando `pull`.
2. Creando un contenedor a partir de una imagen con el comando `docker container run nombre_imagen`, si la imagen no existe en el sistema anfitrión se descarga del repositorio.

Ejemplo para instalar un servidor Web [NGINX](https://www.nginx.com/):

```ps
PS> docker container run nginx
```

## Algunos comandos útiles

- `docker image ls`: Listado de imágenes disponibles, el comando `docker images` es equivalente.
- `docker image rm`: Borrar una imagen por su ID. En agunos casos puede ser necesario forzar el borrado si un contenedor está usando la imagen. Ejemplo error: _"Error response from daemon: conflict: unable to delete 6358ff4a5570 (must be forced) - image is being used by stopped container c03daa0c6fab"_, en ese caso borramos contenedor con `docker rm` primero,
- `docker image inspect`: Inspeccionar una imagen por su ID.
- `docker container run` Ejecutar un contenedor.
- `docker container ls`: Listar contenedores en ejecución, para obtener todos usar `ls -a`.
- `docker container stop`: Detener un contenedor en ejecución.

## Ejecución del servidor Web NGINX

Ejecutamos un contenedor a partir de la imagén con `docker container run`, le asignamos un nombre y publicamos el puerto 80 en el anfitrión:

```ps
PS> docker container run --name my-nginx -p 80:80 nginx
```

Abrimos una nueva consola y comprobamos que está en ejecutándose:

```ps
PS> docker container ls
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
9f6c13cf63f4        nginx               "nginx -g 'daemon of…"   56 seconds ago      Up 50 seconds       0.0.0.0:80->80/tcp   my-nginx
```

Abrimos el navegador [http://127.0.0.1/](http://127.0.0.1/) y podemos comprobar que efectivamente se está ejecutando el servidor Web:

![](img/01.PNG)

Alternativamente se puede comprobar con cURL:

```ps
PS> curl http://127.0.0.1


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                    <title>Welcome to nginx!</title>
                    <style>
                        body {
                            width: 35em;
                            margin: 0 auto;
                            font-family: Tahoma, Verdana, Arial, sans-serif;
                        }
                    </style>
                    <...
RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Accept-Ranges: bytes
                    Content-Length: 612
                    Content-Type: text/html
                    Date: Mon, 08 Jul 2019 08:05:12 GMT
                    ETag: "5d121161-264"
                    Last-Modified: Tue, 25 Jun 2019 ...
Forms             : {}
Headers           : {[Connection, keep-alive], [Accept-Ranges, bytes], [Content-Length, 612], [Content-Type, text/html]...}
Images            : {}
InputFields       : {}
Links             : {@{innerHTML=nginx.org; innerText=nginx.org; outerHTML=<A href="http://nginx.org/">nginx.org</A>; outerText=nginx.org; tagName=A; href=http://nginx.org/},
                    @{innerHTML=nginx.com; innerText=nginx.com; outerHTML=<A href="http://nginx.com/">nginx.com</A>; outerText=nginx.com; tagName=A; href=http://nginx.com/}}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 612
```

## Detener / arrancar el contenedor

```ps
PS> docker container stop my-nginx
```

```ps
PS> docker container start my-nginx
```

## Sesión interactiva

Ejecutamos el contenedor de forma desacoplada para poder seguir usando la línea de comandos con el parámetro `-d`, podemos comprobar después que esta ejecutandose con `docker ps`:

```ps
PS> docker run --name docker-nginx-01 -p 80:80 -d nginx
PS> docker exec -it 90c5b531d19b bash
root@90c5b531d19b:/#
```

## Mi primer sitio Web

Fichero Dockerfile para crear la imagen:

```ps
FROM nginx:alpine
COPY index.html /usr/share/nginx/html/index.html
```

Creo un index.html personalizado en mi ordenador local:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Hello World - Nginx Docker</title>
    <style>
      h1 {
        font-weight: lighter;
        font-family: Arial, Helvetica, sans-serif;
      }
    </style>
  </head>
  <body>
    <h1>
      Hello World
    </h1>
  </body>
</html>
```

Crea una imagen a partir de un Dockerfile, la opción -t (o --tag) añade un tag opcional, el último “.” indica la ruta del Dockerfile.

```ps
PS> docker build -t simple-nginx .
PS> docker images
```

Ejecuto la imagen con consola interactiva con la imagen recién creada con el Dockerfile, `-i` (o `--interactive`) mantiene la entrada STDIN abierta:

```ps
PS> docker run -it -p 8080:80  --rm --name simple-nginx-running-app simple-nginx
```

La opción `--rm` borra automaticamente el contenedor cuando este se detiene.

Ahora ya puedo abrir el navegador [http://127.0.0.1:8080/](http://127.0.0.1:8080/) y ver el resultado:

## Borrar todo

Primero debemos detener el contenedor si está en ejecución y luego borrarlo:

```ps
PS> docker container stop my-nginx
PS> docker container remove my-nginx
```

Borrar la image:

```ps
PS> docker image remove nginx
```

## PENDIENTE - Almacenamiento de datos persistente en equipo local 

Cuando un contenedor es borrado, toda la información contenida en él, desaparece. Para tener almacenamiento persistente en nuestros contenedores, que no se elimine al borrar el contenedor, es necesario utilizar volúmenes de datos.

Creando un volumen:

```ps
PS> docker volume create mis_datos

PS> docker volume inspect mis_datos
[
    {
        "CreatedAt": "2019-07-08T08:28:10Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/mis_datos/_data",
        "Name": "mis_datos",
        "Options": {},
        "Scope": "local"
    }
]
```

```PS
PS> docker container run --name my-nginx-2 -v $(pwd):/etc/nginx/conf.d/default.conf:ro -p 80:80 nginx
```

Eliminar un volumen:

```ps
PS> docker volume rm mis_datos
```

```ps
docker run --name docker-nginx -p 8080:80 -d -v [RUTA_ABSOLUTA]\docker-nginx\html:/usr/share/nginx/html nginx
```

## Enlaces internos

- ["Contenedores Docker con R"](https://soka.gitlab.io/blog/post/2019-04-13-docker-contenedor-r/).
- ["Docker - Introducción"](https://soka.gitlab.io/blog/post/2019-04-23-docker-iniciacion/).
- ["Entorno compilación GNU gcc con Docker"](https://soka.gitlab.io/blog/post/2019-06-25-docker-gcc-compiler/).

## Enlaces externos

- docs.docker.com ["About images, containers, and storage drivers"](https://docs.docker.com/v17.09/engine/userguide/storagedriver/imagesandcontainers/).
- ["Como poner en funcionamiento un Web Apache2 con PHP 7.0."](https://dockertips.com/apache_y_php).
- ["Gestión del almacenamiento en docker - PLEDIN 3.0"](https://www.josedomingo.org/pledin/2016/05/gestion-del-almacenamiento-en-docker/).
- medium.com ["Using Docker to Run a Simple Nginx Server"](https://medium.com/myriatek/using-docker-to-run-a-simple-nginx-server-75a48d74500b) -
  Aditya Purwa in Myriatek. Sep 1, 2017.
- ["nginx | Docker Documentation"](https://docs.docker.com/samples/library/nginx/).
- GitHub ["GoogleCloudPlatform/nginx-docker"](https://github.com/GoogleCloudPlatform/nginx-docker/blob/master/1/README.md).
- docs.nginx.com ["Deploying NGINX and NGINX Plus on Docker - NGINX Docs"](https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-docker/).
