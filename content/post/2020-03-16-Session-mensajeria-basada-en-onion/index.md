---
title: Session, aplicación de mensajería basada en Tor
subtitle: Aplicaciones mensajería alternativa, privacidad, anonimato y libertad
date: 2020-03-16
draft: false
description: 
tags: ["Onion","Session","Mensajería","Aplicaciones"]
---

![](img/01.png)

[Session](https://getsession.org/) es básicamente un fork de [Signal](https://signal.org/es/) pero con algunas mejoras especialmente dirigidas a la privacidad y el anonimato. Una de sus ventajas a la hora de usarlo es que **no es necesario un número de teléfono**. A nivel de funcionalidades se pueden hacer chats en grupo, mandar mensajes de voz o archivos adjuntos. 

Cuando accedemos genera un ID de sesión y debemos introducir un _nick_ y una contraseña (opcional).

![](img/02.png)

En este punto sobre Debian parece que se ha quedado atascadete. En el iPad sin embargo parece que funciona mejor. Veo un problema que no tenga la opción de usarlo a través de un navegador Web (más teniendo en cuenta que en Debian sigue bloqueado en el _login_ aún).

Para conectarnos con otros necesitamos el ID de sesión de otros usuarios o la URL de un grupo.

En resumen [Session](https://getsession.org/) es una alternativa si se busca la privacidad, las conservaciones se cifran de extremo a extremo y las comunicaciones se enrutan en una especie de red Tor. El anonimato también está asegurado ya que se usan las sesiones, no requiere registro de datos personales y la aplicación no está asociada a un número de teléfonos, es una pena que no tenga aplicación Web y que no me acabe de funcionar en Debian. 

## Referencias externas

- NEOTEO [Session: El «Tor» de la mensajería instantánea](https://www.neoteo.com/session-el-tor-de-la-mensajeria-instantanea/).
- [Session](https://getsession.org/)


