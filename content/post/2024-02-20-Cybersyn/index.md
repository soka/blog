---
title: Allenderen amets zibernetikoak
subtitle: Licencias  
date: 2024-02-20
draft: true
description: Licencias
tags: ["Txile","Cybersyn","Synco","Sozialismoa"]
---

<!-- vscode-markdown-toc -->
* 1. [Hegoamerika 70. hamarkadaren hasieran](#Hegoamerika70.hamarkadarenhasieran)
* 2. [Salvador Allenderen gobernua Txilen](#SalvadorAllenderengobernuaTxilen)
* 3. [Kanpo loturak](#Kanpoloturak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Hegoamerika70.hamarkadarenhasieran'></a>Hegoamerika 70. hamarkadaren hasieran

70. hamarkadaren hasieran hegoameriketako herrialde askotan iraultza soziala bor bor zegoen, gerra hotzaren erdian mundua polarizatuta zegoen AEB eta bloke komunistako herrialdeen artean, testuinguru horretan Salvador Allendek Txileko hauteskundean irabazi zituen eta gizarte sozialista bat eraikitzeari ekin zioen, besteak beste Allenderen gobernuak 91 bat enpresa desjabetu eta nazionalizatu zituen   

##  2. <a name='SalvadorAllenderengobernuaTxilen'></a>Salvador Allenderen gobernua Txilen




##  3. <a name='Kanpoloturak'></a>Kanpo loturak

* [https://eu.wikipedia.org/wiki/Cybersyn_proiektua](https://eu.wikipedia.org/wiki/Cybersyn_proiektua)
* [https://www.latimes.com/business/technology/story/2023-09-21/column-merchant-cybersyn-chile-tech-utopia-experiment](https://www.latimes.com/business/technology/story/2023-09-21/column-merchant-cybersyn-chile-tech-utopia-experiment)
* [https://www.bbc.com/mundo/articles/cz4gljnvnnwo](https://www.bbc.com/mundo/articles/cz4gljnvnnwo)
* [https://www.theclinic.cl/2011/08/28/el-sueno-cibernetico-de-allende/](https://www.theclinic.cl/2011/08/28/el-sueno-cibernetico-de-allende/)
* [https://www.youtube.com/watch?v=DYM34Yr9p9s](https://www.youtube.com/watch?v=DYM34Yr9p9s)
* [https://www.youtube.com/watch?v=CD9T_dRzi-k&t=4s](https://www.youtube.com/watch?v=CD9T_dRzi-k&t=4s)
* [https://x.com/evgenymorozov/status/1705164964007174290](https://x.com/evgenymorozov/status/1705164964007174290)
* [https://www.pagina12.com.ar/591145-evgeny-morozov-otro-mundo-digital-es-posible](https://www.pagina12.com.ar/591145-evgeny-morozov-otro-mundo-digital-es-posible)
* [https://x.com/CyberneticSere1/status/1382446210733199361](https://x.com/CyberneticSere1/status/1382446210733199361)
* [https://x.com/ThorOIvers/status/1684689861200973824](https://x.com/ThorOIvers/status/1684689861200973824)
* [https://x.com/PlanetaQuintana/status/1759631124001681626](https://x.com/PlanetaQuintana/status/1759631124001681626)
* [https://eu.wikipedia.org/wiki/Gerra_Hotza](https://eu.wikipedia.org/wiki/Gerra_Hotza)