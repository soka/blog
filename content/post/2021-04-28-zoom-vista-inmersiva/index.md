---
title: La nueva vista inmersiva de Zoom
subtitle: Visualiza los participantes en la misma sala
date: 2021-04-28
draft: false
description: 
tags: ["Zoom","Aplicaciones","Videollamadas"]
---

Zoom acaba de lanzar una nueva funcionalidad (para versión 5.6.3 o superior de la aplicación de escritorio) llamada vista inmersiva que permite visualizar hasta 25 participantes en la misma sala. La opción de configuración está activada por defecto en los ajustes de la cuenta en la Web de Zoom.  

![](img/01.PNG)

Con esta nueva opción se recrea un espacio común que acrecienta la sensación de estar juntos donde los encuentros son más cercanos o menos aburridos, se pueden configurar diferentes espacios virtuales, desde aula o un auditorio donde se imparte una conferencia hasta una galería de arte, o incluso subir nuestro propio fondo de pantalla.

Para usar esta opción una vez dentro de la reunión usamos las opciones de visualización en la esquina superior derecha.

![](img/04.png)

Seleccionamos la escena y si queremos colocar a las personas participantes de forma automática o manual. 

![](img/02.PNG)

Si hemos escogido posicionar de forma manual a los participantes se puede redimensionar el tamaño de cada uno y moverlo por la pantalla.

![](img/03.PNG)

## ENLACES EXTERNOS

- [https://support.zoom.us/hc/en-us/articles/360060220511-Immersive-View](https://support.zoom.us/hc/en-us/articles/360060220511-Immersive-View)
- [https://www.genbeta.com/actualidad/zoom-lanza-su-vista-inmersiva-que-te-permite-ver-a-todos-participantes-videollamada-sala](https://www.genbeta.com/actualidad/zoom-lanza-su-vista-inmersiva-que-te-permite-ver-a-todos-participantes-videollamada-sala)
- [https://wwwhatsnew.com/2021/04/26/zoom-tambien-quiere-que-los-asistentes-a-las-reuniones-tengan-sensacion-de-estar-juntos](https://wwwhatsnew.com/2021/04/26/zoom-tambien-quiere-que-los-asistentes-a-las-reuniones-tengan-sensacion-de-estar-juntos)



