---
title: Etiquetado
subtitle: Sesión nº 4 curso Git
date: 2020-11-08
draft: false
description: Control de versiones con Git y GitLab
tags: ["Cursos","Curso Git","Código","Control Versiones","GitLab","Git","SCM"]
---

![](img/00.png)

Git tiene la posibilidad de etiquetar puntos específicos del historial como importantes. Esta funcionalidad se usa típicamente para marcar versiones de lanzamiento (v1.0, por ejemplo).

## Listar etiquetas

```bash
$ git tag
```

## Crear Etiquetas

Git utiliza dos tipos principales de etiquetas: ligeras y anotadas.

Una etiqueta ligera es muy parecido a una rama que no cambia - simplemente es un puntero a un commit específico.

Sin embargo, las etiquetas anotadas se guardan en la base de datos de Git como objetos enteros. Tienen un checksum; contienen el nombre del etiquetador, correo electrónico y fecha; tienen un mensaje asociado; y pueden ser firmadas y verificadas con GNU Privacy Guard (GPG).

### Etiquetas anotadas

Crear una etiqueta anotada en Git es sencillo. La forma más fácil de hacerlo es especificar la opción -a cuando ejecutas el comando [`git tag`](https://git-scm.com/docs/git-tag):

```bash
$ git tag -a v0.1 -m 'mi version 0.1'
```

La opción -m especifica el mensaje de la etiqueta, el cual es guardado junto con ella. Si no especificas el mensaje de una etiqueta anotada, Git abrirá el editor de texto para que lo escribas.



## Enlaces internos

- [Control de versiones con Git - Sesión nº 1 curso Git](https://soka.gitlab.io/blog/post/2020-11-07-curso-git-1/).
- [Uso básico de Git - Sesión nº 2 curso Git](https://soka.gitlab.io/blog/post/2020-11-07-curso-git-2/)
- [Trabajando con repositorios remotos - Sesión nº 3 curso Git](https://soka.gitlab.io/blog/post/2020-11-08-curso-git-3/).

## Enlaces externos

- [https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado)

## Autor

Iker Landajuela.

## Licencia

[GNU Free Documentation License Version 1.3](https://www.gnu.org/licenses/fdl-1.3-standalone.html).