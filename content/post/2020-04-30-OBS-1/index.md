---
title: OBS Studio - Software libre de grabación y transmisión de vídeo en vivo [borrador]
subtitle: Aplicaciones mensajería, reuniones, videoconferencia, documentación...
date: 2020-04-30
draft: false
description: 
tags: ["Aplicaciones","OBS","Video","Streaming","Software"]
---

![](img/obs-logo.jpg)

[**OBS**](https://obsproject.com/) (Open Broadcaster Software) es una aplicación libre y de código abierto para la grabación y transmisión de vídeo en vivo (_streaming_), permite captura de fuentes de video en tiempo real, composición de escena, codificación, grabación y retransmisión. [**OBS**](https://obsproject.com/) emplea el protocolo RTMP (Real Time Messaging Protocol) compatible con Youtube, [Twitch](https://es.wikipedia.org/wiki/Twitch) o Facebook Live entre otros.

## Instalación en Debian

He seguido las instrucciones de instalación para Ubuntu en este [enlace](https://obsproject.com/wiki/install-instructions#ubuntu-installation).

```sh
sudo apt-get install ffmpeg
sudo add-apt-repository ppa:obsproject/obs-studio
sudo apt-get update
sudo apt-get install obs-studio
```

## OBS

![](img/01.png)

El programa funciona a base de “Escenas” que se componen usando diferentes “fuentes”. En cada “Escena” es posible modificar la posición o la cantidad de capas activas de tal forma que durante una sesión de captura y/o emisión se pueden modificar las capas activas.

## Streaming en Youtube

Previamente se debe activar Youtube Studio en la cuenta asociada, ojo porque tarda 24 horas. Dentro de Yotube Studio creo una nueva sesión de _streaming_ pública compartida por enlace.

![](img/02.png)

En [**OBS**](https://obsproject.com/) accedemos al menú Archivo > Configuración, buscamos el apartado emisión, seleccionamos el servicio Youtube y pegamos la clave de retransmisión que nos proporciona Yotube Studio.

![](img/03.png)

Ahora en la pantalla principal de [**OBS**](https://obsproject.com/) seleccionar la opción de "Iniciar Transmisión", Youtube Studio detecta que está conectado el cliente de _streaming_, cuando estemos preparados usamos el botón "GO LIVE" para iniciar la transmisión.

Podemos compartir no solo una fuente de entrada de vídeo, en la misma escena podemos solapar el vídeo con la imagen de la pantalla y combinar una y otra dimensionando las ventanas en [**OBS**](https://obsproject.com/), usamos de nuevo la opción añadir fuente para añadir una captura de ventana.

Para habilitar el chat la transmisión no debe ser para menores.

## Screencast

Para hacer un sesión de _screencast_ lo ideal es tener dos monitores, uno con lo que queremos mostrar y el otro con la interfaz de [**OBS**](https://obsproject.com/).




## Enlaces externos

- [Open Broadcaster Software](https://es.wikipedia.org/wiki/Open_Broadcaster_Software).
- https://www.misingresospasivos.com/hacer-streaming-en-youtube-con-obs/.
- https://blog.juggernautplays.com/configurar-obs-studio/. 
- http://www.compartirwifi.com/blog/como-emitir-en-directo-en-youtube-hacer-directos-en-streaming-emitir-audio-y-video-juegos-o-musica/#more-664832.

