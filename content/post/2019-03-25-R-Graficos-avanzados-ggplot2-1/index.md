---
title: Gráficos avanzados en R
subtitle: Introducción a ggplot2
date: 2019-03-25
draft: false
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "Programación",
    "R - Básico",
    "R - Gráficos",
    "R - Diagramas",
    "R - ggplot2",
    "Diagramas",
  ]
---

![](images/ggplot_hex.jpg)

[ggplot2](https://ggplot2.tidyverse.org/) es un paquete especializado para visualización de datos avanzados, el creador de **ggplot2** en el 2009 es [Hadley Wickham](http://hadley.nz/). No hay mas que ver la galería de ejemplos en [https://www.r-graph-gallery.com/portfolio/ggplot2-package/](https://www.r-graph-gallery.com/portfolio/ggplot2-package/) para apreciar su potencial.

![](images/examples.PNG)

Otros desarrolladores a su vez han creado un rico ecosistema de [extensiones basadas en ggplot2](http://www.ggplot2-exts.org/gallery/).

![](images/ggplot2-extension-gallery.PNG)

**ggplot2** es una gramática ideada para definir diagramas basada en el libro de Leland Wilkinson ["The Grammar of Graphics (Statistics and Computing)"](https://www.amazon.com/Grammar-Graphics-Statistics-Computing/dp/0387245448/)).

Un gráfico en **ggplot2** se compone de varias capas o componentes:

- **data set**: **Data frame** que proporciona los datos para el gráfico.
- **aes** (_aesthetic_): Asignación de variables a [abscisas](https://es.wikipedia.org/wiki/Coordenadas_cartesianas) y coordenadas.
- **geoms**: Conjunto de marcas visuales que representan los puntos.
- **coord**: Sistema de coordenadas, por defecto son las [coordenadas Cartesianas](https://es.wikipedia.org/wiki/Coordenadas_cartesianas) pero admite muchos otros tipos como por ejemplo coordenadas geográficas para visualización de mapas.
- **stats**: Transformaciones estadísticas de los datos para conseguir los valores necesarios para reproducir un _geom_. Por ejemplo los bloques de un histograma.
- **facets**: Permite crear el mismo gráfico en subconjuntos de datos.
- **labels**: Etiquetas de los ejes cartesianos y el título del gráfico.
- **legend**: Para añadir leyendas.
- **theme**: Color de fondo, con o sin rejilla, etc.
- **scale**: Tamaños, puntos, etc.

La llamada básica es como sigue:

```R
ggplot(data=xxx,aes(),geom())
```

# Instalación y uso

Antes de nada vamos a **instalar el paquete y cargarlo** en nuestro script:

```R
install.packages("ggplot2")
library(ggplot2)
```

# data set

En primer lugar usaremos una serie de funciones para **definir los datos**, es importante saber que **ggplot2 sólo trabaja con `data.frames`**. Este paso es muy sencillo usando la función `ggplot()`. Hemos creado un objeto `p` con los datos:

```R
p <- ggplot(iris)
```

# aes

A continuación definimos las **estéticas**, son por ejemplo la distancia horizontal o vertical, el color, la forma (de un punto), el tamaño (de un punto o el grosor de una línea). Asociamos las columnas de datos a estéticas y se lo sumamos al objeto 'p'. El nombre de la función `aes()` se traduce como "Aesthetic" (estético) y describe como se relacionan los datos con las propiedades visuales de los `geoms` (capas).

```R
p <- p + aes(x = Petal.Length, y = Petal.Width, colour = Species)
```

También como es evidente dentro de las funciones **estéticas** podemos personalizar otros parámetros:

```R
p <- p + ggtitle("Petal length and width") +
  labs(x = "Petal length",
       y = "Petal width",
       colour = "Species")
```

# geoms

Las capas en **gplot** se traducen como `geoms`. Por ejemplo la función `geom_point()` sirve para crear un [diagrama de dispersión](https://es.wikipedia.org/wiki/Diagrama_de_dispersi%C3%B3n) (scatter plot).

```R
p <- p + geom_point()
```

![](images/scatter-plot-01.PNG)

Existen muchos tipos de capas pero las más comunes son: `geom_point`, `geom_line`, `geom_histogram`, `geom_bar` y `geom_boxplot` (se puede consultar la lista completa en [ggplot2.tidyverse.org/reference/](https://ggplot2.tidyverse.org/reference/)).

# facets

Creamos el mismo gráfico para cada conjunto de datos:

```R
p <- p + facet_grid(. ~ Species)
```

![](images/facets-01.PNG)

# Ejemplo: Nubes de puntos

![](images/scatterplot-01.PNG)

```R
vEdad <- c(12,17,14,15,16,16)
vFuerza <- c(20,30,25,27,30,40)
vSexo <- c('F','M','F','M','F','M')
df1 <- data.frame(edad=vEdad,fmax=vFuerza,sexo=vSexo)
g1 <- ggplot(df1, aes(x = edad, y = fmax, colour = sexo)) +
  geom_point() + # scatterplots o nubes de puntos para visualizar la relación entre 2 variables
  scale_color_brewer(palette = 'Set1') + # Escala de colores
  ggtitle('Edad y fuerza máxima en adolescentes') # Título de la gráfica

g1 <- g1 + geom_smooth() # Curva de regresión
g1
```

Hemos diferenciado por colores los puntos basándonos en el sexo de cada sujeto. Hemos unido cada grupo mediante líneas de regresión suavizada.
La función `ggplot` crea el gráfico y lo asigna al objeto `g1`, le pasamos en primer lugar el _data frame_ seguido de la estética con la función `aes` para determinar que variable irá en cada eje, después se declara la paleta de color con `scale_color_brewer`. Finalmente damos un título al gráfico con `ggtitle`. Para visualizar la gráfica introducimos `g1`.

La orden `geom_smooth` dibuja la **curva de regresión no paramétrica para cada grupo**. Si quisiéramos obtener la recta de regresión líneal:

```R
g1 <- g1 + geom_smooth(method = lm)
```

# Conclusiones

**ggplot2** tiene un funcionamiento peculiar, lo importante es recordar como se van sumando en sucesivos pasos cada componente del diagrama aunque siempre podemos combinar varios pasos en una sola expresión como sigue:

```R
ggplot(iris, aes(x = Petal.Length, y = Petal.Width, colour = Species)) + geom_point()
```

# Ejercicios

- r / intro / 30-ggplot2-1 / [ggplot2-intro.R](https://gitlab.com/soka/r/blob/master/intro/30-ggplot2-1/ggplot2-intro.R).

# Enlaces externos

- Repositorio en GitHub [https://github.com/tidyverse/ggplot2](https://github.com/tidyverse/ggplot2).
- Paquete en CRAN [https://cloud.r-project.org/web/packages/ggplot2/index.html](https://cloud.r-project.org/web/packages/ggplot2/index.html).
- PDF ["rstudio/cheatsheets/data-visualization-2.1.pdf"](https://github.com/rstudio/cheatsheets/blob/master/data-visualization-2.1.pdf).
- ["Introducción a ggplot2"](https://www.datanalytics.com/libro_r/introduccion-a-ggplot2.html): Fuente principal de este post.
