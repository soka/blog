---
title: Habilitar escritorio remoto y reglas del Firewall [borrador]
subtitle: Administración de Windows Server con PowerShell
date: 2020-01-03
draft: false
description: Administración servidores Windows
tags: ["Windows","Sistemas","PowerShell","Administración","cmdlets","Scripting","RDP","Escritorio remoto","FW","Windows Server"]
---

![](img/logo2.png)

El servicio [RDP](https://es.wikipedia.org/wiki/Remote_Desktop_Protocol) utiliza por defecto el **puerto TCP 3389** (se puede implementar sobre un servidor IIS de manera que acepte conexiones en el puerto 443 mediante conexiones HTTPS "Remote Desktop Gateway").

Desde la administración gráfica del servidor es muy sencillo:

![](img/01.png)

Desde la línea de comandos se puede usar `sconfig`:

![](img/02.png)

Yo he tenido que bajar el nivel de seguridad al mínimo para poder conectarme desde otra máquina con el usuario "Administrador". Cuando me conecto la sesión local se cierra (si accedo expulso la conexión RDP del cliente).

Desde PS modificando una clave del registro

```ps
Set-ItemProperty -Path 'HKML:\System\CurrentControlSet\Control\Terminal Server' -name "fDenyTSConnections" -value 0
```

Habilitar RDP en el FW:

```ps
Enable-NetFirewallRule -DisplayGroup "Escritorio Remoto"
```

Se puede comprobar desde el administrador del servidor:

![](img/03.png)

O desde PS:

```ps
Get-NetFirewallRule -DisplayName "Escritorio Remoto"
```

```ps
Set-ItemProperty -Path 'HKML:\System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -name "UserAuthentication" -value 1
```

Ahora se puede probar a abrir una sesión RDP desde otro equipo con el comando [`mstsc`](https://docs.microsoft.com/es-es/windows-server/administration/windows-commands/mstsc):

```ps
mstsc /v:ServerCore /admin 
```

Comprobando que el puerto RDP está abierto usando la utilidad [Portqry.exe](https://support.microsoft.com/es-es/help/310099/description-of-the-portqry-exe-command-line-utility):

![](img/04.png)



## Enlaces

- [Enable-RDP.ps1](src/Enable-RDP.ps1).
- [portqryui.exe](src/portqryui.exe).
- [Descripción de la utilidad de línea de comandos Portqry.exe](https://support.microsoft.com/es-es/help/310099/description-of-the-portqry-exe-command-line-utility): Portqry.exe es un programa de la línea de comandos que sirve como ayuda para solucionar problemas de conectividad TCP/IP. 
- [Remote Registry PowerShell Module](http://blogs.microsoft.co.il/scriptfanatic/2010/01/10/remote-registry-powershell-module/).
- [Checking and enabling Remote Desktop with PowerShell](https://blogs.technet.microsoft.com/jamesone/2009/01/31/checking-and-enabling-remote-desktop-with-powershell/).