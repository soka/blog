---
title: Conviértete en un Ninja de la productividad con el correo electrónico
subtitle: Aplica la metodología Inbox Zero en GMail
date: 2021-03-14
draft: false
description: 
tags: ["Gmail","Inbox Zero","GTD","Productividad","Correo"]
---

<!-- vscode-markdown-toc -->
* 1. [INTRODUCCIÓN](#INTRODUCCIN)
	* 1.1. [Reflexiones previas](#Reflexionesprevias)
		* 1.1.1. [¿Tienes un plan diario de acción? ¿Alguna vez te has detenido a analizar tu rutina de trabajo diaria?](#TienesunplandiariodeaccinAlgunaveztehasdetenidoaanalizarturutinadetrabajodiaria)
		* 1.1.2. [¿Cual es tu relación con el correo electrónico?](#Cualesturelacinconelcorreoelectrnico)
		* 1.1.3. [¿Síntomas de desbordamiento?](#Sntomasdedesbordamiento)
* 2. [INBOX ZERO - Merlin Mann](#INBOXZERO-MerlinMann)
	* 2.1. [¿Qué es Inbox Zero?](#QuesInboxZero)
		* 2.1.1. [Necesitas un sistema (pero mantenlo sencillo)](#Necesitasunsistemaperomantenlosencillo)
		* 2.1.2. [Características principales](#Caractersticasprincipales)
		* 2.1.3. [Las ideas básicas detrás de Inbox Zero](#LasideasbsicasdetrsdeInboxZero)
		* 2.1.4. [¡Trabajadores del conocimiento uníos!](#Trabajadoresdelconocimientounos)
		* 2.1.5. [Tiempo y atención finitos](#Tiempoyatencinfinitos)
		* 2.1.6. [Debemos buscar un estado mental propicio](#Debemosbuscarunestadomentalpropicio)
	* 2.2. [Gestiona tus acciones](#Gestionatusacciones)
		* 2.2.1. [Borrar (o archivar)](#Borraroarchivar)
		* 2.2.2. [Delegar](#Delegar)
		* 2.2.3. [Responder](#Responder)
		* 2.2.4. [Aplazarlo](#Aplazarlo)
		* 2.2.5. [Hacerlo](#Hacerlo)
	* 2.3. [El hábito de procesar](#Elhbitodeprocesar)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

En este taller compartiremos buenas prácticas de metodologías que abordan la gestión del correo electrónico para que su clasificación y organización no supongan un lastre en tu productividad y vida personal.

Este taller te ayudará a mantener tu correo electrónico organizado, ordenado, limpio, visualmente controlado y sostenible.

**Objetivos**:

* Cómo contemplar la gestión del correo electrónico según la metodología Inbox Zero.
* Abordar las buenas prácticas desde nuestra cuenta de GMAIL.

**Programa**

* Reflexiones previas: ¿Cuál es mi problema? ¿Qué relación mantengo con mi email?.
* Aproximación a la metodología Inbox Zero.
* Aplicación en Gmail y otras buenas prácticas.

**Material adicional**

* Puedes descargar las presentaciones de este enlace [presentación-20210321a.zip](./presentacion-20210321a.zip). Descomprime el archivo y abre en el navegador uno de los tres archivos HTML (presentación realizada con [reveal.js](https://revealjs.com/)).

##  1. <a name='INTRODUCCIN'></a>INTRODUCCIÓN

###  1.1. <a name='Reflexionesprevias'></a>Reflexiones previas

####  1.1.1. <a name='TienesunplandiariodeaccinAlgunaveztehasdetenidoaanalizarturutinadetrabajodiaria'></a>¿Tienes un plan diario de acción? ¿Alguna vez te has detenido a analizar tu rutina de trabajo diaria?

Suena el despertador. Un largo día por delante para disfrutar, pero también muchas cosas por hacer. ¿Te sientes relajado o estás agobiado por todo lo que tienes que hacer?

La forma en que vas a **afrontar el día no depende tanto de la cantidad de cosas que tienes que hacer como de la percepción que tienes de lo que viene por delante**. Si no tienes ningún plan y te dedicas a “verlas venir”, la incertidumbre y falta de claridad pueden elevar tu nivel de estrés de manera considerable. **Tener claro dónde vas a poner tu atención**, sin embargo, te proporcionará tranquilidad y confianza.

Imagino que a maś de una persona le pasa lo mismo que a mí, a veces empiezo la jornada de trabajo sin tener claras una serie de tareas y prioridades, me pongo inmediatamente a "apagar fuegos", sin un momento previo de reflexión y planificación mi ansiedad aumenta, especialmente el no saber que voy a hacer lo siguiente me mata. Cada cosa nueva que voy a hacer vuelvo a revisar toda mi lista mental de tareas y me embarga una sensación de ser improductivo y de estar perdiendo el tiempo.

A veces es incluso mejor tratar de planificar lo que haremos el día siguiente al final de la jornada, seguramente tendremos ya citas y compromisos de antemano, nuestra planificación girara en torno a esos eventos ineludibles y los huecos que quedan para realizar cualquier tarea.

Tener unas tareas prioritarias marcadas para el día no significa que no debamos alterar nuestro plan en el transcurso del mismo, en la mayoría de nuestros trabajos nos exigen que sepamos tomar decisiones acertadas sobre que es lo que hay que hacer o no en cada momento.

####  1.1.2. <a name='Cualesturelacinconelcorreoelectrnico'></a>¿Cual es tu relación con el correo electrónico?

![](./presentacion/img/intro/08.jpg)

* ¿**Cuántos** correos electrónicos tienes sin abrir?
* ¿Eres capaz de discernir qué correos electrónicos son urgentes e importantes o requieren una acción? ¿Usas algún **sistema de priorización**?.
* ¿Alguna vez no has hecho lo que debías hacer a tiempo porque se te ha pasado ese correo o se te olvido? O lo que es peor, te lo han tenido que recordar o has recibido una llamada de atención. Peor que saber que no has hecho algo en plazo es no saber que estás dejando de hacer.
* ¿**Te consideras mail-dependiente**? ¿Sientes que debes responder inmediatamente a todos y cada uno de los mensajes al momento y esto te provoca ansiedad?.

* ¿Estás continuamente revisando el correo electrónico para decidir que hacer a continuación?.
![](./presentacion/img/intro/18.jpg)

* ¿**Eres capaz de encontrar la información que buscas de un correo electrónico en menos de 1 minuto**? Igual directamente ni siquiera encuentras el dichoso correo electrónico.
* **¿Cuántas cuentas de correo electrónico tienes y gestionas?.**
* ¿Conoces el volumen medio de correos diarios que recibes y envías? ¿De qué tipo son?
* ¿Tienes algún sistema de organización en carpetas o categorías? Taxonomía o sistema de clasificación.
* **¿Tu correo electrónico está siempre “abierto”?**.

####  1.1.3. <a name='Sntomasdedesbordamiento'></a>¿Síntomas de desbordamiento?

Ódialo o amalo pero el correo electrónico estará entre nosotros por mucho tiempo áun. A pesar del crecimiento de los sistemas de mensajería y chats el correo electrónico es parte integral de nuestro trabajo diario y su uso no decrece con el tiempo ([Number of e-mail users worldwide 2017-2025](https://www.statista.com/statistics/255080/number-of-e-mail-users-worldwide/)).

* De acuerdo con una encuesta realizada [**pasamos un 28% de la jornada laboral gestionando el correo electrónico**](https://mashable.com/2012/08/01/email-workers-time/?europe=true#fXdJpLq4aGqL) (la misma encuesta dice que es la 2º tarea que más tiempo consume después de las tareas específicas de nuestro rol o cargo).  
* Otra encuesta dice que [**los trabajadores empleamos 6,3 horas al día de media chequeando nuestro correo**](https://www.reuters.com/article/us-usa-work-emails-idUSKCN0QV1I620150826) (tiempo repartido de forma equitativa entre correos laborales y personales).
* De acuerdo a un estudio realizado [**sólo el 38% de la bandeja de entrada contiene información relevante**](https://blog.sanebox.com/2016/02/18/email-overload-research-statistics-sanebox/). Dicho de otra manera, el 62% de los correos no son importantes y pueden ser procesados en lote.
* Tardamos 64 segundos en recuperar la atención de una interrupción del un correo electrónico (independientemente  de la importancia que tenga este). El _multitasking_ es el enemigo de la productividad.

##  2. <a name='INBOXZERO-MerlinMann'></a>INBOX ZERO - Merlin Mann

###  2.1. <a name='QuesInboxZero'></a>¿Qué es Inbox Zero?

![](./presentacion/img/inbox-zero/26.png)

**Inbox Zero es un metodología desarrollada por [Merlin Mann](http://www.merlinmann.com/) para lidiar con grandes volúmenes de correo electrónico** y evitar que estos nos saturen y lleguen a bloquearnos evitando realizar nuestro verdadero trabajo.

Puedes ver la [charla completa de Merlin Mann organizada por Google TechTalks](https://youtu.be/z9UjeTMb3Yk) hablando de **Inbox Zero** en Youtube (la presentación está también disponible en Slideshare en este [enlace](https://es.slideshare.net/merlinmann/inbox-zero-actionbased-email/3-Get_aSystembut_keep_it_simple )). La conferencia de Merlin Mann es el resultado de de varios artículos publicados en su sitio Web [43Folders](https://www.43folders.com/topics/inbox-zero), resume su lucha con grandes volúmenes de correo electrónico.

Lejos de lo que se pueda pensar a priori, **el concepto “zero” no hace referencia al número de mensajes que pretendemos que existan en la bandeja de entrada**; sino que hace referencia al tiempo deseado que una persona debería pasar en su bandeja de entrada, “vaciar” tu mente de preocupaciones y agobios para poder disfrutar (si eso es posible) así algo más del trabajo.

> "One of the most importante soft skills you can have si figuring out how to deal with a high volume of email. And the only way to do that is to put some kind of a system in place that's simple and repeteatable and is going to allow you to have an actual life outside of email" - Merlin Mann, 43folders.com and creator of Inbox Zero.

**Manejar el correo y saber lidiar con un gran volumen diario de correos electrónicos se puede considerar como un Soft skill muy importante**. La única forma de hacerlo es construir algún tipo de sistema, simple pero completo y fácil de repetir, independientemente del medio o contenido.

####  2.1.1. <a name='Necesitasunsistemaperomantenlosencillo'></a>Necesitas un sistema (pero mantenlo sencillo)

![](./presentacion/img/inbox-zero/34.jpg)

####  2.1.2. <a name='Caractersticasprincipales'></a>Características principales

* La siguiente metodología es la esencia de este arte dinámico de **gestión del flujo de trabajo y de la productividad personal de forma lineal**. Se trata de una **vacuna contra la lucha cotidiana por apagar fuegos** (lo que algunos llaman las urgencias y crisis de cualquier jornada laboral) y un antídoto para el desequilibrio. Aprender de forma sistemática un método para **controlar las cosas y no que ellas nos controlen a nosotros**.
* **Sencillo e intuitivo**, es fácil de poner en marcha. No requiere ninguna habilidad nueva, en absoluto. ofrece un abanico de acciones limitadas a cada correo entrante.
* **No debería** depender del soporte o la tecnología. Podríamos implementarlo en una agenda de papel si quisiésemos. **Una de las cosas que podemos encontrar más relajantes es la idea de que sólo tenemos que adoptar un sistema**, está idea es más poderosa que pensar que nos vamos a organizar mejor por el simple hecho de comprar una PDA o una tableta.

####  2.1.3. <a name='LasideasbsicasdetrsdeInboxZero'></a>Las ideas básicas detrás de Inbox Zero

La idea básica es que **en la bandeja de entrada sólo estén los correos que aún no hemos leído o abierto**.

* El correo electrónico es sólo un medio.
* Un sitio para todo.
* Procesar hasta cero.
* Convertir en acciones, la verdadera acción y el trabajo real se produce fuera de nuestra bandeja de entrada.

![](./presentacion/img/inbox-zero/19.png)

**Tu bandeja de entrada no es una lista de tareas para hacer**, sólo es un contenedor donde aterrizan las entradas o _inputs_. A menudo queremos conservar los correos en la bandeja de entrada porque no queremos perderlos o por temor a querer recuperarlos más adelante. **Pero el trabajo verdadero se realiza fuera de la bandeja de entrada**, usarlo como una lista de tareas a realizar significa que cualquier otra cosa se pierda si no llega por correo o que acabemos mandándonos correos a nosotros mismos con recordatorios. Además **usar la bandeja de entrada como lista de tareas mezcla los recordatorios de lo que debes hacer con otro montón de ruido e información**, lo que puede dificultar separar que debemos hacer frente a qué está pasando o que puede ser ignorado.  

Tenemos que crear alguna nueva forma de organizar  para todas estas categorías diferentes, de otra forma estaremos una y otra vez revisando nuestros correos electrónicos para decidir que requiere una acción por nuestra parte o que no.

![](./presentacion/img/inbox-zero/41.png)

El punto de partida de la metodología sería que **nuestra atención es limitada y cuando la bandeja de entrada se confunde con una lista de tareas, nuestra productividad sufre**.

####  2.1.4. <a name='Trabajadoresdelconocimientounos'></a>¡Trabajadores del conocimiento uníos!

**La naturaleza de nuestro trabajo ha cambiado estos últimos años**, ha pasado de una actividad productiva industrial a convertirse en "trabajo del saber" (“la sociedad de la información y el conocimiento” a finales de los 70 generaría la mitad del PIB).

![](./presentacion/img/inbox-zero/41.jpg)

Antaño la naturaleza del trabajo era más evidente, en la actualidad, muchos trabajamos en proyectos sin límites definidos y con indicadores del logro/éxito más difíciles de identificar, **el nuevo tipo de trabajo está caracterizado por la necesidad de decidir que hacer y el criterio para saber cuando hacerlo**. Los silos individuales que son nuestras oficinas se están desmoronando (las organizaciones están en constante cambio), y con ellos, el lujo de no tener que leer copias de los correos electrónicos del departamento de marketing, o del de gestión o RRHH.

La mayoría de nosotros nos podemos autodenominar [**trabajadores del conocimiento**](https://es.wikipedia.org/wiki/Trabajador_del_conocimiento) (termino acuñado por el experto en gestión [Peter Drucker](https://es.wikipedia.org/wiki/Peter_F._Drucker) en 1959), nuestro capital principal es el conocimiento, agregamos valor a la información, la diferencia principal respecto de otras formas de trabajo es la resolución de problemas no "rutinarios" que requiere una forma de pensamiento creativo, flexible y adaptativo. Trabajamos **con recursos limitados, finitos e insustituibles como son el tiempo y la atención** (_"No compras con plata. Compras con el tiempo de tu vida que gastas para conseguirla"_ - Pepe Mujica), sin embargo **las demandas que nos reclaman son infinitas, no hay límite a lo que la gente puede pedir a nuestro tiempo y atención**, la multitarea es una especie de mito.

####  2.1.5. <a name='Tiempoyatencinfinitos'></a>Tiempo y atención finitos

Este método **consiste en vaciar la bandeja de entrada de nuestro correo mediante el procesado de los mails y la clasificación según su utilidad**. ¡Al final eso es Inbox Zero!  Deshacernos de la basura lo antes posible, esto requiere un enfoque proactivo para gestionar las acciones que conducen tu vida.

![](./presentacion/img/inbox-zero/35.png)

[El pase Invisible - Test de atención](https://youtu.be/PbVYH8FCLvo).

El experimento del “gorila invisible”, efectuado por dos psicólogos de la Universidad de Harvard, muestra las limitaciones que tiene la percepción humana, y señala también la incapacidad de los sujetos para admitir esas limitaciones.

####  2.1.6. <a name='Debemosbuscarunestadomentalpropicio'></a>Debemos buscar un estado mental propicio

![](./presentacion/img/inbox-zero/41.jpg)

Debemos buscar el estado mental propicio para trabajar, en palabras de David Allen creador de la metodología GTD **debemos experimentar lo que llama "mente como el agua" o la "zona" entre los atletas de élite**.

![](./presentacion/img/inbox-zero/42.jpg)

Imagina que lanzas un guijarro a un estanque de aguas tranquilas. El agua responde de forma totalmente apropiada a la fuerza y la masa de la entrada; después recupera la calma. No tiene una reacción inapropiada, ni por exceso ni por defecto que nos domine y nos haga perder el control.

###  2.2. <a name='Gestionatusacciones'></a>Gestiona tus acciones

![](./presentacion/img/inbox-zero/32.jpg)

**Lo que está en la bandeja de entrada, por definición está pendiente**. Pendiente de definir qué hay que hacer con el email. Pendiente de decidir si puedes archivar o borrar el correo. Pendiente de contestar o delegar.

####  2.2.1. <a name='Borraroarchivar'></a>Borrar (o archivar)

![](./presentacion/img/intro/13.jpg)

Pregúntate a ti mismo: _¿Realmente voy a hacer algo respecto a este correo?_. _¿Cuántos mensajes permanecen semanas deambulando por nuestra bandeja de correo, para al final ser eliminados porque no servían?_. **NO TENGAS MIEDO A BORRAR**.

No sólo se trata de anuncios de Viagra, correos promocionales o suscripciones y esas cosas, una vez que cojas velocidad te asombrará de cuantos puedes tirar una vez pierdes el miedo. Retíralos de tu camino tan pronto como sea posible.

Nada más ver el “Asunto” puedes intuir de qué se trata. ¿Son necesarios? Pierde el miedo a borrar, **evita el síndrome de Diógenes digital**. En el caso que algo pudiera ser necesario en el futuro, archivarlo.

**A tomar en cuenta**:

* ¿Qué mails son innecesarios? SPAM, boletines o newsletters que ya no te interesan o que no lees, publicidad no solicitada o consentida. Emails que con el paso del tiempo han quedado “obsoletos”.
* ¿Cómo evitar que lleguen a mí? Darse de baja, bloquear, señalar como “correo no deseado”, adoptar medidas para que no lleguen en el futuro: por ejemplo, tener más criterio a la hora de solicitar el alta en newsletter o RSS o
creación de alertas...
* ¿Generamos suciedad? ¿Enviamos adjuntos de gran tamaño a varios destinatarios?

Si son cosas que necesitarás en el futuro puedes archivarlas, **archivar sólo es una carpeta y no veinte**, es quitarlo de tu vista, no es necesario crear una taxonomía de etiquetas imposible de manejar (**no debo perder el tiempo pensando dónde va**, no eres un bibliotecario), debes pensar en una taxonomía mínima como para poder encontrarlo de forma fácil. Además **¡Tenemos las búsquedas en GMail! Simplemente debe ser fácil de encontrar**.

####  2.2.2. <a name='Delegar'></a>Delegar

Delegar es obvio, el privilegio de delegar no sólo corresponde a mandos y responsables. A veces alguien puede hacer mejor algo o directamente no te concierne a ti hacerlo.  Si estás aplicando GTD tal vez quieras hacer un seguimiento, o añadir un recordatorio en unos días para comprobar si la otra persona ha hecho algo.

####  2.2.3. <a name='Responder'></a>Responder

Cualquier mensaje que llegue y te tome 2 minutos o menos para atenderlo hazlo. ¡Aprender a responder rápidamente en pocas palabras es un movimiento Ninja! Puedes proponerte por ejemplo responder siempre en menos de 6 líneas siempre, debe ser conciso y eficiente.
Cuando acabes archiva el correo.

> “If any man wishes to write a clear style let him first be clear in his thoughts.” - Johann Wolfgang Von Goethe.

##### Modelo P.A.S.S

* **P**: What is the **PURPOSE** of this communication?
	* Alinearlo con un objetivo comprensible. 
	* Hazte la siguiente pregunta: ¿Qué resultado quieres producir con el mensaje?.
	* Cómo sugerencia es mejor que escribas primero el contenido del mensaje para ayudarte a clarificar el asunto y definir los campos To, Cc y Bcc. 
* **A**: What **ACTION** is required? Does it have a due date?.
* **S**: Supporting **DOCUMENTATION**. Información de soporte: Un documento, un enlace, etc.
* **S**: Does the **SUBJECT** line effectively SUMMARIZE the message?

**¿Usas las opciones To, Cc y Bcc de forma efectiva?** 

El exceso de comunicación se valora hoy día más que el silencio y pensar sobre las cosas y mandar un correo a veces se usa como reemplazo de tomar la responsabilidad y reflexionar. Como resultado, **muchas organizaciones están paralizadas por el botón CC, la opción de responder a todos y otra serie de malos hábitos**.

![](./presentacion/img/inbox-zero/45.jpg)

Hay algunos mensajes, que invitan a la conversación. **Evita el pin pon de correos y si es necesario**, cambia a otra vía de comunicación más eficaz, o si es necesario programa el envío de los correos.

Usa las plantillas.

##### Cómo escribir un correo electrónico

* El asunto debe permitir ver de qué se trata, claro, conciso, relevante en referencia al contenido. No usar:  “urgente”, “no funciona”, “WiFi” etc,. (ejemplos reales).
* Adjuntos VS. **Enlaces**
* Conversación VS. **Medio comunicación asíncrona.**
* Responder a la información que se pide, aporte de valor en la cadena.

####  2.2.4. <a name='Aplazarlo'></a>Aplazarlo

Si estás realizando un chequeo rápido de tu correo electrónico seguramente te encuentres algunos en los que no quieres trabajar ahora mismo pero sí más tarde. Tal vez necesites información extra o realizar alguna tarea antes de responder, lo podemos colocar en una carpeta “pendiente de respuesta”.
Si necesitas averiguar qué es lo que tienes que hacer hazlo, pero  procesalo, sácalo del camino. 

####  2.2.5. <a name='Hacerlo'></a>Hacerlo

Finalmente. si hay algo que deba hacerse hazlo. Si es algo que no puedes o no debes hacer ahora ponlo en un calendario (por ejemplo una reunión en el futuro).

Es increíble cuanta gente decide lo que va a hacer en el día revisando su correo, libera actividad de tu bandeja de entrada. Mantén una lista separada para las tareas, podemos usar las tareas de Google (o Google Keep mediante notas) o aplicaciones externas.

* [Todoist](https://todoist.com/es).
* [Remember The Milk](https://www.rememberthemilk.com/).
* [Notion](https://www.notion.so/).
* [Trello](https://trello.com/es).
* [Any.Do](https://www.any.do/).
* [MS To Do](https://todo.microsoft.com/tasks/es-es/).

Independientemente del cliente de correo que use prefiero confiar en aplicaciones que sean gestores de tareas en si mismas y que se integren con el correo electrónico para crear nuevas tareas por ejemplo.

**No uses la bandeja de entrada como una lista de tareas o te volverás loco**, además las tareas no sólo llegan por correo electrónico (evita mantener dos o más sistemas paralelos para gestionar las acciones), sólo debemos hacer alguna de las 5 acciones.

La bandeja de entrada es como tu habitación, un día dejas un pantalón tirado, al siguiente una sudadera, etc.  Revisa con cierta regularidad o hazlo al momento (no la dejes en el suelo).

**Tienes que averiguar por ti mismo y adaptar este sistema si es necesario a tus flujos de trabajo, tienes tu propia vida y tus propios hábitos.**

###  2.3. <a name='Elhbitodeprocesar'></a>El hábito de procesar

> «Somos lo que hacemos repetidamente. La excelencia, entonces, no es un acto, sino un hábito» - (Will Durant).

No seas ese tipo que sube y baja en la bandeja de entrada todo el día pensando sobre dónde pones las cosas. Trata de minimizar cualquier tipo de actividad que no apoye directamente la creación y gestión.  Si realmente no está haciendo cosas no está trabajando. Estás jugando con el correo electrónico.

**La productividad personal es  99% hábitos, 1% herramienta.**

Es importante enviar menos correos electrónicos. Eso no quiere decir que los tengas abiertos o que no respondas, considera cerrar el correo electrónico. Si estás atendiendo o cara al público tal vez tu trabajo no te lo permita, si eres el responsable del sistema de misiles coreanos no puedes hacer eso. En otros casos cierra el correo y deja que se acumule mientras haces otros trabajos.  

¿Alguna vez te ha pasado llegar de vacaciones y tener 700 correos electrónicos? Lo que no procesamos no deja de rondarnos la cabeza y nos viene una y otra vez.  Eso supone interrupciones que se acumulan y que pueden suponer muchas horas semanales, ¿cuántas de esas interrupciones y distracciones podemos tolerar? Procesa el correo una vez cada hora, tardas 10 minutos, responde a la que puedas, cierralo y vuelve tu trabajo. Tanto como puedas intenta desconectarte y alejarte de eso. Una de las cosas que te darás cuentas es que tienes menos impresión de estar apagando fuegos que cuando lo tenías todo el rato abierto y recibiendo correos sin cesar.

¿Pero qué pasa si leemos el correo una vez a la hora pero sin embargo esperan que respondamos en 30 segundos? Deberíamos pensar quien tiene un problema con eso. ¿Qué podemos hacer para solucionar eso? Nunca llamarías a casa de nadie un domingo a las 23:00 de la noche, pero ahora debido a que tu correo electrónico está disponible 24x7 las personas sienten que tienen acceso ilimitado para ti.

Si descubres que el 80% del correo proviene de las mismas 10 respuestas plantéate usar plantillas de correo. Si descubres que estás respondiendo cosas parecidas una y otra vez trata de automatizar.

No dejes que tu correo electrónico te incordie todo el día, visita la bandeja de entrada cuando estés preparado.

Si no quieres estar todo el día enganchado con una cosa, si solo estás haciendo un trabajo meteórico dentro del correo realmente no estás haciendo nada. 

![](./presentacion/img/inbox-zero/44.jpg)

Pregúntate cada cierto tiempo, _¿Estoy enfocando mi atención donde debería ahora mismo? Estoy en el lugar mental donde se desarrolla la acción?_

_¿Hay alguna manera de que pueda hacer que este correo electrónico sea menos ruidoso para mi y genere más acciones?_

Establecer una cultura de equipo. En algunos sitios se reemplazan muchos correos por un CMS o una Wiki interna donde colgar contenidos que la gente sigue o no si está interesada. 

Seguramente no hay aplicaciones GTD,etc que lo hagan todo, en su lugar es mejor buscar aplicaciones que hagan bien lo que tienen que hacer, no quiero una navaja suiza que se rompe a la primera de cambio.

Es normal llegar un lunes y empezar apagando fuegos, en lugar de eso establece una rutina, yo primero reviso los sistemas y otras cosas, luego leo los correos y elimino la “basura” y luego veo que queda.
