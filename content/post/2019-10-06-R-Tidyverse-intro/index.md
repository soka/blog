---
title: Introducción a Tidyverse (1)
subtitle: Programación en R
date: 2019-10-06
draft: false
description: Ciencia de datos con R
tags: ["R","R - Tidyverse","R - Paquetes","R - DataExplorer","R - ggplot","R - Diagramas"]
---

![](images/hex-tidyverse.png)

[**Tidyverse**](https://www.tidyverse.org/) es un conjunto de paquetes para la ciencia de datos, todos los paquetes que lo componen comparten la misma filosofía de diseño, gramática y estructuras de datos.

## Instalación

```r
install.packages("tidyverse")
library(tidyverse)
```

![](images/01.png)

Si de detectan conflictos podemos deshabilitar un paquete con `detach("package:magrittr",unload = T)`. Otra forma para "desambiguar" una función es hacer referencia al paquete que contiene la función de forma explicita, por ejemplo `dplyr::mutate()`.

Compruebo si hay paquetes desactualizados: `tidyverse_update()` Actualizo algunos paquetes pasando sus nombres como un vector de entrada en una línea `install.packages(c("httr", "xml2"))`.

## Más sobre paquetes

En cualquier momento podemos consultar los paquetes instalados en nuestra computadora: `installed.packages()`.

Consultar datos sobre un paquete:

```r
packageDescription("tidyverse")
packageVersion("tidyverse")
help(package = "tidyverse")
```

Para desinstalar un paquete:

```r
remove.packages("vioplot")
```

Para obtener los paquetes que requieren actualizarse:

```r
old.packages()
```

Para actualizar todos los paquetes:

```r
update.packages()
```

## Análisis exploratorio de los datos

```r
View(mpg) # Invoca un visor de datos en una nueva pestaña de RStudio
?mpg # help(mpg)
```

Muestra los primeros 10 registros del _dataset_ (por defecto muestra 6):

```r
head(mpg,10)
```

Dimensiones del objeto

```r
> dim(mpg) 
[1] 234  11
> nrow(mpg)
[1] 234
> ncol(mpg)
[1] 11
```

La función `glimpse` del paquete dplyr:

```r
> glimpse(mpg)
Observations: 234
Variables: 11
$ manufacturer <chr> "audi", "audi", "audi", "audi", "audi", "audi", "audi", "audi", "…
$ model        <chr> "a4", "a4", "a4", "a4", "a4", "a4", "a4", "a4 quattro", "a4 quatt…
$ displ        <dbl> 1.8, 1.8, 2.0, 2.0, 2.8, 2.8, 3.1, 1.8, 1.8, 2.0, 2.0, 2.8, 2.8, …
$ year         <int> 1999, 1999, 2008, 2008, 1999, 1999, 2008, 1999, 1999, 2008, 2008,…
$ cyl          <int> 4, 4, 4, 4, 6, 6, 6, 4, 4, 4, 4, 6, 6, 6, 6, 6, 6, 8, 8, 8, 8, 8,…
$ trans        <chr> "auto(l5)", "manual(m5)", "manual(m6)", "auto(av)", "auto(l5)", "…
$ drv          <chr> "f", "f", "f", "f", "f", "f", "f", "4", "4", "4", "4", "4", "4", …
$ cty          <int> 18, 21, 20, 21, 16, 18, 18, 18, 16, 20, 19, 15, 17, 17, 15, 15, 1…
$ hwy          <int> 29, 29, 31, 30, 26, 26, 27, 26, 25, 28, 27, 25, 25, 25, 25, 24, 2…
$ fl           <chr> "p", "p", "p", "p", "p", "p", "p", "p", "p", "p", "p", "p", "p", …
$ class        <chr> "compact", "compact", "compact", "compact", "compact", "compact",…
```

La función `summary` es muy útil para analizar cada variable y algunos datos calculados si son numéricas:

```r
> summary(mpg)
 manufacturer          model               displ            year           cyl       
 Length:234         Length:234         Min.   :1.600   Min.   :1999   Min.   :4.000  
 Class :character   Class :character   1st Qu.:2.400   1st Qu.:1999   1st Qu.:4.000  
 Mode  :character   Mode  :character   Median :3.300   Median :2004   Median :6.000  
                                       Mean   :3.472   Mean   :2004   Mean   :5.889  
                                       3rd Qu.:4.600   3rd Qu.:2008   3rd Qu.:8.000  
                                       Max.   :7.000   Max.   :2008   Max.   :8.000  
    trans               drv                 cty             hwy       
 Length:234         Length:234         Min.   : 9.00   Min.   :12.00  
 Class :character   Class :character   1st Qu.:14.00   1st Qu.:18.00  
 Mode  :character   Mode  :character   Median :17.00   Median :24.00  
                                       Mean   :16.86   Mean   :23.44  
                                       3rd Qu.:19.00   3rd Qu.:27.00  
                                       Max.   :35.00   Max.   :44.00  
      fl               class          
 Length:234         Length:234        
 Class :character   Class :character  
 Mode  :character   Mode  :character
 ```

Para generar un resumen automático en HTML [DataExplorer](https://www.rdocumentation.org/packages/DataExplorer/versions/0.8.0) hace un excelente trabajo aplicando varias observaciones estadísticas y diagramas.

```r
install.packages("DataExplorer")
library(DataExplorer)
DataExplorer::create_report(mpg)
```

## Visualización de datos con ggplot2

Genero un [diagrama de dispersión](https://es.wikipedia.org/wiki/Diagrama_de_dispersi%C3%B3n) de puntos (_scatter plot_) que muestra la relación negativa entre el tamaño del depósito y la eficiencia (millas que recorren por un galón) del combustible (a mayor tamaño menor eficiencia).

```r
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ,y = hwy))
```

![](images/02.png)

La función [`geom_point`](https://ggplot2.tidyverse.org/reference/geom_point.html) no es la única que podemos crear con ggplot2, si introducimos "geom_" RStudio despliega un montón de funciones geométricas para crear representaciones gráficas. El argumento `mapping` indica como los datos del _dataset_ van a ser traducidos o mapeados a elementos visuales, el argumento mapping siempre ira relacionado con la función [`aes`](https://ggplot2.tidyverse.org/reference/aes.html) (_aesthetic_ o estética) para indicar en este caso en que eje se representa cada variable. A partir de aquí es muy util definir unos _templates_ o recetas para generar nuestros gráficos.

Siguiendo con el ejemplo anterior voy a introducir una tercera vari able con el tipo de vehículo (`class`) que se representará como el color del punto en el _scatter plot_:

```r
ggplot(data = mpg)+
  geom_point(mapping = aes(x=displ,y=hwy,colour=class))
```

![](images/03.png)

Podemos crear otras visualizaciones, en vez de representar con un color cada tipo de coche podemos usar el tamaño del punto para diferenciarlos:

```r
ggplot(data = mpg)+
  geom_point(mapping = aes(x=displ,y=hwy,size=class))
```

Se produce una advertencia o _warning_ porque estamos usando valores no [discretos](https://es.wikipedia.org/wiki/Variable_discreta_y_variable_continua) para una variable _size_ que espera sea numérica.

![](images/04.png)

Podemos jugar con la transparencia o opacidad de los puntos usando el parámetro `alpha`:

```r
ggplot(data = mpg)+
  geom_point(mapping = aes(x=displ,y=hwy,alpha=class))
```

![](images/05.png)

Podemos jugar con la forma de los puntos con el parámetro `shape`:

```r
ggplot(data = mpg)+
  geom_point(mapping = aes(x=displ,y=hwy,shape=class))
```

Produce un _warning_ porque gglot sólo puede usar 6 tipos de puntos.

![](images/06.png)

## Selección manual de estéticas

Fuera del mapeo de la estética defino el color de los puntos:

```r
ggplot(data = mpg)+
  geom_point(mapping = aes(x=displ,y=hwy),color="red")
```

![](images/07.png)

Lo mismo que el `color` podemos jugar con el tamaño `size` o la forma de los puntos con [`shape`](ggplot2 Quick Reference: shape) representable con números del 1 al 24.

Ahora si mezclamos todo lo aprendido:

```r
ggplot(data = mpg)+
  geom_point(mapping = aes(x=displ,y=hwy),shape=23,color="red",size=10,fill="yellow")
```

![](images/08.png)

## Los facets de ggplot

Otro modo adicional del uso de las estéticas es añadir _facets_ o subconjuntos de diagramas. La variable que usamos para segmentar debe ser [discreta](https://es.wikipedia.org/wiki/Variable_discreta_y_variable_continua) (no acepta cualquier valor, únicamente aquellos que pertenecen al conjunto), en el siguiente ejemplo creo un diagrama para cada tipo de coche:

```r
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy)) +
  facet_wrap(~class,nrow=2)
```

[`facet_grid`](https://www.rdocumentation.org/packages/ggplot2/versions/3.2.1/topics/facet_grid) forma una matriz de paneles formada en este caso por las combinaciones de las variables `drv` (tracción delantera 'f', trasera 'r' o 4x4 '4') y `cyl` (número de cilindros).

![](images/09.png)


```r
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy)) +
  facet_grid(drv~cyl)
```  

Distribuido por una única variable como `.~cyl`:

```r
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy)) +
  facet_grid(.~cyl)
```

![](images/10.png)

## Geometrías con ggplot

Hasta ahora hemos visto la geometría de puntos con [`geom_point`](https://ggplot2.tidyverse.org/reference/geom_point.html) pero ggplot ofrece otras formas de representación, por ejemplo [`geom_bar`](https://ggplot2.tidyverse.org/reference/geom_bar.html), [`geom_line`](https://ggplot2.tidyverse.org/reference/geom_path.html) o `geom_boxplot`.

Un ejemplo, [`geom_smooth`](https://ggplot2.tidyverse.org/reference/geom_smooth.html) ayuda de identificar el patrón de forma visual:

```r
ggplot(data=mpg) +
  geom_smooth(mapping = aes(x=displ,y=hwy))
```  

Resultado:

![](images/11.png)

Si queremos dividir las rectas por cada tipo de coche:

```r
ggplot(data=mpg) +
  geom_smooth(mapping = aes(x=displ,y=hwy,linetype = drv))
```

![](images/12.png)

```r
ggplot(data=mpg) +
  geom_smooth(mapping = aes(x=displ,y=hwy,linetype = drv,color =drv))
```

![](images/13.png)

Combinando puntos y líneas:

```r
ggplot(data=mpg) +
  geom_point(mapping = aes(x=displ,y=hwy,color=drv)) + 
  geom_smooth(mapping = aes(x=displ,y=hwy,linetype = drv,color =drv))
```

![](images/14.png)

Esta guia rápida [ggplot2-cheatsheet-2.1-Spanish.pdf](./ggplot2-cheatsheet-2.1-Spanish.pdf) ofrece una visión del potencial de ggplot para crear diagramas.

## Código fuente

- r / intro / 56-Tidyverse / [Tidyverse.R](https://gitlab.com/soka/r/blob/master/intro/56-Tidyverse/Tidyverse.R).

## Enlaces internos

- [ggplot2-cheatsheet-2.1-Spanish.pdf](./ggplot2-cheatsheet-2.1-Spanish.pdf).
- ["Paquetes en R - Como instalarlos y usarlos"](https://soka.gitlab.io/blog/post/2019-01-30-paquetes-en-r/).
- [Artículos sobre ggplot2](https://soka.gitlab.io/blog/tags/r-ggplot2/) en mi blog.

## Enlaces externos

- datacamp.com ["R Packages: A Beginner's Guide "](https://www.datacamp.com/community/tutorials/r-packages-guide): Excelente introducción.
- ggplot2.tidyverse.org ["geom_point"](https://ggplot2.tidyverse.org/reference/geom_point.html).
- ggplot2.tidyverse.org ["Construct aesthetic mappings"](https://ggplot2.tidyverse.org/reference/aes.html).
- ["ggplot2 Quick Reference: shape"](http://sape.inf.usi.ch/quick-reference/ggplot2/shape).
- ["One Page R"](https://togaware.com/onepager/).
- [joanby/tidyverse-data-science](https://github.com/joanby/tidyverse-data-science).

![](images/Pic_2.png)