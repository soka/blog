---
title: Data Frames en R
subtitle: Tipos de datos
date: 2019-03-12
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico", "R - Datos", "R - Data.Frame"]
---

![](images/r-logo.PNG)

Los **DF (Data Frame)** son muy parecidos a las matrices, tienen filas y columnas y por lo tanto son bidimensionales, pero a diferencia de las matrices cada columna puede almacenar diferentes tipos de datos. Los DF son la mejor forma de almacenar datos donde cada fila representa una una unidad de información (por ejemplo datos de personas de un fichero CSV importado).

**R** tiene una función para importar el contenido de un fichero de texto plano directamente a un DF usando la función `read.table()` pasandole como parámetro la ruta del archivo, el carácter separados de campos y otros parámetros.

## Importar un DF

```R
df1 <- read.table("sample.csv",sep = ";",row.names = 1)
```

## Crear un DF

```R
> v1 <- c(1,2,3,4)
> v2 <- c("uno","dos","tres","cuatro")
> df2 <- data.frame(valor=v1,caso=v2)
> df2
  valor   caso
1     1    uno
2     2    dos
3     3   tres
4     4 cuatro
```

En RStudio si pinchamos sobre la variable en la ventana de entorno de variables se abre una nueva pestaña donde podemos visualizar el DF en forma de tabla:

![](images/df-preview-rstudio.PNG)

## Índices como en matrices y nombres como en listas...

Se pueden usar índices numéricos para seleccionar elementos.

Seleccionamos la primera fila:

```R
> df2[1,]
  valor caso
1     1  uno
```

Y ahora la segunda columna:

```R
> df2[,2]
[1] uno    dos    tres   cuatro
Levels: cuatro dos tres uno
```

Acceder a una columna por su nombre:

```R
> df2$valor
[1] 1 2 3 4
```

Nombres de columnas:

```R
> df2[0,] # Nombre de columnas
[1] valor caso
<0 rows> (or 0-length row.names)
```

un rango de filas:

```R
> df2[1:3,]
  valor caso
1     1  uno
2     2  dos
3     3 tres
```

![](images/dataframe-cheatsheet.PNG)

## Propiedades

Dimensiones (filas y columnas):

```R
> dim(df2)
[1] 4 2
```

Resumen estadístico de las columnas:

```R
> summary(df2)
     valor          caso
 Min.   :1.00   cuatro:1
 1st Qu.:1.75   dos   :1
 Median :2.50   tres  :1
 Mean   :2.50   uno   :1
 3rd Qu.:3.25
 Max.   :4.00
```

## Añadir nueva columna

Para añadir una nueva columna usamos la función `cbind()`:

```R
v1 <- c(1,2,3,4)
v2 <- c("uno","dos","tres","cuatro")
df1 <- data.frame(valor=v1,caso=v2)
df2 <- c(1:4)
df1 <- cbind(df1,df2)
head(df1)
```

![](images/df-preview-rstudio-01.PNG)

## Añadir una nueva fila

```R
fila <- data.frame(valor=5,caso="cinco",df2=5)
df1 <- rbind(df1,fila)
```

![](images/df-preview-rstudio-02.PNG)

## Subset o subconjunto de un data.frame

Obtenemos la segunda columna de las dos primeras filas:

```R
> df1[c(1,2),c(2)]
[1] uno dos
Levels: cuatro dos tres uno cinco
```

Podemos hacer lo mismo por el nombre de columna:

```R
> df1[c(1,2),c('valor','caso')]
  valor caso
1     1  uno
2     2  dos
```

## Nombres de las columnas

```R
> names(df1)
[1] "valor" "caso"  "df2"
```

Si quisiesemos obtener solo algunos nombres de columnas:

```R
> names(df1)[c(2,3)]
[1] "caso" "df2"
```

Si quisiesemos cambiar los nombres de las columnas:

```R
> names(df1)[3] <- 'cantidad'
> df1
  valor   caso cantidad
1     1    uno        1
2     2    dos        2
3     3   tres        3
4     4 cuatro        4
5     5  cinco        5
```

## Ejercicios

- r / intro / 22-Data-Frame / [DataFrame.R](https://gitlab.com/soka/r/blob/master/intro/22-Data-Frame/DataFrame.R).

## Enlaces

- ["Añadir una nueva columna cbind()"](https://soka.gitlab.io/blog/post/2019-03-01-ainadir-nueva-columna-df-en-r/).
