---
title: Verfificar una contraseña de dominio
subtitle: Scripting PowerShell en Active Directory
date: 2019-02-22
draft: false
description: 
tags: ["PS","PowerShell","Scripting","Active Directory","Windows","MS","Domain"]
---

Con **PowerShell** es muy sencillo verificar si la contraseña de un equipo corresponde con la del dominio **AD (Active Directory)**, el cmdlet retorna un valor lógico $True si es correcta, o $False en caso contrario. El cmdlet no funciona con cuentas locales, obtiene el nombre de usuario con el que estamos logeados en el equipo donde se ejecuta el cmdlet y el nombre del dominio de las variables de entorno, a continuación solicita la contraseña y la comprueba usando la función `ValidateCredentials()`.

```
$UserDomain = $env:USERDOMAIN
$UserName = $env:USERNAME
$Password = Read-Host -Prompt "Enter password to test"

# test password
Add-Type -AssemblyName System.DirectoryServices.AccountManagement 
$ContextType = [System.DirectoryServices.AccountManagement.ContextType]::Domain
$PrincipalContext = [System.DirectoryServices.AccountManagement.PrincipalContext]::new($ContextType, $UserDomain)
$PrincipalContext.ValidateCredentials($UserName,$Password)
```

**cmdlet**: PSFabrik / docs / src / active_directory /users / [VerifyADPwd.ps1](https://gitlab.com/soka/PSFabrik/blob/master/docs/src/active_directory/users/VerifyADPwd.ps1).


# Enlaces externos

* ["Powershell - Windows Environment variables - PowerShell - SS64.com"](https://ss64.com/ps/syntax-env.html).
* ["PrincipalContext.ValidateCredentials Method (System ... - Microsoft Docs"](https://docs.microsoft.com/en-us/dotnet/api/system.directoryservices.accountmanagement.principalcontext.validatecredentials?view=netframework-4.7.2).