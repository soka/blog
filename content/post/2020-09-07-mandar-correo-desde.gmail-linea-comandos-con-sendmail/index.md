---
title: Como mandar un correo electrónico desde la línea de comandos
subtitle: Sendemail en Debian y una cuenta de GMail como remitente
date: 2020-09-07
draft: false
description: Sendmail
tags: ["Comandos","Sendemail","Correo electrónico","Linux","Debian","Mail","Gmail"]
---

Cada vez que tengo que demostrar a alguna empresa de desarrollo Web externa como envíar un correo electrónico con una cuenta de Gmail corporativa como remitente me vuelvo loco reinventando la rueda, yo sólo tengo que demostrar que funciona y a continuación proporcionarles los datos de la cuenta para que automaticen el envión de correos. Dejo por escrita una de mis soluciones usando [Sendemail](https://packages.debian.org/sid/mail/sendemail) en Debian, no me voy a liar en nada más, el comando funciona y listo.

La instalación de Sendemail es trivial `# apt-get install sendemail`.

Y ahora el comando para mandar un correo:

```bash
echo "TEST" | sendemail -l email.log   \
    -f "remitente@gmail.com"   \
    -u "Asunto correo"     \
    -t "destinatario@domain.com" \
    -s "smtp.gmail.com:587"  \
    -o tls=yes \
    -xu "remitente@gmail.com" \
    -xp "Contraseña correo"
```

En la carpeta donde se ejecuta el comando deja el fichero "email.log" con la traza de la ejecución.

Recordatorio mental, **bajar el nivel de seguridad en la cuenta de Gmail del remitente**.

![](img/01.png)

Para Windows funciona muy bien [SwithMail](https://sourceforge.net/projects/swithmail/)

Agur.
