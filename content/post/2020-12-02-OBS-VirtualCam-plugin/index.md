---
title: Usar OBS como una cámara virtual (Windows)
subtitle: OBS-VirtualCam para OBS Studio
date: 2020-12-02
draft: false
description: Controla la fuente de la Webcam desde OBS Studio y envíala a una aplicación de videoconferencia
tags: ["OBS Studio","OBS","OBS - Plugins","OBS WebCam","Videoconferencías","Streaming","Vídeo","Aplicaciones"]
---

![](img/obs-logo-01.png)

Este plugin para OBS Studio es realmente ingenioso, crea una cámara virtual en nuestro sistema, el funcionamiento es el siguiente, desde OBS capturamos la Webcam y componemos la imagen como siempre, añadimos otros elementos entre las fuentes, una presentación o cualquier ventana de Windows que queramos mostrar, **desde nuestra aplicación de videoconferencia favorita en lugar de seleccionar la Webcam real seleccionamos la nueva cámara virtual y nos llega la composición de OBS Studio**.

Lo primero es descargar el plugin para Windows de [este enlace](https://obsproject.com/forum/resources/obs-virtualcam.949/) e instalarlo, en el momento de escribir este post la versión del plugin es 2.0.5.

Una vez instalado el primer paso es ir al menu herramientas de OBS Studio **Tools -> VirtualCam** y presionar Start, después podemos cerrar la ventana.

![](img/01.jpg)

Ahora en nuestra aplicación de videoconferencías "favorita" (perdón por usar Zoom) seleccionamos "OBS-Camera" como fuente de vídeo:

![](img/02.jpg)

Ahora en OBS Studio podemos componer nuestro vídeo usando diferentes fuentes, nuestra imagen de la Webcam, una ventana activa que queramos mostrar con una presentación o cualquier otro elemento.

![](img/03.jpg)

Si queremos montar algún tipo de charla o taller en línea podemos crear una escenario de fondo con algun título o logos de empresa por ejemplo:

![](img/04.jpg)

## Enlaces internos

- [OBS Studio - Software libre de grabación y transmisión de vídeo en vivo](https://soka.gitlab.io/blog/post/2020-04-30-obs-1/).