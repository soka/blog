---
title: Generador claves
subtitle: Cmdlet Powershell para generar contraseñas seguras
date: 2019-04-10
draft: true
description: PowerShell cmdlets
tags: ["PowerShell", "PS", "Scripting", "Seguridad", "Cmdlet"]
---

C:\Users\i.landajuela\Documents\GitLab\PSFabrik\docs\src\utils\pwd_generator

New-SuperSimplePwdGenerator.ps1

https://community.idera.com/database-tools/powershell/powertips/b/tips/posts/super-simple-random-password-generator

# Enlaces internos

- [soka.gitlab.io/PSFabrik/](https://soka.gitlab.io/PSFabrik/): Web estática sobre PS.
- ["Windows Management Instrumentation (WMI)"](https://soka.gitlab.io/PSFabrik/networking/wmi/wmi-intro/): Post sobre WMI en PSFabrik.

# Enlaces externos

- ["Remote Computer Inventory with PowerShell"](https://www.signalwarrant.com/remote-computer-inventory-powershell/): **Fuente de este artículo**.
