---
title: Manipulación de cadenas en R
subtitle: Fundamentos de R
date: 2019-08-11
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Básico", "R - Funciones", "R - Cadenas"]
---

![](images/documentation-where-were-going-we-dont-need-documentation.jpg)

## grep(): Búsqueda y reemplazo básado en patrones

Un ejemplo muy sencillo pasando un vector de _strings_ a evaluar y la expresión "pattern" para buscar coincidencias.

```r
> txt <- c("arm","foot","lefroo", "bafoobar","pattern","cpu")
> grep("pattern",txt)
[1] 5
```

Este ejemplo es algo más avanzado y permite ver el juego que puede dar para hacer búsquedas avanzadas usando expresiones regulares con `perl=TRUE` (ver ["Perl - Regular Expressions - Tutorialspoint"](https://www.tutorialspoint.com/perl/perl_regular_expressions)), `+` busca una o más ocurrencias de la letra 'b'.

```r
> grep("b+", c("abc", "bda", "cca a", "abd","bb","bab","b"), perl=TRUE, value=TRUE)
[1] "abc" "bda" "abd" "bb"  "bab" "b"
```

Si `value=FALSE` retorna el índice de los elementos que coincidan, si es `TRUE` devuelve los contenidos, muy interesante.

```r
> grep("b+", c("abc", "bda", "cca a", "abd","b"), perl=TRUE, value=TRUE) # [1] 1 2 4 5
[1] "abc" "bda" "abd" "b"
```

## nchar(): Longitud de una cadena

```r
> str <- "Hola mundo!"
> nchar(str)
[1] 11
```

## paste(): Concatenar cadenas

```r
> paste("Hola"," ","mundo!")
[1] "Hola   mundo!"
```

## sprintf(): Formateo cadenas estilo C

```r
> sprintf("Hola mundo %s y a sus %d soles", "Nostromo",7)
[1] "Hola mundo Nostromo y a sus 7 soles"
```

## substr(): Obtiene o reemplaza subcadenas

```r
str <- "Hola mundo!"
substr(str, 1, 4) # [1] "Hola"
substr(str, 6, 10) # [1] "mundo"
```

## Código fuente

- r / intro / 48-manipulacion-de-cadenas / [manipulacion-de-cadenas.R](https://gitlab.com/soka/r/blob/master/intro/48-manipulacion-de-cadenas/manipulacion-de-cadenas.R).

## Enlaces internos

- ["Tipos de datos básicos en R"](https://soka.gitlab.io/blog/post/2019-03-03-tipos-de-datos-r/).

## Enlaces externos

- Fuente: [https://data-flair.training/blogs/r-string-manipulation](https://data-flair.training/blogs/r-string-manipulation).
