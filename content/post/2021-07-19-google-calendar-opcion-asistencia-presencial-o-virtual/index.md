---
title: Google Calendar permite especificar si asistirás virtualmente o de forma presencial
subtitle: Nueva opción RSVP para las opciones de asistencia
date: 2021-07-19
draft: false
description: 
tags: ["Google","Calendar","Updates","Actualizaciones","Google Workspace"]
---

Google ha lanzado una nueva funcionalidad para ofrecer mayor flexibilidad en entornos de trabajo híbribidos, ahora **permite especificar si asistiremos a un evento de forma presencial o virtual**. Por el momento sólo está disponible si accedemos dentro de Google Calendar cuando respondemos a una invitación.

![](img/01.gif)

A pesar de que es un pequeño cambio puede resultar de gran utilidad al organizador para anticiparse y adaptar el formato de una reunión.   

## Enlaces internos

* Artículos con etiqueta Google [https://soka.gitlab.io/blog/tags/google/](https://soka.gitlab.io/blog/tags/google/).

## Enlaces externos

* Indicate whether you’ll join a meeting virtually or in person on Google Calendar [https://workspaceupdates.googleblog.com/2021/07/join-meeting-virtually-or-in-person-google-calendar.html](https://workspaceupdates.googleblog.com/2021/07/join-meeting-virtually-or-in-person-google-calendar.html).

