---
title: Hackathon 4 - SWAPI The Star Wars API
subtitle: API REST interactuando con servicios Web
date: 2022-07-24
draft: false
description: API REST interactuando con servicios Web
tags: ["Python","Retos","Aplicaciones","Juegos","ASCII","Hackathon","Dungeon crawl","Proyectos"]
---


<!-- vscode-markdown-toc -->
* 1. [Descripción del reto](#Descripcindelreto)
* 2. [Creación del programa paso a paso](#Creacindelprogramapasoapaso)
	* 2.1. [Clase SWAPI básica](#ClaseSWAPIbsica)
	* 2.2. [Métodos de la clase](#Mtodosdelaclase)
		* 2.2.1. [Método genérico para llamadas GET (get_data)](#MtodogenricoparallamadasGETget_data)
		* 2.2.2. [Método para obtener información del catálogo (get_root)](#Mtodoparaobtenerinformacindelcatlogoget_root)
		* 2.2.3. [Obtener información de todos los personajes (get_all_people)](#Obtenerinformacindetodoslospersonajesget_all_people)
		* 2.2.4. [Buscar un personaje por nombre (get_people_by_name)](#Buscarunpersonajepornombreget_people_by_name)
		* 2.2.5. [Informes y resúmenes (get_people_statistics)](#Informesyresmenesget_people_statistics)
		* 2.2.6. [Obtener las películas en las que ha participado un personaje](#Obtenerlaspelculasenlasquehaparticipadounpersonaje)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


![](img/01.png)

##  1. <a name='Descripcindelreto'></a>Descripción del reto

Somos super fans del universo Star Wars, queremos crear un programa que use la API JSON ([https://swapi.dev/](https://swapi.dev/)) con información del universo SW y nos permita trabajar con estos datos.

Al final de este artículo se han recopilado enlaces de interés sobre temas diversos que te pueden ayudar.

##  2. <a name='Creacindelprogramapasoapaso'></a>Creación del programa paso a paso

###  2.1. <a name='ClaseSWAPIbsica'></a>Clase SWAPI básica

La URL base ([https://swapi.dev/api/](https://swapi.dev/api/)) para todas las consultas estará almacenada en una **constante** `API_BASE_URL` en la cabecera del programa.

Toda nuestra aplicación estará contenida en una **clase llamada `SWAPI`**, nuestro primer **método** se llamará `test_connection` y realizará lo siguiente: Una llamada con el método GET a la URL base. El método deberá controlar las excepciones posibles, al menos cuando se produce un [error de conexión](https://requests.readthedocs.io/en/latest/api/#requests.ConnectionError) (`ConnectionError`), si no se produce un error de conexión además deberemos comprobar que el código de estado HTTP devuelto por la llamada sea OK (200), en lugar de comprobar el código 200 _hardcoded_ o usando constantes definidas en nuestro programa la librería requests incluye estos códigos, por ejemplo 200 corresponde a `requests.codes.ok`. 

**El método debe retornar** `False` si se produjo una excepción o si no obtienes el código de respuesta 200, en caso contrario retornará `True`.

Puedes probar este primer paso creando una instancia de esta clase básica `SWAPI` y llamando al método para comprobar que no tienes ningún problema de red.

###  2.2. <a name='Mtodosdelaclase'></a>Métodos de la clase

####  2.2.1. <a name='MtodogenricoparallamadasGETget_data'></a>Método genérico para llamadas GET (get_data)

Vamos a programar un método genérico `get_data` para llamadas GET que nos será de gran utilidad para el resto de llamadas para obtener datos. **El método recibirá un sólo parámetro**, la `url` que debemos llamar. El método retornará `False` si se produce algún problema (como en el método `test_connection`), en caso contrario **retornará la respuesta sin tratar en un diccionario** (convertir la respuesta en diccionario y retornar la información).

####  2.2.2. <a name='Mtodoparaobtenerinformacindelcatlogoget_root'></a>Método para obtener información del catálogo (get_root)

La API ofrece la información agrupada en los siguientes apartados ([Api Root](https://swapi.dev/api/)):

* [people](https://swapi.dev/api/people/)
* [planets](https://swapi.dev/api/planets/).
* [films](https://swapi.dev/api/films/).
* [species](https://swapi.dev/api/species/)
* [vehicles](https://swapi.dev/api/vehicles/)
* [starships](https://swapi.dev/api/starships/)

Queremos obtener los elementos de arriba junto con la URL asociada e imprimirlos, para ello crearemos un método nueva en la clase `get_root` que devolverá un diccionario con todas las opciones, ahora es el momento de usar el método `get_data` que hemos creado antes.

![](img/06.png)

####  2.2.3. <a name='Obtenerinformacindetodoslospersonajesget_all_people'></a>Obtener información de todos los personajes (get_all_people)

Ahora vamos a crear un método que retorne la información de todos los personajes. Para obtener todos los personajes tendremos que hacer un trabajito especial, la información está paginada y debemos obtener la información e ir juntandola poco a poco realizando varias llamadas GET, si llamamos a [https://swapi.dev/api/people](https://swapi.dev/api/people) saldrá algo así:

![](img/02.png)

La clave `next:"https://swapi.dev/api/people/?page=2"` nos dirá la siguiente URL a la que debemos llamar para obtener los siguientes personajes, seguiremos realizando llamadas hasta que el parámetro "next" sea "null" (`None` en Python) como en la captura inferior:

![](img/03.png)

La información que retorna el método debe ser una lista compuesta de diccionarios con cada personaje, es decir, no queremos el resto de datos que retorna la llamada ("count","next","previous"), sólo el contenido de los resultados.

####  2.2.4. <a name='Buscarunpersonajepornombreget_people_by_name'></a>Buscar un personaje por nombre (get_people_by_name)

Este método casi se explica por si mismo, recibe como argumento el nombre de un personaje (ejemplo: "R2-D2") y retornará su información.

####  2.2.5. <a name='Informesyresmenesget_people_statistics'></a>Informes y resúmenes (get_people_statistics)

Queremos crear un método `get_people_statistics` que calcule datos de los personajes y nos muestre un resumen de los mismos imprimido por pantalla.

##### Masa total

La masa ("mass") total sumada de todos los personajes. Advertencia: de algunos personajes no se conoce la masa ("unknown"), la masa está expresada con el separador decimal ",", antes de convertir en _float_ deberías reemplazar la "," por "." (método `replace` de cadenas).

##### Altura media

La altura media ('height') de los personajes. Como en el caso de arriba también podemos tener personajes de los que no conocemos la altura.  

####  2.2.6. <a name='Obtenerlaspelculasenlasquehaparticipadounpersonaje'></a>Obtener las películas en las que ha participado un personaje (get_people_films)

Ahora queremos obtener la información de las películas en las que ha participado un personaje, el método `get_people_films` recibirá como argumento el ID de un personaje que usaremos para realizar una llamada GET, por ejemplo el ID 1 (Luke Skywalker) [https://swapi.dev/api/people/1/](https://swapi.dev/api/people/1/) retorna esta información:

![](img/04.png)

Debemos recorrer la lista de "films" de la respuesta y realizar una llamada a cada URL.

[https://swapi.dev/api/films/1/](https://swapi.dev/api/films/1/)

![](img/05.png)

De cada película en la que ha participado sólo queremos saber su título, el método retornará una lista con todos los títulos.

##  3. <a name='Enlacesexternos'></a>Enlaces externos

**SWAPI**:

* [https://swapi.dev/](https://swapi.dev/)
* [https://swapi.dev/documentation](https://swapi.dev/documentation)

**HTTP**:

* [https://developer.mozilla.org/en-US/docs/Web/HTTP/Status](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

**Requests**:

* General:
	* [https://realpython.com/python-requests/](https://realpython.com/python-requests/)

* Gestión de errores:
    * [https://requests.readthedocs.io/en/latest/api/#requests.ConnectionError](https://requests.readthedocs.io/en/latest/api/#requests.ConnectionError)
    * [https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions](https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions)