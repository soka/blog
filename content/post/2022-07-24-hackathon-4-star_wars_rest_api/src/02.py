#!/usr/bin/env python3

import requests

API_BASE_URL = "https://swapi.dev/api/"


# ======================================================
# CLASES 
# ======================================================

class SWAPI:
    #root_data = {}

    def test_connection(self):
        '''
        Esta función sólo realiza una llamada a la URL base de la API.

        Resumen códigos de estado HTTP
        https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
        - 200: OK

        Retorna:
        - False si hay excepcion por error de conexión o el codige de retorno HTTP no es 200
        - True si la conexión ha tenido éxito   
        '''
        try:
            response = requests.get(API_BASE_URL)
        except requests.exceptions.ConnectionError as errc:
            # https://requests.readthedocs.io/en/latest/api/#requests.ConnectionError
            # https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions
            #print ("Error Connecting:",errc)
            return False 
        else: 
            #print('GET '+API_BASE_URL+' status code:'+str(response.status_code)) # 200
            if response.status_code == requests.codes.ok:
                return True
            else:
                return False 

    def get_data(self,url):
        try:
            response = requests.get(url)
        except requests.exceptions.ConnectionError as errc:
            # https://requests.readthedocs.io/en/latest/api/#requests.ConnectionError
            # https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions
            #print ("Error Connecting:",errc)
            return False 
        else: 
            #print('GET '+API_BASE_URL+' status code:'+str(response.status_code)) # 200
            if response.status_code == requests.codes.ok:
                return response.json()
            else:
                return False 

    def get_root(self):        
        response_dict = self.get_data(API_BASE_URL)        
        return response_dict


 
swapi = SWAPI()

if swapi.test_connection() == False:
    print("Error de conexión, revisa la red y la URL")
else:
    root_dict=swapi.get_root()
    for key,val in root_dict.items():
        print(key,val)
    

      


