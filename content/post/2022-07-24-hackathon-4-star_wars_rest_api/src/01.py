#!/usr/bin/env python3

import requests

API_BASE_URL = "https://swapi.dev/api/"

class SWAPI:

    def test_connection(self):
        '''
        Esta función sólo realiza una llamada GET a la URL base de la API y retorna el código
        de estado HTTP o None si se produjo algún error

        Resumen códigos de estado HTTP
        https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
        - 200: OK
        '''
        try:
            response = requests.get(API_BASE_URL)
        except requests.exceptions.ConnectionError as errc:
            # https://requests.readthedocs.io/en/latest/api/#requests.ConnectionError
            # https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions
            print ("Error Connecting:",errc)
            return False 
        else:             
            if response.status_code == requests.codes.ok:
                return True
            else:
                return False 


swapi = SWAPI()
if swapi.test_connection() == False:
    print("Error de conexión, revisa la red y la URL")
else:
    print("Conexión con exito a "+API_BASE_URL)   

