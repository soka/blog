#!/usr/bin/env python3

import requests
import json
import pandas as pd

api_url = "https://swapi.dev/api/people/"
people_response = requests.get(api_url)
people_data= people_response.json()
#print(people_data['results'])
df = pd.DataFrame.from_dict(people_data['results'])
#print(df)
df.to_excel("output.xlsx",sheet_name='people')  
