#!/usr/bin/env python3

import requests

API_BASE_URL = "https://swapi.dev/api/"


# ======================================================
# CLASES 
# ======================================================

class SWAPI:
    #root_data = {}

    def test_connection(self):
        '''
        Esta función sólo realiza una llamada a la URL base de la API.

        Resumen códigos de estado HTTP
        https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
        - 200: OK

        Retorna:
        - False si hay excepcion por error de conexión o el codige de retorno HTTP no es 200
        - True si la conexión ha tenido éxito   
        '''
        try:
            response = requests.get(API_BASE_URL)
        except requests.exceptions.ConnectionError as errc:
            # https://requests.readthedocs.io/en/latest/api/#requests.ConnectionError
            # https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions
            #print ("Error Connecting:",errc)
            return False 
        else: 
            #print('GET '+API_BASE_URL+' status code:'+str(response.status_code)) # 200
            if response.status_code == requests.codes.ok:
                return True
            else:
                return False 

    def get_data(self,url):
        try:
            response = requests.get(url)
        except requests.exceptions.ConnectionError as errc:
            # https://requests.readthedocs.io/en/latest/api/#requests.ConnectionError
            # https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions
            #print ("Error Connecting:",errc)
            return False 
        else: 
            #print('GET '+API_BASE_URL+' status code:'+str(response.status_code)) # 200
            if response.status_code == requests.codes.ok:
                return response.json()
            else:
                return False 

    def get_root(self):        
        response_dict = self.get_data(API_BASE_URL)        
        return response_dict


    def get_all_people(self):
        people_lst = []

        response_dict = self.get_data(API_BASE_URL)
        if response_dict == False:
            return []        
        response_dict = self.get_data(response_dict['people'])        
        while response_dict['next']!=None:            
            for people in response_dict['results']:
                people_lst.append(people)            
            response_dict = self.get_data(response_dict['next'])        
        #print('People count: ',len(people_lst))
        return people_lst
        
    def get_people_by_id(self,id):
        response_dict = self.get_data(API_BASE_URL+'people/'+str(id))
        #print(response_dict)
        for key,val in response_dict.items():
            print(key,val)
        return response_dict

    def get_people_by_name(self,name):
        print(name)
        lst = self.get_all_people()
        for people in lst:
            #print(people['name'])
            if people['name'] == name:
                #print(name,' found!')
                return people
    
    def get_people_statistics(self):
        people_lst = self.get_all_people()
        total_mass=0
        total_height=0
        height_cnt=0
        
        for people in people_lst:
            if people['mass'] != 'unknown':
                #print(people['mass'].replace(',','.'))                
                total_mass+=float(people['mass'].replace(',','.'))
            if people['height'] != 'unknown':
                #print(people['height'])
                total_height+=int(people['height'])
                height_cnt+=1
        print('\n-Total mass: ',total_mass)
        print('-Mean height: ',total_height/height_cnt)
        
    def get_people_films(self,id):
        print(self.get_people_by_id(id))
        
        
swapi = SWAPI()

if swapi.test_connection() == False:
    print("Error de conexión, revisa la red y la URL")
else:
    #root_dict=swapi.get_root()
    #for key,val in root_dict.items():
    #    print(key,val)    
    #print(swapi.get_all_people()) 
    #print(swapi.get_all_people()) 
    #print(swapi.get_people_by_name("R2-D2")) 
    #swapi.get_people_statistics()
    swapi.get_people_films(1)
    