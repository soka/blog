#!/usr/bin/env python3

import requests
import json
import pandas as pd

page_num=1
api_url = "https://swapi.dev/api/people/?page="+str(page_num)
response = requests.get(api_url)
result_lst=[]
while response.status_code == 200:    
    people_data = response.json()          
    for item in people_data['results']:
        result_lst.append(item)       
    page_num+=1
    api_url = "https://swapi.dev/api/people/?page="+str(page_num)
    response = requests.get(api_url)

df = pd.DataFrame(result_lst)
df.to_excel("output.xlsx",sheet_name='people') 
