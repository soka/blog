---
title: Chrome Remote Desktop [borrador]
subtitle: Control remoto de tu PC
date: 2020-03-25
draft: false
description: 
tags: ["Aplicaciones","Remoto","Chrome","Google"]
---

## Puesta en marcha acceso remoto

![](img/02.png)

Sólo es necesario abrir este enlace [remotedesktop.google.com/access](remotedesktop.google.com/access) en la computadora que queremos controlar remotamente, si pinchamos sobre la flecha se instala una [extensión para el navegador](https://chrome.google.com/webstore/detail/chrome-remote-desktop/inomeogfingihgjfjlpeplalcfajhgai) y se descarga el programa correspondiente que necesitamos instalar.

![](img/01.png)

## Tomando el control, asistencia remota

El apartado de asistencia remota contiene dos opciones.

![](img/03.png)

Si queremos recibir asistencia remota debemos generar un código de uso único y compartirlo con el equipo que debe coger el control.

![](img/04.png)

La segunda opción es donde debemos introducir el código para coger abrir el acceso remoto a otro equipo.


## Referencias externas

- [Chrome Remote Desktop: qué es, cómo funciona y cómo puedes usarlo para controlar tu PC de forma remota ](https://www.genbeta.com/herramientas/chrome-remote-desktop-que-como-funciona-como-puedes-usarlo-para-controlar-tu-pc-forma-remota).
