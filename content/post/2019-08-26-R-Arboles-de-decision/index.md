---
title: Arboles de decisión
subtitle: Tomado de "R para profesionales de los datos" de Carlos J. Gil Bellosta
date: 2019-08-26
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "R - Party", "R - Paquetes", "Árboles de decisión"]
---

![](images/r-logo.PNG)

Esta entrada se basa en la excelente guía ["R para profesionales de los datos: una introducción"](https://www.datanalytics.com/libro_r/index.html) de Carlos J. Gil Bellosta (2018-04-22).

Un [árbol de decisión](ttps://es.wikipedia.org/wiki/%C3%81rbol_de_decisi%C3%B3n) es un modelo de predicción basado en un conjunto de datos que forman construcciones lógicas basadas en nodos (decisión sobre una variable) y conexiones o flechas entre sí. Un área donde son aplicados los árboles de decisión es la Teoría de juegos, el tres en raya es un claro ejemplo (llamado TicTacToe también), se juega por turnos, tiene 9 movimientos posibles (profundidad) y el resultado final puede ser ganar, empate o perder (se suelen representar como 1,0 o -1). A cada nodo (posible situación del tablero a en el desarrollo de la partida) se aplica el método Minimax para elegir el mejor movimiento (suponiendo que tu contrincante escogerá el peor para ti).

![](images/01.png)

[[Fuente](https://devcode.la/tutoriales/algoritmo-minimax/)]

Lo que hará el algoritmo Minimax cuando vaya regresando hacia atrás, será comunicarle a la llamada recursiva superior cuál es el mejor nodo hoja alcanzado hasta el momento. Siguiendo la imagén de arriba jugando nosotros con "X" parece que la mejor jugada es colocarla en la esquina inferior izquierda, tenemos más posibilidades de ganar cerrando en diagonal o en horizontal en las casilla inferiores.

## Ejemplo

En ["R para profesionales de los datos: una introducción"](https://www.datanalytics.com/libro_r/index.html) aplica los arboles de decisión para clasificar el origen de un aceite de oliva basado en sus propiedades.

El _dataset_ [olive](https://www.rdocumentation.org/packages/zenplots/versions/0.0-4/topics/olive):

![](images/02.png)

Lo primero es cargar el paquete [party](https://cran.r-project.org/web/packages/party/index.html) para trabajar con el árbol.

```r
install.packages("party")
library(party)
```

Vamos a tratar de predecir el "Area" de origen (North, South o Sardinia), por lo que eliminaremos la variable "Region":

```r
olive.00 <- olive
olive.00$Region <- NULL
View(olive.00)
```

Creamos el modelo:

```r
modelo <- ctree(Area ~ ., data = olive.00)
```

La fórmula `Area ~ .` indica que queremos modelar "Area" en función de ., es decir, el resto de las variables disponibles en nuestros datos.

Gráficamente:

```r
plot(modelo)
```

![](images/Rplot01.png)

Por ejemplo, el nodo terminal de la derecha, el que se construye con la regla eicosenoic > 0, contiene únicamente aceites procedentes de la zona sur de Italia.

## Código

- r / intro / 52-Arboles-de-decision / [Arboles-de-decision.R](https://gitlab.com/soka/r/blob/master/intro/52-Arboles-de-decision/Arboles-de-decision.R).

## Enlaces externos

- Wikipedia ["Árbol de decisión"](https://es.wikipedia.org/wiki/%C3%81rbol_de_decisi%C3%B3n).
- ["R para profesionales de los datos: una introducción"](https://www.datanalytics.com/libro_r/index.html) de Carlos J. Gil Bellosta (2018-04-22).
