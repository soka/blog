---
title: Meet for Slack
subtitle: Organiza rápidamente un Google Meet sin salir de Slack
date: 2021-04-26
draft: false
description: Aplicación para Slack
tags: ["Slack","Google Meet","Meet","Google","Aplicaciones","Videollamadas","Reuniones","Productividad"]
---

![](img/05.png)

Con la aplicación [Google Meet for Slack](https://meetforslack.com/) puedes organizar una reunión sin salir de Slack con un simple comando escribiendo `/meet [título del meet]` seguido del nombre de la sala en cualquier canal, mensaje directo individual o grupal.

La aplicación de puede instalar desde el propio Slack o desde la [página principal de la aplicación](https://meetforslack.com/).

![](img/01.png)

![](img/02.png)

Este Gif cogido de la página oficial de la aplicación demuestra lo fácil que es usarlo:

![](img/03.gif)

Al escribir el comando `/meet [título del meet]` la aplicación crea un enlace compartido a la reunión.

![](img/04.png)

Cuando comienza la reunión de forma automática cambia tu estado de Slack para indicar que estás en una reunión, ademas se crea una reserva en el calendario de Google.

En definitiva, es una buena aplicación en los tiempos que corren, nos permite ahorrar tiempo y saltar a una videoconferencia en esas situaciones en las que no conseguimos avanzar y la conversación mediante mensajes se eterniza.

## ENLACES INTERNOS

- Slack - Comunicación colaborativa para el trabajo en equipo [https://soka.gitlab.io/blog/post/2021-04-01-taller-slack-spri-enpresa-digitala/](https://soka.gitlab.io/blog/post/2021-04-01-taller-slack-spri-enpresa-digitala/).

## ENLACES EXTERNOS

- [https://meetforslack.com/](https://meetforslack.com/).
- [https://wwwhatsnew.com/2021/04/23/google-meet-para-slack-para-organizar-reuniones-en-segundos/](https://wwwhatsnew.com/2021/04/23/google-meet-para-slack-para-organizar-reuniones-en-segundos/)