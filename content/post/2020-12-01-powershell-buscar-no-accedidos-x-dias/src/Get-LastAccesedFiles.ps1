Function Get-NotAccessedFilesInDays
{

    Param([string[]]$path,

       [int]$numberDays)

    #Write-Output $path
    #Write-Output $numberDays

    $cutOffDate = (Get-Date).AddDays(-$numberDays)

    Get-ChildItem -Recurse -Path $path | Where-Object {$_.LastAccessTime -le $cutOffDate}

}

#Write-Output  $args[1] 
# Ficheros no accedidos en los ultimos 4 días
# .\Get-LastAccesedFiles.ps1 C:\Users\informatica\Documents\ 4 
 
Get-NotAccessedFilesInDays $args[0] $args[1]  | select  name, lastaccesstime, fullname | Export-CSV output.csv


