---
title: Buscar archivos no accedidos en X días
subtitle: Scripts PowerShell 
date: 2020-12-01
draft: false
description: Pasando como parámetro la ruta y días
tags: ["PS","PowerShell","PowerShell - Archivos","Windows","cmdlets","Scripting"]
---

![](img/powershell.png)

En mi portal de [PSFabrik](https://soka.gitlab.io/PSFabrik/) encontrarás más referencias agrupadas por temás.

Llega el fin de año y queremos comenzar el nuevo con buenos propósitos, dentro de la guía de buenas prácticas de gestión de documentos en el repositorio compartido está la sugerencía de **retirar todos los archivos no accedidos en varios años y guardarlos en un trastero digital**, me pongo manos a la obra y programo un script (muy mejorable) pero sólo me lleva unos minutos consultando aquí y allá, **el script debe aceptar dos parámetros, la ruta donde debemos buscar y el número de días que lleva el archivo sin ser accedido**, el resultado se exporta a un fichero CSV para que los responsables de cada carpeta tomen cartas en el asunto, una mejora futura es que mueva estos archivos a otra ubicación (manteniendo la ruta actual).

## Get-LastAccesedFiles.ps1

El el comando principal que usaré es [`Get-ChildItem`](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-childitem?view=powershell-7.1), le paso el parámetro `-Recurse` para que busque en todas las subcarpetas, le paso además la ruta y el resultado se canaliza por una tubería para buscar los elementos que me interesan.

¡He borrado algunos comentarios superfluos para dejar sólo lo mejor!

```ps
Function Get-NotAccessedFilesInDays
{

    Param([string[]]$path,

       [int]$numberDays)

    $cutOffDate = (Get-Date).AddDays(-$numberDays)

    Get-ChildItem -Recurse -Path $path | Where-Object {$_.LastAccessTime -le $cutOffDate}

}
 
Get-NotAccessedFilesInDays $args[0] $args[1]  | select  name, lastaccesstime, fullname | Export-CSV output.csv
```

## Uso

El uso del cmdlet es trivial, el primer parámetro es la ruta y el segundo el número de días que lleva sin ser accedido.

```dos
./Get-LastAccesedFiles.ps1 O:\datos\area-economica 365
```

El resultado se almacena en el fichero Export-CSV output.csv en la misma carpeta de ejecución del script.

## Enlaces externos

- [https://soka.gitlab.io/PSFabrik/](https://soka.gitlab.io/PSFabrik/).
- [https://soka.gitlab.io/PSFabrik/sistema-archivos/directorios/](https://soka.gitlab.io/PSFabrik/sistema-archivos/directorios/)
- [https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-childitem?view=powershell-7.1](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-childitem?view=powershell-7.1).
