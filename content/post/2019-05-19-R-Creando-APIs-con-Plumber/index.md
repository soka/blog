---
title: Como crear una API RESTful en R con Plumber
subtitle: Servicio Web local en R
date: 2019-05-19
draft: false
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "R - Básico",
    "R - Redes",
    "R - API",
    "R - Paquetes",
    "Programación",
    "API",
    "Webservice",
    "Plumber",
    "Swagger",
  ]
---

![](images/plumber-broken.png)

[**Plumber**](https://www.rplumber.io/) es un paquete de **R** que permite crear una API Web usando comentarios del código fuente.

## Instalación

Se instala como cualquier otro paquete:

```R
install.packages("plumber")
library(plumber)
```

## Ejemplo básico

### Diseño de la API

He creado un fichero "plumber.R" donde diseño mi API usando comentarios que empiezan por `#*`:

```R
# plumber.R

#* Echo back the input
#* @param msg The message to echo
#* @get /echo
function(msg=""){
  list(msg = paste0("The message is: '", msg, "'"))
}

#* Plot a histogram
#* @png
#* @get /plot
function(){
  rand <- rnorm(100)
  hist(rand)
}

#* Return the sum of two numbers
#* @param a The first number to add
#* @param b The second number to add
#* @post /sum
function(a, b){
  as.numeric(a) + as.numeric(b)
}
```

El fichero de ejemplo obtenido de la Web oficial de **Plumber** expone dos _endpoints_ de comunicaciones,`/echo` simplemente responde con el mismo mensaje que le pasamos en la llamada en formato JSON, `/plot` retorna una imagen en formato PNG de un diagrama generado con `hist`.

### Ejecución

En otro script cargo y ejecuto el fichero "plumber.R":

```R
install.packages("plumber")
library(plumber)

r <- plumb("plumber.R")
r$run(port=8000)
```

En la consola de RStudio vemos como arranca el servicio, para detenerlo usaremos `Esc`.

```
Starting server to listen on port 8000
Running the swagger UI at http://127.0.0.1:8000/__swagger__/
```

Así de sencillo, ahora podemos introducir esta URL en el navegador [http://localhost:8000/plot](http://localhost:8000/plot) y nos mostrará un [histograma](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-basicos-hist/), [http://localhost:8000/echo?msg=hello](http://localhost:8000/echo?msg=hello) mostrará de vuelta el mensaje que pasamos como parámetro a la URL.

```R
{"msg":["The message is: 'hello'"]}
```

Si accedemos a [http://127.0.0.1:8000/**swagger**/](http://127.0.0.1:8000/__swagger__/) se carga un UI creado con [Swagger](https://swagger.io/). Swagger es una herramienta que facilita la generación de APIs

![](images/01.PNG)

## Definiendo la salida devuelta

Por defecto **Plumber** serializa el objeto devuelto por la función en formato JSON usando el paquete jsonlite. En el ejemplo de arriba las llamadas devolvían cadenas JSON (podemos explicitar la salida JSON con `#* @json`) y una imagen (con `#* @png`), también podemos generar la salida con sintaxis HTML como sigue:

```R
#* @get /hello
#* @html
function(){
  "<html><h1>hello world</h1></html>"
}
```

## Ejercicios

- r / intro / [46-R-Creando-APIs-con-Plumber](https://gitlab.com/soka/r/tree/master/intro/46-R-Creando-APIs-con-Plumber).

## Enlaces internos

- ["Acceso a APIs con R - Cliente REST usando httr y jsonlite"](https://soka.gitlab.io/blog/post/2019-05-18-r-acceso-apis/).

## Enlaces externos

- ["rplumber.io"](https://www.rplumber.io/).
- ["Crear una RESTful API con R con plumber - Análisis y Decisión"](https://analisisydecision.es/crear-una-restful-api-con-r-plumber/).
- ["REST APIs and Plumber | R-bloggers"](https://www.r-bloggers.com/rest-apis-and-plumber/).
- GitHub ["rstudio/swagger"](https://github.com/rstudio/swagger): Swagger is a collection of HTML, Javascript, and CSS assets that dynamically generate beautiful documentation from a Swagger-compliant API. https://swagger.io/swagger-ui/.
- ["CRAN - Package swagger"](https://cran.r-project.org/web/packages/swagger/index.html): A collection of 'HTML', 'JavaScript', and 'CSS' assets that dynamically generate beautiful documentation from a 'Swagger' compliant API: <https://swagger.io/specification/>.
- ["RStudio 1.2 Preview: Plumber Integration"](https://blog.rstudio.com/2018/10/23/rstudio-1-2-preview-plumber-integration/).
- ["Webinar: Plumbing APIs with Plumber"](https://www.rstudio.com/resources/videos/plumbing-apis-with-plumber/).
