---
title: Trello - Etiquetas de colores en las tarjetas
subtitle: La pizarra digital inspirada en kanban
date: 2019-07-10
draft: false
description: Como añadir etiquetas de colores con texto a las tarjetas
tags:
  [
    "Trello",
    "Talleres",
    "Cursos",
    "Aplicaciones",
    "Trello - Introducción",
    "Trello - Tarjetas",
    "Kanban",
    "Metodologías",
    "Gestión de Proyectos",
    "Herramientas",
  ]
---

![](img/logo-trello.png)

Las etiquetas permiten categorizar las tarjetas (y posteriormente filtrarlas) por colores (10 en total) y un texto personalizado superpuesto. Una tarjeta puede tener varias etiquetas. Las tarjetas

Se pueden añadir etiquetas a una tarjeta a través del botón "Etiquetas" en la parte trasera de la tarjeta, en el menú de edición (haz clic en el ícono de lápiz en modo estacionario) o con el ratón encima de la tarjeta y usando el atajo "l" (L en minúscula) para ver todas las etiquetas disponibles. Si conoces el número de acceso directo (ver más abajo) también se puede añadir directamente la etiqueta pulsando el número correspondiente.

![](img/01.gif)

## Enlaces externos

- ["Añadir etiquetas a las tarjetas"](https://help.trello.com/article/797-adding-labels-to-cards).
