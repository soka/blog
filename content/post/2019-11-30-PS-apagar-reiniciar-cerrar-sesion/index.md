---
title: Apagar, reiniciar y cerrar sesión
subtitle: PowerShell
date: 2019-11-30
draft: false
description: Administración servidores Windows
tags: ["Windows","Sistemas","PowerShell","Administración","cmdlets","Scripting"]
---

![](img/logo2.png)

## Apagar el equipo

Apagado del equipo con `Stop-Computer`, también podemos hacerlo remotamente y aplicarlo sobre un conjunto de equipos:

```ps
PS> Stop-Computer -ComputerName "Server01", "Server02", "localhost"
```

Puedo usar la ayuda con `Get-Help Stop-Computer` para ampliar la información y consultar todos los parámetros.

## Reiniciar

```ps
PS> Restart-Computer
```

Al igual que el comando anterior podemos aplicar el reinicio sobre varias computadoras remotas con `-ComputerName`. Podemos usar el modificador `-Credential` para especificar un usuario del dominio.

## Cerrar sesión

```ps
C:\ logoff.exe
```

## Enlaces

- [Stop-Computer](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/stop-computer?view=powershell-6).
- [Comandos PS en computadoras remotas](https://soka.gitlab.io/PSFabrik/networking/remoto/).
- [Enable-PSRemoting](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/enable-psremoting?view=powershell-5.1).
- [How to Run PowerShell Commands on Remote Computers](https://www.howtogeek.com/117192/how-to-run-powershell-commands-on-remote-computers/).