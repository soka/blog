---
title: Ajuste dinámico de precios en comercios electrónicos (2/3) [BORRADOR]
subtitle: Machine learning 
date: 2019-01-18
draft: true
description: Predicción de la demanda y tecnologías
tags: ["Algoritmos","eCommerce","Machine learning","Marketing","R","Economía","Comercio"]
---

# Introducción 

![](images/chiste.jpg)

https://iahuawei.xataka.com/ia-inteligente-va-a-quitarnos-trabajo-ocho-mitos-interrogantes/

La humanidad siempre ha sido curiosa respecto a que le depara el futuro, tratamos de predecir pequeñas cosas como el resultado de un encuentro deportivo (no es mi caso) o el partido vencedor de las próximas elecciones. Se atribuie al presidente de IBM [Thomas Watson la siguiente predicción en 1943]((https://maikelnai.naukas.com/2008/06/26/las-mas-famosas-predicciones-erroneas-de-la-historia-reciente/)): 

_"Creo que existe un mercado mundial para tal vez… cinco computadoras"_

Esta predicción se ha demostrado evidentemente como un gran error en el mundo de las tecnologías, esto pone de relieve a su vez **la importancia de las predicciones económicas para el éxito de nuestra empresa en el futuro**. 

Este pasado año uno de mis campos de estudio han sido las plataformas eCommerce y el marketing digital, en la era pre-internet la capacidad para establecer diferentes precios para diferentes consumidores era muy limitada. Con la llegada de Amazon, eBay y otras plataformas de ventas en línea **los eCommerce pueden desarrollar portales especificos para cada cliente basado en sus necesidades**, la información que podemos recopilar de nuestros sistemas ofrece interesantes posibilidades para personalizar servicios a un nivel de detalle tremendo (si sabemos reconocer las señales). 

Lo mismo que todas las personas no son iguales tampoco los usuarios de nuestra tienda en línea lo son, factores culturales como el lugar de origen, factores económicos, preferencias personales u otros pueden determinar el gasto medio de nuestro cliente, **el factor precio aún prevalece como razon para adquirir un producto entre los consumidores** ([para el 60% de los compradores](http://www.pwc.com/gx/en/industries/retail-consumer/global-total-retail.html)) 

_¿Cuanto esta dispuesto a pagar un **cliente** por nuestro **producto**?_. 

Además el usuario puede comparar precios y características en diferentes sitios Web o buscar productos similares o alternativos. La siguiente gráfica está obtenida con [Camel Camel Camel](https://es.camelcamelcamel.com/) con la fluctuación de precios de venta de un producto concreto en Amazon.

![](images/camelcamelcamel_01.PNG)

# Ajuste de precios dinámicos en eCommerce

**La variación de precios de forma dinámica en respuesta a cambios en los mercados puede ayudarnos a maximizar los beneficios y crear ventajas competitivas a nuestro favor**.

Volviendo al tema de las grandes empresas y sus estrategias de fijación de precios, **en el sector retail Zara fija diferentes precios basándose en los ingresos medios por familia** dependiendo del pais en el que operan sus tiendas.

Algunos negocios apuestan por ofrecer sus productos en **marketplaces** (plataformas en las que se venden productos de diferentes negocios, marcas, empresas) creados por empresas como eBay, Amazon o AliExpress que actuan como intermediarios.

Voy a tratar de agrupar de forma resumida algunas de las **variables principales que pueden afectar a nuestras ventas**.

**El catalogo de productos**, sus atributos y clasificación en clases lógicas que permita más tarde agruparlos basandonos en diferentes criterios. También deberiamos tomar en cuenta la tipología de los productos y sus atributos. Todo o casi todo gira en torno al producto que ofrecemos, modelizar estos artículos para nuestra estrategia de precios es esencial, la calidad de estos datos puede determinar el éxito de nuestro proyecto.

**Existencias y costes**: Si contamos con stock o cada uno es único por ejemplo, dentro de este campo puede entrar el **margen deseado coste-beneficio**.


**Demanda**: Una alta demanda de nuestro producto en el futuro puede animarmos a subir el precio o al contrario un producto que no se vende bien puede ser aconsejable una bajada de precios.  

Datos específicos del **consumidor y datos historicos de ventas**. Ejemplos como el total que paga el cliente en cada compra (de uno o varios productos), cantidad de artículos por cada compra. 

**Precios y promociones**. Tipos de promociones y precios, pueden estar sujetas a un producto concreto, el clásico descuento por volumen por ejemplo, o al conjunto de la compra (descuentos por carrito de la compra combinando X productos), hoy en día las aplicaciones eCommerce ofrecen muchas y variadas formas de ofrecer descuentos, por tiempo límitado, los típicos cupones de descuento especiales para algunos clientes,etc. 

Precios de la **competencia** de productos similares. 

**Calendarios y eventos**. Si el día de la semana es laborable, fin de semana, festivo o un evento concreto como el Black Friday.

# Teoría del aprendizaje automático

[El CEO de Google recientemente declaraba](https://www.techworm.net/2018/01/google-ceo-sundar-pichai-compares-ai-fire-electricity.html) que las IA pueden tener el mismo impacto o más que el descubrimiento del fuego o la electricidad en la sociedad. Desde la economía a la educación o la salud las IA van a cambiar de forma profunda nuestras formas de trabajar y relacionarnos.  

Las tecnologías de **aprendizaje automático o ML (machine learning)** pueden ser empleadas para definir **algoritmos de ajuste de precios dinámicos** aprendiendo de los datos previos. Aún hoy se usan métodos manuales y en muchos casos basados en gran parte en la intuición y el conocimiento no explicito que depende de las personas que conocen el negocio, **la mayoría de las compañias se enfrentan a una caja negra para entender como se determinan los precios**. **El ajuste de precios debe ser tomado como una ciencia**, un ajuste en tiempo real en respuesta a la demanda basado en datos es más efectivo y medible, permite responder más rápido a las fluctuaciones de la demanda y tomar en cuenta nuevas variables que hace unos años eran impensables. 


http://www.aprendemachinelearning.com/7-pasos-machine-learning-construir-maquina/

https://cleverdata.io/limpiar-seleccionar-transformar-datos/

Un problema de **ML (Machine Learning)** o [aprendizaje automático](https://es.wikipedia.org/wiki/Aprendizaje_autom%C3%A1tico) es una rama de la IA (Inteligencia Artificial) que persigue las computadoras _aprendan_ a partir de ejemplos generalizando comportamientos. 

Generalmente los algoritmos de ML se clasifican en dos grandes grupos principales:

* Supervisados  

sigue una serie de fases básicas.

![](https://www.esspl.com/wp-content/uploads/2017/05/Untitled-1-4.png)

# Modelado de datos y definición del problema 

_“Los datos son la materia prima del siglo XXI”_ (Ángela Merkel) [Fuente](https://www.corporateit.cl/index.php/2016/03/14/angela-merkel-los-datos-son-la-materia-prima-del-siglo-xxi/)

La definición del problema es la **predición de la evolución de ventas de cada producto en relación a sus atributos**, el ajuste dinámico de precios **también debe comprender como responde el consumidor y la relación con los precios fijados**. 

La **primera fase** empieza en la **selección de datos** o variables que servirán para determinar nuestra estrategia de precios, preferiblemente una gran cantidad de datos (ejemplos u observaciones) para los que ya tiene la respuesta de destino. **El poder predictivo del modelo será solo tan bueno como los datos que usemos**. 

El precio final debería estar limitado por unos valores máximos y especialmente mínimos dependiendo del beneficio esperado por la venta del producto (_"standard deviation and minimum and maximum values for quantitative variables; Product Sale Price, Order Item Quantity, and Supplier Purchase Price"_). 



## Clasificar y clusterización 

El objetivo fundamental de ML consiste en generalizar más allá de las instancias de datos que se utilizan para entrenar a los modelos.

Debemos definir subconjuntos de datos de entrenamiento y evaluación del modelo. ML utiliza los datos de entrenamiento para entrenar a los modelos a que vean patrones y utiliza los datos de evaluación para evaluar la calidad de predicción del modelo entrenado.

## Entrenar modelo  


* Análisis predictivo. 
* Búsqueda de patrones de venta 
* Interrelación de datos entre atributos o categorias comunes entre productos.

# Generar modelo y validar 

El **tercer paso** es la aplicación del modelo optimo para fijar los precios de forma dinámica y evaluar los resultados del modelo para mejorar su precisión. Lo que haríamos es **proporcionar a nuestro algoritmo todos estos datos para “entrenarlo” y que aprenda de patrones**.

Una vez tengamos este modelo entrenado, podremos pedirle que haga una predicción al darle las características de un nuevo producto. Lo que define a un "buen" o "mal" algoritmo es la precisión con la que haga las predicciones en un dominio y contexto dado.

# Mejorar 



# Enlaces externos

* sweetpricing.com ["How Dynamic Pricing Uses Machine Learning to Increase Revenue"](https://sweetpricing.com/blog/2017/02/machine-learning/).
* blog.statsbot.co ["Strategic Pricing in Retail with Machine Learning"](https://blog.statsbot.co/strategic-pricing-d1adcc2e0fd6).
* pwc.com ["2018 Global Consumer Insights Survey"](https://www.pwc.com/gx/en/industries/consumer-markets/consumer-insights-survey.html).
* cloud.google.com ["Using machine learning for insurance pricing optimization"](https://cloud.google.com/blog/products/gcp/using-machine-learning-for-insurance-pricing-optimization).
* news.mit.edu ["A machine-learning approach to inventory-constrained dynamic pricing"](https://news.mit.edu/2018/thompson-sampling-machine-learning-inventory-constrained-dynamic-pricing-0315).
* blog.aimultiple.com ["Dynamic pricing: In-depth Guide to Improved Margins [2019]"](https://blog.aimultiple.com/dynamic-pricing/).
* blog.aimultiple.com ["Dynamic pricing is the strongest profitability lever for a large company"](https://blog.aimultiple.com/pricing-optimization/).
* datasciencecentral.com ["Price Optimisation Using Decision Tree (Regression Tree) - Machine Learning"](https://www.datasciencecentral.com/profiles/blogs/price-optimisation-using-decision-tree-regression-tree).
* ["Decision Tree Classification and Forecasting of Pricing Time Series Data"](http://www.diva-portal.org/smash/get/diva2:746332/FULLTEXT01.pdf) EMIL LUNDKVIST [PDF].
* linkedin.com ["https://www.linkedin.com/pulse/5-algorithms-every-pricing-director-should-know-manu-carricano-phd/"](5 Algorithms every Pricing Director should know).
* ["Dynamic Pricing and Learning"](https://www.math.vu.nl/~mei/theses/arnoud/proefschrift.pdf) A.V. den Boer [PDF].
* mindtools.com [Decision Trees - Choosing by Projecting "Expected Outcomes"](https://www.mindtools.com/dectree.html).
* researchgate.net ["Online Self-Adaptive Cellular Neural Network Architecture for Robust Time-Series Forecast"](https://www.researchgate.net/publication/309590088_Online_Self-Adaptive_Cellular_Neural_Network_Architecture_for_Robust_Time-Series_Forecast).
* inbest.cloud ["MACHINE LEARNING: REGRESIONES PARA LA PREDICCIÓN FINANCIERA"](https://www.inbest.cloud/comunidad/machine-learning-regresiones-para-predicci%C3%B3n-financiera).
* ["Machine Learning para dummies"](https://www.paradigmadigital.com/techbiz/machine-learning-dummies/).

Aplicaciones en línea para dibujar gráficas:

* [XY - Online Chart Tool](https://www.onlinecharttool.com/): Soporta diferentes tipos de gráficos parametrizables. 

Aplicaciones en línea para aplicar el modelo de regresión líneal:

* [GraphPad - Linear regression calculator](https://www.graphpad.com/quickcalcs/linear1/). Introduciendo o copiando y pegando los valores X e Y muestra la gráfica de dispersión de los puntos y la recta de regresión líneal junto con el conjunto de datos resultantes como la pendiente o el valor [R Cuadrado (Coeficiente de determinación)](https://economipedia.com/definiciones/r-cuadrado-coeficiente-determinacion.html).



# Imágenes

