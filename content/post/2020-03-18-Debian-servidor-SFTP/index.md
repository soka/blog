---
title: Servidor de arhivos con OpenSSH SFTP en Debian
subtitle: Compartiendo un disco USB desde mi casa
date: 2020-03-18
draft: false
description: 
tags: ["Aplicaciones","Linux","Debian","OpenSSH"]
---

![](img/OpenSSH.png)

## Instalación OpenSSH

Comando:

```sh
apt install openssh-server
```

## Configuración

Creo el grupo usuarios-sftp con `/sbin/groupadd usuarios-sftp` (para borrarlo posteriormente `/sbin/groupdel usuarios-sftp`).

Edito el archivo /etc/ssh/sshd_config y comento la línea `#Subsystem     sftp      /usr/lib/openssh/sftp-server` y añado que viene después:

```sh
# vim /etc/ssh/sshd_config

#Subsystem     sftp      /usr/lib/openssh/sftp-server
Subsystem      sftp      internal-sftp
Match Group usuarios-sftp
      #ChrootDirectory %h
      X11Forwarding no
      AllowTcpForwarding no
      ForceCommand internal-sftp
```

Yo he comentado una línea `#ChrootDirectory %h` para que los usuarios no queden enjaulados (chroot) dentro de su carpeta _home_.

Reinicio el servidor:

``` sh
# systemctl restart sshd
```

## Creación de usuarios

Creo el usuario `# /sbin/useradd -s /usr/sbin/nologin -m iker.landajuela` (para borrarlo posteriormente `/sbin/userdel -r EllacuriaSFTP`).

La oción -m especifica que debe crearse la carpeta personal del usuario que por defecto lleva su mismo nombre y se ubica en /home. No podrá acceder a una línea de órdenes porque le asignamos /usr/sbin/nologin como shell (la línea /usr/sbin/nologin este presente en el archivo /etc/shells).

Establezco una contraseña para el usuario: `# passwd iker.landajuela`.


Añadimos el usuario al grupo:

```sh
/sbin/usermod -aG usuarios-sftp iker.landajuela
```

Establecer a root como propietario de la carpeta personal del usuario y cambiar sus permisos. El usuario no podrá acceder remotamente a una línea de órdenes ni crear archivos en su carpeta personal.

```sh
# chown root:root /home/iker.landajuela
# chmod 755 /home/iker.landajuela
```

```sh
# mkdir /home/iker.landajuela/publico
# chown iker.landajuela:iker.landajuela /home/iker.landajuela/publico
```

Hago la primera prueba desde la línea de comandos de mi propio equipo con `sftp iker.landajuela@192.168.221.103`.

## Acceso media USB

A pesar de que el usuario puede desplazarse libremente entre carpetas fuera de _home_ no puede acceder a una unidad USB con en enlace simbólico en el _home_ (`ln -s /media/usb/ datosp`)

Lo que he hecho es desmontar la unidad y volver montarla con el UID del usuario SFTP

```sh
umount /dev/sdc
mkdir -p /media/usb
mount -o rw,user,uid=1001,umask=000,exec /dev/sdc /media/usb
```

## NAT en el router de casa para acceder desde fuera

Abrir el puerto 22 sftp TCP.

## Cliente

Para Windows sin duda WinSCP portable. En linux se puede usar la línea de comandos con sftp. Si tenemos un navegador Chrome parece que existe un plugin [sFTP Client](https://chrome.google.com/webstore/detail/sftp-client/jajcoljhdglkjpfefjkgiohbhnkkmipm?hl=en-GB).



## Referencias externas

- [Servidor SFTP en Debian 9](https://jealetecnologia.wordpress.com/2018/04/01/servidor-sftp-en-debian-9/).
- [Crear un servidor SFTP enjaulado](https://geekland.eu/crear-servidor-sftp-enjaulado/).
- [Trying to setup a SFTP user with limited access](https://www.digitalocean.com/community/questions/trying-to-setup-a-sftp-user-with-limited-access).
- [Patch webOS Make USB Partition Writable via SFTP](https://www.webos-internals.org/wiki/Patch_webOS_Make_USB_Partition_Writable_via_SFTP  ).
- [How to Create Users in Linux (useradd Command)](https://linuxize.com/post/how-to-create-users-in-linux-using-the-useradd-command/). Creating a User with Specific Home Directory.
- [How to Restrict SFTP Users to Home Directories Using chroot Jail](https://www.tecmint.com/restrict-sftp-user-home-directories-using-chroot/).