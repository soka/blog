---
title: Introducción a Laravel
subtitle: Instalación Composer y primeros pasos con Laravel
date: 2019-04-10
draft: true
description: PowerShell cmdlets
tags: ["Composer", "Laravel", "PHP", "Web"]
---

[**Composer**](https://getcomposer.org/) es un gestor de paquetes y dependencias para **PHP**, la diferencia respecto a Yum, NPM, APT u otros es que los paquetes se instalan en la carpeta local de trabajo de nuestro proyecto y no de forma global. La forma más sencilla de instalar para MS Win es descargar y ejecutar [Composer-Setup.exe](https://getcomposer.org/Composer-Setup.exe).

```
C:\>composer -V
Composer version 1.8.5 2019-04-09 17:46:47
```

# Proyecto en Laravel

# Enlaces internos

# Enlaces externos

- ["Laravel Homestead - Laravel - The PHP Framework For Web Artisans"](https://laravel.com/docs/5.8/homestead).
