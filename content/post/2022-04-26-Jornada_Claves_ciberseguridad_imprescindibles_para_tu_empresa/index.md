---
title: Claves de ciberseguridad imprescindibles para tu empresa
subtitle: Jornada a cargo de Assured Clarity impartida por Asier Barranco
date: 2022-04-26
draft: false
description: Jornada a cargo de Assured Clarity impartida por Asier Barranco
tags: ["Jornadas","Formación","SPRI","Ciberseguridad","SEINF","Seguridad Informática","Empresas","SEIN","Enpresa Digitala"]
---

Imparte: [Asier Barranco](https://github.com/asierbarran) ([https://cibercracking.weebly.com/](https://cibercracking.weebly.com/)).

Esta guía te ayudará a dar los primeros pasos a la hora de integrar la ciberseguridad como un aspecto más para tener en cuenta respecto a la infraestructura tecnológica de tu empresa. A través de una variedad de herramientas, te ayudaremos en las necesidades básicas que debes cubrir para mantener las tecnologías de información a salvo dentro de tu empresa. [doc/Guía_-_Claves_de_ciberseguridad.pdf](doc/Guía_-_Claves_de_ciberseguridad.pdf)

**Algunos enlaces**:

* [https://www.smartfense.com/es-es/herramientas/spoofcheck/](https://www.smartfense.com/es-es/herramientas/spoofcheck/)
* [https://www.abuseipdb.com/](https://www.abuseipdb.com/)
* [https://www.virustotal.com/gui/home/upload](https://www.virustotal.com/gui/home/upload)
* [https://haveibeenpwned.com/](https://haveibeenpwned.com/)
* [https://monitor.firefox.com/](https://monitor.firefox.com/)
* [https://www.dehashed.com/](https://www.dehashed.com/). Herramienta de pago.
* [https://cibercracking.weebly.com/rompiendo-un-comprimido-con-contrasentildea.html](https://cibercracking.weebly.com/rompiendo-un-comprimido-con-contrasentildea.html)
* [https://www.passwordmonster.com/](https://www.passwordmonster.com/)

**Presentación:**

![](img/01.png)

![](img/02.png)

![](img/03.png)

![](img/04.png)

![](img/05.png)

![](img/06.png)

![](img/07.png)

![](img/08.png)

![](img/09.png)

![](img/10.png)

![](img/11.png)

![](img/12.png)

![](img/13.png)

![](img/14.png)

![](img/15.png)

![](img/16.png)

![](img/17.png)

![](img/18.png)

![](img/19.png)

![](img/20.png)

![](img/21.png)

![](img/22.png)

![](img/23.png)

![](img/24.png)

![](img/25.png)

![](img/26.png)

![](img/27.png)

![](img/28.png)

![](img/29.png)

![](img/30.png)

![](img/31.png)

![](img/32.png)

![](img/33.png)

![](img/34.png)

![](img/35.png)

![](img/36.png)

![](img/37.png)

![](img/38.png)

![](img/39.png)

![](img/40.png)

![](img/41.png)

![](img/42.png)

![](img/43.png)

![](img/44.png)

![](img/45.png)



