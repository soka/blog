---
title: Uso básico de Git
subtitle: Sesión nº 2 curso Git
date: 2020-11-07
draft: false
description: Control de versiones con Git y GitLab
tags: ["Cursos","Curso Git","Código","Control Versiones","GitLab","Git","SCM"]
---

![](img/00.png)

## Uso básico de Git

## Recursos

Guía visual de las áreas de trabajo y comandos Git [Visual Git Cheat Sheet](https://ndpsoftware.com/git-cheatsheet.html).

![](img/03.png)

Comandos básicos:

![](img/02.png)

## Introducción

Se puede empezar de dos formas, clonando un proyecto existente de un repositorio con [`git clone`](https://git-scm.com/docs/git-clone), otra forma es crear un nuevo repositorio vacio con [`git init`](https://git-scm.com/docs/git-init).

## Iniciando un nuevo repositorio

```bash
$ mkdir testrepo
$ cd testrepo/
$ git init
Inicializado repositorio Git vacío en /home/popu/Documentos/GitLab/testrepo/.git/
```

El comando crea un subdirectorio llamado .git que contiene todo lo necesario.

```bash
$ ls .git/
branches  config  description  HEAD  hooks  info  objects  refs
```

Creamos un archivo de prueba README.md y lo añadimos con [`git add`](https://git-scm.com/docs/git-add) y grabamos los cambios al repositorio con [`git commit`](https://git-scm.com/docs/git-commit).

```bash
$ code README.md
$ git add README.md
$ git commit -m "Versión inicial del proyecto"
[master (commit-raíz) 161c044] Versión inicial del proyecto
 1 file changed, 1 insertion(+)
 create mode 100644 README.md
```

## Clonando un repositorio remoto existente

Si quieres **obtener una copia local de un repositorio remoto existente** el comando que necesitas es [`git clone`](https://git-scm.com/docs/git-clone)

```bash
$ git clone https://gitlab.com/soka/penascalf52021.git
```

El comando crea una carpeta penascalf52021 y inicia un directorio .git dentro usando el protocolo HTTP, también se puede usar [SSH](https://es.wikipedia.org/wiki/Secure_Shell) (lo veremos más adelante).

## Grabando los cambios en el repositorio

Ya tienes un repositorio Git y un _checkout_ o copia de trabajo de los archivos de dicho proyecto.  El siguiente paso es realizar algunos cambios y confirmar instantáneas de esos cambios en el repositorio cada vez que el proyecto alcance un estado que quieras conservar.

Recuerda que **cada archivo de tu repositorio puede tener dos estados: rastreados y sin rastrear** (_tracked_ o _untracked_). Los archivos rastreados (tracked files en inglés) son todos aquellos archivos que estaban en la última instantánea del proyecto; pueden ser archivos sin modificar, modificados o preparados (_staged_). 

Los archivos sin rastrear son todos los demás - cualquier otro archivo en tu directorio de trabajo que no estaba en tu última instantánea y que no está en el área de preparación (staging area). Cuando clonas por primera vez un repositorio, todos tus archivos estarán rastreados y sin modificar pues acabas de sacarlos y aun no han sido editados.

Mientras editas archivos, Git los ve como modificados, pues han sido cambiados desde su último commit. Luego preparas estos archivos modificados y finalmente confirmas todos los cambios preparados, y repites el ciclo.

![](img/01.png)

## Revisando el Estado de tus Archivos

La herramienta principal para **determinar en qué estado se encuentran los archivos** es el comando [`git status`](https://git-scm.com/docs/git-status). Si ejecutas este comando inmediatamente después de clonar un repositorio, deberías ver algo como esto: 

```bash
$ git status
En la rama master
Tu rama está actualizada con 'origin/master'.

nada para hacer commit, el árbol de trabajo está limpio
```

Esto significa que tienes un directorio de trabajo limpio - en otras palabras, que no hay archivos rastreados y modificados. Además, Git no encuentra archivos sin rastrear, de lo contrario aparecerían listados aquí. Finalmente, el comando te indica en cuál rama estás y te informa que no ha variado con respecto a la misma rama en el servidor. Por ahora, la rama siempre será “master”, que es la rama por defecto; no le prestaremos atención de momento.

Supongamos que añades un nuevo archivo a tu proyecto, un simple TEST.md. Si el archivo no existía antes y ejecutas git status, verás el archivo sin rastrear de la siguiente manera

```bash
$ echo '# Mi primer proyecto' > TEST.md
$ git status

En la rama master
Tu rama está actualizada con 'origin/master'.

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	TEST.md

no hay nada agregado al commit pero hay archivos sin seguimiento presentes (usa "git add" para hacerles seguimiento)
```

Puedes ver que el archivo TEST.md está sin rastrear porque aparece debajo del encabezado “Untracked files” (Archivos sin seguimiento) en la salida. Sin rastrear significa que Git ve archivos que no tenías en el commit anterior. Git no los incluirá en tu próximo commit a menos que se lo indiques explícitamente. Se comporta así para evitar incluir accidentalmente archivos binarios o cualquier otro archivo que no quieras incluir. Como tú sí quieres incluir TEST.md, debes comenzar a rastrearlo.

## Rastrear Archivos Nuevos

Para comenzar a rastrear un archivo debes usar el comando [`git add`](https://git-scm.com/docs/git-add). Para comenzar a rastrear el archivo TEST.md, puedes ejecutar lo siguiente:

```bash
$ git add TEST.md
```

Ahora si vuelves a ver el estado del proyecto, verás que el archivo TEST.md está siendo rastreado y está preparado para ser confirmado:

```bash
$ git status

En la rama master
Tu rama está actualizada con 'origin/master'.

Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	nuevo archivo:  TEST.md
```

Puedes ver que está siendo rastreado porque aparece luego del encabezado “Cambios a ser confirmados” (“Changes to be committed” en inglés). Si confirmas en este punto, se guardará en el historial la versión del archivo correspondiente al instante en que ejecutaste [`git add`](https://git-scm.com/docs/git-add). 

El comando [`git add`](https://git-scm.com/docs/git-add) puede recibir tanto una ruta de archivo como de un directorio; si es de un directorio, el comando añade recursivamente los archivos que están dentro de él.

Vamos a aprovechar para modificar un archivo que estaba previamente rastreado y que ya existía en el repositorio original, para ver las diferencias respecto al repositorio usamos `git diff README.md`.

## Confirmar tus Cambios

Ahora que tu área de preparación (_stage area_ o _index area_) está como quieres, puedes confirmar tus cambios. Recuerda que cualquier cosa que no esté preparada - cualquier archivo que hayas creado o modificado y que no hayas agregado con git add desde su edición - no será confirmado. Se mantendrán como archivos modificados en tu disco. En este caso, digamos que la última vez que ejecutaste git status verificaste que todo estaba preparado y que estás listo para confirmar tus cambios. La forma más sencilla de confirmar es escribiendo [`git commit`](https://git-scm.com/docs/git-commit):

```bash
$ git commit
```

Al hacerlo, arrancará el editor de tu preferencia. (El editor se establece a través de la variable de ambiente $EDITOR de tu terminal - usualmente es vim o emacs, aunque puedes configurarlo con el editor que quieras usando el comando `git config --global core.editor`.

Otra alternativa es escribir el mensaje de confirmación directamente en el comando commit utilizando la opción -m:

```bash
$ git commit -m "Modificado README.md"
[master bf44ac9] Modificado README.md
 1 file changed, 1 insertion(+)
 create mode 100644 TEST.md
```

## Estado resumido

Si ejecutamos `git status` -s o `git status --short` permite visualizar un resumen con _flag_ que indica el estado de los archivos.

```bash
$ git status -s
 M README.md
?? nuevo-fichero.txt
```

Los ficheros nuevos sin rastrear son marcados con `??`, los nuevos ficheros añadidos al _stagging area_ o _index_ con `git add` se marcan con `A`, los modificados con una `M`, etc.

```bash
$ git add nuevo-fichero.txt 
$ git status -s
 M README.md
A  nuevo-fichero.txt
```

## Excluyendo ficheros

A menudo querrás excluir ficheros del repositorio, ejemplos típicos son ficheros con logs de ejecución, ficheros intermedios de compilación o ejecutables o binarios resultantes.

Este es un pequeño ejemplo

```bash
$ cat .gitignore 
*.exe
```

La documentación oficial ofrece información detallada [https://git-scm.com/docs/gitignore](https://git-scm.com/docs/gitignore).

## Viendo las modificaciones 

Si deseas saber que has cambiado exactamente usamos [`git diff`](https://git-scm.com/docs/git-diff), modificamos el fichero README.md para probar, hacemos un `git status` y luego:

```bash
$ git diff
diff --git a/README.md b/README.md
index b4920f9..6ff3bf1 100644
--- a/README.md
+++ b/README.md
@@ -1,3 +1,7 @@
 # PeñascalF52021
 
-Curso Full Stack Web Developer. Ejercicios básicos Git y VSCode 
\ No newline at end of file
+Curso Full Stack Web Developer. Ejercicios básicos Git y VSCode.
+
+Viewing Your Staged and Unstaged Changes
+
+
```

El comando compara los contenidos del directorio de trabajo con el área de _staging_.

## Borrando ficheros 

Para borrar un fichero debemos hacerlo del área de _staging_ o _index_ y hacer un _commit_. El comando es [`git rm`](https://git-scm.com/docs/git-rm) y hace que el archivo no figure como no rastreado.


```bash
$ git status
En la rama master
Tu rama está actualizada con 'origin/master'.

Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	nuevo archivo:  TEST.md

$ rm TEST.md 
$ git status
En la rama master
Tu rama está actualizada con 'origin/master'.

Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	nuevo archivo:  TEST.md

Cambios no rastreados para el commit:
  (usa "git add/rm <archivo>..." para actualizar a lo que se le va a hacer commit)
  (usa "git checkout -- <archivo>..." para descartar los cambios en el directorio de trabajo)

	borrado:        TEST.md

$ git rm TEST.md
rm 'TEST.md'
$ git status
En la rama master
Tu rama está actualizada con 'origin/master'.

nada para hacer commit, el árbol de trabajo está limpio
```

## Moviendo ficheros

Si renombrar un fichero en Git no queda registro del movimiento, Git tiene un comando para realizar esta operación `$ git mv file_from file_to`.

```bash
$ git mv rename.txt rename
popu@diablo:~/Documentos/GitLab/penascalf52021$ git status
En la rama master
Tu rama está adelantada a 'origin/master' por 1 commit.
  (usa "git push" para publicar tus commits locales)

Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	renombrado:     rename.txt -> rename
```

## Ver el Historial de Confirmaciones (commits)

Cuando has realizado varios _commits_ o has clonado un repositorio de un proyecto existente con un historial de _commits_ previos probablemente probablemente quieras mirar atrás para ver qué modificaciones se han llevado a cabo. La herramienta más básica y potente para hacer esto es el comando [`git log`](https://git-scm.com/docs/git-log).

Cuando ejecutes git log sobre tu proyecto, deberías ver una salida similar a esta:

```bash
$ git log
commit ca82a6dff817ec66f44342007202690a93763949
Author: Scott Chacon <schacon@gee-mail.com>
Date:   Mon Mar 17 21:52:11 2008 -0700

    changed the version number

commit 085bb3bcb608e1e8451d4b2432f8ecbe6306e7e7
Author: Scott Chacon <schacon@gee-mail.com>
Date:   Sat Mar 15 16:40:33 2008 -0700

    removed unnecessary test

commit a11bef06a3f659402fe7563abf99ad00de2209e6
Author: Scott Chacon <schacon@gee-mail.com>
Date:   Sat Mar 15 10:31:28 2008 -0700

    first commit
```

Por defecto, si no pasas ningún parámetro, git log lista las confirmaciones hechas sobre ese repositorio en orden cronológico inverso. Es decir, las confirmaciones más recientes se muestran al principio. Como puedes ver, este comando lista cada confirmación con su suma de comprobación SHA-1, el nombre y dirección de correo del autor, la fecha y el mensaje de confirmación.

## Enlaces internos

- [Control de versiones con Git - Sesión nº 1 curso Git](https://soka.gitlab.io/blog/post/2020-11-07-curso-git-1/).

## Enlaces externos

- [https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio).
- [https://git-scm.com/book/es/v2/Fundamentos-de-Git-Ver-el-Historial-de-Confirmaciones](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Ver-el-Historial-de-Confirmaciones)
- [https://about.gitlab.com/images/press/git-cheat-sheet.pdf](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

## Autor

Iker Landajuela.

## Licencia

[GNU Free Documentation License Version 1.3](https://www.gnu.org/licenses/fdl-1.3-standalone.html).