---
title: Google Calendar para Slack
subtitle: Apps para Slack
date: 2021-06-21
draft: false
description: Conectar Google Calendar con Slack os permite a ti y a tu equipo organizaros y estar al día con las notificaciones, recordatorios de eventos y mucho más
tags: ["Slack","Google","Productividad","Aplicaciones"]
---

<!-- vscode-markdown-toc -->
* 1. [Google Calendar para Slack](#GoogleCalendarparaSlack)
	* 1.1. [Añade la app Google Calendar a Slack](#AadelaappGoogleCalendaraSlack)
	* 1.2. [Conectar Google Calendar a Slack](#ConectarGoogleCalendaraSlack)
	* 1.3. [Ajustes de la app](#Ajustesdelaapp)
	* 1.4. [Cómo usar Google Calendar en Slack](#CmousarGoogleCalendarenSlack)
		* 1.4.1. [Crear un evento](#Crearunevento)
		* 1.4.2. [Consulta tu programación y responde a los eventos](#Consultatuprogramacinyrespondealoseventos)
		* 1.4.3. [Actualiza automáticamente tu estado de Slack](#ActualizaautomticamentetuestadodeSlack)
		* 1.4.4. [Cómo modificar los ajustes de las notificaciones](#Cmomodificarlosajustesdelasnotificaciones)
		* 1.4.5. [Conectar calendarios adicionales](#Conectarcalendariosadicionales)
		* 1.4.6. [Cómo desconectar un calendario](#Cmodesconectaruncalendario)
		* 1.4.7. [Comandos](#Comandos)
		* 1.4.8. [Eliminar la aplicación](#Eliminarlaaplicacin)
	* 1.5. [Google Calendar para Team Events](#GoogleCalendarparaTeamEvents)
* 2. [Enlaces internos](#Enlacesinternos)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='GoogleCalendarparaSlack'></a>Google Calendar para Slack

Esta aplicación es genial, **te permite ver tus citas del día sin salir de Slack y te notifica cuando una reunión o evento está próximo**.

###  1.1. <a name='AadelaappGoogleCalendaraSlack'></a>Añade la app Google Calendar a Slack

Accede a la [página de Google Calendar](https://slack.com/apps/ADZ494LHY-google-calendar) en el Directorio de Aplicaciones de Slack (también es accesible desde el propio Slack) y Haz clic en **Añadir a Slack**. 

![](img/01.PNG)

Haz clic en **Permitir** para conceder acceso a Google Calendar a Slack. 

![](img/02.PNG)

Cuando te lo pidan, haz clic en **Open Slack (Abrir Slack)** para volver al espacio de trabajo.  

###  1.2. <a name='ConectarGoogleCalendaraSlack'></a>Conectar Google Calendar a Slack

Cuando la aplicación Google Calendar esté instalada en tu espacio de trabajo, sigue los pasos que se indican a continuación para conectar la cuenta.

En la barra lateral izquierda busca en la parte inferior la sección "Aplicaciones", busca y selecciona Google Calendar para abrir la app (¿Este paso es necesario? Creo que conecta al calendario de la cuenta que estemos usando).  

![](img/03.PNG)

###  1.3. <a name='Ajustesdelaapp'></a>Ajustes de la app

![](img/05.PNG)

Desde los ajustes dentro de la app en Slack podemos:

* **Recibir un mensaje en Slack con el resumen de las citas del calendario cada día**, puedes ajustar a que hora quieres que te mande el mensaje.
* Permitir a Slack **cambiar tu estado de forma automática durante un evento** para que tus compañeros sepan que no estas disponible.
* **Configurar los recordatorios** en Slack para que te notifique X minutos antes de comenzar una reunión, etc.

###  1.4. <a name='CmousarGoogleCalendarenSlack'></a>Cómo usar Google Calendar en Slack

####  1.4.1. <a name='Crearunevento'></a>Crear un evento

![](img/04.PNG)

Desde dentro de la aplicación de "Google Calendar" sin salir de Slack podemos crear una cita nueva.

![](img/06.PNG)

Los usuarios invitados recibirán un mensaje en Slack (si tienen la app de Google Calendar) y mediante correo electrónico, los invitados pueden responder a tu evento desde ese mensaje o desde sus calendarios.

En la zona de mensajes de la App se mostrarán mensajes con las citas que vamos creando.

####  1.4.2. <a name='Consultatuprogramacinyrespondealoseventos'></a>Consulta tu programación y responde a los eventos

Desde la pestaña de Inicio, haz clic en Hoy, Mañana o  Seleccionar una fecha para ver todos los eventos de un día en particular. 

Para ver los detalles de algún evento que se vaya a realizar pronto, como la ubicación, lista de invitados y descripción, haz clic en el  icono de tres puntos de la derecha y selecciona Ver detalles del evento.

Abre la pestaña de Mensajes de la aplicación para responder o actualizar las respuestas a eventos.

####  1.4.3. <a name='ActualizaautomticamentetuestadodeSlack'></a>Actualiza automáticamente tu estado de Slack

Utilizando tu calendario, la app de Google Calendar configurará tu estado de Slack de forma automática mientras estás en una reunión. 

* Tu estado no incluirá el nombre de la reunión a la que estás asistiendo. 
* Puedes eliminar o actualizar tu estado cuando quieras (la app Google Calendar no sobrescribirá ningún estado que hayas configurado tú). 
* Solamente los eventos que aceptes o que marquen tu disponibilidad como Ocupado activarán una actualización del estado en Slack. 
* Si hay eventos que coincidan en tu calendario, Slack configurará tu estado para que coincida con el evento más largo o con el que empiece antes.

####  1.4.4. <a name='Cmomodificarlosajustesdelasnotificaciones'></a>Cómo modificar los ajustes de las notificaciones

Puedes personalizar los ajustes de las notificaciones de Google Calendar en Slack.

![](img/07.PNG)

####  1.4.5. <a name='Conectarcalendariosadicionales'></a>Conectar calendarios adicionales

Si quieres recibir actualizaciones de otros calendarios que se hayan compartido contigo, abre la aplicación Calendar en Slack. En la pestaña Inicio de la aplicación, haz clic en Ajustes, en Cuentas, selecciona un calendario del menú desplegable.

####  1.4.6. <a name='Cmodesconectaruncalendario'></a>Cómo desconectar un calendario  

Dentro de la app de Calendar en la sección ajustes:

![](img/08.PNG)

####  1.4.7. <a name='Comandos'></a>Comandos

```
/gcal[today][tomorrow][settings]
```

Ejemplos:

* `/gcal today` o `/gcal tomorrow`: permite ver el resumen de tus citas para hoy o mañana.
* `/gcal`: para acceder a preferencias y ajustes. 

####  1.4.8. <a name='Eliminarlaaplicacin'></a>Eliminar la aplicación

###  1.5. <a name='GoogleCalendarparaTeamEvents'></a>Google Calendar para Team Events 


##  2. <a name='Enlacesinternos'></a>Enlaces internos

* Meet for Slack [https://soka.gitlab.io/blog/post/2021-04-26-meet-for-slack/](https://soka.gitlab.io/blog/post/2021-04-26-meet-for-slack/).
* Slack - Comunicación colaborativa para el trabajo en equipo [https://soka.gitlab.io/blog/post/2021-04-01-taller-slack-spri-enpresa-digitala/](https://soka.gitlab.io/blog/post/2021-04-01-taller-slack-spri-enpresa-digitala/).

##  3. <a name='Enlacesexternos'></a>Enlaces externos

* [https://slack.com/app-pages/google-calendar](https://slack.com/app-pages/google-calendar).
* [https://slack.com/intl/es-es/help/articles/206329808-Google-Calendar-para-Slack](https://slack.com/intl/es-es/help/articles/206329808-Google-Calendar-para-Slack).