---
title: Actividad práctica Módulo 0
subtitle: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 0
date: 2022-04-28
draft: false
description: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 0
tags: ["PEPHPPAW_3","Cursos","Miriadax","Programación","Web","Desarrollo","HTML","CSS","Bootstrap"]
---

A continuación encontrarás la actividad práctica voluntaria a desarrollar correspondiente al módulo 0.

Construye una página web en formato HTML y CSS, con las siguientes características:

Título de la página, centrado, color azul y en negrita,

Subtitulo de la página, alineado a la izquierda, color negro y en negrita.

Dos (2) párrafos, en el primero defines el concepto del lenguaje HTML, usando color rojo y en el segundo presentas la importancia del lenguaje HTML, en cursiva y color verde.

Al final debes colocar tu nombre, subrayado.

