---
title: RSSOwl lector de feeds y Mastodon   
subtitle: Aplicación de escritorio para Win, Mac y Gnu/Linux
date: 2018-11-20
draft: false
description: Como configurar para leer los feeds de Mastodon
tags: ["Aplicaciones", "RSSOwl","RSS","Atom"]
---

![](images/RSSOwl-logo-transparente.png) 

[**RSSOwl**](http://www.rssowl.org/) es un lector de feeds. Es una aplicación gráfica de escritorio gratuita distribuida bajo [licencia pública]((https://github.com/rssowl/RSSOwl/blob/master/LICENSE)), está disponible Win, Mac y Linux. 

Algo de lo que me acabo de dar cuenta es que el proyecto esta "abandonado" en GitHub aunque funcione no se si es muy recomendable. 

![](images/rsswol-main-web.PNG)

Una vez [descargada](http://www.rssowl.org/#download) e instalada abrimos la aplicación:

![](images/rsswol-app-1.PNG)

El panel izquierdo permite ordenar los diferentes origenes de información en carpetas y a su vez en subcarpetas. Añadir un nuevo feed es trivial, seleccionamos la carpeta donde queremos guardarlo y con el botón derecho del ratón seleccionamos la opción "New > Feed". Si hemos copiado la dirección o URL del feed cuando entremos al formulario para añadir nuevo feed autocompleta la dirección sin necesidad de pegarla. 

![](images/new-feed-1.PNG)


# Enlaces

* [RSSOwl: Cliente Win lector RSS con GitLab](https://ikerlandajuela.wordpress.com/2018/05/09/rssowl-cliente-win-lector-rss-con-gitlab/).
