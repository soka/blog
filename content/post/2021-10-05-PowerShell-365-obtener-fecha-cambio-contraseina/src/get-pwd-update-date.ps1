$UserCredential = Get-Credential
Connect-MsolService -Credential $UserCredential

Get-MsolUser -All | select DisplayName,LastPasswordChangeTimeStamp | Export-CSV LastPasswordChangeDate.csv -NoTypeInformation
