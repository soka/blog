---
title: PowerShell 365 obtener fecha último cambio contraseñas usuarios
subtitle: Exportar a CSV la fecha del último cambio contraseñas de las cuentas Office 365
date: 2021-10-05
draft: false
description: Exportar a CSV la fecha del último cambio contraseñas de las cuentas Office 365
tags: ["Sysadmin","PowerShell","Office 365","Azure","Scripting","Contraseñas","Seguridad"]
---

<!-- vscode-markdown-toc -->
* 1. [Enlaces internos](#Enlacesinternos)
* 2. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Desde el centro de administración de MS 365 se puede establecer una [directiva de expiración de contraseñas](https://docs.microsoft.com/es-ES/microsoft-365/admin/misc/password-policy-recommendations?WT.mc_id=365AdminCSH_inproduct&view=o365-worldwide#password-expiration-requirements-for-users) ("Inicio > Configuración > Configuración de la organización" pestaña "Seguridad y privacidad").

![](img/02.PNG)

**La periodicidad de renovación dependerá de la criticidad de la información que maneja una cuenta**, en general 90 días puede ser un periódo adecuado.

Antes de empezar instalar el módulo [MSOnline](https://docs.microsoft.com/en-us/powershell/module/msonline/?view=azureadps-1.0) (utilidades Azure Active Directory) abriendo la consola PS como admin:

```ps
Install-Module MSOnline
```

[`Get-Credential`](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/get-credential?view=powershell-7.1) crea un objeto para manejar credenciales que solicita:

```ps
$UserCredential = Get-Credential
Connect-MsolService -Credential $UserCredential
```

![](img/01.PNG)


El cmdlet en cuestión es  [`Get-MsolUser`](https://docs.microsoft.com/en-us/powershell/module/msonline/get-msoluser?view=azureadps-1.0):

```ps
Get-MsolUser -All | select DisplayName,LastPasswordChangeTimeStamp | Export-CSV LastPasswordChangeDate.csv -NoTypeInformation
``` 

Cmdlet: [src/get-pwd-update-date.ps1](src/get-pwd-update-date.ps1).

##  1. <a name='Enlacesinternos'></a>Enlaces internos

* [Publicaciones internas dentro de la categoría PowerShell](https://soka.gitlab.io/blog/tags/powershell/).

##  2. <a name='Enlacesexternos'></a>Enlaces externos

* [Recomendaciones de directiva de contraseñas](https://docs.microsoft.com/es-ES/microsoft-365/admin/misc/password-policy-recommendations?WT.mc_id=365AdminCSH_inproduct&view=o365-worldwide#password-expiration-requirements-for-users).
* [Export Office 365 Users’ Last Password Change Date to CSV](https://o365reports.com/2020/02/17/export-office-365-users-last-password-change-date-to-csv/).
* [Get-Credential](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/get-credential?view=powershell-7.1).
* [MSOnline](https://docs.microsoft.com/en-us/powershell/module/msonline/?view=azureadps-1.0).
