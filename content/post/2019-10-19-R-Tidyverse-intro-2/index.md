---
title: Introducción a Tidyverse (2) [borrador]
subtitle: Programación en R
date: 2019-10-19
draft: true
description: Ciencia de datos con R
tags: ["R","R - Tidyverse","R - Paquetes","R - DataExplorer","R - ggplot","R - Diagramas"]
---

![](images/hex-tidyverse.png)

## Combinar gráficos y añadir dimensiones de visualización

**Los gráficos tienen que ser auto-explicativos**, usar el color para visualizar una variable discreta "necesita" una leyenda para poder interpretar el diagrama:

```r
ggplot(data=mpg) +
  geom_smooth(mapping = aes(x=displ,y=hwy,color=drv),
              show.legend=T)
```

![](images/01.png)

Cuando combinamos varias geometrías estamos duplicando mucho código con el `mapping`, en estos casos se puede definir el `mapping` global en la función `ggplot`:

```r
ggplot(data=mpg,mapping = aes(x=displ,y=hwy,color=drv)) +
  geom_point() + 
  geom_smooth()
```

Se pueden seguir definiendo detalles adicionales para cada geometría:

```r
ggplot(data=mpg,mapping = aes(x=displ,y=hwy)) +
  geom_point(mapping=aes(color=drv)) + 
  geom_smooth()
```

Un último ejemplo con más opciones:

```r
ggplot(data=mpg,mapping = aes(x=displ,y=hwy)) +
  geom_point(mapping=aes(fill=drv),size=4,shape=23,
             col="white",stroke=2) 
```

![](images/02.png)

## Transformaciones estadísticas básicas

### Diagrama de barras

En este caso sólo tenemos que definir la dimensión x usando una variable discreta con las categorías que queremos visualizar:

```r
ggplot(data = diamonds) + 
  geom_bar(mapping = aes(x = cut))
```

![](images/03.png)

El diagrama de barras calcula o infiere los valores agrupados por variable, por defecto la altura de cada barra representa el número de observaciones para esa variable, definido como "count" en el parámetro `stat`:

```r
geom_bar(mapping = NULL, data = NULL, stat = "count",
  position = "stack", ..., width = NULL, binwidth = NULL,
  na.rm = FALSE, show.legend = NA, inherit.aes = TRUE)
```

Toda geometría tiene un transformación estadística subyacente. 

## Cambiando las transformaciones estadísticas

Queremos representar el tipo de corte basado en las frecuencias de cada una de ellos.


## Código fuente

- r / intro / 56-Tidyverse / [Tidyverse.R](https://gitlab.com/soka/r/blob/master/intro/56-Tidyverse/Tidyverse.R).

## Enlaces internos

- [ggplot2-cheatsheet-2.1-Spanish.pdf](./ggplot2-cheatsheet-2.1-Spanish.pdf).
- [Artículos sobre ggplot2](https://soka.gitlab.io/blog/tags/r-ggplot2/) en mi blog.

## Enlaces externos
