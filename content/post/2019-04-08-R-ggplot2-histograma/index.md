---
title: Histogramas
subtitle: Introducción a ggplot2
date: 2019-04-08
draft: false
description: Manipulación de datos y análisis estadístico
tags:
  [
    "R",
    "Programación",
    "R - Básico",
    "R - Gráficos",
    "R - Diagramas",
    "R - ggplot2",
    "Diagramas",
  ]
---

![](images/ggplot_hex.jpg)

Comenzamos como en otros tipos de diagramas con el ejemplo más sencillo:

```R
ggplot(data = iris,aes(x=Sepal.Width)) + geom_histogram()
```

![](images/ggplot2-histogram-01.PNG)

El resultado no es muy espectacular, ahora usamos el parámetro `binwidth` de `geom_histogram` para definir los intervalos o como se agrupan. El resultado es:

```R
ggplot(data = iris,aes(x=Sepal.Width)) + geom_histogram(binwidth = 0.2)
```

![](images/ggplot2-histogram-02.PNG)

Otra cosa que puedo hacer es definir el color de la línea de las barras como sigue:

```R
ggplot(data = iris,aes(x=Sepal.Width)) + geom_histogram(binwidth = 0.2,color="black")
```

![](images/ggplot2-histogram-03.PNG)

Ahora voy a modificar el color del interior de las barras:

```R
ggplot(data = iris,aes(x=Sepal.Width)) + geom_histogram(binwidth = 0.2,color="black",fill="steelblue")
```

![](images/ggplot2-histogram-04.PNG)

Sigo agregando cosas, ahora el título del diagrama:

```R
ggplot(data = iris,aes(x=Sepal.Width)) + geom_histogram(binwidth = 0.2,color="black",fill="steelblue") +
  ggtitle("Histograma")
```

![](images/ggplot2-histogram-05.PNG)

Añadimos también descripción de los ejes:

```R
ggplot(data = iris,aes(x=Sepal.Width)) + geom_histogram(binwidth = 0.2,color="black",fill="steelblue") +
  ggtitle("Histograma") +
  xlab("Ancho petalo") +
  ylab("Cantidad")
```

# Curva de densidad

```R
ggplot(data = iris,aes(x=Sepal.Width)) +
  geom_histogram(binwidth = 0.2,color="black",fill="steelblue", aes(y=..density..)) +
  ggtitle("Histograma") +
  xlab("Ancho petalo") +
  ylab("Cantidad") +
  geom_density(stat="density",alpha=I(0.2),fill="blue")
```

![](images/ggplot2-histogram-06.PNG)

# Ejercicios

- r / intro / 33-ggplot2-histograma / [ggplo2-histogram.R](https://gitlab.com/soka/r/blob/master/intro/33-ggplot2-histograma/ggplo2-histogram.R).

# Enlaces internos

- ["Histogramas con hist()"](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-basicos-hist/).

**ggplot2:**

- ["Gráficos avanzados en R - Introducción a ggplot2"](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-avanzados-ggplot2-1/).
- ["geom_line() - Gráficos de líneas"](https://soka.gitlab.io/blog/post/2019-04-01-r-ggplot2-grafico-lineas/).
- ["geom_bar() - Gráficos de barras"](https://soka.gitlab.io/blog/post/2019-04-08-r-ggplot2-grafico-barras/).

# Enlaces externos
