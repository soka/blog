---
title: Una nueva herramienta para buscar de forma segura en Google
subtitle: 
date: 2021-07-21
draft: true
description: 
tags: ["Google","Búsquedas","Search","Google Search"]
---


[https://myactivity.google.com/myactivity](https://myactivity.google.com/myactivity)

![](img/01.PNG)



## Enlaces internos

* Artículos con etiqueta Google [https://soka.gitlab.io/blog/tags/google/](https://soka.gitlab.io/blog/tags/google/).

## Enlaces externos

* A new tool (and some tips) to search safely with Google [https://blog.google/products/search/new-privacy-tool-search/](https://blog.google/products/search/new-privacy-tool-search/).