---
title: Medidas contra correos fraudulentos
subtitle: Tip ciberseguridad Nº 1
date: 2021-06-24
draft: false
description: 
tags: ["Ciberseguridad","Correo","Fraudes","Phising","Scam","Malware","Tips","Seguridad"]
---

<!-- vscode-markdown-toc -->
* 1. [Introducción](#Introduccin)
* 2. [Tipos de correos fraudulentos](#Tiposdecorreosfraudulentos)
	* 2.1. [Phising](#Phising)
		* 2.1.1. [Algunos ejemplos](#Algunosejemplos)
	* 2.2. [Scam](#Scam)
	* 2.3. [Sextorsión](#Sextorsin)
	* 2.4. [Malware](#Malware)
* 3. [Consejos básicos para detectar correos falsos:](#Consejosbsicosparadetectarcorreosfalsos:)
	* 3.1. [Remitentes desconocidos](#Remitentesdesconocidos)
	* 3.2. [Remitentes falseados](#Remitentesfalseados)
	* 3.3. [Correos en otro idioma](#Correosenotroidioma)
	* 3.4. [Asunto del mensaje](#Asuntodelmensaje)
	* 3.5. [Faltas de ortografía y estilo](#Faltasdeortografayestilo)
	* 3.6. [Busca la urgencia](#Buscalaurgencia)
	* 3.7. [El correo solicita información personal](#Elcorreosolicitainformacinpersonal)
	* 3.8. [Ofertas laborales y económicas poco probables](#Ofertaslaboralesyeconmicaspocoprobables)
	* 3.9. [Enlaces](#Enlaces)
	* 3.10. [Adjuntos](#Adjuntos)
	* 3.11. [Evita divulgar información por correo](#Evitadivulgarinformacinporcorreo)
* 4. [Resumen de herramientas](#Resumendeherramientas)
	* 4.1. [Antivirus y analizadores de Webs sospechosas en línea](#AntivirusyanalizadoresdeWebssospechosasenlnea)
	* 4.2. [Comprobar si nuestro correo se ha visto involucrado en una brecha de seguridad](#Comprobarsinuestrocorreosehavistoinvolucradoenunabrechadeseguridad)
	* 4.3. [Navegadores](#Navegadores)
		* 4.3.1. [HTTPS EveryWhere, extensión para navegadores](#HTTPSEveryWhereextensinparanavegadores)
	* 4.4. [Reportes y sitios](#Reportesysitios)
* 5. [Conclusiones](#Conclusiones)
* 6. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Introduccin'></a>Introducción

**El correo electrónico es una herramienta de comunicación indispensable en la empresa**, el número de correos y usuarios en el mundo crece cada año a pesar de otros sistemas de comunicación como los sistemas de mensajería, chats y videollamadas ([Email Statistics Report, 2015-2019 - Radicati Group](https://www.radicati.com/wp/wp-content/uploads/2015/02/Email-Statistics-Report-2015-2019-Executive-Summary.pdf)).

Además **el correo forma parte integral de la experiencia de usuario en Internet** ya que normalmente usamos las cuentas de correo como sistema de autenticación en redes sociales, aplicaciones, etc.

![](img/15.PNG)

Como cualquier herramienta de comunicación **es necesario definir su uso correcto y seguro**. 

Actualmente el correo es **uno de los medios más utilizados por los ciberdelincuentes ganadose la confianza o del desconocimiento del destinatario.**

Es habitual que recibamos en nuestros correos corporativos _spam_, correos tipo _phising_ suplantando a entidades, etc. En estos casos los atacantes usan lo que se conoce como "ingenieria social" para ganarse nuestra confianza, el **objetivo suele ser apoderarse de claves de acceso y/o de información sensible**. 

Algunos titulares a modo de resumen de cómo están las cosas: 

* De acuerdo al [FBI](https://www.ic3.gov/Media/PDF/AnnualReport/2020_IC3Report.pdf) el **phising fue el tipo más común de cibercrimen** en 2020 (241.324 casos), casi duplican el número de casos en 2019 (114.702 casos). 
* El 74% de las empresas en EEUU han sufrido un ataque de _phising_ con éxito.
* Según un reporte de Eset la amenaza más detectada en adjuntos de correos aprovecha una vulnerabilidad en el editor de ecuaciones de Microsoft ( [CVE-2017-11882](https://wsg127.com/vulnerabilidad-microsoft/)) usando archivos de Office adjuntos ([prueba de concepto en GitHub](https://github.com/embedi/CVE-2017-11882)). 

Correos fraudulentos que usan empresas de logística como DHL y Correos se han vuelto frecuentes, en el correo un enlace para el _tracking_ de un supuesto paquete nos lleva a una Web fraudulenta (el enlace visible no tiene porqué ser el real), ultimamente también se están usando entidades como la [Agencia Tributaria](https://www.agenciatributaria.gob.es/AEAT.sede/Inicio/_pie_/_Aviso_de_seguridad_/_Aviso_de_seguridad_.shtml) (consulta [aquí](https://www.agenciatributaria.es/AEAT.internet/Inicio/Ayuda/_comp_Consultas_informaticas/_Informacion_de_casos_de_Phishing_/_Informacion_de_casos_de_Phishing_.shtml) casos de _phising_ suplantando la Agencia Tributaria), operadores de telecomunicaciones y proveedores como Endesa o Iberdrola, es más fácil engañar a los usuarios ya que son empresas e instituciciones con presencia estatal y probablemente muchos de los objetivos son clientes de estos servicios.

![](img/16.png)

##  2. <a name='Tiposdecorreosfraudulentos'></a>Tipos de correos fraudulentos

###  2.1. <a name='Phising'></a>Phising

Se trata de un engaño donde el **atacante suplanta una entidad, empresa o persona** (bancos, tiendas en línea, gobierno, etc).

####  2.1.1. <a name='Algunosejemplos'></a>Algunos ejemplos

**Correo electrónico suplantando a PayPal**

Correos suplantando a Paypal cuyo objetivo es que accedamos a una Web fraudulenta que suplanta la original (_phishing_) con el objetivo de robar nuestras credenciales.

![](img/01.jpg)

En este caso la dirección del **remitente no coincidia con el dominio de la entidad** (paypal.com), en el cuerpo del correo se dirige al usuario como "cliente", normalmente una entidad en sus comunicaciones oficiales se dirige a nosotros por nombre y apellido. El correo nos invita a pinchar en un enlace (tampoco es lo normal en estos casos, si sospechamos podemos ir por nuestra cuenta a la Web de la entidad mejor)  que nos redirige a una página Web que suplanta ser legítima.

**Otra pista que nos puede hacer sospechar es el candado rojo en la barra del navegador**:

![](img/02.jpg)

**Enlaces**:

* [https://www.incibe.es/protege-tu-empresa/avisos-seguridad/detectada-campana-phishing-paypal](https://www.incibe.es/protege-tu-empresa/avisos-seguridad/detectada-campana-phishing-paypal).
* [https://computerhoy.com/noticias/tecnologia/email-paypal-phishing-estafa-robar-datos-700319](https://computerhoy.com/noticias/tecnologia/email-paypal-phishing-estafa-robar-datos-700319).

**Campaña phising suplantando a ING**

![](img/04.jpg)

Este es un caso similar al anterior, mediante un enlace nos solicitan actualizar nuestros datos personales.

* [https://www.incibe.es/protege-tu-empresa/avisos-seguridad/cuidado-no-piques-campana-phishing-suplantando-ing](https://www.incibe.es/protege-tu-empresa/avisos-seguridad/cuidado-no-piques-campana-phishing-suplantando-ing)

Existen muchos más casos, suplantando a la [Agencia Tributaria](https://www.incibe.es/protege-tu-empresa/avisos-seguridad/nueva-campana-correos-fraudulentos-suplantan-agencia-tributaria) o a proveedores como [Endesa](https://www.incibe.es/protege-tu-empresa/avisos-seguridad/campana-phishing-suplanta-falsas-devoluciones-endesa-0) ofreciendo una falsa devolución de una factura.

[Cuidado con el Prime Day: 80% de sitios web con la palabra Amazon son peligrosos](https://www.unocero.com/noticias/cibercriminales-aprovechar-prime-day-2021-sitios-falsos/)

La URL falsa aparece como amazon[.]update-prime[.]pop2[.]live.

![](img/14.png)

**Enlaces**

* [https://blog.checkpoint.com/2021/06/16/cybercriminals-go-after-amazon-prime-day-shoppers/](https://blog.checkpoint.com/2021/06/16/cybercriminals-go-after-amazon-prime-day-shoppers/)

###  2.2. <a name='Scam'></a>Scam

En la mayoría de los casos es una estafa económica, por ejemplo un falso premio de lotería o algún concurso que hemos ganado (sin participar en muchos casos) que nos solicita desembolsar previamente una cantidad.

Alguna de estas es tan común o conocida que tiene su propio nombre, la [estafa nigeriana](https://es.wikipedia.org/wiki/Estafa_nigeriana), timo nigeriano o timo 419.

Este ejemplo es muy común, alguien que se presenta así mismo como representante de una entidad bancaria:

![](img/05.png)

Otro ejemplo de la estafa nigeriana, un banquero que te quiere dar parte de una herencia de un petrolero ruso, para ser banquero muy bien no escribe, ojo a la dirección de correo ([fuente](https://www.abc.es/sociedad/abci-guardia-civil-alerta-sobre-estafa-nigeriana-201903141132_noticia.html)):

![](img/08.png)

**Enlaces**:

* [https://www.incibe.es/protege-tu-empresa/blog/historias-reales-galan-vacio-las-cuentas-mi-empresa](https://www.incibe.es/protege-tu-empresa/blog/historias-reales-galan-vacio-las-cuentas-mi-empresa).
* [welivesecurity.com/la-es/2012/01/11/estafa-nigeriana-419-y-latinoamericana/](welivesecurity.com/la-es/2012/01/11/estafa-nigeriana-419-y-latinoamericana/).
* [https://es.wikipedia.org/wiki/Estafa_nigeriana](https://es.wikipedia.org/wiki/Estafa_nigeriana).

###  2.3. <a name='Sextorsin'></a>Sextorsión

_"El  objetivo  es extorsionar al receptor con un supuesto  video privado o de contenido comprometedor (que  lo  más  probable es que no exista), amenazando con difundirlo a todos sus contactos de correo y redes sociales a no ser que realice el pago  de una cantidad económica. El pago, generalmente, es solicitado en criptomonedas como el bitcoin y el destinatario parece ser el  remitente cuando en realidad, no es así"_

Ejemplo: En el correo, el ciberdelincuente indica que mediante un tipo de malware ha infectado el equipo de la víctima, que tiene control total del mismo y que lleva un tiempo monitorizándolo. Para hacer el fraude más creíble **falsea la dirección del remitente haciendo que parezca que el correo se ha enviado desde la cuenta del destinatario**.

![](img/09.jpg)

**Enlaces**:

* [https://www.incibe.es/protege-tu-empresa/avisos-seguridad/campana-correos-extorsionan-supuestos-videos-privados](https://www.incibe.es/protege-tu-empresa/avisos-seguridad/campana-correos-extorsionan-supuestos-videos-privados).

###  2.4. <a name='Malware'></a>Malware

En  este  caso,  se  trata  de  código  malicioso  que  podría  infectar  los  dispositivos.  Los  correos  podrían  contener  algún  tipo  de **archivo  adjunto o enlaces a webs donde una vez descargado y  ejecutado el fichero infectará el dispositivo**.

![](img/10.png)

**Algunas herramientas**:

* [**VirusTotal**](https://www.virustotal.com/gui/): Permite analizar archivos y direcciones de páginas Web sospechosas.
* [**ESET Online Scanner**](https://www.eset.com/es/hogar/online-scanner/#content-c2387532) es una aplicación gratuita, requiere descarga pero no es necesario instalarla, para usos puntuales.

**Enlaces**:

* [https://www.incibe.es/protege-tu-empresa/avisos-seguridad/envio-falsos-presupuestos-excel-adjuntos-maliciosos](https://www.incibe.es/protege-tu-empresa/avisos-seguridad/envio-falsos-presupuestos-excel-adjuntos-maliciosos).

**Nota**: el _ransomware_ un tipo de _malware_ que cifra la información del ordenador o impide el acceso a la misma, amenazando al usuario con destruirla, si no paga un rescate (ransom).

Infografía resumen del OSI [Técnicas de ingeniería social: ¿Cómo consiguen engañarnos?](https://www.osi.es/es/campanas/ingenieria-social/tecnicas-ingenieria-social):

![](img/13.jpg)

##  3. <a name='Consejosbsicosparadetectarcorreosfalsos:'></a>Consejos básicos para detectar correos falsos:

![](img/11.PNG)

###  3.1. <a name='Remitentesdesconocidos'></a>Remitentes desconocidos

En general,  hay  que  **sospechar  de  los  mensajes  cuyo  remitente  sea  desconocido**  y  comprobarlo  por  otro  medio, como por teléfono.

Generalmente cuando se trata de correos falsos, genéricos y que buscan infectar nuestro sistema suelen tener una dirección extraña. Por ejemplo muchos números y letras sin sentido o incluso intentando imitar una dirección legítima (remitentes falseados).

###  3.2. <a name='Remitentesfalseados'></a>Remitentes falseados

En  otras  ocasiones, los  ciberdelincuentes  falsifican  la  dirección  del  remitente  haciendo  que,  a simple vista, no se identifique el correo como fraudulento

Algo fundamental para determina si un correo que hemos recibido es falso o puede ser una amenaza es **observar bien el nombre del dominio**.

Es importante revisar cada carácter, pues también son frecuentes suplantaciones cambiando alguna letra o utilizando un carácter de grafía similar o que suene igual.

¿El correo NO-REPLY@MlCROSOFT.COM parece legítimo no?  Si vemos con detalle este correo, la “i” de Microsoft no es una “i” sino una l minúscula.

En  otras  ocasiones, los  ciberdelincuentes  **falsifican  la  dirección  del  remitente**  haciendo  que,  a simple vista, no se identifique el correo como fraudulento. Esta técnica se conoce como **_email spoofing_**. En este caso, para comprobar si el correo es legítimo es necesario analizar las cabeceras del mismo. Esta entre otra mucha información puede servir para saber si el correo procede, realmente, de la dirección que figura  en el remitente o ha sido falseada.

* [https://toolbox.googleapps.com/apps/messageheader/analyzeheader](https://toolbox.googleapps.com/apps/messageheader/analyzeheader).

###  3.3. <a name='Correosenotroidioma'></a>Correos en otro idioma

Muchos de los ataques realizados por este medio son dirigidos a un solo país o países, por lo que el mismo utiliza el lenguaje utilizado en dicho/s sector/es. **Es muy poco probable recibir un correo en el idioma que no sea el propio a menos que estemos esperando el mismo ya sea por temas laborales o personales**.

###  3.4. <a name='Asuntodelmensaje'></a>Asunto del mensaje

En el **asunto del mensaje** también podemos detectar posibles anomalías. Pongamos que recibimos un e-mail con un asunto que nos hace sospechar, **dirigido de forma genérica, con un nombre que no concuerda, etc**.  **Las entidades legítimas en las comunicaciones suelen utilizar el nombre y apellidos del destinatario**, haciendo que la comunicación sea más personal.

###  3.5. <a name='Faltasdeortografayestilo'></a>Faltas de ortografía y estilo

Fijarse en la redacción de los mensajes sospechosos recibidos, suelen estar **mal escritos o con faltas ortográficas**.

En muchas ocasiones los piratas informáticos envían correos genéricos a muchos usuarios. Utilizan básicamente la misma información traducida a varios idiomas y eso lo podemos apreciar. Es un indicativo de que estamos ante un intento de estafa.

###  3.6. <a name='Buscalaurgencia'></a>Busca la urgencia

Abuso de los signos de exclamación que exigen una llamada a la acción inmediata, nos piden realizar alguna acción con urgencia, por ejemplo la cancelación o cierre de un servicio o que hemos sido victimas de un ataque.

###  3.7. <a name='Elcorreosolicitainformacinpersonal'></a>El correo solicita información personal

###  3.8. <a name='Ofertaslaboralesyeconmicaspocoprobables'></a>Ofertas laborales y económicas poco probables

Nadie da duros por cuatro pesetas, un poco de sentido común por favor.

###  3.9. <a name='Enlaces'></a>Enlaces

**Pasa el ratón por encima de los enlaces que incluye el correo sin hacer clic en ellos** para comprobar a qué dirección te dirigen sin ponerte en riesgo.

![](img/25.png)

¿La dirección https://adl.incibe.es.otrodominio.com es válida si estamos tratando de acceder a una Web del INCIBE?

Otra variedad es el _cybersquatting_ donde los ciberdelincuentes compran el mismo dominio pero con una extensión libre, por ejemplo "microsoft.eus".

###  3.10. <a name='Adjuntos'></a>Adjuntos

**Cualquier documento adjunto en un correo electrónico debe ser una señal de alerta**.Por norma, ninguna entidad ya sea bancaria, pública, energética, etc., envía a sus destinatarios documentos adjuntos en el correo. Si es necesario que se descargue algún archivo se hará desde su portal web o aplicación oficial.

Extensiones sospechosas: .exe (ejecutable de Windows), .vbs, .docm (Word con macros), .xlsm, .pptm.

También hay que tener cuidado con ficheros comprimidos como los .zip o .rar, ya que pueden contener archivos maliciosos, como los anteriores, en su interior, se suelen comprimir para evitar que los antivirus del servidor de correo los detecte. Lo mejor es usar siempre un servicio como [VirusTotal](https://www.virustotal.com/gui/) para cerciorarnos que el archivo está libre de amenazas.

###  3.11. <a name='Evitadivulgarinformacinporcorreo'></a>Evita divulgar información por correo

Usa las opciones CC y CCO correctamente.

[Infografía del OSI](https://www.osi.es/es/actualidad/blog/2016/10/25/aprende-identificar-correos-fraudulentos-mediante-una-infografia) que resume como identificar correos fraudulentos:

![](img/12.png)

##  4. <a name='Resumendeherramientas'></a>Resumen de herramientas

Por supuesto una de las medidas más importantes es tener un antivirus profesional instalado en nuestro equipo, hoy en día son capaces de alertar cuando la página Web a la que vamos a acceder es insegura o contiene _malware_.

###  4.1. <a name='AntivirusyanalizadoresdeWebssospechosasenlnea'></a>Antivirus y analizadores de Webs sospechosas en línea

[**VirusTotal**](https://www.virustotal.com/gui/)

Permite analizar archivos y direcciones de páginas Web sospechosas.

[**ESET Online Scanner**](https://www.eset.com/es/hogar/online-scanner/#content-c2387532) es una aplicación gratuita, requiere descarga pero no es necesario instalarla, para usos puntuales.

![](img/06.PNG)

###  4.2. <a name='Comprobarsinuestrocorreosehavistoinvolucradoenunabrechadeseguridad'></a>Comprobar si nuestro correo se ha visto involucrado en una brecha de seguridad

###  4.3. <a name='Navegadores'></a>Navegadores

Hoy en día todos los navegadores alertan si una página es segura

[http://www.baidu.com/](http://www.baidu.com/)

![](img/03.PNG)

####  4.3.1. <a name='HTTPSEveryWhereextensinparanavegadores'></a>HTTPS EveryWhere, extensión para navegadores

Se puede instalar como extensión para la mayoría de navegadores Web.

###  4.4. <a name='Reportesysitios'></a>Reportes y sitios 

[https://phishunt.io/](https://phishunt.io/) es una Web interesante, analizan y monitorizan Webs sospechosas y las reportan en listas (Ejemplo: [https://amazon.zkiwj.cn/404.html](https://amazon.zkiwj.cn/404.html))

##  5. <a name='Conclusiones'></a>Conclusiones

La tecnología puede ayudar a defendernos de algunos estos ataques pero cuando la victima es una persona es más complicado ya que estas técnicas se aprovechan del miedo, de la urgencia o de nuestra curiosidad inata. 

##  6. <a name='Enlacesexternos'></a>Enlaces externos

![](img/34.jpeg)

* Infografía PDF "Cómo identificar un correo electrónico malicioso" [https://www.osi.es/es/como-identificar-un-correo-electronico-malicioso](https://www.osi.es/es/como-identificar-un-correo-electronico-malicioso).
* Test phising [https://phishingquiz.withgoogle.com/](https://phishingquiz.withgoogle.com/).
* El 98% de los emails de los organismos púbicos son vulnerables ante posibles suplantaciones de identidad [https://www.20minutos.es/tecnologia/ciberseguridad/el-98-de-los-emails-de-los-organismos-pubicos-son-vulnerables-ante-posibles-suplantaciones-de-identidad-4783217/](https://www.20minutos.es/tecnologia/ciberseguridad/el-98-de-los-emails-de-los-organismos-pubicos-son-vulnerables-ante-posibles-suplantaciones-de-identidad-4783217/).
* Sextorsión en verano: la Oficina de Seguridad del Internauta alerta de una oleada de casos de chantaje sexual [https://www.20minutos.es/tecnologia/ciberseguridad/sextorsion-en-verano-la-oficina-de-seguridad-del-internauta-alerta-de-una-oleada-de-casos-de-chantaje-sexual-4783272/](https://www.20minutos.es/tecnologia/ciberseguridad/sextorsion-en-verano-la-oficina-de-seguridad-del-internauta-alerta-de-una-oleada-de-casos-de-chantaje-sexual-4783272/?utm_source=twitter.com&utm_medium=smm&utm_campaign=noticias)
* [https://www.clearedin.com/blog/phishing-attack-statistics](https://www.clearedin.com/blog/phishing-attack-statistics).
