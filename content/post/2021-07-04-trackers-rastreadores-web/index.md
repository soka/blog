---
title: Rastreadores Web y nuestra huella digital
subtitle: Privacidad y anonimato en la red
date: 2021-07-04
draft: true
description: La huella digital que nos identifica en Internet
tags: ["Ciberseguridad","Anonimato","Privacidad","Rastreadores","Web","Navegación"]
---

<!-- vscode-markdown-toc -->
* 1. [¿Como funcionan los rastreadores?](#Comofuncionanlosrastreadores)
* 2. [Recomendaciones](#Recomendaciones)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Los rastreadores junto con las famosas [_cookies_](https://es.wikipedia.org/wiki/Cookie_(inform%C3%A1tica)) permiten recopilar información sobre nuestros hábitos de navegación, en algunos casos puede tener como objetivo ofrecernos una experiencia de usuario más personalizada, por desgracia en otras ocasiones se usa para condicionar lo que vemos con otros propósitos, inducirnos a consumir o comprar algún producto o servicio es sólo la punta del iceberg.

La [RGDP](https://es.wikipedia.org/wiki/Reglamento_General_de_Protecci%C3%B3n_de_Datos) (Reglamento General de Protección de Datos) Europea viene a tratar de ponerle coto al descontrol de las _cookies_, de forma resumida una _cookie_ es un pequeño archivo que un sitio Web guarda en nuestro navegador, por ejemplo el contenido del carro de la compra en un comercio electrónico, ahora los sitios Web deben informar al usuario sobre la información que almacenan y el propósito de las _cookies_ que usan, el usuario puede rechazarlas todas o sólo permitir las estrictamente necesarias.  

La [EFF](https://es.wikipedia.org/wiki/Electronic_Frontier_Foundation) (Electronic Frontier Foundation) es una veterana en la lucha por la privacidad y el anonimato de los usuarios en la red, llevo años usando su extensión [HTTPS Everywhere](https://www.eff.org/https-everywhere), en su Web [Cover your tracks](https://coveryourtracks.eff.org/) podemos realizar una prueba para comprobar la información que pueden obtener los rastreadores de nuestro navegador.

##  1. <a name='Comofuncionanlosrastreadores'></a>¿Como funcionan los rastreadores?

Cuando accedemos a una Web nuestro navegador hace una petición al sitio para que le sirva el contenido, la Web consultada a su vez puede realizar llamadas a otras Webs de terceros, todo esto de forma transparente sin que nos percatemos, por detrás están intercambiando una serie de peticiones y información usando el protocolo HTTP, cada mensaje contiene una cabecera con meta información sobre el navegador y sobre nosotros.

La siguiente captura es sólo un ejemplo realizado con la extensión [HTTP Header Live](https://addons.mozilla.org/en-US/firefox/addon/http-header-live/) que permite visualizar todo el contenido de las llamadas HTTP:

![](img/01.png)

Esta información permite también que el sitio Web adapte sus contenidos dependiendo del navegador o las dimensiones dispositivo que estemos usando por ejemplo, el problema es que toda está información también puede permitir crear un perfil sobre nuestro comportamiento en Internet para otros propósitos.

Volviendo a la herramienta [Cover your tracks](https://coveryourtracks.eff.org/) de EFF el sitio genera un resumen con toda la información recabada de nuestro navegador, datos como el sistema operativo que usamos, el navegador, etc.

![](img/02.png)	

Toda esta información combinada conforma una huella digital que puede permitir identificarte de forma precisa respecto a otros usuarios.

##  2. <a name='Recomendaciones'></a>Recomendaciones


##  3. <a name='Enlacesexternos'></a>Enlaces externos

* [https://coveryourtracks.eff.org/](https://coveryourtracks.eff.org/)
* [https://spreadprivacy.com/duckduckgo-tracker-radar/](https://spreadprivacy.com/duckduckgo-tracker-radar/)
* [https://www.redeszone.net/tutoriales/seguridad/comprobar-quien-te-espia-internet/](https://www.redeszone.net/tutoriales/seguridad/comprobar-quien-te-espia-internet/)
* [https://addons.mozilla.org/en-US/firefox/addon/duckduckgo-for-firefox/](https://addons.mozilla.org/en-US/firefox/addon/duckduckgo-for-firefox/)
* [https://thehackernews.com/2015/10/track-online-users.html](https://thehackernews.com/2015/10/track-online-users.html)
* [https://addons.mozilla.org/en-US/firefox/addon/http-header-live/](https://addons.mozilla.org/en-US/firefox/addon/http-header-live/)