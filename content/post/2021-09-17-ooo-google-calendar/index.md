---
title: OOO Out Of Office en Google Calendar
subtitle: Indica cuando estarás fuera de la oficina
date: 2021-09-17
draft: false
description: Indica cuando estarás fuera de la oficina
tags: ["Google","Calendar","Updates","Workspace","Novedades"]
---

<!-- vscode-markdown-toc -->
* 1. [Modo Enfoque](#ModoEnfoque)
* 2. [Otras novedades](#Otrasnovedades)
* 3. [Enlaces internos](#Enlacesinternos)
* 4. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

La opción OOO (Out Of Office) permite informar a los compañer@s que no estamos disponibles, está opción permite definir un mensaje para declinar la invitación, ajustar las opciones de visibilidad pública/privada y rechazar de forma automática cualquier cita en ese intervalo.

Invitación nueva rechazada automáticamente, la cuenta que envión la invitación recibirá un correo informando de que la cita ha sido rechazada.

![](img/01.PNG)

Y ahora un pequeño truco, si pones "ooo" (en mayúsculas o minúsculas) en el título Calendar automáticamente determina que queremos definir que estamos fuera de la oficina:

![](img/03.gif)

##  3. <a name='Enlacesinternos'></a>Enlaces internos

Entradas de la categoría [Google](https://soka.gitlab.io/blog/tags/google/).

##  4. <a name='Enlacesexternos'></a>Enlaces externos

* 27 de Junio [Better manage your work and personal time with Google Calendar](https://workspaceupdates.googleblog.com/2018/06/better-manage-your-work-and-personal_27.html).
* [Google Calendar now has an ‘out of office’ option](https://www.theverge.com/2018/6/27/17510656/google-calendar-out-of-office-option).