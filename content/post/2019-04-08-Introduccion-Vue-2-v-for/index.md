---
title: Vue.js - Introducción
subtitle: Bucles v-for
date: 2019-04-08
draft: true
description: The Progressive JavaScript Framework
tags: ["Vue", "JS", "JavaScript", "Programación", "Vue.js"]
---

![](images/logo.png)

# Recorrer un listado de elementos

La sintaxis es muy parecida a otros lenguajes de programación la forma `item in items` donde en cada iteración `item` contiene el siguiente elemento de la lista `items`.

```JS
  <ul id="app1">
    <li v-for="item in items">
      {{ item.message }}
    </li>
  </ul>
```

En el código JS creamos una instancia con el constructor de **Vue**, al constructor debemos pasar un objeto con opciones ([ver referencia API oficial](https://es-vuejs.github.io/vuejs.org/v2/api/)):

- `el`: Provee a la instancia de Vue un elemento del DOM existente sobre el que montarse. Puede ser un selector CSS o un elemento HTML.
- `data`: El objeto de datos para la instancia de Vue. Vue convertirá recursivamente sus propiedades en getters/setters para hacerlos “reactivos”. En el ejemplo de abajo definimos un array de objetos.

```JS
      var app1 = new Vue({
        el: "#app1",
        data: {
          items: [{ message: "Foo" }, { message: "Bar" }]
        }
      });
```

# Acceder a un elemento padre

Dentro del bloque for podemos acceder a las propiedades del elemento padre 'parentMessage'.
El bucle for también admite otro parámetro opcional con el índice numérico cada elemento del array (variable 'index').

```JS
  <ul id="app2">
    <li v-for="(item, index) in items">
      {{ parentMessage }} - {{ index }} - {{ item.message }}
    </li>
  </ul>
```

```JS
      var app2 = new Vue({
        el: "#app2",
        data: {
          items: [{ message: "One" }, { message: "Two" }]
        }
      });
```

# Template

```JS
  <ul id="app3">
    <template v-for="item in items">
      <li>{{ item.message }}</li>
      <li>-</li>
    </template>
  </ul>
```

```JS
      var app3 = new Vue({
        el: "#app3",
        data: {
          items: [{ message: "Uno" }, { message: "Dos" }]
        }
      });
```

# Ejercicio

- [https://repl.it/@IkerLandajuela/Loops-v-for](https://repl.it/@IkerLandajuela/Loops-v-for).

<iframe height="400px" width="100%" src="https://repl.it/@IkerLandajuela/Loops-v-for?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

# Enlaces internos

- ["Vue.js - Introducción: Introducción general"](https://soka.gitlab.io/blog/post/2019-03-18-introduccion-vue/): Primer post.
- ["Vue.js - Introducción: Condicionales v-if y v-show"](https://soka.gitlab.io/blog/post/2019-03-27-introduccion-vue-2/).
- ["Recarga los cambios de tu página HTML/JS/CSS de forma automática"](https://soka.gitlab.io/blog/post/2019-03-17-npm-live-server/): Una utilidad para probar los ejemplos.

# Enlaces externos

- Guía oficial Vue.js con una introducción muy recomendable para empezar [enlace](https://vuejs.org/v2/guide/index.html).
- ["Aprende Vue2 y Firebase paso a paso"](https://wmedia.teachable.com/courses/enrolled/140226): **Curso gratuito muy recomendable** para empezar.

**vue general**:

- ["La instancia de Vue — Vue.js"](https://es-vuejs.github.io/vuejs.org/v2/guide/instance.html).

**v-for:**

- ["Renderizado de listas — Vue.js"](https://es-vuejs.github.io/vuejs.org/v2/guide/list.html).
- ["Listado de elementos con v-for en Vue 2 – Styde.net"](https://styde.net/listado-de-elementos-con-v-for-en-vue-2/).
- ["The Key to the Vue v-for - RIMdev"](https://rimdev.io/the-v-for-key/).
- ["List Rendering - Intro to Vue.js | Vue Mastery"](https://www.vuemastery.com/courses/intro-to-vue-js/list-rendering/).

**JavaScript:**

- ["JavaScript Arrays - W3Schools"](https://www.w3schools.com/js/js_arrays.asp).
