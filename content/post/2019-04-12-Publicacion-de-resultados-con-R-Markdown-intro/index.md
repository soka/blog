---
title: Publicación de contenidos con R Markdown
subtitle: Introducción
date: 2019-04-12
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico", "R - Markdown"]
---

![](images/R-Markdown-logo.PNG)

En este post vamos a ver como publicar contenidos con **R Markdown**, vamos a crear un nuevo archivo desde R Studio de tipo **R Markdown**, introducimos el nombre del documento y el autor, seleccionamos la salida por defecto de tipo PDF. Automáticamente R Studio crea un fichero con una cabecera de datos y contenido de ejemplo. Los ficheros de tipo **Markdown** en R se guardan con extensión ".Rmd".

![](images/rstudio-rmd.PNG)

R Studio cuando trabaja con ficheros ".Rmd" carga una nueva opción de menu llamada "Knit", básicamente lo que hace es generar el archivo de salida en formato PDF, HTML o Word. El fichero "Rmd" debe estar codificado con UTF-8.

Seguramente la primera vez que tratemos de exportarlo a PDF no funcionará, **R Markdown** depende de [MiKTeX](https://miktex.org/) para hacerlo. Si probamos con HTML muestra la salida sin problema y generá un fichero HTML, incluso lo podemos publicar en la nube si tenemos cuenta en [RPubs](https://rpubs.com/) ([enlace](https://rpubs.com/Iker) a mi cuenta) o [RStudio Connect](https://www.rstudio.com/products/connect/) (de [pago](https://www.rstudio.com/pricing/#ConnectPricing)), el documento de ejemplo se puede ver publicado en RPubs en este enlace [https://rpubs.com/Iker/RMardownTest](https://rpubs.com/Iker/RMardownTest).

Ahora borramos el cuerpo del documento y comenzamos a escribir nuestros contenidos propios, copiamos código de un ejercicio previo de ggplot por ejemplo.

![](images/rstudio-rmd-01.PNG)

Si publicamos el resultado veremos que se muestra tanto el código como el diagrama:

![](images/rstudio-rmd-02.PNG)

Evidentemente podemos ajustar las opciones de cada bloque de código para que genere la salida de diferentes maneras:

- `echo = FALSE` - No muestra el código.
- `eval = FALSE` - Incluye el código pero no lo ejecuta.
- `result = 'hide'` - No muestra resultados pero si los ejecuta.

Es muy común si queremos generar un reporte que sólo necesitemos los diagramas resultantes sin mostrar el código, voy a probarlo:

![](images/rstudio-rmd-03.PNG)

En el ejemplo la salida generá un _Warning_, uso otro parámetro para esconderlo:

````R
```{r ,echo=FALSE, warning=FALSE}
````

Si no conoces como formatear el texto con la sintaxis **Markdown** [este PDF](https://www.rstudio.com/wp-content/uploads/2015/03/rmarkdown-reference.pdf) de resumen es muy útil:

![](images/r-markdown-resumen-pdf.PNG)

No me voy a detener en la sintaxis **Markdown** ya que la uso a diario en mis proyectos y en este blog.

# Ejercicios

- / r / intro / 35-r-markdown / [r-markdown-01.Rmd](https://gitlab.com/soka/r/blob/master/intro/35-r-markdown/r-markdown-01.Rmd).
- / r / intro / 35-r-markdown / [rmarkdown-reference.pdf](https://gitlab.com/soka/r/blob/master/intro/35-r-markdown/rmarkdown-reference.pdf).

# Enlaces externos

- ["Knitr with R Markdown"](https://kbroman.org/knitr_knutshell/pages/Rmarkdown.html).
