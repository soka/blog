#!/usr/bin/env python3

from random import randint

class Personaje:
    
    def __init__(self,nombre='desconocido'):
        self.nombre = nombre
        #self.puntos_salud = 1
        self.puntos_salud = randint(1,10)        
        self.salud_max = 10
        print(self.nombre+' tiene '+str(self.puntos_salud)+' puntos de salud iniciales')
  
    def realizar_ataque(self, enemigo):
        puntos_ataque = randint(0,enemigo.puntos_salud)
        #print('puntos_ataque: ',puntos_ataque)
    
        '''puntos_ataque = min(
            max(
                randint(0, self.puntos_salud) - randint(0, enemigo.puntos_salud), 
                0),
            enemigo.puntos_salud)'''                        
    
        enemigo.puntos_salud = enemigo.puntos_salud - puntos_ataque    
        
        if puntos_ataque == 0: 
            print(enemigo.nombre+' evita el ataque de '+self.nombre)
        else: 
            print(self.nombre+' inflinje '+str(puntos_ataque)+' puntos de daño a '+enemigo.nombre+'!')
    
        return enemigo.puntos_salud <= 0


class Enemigo(Personaje):

    def __init__(self, player):
        Personaje.__init__(self)
        self.nombre = 'Orco'
        self.puntos_salud = randint(1, player.health)




class Jugador(Personaje):
    def __init__(self):
        Personaje.__init__(self)
        self.estado = 'normal'
        self.health = 10
        self.health_max = 10
    
    def quit(self):
        #print "%s can't find the way back home, and dies of starvation.\nR.I.P." % self.name self.health = 0
        pass
    
    def ayuda(self): 
        print(Comandos.keys())
      
    def status(self): 
        #print "%s's health: %d/%d" % (self.name, self.health, self.health_max)
        pass
  
    def tired(self):
        #print "%s feels tired." % self.name
        self.puntos_salud = max(1, self.puntos_salud - 1)
    
    def rest(self):
        if self.state != 'normal': 
            #print "%s can't rest now!" % self.name; self.enemy_attacks()
            pass
        else:
            #print "%s rests." % self.name
            pass
        if randint(0, 1):
            self.enemy = Enemy(self)
            #print "%s is rudely awakened by %s!" % (self.name, self.enemy.name)
            self.state = 'fight'
            self.enemy_attacks()
        else:
            if self.health < self.health_max:
                self.health = self.health + 1
            else: 
                print "%s slept too much." % self.name; self.health = self.health - 1
  
    def explore(self):
        if self.state != 'normal':
            print "%s is too busy right now!" % self.name self.enemy_attacks()
        else:
            print "%s explores a twisty passage." % self.name
        
        if randint(0, 1):
            self.enemy = Enemy(self)
            #print "%s encounters %s!" % (self.name, self.enemy.name)
            self.state = 'fight'
        else:
            if randint(0, 1): self.tired()
  
    def flee(self):
        if self.state != 'fight': 
            #print "%s runs in circles for a while." % self.name; self.tired()
            pass
        else:
            if randint(1, self.health + 5) > randint(1, self.enemy.health):
                #print "%s flees from %s." % (self.name, self.enemy.name)
                self.enemy = None
                self.state = 'normal'
            else: 
                #print "%s couldn't escape from %s!" % (self.name, self.enemy.name); self.enemy_attacks()
                pass
  
    def attack(self):
        if self.state != 'fight': 
            #print "%s swats the air, without notable results." % self.name; self.tired()
            pass
        else:
            if self.do_damage(self.enemy):
                #print "%s executes %s!" % (self.name, self.enemy.name)
                self.enemy = None
                self.state = 'normal'
                if randint(0, self.health) < 10:
                    self.health = self.health + 1
                    self.health_max = self.health_max + 1
                    #print "%s feels stronger!" % self.name
                else: 
                    self.enemy_attacks()
    def enemy_attacks(self):       
        if self.enemy.do_damage(self): 
            #print "%s was slaughtered by %s!!!\nR.I.P." %(self.name, self.enemy.name)
            pass


Comandos = {
  'quit': Jugador.quit,
  'help': Jugador.help,
  'status': Jugador.status,
  'rest': Jugador.rest,
  'explore': Jugador.explore,
  'flee': Jugador.flee,
  'attack': Jugador.attack,
  }