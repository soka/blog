#!/usr/bin/env python3

from random import randint

class Personaje:
    
    def __init__(self,nombre='desconocido'):
        self.nombre = nombre
        #self.puntos_salud = 1
        self.puntos_salud = randint(1,10)        
        self.salud_max = 10
        print(self.nombre+' tiene '+str(self.puntos_salud)+' puntos de salud iniciales')
  
    def realizar_ataque(self, enemigo):
        puntos_ataque = randint(0,enemigo.puntos_salud)
        #print('puntos_ataque: ',puntos_ataque)
    
        '''puntos_ataque = min(
            max(
                randint(0, self.puntos_salud) - randint(0, enemigo.puntos_salud), 
                0),
            enemigo.puntos_salud)'''                        
    
        enemigo.puntos_salud = enemigo.puntos_salud - puntos_ataque    
        
        if puntos_ataque == 0: 
            print(enemigo.nombre+' evita el ataque de '+self.nombre)
        else: 
            print(self.nombre+' inflinje '+str(puntos_ataque)+' puntos de daño a '+enemigo.nombre+'!')
    
        return enemigo.puntos_salud <= 0


class Enemigo(Personaje):

    def __init__(self, player):
        Personaje.__init__(self)
        self.nombre = 'Orco'
        self.puntos_salud = randint(1, player.health)


orco = Enemigo()

