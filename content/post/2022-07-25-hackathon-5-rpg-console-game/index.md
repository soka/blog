---
title: Juego RPG en modo texto
subtitle: API REST interactuando con servicios Web
date: 2022-07-25
draft: false
description: API REST interactuando con servicios Web
tags: ["Python","Retos","Aplicaciones","Juegos","ASCII","Hackathon","Dungeon crawl","Proyectos"]
---


<!-- vscode-markdown-toc -->
* 1. [Descripción general](#Descripcingeneral)
* 2. [Creación del programa paso a paso](#Creacindelprogramapasoapaso)
	* 2.1. [Clase base Personaje](#ClasebasePersonaje)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


##  1. <a name='Descripcingeneral'></a>Descripción general


##  2. <a name='Creacindelprogramapasoapaso'></a>Creación del programa paso a paso

###  2.1. <a name='ClasebasePersonaje'></a>Clase base Personaje




##  3. <a name='Enlacesexternos'></a>Enlaces externos

**SWAPI**:

* [https://swapi.dev/](https://swapi.dev/)
* [https://swapi.dev/documentation](https://swapi.dev/documentation)

**HTTP**:

* [https://developer.mozilla.org/en-US/docs/Web/HTTP/Status](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

**Requests**:

* General:
	* [https://realpython.com/python-requests/](https://realpython.com/python-requests/)

* Gestión de errores:
    * [https://requests.readthedocs.io/en/latest/api/#requests.ConnectionError](https://requests.readthedocs.io/en/latest/api/#requests.ConnectionError)
    * [https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions](https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions)

