---
title: RDPStrip MiTM captura tráfico RDP
subtitle: Kali Linux
date: 2022-03-17
draft: false
description: Kali Linux
tags: ["Seguridad Informática","LAN","RDP","Kali","SEIF","SEIN","RDPStrip","MiTM"]
---



## Enlaces externos

* [https://www.elladodelmal.com/2019/06/rdpstrip-como-atacar-remote-desktop.html](https://www.elladodelmal.com/2019/06/rdpstrip-como-atacar-remote-desktop.html).
* [https://github.com/SySS-Research/Seth/blob/master/doc/paper/Attacking_RDP-Paper.pdf](https://github.com/SySS-Research/Seth/blob/master/doc/paper/Attacking_RDP-Paper.pdf).