---
title: Las tarjetas de Trello, elemento básico de información
subtitle: La pizarra digital inspirada en kanban
date: 2019-07-04
draft: false
description: Creando tarjetas en Trello
tags:
  [
    "Trello",
    "Talleres",
    "Cursos",
    "Aplicaciones",
    "Trello - Introducción",
    "Trello - Tarjetas",
    "Kanban",
    "Metodologías",
    "Gestión de Proyectos",
    "Herramientas",
  ]
---

![](img/logo-trello.png)

## Añadir nuevas tarjetas

Las [**tarjetas**](https://help.trello.com/article/729-adding-cards) son la unidad básica de un tablero. Sirven para representar cualquier elemento de información que se nos ocurra.

[**Añade tarjetas**](<(https://help.trello.com/article/729-adding-cards)>) a través de la opción de entrada "Agregar una tarjeta... " en la parte inferior de las listas. También puedes agregar tarjetas mediante el menú contextual en la parte superior de las listas (consejo Pro: Esto agregará la nueva tarjeta en la parte superior de la lista).

![](img/02.gif)

## Editar tarjetas

Para [**editar un tarjeta**](https://help.trello.com/article/784-editing-cards), haz clic sobre la tarjeta de del tablero que quieras editar. De esta manera, abrirás la parte posterior. Si quieres editar el título de una tarjeta, haz clic sobre él, en la parte posterior de la tarjeta. **Las tarjetas también incluyen un campo de descripción opcional**. Introduce un texto en la descripción para añadirle más detalles a la tarjeta. [Si quieres, puedes utilizar **Markdown**](https://help.trello.com/customer/portal/articles/1496117-using-markdown-in-trello) para dar formato al texto de la descripción.

![](img/01.PNG)

Abre un tarjeta para editar el título y la descripción.

**Si alguien más está editando la descripción de la tarjeta mientras estás en la tarjeta, Verás su avatar con un lápiz al lado de él**.

## Menús rápidos de tarjetas

Pasa el ratón sobre un tarjeta y haz clic en el icono del lápiz que aparecerá encima de la tarjeta. Dentro del menú de la tarjeta podrás realizar una serie de acciones en un tarjeta directamente desde la vista del tablero.

![](img/03.gif)

## Atajos del teclado

En las tarjetas se pueden utilizar los siguientes atajos de teclado ([ver lista completa](https://trello.com/shortcuts)):

- "a" - añadir miembros a la tarjeta
- ESPACIO - añadirte a la tarjeta
- "c" - archivado
- "l" - acceder a las etiquetas
- "d" - definir fechas de vencimiento
- "<" - mover una tarjeta a la izquierda
- ">" mover una tarjeta a la derecha
- ">" v - votar
- ">" 1 - 6 - añadir etiquetas

## Añadir un miembro a una tarjeta

Los miembros del tablero también se pueden agregar a las tarjetas individuales para indicar la asociación o asignar esos miembros a la tarjeta. Agregar un miembro a una tarjeta automáticamente les permite ver la tarjeta. **Recibirán notificaciones de lo siguiente:**

- Se publique un comentario nuevo
- Adiciones, modificaciones o fechas de vencimiento próximas
- Las tarjetas que se han movido o archivado
- Si los miembros de la tarjeta eligen no verlo en un momento posterior, ya no recibirán estas notificaciones.

Para añadir un usuario a un tarjeta, ábrela con tan sólo hacer clic sobre ella. Después, elige la opción "Miembros", a la derecha de la tarjeta. A continuación, escribe el nombre del miembro o haz clic en su avatar. También puedes añadir un miembro a una tarjeta si abres el menú de edición rápida de la tarjeta (pasa el cursor por encima de la tarjeta y haz clic en el icono del lápiz), y seleccionas a continuación la opción "Cambiar miembros" en el menú desplegable.

![](img/04.gif)

Puedes arrastrar y soltar el avatar de un usuario directamente en una tarjeta.

![](img/05.gif)

Los siguientes atajos de teclado te serán útiles a la hora de añadir miembros a una tarjeta. Para que el atajo funcione, deberás pulsar la tecla cuando pases el ratón por encima de la tarjeta o cuando la tarjeta esté abierta.

- ESPACIO - Te añadirá a una tarjeta.
- a - Añadir otro miembro a una tarjeta.

**Para agregar un miembro a una tarjeta, ese usuario primero tendrá que ser miembro del equipo. o del tablero**.

## Comentar tarjetas

Cuando comentes sobre una tarjeta de la que aún no eres miembro, **estarás Mirando automáticamente esa tarjeta para ayudarte a responder las respuestas que no te mencionen**. Si prefiere no mirar, puede desmarcar la casilla "Ver actualizaciones de esta tarjeta". Después de desmarcar la casilla una vez, esto se desactivará de forma predeterminada en el futuro.

![](img/06.gif)

El nuevo comentario introducido se muestra como icono en el frontal de la tarjeta.

## Añadir un archivo adjunto

Se pueden agregar archivos adjunto a la fuente de actividad mientras hace comentarios en una tarjeta haciendo clic en el icono del clip y seleccionando el archivo que desea adjuntar a su tarjeta. También puede usarlo para adjuntar una tarjeta.

![](img/07.gif)

## Menciones con @

Puede mencionar a las personas en los comentarios usando "@" seguido del nombre de usuario del usuario o haciendo clic en el ícono @ debajo del campo de entrada de comentarios y luego seleccionando los miembros que desea mencionar. Los usuarios mencionados recibirán una notificación sobre el comentario.

![](img/08.gif)

## Añadir emojis

También puedes **añadir emojis a tu comentario** desde el icono de la sonrisa por debajo del campo de entrada de comentarios o mediante el uso de dos puntos alrededor de tu término emoji (es decir, :emoji:).

![](img/09.gif)

## Editar y borrar comentarios

**Edite o elimine los comentarios haciendo clic en los vínculos "editar" o "eliminar" debajo del comentario**. Puede editar o eliminar cualquier comentario que realice en cualquier tabla. A pesar de que **no es posible editar los comentarios realizados por otro usuario, los administradores pueden eliminar los comentarios realizados por los no administradores en sus tableros**. La eliminación de un comentario es permanente.

![](img/10.gif)

## Cómo compartir un enlace a un comentario

Para **obtener un enlace a un comentario específico**, haz clic con el botón derecho en la marca temporal debajo del comentario y selecciona la opción "Copiar enlace" de tu navegador.

![](img/11.gif)

## Colaborar con los demás

Si otro usuario está escribiendo un comentario al mismo tiempo que tú, verás su avatar por encima de la caja de comentarios, con una burbuja de texto.

## Enlaces externos

- ["Trello 101"](https://trello.com/guide/trello-101): Obtenga información acerca de los aspectos básicos con esta guía rápida y útil.
- ["Editing cards - Trello Help"](https://help.trello.com/article/784-editing-cards).
- ["Cómo formatear tu texto en Trello"](https://help.trello.com/article/821-using-markdown-in-trello).
- ["Añadir tarjetas"](https://help.trello.com/article/729-adding-cards).

**Markdown:**

- ["Markdown - La guía definitiva en español"](https://markdown.es/).
- ["Online Markdown Editor - Dillinger, the Last Markdown Editor ever."](https://dillinger.io/).
- ["StackEdit – In-browser Markdown editor"](https://stackedit.io/app#).
