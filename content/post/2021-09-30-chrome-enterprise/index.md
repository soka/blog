---
title: Chrome para entornos empresariales
subtitle: Navegador gestionado en la nube
date: 2021-09-30
draft: true
description: Navegador gestionado en la nube
tags: ["Google","Workspace","Chrome","Navegadores","Sysadmin"]
---

<!-- vscode-markdown-toc -->
* 1. [Plantillas ADM/ADMX](#PlantillasADMADMX)
* 2. [Enlaces internos](#Enlacesinternos)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Google ofrece una versión [empresarial de su navegador Chrome](https://chromeenterprise.google/), permite personalizar la configuración según las necesidades de tu organización y gestionar politicas _on-premise_ o usando plantillas ADM/ADMX (estas plantillas indican cuáles son las claves de registro que pueden definirse para configurar Chrome), a traves de la consola de admin de Google Workspace se pueden gestionar todos los navegadores desde un único sitio. 

Dos navegadores gestionados desde la consola de administrador, para registrarlos en tan sencillo como descargar un arhivo de registro de Windows y ejecutarlo en la máquina destino:

![](img/02.PNG)

Lo primero es [descargar](https://chromeenterprise.google/intl/es_es/browser/download/#windows-tab) el navegador para empresa, incluye versiones 32/64 bits como instalador MSI o un paquete con el mismo instalador junto con plantillas de políticas y actualizaciones automáticas gestionables.

![](img/01.PNG)

A parte del instalador (GoogleChromeStandaloneEnterprise64.msi) el paque contiene otros archivos: 

**Configuration\master_preferences**: Fichero JSON de texto plano con las preferencias partículares del usuario ([preferencias VS políticas](https://www.chromium.org/administrators/configuring-other-preferences)), este fichero se ejecuta la primera vez.

En Windows se encuentra en 'C:\Program Files\Google\Chrome\Application'.

Ejemplo:

```
{
  "homepage": "http://www.google.com",
  "homepage_is_newtabpage": false,
  "browser": {
    "show_home_button": true
  },
  "session": {
    "restore_on_startup": 4,
    "startup_urls": [
      "http://www.google.com/ig"
    ]
  },
  "bookmark_bar": {
    "show_on_all_tabs": true
  },
  "sync_promo": {
    "show_on_first_run_allowed": false
  },
  "distribution": {
    "import_bookmarks_from_file": "bookmarks.html",
    "import_bookmarks": true,
    "import_history": true,
    "import_home_page": true,
    "import_search_engine": true,
    "ping_delay": 60,
    "suppress_first_run_bubble": true,
    "do_not_create_desktop_shortcut": true,
    "do_not_create_quick_launch_shortcut": true,
    "do_not_launch_chrome": true,
    "do_not_register_for_update_launch": true,
    "make_chrome_default": true,
    "make_chrome_default_for_user": true,
    "suppress_first_run_default_browser_prompt": true,
    "system_level": true,
    "verbose_logging": true
  },
  "first_run_tabs": [
    "http://www.example.com",
    "http://welcome_page",
    "http://new_tab_page"
  ]
}
``` 

##  1. <a name='PlantillasADMADMX'></a>Plantillas ADM/ADMX

Ejecuta `gpedit.msc` y accede a "Directiva de equipo local > Configuración del equipo > Plantillas administrativas". Haz clic con el botón derecho del ratón en Plantillas administrativas y selecciona Agregar o quitar plantillas, aplicamos plantillas administrativas *.adm.


## Establecer políticas en la consola Web

Dispositivos > Chrome > Configuración > Usuarios y navegadores.

![](img/03.PNG)


##  2. <a name='Enlacesinternos'></a>Enlaces internos

* [Publicaciones internas dentro de la categoría Google](https://soka.gitlab.io/blog/tags/google/) .

##  3. <a name='Enlacesexternos'></a>Enlaces externos

* [https://chromeenterprise.google/](https://chromeenterprise.google/).
* [https://www.chromium.org/administrators/configuring-other-preferences](https://www.chromium.org/administrators/configuring-other-preferences).
* Chrome Enterprise policy list [https://cloud.google.com/docs/chrome-enterprise/policies](https://cloud.google.com/docs/chrome-enterprise/policies): Todas las políticas que se pueden aplicar y su correspondiente clave de registro. El paquete de Chrome para empresas incluye un archivo chrome.reg donde se pueden modificar todas estas claves de registro.
* [chrome://policy/](chrome://policy/): Políticas de Chrome.
* [https://dl.google.com/dl/edgedl/chrome/policy/policy_templates.zip](https://dl.google.com/dl/edgedl/chrome/policy/policy_templates.zip): También puedes descargar las plantillas por separado y ver documentación común sobre políticas relativas a todos los sistemas operativos.
