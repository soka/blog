---
title: Shu-Ha-Ri
subtitle: primero aprender, después desprenderse y finalmente trascender.
date: 2019-07-14
draft: true
description: Manipulación de datos y análisis estadístico
tags: ["Conceptos", "Prácticas", "Metodologías", "Técnicas"]
---

## Enlaces externos

- ["Aplicate el Shuhari - Javier Garzas"](https://www.javiergarzas.com/2017/03/aplicate-el-shuhari.html).
- ["Shu Ha Ri, o las etapas del aprendizaje | Utópica Informática"](http://blog.utopicainformatica.com/2010/12/shu-ha-ri-o-las-etapas-del-aprendizaje.html).
