---
title: MiKIT [borrador en progreso]
subtitle: Aplicaciones de seguridad para empresas
date: 2022-03-18
draft: false
description: Aplicaciones de seguridad para empresas
tags: ["Seguridad Informática","SUITE","Aplicaciones","Programas","1SM2","Herramientas"]
---

<!-- vscode-markdown-toc -->
* 1. [Antivirus Antimalware](#AntivirusAntimalware)
* 2. [Herramientas en línea](#Herramientasenlnea)
* 3. [Limpieza y optimización](#Limpiezayoptimizacin)
* 4. [Recuperación datos](#Recuperacindatos)
* 5. [Auditorias](#Auditorias)
* 6. [SUITEs](#SUITEs)
* 7. [Cifrado](#Cifrado)
* 8. [Copias de seguridad](#Copiasdeseguridad)
* 9. [Gestores de contraseñas](#Gestoresdecontraseas)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='AntivirusAntimalware'></a>Antivirus Antimalware

* [Herramientas y Utilidades de ESET](https://www.eset.com/hn/descargar-herramientas-y-utilidades/).
    * [**ESET Online Scanner**](https://www.eset.com/hn/hogar/deteccion-de-malware-online/): Herramienta portable que no requiere instalación.Totalmente gratuita.
    * Recopilador de registros de ESET: ESET Log Collector es una aplicación que recopila automáticamente información y registros de tu equipo para ayudarnos a resolver los problemas con mayor rapidez.
    * [**ESET SysRescue Live**](https://www.eset.com/hn/soporte/elimina-amenazas-de-su-pc/): Distribución de Linux encargada de explorar en busca de malware y desinfectar los archivos infectados. Se ejecuta desde un CD, DVD o dispositivo USB en forma independiente del sistema operativo en el equipo host, pero cuenta con acceso directo al disco y al sistema de archivos. Esto hace posible quitar las amenazas que en condiciones normales de funcionamiento serían imposibles de eliminar.
    * [ESET SysInspector](https://www.eset.com/hn/soporte/diagnostico-de-pc-gratuito/): ESET SysInspector analiza tu sistema operativo y captura detalles como los procesos en ejecución, el contenido de registro, los elementos de inicio y las conexiones de red. ESET SysInspector es una utilidad conveniente para incluir en la caja de herramientas de todo experto en TI y para quienes son los primeros en responder ante una emergencia.
* [Malwarebytes ](https://es.malwarebytes.com/).
* [McAfee Stinger](https://www.mcafee.com/enterprise/es-es/downloads/free-tools/stinger.html) McAfee Stinger is a standalone utility used to detect and remove specific viruses. It’s not a substitute for full antivirus protection, but a specialized tool to assist administrators and users when dealing with infected system. 
* [Kaspersky Virus Removal Tool](https://www.kaspersky.es/downloads/free-virus-removal-tool)
* [Bitdefender Total Security Free](https://www.bitdefender.es/solutions/free.html).

* [Avast](https://www.avast.com/es-es/index#pc).

##  2. <a name='Herramientasenlnea'></a>Herramientas en línea

* [VirusTotal](https://www.virustotal.com/gui/home/upload): Analiza URLs y ficheros sospechosos.
* [Have I Been Pwned?](https://haveibeenpwned.com/).
* [desenmascara.me](http://desenmascara.me/) Analizador URLs.
* [Servicio Antibotnet | INCIBE](https://www.incibe.es/protege-tu-empresa/herramientas/servicio-antibotnet)

##  3. <a name='Limpiezayoptimizacin'></a>Limpieza y optimización

* [CCleaner](https://www.ccleaner.com/).
* [Ninja System](https://singularlabs.com/software/system-ninja/)
* [Revo Uninstaller](https://www.revouninstaller.com/es/revo-uninstaller-free-download/)

##  4. <a name='Recuperacindatos'></a>Recuperación datos

* [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec).

##  5. <a name='Auditorias'></a>Auditorias 

* [CLARA](https://www.ccn-cert.cni.es/soluciones-seguridad/clara.html). Artículo [Auditoria de seguridad en Windows con CLARA](https://soka.gitlab.io/blog/post/2021-07-05-auditorias-seguridad-con-clara/).

##  6. <a name='SUITEs'></a>SUITEs

* [Hiren’s BootCD PE](https://www.hirensbootcd.org/download/).

##  7. <a name='Cifrado'></a>Cifrado

* [VeraCrypt](https://www.veracrypt.fr/en/Home.html): Solución de código abierto multiplataforma para cifrar discos. 
* [AES Crypt](https://www.aescrypt.com/).
* [zuluCrypt](http://mhogomchungu.github.io/zuluCrypt/).

##  8. <a name='Copiasdeseguridad'></a>Copias de seguridad

* [Cobian](https://www.cobiansoft.com/).
* [EaseUS Todo Backup](https://es.easeus.com/backup-software/).
* [Clonezilla](https://clonezilla.org/):
* [Robocopy](https://es.wikipedia.org/wiki/Robocopy).
* [Rsync (Linux)](https://es.wikipedia.org/wiki/Rsync).
* Norton Ghost.

##  9. <a name='Gestoresdecontraseas'></a>Gestores de contraseñas

Gratuitos para uso empresarial o doméstico: 

* [KeePass](https://keepass.info/).
* [KeePassX](https://www.keepassx.org/).

Freemium, productos empresariales profesionales:

* LastPass.
* Bitwarden.

