---
title: Instalar XAMPP en Debian 
subtitle:  
date: 2022-07-20
draft: true
description: 
tags: ["Debian","Web","Sysadmin","XAMPP","Apache"]
---

Descargar la última versión para Linux de https://www.apachefriends.org/download.html

```
wget https://downloadsapachefriends.global.ssl.fastly.net/8.1.6/xampp-linux-x64-8.1.6-0-installer.run
```

Darle permisos de ejecución:

```
chmod 755 xampp-linux-x64-8.1.6-0-installer.run
```

Ejecutar como administrador para comenzar el proceso de instalación (en /opt/lampp):

```
su ./xampp-linux-x64-8.1.6-0-installer.run
```

Cuando finaliza la instalación ya podemos lanzar el administrador en modo gráfico. Creo que el problema que me acaba de ocurrir ya me ha pasado otras veces, el servidor HTTP Apache no arranca bien, es porque tengo otro demonio de Apache corriendo, lo he parado con `$sudo /etc/init.d/apache2 stop`. Ahora compruebo que funciona el servidor HTTP y PHP en la siguiente dirección `http://localhost/dashboard/` desde donde compruebo que funciona PHP (http://localhost/dashboard/phpinfo.php) y la base de datos (http://localhost/phpmyadmin/).

Debemos colocar nuestra web en la ruta /opt/lampp/htdocs.






