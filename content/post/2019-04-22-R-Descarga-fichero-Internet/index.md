---
title: Descarga de un fichero de Internet
subtitle: Carga de datos, origen de datos y limpieza
date: 2019-04-22
draft: false
description: Manipulación de datos y análisis estadístico
tags: ["R", "Programación", "R - Básico", "R - Ficheros"]
---

![](images/r-logo.PNG)

En este artículo veremos como descargar información de Internet y cargarla en R para su posterior tratamiento. Vamos a usar una base de datos pública la ciudad de Baltimore ([Open Baltimore](https://data.baltimorecity.gov/)). El fichero CSV a descargar contiene información sobre una serie de cámaras que detectan excesos de velocidad ([enlace](https://data.baltimorecity.gov/Transportation/Baltimore-Fixed-Speed-Cameras/dz54-2aru)).

![](open-baltimore-01.PNG)

## Descarga de fichero de Internet

Una buena practica es descargar los ficheros de datos a una carpeta, la llamaremos "data". Vamos a utilizar la orden `file.exists` para comprobar si existe o no ese directorio. En caso de que no exista el código dentro de la condicional `if` crea el directorio.

```R
if(!file.exists("data")) {
  dir.create("data")
}
```

Ahora procedemos a descargar el fichero que renombramos como "camaras.csv" en el directorio "data":

```R
URL <- "https://data.baltimorecity.gov/api/views/dz54-2aru/rows.csv?accessType=DOWNLOAD"
download.file(URL,destfile = "./data/camaras.csv")
```

![](images/data-camaras-01.PNG)

## Carga de fichero en R

Esta es una captura del fichero "camaras.csv".

![](images/camaras-csv-01.PNG)

La primera línea contiene la cabecera con la descripción de las columnas, el delimitador de campos o columnas es el carácter "," y el signo de puntuación para los números decimales.

Si intentamos cargarlo así produce un error:

```R
> camarasData <- read.table("./data/camaras.csv")
Error in scan(file = file, what = what, sep = sep, quote = quote, dec = dec,  :
  line 1 did not have 13 elements
```

Podemos cargar el archivo "camaras.csv" para estudiarlo desde el panel "Files" de RStudio.

![](images/camaras-csv-02.PNG)

Vamos a añadir que el separador es la "," y que además la primera línea es el encabezado:

```R
camarasData <- read.table("./data/camaras.csv",sep = ',',header = T)
```

En esta ocasión carga bien el fichero, le damos un primer vistazo en RStudio:

![](images/camaras-csv-03.PNG)

Podemos usar también el comando `head(camarasData)`.

En lugar de `read.table` podemos usar una función más especializada `read.csv` como sigue:

```R
camarasData <-  read.csv("./data/camaras.csv")
```

Si no indicamos más parámetros `read.csv` define el separador como ',' por defecto, presupone que tiene una cabecera y el separador decimal es '.' entre otras cosas.

Cuando solo queremos coger una muestra de las 10 primeras filas por ejemplo:

```R
camarasData <-  read.csv("./data/camaras.csv",nrows = 10)
```

Ahora queremos cinco líneas y saltarnos las cinco primeras:

```R
camarasData <-  read.csv("./data/camaras.csv",nrows = 5,skip = 5)
```

## Ejercicios

- r / intro / 39-descarga-fichero-internet / [descarga-fichero-internet.R](https://gitlab.com/soka/r/blob/master/intro/39-descarga-fichero-internet/descarga-fichero-internet.R).

## Enlaces externos
