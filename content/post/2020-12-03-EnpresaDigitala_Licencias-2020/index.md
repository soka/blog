---
title: Licencias software libre. ¿Cómo impacta en mi desarrollo?
subtitle: Jornada Enpresa Digitala
date: 2020-12-03
draft: false
description: Impartido por Alejandra Ruiz - Tecnalia 
tags: ["Jornadas","Tecnalia","Enpresa Digitala","Formación","Presentaciones","Recursos","Licencias","Software Libre"]
---

Jornada organizada por Tecnalia y Enpresa Digitala, impartida por Alejandra Ruiz ([@a_ruizTECNALIA](https://twitter.com/a_ruizTECNALIA)).

El siguiente resumen de la jornada está tomada directamente de la [convocatoria del evento](https://www.spri.eus/euskadinnova/es/enpresa-digitala/agenda/licencias-software-libre-como-impacta-desarrollo-online/16671.aspx):

El objetivo de la jornada es dar a conocer las Licencias software libre, su origen, de dónde y por qué surgen.

¿Qué hace que una licencia sea de código abierto? ¿Qué implicaciones tiene? Se explicarán diferentes casos prácticos con los que nos podemos encontrar.

No se trata de aportar conocimientos legales exhaustivos sobre las licencias, sino que lo que se pretende es que a través de casos prácticos se puedan comprender las implicaciones de las licencias en nuestro trabajo.

Se explicarán los objetivos que se buscaban cuando se hicieron las licencias y sus implicaciones técnicas.

Documento con la presentación: [EmpresaDigitala_Licencias-2020.pdf](./EmpresaDigitala_Licencias-2020.pdf)

![](img/EmpresaDigitala_Licencias-2020-01.png)

![](img/EmpresaDigitala_Licencias-2020-02.png)

![](img/EmpresaDigitala_Licencias-2020-03.png)

![](img/EmpresaDigitala_Licencias-2020-04.png)

![](img/EmpresaDigitala_Licencias-2020-05.png)

![](img/EmpresaDigitala_Licencias-2020-06.png)

![](img/EmpresaDigitala_Licencias-2020-07.png)

![](img/EmpresaDigitala_Licencias-2020-08.png)

![](img/EmpresaDigitala_Licencias-2020-09.png)

![](img/EmpresaDigitala_Licencias-2020-10.png)

![](img/EmpresaDigitala_Licencias-2020-12.png)

![](img/EmpresaDigitala_Licencias-2020-13.png)

![](img/EmpresaDigitala_Licencias-2020-14.png)

![](img/EmpresaDigitala_Licencias-2020-15.png)

![](img/EmpresaDigitala_Licencias-2020-16.png)

![](img/EmpresaDigitala_Licencias-2020-17.png)

![](img/EmpresaDigitala_Licencias-2020-18.png)

![](img/EmpresaDigitala_Licencias-2020-19.png)

![](img/EmpresaDigitala_Licencias-2020-20.png)

![](img/EmpresaDigitala_Licencias-2020-21.png)

![](img/EmpresaDigitala_Licencias-2020-22.png)

![](img/EmpresaDigitala_Licencias-2020-23.png)

![](img/EmpresaDigitala_Licencias-2020-24.png)

![](img/EmpresaDigitala_Licencias-2020-25.png)

![](img/EmpresaDigitala_Licencias-2020-26.png)

![](img/EmpresaDigitala_Licencias-2020-27.png)

![](img/EmpresaDigitala_Licencias-2020-28.png)

![](img/EmpresaDigitala_Licencias-2020-29.png)

![](img/EmpresaDigitala_Licencias-2020-30.png)

![](img/EmpresaDigitala_Licencias-2020-31.png)

![](img/EmpresaDigitala_Licencias-2020-32.png)

![](img/EmpresaDigitala_Licencias-2020-33.png)

![](img/EmpresaDigitala_Licencias-2020-34.png)

![](img/EmpresaDigitala_Licencias-2020-35.png)

![](img/EmpresaDigitala_Licencias-2020-36.png)

![](img/EmpresaDigitala_Licencias-2020-37.png)

![](img/EmpresaDigitala_Licencias-2020-38.png)

![](img/EmpresaDigitala_Licencias-2020-39.png)

![](img/EmpresaDigitala_Licencias-2020-40.png)

![](img/EmpresaDigitala_Licencias-2020-41.png)

![](img/EmpresaDigitala_Licencias-2020-42.png)

![](img/EmpresaDigitala_Licencias-2020-43.png)

![](img/EmpresaDigitala_Licencias-2020-44.png)

![](img/EmpresaDigitala_Licencias-2020-45.png)

![](img/EmpresaDigitala_Licencias-2020-46.png)

![](img/EmpresaDigitala_Licencias-2020-47.png)

![](img/EmpresaDigitala_Licencias-2020-48.png)

![](img/EmpresaDigitala_Licencias-2020-49.png)

![](img/EmpresaDigitala_Licencias-2020-50.png)

![](img/EmpresaDigitala_Licencias-2020-51.png)

![](img/EmpresaDigitala_Licencias-2020-52.png)

![](img/EmpresaDigitala_Licencias-2020-53.png)
