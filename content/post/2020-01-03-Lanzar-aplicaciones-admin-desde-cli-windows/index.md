---
title: Abrir aplicaciones de administración desde línea de comandos
subtitle: Administración de Windows Server con PowerShell
date: 2020-01-03
draft: false
description: Administración servidores Windows
tags: ["Windows","Sistemas","PowerShell","Administración","cmdlets","Scripting","Windows Server"]
---

![](img/win-server-logo.png)

La gran mayoría de aplicaciones para administradores de Windows Server son [MMC (Microsoft Management Console)](https://es.wikipedia.org/wiki/Microsoft_Management_Console)  accesibles usando el botón inicio pero también se pueden lanzar desde la consola PS, se puede combinar con el comando [`runas`](https://en.wikipedia.org/wiki/Runas) para ejecutar una herramienta con privilegios elevados o bajo otra cuenta del sistema.

Algunos de estos comandos es posible que no funcionen si no tienes el rol instalado. Aquí sólo cito algunos que me interesan:

| Descripción         | Comando      | Notas  |
|---------------------|--------------|--------|
| Computer Management | compmgmt.msc | Administración de la computadora: Programador de tareas, usuarios y grupos locales, visor de eventos, carpetas compartidas, etc.|
| Component Services | comexp.msc | Servicios de componentes COM, [DCOM+](https://es.wikipedia.org/wiki/Modelo_de_Objetos_de_Componentes_Distribuidos), etc. | 
| **DHCP Managment**  | **dhcpmgmt.msc** | Gestor DHCP |
| **DNS Managment** | **dnsmgmt.msc** |  |
| **Event Viewer** | **eventvwr.msc**| |
| Local Certificates Management | certmgr.msc | |
| **Local Group Policy Editor** | gpedit.msc | Permite establecer la configuración de la computadora o usuario local mediante políticas de grupo. **Por ejemplo se pueden definir scripts para el arranque o apagado del equipo o al inicio / fin de sesión de un usuario** |
| **Local Security Settings Manager** | **secpol.msc** | Directiva de seguridad local | 
| **Local Users and Groups Manager** | **lusrmgr.msc** | |
| **Services Management** | **services.msc** |  Administrador de servicios locales |
| **Shared Folders** | **fsmgmt.msc** | Carpetas compartidas |
| Terminal Server Manager | tsadmin.exe | |
| AD Domains and Trusts | domain.msc | |
| Active Directory Management | admgmt.msc | |
| AD Sites and Services | dssite.msc | |
| AD Users and Computers | dsa.msc | | 

# Enlaces externos

- Wikipedia [MMC (Microsoft Management Console)](https://es.wikipedia.org/wiki/Microsoft_Management_Console). Contiene un listado de comandos.
- Wikipedia [runas](https://en.wikipedia.org/wiki/Runas).
- docs.microsoft.com [Local Group Policy Editor](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/cc725970(v=ws.11)).
- [Use Startup, Shutdown, Logon, and Logoff Scripts](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/cc753404%28v%3dws.10%29).
- [Assign User Logon Scripts](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/cc770908%28v%3dws.10%29).