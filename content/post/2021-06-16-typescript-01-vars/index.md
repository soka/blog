---
title: TypeScript
subtitle: Lo básico sobre las variables 
date: 2021-06-16
draft: false
description: 
tags: ["TypeScript","TypeScript - Variables básicas","Programación","TypeScript - Variables scope"]
---

<!-- vscode-markdown-toc -->
* 1. [NOMBRES DE VARIABLES](#NOMBRESDEVARIABLES)
* 2. [ÁMBITO DE VARIABLES](#MBITODEVARIABLES)
* 3. [CONSTANTES](#CONSTANTES)
* 4. [ENLACES INTERNOS](#ENLACESINTERNOS)
* 5. [RECURSOS](#RECURSOS)
* 6. [ENLACES EXTERNOS](#ENLACESEXTERNOS)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='NOMBRESDEVARIABLES'></a>NOMBRES DE VARIABLES

Los nombres de variables siguen las mismas reglas que en JS, el primer carácter debe ser una letra, un guión bajo o el símbolo $, el resto siguen las mismas condiciones y pueden incluir números. Puedes probar si un nombre de variable sigue las normas ECMAScript 6 en esta página [https://mothereff.in/js-variables](https://mothereff.in/js-variables).

Y esto de abajo también es un nombre de variable válido:

```ts
const ಠ_ಠ = "nombres exóticos de variables "
```

##  2. <a name='MBITODEVARIABLES'></a>ÁMBITO DE VARIABLES

Para las variables declaradas con `const` o `let` el **ámbito de la variable es local al bloque**. El valor de una variable declarada con `const` es inmutable como su nombre da a entender.

Esto producirá un error, la variable está declarada dentro de un bloque condicional (ver un ejemplo inverso más abajo):

```ts
let i:boolean = true;

if (i===true) {
    let a:number = 5
}

console.log(a); // Error: Cannot find name 'a'.
```

Una variable declarada con `var` es global, si no lo hacemos de forma intencionada puede producir problemas como colisión de nombres de variables repetidas o sobrescribir el contenido de forma accidental, con `var` podemos hacer cosas como esta:

```ts
var cadena_nombre:string = "Iker";

function escribe_nombre() {
  console.log("Mi nombre es: "+cadena_nombre);
}

escribe_nombre();
```

El acto de introducir una nueva variable de un bloque anidado con el mismo nombre que una variable de un bloque superior se llama **shadowing** (evitar como la peste salvo con causa justificada en contadas ocasiones). Esta locura en otros lenguajes en estos páramos es válida:

```ts
var myNum:number = 7;
let isOn:boolean = true;

if (isOn===true) {
    var myNum:number = 5
}
console.log("Haciendo shawdowing es "+myNum); // 5
```

Otro ejercicio con para comprender el ámbito de las variables declaradas con `let`, si podemos usarla dentro de un bloque si proviene de un ámbito superior:

```ts
let globalScope = 1;
{
    let blockScope = 2;
    // OK. This is from a wider scope
    globalScope = 100;
    // Error! This is outside of the scope the variable is declared in
    nestedBlockScope = 300;
    {
        let nestedBlockScope = 3;
        // OK. This is from a wider scope
        globalScope = 1000;
        // OK. This is from a wider scope
        blockScope = 2000;
    }
}
```

Reutilización de un nombre de variable con `let`:

```ts
let firstName = 'Chris';
{
    let firstName = 'Tudor';
    console.log('Name 1: ' + firstName);
}
console.log('Name 2: ' + firstName);
// Output:
// Name 1: Tudor
// Name 2: Chris
```

Hacemos lo mismo con `var`:

```ts
var firstName = 'Chris';
{
    var firstName = 'Tudor';
    console.log('Name 1: ' + firstName);
}
console.log('Name 2: ' + firstName);
// Output:
// Name 1: Tudor
// Name 2: Tudor
```

Como curiosidad, **con `let` no se puede usar una variable antes de declararla**, con `var` esto no pasa:

```ts
a = 6;
var a:number = 5;

b = 6; // Error Block-scoped variable 'b' used before its declaration.
let b:number = 5;
```

En JS se puede prescindir de "var" en la declaración, eso la convierte en global, una práctica peligrosa ya que si la olvidamos se puede sobrescribir su valor fuera del ámbito.

Esto funciona a las mil maravillas en JS y en TS arroja un error:

```js
function suma(a,b) {
  total = a + b;
  return total;
}
total = 3;
console.log("total: " + total);
```

##  3. <a name='CONSTANTES'></a>CONSTANTES

EL ámbito de `const` sigue las mismas reglas que `let` con la salvedad que **no puede ser reasignada**, su valor es inmutable.

Una **práctica de programación en TS para iniciarse con las variables** es usar `const` siempre y luego cambiar a `let` si decides cambiarle el valor.

##  4. <a name='ENLACESINTERNOS'></a>ENLACES INTERNOS

* Taller Angular y TypeScript [https://soka.gitlab.io/blog/post/2021-06-08-taller-angular/](https://soka.gitlab.io/blog/post/2021-06-08-taller-angular/).

##  5. <a name='RECURSOS'></a>RECURSOS

* Transpilador en línea TS a JS con consola y muestra errores [https://www.typescriptlang.org/play]( https://www.typescriptlang.org/play).
* IDE JS muy sencillo y no requiere abrir una cuenta de usuario [https://www.cubicfactory.com/jseditor/](https://www.cubicfactory.com/jseditor/).

##  6. <a name='ENLACESEXTERNOS'></a>ENLACES EXTERNOS

Prueba nombres de variables basado en ECMAScript 6:

* [https://mothereff.in/js-variables](https://mothereff.in/js-variables)
* [https://mathiasbynens.be/notes/javascript-identifiers-es6](https://mathiasbynens.be/notes/javascript-identifiers-es6).

Web de MS con la documentación de TS muy bien elaborada:

* [https://www.typescriptlang.org/docs/handbook/variable-declarations.html](https://www.typescriptlang.org/docs/handbook/variable-declarations.html).