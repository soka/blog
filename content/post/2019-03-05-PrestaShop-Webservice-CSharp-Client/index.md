---
title: Cliente acceso Webservice PrestaShop con C#
subtitle: Webservices, PrestaShop y C#
date: 2019-03-05
draft: false
description: API de PrestaShop
tags: ["PrestaShop","Webservice","CSharp","Servicios Web","API","HTTP","Visual Studio","C#"]
---

![](images/logo_prestashop.jpg)

Este artículo es la continuación lógica de ["Usar servicios Web en PrestaShop"](https://soka.gitlab.io/blog/post/2019-03-04-prestashop-webservice/), busco desarrollar una aplicación básica de ventanas en C# que permita administrar un eCommerce basado en **PrestaShop**, a un nivel muy básico para probar la viabilidad de una comunicación bidireccional para realizar o consultas o dar de alta nuevos productos en el eCommerce. Antes de empezar es necesario que se haya dado de alta un webservice en el backend de administración de **PrestaShop** (con los accesos a los modulos que deseemos).

# Products: Lista de productos

La llamada a `https://tudominio.xxx/dondeestetutienda/api/products` retorna contenido XML codificado como "UTF-8", la cabecera contiene lo siguiente:

```
<?xml version="1.0" encoding="UTF-8"?>
<prestashop xmlns:xlink="http://www.w3.org/1999/xlink">
```

Indica que usa la especificación [XLink](https://www.w3.org/TR/xlink/) que permite introducir elementos para enlazar con recursos en otros documentos, cada producto contiene un enlace al recurso para obtener su detalle (`xmlns:xlink="http://www.w3.org/1999/xlink"` es un namespace).

A continuación viene la lista de productos.

```
    <products>
        <product id="108" xlink:href="https://tudominio.xxx/dondeestetutienda/api/products/108"/>
        <product id="1" xlink:href="https://tudominio.xxx/dondeestetutienda/api/products/1"/>
    </products>
</prestashop>
``` 

# Dependencias y paquetes del proyecto en C# .Net con Visual Studio

Espacios de nombres:

* [System.Xml](https://docs.microsoft.com/es-es/dotnet/api/system.xml?view=netframework-4.7.2).
* [System.Net](https://docs.microsoft.com/en-us/dotnet/api/system.net?view=netframework-4.7.2).
* [System.IO](https://docs.microsoft.com/es-es/dotnet/api/system.io?view=netframework-4.7.2).

# Solicitar datos al servidor - clase HttpWebRequest/Response

La clase [`HttpWebRequest`](https://docs.microsoft.com/es-es/dotnet/api/system.net.httpwebrequest?view=netframework-4.7.2) (espacio de nombres `System.Net` herencia de `WebRequest`) proporciona recursos para realizar llamadas HTTP. Podriamos haber usado la clase `WebClient` que es una abstracción de `HttpWebRequest` simplificando la llamada para el desarrollador y requiere menos líneas de código.

La clase `HttpWebResponse` es la encargada de tratar la respuesta.

La siguiente tables es una equivalencia métodos [HTTP/REST](https://es.wikipedia.org/wiki/Transferencia_de_Estado_Representacional), [CRUD (Create, Read, Update and Delete)](https://es.wikipedia.org/wiki/CRUD) y sentencias SQL con operaciones que podemos realizar:

![](images/http_methods_sql.PNG)

`HttpWebRequest` realiza la petición en un hilo separado sin bloquear la interfaz. Debemos ser nosotros quien pasemos los datos obtenidos a la interface de usuario. 

Creamos una instancia de la clase `HttpWebRequest` llamando al método `Create` con el URI del recurso (la dirección Web de la API de PrestaShop, algo así como "https://tudominio.xxx/dondeestetutienda/api/"). 

```
HttpWebRequest request = (HttpWebRequest)WebRequest.Create(this.PrestaShopAPIUri);
```

Antes de realizar la llamada debemos definir las credenciales de acceso (clave generada previamente en el backend de PrestaShop para el Webservice), usamos la clase [`NetworkCredential`](https://docs.microsoft.com/es-es/dotnet/api/system.net.networkcredential?view=netframework-4.7.2) para definir una nueva instancia pasándole como parámetros de entrada una cadena la clave (nombre de usuario) y la contraseña como cadena vacia. Le pasamos el objeto recién creado a la propiedad `Credentials` de la clase `HttpWebRequest`.

```
NetworkCredential nc = new NetworkCredential(this.PasswordToken, "");
request.Credentials = nc;
```

Establecemos un método para la solicitud HTTP (un SELECT para leer datos):

```
request.Method = "GET";
```

# Leer la respuesta 

Realizamos la llamada usando el método `HttpWebRequest.GetResponse`  y esperamos la respuesta en la variable de tipo [`HttpWebResponse`](https://docs.microsoft.com/es-es/dotnet/api/system.net.httpwebresponse?view=netframework-4.7.2).

```
var response = (HttpWebResponse)request.GetResponse()
```

Comprobamos que la llamada HTTP ha tenido éxito con el [código de estado](https://es.wikipedia.org/wiki/Anexo:C%C3%B3digos_de_estado_HTTP#2xx:_Peticiones_correctas) del atributo `StatusCode` de la clase `HttpWebResponse` (200 indica petición correcta):

```
response.StatusCode == HttpStatusCode.OK
```

El contenido de la respuesta  se devuelve como un [`Stream`](https://docs.microsoft.com/es-es/dotnet/api/system.io.stream?view=netframework-4.7.2) mediante una llamada a la `GetResponseStream` de `HttpWebResponse` leemos el cuerpo del contenido de la respuesta del servidor. 

```
using (var stream = response.GetResponseStream()) {
    using (var sr = new StreamReader(stream)) {
	     content = sr.ReadToEnd();
         // .... tratamiento XML
    }
}
```

`Stream` es una clase genérica para leer secuencias de bytes de un recurso como por ejemplo un fichero, un dispositivo de entrada/salida o un socket TCP/IP. El método `ReadToEnd` lee toda la secuencia y la volcamos en un cadena. 


# Cargar documento XML

Ahora que ya tenemos el fichero XML como una cadena el siguiente paso es cargarlo usando el método `XmlDocument.LoadXml(String)` de la clase `XmlDocument` (espacio de nombres `System.Xml`) para acceder a los nodos usando consultas [XPATH](https://es.wikipedia.org/wiki/XPath) con el método `SelectNodes()`.

```
XmlDocument xmlDoc = new XmlDocument();
xmlDoc.LoadXml(XMLInp);

XmlNodeList nodes = xmlDoc.SelectNodes(XPath);
```

El resultado es una serie de nodos XML donde cada uno representa un producto, recorremos en un bucle los nodos:

foreach (XmlNode childrenNode in nodes) {                                  
    ProductsItem Item = new ProductsItem(childrenNode.Attributes["id"].Value,
                                            childrenNode.Attributes["xlink:href"].Value);
    this.ProductsList.Add(Item);
}

He creado una clase `ProductsItem` para almacenar los atributos de cada producto:

```
public class ProductsItem {
    private string Id;
    private string Xlink;
        
    public ProductsItem(string Id,string Xlink) {
        if (string.IsNullOrEmpty(Id)) {
            throw new ArgumentException("message", nameof(Id));
        }
        this.Id = Id;
        this.Xlink = Xlink;
    }
} 
```

La clase principal `Products` tiene como miembro una lista de `ProductsItem`

```
public List<ProductsItem> ProductsList = new List<ProductsItem>();
```

# Depuración y ejecución

Si queremos depurar el programa en VS hay que definir los argumentos de entrada (menú Proyecto > Opciones).

![](images/vs_debug_options.PNG)

# Comming soon

* Crear una clase nueva para que el código sea más portable para futuros proyectos.
* Añadir funcionalidad [NLog](https://nlog-project.org/) para registrar logs de ejecución de la aplicación (ver mi post en WP "NLog: Sistema de logging gratuito y de código abierto para .Net"](https://ikerlandajuela.wordpress.com/2017/07/23/nlog-sistema-de-logging-gratuito-y-de-codigo-abierto-para-net/)).
* Versionado del proyecto.
* Probar a subir un nuevo producto es esencial para demostrar que el proyecto puede llegar a buen puerto.

# Código fuente

El código fuente seguro que es muy mejorable pero me urge seguir adelante y probar una operación POST para subir un nuevo artículo al eCommerce (se aceptan colaboraciones y sugerencias). 

* csharp / projects / PrestaShop / [PrestaShopAPICLI](https://gitlab.com/soka/csharp/tree/master/projects/PrestaShop/PrestaShopAPICLI): Solución ejercicio para VS2017.

# Enlaces internos

* ["Usar servicios Web en PrestaShop v1.7.0.4"](https://soka.gitlab.io/blog/post/2019-03-04-prestashop-webservice/).
* [Algunos posts](https://ikerlandajuela.wordpress.com/category/programacion/csharp/) en mi blog WP sobre C#.

# Enlaces externos

Documentación oficial webservice PrestaShop:

* ["PrestaShop 1.6 …  Using the PrestaShop Web Service - Web service tutorial"](http://doc.prestashop.com/display/PS16/Web+service+tutorial).
* ["PrestaShop 1.6 …  Using the PrestaShop Web Service - Web service one-page documentation"](http://doc.prestashop.com/display/PS16/Web+service+one-page+documentation).

Generales:

* ["Windows Phone. Leer un RSS."](https://javiersuarezruiz.wordpress.com/tag/httpwebrequest/).
* ["Build an RSS Reader for Windows Phone 7"](https://dzone.com/articles/build-rss-reader-windows-phone).
* ["A Few Great Ways to Consume RESTful APIs in C#"](https://dzone.com/articles/a-few-great-ways-to-consume-restful-apis-in-c).
* ["CONSUMIENDO REST DESDE C#.NET"](http://qbit.com.mx/blog/2012/03/27/consumiendo-rest-desde-c-net/).
* ["Post XML over HTTP and capture the response – C#"](https://rajeevpentyala.com/2016/01/07/post-xml-over-http-and-capture-the-response-c/).
* ["A Few Great Ways to Consume RESTful API in C#"](https://code-maze.com/different-ways-consume-restful-api-csharp/).
* ["Dynamic Usage of RESTful API With C# and .NET"](https://wiki.scn.sap.com/wiki/display/BOBJ/Dynamic+Usage+of+RESTful+API+With+C%23+and+.NET).
* ["Generate HTTP Requests using c#"](https://codesamplez.com/programming/http-request-c-sharp).

C# y XML,deserialización...: 

* ["Using Returned XML with C#"](https://developer.yahoo.com/dotnet/howto-xml_cs.html).
* ["WebClient vs HttpClient vs HttpWebRequest"](https://www.diogonunes.com/blog/webclient-vs-httpclient-vs-httpwebrequest/).
* ["WebClient, HttpWebRequest or HttpClient to perform HTTP requests in .NET?"](http://codecaster.nl/blog/2015/11/webclient-httpwebrequest-httpclient-perform-web-requests-net/).
* ["Simple approach how to deserialize XML to C# object without attributes."](http://www.janholinka.net/Blog/Article/11).
* ["Convert XML to C# Object - CodeProject"](https://www.codeproject.com/Articles/1163664/Convert-XML-to-Csharp-Object).
* ["C# Object To XML and Vice Versa using C#"](https://codehandbook.org/c-object-xml/).
* ["Serializing/Deserializing XML Attribute Lists"](https://blog.bitscry.com/2017/11/21/serializing-deserializing-xml-attribute-lists/).
* ["XMLSerializer y XML Attributes"](https://www.returngis.net/2010/05/xmlserializer-y-xml-attributes/).
* ["Listado de elementos de un XML a .NET con XMLSerializer"](https://www.returngis.net/2015/09/listado-de-elementos-de-un-xml-a-net-con-xmlserializer/).
* ["From XML to Strong Types in C#"]().

APIs y conectores:

* ["PSWebServiceLibrary.php"](https://github.com/PrestaShop/PrestaShop-webservice-lib/blob/master/PSWebServiceLibrary.php).
* ["PrestaShop/PrestaShop-webservice-lib"](PrestaShop/PrestaShop-webservice-lib).

Sintaxis CSharp:

* ["HttpWebRequest Class"](https://docs.microsoft.com/es-es/dotnet/api/system.net.httpwebrequest?view=netframework-4.7.2).
* ["NetworkCredential Class (System.Net) | Microsoft Docs"](https://docs.microsoft.com/es-es/dotnet/api/system.net.networkcredential?view=netframework-4.7.2).
* ["Procedimiento para solicitar datos mediante la clase WebRequest"](https://docs.microsoft.com/es-es/dotnet/framework/network-programming/how-to-request-data-using-the-webrequest-class).
* ["HttpWebRequest.GetResponse Method"](https://docs.microsoft.com/es-es/dotnet/api/system.net.httpwebrequest.getresponse?view=netframework-4.7.2): Devuelve una respuesta desde un recurso de Internet.
C#.
* ["HttpWebResponse.GetResponseStream Method"](https://docs.microsoft.com/es-es/dotnet/api/system.net.httpwebresponse.getresponsestream?view=netframework-4.7.2): Obtiene la secuencia usada para leer el cuerpo de la respuesta del servidor.


XPATH:

* ["XPath: XML Path language"](http://www.mclibre.org/consultar/xml/lecciones/xml-xpath.html).
* ["Introducción al uso de las consultas XPath (SQLXML 4.0) - SQL ..."](https://docs.microsoft.com/es-es/sql/relational-databases/sqlxml-annotated-xsd-schemas-xpath-queries/introduction-to-using-xpath-queries-sqlxml-4-0?view=sql-server-2017).
* ["XPath Tester / Evaluator"](https://www.freeformatter.com/xpath-tester.html#ad-output).

**Librerías y paquetes:**

* ["Bukimedia/PrestaSharp"](https://github.com/Bukimedia/PrestaSharp): CSharp .Net client library for the PrestaShop API via web service.

