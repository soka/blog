---
title: Programación en Electron 
subtitle: Introducción básica
date: 2018-11-03
draft: false
toc: true
description: Descripción básica del entorno de trabajo Electron
tags: ["Electron", "Programación"]
---

En este [**enlace**](https://soka.gitlab.io/electron/) puedes encontrar mi Web sobre [**Electron**](https://electronjs.org/)  con ejercicios (en continuo progreso).

![](images/electron_logo_01.png)

Recientemente he podido dedicar algo de mi escaso tiempo de ocio a programar aplicaciones de escritorio usando [**Electron**](https://electronjs.org/). 

Cuando comenze a trabajar con [GitHub](https://github.com/ikerlandajuela), como cliente gráfico [Git](https://git-scm.com/) para Windows usaba [GitHub Desktop](https://desktop.github.com/), si acudimos al repositorio público del código fuente del proyecto una de las primeras cosas que se menciona es que la aplicación está programada en [**Electron**](https://electronjs.org/), si investigamos un poco más también **el editor de texto [Atom](https://atom.io/) o la aplicación de mensajería [Slack](https://slack.com/intl/es-es/downloads/windows) están creadas usando** [**Electron**](https://electronjs.org/), [la lista completa](https://electronjs.org/apps) de aplicaciones programadas usando esta tecnología crece cada día. 

![](images/electron_apps_list.png)

Lo que me llamo la atención de las aplicaciones arribas mencionadas es que tienen en comun un diseño visual muy atractivo basado en **Electron**. Ni corto ni perezoso he emprendido una nueva cruzada hasta que consiga hacer aplicaciones de uso real programadas con **Electron**.

## ¿Qué es Electron?

Electron fue creado en 2013 por [Cheng Zhao](https://github.com/zcbenz) ingeniero en GitHub. Inicialmente fue desarrollado para el editor de textos [Atom](https://atom.io/) con el nombre de [Atom Shell](https://electronjs.org/blog/electron) (["Atom Shell is now Electron"-April 23, 2015](https://electronjs.org/blog/electron)).

![](images/electron_web_caracteristicas_02.png)

Electron es un framework de [**código abierto**](https://es.wikipedia.org/wiki/C%C3%B3digo_abierto) (alojado de forma pública en [GitHub](https://github.com/electron/electron)) y **multiplataforma** que permite crear aplicaciones nativas con **tecnologías Web como Javascript, HTML y CSS**. Utiliza páginas Web como interfaz gráfica de usuario por lo que, se podría ver como un navegador Chrome (que internamente posee el motor de Chromium) controlado por Javascript. También se puede ver como una variante de Node.js centrada en aplicaciones de ventanas de escritorio en vez de un servidor Web. 

![](images/electron_web_caracteristicas_01.png)

**Algunas de sus caraterísticas más notables son**:

* [**Actualizaciones automáticas de nuestras aplicaciones (autoUpdater)**](https://electronjs.org/docs/tutorial/updates). Se instala como un modulo de nuestra aplicación (`update-electron-app`) y busca nuevas versiones en un servidor de actualizaciones, existen diferentes alternativas de terceros pero cuando es un proyecto público alojado en GitHub lo más fácil parece usar [update.electronjs.org](https://github.com/electron/update.electronjs.org), un servicio Web gratuito creado por el propio equipo de Electron. 
* [**Menús nativos**](https://electronjs.org/docs/api/menu): La API de Electron provee la clase `Menu` para crear menús.
* [**Notificaciones**](https://electronjs.org/docs/api/notification): Crea las notificaciones de escritorio del sistema operativo usando [HTML5 Notification API](https://developer.mozilla.org/es/docs/Web/API/Notifications_API/Usando_la_API_de_Notificaciones). Tutorial [Docs / Guides / Notificaciones (Windows, Linux, macOS)](https://electronjs.org/docs/tutorial/notifications).
* [**Informe de errores (crashReporter)**](https://github.com/electron/electron/blob/master/docs/api/crash-reporter.md): Maneja informes cuando se produce un fallo de la aplicación, puede envíar informes de fallas a un servidor remoto. Los informes de fallos se guardan localmente en una carpeta de directorio temporal específica de la aplicación.
* [**Depuración y optimización**](https://electronjs.org/docs/tutorial/application-debugging): Electron soluciona este tema usando el conjunto de herramientas de depuración de [Chrome DevTools](https://developers.google.com/web/tools/chrome-devtools/). Se pueden abrir mediante programación usando la llamada `openDevTools()` de `webContents` de una instancia de la ventana `BrowserWindow`.
* [**Instaladores de Windows**](https://github.com/electron/windows-installer): Es un modulo NPM para generar instaladores para Windows usando [Squirrel](https://github.com/Squirrel/Squirrel.Windows).

## ¿Como funciona Electron?

**Electron** combina la librería de renderizado de [Chromium](https://www.chromium.org/) (versión de código abierto del navegador Web de Google) con la API de Node.js en un entorno de ejecución. Node.js es un entorno de ejecución de Javascript construido con el motor JavaScript V8 de Chrome. Fue publicado en 2009 como un proyecto de código abierto. Permite crear aplicaciones del lado del servidor usando JavaScript. Node.js y Chromium comparten el mismo motor JavaScript [V8](https://v8.dev/) de Google escrito en C++ (el motor V8 se puede incrustar en cualquier aplicación C++ para interpretar JavaScript). 

![](images/web_v8.png)  

**Las aplicaciones basadas en Electron se ejecutan en dos procesos**: 

* **El proceso principal ("main process")**: maneja varias actividades a nivel de sistema, como los eventos del ciclo de vida de la aplicación, también es el proceso donde se crean los menús de la aplicación y otros cuadros de dialogo nativos como abrir o salvar un archivo. El proceso principal es el fichero JavaScript referenciado por el fichero packages.json.
* **Proceso de renderizado**: Muestra la UI de nuestra app. Cada uno de los procesos de renderizado carga su contenido y lo ejecuta en un hilo separado. Podemos desplegar tantas ventanas como queramos para nuestra app. El proceso de renderizado está aislado de cualquier interacción con los eventos a nivel de sistema, Electron incluye un sistema de comunicación el proceso principal y cualquier proceso de renderizado.

![](images/electron_two_process.png)

Cada uno de estos procesos juega un papel específico. Por ejemplo las APIs del SO estan restringidas al proceso principal. Esto debe ser tenido en cuenta a la hora de diseñar nuestras aplicaciones. 


## ¿Como empiezo a programar en Electron?

Yo he seguido la guía rápida para generar una aplicación mínima de Electron en pocos minutos 

Si necesitas ayuda puedes recurrir al [foro de discusión](https://discuss.atom.io/c/electron) o al canal de [Slack](http://atom-slack.herokuapp.com/).  También puedes seguir las últimas novedades en Twitter [@electronjs](https://twitter.com/electronjs).


## Conclusiones 

Entre las ventajas principales que podemos citar es que Electron es **multiplataforma** (macOS, Windows y Linux), se pueden **desarrollar aplicaciones relativamente rápido** para los principales sistemas operativos, poder desarrollar aplicaciones de escritorio abre un nuevo mundo para la gente que proviene de desarrollar sitios Web.

Electron puede ser enriquecido o combinado con frameworks JavaScript como [Angular](https://angular.io/) (Google), [React](https://reactjs.org/) (Facebook) o [Vue.js](https://vuejs.org/). Tenemos a nuestro alcance bibliotecas para facilitar el diseño de los interfaces como [Bootstrap](https://getbootstrap.com/) o [Photon](http://photonkit.com/) (parece que está horientado claramente a usarlo con Electron) y infinidad de paquetes Node.js.

Entre los detractores una de las razones principales esgrimidas para no usar Electron es **el rendimiento de las aplicaciones**, cada aplicación viene acompañada de su versión de [Chromium](https://www.chromium.org/) y dicen que en tiempo de ejecución consumen mucha memoria RAM. El mismo autor que practicamente denomima como cancer a Electron en otro post somete a varios editores de texto a varias pruebas de rendimiento ([Why I Still Use Vim](https://medium.com/commitlog/why-i-still-use-vim-67afd76b4db6) - Casper Beyer), en la comparativa compiten Atom y [Visual Studio Code](https://code.visualstudio.com/) ambos creados usando **Electron** y no salen precisamente bien parados, aunque también es verdad que compiten con editores de la vieja guardia como [Vi](https://es.wikipedia.org/wiki/Vi) (uno de mis editores más queridos) o [Nano](https://es.wikipedia.org/wiki/GNU_Nano) bastante espartanos y sin entorno gráfico. Esto tal vez sea un asunto sin importancia para los usuarios finales mientras funcionen las aplicaciones, pero desde el punto de vista de un desarrollador cobra otra dimension.

Otra razón de algunos para denostar a Electron es que **las aplicaciones no son realmente nativas**, no dejan de ser Webs y nunca se integrarán con el sistema operativo como se supone que deberían hacerlo (o como lo hacen aplicaciones desarrolladas en C# o Java por ejemplo). 

Lo que es indudable es que Electron está ganando popularidad, en los últimos años empresas y proyectos de código abierto lo han adoptado como tecnología. 

![](images/electron_downloads.png)
 

## Enlaces externos

* [@ElectronJS](https://twitter.com/electronjs) Twitter.
* [Docs / Guides / Actualización de aplicaciones](https://electronjs.org/docs/tutorial/updates).
* [Quick Start](https://github.com/electron/electron/blob/master/docs/tutorial/quick-start.md).
* [Writing Your First Electron App](https://github.com/electron/electron/blob/master/docs/tutorial/first-app.md).
* Documentación de Electron [Docs / API / Clase: Menu](https://electronjs.org/docs/api/menu).
* [Working with Electron Menus - Tutorial](https://coursetro.com/posts/code/119/Working-with-Electron-Menus---Tutorial).
* Documentación de Electron [Docs / API / Notification](https://electronjs.org/docs/api/notification).
* DZone [Build Desktop Apps Using Electron: The Beginner’s Guide](https://dzone.com/articles/build-desktop-apps-using-electron-the-beginners-gu).
* freeCodeCamp [How to Build Your First Desktop App with JavaScript Using Electron](https://medium.freecodecamp.org/how-to-build-your-first-app-with-electron-41ebdb796930).
* [Electron is Cancer](https://medium.com/commitlog/electron-is-cancer-b066108e6c32).
* [The desktop belongs to Electron](https://www.theverge.com/circuitbreaker/2018/5/16/17361696/chrome-os-electron-desktop-applications-apple-microsoft-google).
* [Electron: The Bad Parts](https://hackernoon.com/electron-the-bad-parts-2b710c491547).
* [How To Make Your Electron App Sexy](https://blog.dcpos.ch/how-to-make-your-electron-app-sexy).
* [The Electron Open Framework and Why It’s Awesome for Apps Development](https://www.makeuseof.com/tag/electron-open-framework-apps-development/).
* [What Is Electron and Why Should We Use it?](https://dzone.com/articles/what-is-electron-amp-why-should-we-use-it).
* [Devtron - An Electron DevTools Extension](https://electronjs.org/devtron).
* [Notifications API](https://notifications.spec.whatwg.org/).
* [Why I Still Use Vim](https://medium.com/commitlog/why-i-still-use-vim-67afd76b4db6).
* [Atom Shell is now Electron](https://electronjs.org/blog/electron).
* Documentación de Electron [Docs / Guides / Aplicación de Depuración](https://electronjs.org/docs/tutorial/application-debugging).
* [Photon](https://github.com/connors/photon) en GitHub, conjunto de herramientas para Electron para diseñar la interface de usuario.
* [Into an Electron App with Photon (Part I)](https://medium.com/@philipaffulnunoo/into-an-electron-app-with-photon-part-i-d0cabbcf6307).
* [Electron 1.0](https://electronjs.org/blog/electron-1-0) May 11, 2016.