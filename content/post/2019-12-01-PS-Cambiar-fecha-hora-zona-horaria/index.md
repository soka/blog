---
title: Configurar la hora, fecha y zona horaria
subtitle: Administración de Windows Server con PowerShell
date: 2019-12-01
draft: false
description: Administración servidores Windows
tags: ["Windows","Sistemas","PowerShell","Administración","cmdlets","Scripting"]
---

![](img/logo2.png)

## Establecer la fecha-hora

El cmdlet `Get-Date` muestra la fecha-hora del sistema, para establecer la fecha usamos `Set-Date`.

Establezco la fecha sumando 3 días a la actual:

```ps
Set-Date -Date (Get-Date).AddDays(3)
```

Podemos usar un número negativo para restar fechas o agregar / restar horas con el método de la clase `AddHours`.

## Zona horaria

Consultar la zona horaria establecida:

```ps
tzutil /g
```

Para consultar las posibles zonas horarias:

```ps
tzutil /l | more
```

Copia la cadena literal entre las opciones para establecer una nueva zona horaria:

```ps
tzutil /s "Romance Standard Time"
```


## Enlaces

- [Get-Date](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-date?view=powershell-6).
- [Set-Date](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/set-date?view=powershell-6).
- [Setting System Time with PowerShell - TechNet - Microsoft](https://social.technet.microsoft.com/Forums/windowsserver/en-US/1a4467e5-9b9e-4ef1-9b22-4af3a1a77362/setting-system-time-with-powershell?forum=winserverpowershell).
