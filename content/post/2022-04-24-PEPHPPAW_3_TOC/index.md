---
title: Curso Programación Efectiva en PHP para Aplicaciones Web - Índice
subtitle: Índice de contenidos
date: 2022-04-24
draft: false
description: Índice de contenidos
tags: ["PEPHPPAW_3","Cursos","Miriadax","Programación","Web","Desarrollo","HTML","TOC","Contenidos","Índice"]
---

## Módulo 0

* [Lenguaje de etiquetas HTML](https://soka.gitlab.io/blog/post/2022-04-25-pephppaw_3-modulo_0-01-lenguaje_etiquetas_html/).
* [Lenguaje de estilos CSS](https://soka.gitlab.io/blog/post/2022-04-25-pephppaw_3-modulo_0-02-lenguaje_de_estilos_css/).
* [Introducción Framework Bootstrap](https://soka.gitlab.io/blog/post/2022-04-26-pephppaw_3-modulo_0-03-intro_framework_bootstrap/).
* [Test de conocimientos](https://soka.gitlab.io/blog/post/2022-04-27-pephppaw_3-modulo_0-04-test-conocimientos/).
* [Actividad práctica](https://soka.gitlab.io/blog/post/2022-04-27-pephppaw_3-modulo_0-05-actividad-pr%C3%A1ctica/)

## Módulo 1

* [Arquitectura Cliente Servidor](https://soka.gitlab.io/blog/post/2022-04-28-pephppaw_3-modulo_1-01-arquitectura_cliente_servidor/).



