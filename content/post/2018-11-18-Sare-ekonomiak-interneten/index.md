---
title: Sare ekomonomiak Interneten eta Mastodon
subtitle: Txikiak jan dezake handia?
date: 2018-11-18
draft: false
description: Eskala eta sare ekonomiak
tags: ["Opinión", "Redes","Artículos","Euskaraz"]
---

![](images/betamax-214x300.jpg)

Gure adinekoen artean aski ezagunak dira Betamax (Sony) edo VHS bideo irakurgailuak baina gaur egungo "milenial" gutxik ikusiko zituzten, agian gurasoen etxeko apalean bakero filme bilduma bat hautsa pilatzen gehienez jota (nire aitaren filmak gogoan ditut txukun denak ordenatuta). 

Hasiera baten bi formatu hauek eta beste batzuk merkatu kuota antzekoa zuten 80. hamarkadan, Betamax bideo formatuak zenbait ezaugarri hobeak zituen ala nola irudien kalitatea eta zinten tamaina txikiagoa, VHS-ak aldiz abantaila hartu zion, 2 orduz grabatu zezakeen eta Betamax-ek ordu bat baino ez, VHS zintak merkeagoak ziren eta irakurgaileen prezioak ere jeitsi zituzten, **formatuen eta estandarren arteko lehia** hasi zen merkatuan. 

{{< youtube qaTFmyAhF9Y >}}

**Bideokluben etorrerarekin** VHS formatuari bultzada eman zioten aren alde eginez 2 ordu grabatu zitzazkeelako batez ere, honek **sare ekonomia deritzona sortu zuen**, VHS bideo grabatzaileek masa kritiko bat lortu zutenean "de facto" estandar bat sortu zuen merkatuan beste danak alboratuz. 

**Sistema batek geroz eta erabiltzaile gehiago izan orduan eta interakzio gehiago izateko aukera sortzen da**, zure bizilagunak VHS sistema bazuen zuk ere berdina behar zenuen filmak elkar trukatzeko. Fax-ari halakoxe gertatzen hari zaio, **zertarako balio dezake fax makina batek nirea bakarra bada? Norbaitek bigarren Fax makina erosten duenean orduan bakarrik nirea ere erabilgarria da**. 

{{< youtube NoNyGPnZACs >}}

**Sare ekonomia komunikazio teknologietan oso ondo uler daiteke**, Twitter, WhatsApp edo Facebook-en kasu, geroz eta erabiltzaile gehiago izan orduan eta erakargarriagoa da sartzea bertan bait daude gure lagun, senitarteko, lan kontaktuak etab. **Mastodon** bezalako lehiakideei oso zail egiten zaie behar beste erabiltzaile erakartzea alternatiba hori erakargarriagoa bihurtzeko.

# Estekak

* ["QUE ES EFECTO RED: DEFINICION "](https://iiemd.com/efecto-red/que-es-efecto-red).
* ["El efecto red y por qué te cuesta tanto dejar Facebook"](https://verne.elpais.com/verne/2018/04/17/articulo/1523972987_879831.html)
* ["Economías de Red"](http://www.mordecki.com/html/economias_de_red.php).
* ["Efecto red: llegar alto por la suma de usuarios"](https://www.eleconomista.es/mercados-cotizaciones/noticias/9031891/03/18/Efecto-red-llegar-alto-por-la-suma-de-usuarios.html).
* ["¿QUÉ SON LAS EXTERNALIDADES DE RED?"](https://economy.blogs.ie.edu/archives/2009/10/externalidades-de-red/).
* Wikipedia ["Efecto de red"](https://es.wikipedia.org/wiki/Efecto_de_red)

