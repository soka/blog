---
title: Lenguaje de estilos CSS
subtitle: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 0
date: 2022-04-25
draft: false
description: Curso Programación Efectiva en PHP para Aplicaciones Web - Módulo 0
tags: ["PEPHPPAW_3","Cursos","Miriadax","Programación","Web","Desarrollo","HTML","CSS"]
---

<!-- vscode-markdown-toc -->
* 1. [Lenguaje de estilos en cascada CSS](#LenguajedeestilosencascadaCSS)
* 2. [Sintaxis](#Sintaxis)
* 3. [Selectores](#Selectores)
* 4. [Colores](#Colores)
* 5. [Unidades de medida](#Unidadesdemedida)
* 6. [Estilos populares en CSS](#EstilospopularesenCSS)
	* 6.1. [Float](#Float)
	* 6.2. [Link](#Link)
	* 6.3. [Margins](#Margins)
	* 6.4. [Overflow](#Overflow)
	* 6.5. [Padding](#Padding)
	* 6.6. [Borders](#Borders)
	* 6.7. [Position](#Position)
	* 6.8. [Width & Height](#WidthHeight)
	* 6.9. [Backgrounds](#Backgrounds)
	* 6.10. [Tables](#Tables)
	* 6.11. [Fonts](#Fonts)
	* 6.12. [Colors](#Colors)
	* 6.13. [List](#List)
	* 6.14. [Align](#Align)
* 7. [Video parte I -  Módulo 0 Lenguaje CSS parte 1](#VideoparteI-Mdulo0LenguajeCSSparte1)
* 8. [Video parte II - Módulo 0 Lenguaje CSS parte2](#VideoparteII-Mdulo0LenguajeCSSparte2)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='LenguajedeestilosencascadaCSS'></a>Lenguaje de estilos en cascada CSS

CSS es un lenguaje que le permite agregar estilos a un lenguaje de marcado, aplicable también a documentos de tipo XML o SVG, generalmente se usa para páginas web (el lenguaje de marcado es HTML). 

CSS está diseñado para usarse en documentos fuera del contenido en sí, agregando colores, fuentes y diseño al documento anterior. El lenguaje se basa en las especificaciones creadas por W3C (World Wide Web Consortium) y por lo tanto funciona de la misma forma en diferentes implementaciones (diferentes navegadores, sistemas operativos, etc.).

**Ejemplo**: en color azul se resalta la aplicación una regla del Lenguaje CSS al elemento `<h1>` de un documento HTML.

```html
<!DOCTYPE html>
<html>
<head>
<style>
h1 { background-color: rgb(38, 99, 229); }
</style>
</head>
<body>
<h1>Este elemento tiene un color de fondo RGB (38, 99, 229 ) </h1>
</body>
</html>
```

##  2. <a name='Sintaxis'></a>Sintaxis

La sintaxis permite definir la forma correcta de escribir las reglas a aplicar a los elementos de una página web. 

**Estructura**: 

```
selector { propiedad : valor; 
propiedad : valor; } 
```

Para comprender la sintaxis de CSS es importante comprender las siguientes palabras clave:

* **Regla**: cada regla esta compuesta por "selectores", un símbolo de "llave de apertura" ( { ), otra parte denominada "declaraciones" y al finalizar un símbolo de "llave de cierre" ( } ).
* **Selector**: permite indicar el elemento o elementos HTML a los que se aplica la regla CSS.
* **Declaración**: especifica los estilos que se aplican a los elementos. Se compone de una o mas propiedades CSS con sus valores.
* **Propiedad**: permite modificar el aspecto de una característica del elemento.
* **Valor**: permite ajustar el nuevo valor de la característica modificada en el elemento

Ejemplo:

```css
h1 { background-color: rgb(38, 99, 229); }
```

Donde:

* Regla: h1 { background-color: rgb(38, 99, 229); }
* Selector: h1
* Declaración: { background-color: rgb(38, 99, 229); } 
* Propiedad: background-color: 
* Valor: rgb(38, 99, 229) 

##  3. <a name='Selectores'></a>Selectores

Los selectores son parte fundamental de las regla CSS y permiten indicar a qué elemento o elementos de la página se deben aplicar los ajustes al estilo. Se llama "selector" porque "selecciona" una parte de la página.

**Selector universal**: permite aplica el estilo a todos los elementos del documento

```css
* { margin: 0; padding: 0; }
```

**Selector de etiqueta**: permite aplica el estilo a una etiqueta 

```css
h1 { background-color: rgb(38, 99, 229); } 
```

**Selector de clase**: permite aplica el estilo a una clase

Código html:

```html
<p class="textorojo">este texto está en rojo</p>
```

Codigo css:

```css
.textorojo { color: red;}
```

**Etiquetas `<div>` `<span>`**: permite aplica estilos a elementos muy pequeños o muy grandes.

Código html:

```html
<p>El <span class="rojo"> rojo </span> y el <span class="azul"> azul </span> son colores importantes.</p>
```

Código css:

```css
.rojo { color: red; } .azul { color: blue; }
```

**Selector de ID**: permite aplicar el estilo a un ID determinado 

código html:

```html
<h1>Bienvenido a este sitio web</h1>

<div class="intro">

  <p id="firstname">Mi nombre es Francisco.</p>

  <p id="hometown">Vivo en Colombia.</p>

</div>

<p>Mi mascota se llama Dante.</p>
```

código css:

```css
#firstname { background-color: blue; }
```

**NOTA**: para hacer uso de cada uno de los ejemplos y observar su funcionalidad, debe tener en cuenta colocar el codigo HTML dentro de las etiquetas `<body></body>` de un documento con formato HTML y los estilos CSS dentro de una etiqueta `<style></style>` como se observa a continuación. Luego de guardarlo con extensión (.html), puede abrirlo mediante el navegador de su preferencia.

**Ejemplo**: el texto en azul puede copiarlo y pegarlo en un bloc de notas y guardarlo con el nombre ejemplo y con la extensión (.html)

```html
<!DOCTYPE html>

<html>

<head>

<style>

#firstname {

  background-color: blue;

}

</style>

</head>

<body>

<h1>Bienvenido a este sitio web</h1>

<div class="intro">

  <p id="firstname">Mi nombre es Francisco.</p>

  <p id="hometown">Vivo en Colombia.</p>

</div>

<p>Mi mascota se llama Dante.</p>

</body>

</html>
```

##  4. <a name='Colores'></a>Colores

En el lenguaje de estilos CSS, los colores se puede asignar de varias formas.

* **Por nombre predefinido**: especificando el nombre del color predefinido. CSS y HTML tiene predefinido unos nombres de colores que se pueden consultar en la web.
* **Por código RGB**: a través del código de colores (Red, Green, Blue). Así: rgb(255, 99, 71) 
* **Por código HEX**: a través del código de colores hexadecimal. Así: #ff6347 
* **Por código HSL**: a través del sistema de color HSL (Hue, Saturation, Intensity) (Matiz, Saturación, Intensidad). Así: hsl(9, 100%, 64%) 

**Ejemplo:**

```html
<!DOCTYPE html>

<html>

<body>

<p>Por nombre de color "Tomato":</p>

<h1 style="background-color:rgb(255, 99, 71);">rgb(255, 99, 71)</h1>

<h1 style="background-color:#ff6347;">#ff6347</h1>

<h1 style="background-color:hsl(9, 100%, 64%);">hsl(9, 100%, 64%)</h1>

<p>Por nombre de color "Tomato", con 50% de transparencia:</p>

<h1 style="background-color:rgba(255, 99, 71, 0.5);">rgba(255, 99, 71, 0.5)</h1>

<h1 style="background-color:hsla(9, 100%, 64%, 0.5);">hsla(9, 100%, 64%, 0.5)</h1>

<p>Además de los nombres de color predefinidos, los colores se pueden especificar usando RGB, HEX, HSL o incluso colores transparentes usando valores de color RGBA o HSLA.</p>

</body>
</html>
```

##  5. <a name='Unidadesdemedida'></a>Unidades de medida

CSS permite definir varios tipos de unidades de medida y es importante conocerlas, con el fin de tener claridad en su forma de uso.

**Unidades absolutas**: no dependen de otro valor

* in: pulgadas
* cm: centimetros
* mm: milimetros
* pt: puntos
* pc: picas

**Unidades relativas**: dependen de otro valor

* em: relativa respecto del tamaño de letra del elemento
* ex: relativa respecto de la altura, tipo y tamaño de letra del elemento
* px: relativa con respecto a la resolución de pantalla
* Porcentajes: dependen de otro valor y finalizan con el símbolo %

**Ejemplo unidades absolutas:**

```css
/* El cuerpo de la página debe mostrar un margen de media pulgada */
body { margin: 0.5in; } 
/* Los elementos <h1> deben mostrar un interlineado de 3 centímetros */
h1 { line-height: 3cm; } 
/* Las palabras de todos los párrafos deben estar separadas 5 milímetros entre si */
p { word-spacing: 5mm; } 
/* Los enlaces se deben mostrar con un tamaño de letra de 13 puntos */
a { font-size: 13pt } 
/* Los elementos <span> deben tener un tamaño de letra de 1 pica */
span { font-size: 1pc }
```

**Ejemplo unidades relativas:**

```css
body { font-size: 12px; }
h1 { font-size: 2.6em; }
```

**Ejemplo unidades porcentuales:**

```css
body { font-size: 1em; }
h1 { font-size: 210%; }
h2 { font-size: 160%; }
div#contenidos { width: 500px; }
div.principales { width: 70%; }
```

##  6. <a name='EstilospopularesenCSS'></a>Estilos populares en CSS

###  6.1. <a name='Float'></a>Float

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de posicionamiento flotante de un elemento HTML. (left, right, none, inherit)

En el siguiente ejemplo se establecen las propiedades de posicionamiento flotante del elemento `<img>`. 

[./src/float.html](./src/float.html)

```html
<!DOCTYPE html>

<html>
  <head>
    <style>
      img {
        float: right;
      }
    </style>
  </head>

  <body>
    <p>
      En este ejemplo, la imagen flotará hacia la derecha en el párrafo y el
      texto del párrafo se ajustará alrededor de la imagen.
    </p>

    <p>
      <img
        src="foto.jpg"
        alt="foto"
        style="width: 640px; height: 480px; margin-left: 15px"
      />

      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus
      imperdiet, nulla et dictum interdum, nisi lorem egestas odio, vitae
      scelerisque enim ligula venenatis dolor. Maecenas nisl est, ultrices nec
      congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet.
      Mauris ante ligula, facilisis sed ornare eu, lobortis in odio. Praesent
      convallis urna a lacus interdum ut hendrerit risus congue. Nunc sagittis
      dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero sed nunc
      venenatis imperdiet sed ornare turpis. Donec vitae dui eget tellus gravida
      venenatis. Integer fringilla congue eros non fermentum. Sed dapibus
      pulvinar nibh tempor porta. Cras ac leo purus. Mauris quis diam velit.
    </p>
  </body>
</html>
```

![](img/01.png)

###  6.2. <a name='Link'></a>Link

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de estilo de un link.

En el siguiente ejemplo se establecen las propiedades para un link. 

[src/link.html](src/link.html)

```html
<!DOCTYPE html>

<html>
  <head>
    <style>
      /* Link no visitado */

      a:link {
        color: red;
      }

      /* Link visitado */

      a:visited {
        color: green;
      }

      /* Al pasar el mouse */

      a:hover {
        color: yellow;
      }

      /* Link seleccionado */

      a:active {
        color: blue;
      }
    </style>
  </head>

  <body>
    <p>
      <b
        ><a href="https://www.miriadax.net" target="_blank"
          >Este es un link</a
        ></b
      >
    </p>
  </body>
</html>
```

###  6.3. <a name='Margins'></a>Margins

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de las márgenes para algunos elementos HTML.

En el siguiente ejemplo se establecen las propiedades de las márgenes para el elemento <div> de un documento HTML. 


```html
<!DOCTYPE html>

<html>
  <head>
    <style>
      div {
        margin: 80px;

        border: 1px solid #4caf50;
      }
    </style>
  </head>

  <body>
    <div>Este elemento tiene un márgen de 80px.</div>
  </body>
</html>
```

###  6.4. <a name='Overflow'></a>Overflow

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de desbordamiento de un elemento HTML cuando su contenido es muy grande. (Visible, hidden, scroll, auto)

En el siguiente ejemplo se establecen las propiedades de desbordamiento del elemento <div>. 

[src/overflow.html](src/overflow.html)

![](img/02.png)

###  6.5. <a name='Padding'></a>Padding

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de relleno entre el contenido de un elemento HTML y sus bordes.

En el siguiente ejemplo se establecen las propiedades de padding para el elemento `<div>` de un documento HTML. 

[src/padding](src/padding.html)

![](img/03.png)

###  6.6. <a name='Borders'></a>Borders

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de los bordes para algunos elementos HTML.

En el siguiente ejemplo se establecen las propiedades de los bordes para el elemento `<h1>` de un documento HTML. 

[src/borders.html](src/borders.html)

![](img/04.png)

###  6.7. <a name='Position'></a>Position

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de posicionamiento de un elemento HTML. (Static, relative, fixed, absolute, sticky)

En el siguiente ejemplo se establecen las propiedades de posicionamiento relative y absolute del elemento `<div>`. 

[src/position.html](src/position.html)

```html
<!DOCTYPE html>

<html>
  <head>
    <style>
      div.relative {
        position: relative;
        width: 500px;
        height: 230px;
        border: 3px solid #83a4eb;
      }

      div.absolute {
        position: absolute;
        top: 80px;
        right: 0;
        width: 250px;
        height: 150px;
        border: 3px solid #83a4eb;
      }
    </style>
  </head>

  <body>
    <h2>Position: relative/absolute</h2>

    <div class="relative">
      Este elemento posee un Position: relative;

      <div class="absolute">Este elemento posee un Position: absolute;</div>
    </div>
  </body>
</html>
```

![](img/05.png)

###  6.8. <a name='WidthHeight'></a>Width & Height

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de ancho y alto para algunos elementos HTML.

En el siguiente ejemplo se establecen las propiedades de ancho y alto para para el elemento <div> de un documento HTML. 

[src/width_height.html](src/width_height.html)

```html
<!DOCTYPE html>
<html>
  <head>
    <style>
      div {
        width: 500px;
        height: 100px;
        background-color: rgb(0, 0, 129);
      }
    </style>
  </head>

  <body>
    <h2>Configuración ancho y alto de un elemento</h2>

    <p>Este elemento tiene un ancho de 500px y una altura de 100px:</p>

    <div></div>
  </body>
</html>
```

![](img/06.png)

###  6.9. <a name='Backgrounds'></a>Backgrounds

A través del Lenguaje de estilo en cascada CSS se puede configurar el color de fondo de algunos elementos HTML.

En el siguiente ejemplo se establece un color de fondo en código RGB para el elemento `<h1>` de un documento HTML.

[src/backgrounds.html](src/backgrounds.html)


```html
<!DOCTYPE html>
<html>
  <head>
    <style>
      h1 {
        background-color: rgb(38, 99, 229);
      }
    </style>
  </head>

  <body>
    <h1>Este elemento tiene un color de fondo RGB (38, 99, 229 )</h1>
  </body>
</html>
```

![](img/07.png)

###  6.10. <a name='Tables'></a>Tables

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de estilo de las tablas en HTML.

En el siguiente ejemplo se establecen las propiedades de estilo a una tabla de vehículos. 

[src/tables.html](src/tables.html)

```html
<!DOCTYPE html>

<html>
  <head>
    <style>
      table,
      td,
      th {
        border: 1px solid #ddd;
        text-align: left;
      }

      th {
        background-color: rgb(83, 117, 191);
      }

      td {
        background-color: rgb(131, 164, 235);
      }

      table {
        border-collapse: collapse;
        width: 100%;
      }

      th,
      td {
        padding: 15px;
      }
    </style>
  </head>

  <body>
    <h2>Propiedades de estilo en Tablas</h2>

    <table>
      <tr>
        <th>Marca</th>

        <th>Línea</th>

        <th>Modelo</th>
      </tr>

      <tr>
        <td>Subaru</td>

        <td>Forester</td>

        <td>2020</td>
      </tr>

      <tr>
        <td>Toyota</td>

        <td>4Runner</td>

        <td>2019</td>
      </tr>

      <tr>
        <td>Mitsubishi</td>

        <td>Lancer</td>

        <td>2018</td>
      </tr>
    </table>
  </body>
</html>
```

![](img/08.png)

###  6.11. <a name='Fonts'></a>Fonts

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de fuente para algunos elementos HTML.

En el siguiente ejemplo se establecen las propiedades de fuente para el elemento `<h1>` y `<h2>` de un documento HTML. 

[src/fonts.html](src/fonts.html)

```html
<!DOCTYPE html>
<html>
  <head>
    <style>
      h1 {
        font-family: sans-serif;
      }

      h2 {
        font-family: cursive;
      }
    </style>
  </head>

  <body>
    <h1>Este elemento usa fuente Sans Serif</h1>
    <h2>Este elemento usa fuente Cursiva</h2>
  </body>
</html>
```

![](img/09.png)

###  6.12. <a name='Colors'></a>Colors

A través del Lenguaje de estilo en cascada CSS se puede cambiar el color de los elementos HTML.

En el siguiente ejemplo se establece un color en código RGB y en código Hexadecimal para un elemento `<h1>` y un elemento `<p>` de un documento HTML. 

[src/colors.html](src/colors.html)

```html
<!DOCTYPE html>
<html>
  <head>
    <style>
      h1 {
        color: rgb(38, 99, 229);
      }

      p {
        color: rgb(131, 36, 226);
      }
    </style>
  </head>

  <body>
    <h1>Este elemento tiene un color RGB (38, 99, 229 )</h1>

    <p>Este elemento tiene un color RGB (131, 36, 226)</p>
  </body>
</html>
```

![](img/10.png)


###  6.13. <a name='List'></a>List

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de estilo de las listas HTML.

En el siguiente ejemplo se establecen las propiedades de estilo a la lista de marcas de vehículos. 

[src/list.html](src/list.html)

```html
<!DOCTYPE html>

<html>
  <head>
    <style>
      ul.a {
        list-style-type: square;
      }

      ol.b {
        list-style-type: lower-alpha;
      }
    </style>
  </head>

  <body>
    <p>Listas ordenadas:</p>

    <ul class="a">
      <li>Ford</li>
      <li>Toyota</li>
      <li>Subaru</li>
    </ul>

    <p>Listas no ordenadas:</p>

    <ol class="b">
      <li>Subaru</li>
      <li>Toyota</li>
      <li>Ford</li>
    </ol>
  </body>
</html>
```

![](img/11.png)

###  6.14. <a name='Align'></a>Align

A través del Lenguaje de estilo en cascada CSS se pueden configurar las propiedades de alineación para algunos elementos HTML.

En el siguiente ejemplo se establecen las propiedades de alineación para los elementos `<h1>`, `<h2>`, `<h3>` y `<h4>` de un documento HTML. 

[src/align.html](src/align.html)

```html
<!DOCTYPE html>

<html>
  <head>
    <style>
      h1 {
        text-align: center;
      }

      h2 {
        text-align: left;
      }

      h3 {
        text-align: right;
      }

      h4 {
        text-align: justify;
      }
    </style>
  </head>

  <body>
    <h1>Este elemento está alineado al centro</h1>
    <h2>Este elemento está alineado a la izquierda</h2>
    <h3>Este elemento está alineado a la derecha</h3>
    <h4>
      It is a long established fact that a reader will be distracted by the
      readable content of a page when looking at its layout. The point of using
      Lorem Ipsum is that it has a more-or-less normal distribution of letters,
      as opposed to using 'Content here, content here', making it look like
      readable English. Many desktop publishing packages and web page editors
      now use Lorem Ipsum as their default model text, and a search for 'lorem
      ipsum' will uncover many web sites still in their infancy. Various
      versions have evolved over the years, sometimes by accident, sometimes on
      purpose (injected humour and the like). Este elemento está justificado
    </h4>
  </body>
</html>
```

![](img/12.png)

##  7. <a name='VideoparteI-Mdulo0LenguajeCSSparte1'></a>Video parte I -  Módulo 0 Lenguaje CSS parte 1

{{< youtube NdK14BKJsvE >}}

[https://www.youtube.com/watch?v=NdK14BKJsvE](https://www.youtube.com/watch?v=NdK14BKJsvE)

Ejemplo: [src/html&css.html](src/html&css.html)

Seguimos con el ejemplo previo y revisamos las dos formas de trabajar con CSS.

* Hoja de estilos en un fichero externo con `<link rel="stylesheet" type="text/css" href="styles.css">`.
* Directamente dentro del propio HTML con la etiqueta `<style>`.

Definimos un color de fondo y un tipo de letra para todo el cuerpo del documento en styles.css, asociamos el estilo a la etiqueta HTML con los estilos entre "{ ... }".

```
body {
    font-family: Arial;
    background-color: rgb(226,238,8);
}
```

Por el momento quitamos el color de fondo de arriba y definimos el estilo del `header`.

```
header {
    background-color: rgb(226,238,8);
    padding: 20px; /* espacio entre contenidos y borde elemento */
    text-align: center;
    font-size: 20px;
    border: groove; /* https://www.w3schools.com/css/css_border.asp */
    border-color: rgb(0,0,129);
}
```

Podemos consultar la propiedad del borde en [https://www.w3schools.com/css/css_border.asp](https://www.w3schools.com/css/css_border.asp). Probamos con otros tipos de border como "dotted". Damos color específico al borde `border-color: rgb(0,0,129);`. 

![](img/13.png)

Recurso Web para colores: [https://htmlcolorcodes.com/](https://htmlcolorcodes.com/) y documentación [https://www.w3schools.com/css/css_colors.asp](https://www.w3schools.com/css/css_colors.asp).

Elemento `<nav>` con el menú:

```css
nav {
    float: left;
    width: 10%;
    height: 100%;
    background: rgb(226,238,8);
    padding: 30px;
    border: groove;
    border-color: rgb(0,0,129);
}
```

Si cambiamos a `float: center;` cambia el aspecto ya que comparte el espacio con `<article>` dentro del `<section>`:

![](img/14.png)

Podemos probar a modificar al ancho de section y nav. Pruebo a modificar la altura, del 100% al 0% `height: 0%;`, realmente no se aprecia ningún cambio ya que la altura se ajusta al contenido. Juego con el `padding: 20px;` para centrar el menú en su caja.


##  8. <a name='VideoparteII-Mdulo0LenguajeCSSparte2'></a>Video parte II - Módulo 0 Lenguaje CSS parte2

{{< youtube WnJy061CslY >}}

[https://www.youtube.com/watch?v=WnJy061CslY](https://www.youtube.com/watch?v=WnJy061CslY)

Ahora colocamos un padding de `padding: 20px;` para separar un poco los contenidos de `article`. Colocamos un fondo blanco con `background: rgb(255,255,255);`.

Estilo de las tablas

```css
table {
    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
    border-collapse: collapse;
    width: 60%;
    float: center;
}

td,th {
    border: 2px solid RGB(0,0,0);
    text-align: center;
    padding: 8px;
}

/* Filas impares */
tr:nth-child(even) {
    background-color: rgb(226, 238, 8);
}
```

![](img/15.png)

