---
title: Metodología Kanban de David J.Anderson (II)
subtitle: La pizarra digital inspirada en Kanban
date: 2019-07-02
draft: false
description: Aplicación
tags:
  [
    "Trello",
    "Talleres",
    "Cursos",
    "Aplicaciones",
    "Trello - Introducción",
    "Kanban",
    "Metodologías",
    "Gestión de Proyectos",
    "Herramientas",
  ]
---

![](img/logo-trello.png)

## Método Kanban

La palabra **kanban** se usa a menudo a lo largo de este post pero no en todas las ocasiones se escribe en mayúsculas, en el [post anterior](https://soka.gitlab.io/blog/post/2019-07-02-trello-que-es-kanban/) me he referido en todo momento a la palabra Japonesa **kanban** empleado dentro del proceso de producción de Toyota para **controlar el trabajo en curso** y referido a las tarjetas físicas con información o a tableros visuales. Estos sistemas fueron una de las muchas inspiraciones que están detrás del **Método Kanban**.

El **Método Kanban** escrito en con la primera letra en mayúscula tiene su origen en el año 2007 de una serie de metodologías de gestión ideadas por **David J. Anderson** mientras trabajaba en Microsoft y [Corbis](https://en.wikipedia.org/wiki/Branded_Entertainment_Network).

![](img/02.jpeg)

Es importante **diferenciar entre el método que utilizamos y el sistema sobre el que actuamos**. Muchos equipos piensan que están haciendo **Kanban** cuando en realidad sólo usan un tablero visual sin imponer ninguna de la prácticas básicas del método, a esto se de suele denominar un sistema **protokanban**. El simple uso de un tablero visual no significa que ya estemos usando una metodología ágil.

Esta metodología también la podemos enmarcar dentro de las **metodologías Ágiles o “Agile”**, que pretender reducir la burocracia en los proyectos y dar respuestas rápidas a la gestión del cambio o imprevistos asociados a cualquier proyecto.

## Kanban esencial condensado

![](img/01.PNG)

La metodología **Kanban** se ocupa del **diseño, la gestión y la mejora de los sistemas
de flujo (_workflows_)** en el ámbito de la gestión del conocimiento.

**Los elementos de trabajo fluyen a través de diferentes etapas para acabar convirtiéndose en valor** para el "cliente" final o el destinatario.

**Kanban** es un **sistema visual que modela el ciclo de vida de los elementos de trabajo e impone restricciones al trabajo en curso** (**WiP**, del inglés _Work in Progress_).

Las **políticas para limitar el WiP crean un sistema de arrastre** (sistema _pull_ VS _push_), el trabajo es “arrastrado” al sistema cuando otro de los trabajos es completado y queda capacidad disponible, en lugar de “empujar” estos trabajos al sistema cuando hay nuevo trabajo
demandado.

## Valores Kanban

![](img/03.jpeg)

**El Método Kanban está guiado por valores**, según David J.Anderson son los siguientes:

1. Transparencia.
2. Equilibrio.
3. Colaboración.
4. Foco en el cliente.
5. Flujo
6. Liderazgo
7. Entendimiento.
8. Acuerdo
9. Respeto.

**Trello** y otros tableros digitales hacen suyos algunos de estos valores de forma natural, algunos ejemplos son las menciones a otros usuarios (@) invitando a la colaboración en una tarjeta o la revisión de un tablero por parte del equipo del proyecto de forma periódica para realizar un seguimiento de un proyecto, normalmente todos los tableros son visibles para todos los miembros del equipo y pueden consultar cualquier tarjeta. Permite que todos los miembros del equipo tengan una **visión global, común y compartida de un proyecto**.

- **Transparencia**: La creencia de que **compartir información abiertamente mejora el flujo de valor de negocio**. Utilizar un lenguaje claro y directo es parte del valor.
- **Equilibrio**: El entendimiento de que los diferentes aspectos, puntos de vista y capacidades deben ser equilibrados para conseguir efectividad. **Algunos aspectos (como demanda y capacidad) causarán colapso si no se encuentran equilibrados** por un periodo prolongado.
- **Colaboración** Trabajar juntos. El Método Kanban fue formulado para mejorar la manera en que las personas trabajan juntas, por ello, la colaboración está en su corazón.
- **Foco en el cliente**: Conociendo el objetivo para el sistema. **Cada sistema kanban fluye a un punto de valor realizable** — cuando los clientes reciben un elemento solicitado o servicio. Los clientes en este contexto son externos al servicio, pero pueden ser internos o externos a la organización como un todo. Los clientes y el valor que estos reciben es el foco natural en Kanban.
- **Flujo** La realización de ese trabajo es el flujo de valor, tanto si es continuo como puntual. **Ver el flujo es un punto de partida esencial en el uso de Kanban**.
- **Liderazgo**: La habilidad de **inspirar a otros a la acción a través de ejemplo**, de las palabras y la reflexión. Muchas organizaciones tienen diferentes grados de jerarquía estructural, pero **en Kanban, el liderazgo es necesario a todos los niveles** para alcanzar la entrega de valor y la mejora.
- **Entendimiento**: Principalmente conocimiento de sí mismo (tanto individual como de la organización) para ir hacia adelante. Kanban es un método de mejora, por lo que conocer el punto de inicio es la base de todo.
- **Acuerdo**: El compromiso de avanzar juntos hacia los objetivos, respetando — y donde sea posible, acomodando — las diferencias de opinión o aproximaciones. Esto no es gestión por consenso sino un co-compromiso dinámico para mejorar.
- **Respeto**: Valorando, entendiendo y mostrando consideración por las personas.

## Enlaces internos

- ["Origen de los sistemas kanban en Toyota (I)"](https://soka.gitlab.io/blog/post/2019-07-02-trello-que-es-kanban/).
- ["Breve historia de Trello (III)"](https://soka.gitlab.io/blog/post/2019-07-02-trello-historia/).

## Enlaces externos

- ["Kanban esencial condensado"](https://leankanban.com/wp-content/uploads/2017/11/EssentialKanbanSpanishEversion.pdf).
