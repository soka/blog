---
title: Pipes en R
subtitle: Encadenando comandos para transformar datos
date: 2019-10-05
draft: false
description: Ciencia de datos con R
tags: ["R","R - Pipes","R - magrittr","R - pipeR","R- dplyr"]
---

![](images/FreeThoughtfulAmericancrayfish-size_restricted.gif)

Los _pipes_ o [tuberías](https://es.wikipedia.org/wiki/Tuber%C3%ADa_(inform%C3%A1tica)) (me suena fatal) están presentes en casi todos los lenguajes de programación, permiten encadenar una serie de procesos conectados de forma que la salida de uno alimenta la entrada del siguiente y facilita la legibilidad.

Este es un ejemplo básico usando la línea de comandos y el comando pipe `|` en Linux, listamos el contenido de un directorio, del resultado obtenemos los 3 primeros archivos y a continuación el último elemento con `tail` de esos tres:

```bash
$ ls | head -3 | tail -1
```

Creo que el paquete base de R no incluye un operador para crear _pipes_, pero R son sus paquetes y existen varios con este propósito.

## Operadores de pipe con el paquete magrittr

En [este PDF](magrittr.pdf) está la referencia completa del paquete descargada de este [enlace](https://cran.r-project.org/web/packages/magrittr/magrittr.pdf).

El operador más usado es `%>%`, hacer esto `c(1,3,4) %>% mean()` es equivalente a `mean(c(1,3,4))`.

Para una función que acepta varios parámetros hacer `x %>% f(y)` es equivalente a `f(x, y)`.

Podemos usar el operador varias veces,`x %>% f %>% g %>% h` es equivalente a `h(g(f(x)))`.

A continuación genero una muestra aleatoria de 10 números usando la función de [distribución normal](https://es.wikipedia.org/wiki/Distribuci%C3%B3n_normal) [`rnorm`](https://www.rdocumentation.org/packages/stats/versions/3.6.1/topics/Normal), calculo la media y la redondeo:

```r
x <- 10
x %>% rnorm() %>% mean() %>% round()
```

Otro ejemplo creando un diagrama con la función [`plot`](https://www.rdocumentation.org/packages/graphics/versions/3.6.1/topics/plot):

```r
rnorm(100) %>% plot
```

![](images/01.png)

### Comandos adicionales

A pesar de que el operador `%>%` es el más usado del paquete magrittr este ofrece otros.

El operador `%<>%` ejecuta el pipe y vuelve a asignar el valor a la variable:

```r
x <- rnorm(100)
x %<>% abs %>% sort
```

El operador _tee_ `%T>%` se usa para devolver el valor original en vez del resultado (se lo llama “T” porque literalmente tiene la forma de una T).

```r
rnorm(100) %>%
  matrix(ncol = 2) %T>%
  plot() %>%
  str()
```

La expresión de arriba solo formada por `%>%` hubiese retornado NULL (pruébalo).

El operador `%$%` se usa para exponer los nombres de las variables y hacer referencia a ellas, uso el _dataset_ [mtcars](https://www.rdocumentation.org/packages/datasets/versions/3.6.1/topics/mtcars) para buscar la [correlación](https://es.wikipedia.org/wiki/Correlaci%C3%B3n) entre dos de sus variables con la función estadística [`cor`](https://www.rdocumentation.org/packages/stats/versions/3.6.1/topics/cor).

```r
mtcars %$%
  cor(disp, mpg)
```


### Ejemplos

Ahora cargo el paquete [dplyr](https://www.rdocumentation.org/packages/dplyr/versions/0.7.8) que ofrece funciones de manipulación de datos y además incluye un dataset ["storms"](https://dplyr.tidyverse.org/reference/storms.html) con datos de tormentas que usaré de ejemplo.

Uso el pipe para seleccionar dos variables del dataset usando [`select`](https://www.rdocumentation.org/packages/dplyr/versions/0.7.8/topics/select):

```r
storms %>% select(wind,pressure)
```

Filtro el dataset usando un valor de una variable como condición con [`filter`](https://www.rdocumentation.org/packages/dplyr/versions/0.7.8/topics/filter):

```r
storms %>% filter(wind>=50)
```

Si encadeno varios operaciones se ve el potencial de los pipes:

```r
storms %>%
  filter(wind>=50) %>%
  select(wind, pressure)
```

Atajo de teclado para escribir el operador `%>%`:

![](images/pipelineShortcut.PNG)

[[Fuente](https://rsanchezs.gitbooks.io/rprogramming/content/chapter9/pipeline.html)]

Este ejemplo también es muy interesante, aplica varios pipes para obtener primero un subconjunto del _dataset_ [iris](https://rpubs.com/moeransm/intro-iris) basado en los valores de una variable que cumplen una condición con [`subset`](https://www.rdocumentation.org/packages/base/versions/3.6.1/topics/subset), después con el subconjunto obtenido aplico [`aggregate`](https://www.rdocumentation.org/packages/stats/versions/3.6.1/topics/aggregate) para obtener subconjuntos agrupados por especies y calculo la media de las variables:

```r
> iris %>%
+   subset(Sepal.Length > 5) %>%
+   aggregate(. ~ Species, ., mean)
     Species Sepal.Length Sepal.Width Petal.Length Petal.Width
1     setosa     5.313636    3.713636     1.509091   0.2772727
2 versicolor     5.997872    2.804255     4.317021   1.3468085
3  virginica     6.622449    2.983673     5.573469   2.0326531
```

El "." se usa para indicar donde se usa el argumento recibido por el _pipe_ (_argument placeholder_).

## Paquete pipeR

El paquete [pipeR](https://www.rdocumentation.org/packages/pipeR) introduce el operador `%>>%`:

```r
rnorm(100) %>>%
  plot(col="red")
```  

Como antes Usamos "." para posicionar el argumento:

```r
rnorm(100) %>>%
  plot(col="red", main=length(.))
```  

La función [**Pipe**](https://www.rdocumentation.org/packages/pipeR/versions/0.6.1.3/topics/Pipe) crea un objeto que almacena un dato y permite concatenar comandos con `$` (_Object-based pipeline_).

```r
Pipe(rnorm(100))$mean()
```

## Código fuente

- r / intro /55-R-pipes / [pipes.R](https://gitlab.com/soka/r/blob/master/intro/55-R-pipes/pipes.R).

## Enlaces internos

- ["R - paquete dplyr"](https://soka.gitlab.io/blog/post/2019-07-11-r-dplyr-intro/).
- [Enlaces a todos mis posts sobre R en GitLab](https://gitlab.com/soka/r).

## Enlaces externos

- rdocumentation.org ["Pipe"](https://www.rdocumentation.org/packages/pipeR/versions/0.6.1.3/topics/Pipe): Create a Pipe object that stores a value and allows command chaining with $.
- rsanchezs.gitbooks.io ["El operador pipe %>%"](https://rsanchezs.gitbooks.io/rprogramming/content/chapter9/pipeline.html): De este manual Web he obtenido los ejemplos de _pipes_ con dplyr y el _dataset_ storms.
- datacamp.com ["Pipes in R Tutorial For Beginners"](https://www.datacamp.com/community/tutorials/pipe-r-tutorial): Excelente resumen sobre pipes y de donde he sacado algunos ejemplos.
- magrittr.tidyverse.org ["magrittr - Overview"](https://magrittr.tidyverse.org/).
