---
title: Método Polya
subtitle: Resolución de problemas 
date: 2021-06-23
draft: true
description: 
tags: ["Metodologías","Problemas","Productividad","Polya"]
---

<!-- vscode-markdown-toc -->
* 1. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->



##  1. <a name='Enlacesexternos'></a>Enlaces externos

* [https://www.cs.kent.ac.uk/people/staff/sjt/Haskell_craft/HowToProgIt.html](https://www.cs.kent.ac.uk/people/staff/sjt/Haskell_craft/HowToProgIt.html)